import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/bloc/drafts/draft_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/drafts/draft_event.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text_button.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class DraftsSetting extends StatefulWidget {
  const DraftsSetting({super.key, required this.draftBloc, required this.bloc});

  final DraftBloc draftBloc;
  final BuilderBloc bloc;

  @override
  State<DraftsSetting> createState() => _DraftsSettingState();
}

class _DraftsSettingState extends State<DraftsSetting> {
  late dynamic listDrafts;

  @override
  void initState() {
    listDrafts = widget.draftBloc.state.draftSetting;
    super.initState();
  }

  // This function is used to use a draft
  void useDraft(int index) {
    widget.bloc
        .add(ChangeSettingEvent(settings: listDrafts[index]['draftSettings']));
    Navigator.pop(context);
  }

  // This function is used to remove a draft.
  void removeDraft(int index) {
    if (listDrafts.length == 1) {
      widget.draftBloc.add(ClearDraftEvent());
    } else {
      widget.draftBloc.add(RemoveDraftEvent(index: index));
    }
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 520,
      height: 520,
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Drafts Setting'),
          Expanded(
            // [112304TIN] set default text color is black
            child: DefaultTextStyle(
              style: TextStyle(color: Colors.black87),
              child: Container(
                decoration: BoxDecoration(color: Colors.white),
                child: listDrafts.isNotEmpty
                    ? ListView.builder(
                        itemCount: listDrafts.length,
                        itemBuilder: (context, index) {
                          return Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        width: 1, color: Colors.grey))),
                            child: Row(
                              children: [
                                // NAME DRAFT --------------------------------
                                Text('Draft ${listDrafts[index]['draftId']}'),
                                Spacer(),

                                // TIME CREATED DRAFT --------------------------------
                                Text('${listDrafts[index]['draftTime']}'),
                                Spacer(),

                                // BUTTON USE DRAFT --------------------------------
                                TextButton(
                                    onPressed: () => useDraft(index),
                                    child: Text('Use')),

                                // BUTTON REMOVE DRAFT --------------------------------
                                TextButton(
                                  onPressed: () => removeDraft(index),
                                  child: Text(
                                    'Remove',
                                    style: TextStyle(color: Colors.red),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [Text('Empty draft !')],
                      ),
              ),
            ),
          ),

          // Button clear draft
          listDrafts.isNotEmpty
              ? Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.only(bottom: 15, top: 15),
                        color: Colors.white,
                        alignment: Alignment.center,
                        child: customTextBtn('Clear draft', () {
                          widget.draftBloc.add(ClearDraftEvent());
                          // print(widget.draftBloc.state.draftSetting);
                          Navigator.pop(context);
                        }),
                      ),
                    ),
                  ],
                )
              : Container()
        ],
      ),
    );
  }
}
