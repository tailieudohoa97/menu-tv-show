import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/data/json_data_builder.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/build_module_option.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

class InsertModule extends StatefulWidget {
  const InsertModule({super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<InsertModule> createState() => _InsertModuleState();
}

class _InsertModuleState extends State<InsertModule>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  dynamic listInsertModules = JsonDataBuilder.listInsertModules;
  dynamic listInsertLayout = JsonDataBuilder.listInsertLayout;
  BuilderRepository builderRepository = BuilderRepository();
  DataSetting dataSetting = DataSetting();

  @override
  void initState() {
    _tabController = _tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  // function add container
  void _addContainer() {
    final container = builderRepository.generateJsonContainer();
    widget.bloc
        .add(AddModuleEvent(widgetId: widget.widgetId, model: container));
  }

  // function add row
  void _addRow() {
    final row = builderRepository.generateJsonRow();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: row));
  }

  // function add column
  void _addColumn() {
    final column = builderRepository.generateJsonColumn();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: column));
  }

  // function add module content
  void _addModule(String moduleName, dynamic model) {
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add padding
  void _addPadding() {
    final data = builderRepository.generateJsonPadding();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: data));
  }

  // function add padding ratio
  void _addPaddingRatio() {
    final data = builderRepository.generateJsonPaddingByRatio();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: data));
  }

  // function add expanded
  void _addExpanded() {
    final data = builderRepository.generateJsonExpanded();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: data));
  }

  // function add sized box
  void _addSizedBox() {
    final model = BuilderRepository().generateJsonSizedBox();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add center
  void _addCenter() {
    final model = BuilderRepository().generateJsonCenter();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add sized box by ratio
  void _addSizedBoxRatio() {
    final model = BuilderRepository().generateJsonSizedBoxByRatio();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add stack
  void _addStack() {
    final model = BuilderRepository().generateJsonStack();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add intrinsic height
  void _addIntrinsicHeight() {
    final model = BuilderRepository().generateJsonIntrinsicHeight();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add Custom Container
  void _addCustomContainer() {
    final model = BuilderRepository().generateJsonCustomContainer();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add Wrap
  void _addWrap() {
    final model = BuilderRepository().generateJsonWrap();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add Wrap
  void _addPositioned() {
    final model = BuilderRepository().generateJsonPositioned();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add Wrap
  void _addListView() {
    final model = BuilderRepository().generateJsonListView();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add Wrap
  void _addMenuColumn1() {
    final model = BuilderRepository().generateJsonMenuColumn1();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add Category Column 1
  void _addCategoryColumn1() {
    final model = BuilderRepository().generateJsonCategoryColumn1();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add Category Column 1
  void _addCategoryColumn2() {
    final model = BuilderRepository().generateJsonCategoryColumn2();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add Category Column 1
  void _addCategoryBlock1() {
    final model = BuilderRepository().generateJsonCategoryBlock1();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add Category Column 1
  void _addCategoryBlock2() {
    final model = BuilderRepository().generateJsonCategoryBlock2();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // function add Category Column 1
  void _addRotatedBox() {
    final model = BuilderRepository().generateJsonRotateBox();
    widget.bloc.add(AddModuleEvent(widgetId: widget.widgetId, model: model));
  }

  // handle cases current layout that chose.
  void handleLayoutOptions(String value) {
    switch (value) {
      case 'Container':
        _addContainer();
        break;
      case 'Row':
        _addRow();
        break;
      case 'Column':
        _addColumn();
        break;
      case 'Padding':
        _addPadding();
        break;
      case 'Expanded':
        _addExpanded();
        break;
      case 'Sized Box':
        _addSizedBox();
        break;
      case 'Center':
        _addCenter();
        break;
      case 'Sized Box Ratio':
        _addSizedBoxRatio();
        break;
      case 'Stack':
        _addStack();
        break;
      case 'List View':
        _addListView();
        break;
      case 'Positioned':
        _addPositioned();
        break;
      case 'Rotated Box':
        _addRotatedBox();
        break;
      case 'Padding Ratio':
        _addPaddingRatio();
        break;
      case 'Intrinsic Height':
        _addIntrinsicHeight();
      case 'Custom Container':
        _addCustomContainer();
        break;
      case 'Wrap':
        _addWrap();
        break;
      case 'Menu Column 1':
        _addMenuColumn1();
        break;
      case 'Category Column 1':
        _addCategoryColumn1();
        break;
      case 'Category Column 2':
        _addCategoryColumn2();
        break;
      case 'Category Block 1':
        _addCategoryBlock1();
        break;
      case 'Category Block 2':
        _addCategoryBlock2();
        break;
      default:
        null;
    }
  }

  // handle cases current module content that chose.
  void handleContentOptions(String value) {
    dynamic model = {};

    switch (value) {
      case 'Text':
        model = builderRepository.generateJsonText();
      case 'Product':
        model = builderRepository.generateJsonProduct();
      case 'List Product':
        model = builderRepository.generateJsonProductList();
      case 'Image':
        model = builderRepository.generateJsonImage();
      case 'Simple Image':
        model = builderRepository.generateJsonSimpleImage();
      case 'Center':
        model = builderRepository.generateJsonCenter();
      case 'Category Title Column':
        model = builderRepository.generateJsonCategoryTitleSingleColumn();
      case 'Category Title Grid 1':
        model = builderRepository.generateJsonCategoryTitleGridLayout1();
      case 'Category Title Grid 3':
        model = builderRepository.generateJsonCategoryTitleGridLayout3();
      case 'Category Title Grid 4':
        model = builderRepository.generateJsonCategoryTitleGridLayout4();
      case 'Category Title':
        model = builderRepository.generateJsonCategoryTitle();
      case 'Indexed Menu Item':
        model = builderRepository.generateJsonIndexedMenuItem();
      case 'Divider':
        model = builderRepository.generateJsonDivider();
      case 'Vertical Divider':
        model = builderRepository.generateJsonVerticalDivider();
      case 'Underline Brand':
        model = builderRepository.generateJsonUnderlineBrand();
      case 'Menu Item Price':
        model = builderRepository.generateJsonMenuItemPrice();
      case 'Service Item Column':
        model = builderRepository.generateJsonServiceItemColumn();
      case 'Service Item Custom1':
        model = builderRepository.generateJsonServiceCustom1();
      case 'Service Item Thumbnail':
        model = builderRepository.generateJsonServiceItemWithThumbnail();
      case 'Framed Image':
        model = builderRepository.generateJsonFramedImage();
      case 'Product Row 1':
        model = builderRepository.generateJsonProductRow1();
      case 'Product Row 2':
        model = builderRepository.generateJsonProductRow2();
      case 'Product Row 3':
        model = builderRepository.generateJsonProductRow3();
      default:
        model;
    }

    _addModule(value, model);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 400,
      height: 430,
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Choose Module'),
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Layouts", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Contents', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  buildListInsertOptions(listInsertLayout, handleLayoutOptions),
                  buildListInsertOptions(
                      listInsertModules, handleContentOptions)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  // list option modules, each option is a module that includes icons and name
  GridView buildListInsertOptions(dynamic listInsert, Function onTap) {
    return GridView.builder(
      padding: EdgeInsets.all(24),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
          childAspectRatio: 6 / 2),
      shrinkWrap: true,
      itemCount: listInsert.length,
      itemBuilder: (context, index) {
        return InkWell(
            onTap: () {
              onTap(listInsert[index].keys.first);
              Navigator.pop(context);
            },
            child: buildModuleOption(
                listInsert[index].values.first, listInsert[index].keys.first));
      },
    );
  }
}
