import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class PaddingSetting extends StatefulWidget {
  const PaddingSetting({super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<PaddingSetting> createState() => _PaddingSettingState();
}

class _PaddingSettingState extends State<PaddingSetting> {
  TextEditingController controllerPaddingAll = TextEditingController();
  TextEditingController controllerPaddingVertical = TextEditingController();
  TextEditingController controllerPaddingHorizontal = TextEditingController();
  TextEditingController controllerPaddingTop = TextEditingController();
  TextEditingController controllerPaddingRight = TextEditingController();
  TextEditingController controllerPaddingBottom = TextEditingController();
  TextEditingController controllerPaddingLeft = TextEditingController();

  String? selectedUnitAll,
      selectedUnitVertical,
      selectedUnitHorizontal,
      selectedUnitTop,
      selectedUnitRight,
      selectedUnitBottom,
      selectedUnitLeft;

  Map<String, dynamic> currentSetting = {};

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();

    controllerPaddingAll.text =
        ConvertUnit.unitToString(currentSetting['padding']['all']);
    controllerPaddingVertical.text =
        ConvertUnit.unitToString(currentSetting['padding']['vertical']);
    controllerPaddingHorizontal.text =
        ConvertUnit.unitToString(currentSetting['padding']['horizontal']);
    controllerPaddingTop.text =
        ConvertUnit.unitToString(currentSetting['padding']['top']);
    controllerPaddingRight.text =
        ConvertUnit.unitToString(currentSetting['padding']['right']);
    controllerPaddingBottom.text =
        ConvertUnit.unitToString(currentSetting['padding']['bottom']);
    controllerPaddingLeft.text =
        ConvertUnit.unitToString(currentSetting['padding']['left']);

    selectedUnitAll = ConvertUnit.getUnit(currentSetting['padding']['all']);
    selectedUnitVertical =
        ConvertUnit.getUnit(currentSetting['padding']['vertical']);
    selectedUnitHorizontal =
        ConvertUnit.getUnit(currentSetting['padding']['horizontal']);
    selectedUnitTop = ConvertUnit.getUnit(currentSetting['padding']['top']);
    selectedUnitRight = ConvertUnit.getUnit(currentSetting['padding']['right']);
    selectedUnitBottom =
        ConvertUnit.getUnit(currentSetting['padding']['bottom']);
    selectedUnitLeft = ConvertUnit.getUnit(currentSetting['padding']['left']);
  }

  void changeValueUnitAll(String? value) {
    setState(() {
      selectedUnitAll = value!;
    });
  }

  void changeValueUnitVertical(String? value) {
    setState(() {
      selectedUnitVertical = value!;
    });
  }

  void changeValueUnitHorizontal(String? value) {
    setState(() {
      selectedUnitHorizontal = value!;
    });
  }

  void changeValueUnitTop(String? value) {
    setState(() {
      selectedUnitTop = value!;
    });
  }

  void changeValueUnitRight(String? value) {
    setState(() {
      selectedUnitRight = value!;
    });
  }

  void changeValueUnitBottom(String? value) {
    setState(() {
      selectedUnitBottom = value!;
    });
  }

  void changeValueUnitLeft(String? value) {
    setState(() {
      selectedUnitLeft = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "padding": {
        "all": ConvertUnit.stringToUnit(
            controllerPaddingAll.text, selectedUnitAll!),
        "vertical": ConvertUnit.stringToUnit(
            controllerPaddingVertical.text, selectedUnitVertical!),
        "horizontal": ConvertUnit.stringToUnit(
            controllerPaddingHorizontal.text, selectedUnitHorizontal!),
        "top": ConvertUnit.stringToUnit(
            controllerPaddingTop.text, selectedUnitTop!),
        "right": ConvertUnit.stringToUnit(
            controllerPaddingRight.text, selectedUnitRight!),
        "bottom": ConvertUnit.stringToUnit(
            controllerPaddingBottom.text, selectedUnitBottom!),
        "left": ConvertUnit.stringToUnit(
            controllerPaddingLeft.text, selectedUnitLeft!)
      },
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Padding'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    buildAdjustPadding(context),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // This method is used to adjust padding
  Column buildAdjustPadding(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'All',
                  controllerPaddingAll, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitAll!,
                changeValueUnitAll, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        //
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Vertical',
                  controllerPaddingVertical, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitVertical!,
                changeValueUnitVertical, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        //
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Horizontal',
                  controllerPaddingHorizontal, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitHorizontal!,
                changeValueUnitHorizontal, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Top',
                  controllerPaddingTop, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitTop!,
                changeValueUnitTop, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Right',
                  controllerPaddingRight, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitRight!,
                changeValueUnitRight, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Bottom',
                  controllerPaddingBottom, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitBottom!,
                changeValueUnitBottom, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Left',
                  controllerPaddingLeft, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitLeft!,
                changeValueUnitLeft, ['none', 'w', 'h', 'sp'])
          ],
        ),
      ],
    );
  }
}
