import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/data/json_data_builder.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class RowSetting extends StatefulWidget {
  const RowSetting({super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<RowSetting> createState() => _RowSettingState();
}

class _RowSettingState extends State<RowSetting> {
  String? selectedMainAxis, selectedCrossAxis;
  bool textRightToLeft = false;
  Map<String, dynamic> currentSetting = {};

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    textRightToLeft = bool.parse(currentSetting['textRightToLeft']);
    selectedMainAxis = currentSetting['mainAxisAlignment'];
    selectedCrossAxis = currentSetting['crossAxisAlignment'];
  }

  // Get list main axis options.
  List<String> mainAxisOptions = JsonDataBuilder.mainAxisOptions;
  // This function is used to change main axis value.
  void changeValueMainAxis(String? value) {
    setState(() {
      selectedMainAxis = value!;
    });
  }

  // Get list cross axis options.
  List<String> crossAxisOptions = JsonDataBuilder.crossAxisOptions;
  // This function is used to change main cross value.
  void changeValueCrossAxis(String? value) {
    setState(() {
      selectedCrossAxis = value!;
    });
  }

  // This function is used to change use text right to left value.
  void changeValueTextRightToLeft(bool value) {
    setState(() {
      textRightToLeft = value;
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "textRightToLeft": textRightToLeft.toString(),
      "mainAxisAlignment": selectedMainAxis,
      "crossAxisAlignment": selectedCrossAxis,
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 360,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Row'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADJUST USE TEXT RIGHT TO LEFT -----------------------------------
                    ReusableSetting.buildToggleSwitch(
                        context,
                        'Text right to left',
                        textRightToLeft,
                        changeValueTextRightToLeft),
                    SizedBox(height: 20),

                    Align(
                      alignment: Alignment.topLeft,
                      child:
                          customText('Main alignment ', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(
                        context,
                        selectedMainAxis!,
                        changeValueMainAxis,
                        mainAxisOptions),
                    SizedBox(height: 20),

                    Align(
                      alignment: Alignment.topLeft,
                      child:
                          customText('Cross alignment ', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(
                        context,
                        selectedCrossAxis!,
                        changeValueCrossAxis,
                        crossAxisOptions),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
