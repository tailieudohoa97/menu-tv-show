import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/data/json_data_builder.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class TextSetting extends StatefulWidget {
  const TextSetting(
      {super.key,
      this.widgetId,
      required this.bloc,
      this.onUpdateTextStyle,
      this.currentTextStyle});

  final String? widgetId;
  final BuilderBloc bloc;
  final Function? onUpdateTextStyle;
  final Map<String, dynamic>? currentTextStyle;

  @override
  State<TextSetting> createState() => _TextSettingState();
}

class _TextSettingState extends State<TextSetting> {
  TextEditingController controllerFontSize = TextEditingController();
  TextEditingController controllerContent = TextEditingController();
  TextEditingController controllerHeight = TextEditingController();
  TextEditingController controllerLetterSpacing = TextEditingController();
  TextEditingController controllerFontSizeFieldKey = TextEditingController();

  TextEditingController controllerColorText = TextEditingController();

  Map<String, dynamic> currentSetting = {};
  String? selectedFontWeight,
      selectedTextAlign,
      selectedFontFamily,
      selectedFontStyle;
  String? selectedUnitFontSize, selectedUnitHeight, selectedUnitLetterSpacing;

  @override
  void initState() {
    super.initState();

    if (widget.currentTextStyle != null) {
      final currentTextStyle = widget.currentTextStyle!;

      controllerFontSize.text =
          ConvertUnit.unitToString(currentTextStyle["fontSize"]);
      selectedFontWeight = currentTextStyle['fontWeight'];
      controllerHeight.text =
          ConvertUnit.unitToString(currentTextStyle["height"]);
      controllerLetterSpacing.text =
          ConvertUnit.unitToString(currentTextStyle['letterSpacing']);
      selectedFontFamily = currentTextStyle['fontFamily'];
      controllerColorText.text = currentTextStyle['color'];
      selectedFontStyle = currentTextStyle["fontStyle"];

      selectedUnitFontSize = ConvertUnit.getUnit(currentTextStyle["fontSize"]);
      selectedUnitHeight = ConvertUnit.getUnit(currentTextStyle["height"]);
      selectedUnitLetterSpacing =
          ConvertUnit.getUnit(currentTextStyle['letterSpacing']);
    } else {
      currentSetting = widget.bloc.state.currentSetting['widgetSetting'];

      controllerContent.text = currentSetting['data'];
      selectedTextAlign = currentSetting['textAlign'];
      controllerFontSizeFieldKey.text = currentSetting['fontSizeFromFieldKey'];
      controllerFontSize.text =
          ConvertUnit.unitToString(currentSetting['style']["fontSize"]);
      selectedFontWeight = currentSetting["style"]['fontWeight'];
      controllerHeight.text =
          ConvertUnit.unitToString(currentSetting['style']["height"]);
      selectedFontFamily = currentSetting["style"]['fontFamily'];
      controllerColorText.text = currentSetting['style']['color'];
      controllerLetterSpacing.text =
          ConvertUnit.unitToString(currentSetting['style']['letterSpacing']);
      selectedFontStyle = currentSetting["style"]["fontStyle"];

      selectedUnitFontSize =
          ConvertUnit.getUnit(currentSetting['style']["fontSize"]);
      selectedUnitHeight =
          ConvertUnit.getUnit(currentSetting['style']["height"]);
      selectedUnitLetterSpacing =
          ConvertUnit.getUnit(currentSetting['style']['letterSpacing']);
    }
  }

  void changeValueUnitFontSize(String? value) {
    setState(() {
      selectedUnitFontSize = value!;
    });
  }

  void changeValueUnitHeight(String? value) {
    setState(() {
      selectedUnitHeight = value!;
    });
  }

  void changeValueUnitLetterSpacing(String? value) {
    setState(() {
      selectedUnitLetterSpacing = value!;
    });
  }

  // Get list font weight options.
  List<String> fontWeightOptions = JsonDataBuilder.fontWeightOptions;
  // This function is used to change font weight value.
  void changeValueFontWeight(String? value) {
    setState(() {
      selectedFontWeight = value!;
    });
  }

  // Get list font family.
  List<String> fontFamilyOptions = JsonDataBuilder.fontFamilyOptions;
  // This function is used to change font family value.
  void changeValueFontFamily(String? value) {
    setState(() {
      selectedFontFamily = value!;
    });
  }

  // Get list font style options
  List<String> fontStyleOptions = JsonDataBuilder.fontStyleOptions;
  // This function is used to change font style value.
  void changeValueFontStyle(String? value) {
    setState(() {
      selectedFontStyle = value!;
    });
  }

  // Get list text align options.
  List<String> textAlignOptions = JsonDataBuilder.textAlignOptions;
  // This function is used to change text align value.
  void changeValueTextAlign(String? value) {
    setState(() {
      selectedTextAlign = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "data": controllerContent.text,
      "textAlign": selectedTextAlign,
      "fontSizeFromFieldKey": controllerFontSizeFieldKey.text,
      "style": {
        "fontSize": ConvertUnit.stringToUnit(
            controllerFontSize.text, selectedUnitFontSize!),
        "fontWeight": selectedFontWeight,
        "height": ConvertUnit.stringToUnit(
            controllerHeight.text, selectedUnitHeight!),
        "fontFamily": selectedFontFamily,
        "color": controllerColorText.text,
        "letterSpacing": ConvertUnit.stringToUnit(
            controllerLetterSpacing.text, selectedUnitLetterSpacing!),
        "fontStyle": selectedFontStyle,
      }
    };

    // update current setting product
    if (widget.onUpdateTextStyle != null) {
      widget.onUpdateTextStyle!(data['style']);
    } else {
      widget.bloc
          .add(UpdateModuleEvent(widgetId: widget.widgetId!, model: data));
    }

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 520,
      height: 520,
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Text'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    // ADJUST TEXT CONTENT -----------------------------------
                    widget.currentTextStyle == null
                        ? ReusableSetting.buildInputSetting(
                            context, 'Content', controllerContent)
                        : Container(),
                    widget.currentTextStyle == null
                        ? SizedBox(height: 20)
                        : SizedBox(height: 0),

                    // ADJUST FONT SIZE FIELD KEY -----------------------------------
                    widget.currentTextStyle == null
                        ? ReusableSetting.buildInputSetting(context,
                            'Font size field key', controllerFontSizeFieldKey)
                        : Container(),
                    widget.currentTextStyle == null
                        ? SizedBox(height: 20)
                        : SizedBox(height: 0),

                    // ADJUST FONT SIZE -----------------------------------
                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Font size',
                              controllerFontSize,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitFontSize!,
                            changeValueUnitFontSize,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),

                    // ADJUST FONT WEIGHT -----------------------------------
                    Align(
                      alignment: Alignment.centerLeft,
                      child: customText('Font weight', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(
                        context,
                        selectedFontWeight!,
                        changeValueFontWeight,
                        fontWeightOptions),
                    SizedBox(height: 20),

                    // ADJUST FONT FAMILY -----------------------------------
                    Align(
                      alignment: Alignment.centerLeft,
                      child: customText('Font family', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(
                        context,
                        selectedFontFamily!,
                        changeValueFontFamily,
                        fontFamilyOptions),
                    SizedBox(height: 20),

                    // ADJUST FONT STYLE -----------------------------------
                    Align(
                      alignment: Alignment.centerLeft,
                      child: customText('Font style', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(
                        context,
                        selectedFontStyle!,
                        changeValueFontStyle,
                        fontStyleOptions),
                    SizedBox(height: 20),

                    // ADJUST HEIGHT TEXT -----------------------------------
                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Height',
                              controllerHeight,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitHeight!,
                            changeValueUnitHeight,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),

                    // ADJUST LETTER SPACING -----------------------------------
                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Letter spacing',
                              controllerLetterSpacing,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitLetterSpacing!,
                            changeValueUnitLetterSpacing,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),

                    // ADJUST TEXT COLOR -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Color text', controllerColorText),
                    SizedBox(height: 20),

                    // ADJUST TEXT ALIGN -----------------------------------
                    widget.currentTextStyle == null
                        ? Align(
                            alignment: Alignment.centerLeft,
                            child:
                                customText('Text align ', mediumSize, textBold),
                          )
                        : Container(),
                    widget.currentTextStyle == null
                        ? ReusableSetting.buildDropdownMenu(
                            context,
                            selectedTextAlign!,
                            changeValueTextAlign,
                            textAlignOptions)
                        : Container(),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings)
        ],
      ),
    );
  }
}
