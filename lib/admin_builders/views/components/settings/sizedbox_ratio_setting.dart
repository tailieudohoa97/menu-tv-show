import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class SizedBoxRatioSetting extends StatefulWidget {
  const SizedBoxRatioSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<SizedBoxRatioSetting> createState() => _SizedBoxRatioSettingState();
}

class _SizedBoxRatioSettingState extends State<SizedBoxRatioSetting> {
  TextEditingController controllerWidth = TextEditingController();
  TextEditingController controllerHeight = TextEditingController();
  TextEditingController controllerWidthRatio = TextEditingController();
  TextEditingController controllerHeightRatio = TextEditingController();
  Map<String, dynamic> currentSetting = {};
  String? selectedUnitWidth, selectedUnitHeight;

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    controllerHeight.text = ConvertUnit.unitToString(currentSetting["height"]);
    controllerWidth.text = ConvertUnit.unitToString(currentSetting["width"]);

    selectedUnitWidth = ConvertUnit.getUnit(currentSetting["width"]);
    selectedUnitHeight = ConvertUnit.getUnit(currentSetting["height"]);
    controllerWidthRatio.text = currentSetting['widthFromFieldKey'];
    controllerHeightRatio.text = currentSetting['heightFromFieldKey'];
  }

  void changeValueUnitWidth(String? value) {
    setState(() {
      selectedUnitWidth = value!;
    });
  }

  void changeValueUnitHeight(String? value) {
    setState(() {
      selectedUnitHeight = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "width":
          ConvertUnit.stringToUnit(controllerWidth.text, selectedUnitWidth!),
      "height":
          ConvertUnit.stringToUnit(controllerHeight.text, selectedUnitHeight!),
      "widthFromFieldKey": controllerWidthRatio.text,
      "heightFromFieldKey": controllerHeightRatio.text,
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 394,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting SizedBox By Ratio'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADJUST WIDTH -----------------------------------
                    Row(children: [
                      SizedBox(
                        width: 200,
                        child: ReusableSetting.buildAdjustDouble(
                            context,
                            'Width',
                            controllerWidth,
                            increaseValue,
                            decreaseValue),
                      ),
                      Spacer(),
                      ReusableSetting.buildDropdownMenu(
                          context,
                          selectedUnitWidth!,
                          changeValueUnitWidth,
                          ['none', 'w'])
                    ]),
                    SizedBox(height: 20),

                    // ADJUST HEIGHT -----------------------------------
                    Row(children: [
                      SizedBox(
                        width: 200,
                        child: ReusableSetting.buildAdjustDouble(
                            context,
                            'Height',
                            controllerHeight,
                            increaseValue,
                            decreaseValue),
                      ),
                      Spacer(),
                      ReusableSetting.buildDropdownMenu(
                          context,
                          selectedUnitHeight!,
                          changeValueUnitHeight,
                          ['none', 'h'])
                    ]),
                    SizedBox(height: 20),

                    // ADJUST WIDTH FROM RATIO -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Width field key', controllerWidthRatio),
                    SizedBox(height: 20),

                    // ADJUST PRODUCT NAME -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Height field key', controllerHeightRatio),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
