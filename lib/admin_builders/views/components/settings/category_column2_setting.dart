import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/box_decoration_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/category_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/margin_padding_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';
import 'package:tv_menu/admin_builders/views/components/settings/text_setting.dart';

class CategoryColumn2Setting extends StatefulWidget {
  const CategoryColumn2Setting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<CategoryColumn2Setting> createState() => _CategoryColumn2SettingState();
}

class _CategoryColumn2SettingState extends State<CategoryColumn2Setting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerCatImageHeight = TextEditingController();
  TextEditingController controllerCatImageWidth = TextEditingController();
  TextEditingController controllerElementGap = TextEditingController();
  TextEditingController controllerCatImageBorderRadius =
      TextEditingController();
  TextEditingController controllerCatImageWidthFromFieldKey =
      TextEditingController();
  TextEditingController controllerCatImageHeightFromFieldKey =
      TextEditingController();
  TextEditingController controllerCatFontSizeFromFieldKey =
      TextEditingController();
  TextEditingController controllerElementGapFieldKey = TextEditingController();
  bool isUpperCase = false, isHideScrollbar = false;

  Map<String, dynamic> currentSetting = {};
  int categoryItems = 0;
  dynamic category = {},
      categoryTextStyle = {},
      categoryContainerDecoration = {},
      categoryContainerPadding;
  DataSetting dataSetting = DataSetting();
  int currentIndexTab = 0;
  late TabController _tabController;
  String? selectedUnitImageWidth,
      selectedUnitImageHeight,
      selectedUnitBorderRadius,
      selectedUnitElementGap;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updatedCategory = {};

    if (currentSetting["category"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['category']) ?? {};
      updatedCategory = data;
    }

    setState(() {
      category = updatedCategory;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    initDataSetting(currentSetting);
    _tabController = TabController(length: 2, vsync: this);

    categoryItems = currentSetting['categoryItems'].length;
    categoryTextStyle = currentSetting['categoryTextStyle'] ?? {};
    categoryContainerDecoration =
        currentSetting['categoryNameContainerDecoration'] ?? {};
    categoryContainerPadding =
        currentSetting['categoryNameContainerPadding'] ?? {};
    isUpperCase = bool.parse(currentSetting["upperCaseCategoryName"]);
    isHideScrollbar = bool.parse(currentSetting["hideScrollbarWhenOverflow"]);
    controllerCatImageHeight.text =
        ConvertUnit.unitToString(currentSetting["categoryImageHeight"]);
    controllerCatImageWidth.text =
        ConvertUnit.unitToString(currentSetting["categoryImageWidth"]);
    controllerElementGap.text =
        ConvertUnit.unitToString(currentSetting["elementGap"]);
    controllerCatImageBorderRadius.text =
        ConvertUnit.unitToString(currentSetting["categoryImageBorderRadius"]);
    controllerCatFontSizeFromFieldKey.text =
        currentSetting["categoryNameFontSizeFromFieldKey"];
    controllerCatImageWidthFromFieldKey.text =
        currentSetting["categoryImageWidthFromFieldKey"];
    controllerCatImageHeightFromFieldKey.text =
        currentSetting["categoryImageHeightFromFieldKey"];
    controllerElementGapFieldKey.text =
        currentSetting["elementGapFromFieldKey"];

    selectedUnitImageWidth =
        ConvertUnit.getUnit(currentSetting["categoryImageHeight"]);
    selectedUnitImageHeight =
        ConvertUnit.getUnit(currentSetting["categoryImageWidth"]);
    selectedUnitElementGap = ConvertUnit.getUnit(currentSetting["elementGap"]);
    selectedUnitBorderRadius =
        ConvertUnit.getUnit(currentSetting["categoryImageBorderRadius"]);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changeValueUnitImageWidth(String? value) {
    setState(() {
      selectedUnitImageWidth = value!;
    });
  }

  void changeValueUnitImageHeight(String? value) {
    setState(() {
      selectedUnitImageHeight = value!;
    });
  }

  void changeValueUnitElementGap(String? value) {
    setState(() {
      selectedUnitElementGap = value!;
    });
  }

  void changeValueUnitBorderRadius(String? value) {
    setState(() {
      selectedUnitBorderRadius = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to change use upper case category name value.
  void changeValueUppercase(bool value) {
    setState(() {
      isUpperCase = value;
    });
  }

  // This function is used to change use upper case category name value.
  void changeValueHideScrollbar(bool value) {
    setState(() {
      isHideScrollbar = value;
    });
  }

  void addCategory() {
    setState(() {
      final cat = BuilderRepository().generateJsonCategoryData();
      category = cat;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeCategory() {
    // update dataSetting.
    dataSetting.removeDataOnSetting(category["id"]);
    setState(() {
      category = {};
    });
  }

  // This function is used to edit a product.
  void editCategory() {
    void updateCategory(dynamic data) {
      setState(() {
        category = data;
      });
      dataSetting.updateDataOnSetting(category["id"], category);
    }

    customDialog(
      context,
      CategoryDataSetting(
        widgetId: category['id'],
        bloc: widget.bloc,
        onUpdateCategory: updateCategory,
        currentCategory: category,
      ),
    );
  }

  void editCategoryTextStyle() {
    if (categoryTextStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          categoryTextStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          categoryTextStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: categoryTextStyle,
        ),
      );
    }
  }

  void editContainerDecoration() {
    if (categoryContainerDecoration.isEmpty) {
      var b = BuilderRepository().generateJsonBoxDecoration();

      void updateBoxDecoration(dynamic data) {
        b = data;
        setState(() {
          categoryContainerDecoration = b;
        });
      }

      customDialog(
        context,
        BoxDecorationSetting(
          bloc: widget.bloc,
          onUpdateData: updateBoxDecoration,
          currentData: b,
        ),
      );
    } else {
      // category is exists
      void updateBoxDecoration(dynamic data) {
        setState(() {
          categoryContainerDecoration = data;
        });
      }

      customDialog(
        context,
        BoxDecorationSetting(
          bloc: widget.bloc,
          onUpdateData: updateBoxDecoration,
          currentData: categoryContainerDecoration,
        ),
      );
    }
  }

  void editContainerPadding() {
    if (categoryContainerPadding.isEmpty) {
      // new setting border
      var p = BuilderRepository().generateJsonPaddingMargin();

      // update values after edit
      void updatePadding(dynamic data) {
        p = data;
        setState(() {
          categoryContainerPadding = p;
        });
      }

      // pass new setting to MarginPaddingSetting
      customDialog(
        context,
        MarginPaddingSetting(
          type: 'Padding',
          bloc: widget.bloc,
          onUpdateData: updatePadding,
          currentData: p,
        ),
      );
    } else {
      // border side is exists
      void updatePadding(dynamic data) {
        setState(() {
          categoryContainerPadding = data;
        });
      }

      customDialog(
        context,
        MarginPaddingSetting(
          type: 'Padding',
          bloc: widget.bloc,
          onUpdateData: updatePadding,
          currentData: categoryContainerPadding,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "category": category['id'] ?? "",
      "categoryImageHeight": ConvertUnit.stringToUnit(
          controllerCatImageHeight.text, selectedUnitImageHeight!),
      "categoryImageWidth": ConvertUnit.stringToUnit(
          controllerCatImageWidth.text, selectedUnitImageWidth!),
      "elementGap": ConvertUnit.stringToUnit(
          controllerElementGap.text, selectedUnitElementGap!),
      "categoryImageBorderRadius": ConvertUnit.stringToUnit(
          controllerCatImageBorderRadius.text, selectedUnitBorderRadius!),
      // "categoryItems": [],
      "upperCaseCategoryName": isUpperCase.toString(),
      "hideScrollbarWhenOverflow": isHideScrollbar.toString(),
      "categoryTextStyle": categoryTextStyle,
      "categoryNameContainerDecoration": categoryContainerDecoration,
      "categoryNameContainerPadding": categoryContainerPadding,
      "categoryImageWidthFromFieldKey":
          controllerCatImageWidthFromFieldKey.text,
      "categoryImageHeightFromFieldKey":
          controllerCatImageHeightFromFieldKey.text,
      "categoryNameFontSizeFromFieldKey":
          controllerCatFontSizeFromFieldKey.text,
      "elementGapFromFieldKey": controllerElementGapFieldKey.text,
    };

    if (category.isNotEmpty) {
      dataSetting.addDataToSetting(category['id'], category);
    }

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 520,
      height: 520,
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Category Column 2'),
          // Tab bar includes two tab: Categories and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD CATEGORIES
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              Row(
                children: [
                  customText('Add new category', mediumSize, textNormal),

                  // BUTTON ADD CATEGORY --------------------------------
                  category.isEmpty
                      ? IconButton(
                          onPressed: addCategory,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              category.isNotEmpty ? buildCategory() : Text(''),
              category.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 0),

              // ADD LIST CATEGORY ITEMS
              Row(
                children: [
                  SizedBox(
                    width: 300,
                    child: customText('Number products current: $categoryItems',
                        mediumSize, textNormal),
                  )
                ],
              )
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        ReusableSetting.buildInputSetting(
            context,
            "Category name font size field key",
            controllerCatFontSizeFromFieldKey),
        SizedBox(height: 20),

        ReusableSetting.buildInputSetting(
            context,
            "Category image width field key",
            controllerCatImageWidthFromFieldKey),
        SizedBox(height: 20),

        ReusableSetting.buildInputSetting(
            context,
            "Category image height field key",
            controllerCatImageHeightFromFieldKey),
        SizedBox(height: 20),

        ReusableSetting.buildInputSetting(
            context, "Element gap field key", controllerElementGapFieldKey),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(
                  context,
                  'Category image width',
                  controllerCatImageWidth,
                  increaseValue,
                  decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitImageWidth!,
                changeValueUnitImageWidth, ['none', 'w', 'maxFinite'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(
                  context,
                  'Category image height',
                  controllerCatImageHeight,
                  increaseValue,
                  decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitImageHeight!,
                changeValueUnitImageHeight, ['none', 'h'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Element gap',
                  controllerElementGap, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitElementGap!,
                changeValueUnitElementGap, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(
                  context,
                  'Image border radius',
                  controllerCatImageBorderRadius,
                  increaseValue,
                  decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(
                context,
                selectedUnitBorderRadius!,
                changeValueUnitBorderRadius,
                ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            customText('Category text style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editCategoryTextStyle,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            customText('Category name decoration ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editContainerDecoration,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            customText('Category name padding ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editContainerPadding,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
        SizedBox(height: 20),

        // ADJUST USE UPPERCASE CATEGORY TITLE -----------------------------------
        ReusableSetting.buildToggleSwitch(context,
            'Use uppercase category name', isUpperCase, changeValueUppercase),
        SizedBox(height: 20),

        ReusableSetting.buildToggleSwitch(context, 'Use hide scroll bar',
            isHideScrollbar, changeValueHideScrollbar),
      ],
    );
  }

  // This method is used to display a category that added.
  Widget buildCategory() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              category.isEmpty || category['name'].isEmpty
                  ? Text('New category')
                  : Text(category['name']),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT CATEGORY --------------------------------
                  IconButton(
                    onPressed: editCategory,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE CATEGORY --------------------------------
                  IconButton(
                    onPressed: removeCategory,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
