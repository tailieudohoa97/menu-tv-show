import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/data/json_data_builder.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/category_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';
import 'package:tv_menu/admin_builders/views/components/settings/text_setting.dart';

class CategoryColumn1Setting extends StatefulWidget {
  const CategoryColumn1Setting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<CategoryColumn1Setting> createState() => _CategoryColumn1SettingState();
}

class _CategoryColumn1SettingState extends State<CategoryColumn1Setting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerCatImageHeight = TextEditingController();
  TextEditingController controllerCatImageWidth = TextEditingController();
  TextEditingController controllerElementGap = TextEditingController();
  TextEditingController controllerCatImageWidthFromFieldKey =
      TextEditingController();
  TextEditingController controllerCatImageHeightFromFieldKey =
      TextEditingController();
  TextEditingController controllerCatFontSizeFromFieldKey =
      TextEditingController();
  bool isUpperCase = false;
  String? selectedMainAxis, selectedCrossAxis;

  Map<String, dynamic> currentSetting = {};
  int productRows = 0;
  dynamic category = {}, categoryTextStyle = {};
  DataSetting dataSetting = DataSetting();
  int currentIndexTab = 0;
  late TabController _tabController;
  String? selectedUnitImageWidth,
      selectedUnitImageHeight,
      selectedUnitElementGap;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updatedCategory = {};

    if (currentSetting["category"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['category']) ?? {};
      updatedCategory = data;
    }

    setState(() {
      category = updatedCategory;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    initDataSetting(currentSetting);
    _tabController = TabController(length: 2, vsync: this);

    productRows = currentSetting['productRows'].length;
    categoryTextStyle = currentSetting['categoryTextStyle'] ?? {};
    isUpperCase = bool.parse(currentSetting["upperCaseCategoryName"]);
    controllerCatImageHeight.text =
        ConvertUnit.unitToString(currentSetting["categoryImageHeight"]);
    controllerCatImageWidth.text =
        ConvertUnit.unitToString(currentSetting["categoryImageWidth"]);
    selectedMainAxis = currentSetting["mainAxisAlignment"];
    selectedCrossAxis = currentSetting["crossAxisAlignment"];
    controllerElementGap.text =
        ConvertUnit.unitToString(currentSetting["elementGap"]);
    controllerCatFontSizeFromFieldKey.text =
        currentSetting["categoryFontSizeFromFieldKey"];
    controllerCatImageWidthFromFieldKey.text =
        currentSetting["categoryImageWidthFromFieldKey"];
    controllerCatImageHeightFromFieldKey.text =
        currentSetting["categoryImageHeightFromFieldKey"];

    selectedUnitImageWidth =
        ConvertUnit.getUnit(currentSetting["categoryImageHeight"]);
    selectedUnitImageHeight =
        ConvertUnit.getUnit(currentSetting["categoryImageWidth"]);
    selectedUnitElementGap = ConvertUnit.getUnit(currentSetting["elementGap"]);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changeValueUnitImageWidth(String? value) {
    setState(() {
      selectedUnitImageWidth = value!;
    });
  }

  void changeValueUnitImageHeight(String? value) {
    setState(() {
      selectedUnitImageHeight = value!;
    });
  }

  void changeValueUnitElementGap(String? value) {
    setState(() {
      selectedUnitElementGap = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to change use upper case category name value.
  void changeValueUppercase(bool value) {
    setState(() {
      isUpperCase = value;
    });
  }

  // Get list main axis options.
  List<String> mainAxisOptions = JsonDataBuilder.mainAxisOptions;
  // This function is used to change main axis value.
  void changeValueMainAxis(String? value) {
    setState(() {
      selectedMainAxis = value!;
    });
  }

  // Get list cross axis options.
  List<String> crossAxisOptions = JsonDataBuilder.crossAxisOptions;
  // This function is used to change main cross value.
  void changeValueCrossAxis(String? value) {
    setState(() {
      selectedCrossAxis = value!;
    });
  }

  // This function is used to add numbers categories.
  void addCategory() {
    setState(() {
      final cat = BuilderRepository().generateJsonCategoryData();
      category = cat;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeCategory() {
    // update dataSetting.
    dataSetting.removeDataOnSetting(category["id"]);
    setState(() {
      category = {};
    });
  }

  // This function is used to edit a product.
  void editCategory() {
    void updateCategory(dynamic data) {
      setState(() {
        category = data;
      });
      dataSetting.updateDataOnSetting(category["id"], category);
    }

    customDialog(
      context,
      CategoryDataSetting(
        widgetId: category['id'],
        bloc: widget.bloc,
        onUpdateCategory: updateCategory,
        currentCategory: category,
      ),
    );
  }

  // text style
  void editTextStyle() {
    if (categoryTextStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          categoryTextStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          categoryTextStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: categoryTextStyle,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "category": category['id'] ?? "",
      "categoryImageWidthFromFieldKey":
          controllerCatImageWidthFromFieldKey.text,
      "categoryImageHeightFromFieldKey":
          controllerCatImageHeightFromFieldKey.text,
      "categoryFontSizeFromFieldKey": controllerCatFontSizeFromFieldKey.text,
      "categoryImageHeight": ConvertUnit.stringToUnit(
          controllerCatImageHeight.text, selectedUnitImageHeight!),
      "categoryImageWidth": ConvertUnit.stringToUnit(
          controllerCatImageWidth.text, selectedUnitImageWidth!),
      "elementGap": ConvertUnit.stringToUnit(
          controllerElementGap.text, selectedUnitElementGap!),
      "categoryTextStyle": categoryTextStyle,
      "upperCaseCategoryName": isUpperCase.toString(),
      // "productRows": [],
      "mainAxisAlignment": selectedMainAxis, // new
      "crossAxisAlignment": selectedCrossAxis // new
    };

    if (category.isNotEmpty) {
      dataSetting.addDataToSetting(category['id'], category);
    }

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 520,
      height: 520,
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Category Column 1'),
          // Tab bar includes two tab: Categories and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD CATEGORIES
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              Row(
                children: [
                  customText('Add new category', mediumSize, textNormal),

                  // BUTTON ADD CATEGORY --------------------------------
                  category.isEmpty
                      ? IconButton(
                          onPressed: addCategory,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              category.isNotEmpty ? buildCategory() : Text(''),
              category.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 0),

              // ADD LIST PRODUCTS
              Row(
                children: [
                  SizedBox(
                    width: 300,
                    child: customText('Number products current: $productRows',
                        mediumSize, textNormal),
                  )
                ],
              )
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        ReusableSetting.buildInputSetting(context,
            "Category font size field key", controllerCatFontSizeFromFieldKey),
        SizedBox(height: 20),

        ReusableSetting.buildInputSetting(context,
            "Category image width field key", controllerCatImageWidthFromFieldKey),
        SizedBox(height: 20),

        ReusableSetting.buildInputSetting(context,
            "Category image height field key", controllerCatImageHeightFromFieldKey),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(
                  context,
                  'Category image width',
                  controllerCatImageWidth,
                  increaseValue,
                  decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitImageWidth!,
                changeValueUnitImageWidth, ['none', 'w'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(
                  context,
                  'Category image height',
                  controllerCatImageHeight,
                  increaseValue,
                  decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitImageHeight!,
                changeValueUnitImageHeight, ['none', 'h'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Element gap',
                  controllerElementGap, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitElementGap!,
                changeValueUnitElementGap, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            customText('Category text style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editTextStyle,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
        SizedBox(height: 20),

        // ADJUST USE UPPERCASE CATEGORY TITLE -----------------------------------
        ReusableSetting.buildToggleSwitch(context,
            'Use uppercase category name', isUpperCase, changeValueUppercase),
        SizedBox(height: 20),

        Align(
          alignment: Alignment.topLeft,
          child: customText('Main alignment ', mediumSize, textBold),
        ),
        ReusableSetting.buildDropdownMenu(
            context, selectedMainAxis!, changeValueMainAxis, mainAxisOptions),
        SizedBox(height: 20),

        Align(
          alignment: Alignment.topLeft,
          child: customText('Cross alignment ', mediumSize, textBold),
        ),
        ReusableSetting.buildDropdownMenu(context, selectedCrossAxis!,
            changeValueCrossAxis, crossAxisOptions),
      ],
    );
  }

  // This method is used to display a category that added.
  Widget buildCategory() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              category.isEmpty || category['name'].isEmpty
                  ? Text('New category')
                  : Text(category['name']),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT CATEGORY --------------------------------
                  IconButton(
                    onPressed: editCategory,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE CATEGORY --------------------------------
                  IconButton(
                    onPressed: removeCategory,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
