import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/product_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';
import 'package:tv_menu/admin_builders/views/components/settings/divider_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/text_setting.dart';

class ProductRow2Setting extends StatefulWidget {
  const ProductRow2Setting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<ProductRow2Setting> createState() => _ProductRow2SettingState();
}

class _ProductRow2SettingState extends State<ProductRow2Setting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerMaxLineProductName = TextEditingController();
  TextEditingController controllerFractionDigitsForPrice =
      TextEditingController();
  TextEditingController controllerProductNameFontSizeFieldKey =
      TextEditingController();
  TextEditingController controllerProductPriceFontSizeFieldKey =
      TextEditingController();
  TextEditingController controllerProductDescFontSizeFieldKey =
      TextEditingController();
  TextEditingController controllerPrefixProductPrice = TextEditingController();
  TextEditingController controllerSubfixProductPrice = TextEditingController();
  Map<String, dynamic> currentSetting = {};

  bool isUpperCase = false;
  dynamic product = {},
      divider = {},
      productDescriptionStyle = {},
      productNameStyle = {},
      productPriceStyle = {};
  DataSetting dataSetting = DataSetting();
  int currentIndexTab = 0;
  late TabController _tabController;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updatedProduct = {};

    if (currentSetting["product"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['product']) ?? {};
      updatedProduct = data;
    }

    setState(() {
      product = updatedProduct;
    });
  }

  // text style
  void editDivider() {
    if (divider.isEmpty) {
      final d = BuilderRepository().generateJsonDivider();

      void updateDivider(dynamic data) {
        d['widgetSetting'] = data;
        setState(() {
          divider = d;
        });
      }

      customDialog(
        context,
        DividerSetting(
          bloc: widget.bloc,
          onUpdateData: updateDivider,
          currentData: d["widgetSetting"],
        ),
      );
    } else {
      // category is exists
      void updateDivider(dynamic data) {
        setState(() {
          divider["widgetSetting"] = data;
        });
      }

      customDialog(
        context,
        DividerSetting(
          bloc: widget.bloc,
          onUpdateData: updateDivider,
          currentData: divider["widgetSetting"],
        ),
      );
    }
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    initDataSetting(currentSetting);
    _tabController = TabController(length: 2, vsync: this);

    divider = currentSetting['divider'] ?? {};
    productDescriptionStyle =
        currentSetting['productDescriptionTextStyle'] ?? {};
    productNameStyle = currentSetting['productNameTextStyle'] ?? {};
    productPriceStyle = currentSetting['productPriceTextStyle'] ?? {};
    controllerMaxLineProductName.text = currentSetting['maxLineOfProductName'];
    controllerFractionDigitsForPrice.text =
        currentSetting['fractionDigitsForPrice'];
    controllerProductNameFontSizeFieldKey.text =
        currentSetting['productNameFontSizeFromFieldKey'];
    controllerProductPriceFontSizeFieldKey.text =
        currentSetting['productPriceFontSizeFromFieldKey'];
    controllerProductDescFontSizeFieldKey.text =
        currentSetting['productDesFontSizeFromFieldKey'];
    controllerPrefixProductPrice.text = currentSetting['prefixProductPrice'];
    controllerSubfixProductPrice.text = currentSetting['subfixProductPrice'];
    isUpperCase = bool.parse(currentSetting["upperCaseProductName"]);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to change use uppercase value.
  void changeValueUppercase(bool value) {
    setState(() {
      isUpperCase = value;
    });
  }

  // This function is used to add numbers categories.
  void addProduct() {
    setState(() {
      final p = BuilderRepository().generateJsonProductData();
      product = p;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeProduct() {
    // update dataSetting.
    dataSetting.removeDataOnSetting(product["id"]);
    setState(() {
      product = {};
    });
  }

  // This function is used to edit a product.
  void editProduct() {
    void updateProduct(dynamic data) {
      setState(() {
        product = data;
      });
      dataSetting.updateDataOnSetting(product["id"], product);
    }

    customDialog(
      context,
      ProductDataSetting(
        widgetId: product['id'],
        bloc: widget.bloc,
        onUpdateProduct: updateProduct,
        currentProduct: product,
      ),
    );
  }

// text style
  void editProductDescriptionStyle() {
    if (productDescriptionStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          productDescriptionStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          productDescriptionStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: productDescriptionStyle,
        ),
      );
    }
  }

  // text style
  void editProductNameStyle() {
    if (productNameStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          productNameStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          productNameStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: productNameStyle,
        ),
      );
    }
  }

  // text style
  void editProductPriceStyle() {
    if (productPriceStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          productPriceStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          productPriceStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: productPriceStyle,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "product": product['id'] ?? '',
      "upperCaseProductName": isUpperCase.toString(),
      "prefixProductPrice": controllerPrefixProductPrice.text,
      "maxLineOfProductName": controllerMaxLineProductName.text,
      "subfixProductPrice": controllerSubfixProductPrice.text,
      "fractionDigitsForPrice": controllerFractionDigitsForPrice.text,
      "divider": divider,
      "productNameTextStyle": productNameStyle,
      "productPriceTextStyle": productPriceStyle,
      "productDescriptionTextStyle": productDescriptionStyle,
      "productNameFontSizeFromFieldKey":
          controllerProductNameFontSizeFieldKey.text,
      "productPriceFontSizeFromFieldKey":
          controllerProductPriceFontSizeFieldKey.text,
      "productDesFontSizeFromFieldKey":
          controllerProductDescFontSizeFieldKey.text,
    };

    if (product.isNotEmpty) {
      dataSetting.addDataToSetting(product['id'], product);
    } 

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Product Row 2'),
          // Tab bar includes two tab: Products and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD PRODUCTS
                  buildTabData(context),
                  // TAB PRODUCT SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              Row(
                children: [
                  customText('Add new product', mediumSize, textNormal),

                  // BUTTON ADD CATEGORY --------------------------------
                  product.isEmpty
                      ? IconButton(
                          onPressed: addProduct,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              product.isNotEmpty ? buildProduct() : Text(''),
            ],
          ),
        ),
      ],
    );
  }

  // TAB PRODUCT SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        ReusableSetting.buildInputSetting(
            context,
            "Product name font size field key",
            controllerProductNameFontSizeFieldKey),
        SizedBox(height: 20),

        ReusableSetting.buildInputSetting(
            context,
            "Product price font size field key",
            controllerProductPriceFontSizeFieldKey),
        SizedBox(height: 20),

        ReusableSetting.buildInputSetting(
            context,
            "Product description font size field key",
            controllerProductDescFontSizeFieldKey),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustNumber(
                  context,
                  'Max lines product name',
                  controllerMaxLineProductName,
                  increaseValue,
                  decreaseValue),
            )
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustNumber(
                  context,
                  'Fraction digits for price',
                  controllerFractionDigitsForPrice,
                  increaseValue,
                  decreaseValue),
            )
          ],
        ),
        SizedBox(height: 20),

        // ADJUST USE UPPERCASE NAME PRODUCT -----------------------------------
        ReusableSetting.buildToggleSwitch(context, 'Uppercase product name',
            isUpperCase, changeValueUppercase),
        SizedBox(height: 20),

        // PREFIX OPTIONS -----------------------------------
        ReusableSetting.buildInputSetting(
            context, "Prefix product price", controllerPrefixProductPrice),
        SizedBox(height: 20),

        ReusableSetting.buildInputSetting(
            context, "Subfix product price", controllerSubfixProductPrice),
        SizedBox(height: 20),

        Row(
          children: [
            customText('Product name style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editProductNameStyle,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            customText('Product price style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editProductPriceStyle,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            customText('Product description style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editProductDescriptionStyle,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            customText('Divider style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editDivider,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
      ],
    );
  }

  // This method is used to display a category that added.
  Widget buildProduct() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              product.isEmpty || product['name'].isEmpty
                  ? Text('New product')
                  : Text(product['name']),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT CATEGORY --------------------------------
                  IconButton(
                    onPressed: editProduct,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE CATEGORY --------------------------------
                  IconButton(
                    onPressed: removeProduct,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
