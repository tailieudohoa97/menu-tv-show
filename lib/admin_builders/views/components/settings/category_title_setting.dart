import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/category_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/image_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

// CategoryTitle

class CategoryTitleSetting extends StatefulWidget {
  const CategoryTitleSetting(
      {super.key,
      required this.widgetId,
      required this.bloc,
      this.onUpdateCategory,
      this.currentCategory});

  final String widgetId;
  final BuilderBloc bloc;
  final Function? onUpdateCategory;
  final Map<String, dynamic>? currentCategory;

  @override
  State<CategoryTitleSetting> createState() => _CategoryTitleSettingState();
}

class _CategoryTitleSettingState extends State<CategoryTitleSetting>
    with SingleTickerProviderStateMixin {
  bool isFullWidth = false, isBackgroundCategoryTitle = false;

  TextEditingController controllerColorCatTitle = TextEditingController();
  TextEditingController controllerColorBgCatTitle = TextEditingController();

  Map<String, dynamic> currentSetting = {};
  dynamic category = {}, imageBg = {};
  DataSetting dataSetting = DataSetting();
  int currentIndexTab = 0;
  late TabController _tabController;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updatedCategory = {}, updatedImageBg = {};

    if (currentSetting["imageId"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['imageId']) ?? {};
      updatedImageBg = data;
    }

    if (currentSetting["categoryId"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['categoryId']) ?? {};
      updatedCategory = data;
    }

    setState(() {
      category = updatedCategory;
      imageBg = updatedImageBg;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    initDataSetting(currentSetting);
    _tabController = TabController(length: 2, vsync: this);
    super.initState();

    controllerColorCatTitle.text = currentSetting['color'];
    controllerColorBgCatTitle.text = currentSetting['colorBackground'];
    isFullWidth = bool.parse(currentSetting["useFullWidth"]);
    isBackgroundCategoryTitle = bool.parse(currentSetting["useBackground"]);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  // This function is used to change use full width category title value.
  void changeValueFullWidth(bool value) {
    setState(() {
      isFullWidth = value;
    });
  }

  // This function is used to change use background category title value.
  void changeValueBackgroundCategory(bool value) {
    setState(() {
      isBackgroundCategoryTitle = value;
    });
  }

  // This function is used to add numbers products.
  void addCategory() {
    setState(() {
      final cat = BuilderRepository().generateJsonCategoryData();
      category = cat;
    });
  }

  // This function is used to edit a category.
  void editCategory() {
    void updateCategory(dynamic data) {
      setState(() {
        category = data;
      });
      dataSetting.updateDataOnSetting(category["id"], category);
    }

    customDialog(
      context,
      CategoryDataSetting(
        widgetId: category['id'],
        bloc: widget.bloc,
        onUpdateCategory: updateCategory,
        currentCategory: category,
      ),
    );
  }

  // This function is used to remove category added.
  void removeCategory() {
    // update dataSetting.
    dataSetting.removeDataOnSetting(category["id"]);
    setState(() {
      category = {};
    });
  }

  // This function is used to add numbers products.
  void addImageBg() {
    setState(() {
      final i = BuilderRepository().generateJsonImageData();
      imageBg = i;
    });
  }

  // This function is used to edit a category.
  void editImageBg() {
    void updateImage(dynamic data) {
      setState(() {
        imageBg = data;
      });
      dataSetting.updateDataOnSetting(imageBg["id"], imageBg);
    }

    customDialog(
      context,
      ImageDataSetting(
        widgetId: imageBg['id'],
        bloc: widget.bloc,
        onUpdateImage: updateImage,
        currentImage: imageBg,
      ),
    );
  }

  // This function is used to remove category added.
  void removeImageBg() {
    // update dataSetting.
    dataSetting.removeDataOnSetting(imageBg["id"]);
    setState(() {
      imageBg = {};
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "categoryId": category['id'] ?? "",
      "useBackground": isBackgroundCategoryTitle.toString(),
      "useFullWidth": isFullWidth.toString(),
      "imageId": imageBg['id'] ?? "",
      "color": controllerColorCatTitle.text,
      "colorBackground": controllerColorBgCatTitle.text,
    };

    if (category.isNotEmpty) {
      dataSetting.addDataToSetting(category['id'], category);
    }
    if (imageBg.isNotEmpty) {
      dataSetting.addDataToSetting(imageBg['id'], imageBg);
    }

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 440,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Category Title'),
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD PRODUCTS
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              // ADD NEW CATEGORY -----------------------------------
              Row(
                children: [
                  customText('Add a category', mediumSize, textNormal),
                  const SizedBox(width: 20),

                  // BUTTON ADD PRODUCTS --------------------------------
                  category.isEmpty
                      ? IconButton(
                          onPressed: addCategory,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              category.isNotEmpty ? buildCategory() : Container(),
              category.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 0),

              // ADD NEW CATEGORY -----------------------------------
              Row(
                children: [
                  customText('Add a background image', mediumSize, textNormal),
                  const SizedBox(width: 20),

                  // BUTTON ADD PRODUCTS --------------------------------
                  imageBg.isEmpty
                      ? IconButton(
                          onPressed: addImageBg,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              imageBg.isNotEmpty ? buildImageBg() : Container(),
              imageBg.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 10),
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        // ADJUST COLOR CATEGORY TITLE -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color category title', controllerColorCatTitle),
        SizedBox(height: 20),

        // ADJUST COLOR BACKGROUND CATEGORY TITLE -----------------------------------
        ReusableSetting.buildInputSetting(context,
            'Color background category title', controllerColorBgCatTitle),
        SizedBox(height: 20),

        // ADJUST USE BACKGROUND CATEGORY TITLE -----------------------------------
        ReusableSetting.buildToggleSwitch(context, 'Use background',
            isBackgroundCategoryTitle, changeValueBackgroundCategory),
        SizedBox(height: 20),

        // ADJUST USE FULL WIDTH CATEGORY TITLE -----------------------------------
        ReusableSetting.buildToggleSwitch(
            context, 'Use full width', isFullWidth, changeValueFullWidth),
      ],
    );
  }

  // This method is used to display category that added.
  Widget buildCategory() {
    return Container(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              category.isEmpty || category['name'].isEmpty
                  ? Text('New category')
                  : Text(category['name']),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT CATEGORY --------------------------------
                  IconButton(
                    onPressed: editCategory,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE CATEGORY --------------------------------
                  IconButton(
                    onPressed: removeCategory,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }

  // This method is used to display a category that added.
  Widget buildImageBg() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              Text('New background image'),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT IMAGE --------------------------------
                  IconButton(
                    onPressed: editImageBg,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE IMAGE --------------------------------
                  IconButton(
                    onPressed: removeImageBg,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
