import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/data/json_data_builder.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/product_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

class ListProductSetting extends StatefulWidget {
  const ListProductSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<ListProductSetting> createState() => _ListProductSettingState();
}

class _ListProductSettingState extends State<ListProductSetting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerThumbnailPath = TextEditingController();
  TextEditingController controllerMaxLineProductName = TextEditingController();
  TextEditingController controllerMaxLineProductDescription =
      TextEditingController();
  TextEditingController controllerNumberProducts = TextEditingController();

  TextEditingController controllerColorPrice = TextEditingController();
  TextEditingController controllerColorProductName = TextEditingController();
  TextEditingController controllerColorDescription = TextEditingController();

  Map<String, dynamic> currentSetting = {};
  String? selectedMainAxis, selectedCrossAxis;

  bool isUpperCase = false, isMediumStyle = false, isThumbnail = false;
  List<dynamic> listProducts = [];
  DataSetting dataSetting = DataSetting();
  int currentIndexTab = 0;
  late TabController _tabController;

  void initProductList(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    List<dynamic> updatedListProducts = [];

    if (currentSetting["dataList"].isNotEmpty) {
      currentSetting["dataList"].forEach((e) {
        // get list product from data setting
        var data = dataSetting.getDataFromSetting(e);
        updatedListProducts.add(data);
      });
    } else {
      updatedListProducts = [];
    }

    setState(() {
      listProducts = updatedListProducts;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    initProductList(currentSetting);
    _tabController = TabController(length: 2, vsync: this);

    controllerThumbnailPath.text = currentSetting['pathThumbnailProduct'];
    controllerMaxLineProductName.text = currentSetting['maxLineProductName'];
    controllerMaxLineProductDescription.text =
        currentSetting['maxLineProductDescription'];
    selectedMainAxis = currentSetting["mainAxisAlignment"];
    selectedCrossAxis = currentSetting["crossAxisAlignment"];
    controllerColorPrice.text = currentSetting["colorPrice"];
    controllerColorProductName.text = currentSetting["colorProductName"];
    controllerColorDescription.text = currentSetting["colorDescription"];
    isUpperCase = bool.parse(currentSetting["toUpperCaseNameProductName"]);
    isMediumStyle = bool.parse(currentSetting["useMediumStyle"]);
    isThumbnail = bool.parse(currentSetting["useThumbnailProduct"]);
    controllerNumberProducts.text = '1';
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  // Get list main axis options.
  List<String> mainAxisOptions = JsonDataBuilder.mainAxisOptions;
  // This function is used to change main axis value.
  void changeValueMainAxis(String? value) {
    setState(() {
      selectedMainAxis = value!;
    });
  }

  // Get list cross axis options.
  List<String> crossAxisOptions = JsonDataBuilder.crossAxisOptions;
  // This function is used to change main cross value.
  void changeValueCrossAxis(String? value) {
    setState(() {
      selectedCrossAxis = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to change use uppercase value.
  void changeValueUppercase(bool value) {
    setState(() {
      isUpperCase = value;
    });
  }

  // This function is used to change use medium style value.
  void changeValueMediumStyle(bool value) {
    setState(() {
      isMediumStyle = value;
    });
  }

  // This function is used to change use thumbnail value.
  void changeValueThumbnail(bool value) {
    setState(() {
      isThumbnail = value;
    });
  }

  // This function is used to add numbers products.
  void addProducts() {
    setState(() {
      for (var i = 0; i < int.parse(controllerNumberProducts.text); i++) {
        final product = BuilderRepository().generateJsonProductData();
        listProducts.add(product);
      }
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeProduct(int index) {
    // update dataSetting.
    dataSetting.removeDataOnSetting(listProducts[index]['id']);
    setState(() {
      listProducts.removeAt(index);
    });
  }

  // This function is used to edit a product.
  void editProduct(int index) {
    void updateProduct(dynamic data) {
      setState(() {
        listProducts[index] = data;
      });
      dataSetting.updateDataOnSetting(
          listProducts[index]['id'], listProducts[index]);
    }

    customDialog(
      context,
      ProductDataSetting(
        widgetId: listProducts[index]['id'],
        bloc: widget.bloc,
        onUpdateProduct: updateProduct,
        currentProduct: listProducts[index],
      ),
    );
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    dynamic dataList = listProducts.map((e) => e['id']).toList();

    for (var p in listProducts) {
      dataSetting.addDataToSetting(p['id'], p);
    }

    Map<String, dynamic> data = {
      "dataList": dataList,
      "mainAxisAlignment": selectedMainAxis,
      "crossAxisAlignment": selectedCrossAxis,
      "colorPrice": controllerColorPrice.text,
      "colorProductName": controllerColorProductName.text,
      "colorDescription": controllerColorDescription.text,
      "toUpperCaseNameProductName": isUpperCase.toString(),
      "useMediumStyle": isMediumStyle.toString(),
      "useThumbnailProduct": isThumbnail.toString(),
      "pathThumbnailProduct": controllerThumbnailPath.text,
      "maxLineProductName": controllerMaxLineProductName.text,
      "maxLineProductDescription": controllerMaxLineProductDescription.text
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 520,
      height: 520,
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting List Product'),
          // Tab bar includes two tab: Products and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD PRODUCTS
                  buildTabData(context),
                  // TAB PRODUCT SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              Row(
                children: [
                  customText('List product: ', mediumSize, textNormal),
                  Spacer(),
                  customText('Number products: ${listProducts.length}',
                      smallSize, textNormal),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  // ADJUST NUMBER PRODUCT NEED ADD -----------------------------------
                  SizedBox(
                    width: 200,
                    child: Expanded(
                      child: ReusableSetting.buildAdjustNumber(
                          context,
                          'Number products',
                          controllerNumberProducts,
                          increaseValue,
                          decreaseValue),
                    ),
                  ),
                  const SizedBox(width: 20),

                  // BUTTON ADD PRODUCTS --------------------------------
                  IconButton(
                    onPressed: addProducts,
                    icon: Icon(Icons.add),
                    color: Colors.blue,
                  )
                ],
              ),

              // DISPLAY LIST PRODUCTS ADDED --------------------------------
              buildListProduct()
            ],
          ),
        ),
      ],
    );
  }

  // TAB PRODUCT SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        // ADJUST MAIN ALIGNMENT -----------------------------------
        Align(
          alignment: Alignment.centerLeft,
          child: customText('Main alignment ', mediumSize, textBold),
        ),
        ReusableSetting.buildDropdownMenu(
            context, selectedMainAxis!, changeValueMainAxis, mainAxisOptions),
        SizedBox(height: 20),

        // ADJUST CROSS ALIGNMENT -----------------------------------
        Align(
          alignment: Alignment.centerLeft,
          child: customText('Cross alignment ', mediumSize, textBold),
        ),
        ReusableSetting.buildDropdownMenu(context, selectedCrossAxis!,
            changeValueCrossAxis, crossAxisOptions),
        SizedBox(height: 20),

        // ADJUST COLOR PRICE -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color price', controllerColorPrice),
        SizedBox(height: 20),

        // ADJUST COLOR NAME PRODUCT -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color product name', controllerColorProductName),
        SizedBox(height: 20),

        // ADJUST COLOR DESCRIPTION -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color description', controllerColorDescription),
        SizedBox(height: 20),

        // ADJUST USE UPPERCASE NAME PRODUCT -----------------------------------
        ReusableSetting.buildToggleSwitch(context, 'Uppercase product name',
            isUpperCase, changeValueUppercase),
        SizedBox(height: 20),

        // ADJUST USE MEDIUM STYLE -----------------------------------
        ReusableSetting.buildToggleSwitch(
            context, 'Use Medium style', isMediumStyle, changeValueMediumStyle),
        SizedBox(height: 20),

        // ADJUST USE THUMBNAIL -----------------------------------
        ReusableSetting.buildToggleSwitch(context, 'Use Thumbnail product',
            isThumbnail, changeValueThumbnail),
        SizedBox(height: 20),

        // ADJUST THUMBNAIL URL -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Thumbnail url', controllerThumbnailPath),
        SizedBox(height: 20),

        // ADJUST MAX LINES OF PRODUCT NAME -----------------------------------
        Row(
          children: [
            SizedBox(
                width: 200,
                child: ReusableSetting.buildAdjustNumber(
                    context,
                    'Max lines product name',
                    controllerMaxLineProductName,
                    increaseValue,
                    decreaseValue)),
          ],
        ),
        SizedBox(height: 20),

        // ADJUST MAX LINES OF DESCRIPTION -----------------------------------
        Row(
          children: [
            SizedBox(
                width: 200,
                child: ReusableSetting.buildAdjustNumber(
                    context,
                    'Max lines description',
                    controllerMaxLineProductDescription,
                    increaseValue,
                    decreaseValue)),
          ],
        ),
      ],
    );
  }

  // This method is used to display list products that added.
  Widget buildListProduct() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: SingleChildScrollView(
        child: ListView.separated(
          shrinkWrap: true,
          itemCount: listProducts.length,
          separatorBuilder: (context, index) => SizedBox(height: 10),
          itemBuilder: (context, index) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.grey[300],
              ),
              child: Row(
                children: [
                  listProducts[index]['name'].isEmpty
                      ? Text('Product ${index + 1}')
                      : Text(listProducts[index]['name']),
                  Spacer(),
                  Row(
                    children: [
                      // BUTTON EDIT PRODUCT --------------------------------
                      IconButton(
                        onPressed: () => editProduct(index),
                        icon: Icon(Icons.edit, size: 18),
                        color: Colors.teal,
                      ),

                      // BUTTON REMOVE PRODUCT --------------------------------
                      IconButton(
                        onPressed: () => removeProduct(index),
                        icon: Icon(Icons.delete, size: 18),
                        color: Colors.red,
                      ),
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
