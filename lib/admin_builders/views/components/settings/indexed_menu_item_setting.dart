import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/product_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';
import 'package:tv_menu/admin_builders/views/components/settings/text_setting.dart';

class IndexedMenuItemSetting extends StatefulWidget {
  const IndexedMenuItemSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<IndexedMenuItemSetting> createState() => _IndexedMenuItemSettingState();
}

class _IndexedMenuItemSettingState extends State<IndexedMenuItemSetting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerChipRadius = TextEditingController();
  TextEditingController controllerElementGap = TextEditingController();
  TextEditingController controllerIndex = TextEditingController();

  TextEditingController controllerColorChipFg = TextEditingController();
  TextEditingController controllerColorChipBg = TextEditingController();

  TextEditingController controllerPriceFontSizeFieldKey =
      TextEditingController();
  TextEditingController controllerFontSizeFieldKey = TextEditingController();
  TextEditingController controllerIndexChipRadiusFieldKey =
      TextEditingController();

  dynamic priceTextStyle = {}, textStyle = {}, product = {};
  DataSetting dataSetting = DataSetting();
  int currentIndexTab = 0;
  late TabController _tabController;
  Map<String, dynamic> currentSetting = {};
  String? selectedUnitChipRadius, selectedUnitElementGap;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updateProduct = {};

    if (currentSetting["product"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['product']) ?? {};
      updateProduct = data;
    } else {
      updateProduct = {};
    }

    setState(() {
      product = updateProduct;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    initDataSetting(currentSetting);
    _tabController = TabController(length: 2, vsync: this);

    priceTextStyle = currentSetting['priceTextStyle'] ?? {};
    textStyle = currentSetting['textStyle'] ?? {};
    controllerColorChipFg.text = currentSetting["indexChipFgColor"];
    controllerColorChipBg.text = currentSetting["indexChipBgColor"];
    controllerIndex.text = currentSetting["index"];
    controllerFontSizeFieldKey.text = currentSetting["fontSizeFromFieldKey"];
    controllerPriceFontSizeFieldKey.text =
        currentSetting["priceFontSizeFromFieldKey"];
    controllerIndexChipRadiusFieldKey.text =
        currentSetting["indexChipRadiusFromFieldKey"];
    controllerChipRadius.text =
        ConvertUnit.unitToString(currentSetting["indexChipRadius"]);
    controllerElementGap.text =
        ConvertUnit.unitToString(currentSetting["elementGap"]);
    selectedUnitChipRadius =
        ConvertUnit.getUnit(currentSetting["indexChipRadius"]);
    selectedUnitElementGap = ConvertUnit.getUnit(currentSetting["elementGap"]);
  }

  void changeValueUnitChipRadius(String? value) {
    setState(() {
      selectedUnitChipRadius = value!;
    });
  }

  void changeValueUnitElementGap(String? value) {
    setState(() {
      selectedUnitElementGap = value!;
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to add numbers categories.
  void addProduct() {
    setState(() {
      final p = BuilderRepository().generateJsonProductData();
      product = p;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeProduct() {
    dataSetting.removeDataOnSetting(product['id']);
    setState(() {
      product = {};
    });
  }

  // This function is used to edit a product.
  void editProduct() {
    void updateProduct(dynamic data) {
      setState(() {
        product = data;
      });
      dataSetting.updateDataOnSetting(product['id'], product);
    }

    customDialog(
      context,
      ProductDataSetting(
        widgetId: product['id'],
        bloc: widget.bloc,
        onUpdateProduct: updateProduct,
        currentProduct: product,
      ),
    );
  }

  void editTextStyle() {
    if (textStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']["style"] = data;
        setState(() {
          textStyle = text["widgetSetting"]["style"];;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          textStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: textStyle,
        ),
      );
    }
  }

  void editPriceTextPrice() {
    if (priceTextStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting'] = data;
        setState(() {
          priceTextStyle = text;
        });
      }

      customDialog(
        context,
        TextSetting(
          widgetId: text['id'],
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text,
        ),
      );
    } else {
      // price text style is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          priceTextStyle['widgetSetting'] = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          widgetId: priceTextStyle['id'],
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: priceTextStyle,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "index": controllerIndex.text,
      "priceFontSizeFromFieldKey": controllerPriceFontSizeFieldKey.text,
      "fontSizeFromFieldKey": controllerFontSizeFieldKey.text,
      "indexChipRadiusFromFieldKey": controllerIndexChipRadiusFieldKey.text,
      "product": product['id'] ?? "", // product json
      "elementGap": ConvertUnit.stringToUnit(
          controllerElementGap.text, selectedUnitElementGap!),
      "indexChipRadius": ConvertUnit.stringToUnit(
          controllerChipRadius.text, selectedUnitChipRadius!),
      "indexChipBgColor": controllerColorChipBg.text,
      "indexChipFgColor": controllerColorChipFg.text,
      "priceTextStyle": priceTextStyle, // text style json
      "textStyle": textStyle // text st
    };

    if (product.isNotEmpty) {
      dataSetting.addDataToSetting(product['id'], product);
    }

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Indexed Menu Item'),
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Product", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD CATEGORIES
                  buildTabProduct(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabProduct(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              Row(
                children: [
                  customText('Add new product', mediumSize, textNormal),

                  // BUTTON ADD PRODUCT --------------------------------
                  product.isEmpty
                      ? IconButton(
                          onPressed: addProduct,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              product.isNotEmpty ? buildProduct() : Text(''),
              product.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 0),
            ],
          ),
        ),
      ],
    );
  }

  // This method is used to display a category that added.
  Widget buildProduct() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              product.isEmpty || product['name'].isEmpty
                  ? Text('New product')
                  : Text(product['name']),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT IMAGE --------------------------------
                  IconButton(
                    onPressed: editProduct,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE IMAGE --------------------------------
                  IconButton(
                    onPressed: removeProduct,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        ReusableSetting.buildInputSetting(
            context, 'Font size field key', controllerFontSizeFieldKey),
        SizedBox(height: 20),

        ReusableSetting.buildInputSetting(context, 'Price font size field key',
            controllerPriceFontSizeFieldKey),
        SizedBox(height: 20),

        ReusableSetting.buildInputSetting(context,
            'Index chip radius field key', controllerIndexChipRadiusFieldKey),
        SizedBox(height: 20),

        // ADJUST COLOR PRICE -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color Chip Fg color', controllerColorChipFg),
        SizedBox(height: 20),

        // ADJUST COLOR NAME PRODUCT -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color Chip Bg color', controllerColorChipBg),
        SizedBox(height: 20),

        // ADJUST MAX LINES OF PRODUCT NAME -----------------------------------
        Row(
          children: [
            SizedBox(
                width: 200,
                child: ReusableSetting.buildAdjustNumber(context, 'Index',
                    controllerIndex, increaseValue, decreaseValue)),
          ],
        ),
        SizedBox(height: 20),

        // ADJUST MAX LINES OF PRODUCT NAME -----------------------------------
        Row(
          children: [
            SizedBox(
                width: 200,
                child: ReusableSetting.buildAdjustNumber(context, 'Chip radius',
                    controllerChipRadius, increaseValue, decreaseValue)),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitChipRadius!,
                changeValueUnitChipRadius, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        // ADJUST MAX LINES OF DESCRIPTION -----------------------------------
        Row(
          children: [
            SizedBox(
                width: 200,
                child: ReusableSetting.buildAdjustDouble(context, 'Element gap',
                    controllerElementGap, increaseValue, decreaseValue)),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitElementGap!,
                changeValueUnitElementGap, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            customText('Text style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editTextStyle,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
        SizedBox(height: 20),

        Row(
          children: [
            customText('Price text style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editPriceTextPrice,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
      ],
    );
  }
}
