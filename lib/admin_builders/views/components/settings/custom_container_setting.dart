import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/data/json_data_builder.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/margin_padding_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

class CustomContainerSetting extends StatefulWidget {
  const CustomContainerSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<CustomContainerSetting> createState() => _CustomContainerSettingState();
}

enum Fit { cover, contain, fill, none }

final Map<String, Fit> fitValues = {
  "cover": Fit.cover,
  "contain": Fit.contain,
  "fill": Fit.fill,
  "none": Fit.none
};

class _CustomContainerSettingState extends State<CustomContainerSetting> {
  TextEditingController controllerWidth = TextEditingController();
  TextEditingController controllerHeight = TextEditingController();
  TextEditingController controllerImage = TextEditingController();
  TextEditingController controllerOpacity = TextEditingController();

  TextEditingController controllerColorBg = TextEditingController();

  String? selectedAlign;
  String? selectedBlendMode;
  Fit _fitSelected = Fit.cover;
  dynamic padding = {}, margin = {};
  Map<String, dynamic> currentSetting = {};
  String? selectedUnitWidth, selectedUnitHeight;

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();

    padding = currentSetting['padding'] ?? {};
    margin = currentSetting['margin'] ?? {};
    controllerOpacity.text = currentSetting['opacity'];
    controllerHeight.text = ConvertUnit.unitToString(currentSetting["height"]);
    controllerWidth.text = ConvertUnit.unitToString(currentSetting["width"]);
    controllerImage.text = currentSetting['image'];
    _fitSelected = convertStringToFix(currentSetting['fit']);
    controllerColorBg.text = currentSetting['color'];
    selectedAlign = currentSetting['alignment'];
    selectedBlendMode = currentSetting['blendMode'];

    selectedUnitWidth = ConvertUnit.getUnit(currentSetting["width"]);
    selectedUnitHeight = ConvertUnit.getUnit(currentSetting["height"]);
  }

  void changeValueUnitWidth(String? value) {
    setState(() {
      selectedUnitWidth = value!;
    });
  }

  void changeValueUnitHeight(String? value) {
    setState(() {
      selectedUnitHeight = value!;
    });
  }

  // Convert radio value to string value
  String convertFitToString() {
    // output is a string
    String selectedKey = '';
    // loop and find key has value equal _typeSelected current
    for (var entry in fitValues.entries) {
      if (entry.value == _fitSelected) {
        selectedKey = entry.key;
        break;
      }
    }
    return selectedKey;
  }

  Fit convertStringToFix(String fitData) {
    // output is a string
    Fit selectedValue = Fit.cover;
    // loop and find key has value equal _typeSelected current
    for (var entry in fitValues.entries) {
      if (entry.key == fitData) {
        selectedValue = entry.value;
        break;
      }
    }
    return selectedValue;
  }

  // Get list alignment options.
  List<String> alignOptions = JsonDataBuilder.alignOptions;
  // Get list blend mode options.
  List<String> blendModeOptions = JsonDataBuilder.blendModeOptions;

  // This function is used to change algin option value.
  void changeValueAlign(String? value) {
    setState(() {
      selectedAlign = value!;
    });
  }

  // This function is used to change blend mode option value.
  void changeValueBlendMode(String? value) {
    setState(() {
      selectedBlendMode = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to change fit option value.
  void changeFitValue(value) {
    setState(() {
      _fitSelected = value!;
    });
  }

  // Border bottom side
  void editPaddingStyle() {
    // if borderSide current is empty
    if (padding.isEmpty) {
      // new setting border
      var p = BuilderRepository().generateJsonPaddingMargin();

      // update values after edit
      void updatePadding(dynamic data) {
        p = data;
        setState(() {
          padding = p;
        });
      }

      // pass new setting to MarginPaddingSetting
      customDialog(
        context,
        MarginPaddingSetting(
          type: 'Padding',
          bloc: widget.bloc,
          onUpdateData: updatePadding,
          currentData: p,
        ),
      );
    } else {
      // border side is exists
      void updatePadding(dynamic data) {
        setState(() {
          padding = data;
        });
      }

      customDialog(
        context,
        MarginPaddingSetting(
          type: 'Padding',
          bloc: widget.bloc,
          onUpdateData: updatePadding,
          currentData: padding,
        ),
      );
    }
  }

  // Border bottom side
  void editMarginStyle() {
    // if borderSide current is empty
    if (margin.isEmpty) {
      // new setting border
      var p = BuilderRepository().generateJsonPaddingMargin();

      // update values after edit
      void updateMargin(dynamic data) {
        p = data;
        setState(() {
          margin = p;
        });
      }

      // pass new setting to MarginPaddingSetting
      customDialog(
        context,
        MarginPaddingSetting(
          type: 'Margin',
          bloc: widget.bloc,
          onUpdateData: updateMargin,
          currentData: p,
        ),
      );
    } else {
      // border side is exists
      void updateMargin(dynamic data) {
        setState(() {
          margin = data;
        });
      }

      customDialog(
        context,
        MarginPaddingSetting(
          type: 'Margin',
          bloc: widget.bloc,
          onUpdateData: updateMargin,
          currentData: margin,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "height":
          ConvertUnit.stringToUnit(controllerHeight.text, selectedUnitHeight!),
      "width":
          ConvertUnit.stringToUnit(controllerWidth.text, selectedUnitWidth!),
      "opacity": controllerOpacity.text,
      "alignment": selectedAlign,
      "padding": padding,
      "margin": margin,
      "image": controllerImage.text,
      "fit": convertFitToString(),
      "color": controllerColorBg.text,
      "blendMode": selectedBlendMode,
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Custom Container'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADJUST WIDTH AND HEIGHT -----------------------------------
                    Row(children: [
                      SizedBox(
                        width: 200,
                        child: ReusableSetting.buildAdjustDouble(
                            context,
                            'Width',
                            controllerWidth,
                            increaseValue,
                            decreaseValue),
                      ),
                      Spacer(),
                      ReusableSetting.buildDropdownMenu(
                          context,
                          selectedUnitWidth!,
                          changeValueUnitWidth,
                          ['none', 'w'])
                    ]),
                    SizedBox(height: 20),

                    Row(children: [
                      SizedBox(
                        width: 200,
                        child: ReusableSetting.buildAdjustDouble(
                            context,
                            'Height',
                            controllerHeight,
                            increaseValue,
                            decreaseValue),
                      ),
                      Spacer(),
                      ReusableSetting.buildDropdownMenu(
                          context,
                          selectedUnitHeight!,
                          changeValueUnitHeight,
                          ['none', 'h'])
                    ]),
                    SizedBox(height: 20),

                    // ALIGNMENT -----------------------------------
                    Align(
                      alignment: Alignment.topLeft,
                      child: customText('Alignment', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(context, selectedAlign!,
                        changeValueAlign, alignOptions),
                    SizedBox(height: 20),

                    // IMAGE URL -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Image url', controllerImage),
                    SizedBox(height: 20),

                    // FIT IMAGE OPTIONS -----------------------------------
                    Align(
                      alignment: Alignment.topLeft,
                      child: customText('Fit image', mediumSize, textBold),
                    ),
                    buildFitOptions(),
                    SizedBox(height: 20),

                    // ADJUST BACKGROUND COLOR -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Background color', controllerColorBg),
                    SizedBox(height: 20),

                    // Opacity -----------------------------------
                    Row(children: [
                      SizedBox(
                        width: 200,
                        child: ReusableSetting.buildAdjustDouble(
                            context,
                            'Opacity',
                            controllerOpacity,
                            increaseValue,
                            decreaseValue,
                            max: 1.0),
                      ),
                    ]),
                    SizedBox(height: 20),

                    Align(
                      alignment: Alignment.topLeft,
                      child: customText('Blend Mode', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(
                        context,
                        selectedBlendMode!,
                        changeValueBlendMode,
                        blendModeOptions),
                    SizedBox(height: 20),

                    // ADJUST PADDING -----------------------------------
                    Row(
                      children: [
                        customText('Padding style ', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editPaddingStyle,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                    SizedBox(height: 20),

                    // ADJUST MARGIN
                    Row(
                      children: [
                        customText('Margin style ', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editMarginStyle,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // This method is used to build a box that allows to choose radio options.
  Row buildFitOptions() {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'Cover',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.cover,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'Contain',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.contain,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'Fill',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.fill,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'None',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.none,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
      ],
    );
  }
}
