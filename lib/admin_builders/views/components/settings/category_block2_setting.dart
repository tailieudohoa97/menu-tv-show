import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/category_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';
import 'package:tv_menu/admin_builders/views/components/settings/divider_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/text_setting.dart';

class CategoryBlock2Setting extends StatefulWidget {
  const CategoryBlock2Setting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<CategoryBlock2Setting> createState() => _CategoryBlock2SettingState();
}

class _CategoryBlock2SettingState extends State<CategoryBlock2Setting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerItemSectionPadding = TextEditingController();
  TextEditingController controllerIndentCatDesc = TextEditingController();
  TextEditingController controllerItemGap = TextEditingController();
  TextEditingController controllerCatNameFontSizeFieldKey =
      TextEditingController();
  TextEditingController controllerCatDescFontSizeFieldKey =
      TextEditingController();

  Map<String, dynamic> currentSetting = {};
  dynamic category = {},
      catNameTextStyle = {},
      catDescTextStyle = {},
      divider = {};
  bool isUpperCase = false;
  DataSetting dataSetting = DataSetting();
  int currentIndexTab = 0, categoryItems = 0;
  late TabController _tabController;
  String? selectedUnitItemGap,
      selectedUnitSectionPadding,
      selectedUnitIndentDesc;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updatedCategory = {};

    if (currentSetting["category"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['category']) ?? {};
      updatedCategory = data;
    }

    setState(() {
      category = updatedCategory;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    initDataSetting(currentSetting);
    _tabController = TabController(length: 2, vsync: this);

    categoryItems = currentSetting["categoryItems"].length;
    divider = currentSetting["divider"] ?? {};
    catDescTextStyle = currentSetting['categoryDescriptionTextStyle'] ?? {};
    catNameTextStyle = currentSetting['categoryNameTextStyle'] ?? {};
    controllerItemSectionPadding.text =
        ConvertUnit.unitToString(currentSetting["categoryItemSectionPadding"]);
    controllerIndentCatDesc.text =
        ConvertUnit.unitToString(currentSetting["indentCategoryDescription"]);
    controllerItemGap.text =
        ConvertUnit.unitToString(currentSetting["categoryItemGap"]);
    isUpperCase = bool.parse(currentSetting["upperCaseCategoryName"]);
    controllerCatNameFontSizeFieldKey.text =
        currentSetting["categoryNameFontSizeFromFieldKey"];
    controllerCatDescFontSizeFieldKey.text =
        currentSetting["categoryDesFontSizeFromFieldKey"];

    selectedUnitSectionPadding =
        ConvertUnit.getUnit(currentSetting["categoryItemSectionPadding"]);
    selectedUnitIndentDesc =
        ConvertUnit.getUnit(currentSetting["indentCategoryDescription"]);
    selectedUnitItemGap =
        ConvertUnit.getUnit(currentSetting["categoryItemGap"]);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changeValueUnitItemGap(String? value) {
    setState(() {
      selectedUnitItemGap = value!;
    });
  }

  void changeValueUnitSectionPadding(String? value) {
    setState(() {
      selectedUnitSectionPadding = value!;
    });
  }

  void changeValueUnitIndentDesc(String? value) {
    setState(() {
      selectedUnitIndentDesc = value!;
    });
  }

  // This function is used to change use uppercase value.
  void changeValueUppercase(bool value) {
    setState(() {
      isUpperCase = value;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to add numbers categories.
  void addCategory() {
    setState(() {
      final cat = BuilderRepository().generateJsonCategoryData();
      category = cat;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeCategory() {
    // update dataSetting.
    dataSetting.removeDataOnSetting(category["id"]);
    setState(() {
      category = {};
    });
  }

  // This function is used to edit a product.
  void editCategory() {
    void updateCategory(dynamic data) {
      setState(() {
        category = data;
      });
      dataSetting.updateDataOnSetting(category["id"], category);
    }

    customDialog(
      context,
      CategoryDataSetting(
        widgetId: category['id'],
        bloc: widget.bloc,
        onUpdateCategory: updateCategory,
        currentCategory: category,
      ),
    );
  }

  // text style
  void editDivider() {
    if (divider.isEmpty) {
      final d = BuilderRepository().generateJsonDivider();

      void updateDivider(dynamic data) {
        d['widgetSetting'] = data;
        setState(() {
          divider = d;
        });
      }

      customDialog(
        context,
        DividerSetting(
          bloc: widget.bloc,
          onUpdateData: updateDivider,
          currentData: d["widgetSetting"],
        ),
      );
    } else {
      // category is exists
      void updateDivider(dynamic data) {
        setState(() {
          divider["widgetSetting"] = data;
        });
      }

      customDialog(
        context,
        DividerSetting(
          bloc: widget.bloc,
          onUpdateData: updateDivider,
          currentData: divider["widgetSetting"],
        ),
      );
    }
  }

  // text style
  void editCatNameStyle() {
    if (catNameTextStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          catNameTextStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          catNameTextStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: catNameTextStyle,
        ),
      );
    }
  }

  // text style
  void editCatDescStyle() {
    if (catDescTextStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          catDescTextStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          catDescTextStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: catDescTextStyle,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "category": category['id'] ?? "",
      "categoryDescriptionTextStyle": catDescTextStyle,
      "categoryItemGap": ConvertUnit.stringToUnit(
          controllerItemGap.text, selectedUnitItemGap!),
      "categoryItemSectionPadding": ConvertUnit.stringToUnit(
          controllerItemSectionPadding.text, selectedUnitSectionPadding!),
      // "categoryItems": [],
      "categoryNameTextStyle": catNameTextStyle,
      "divider": divider,
      "indentCategoryDescription": ConvertUnit.stringToUnit(
          controllerIndentCatDesc.text, selectedUnitIndentDesc!),
      "upperCaseCategoryName": isUpperCase.toString(),
      "categoryNameFontSizeFromFieldKey":
          controllerCatNameFontSizeFieldKey.text,
      "categoryDesFontSizeFromFieldKey": controllerCatDescFontSizeFieldKey.text,
    };

    if (category.isNotEmpty) {
      dataSetting.addDataToSetting(category['id'], category);
    }

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Category Block 2'),
          // Tab bar includes two tab: Categories and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD CATEGORIES
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              // ADD LIST CATEGORY ITEMS
              Row(
                children: [
                  SizedBox(
                    width: 300,
                    child: customText('Number products current: $categoryItems',
                        mediumSize, textNormal),
                  )
                ],
              ),
              SizedBox(height: 20),

              Row(
                children: [
                  customText('Add new category', mediumSize, textNormal),

                  // BUTTON ADD CATEGORY --------------------------------
                  category.isEmpty
                      ? IconButton(
                          onPressed: addCategory,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              category.isNotEmpty ? buildCategory() : Text(''),
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        ReusableSetting.buildInputSetting(
            context,
            "Category name font size field key",
            controllerCatNameFontSizeFieldKey),
        SizedBox(height: 20),

        ReusableSetting.buildInputSetting(
            context,
            "Category description font size field key",
            controllerCatDescFontSizeFieldKey),
        SizedBox(height: 20),

        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(
                  context,
                  'Indent category description',
                  controllerIndentCatDesc,
                  increaseValue,
                  decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitIndentDesc!,
                changeValueUnitIndentDesc, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(
                  context,
                  'Item section padding',
                  controllerItemSectionPadding,
                  increaseValue,
                  decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(
                context,
                selectedUnitSectionPadding!,
                changeValueUnitSectionPadding,
                ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Item gap',
                  controllerItemGap, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitItemGap!,
                changeValueUnitItemGap, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),
        ReusableSetting.buildToggleSwitch(context, 'Uppercase category name',
            isUpperCase, changeValueUppercase),
        SizedBox(height: 20),
        Row(
          children: [
            customText('Category name style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editCatNameStyle,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            customText('Category description style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editCatDescStyle,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            customText('Divider style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editDivider,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
      ],
    );
  }

  // This method is used to display a category that added.
  Widget buildCategory() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              category.isEmpty || category['name'].isEmpty
                  ? Text('New category')
                  : Text(category['name']),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT CATEGORY --------------------------------
                  IconButton(
                    onPressed: editCategory,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE CATEGORY --------------------------------
                  IconButton(
                    onPressed: removeCategory,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
