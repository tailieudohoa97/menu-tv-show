import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/data/json_data_builder.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/product_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

class ServiceItemCustom1Setting extends StatefulWidget {
  const ServiceItemCustom1Setting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<ServiceItemCustom1Setting> createState() =>
      _ServiceItemCustom1SettingState();
}

class _ServiceItemCustom1SettingState extends State<ServiceItemCustom1Setting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerWidth = TextEditingController();
  TextEditingController controllerMaxLineProductName = TextEditingController();

  TextEditingController controllerColorPrice = TextEditingController();
  TextEditingController controllerColorProductName = TextEditingController();
  TextEditingController controllerColorDescription = TextEditingController();

  Map<String, dynamic> currentSetting = {};
  bool isUpperCase = false, isMediumStyle = false;
  dynamic product = {};
  DataSetting dataSetting = DataSetting();
  int currentIndexTab = 0;
  late TabController _tabController;
  String? selectedUnitWidth, selectedFontWeight;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updateProduct = {};

    if (currentSetting["productId"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['productId']) ?? {};
      updateProduct = data;
    } else {
      updateProduct = {};
    }

    setState(() {
      product = updateProduct;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    _tabController = TabController(length: 2, vsync: this);
    initDataSetting(currentSetting);
    super.initState();

    controllerWidth.text = ConvertUnit.unitToString(currentSetting["width"]);
    controllerMaxLineProductName.text = currentSetting['maxLineProductName'];
    controllerColorPrice.text = currentSetting["colorPrice"];
    controllerColorProductName.text = currentSetting["colorProductName"];
    controllerColorDescription.text = currentSetting["colorDescription"];
    isUpperCase = bool.parse(currentSetting["toUpperCaseNameProductName"]);
    isMediumStyle = bool.parse(currentSetting["useMediumStyle"]);
    selectedFontWeight = currentSetting['fontWeight'];

    selectedUnitWidth = ConvertUnit.getUnit(currentSetting["width"]);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changeValueUnitWidth(String? value) {
    setState(() {
      selectedUnitWidth = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to change use uppercase value.
  void changeValueUppercase(bool value) {
    setState(() {
      isUpperCase = value;
    });
  }

  // This function is used to change use medium style value.
  void changeValueMediumStyle(bool value) {
    setState(() {
      isMediumStyle = value;
    });
  }

  // Get list font weight options.
  List<String> fontWeightOptions = JsonDataBuilder.fontWeightOptions;
  // This function is used to change font weight value.
  void changeValueFontWeight(String? value) {
    setState(() {
      selectedFontWeight = value!;
    });
  }

  // This function is used to add numbers categories.
  void addProduct() {
    setState(() {
      final p = BuilderRepository().generateJsonProductData();
      product = p;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeProduct() {
    dataSetting.removeDataOnSetting(product['id']);
    setState(() {
      product = {};
    });
  }

  // This function is used to edit a product.
  void editProduct() {
    void updateProduct(dynamic data) {
      setState(() {
        product = data;
      });
      dataSetting.updateDataOnSetting(product['id'], product);
    }

    customDialog(
      context,
      ProductDataSetting(
        widgetId: product['id'],
        bloc: widget.bloc,
        onUpdateProduct: updateProduct,
        currentProduct: product,
      ),
    );
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "productId": product['id'] ?? "",
      "toUpperCaseNameProductName": isUpperCase.toString(),
      "useMediumStyle": isMediumStyle.toString(),
      "colorPrice": controllerColorPrice.text,
      "colorProductName": controllerColorProductName.text,
      "colorDescription": controllerColorDescription.text,
      "maxLineProductName": controllerMaxLineProductName.text,
      "width":
          ConvertUnit.stringToUnit(controllerWidth.text, selectedUnitWidth!),
      "fontWeight": selectedFontWeight
    };

    if (product.isNotEmpty) {
      dataSetting.addDataToSetting(product['id'], product);
    }

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 520,
      height: 520,
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Service Item Column'),
          // Tab bar includes two tab: Categories and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD CATEGORIES
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),

          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              Row(
                children: [
                  customText('Add a product', mediumSize, textNormal),

                  // BUTTON ADD IMAGE --------------------------------
                  product.isEmpty
                      ? IconButton(
                          onPressed: addProduct,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              // DISPLAY product ADDED --------------------------------
              product.isNotEmpty ? buildProduct() : Container(),
              product.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 0),
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        // ADJUST COLOR PRICE -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color price', controllerColorPrice),
        SizedBox(height: 20),

        // ADJUST COLOR NAME PRODUCT -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color product name', controllerColorProductName),
        SizedBox(height: 20),

        // ADJUST COLOR DESCRIPTION -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color description', controllerColorDescription),
        SizedBox(height: 20),
        // ADJUST USE UPPERCASE NAME PRODUCT -----------------------------------
        ReusableSetting.buildToggleSwitch(context, 'Uppercase product name',
            isUpperCase, changeValueUppercase),
        SizedBox(height: 20),

        // ADJUST USE MEDIUM STYLE -----------------------------------
        ReusableSetting.buildToggleSwitch(
            context, 'Use Medium style', isMediumStyle, changeValueMediumStyle),
        SizedBox(height: 20),

        // ADJUST FONT WEIGHT -----------------------------------
        Align(
          alignment: Alignment.centerLeft,
          child: customText('Font weight', mediumSize, textBold),
        ),
        ReusableSetting.buildDropdownMenu(context, selectedFontWeight!,
            changeValueFontWeight, fontWeightOptions),
        SizedBox(height: 20),

        // ADJUST MAX LINES OF PRODUCT NAME -----------------------------------
        Row(
          children: [
            SizedBox(
                width: 200,
                child: ReusableSetting.buildAdjustNumber(
                    context,
                    'Max lines product name',
                    controllerMaxLineProductName,
                    increaseValue,
                    decreaseValue)),
          ],
        ),
        SizedBox(height: 20),

        // ADJUST WIDTH -----------------------------------
        Row(children: [
          SizedBox(
            width: 200,
            child: ReusableSetting.buildAdjustDouble(context, 'Width',
                controllerWidth, increaseValue, decreaseValue),
          ),
          Spacer(),
          ReusableSetting.buildDropdownMenu(
              context, selectedUnitWidth!, changeValueUnitWidth, ['none', 'w'])
        ]),
      ],
    );
  }

  // This method is used to display a category that added.
  Widget buildProduct() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              product.isEmpty || product['name'].isEmpty
                  ? Text('New product')
                  : Text(product['name']),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT IMAGE --------------------------------
                  IconButton(
                    onPressed: editProduct,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE IMAGE --------------------------------
                  IconButton(
                    onPressed: removeProduct,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
