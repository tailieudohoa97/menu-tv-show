import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/border_side_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';
import 'package:tv_menu/admin_builders/views/components/settings/text_setting.dart';

class UnderlineBrandSetting extends StatefulWidget {
  const UnderlineBrandSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<UnderlineBrandSetting> createState() => _UnderlineBrandSettingState();
}

class _UnderlineBrandSettingState extends State<UnderlineBrandSetting> {
  TextEditingController controllerBorderWidth = TextEditingController();
  TextEditingController controllerBrandName = TextEditingController();
  TextEditingController controllerFontSizeFieldKey = TextEditingController();
  dynamic bottomBorderSide = {}, textStyle = {};

  Map<String, dynamic> currentSetting = {};

  @override
  void initState() {
    super.initState();

    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    controllerBrandName.text = currentSetting['brand'];
    controllerFontSizeFieldKey.text = currentSetting['fontSizeFromFieldKey'];
    bottomBorderSide = currentSetting['bottomBorderSide'] ?? {};
    textStyle = currentSetting['textStyle'] ?? {};
  }

  void editTextStyle() {
    if (textStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          textStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          textStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: textStyle,
        ),
      );
    }
  }

  // Border bottom side
  void editBorderBottomSide() {
    // if borderSide current is empty
    if (bottomBorderSide.isEmpty) {
      // new setting border
      var border = BuilderRepository().generateJsonBorderSide();

      // update values after edit
      void updateBorderSide(dynamic data) {
        border = data;
        setState(() {
          bottomBorderSide = border;
        });
      }

      // pass new setting to BorderSideSetting
      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: border,
        ),
      );
    } else {
      // border side is exists
      void updateBorderSide(dynamic data) {
        setState(() {
          bottomBorderSide = data;
        });
      }

      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: bottomBorderSide,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "brand": controllerBrandName.text,
      "fontSizeFromFieldKey": controllerFontSizeFieldKey.text,
      "bottomBorderSide": bottomBorderSide,
      "textStyle": textStyle
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 380,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Underline Brand'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADJUST UNDERLINE BRAND -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, "Brand name", controllerBrandName),
                    SizedBox(height: 20),

                    // ADJUST FONT SIZE FIELD KEY -----------------------------------
                    ReusableSetting.buildInputSetting(context,
                        'Font size field key', controllerFontSizeFieldKey),
                    SizedBox(height: 20),

                    Row(
                      children: [
                        customText('Border bottom side ', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editBorderBottomSide,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                    SizedBox(height: 20),

                    Row(
                      children: [
                        customText('Text style ', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editTextStyle,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
