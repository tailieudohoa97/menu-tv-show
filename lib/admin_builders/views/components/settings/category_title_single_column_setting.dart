import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/data/json_data_builder.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/category_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/image_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/product_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

// CategorySingleColumnWidget

class CategoryTitleSingleColumnSetting extends StatefulWidget {
  const CategoryTitleSingleColumnSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<CategoryTitleSingleColumnSetting> createState() =>
      _CategoryTitleSingleColumnSettingState();
}

class _CategoryTitleSingleColumnSettingState
    extends State<CategoryTitleSingleColumnSetting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerThumbnailPath = TextEditingController();
  TextEditingController controllerNumberProducts = TextEditingController();
  TextEditingController controllerRunSpacing = TextEditingController();
  TextEditingController controllerSpaceBetween = TextEditingController();

  TextEditingController controllerColorPrice = TextEditingController();
  TextEditingController controllerColorProductName = TextEditingController();
  TextEditingController controllerColorDescription = TextEditingController();
  TextEditingController controllerColorCategoryTitle = TextEditingController();
  TextEditingController controllerColorBgCatTitle = TextEditingController();

  bool isUpperCase = false,
      isMediumStyle = false,
      isThumbnail = false,
      isFullWidth = false,
      isBackgroundCategoryTitle = false;
  String? selectedMainAxis;

  Map<String, dynamic> currentSetting = {};
  List<dynamic> listProducts = [];
  dynamic category = {}, imageBg = {};
  DataSetting dataSetting = DataSetting();
  int currentIndexTab = 0;
  late TabController _tabController;
  String? selectedUnitRunSpacing, selectedUnitSpaceBetween;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    List<dynamic> updatedListProducts = [];
    dynamic updatedCategory = {}, updatedImageBg = {};

    if (currentSetting["imageId"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['imageId']) ?? {};
      updatedImageBg = data;
    }

    if (currentSetting["dataList"].isNotEmpty) {
      final dataList = currentSetting["dataList"];
      // get list product from data setting
      if (dataList.containsKey("product")) {
        for (var p in dataList['product']) {
          var data = dataSetting.getDataFromSetting(p);
          updatedListProducts.add(data);
        }
      }

      // get category from data setting
      if (dataList.containsKey("category") && dataList['category'].isNotEmpty) {
        var data = dataSetting.getDataFromSetting(dataList["category"]);
        updatedCategory = data;
      }
    } else {
      updatedListProducts = [];
      updatedCategory = {};
    }

    setState(() {
      listProducts = updatedListProducts;
      category = updatedCategory;
      imageBg = updatedImageBg;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    initDataSetting(currentSetting);
    _tabController = TabController(length: 2, vsync: this);

    controllerThumbnailPath.text = currentSetting['pathThumbnailProduct'];
    controllerColorCategoryTitle.text = currentSetting['colorCategoryTitle'];
    controllerColorBgCatTitle.text =
        currentSetting['colorBackgroundCategoryTitle'];
    controllerColorPrice.text = currentSetting["colorPrice"];
    controllerColorProductName.text = currentSetting["colorProductName"];
    controllerColorDescription.text = currentSetting["colorDescription"];
    isUpperCase = bool.parse(currentSetting["toUpperCaseNameProductName"]);
    isMediumStyle = bool.parse(currentSetting["useMediumStyle"]);
    isThumbnail = bool.parse(currentSetting["useThumbnailProduct"]);
    isFullWidth = bool.parse(currentSetting["useFullWidthCategoryTitle"]);
    isBackgroundCategoryTitle =
        bool.parse(currentSetting["useBackgroundCategoryTitle"]);
    controllerRunSpacing.text =
        ConvertUnit.unitToString(currentSetting["runSpacing"]);
    selectedMainAxis = currentSetting["mainAxisAlignment"];
    controllerSpaceBetween.text = ConvertUnit.unitToString(
        currentSetting["spaceBetweenCategoryProductList"]);
    controllerNumberProducts.text = '1';

    selectedUnitRunSpacing = ConvertUnit.getUnit(currentSetting["runSpacing"]);
    selectedUnitSpaceBetween =
        ConvertUnit.getUnit(currentSetting["spaceBetweenCategoryProductList"]);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changeValueUnitRunSpacing(String? value) {
    setState(() {
      selectedUnitRunSpacing = value!;
    });
  }

  void changeValueUnitSpaceBetween(String? value) {
    setState(() {
      selectedUnitSpaceBetween = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to change use uppercase value.
  void changeValueUppercase(bool value) {
    setState(() {
      isUpperCase = value;
    });
  }

  // This function is used to change use medium style value.
  void changeValueMediumStyle(bool value) {
    setState(() {
      isMediumStyle = value;
    });
  }

  // This function is used to change use thumbnail value.
  void changeValueThumbnail(bool value) {
    setState(() {
      isThumbnail = value;
    });
  }

  // This function is used to change use full width category title value.
  void changeValueFullWidth(bool value) {
    setState(() {
      isFullWidth = value;
    });
  }

  // This function is used to change use background category title value.
  void changeValueBackgroundCategory(bool value) {
    setState(() {
      isBackgroundCategoryTitle = value;
    });
  }

  // This function is used to add numbers products.
  void addProducts() {
    setState(() {
      for (var i = 0; i < int.parse(controllerNumberProducts.text); i++) {
        final product = BuilderRepository().generateJsonProductData();
        listProducts.add(product);
      }
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeProduct(int index) {
    // update dataSetting.
    dataSetting.removeDataOnSetting(listProducts[index]['id']);
    setState(() {
      listProducts.removeAt(index);
    });
  }

  // This function is used to edit a product.
  void editProduct(int index) {
    void updateProduct(dynamic data) {
      setState(() {
        listProducts[index] = data;
      });
      dataSetting.updateDataOnSetting(
          listProducts[index]['id'], listProducts[index]);
    }

    customDialog(
      context,
      ProductDataSetting(
        widgetId: listProducts[index]['id'],
        bloc: widget.bloc,
        onUpdateProduct: updateProduct,
        currentProduct: listProducts[index],
      ),
    );
  }

  // Get list main axis options.
  List<String> mainAxisOptions = JsonDataBuilder.mainAxisOptions;
  // This function is used to change main axis value.
  void changeValueMainAxis(String? value) {
    setState(() {
      selectedMainAxis = value!;
    });
  }

  // This function is used to add numbers products.
  void addCategory() {
    setState(() {
      final cat = BuilderRepository().generateJsonCategoryData();
      category = cat;
    });
  }

  // This function is used to edit a category.
  void editCategory() {
    void updateCategory(dynamic data) {
      setState(() {
        category = data;
      });
      dataSetting.updateDataOnSetting(category["id"], category);
    }

    customDialog(
      context,
      CategoryDataSetting(
        widgetId: category['id'],
        bloc: widget.bloc,
        onUpdateCategory: updateCategory,
        currentCategory: category,
      ),
    );
  }

  // This function is used to remove category added.
  void removeCategory() {
    // update dataSetting.
    dataSetting.removeDataOnSetting(category["id"]);
    setState(() {
      category = {};
    });
  }

  // This function is used to add numbers products.
  void addImageBg() {
    setState(() {
      final i = BuilderRepository().generateJsonImageData();
      imageBg = i;
    });
  }

  // This function is used to edit a category.
  void editImageBg() {
    void updateImage(dynamic data) {
      setState(() {
        imageBg = data;
      });
      dataSetting.updateDataOnSetting(imageBg["id"], imageBg);
    }

    customDialog(
      context,
      ImageDataSetting(
        widgetId: imageBg['id'],
        bloc: widget.bloc,
        onUpdateImage: updateImage,
        currentImage: imageBg,
      ),
    );
  }

  // This function is used to remove category added.
  void removeImageBg() {
    // update dataSetting.
    dataSetting.removeDataOnSetting(imageBg["id"]);
    setState(() {
      imageBg = {};
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    dynamic dataList = {
      'category': category['id'] ?? "",
      'product': listProducts.map((e) => e['id']).toList()
    };
    if (category.isNotEmpty) {
      dataSetting.addDataToSetting(category['id'], category);
    }
    if (imageBg.isNotEmpty) {
      dataSetting.addDataToSetting(imageBg['id'], imageBg);
    }
    for (var p in listProducts) {
      dataSetting.addDataToSetting(p['id'], p);
    }

    Map<String, dynamic> data = {
      "dataList": dataList,
      // "indexCategory": "",
      "useBackgroundCategoryTitle": isBackgroundCategoryTitle.toString(),
      // "pathImageBgCategoryTitle": category["image"], // pass imageId here
      "imageId": imageBg["id"] ?? "",
      "colorCategoryTitle": controllerColorCategoryTitle.text,
      "colorBackgroundCategoryTitle": controllerColorBgCatTitle.text,
      "colorPrice": controllerColorPrice.text,
      "colorProductName": controllerColorProductName.text,
      "colorDescription": controllerColorDescription.text,
      "toUpperCaseNameProductName": isUpperCase.toString(),
      "useMediumStyle": isMediumStyle.toString(),
      "useThumbnailProduct": isThumbnail.toString(),
      "pathThumbnailProduct": controllerThumbnailPath.text,
      "useFullWidthCategoryTitle": isFullWidth.toString(),
      "runSpacing": ConvertUnit.stringToUnit(
          controllerRunSpacing.text, selectedUnitRunSpacing!),
      "mainAxisAlignment": selectedMainAxis,
      "spaceBetweenCategoryProductList": ConvertUnit.stringToUnit(
          controllerSpaceBetween.text, selectedUnitSpaceBetween!)
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 520,
      height: 520,
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Category Title Column'),
          // Tab bar includes two tab: Categories and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD PRODUCTS
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              // ADD NEW CATEGORY -----------------------------------
              Row(
                children: [
                  customText('Add a category', mediumSize, textNormal),
                  const SizedBox(width: 20),

                  // BUTTON ADD PRODUCTS --------------------------------
                  category.isEmpty
                      ? IconButton(
                          onPressed: addCategory,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              category.isNotEmpty ? buildCategory() : Container(),
              category.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 0),

              // ADD NEW CATEGORY -----------------------------------
              Row(
                children: [
                  customText('Add a background image', mediumSize, textNormal),
                  const SizedBox(width: 20),

                  // BUTTON ADD PRODUCTS --------------------------------
                  imageBg.isEmpty
                      ? IconButton(
                          onPressed: addImageBg,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              imageBg.isNotEmpty ? buildImageBg() : Container(),
              imageBg.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 10),

              Row(
                children: [
                  customText('List product: ', mediumSize, textNormal),
                  Spacer(),
                  customText('Number products: ${listProducts.length}',
                      smallSize, textNormal),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  // ADJUST NUMBER PRODUCT NEED ADD -----------------------------------
                  SizedBox(
                    width: 200,
                    child: Expanded(
                      child: ReusableSetting.buildAdjustNumber(
                          context,
                          'Number products',
                          controllerNumberProducts,
                          increaseValue,
                          decreaseValue),
                    ),
                  ),
                  const SizedBox(width: 20),

                  // BUTTON ADD PRODUCTS --------------------------------
                  IconButton(
                    onPressed: addProducts,
                    icon: Icon(Icons.add),
                    color: Colors.blue,
                  )
                ],
              ),

              // DISPLAY LIST PRODUCTS ADDED --------------------------------
              buildListProduct()
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        // ADJUST COLOR CATEGORY TITLE -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color category title', controllerColorCategoryTitle),
        SizedBox(height: 20),

        // ADJUST COLOR BACKGROUND CATEGORY TITLE -----------------------------------
        ReusableSetting.buildInputSetting(context,
            'Color background category title', controllerColorBgCatTitle),
        SizedBox(height: 20),

        // ADJUST COLOR PRICE -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color price', controllerColorPrice),
        SizedBox(height: 20),

        // ADJUST COLOR NAME PRODUCT -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color product name', controllerColorProductName),
        SizedBox(height: 20),

        // ADJUST COLOR DESCRIPTION -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color description', controllerColorDescription),
        SizedBox(height: 20),

        // ADJUST USE UPPERCASE NAME PRODUCT -----------------------------------
        ReusableSetting.buildToggleSwitch(context, 'Uppercase product name',
            isUpperCase, changeValueUppercase),
        SizedBox(height: 20),

        // ADJUST USE MEDIUM STYLE -----------------------------------
        ReusableSetting.buildToggleSwitch(
            context, 'Use Medium style', isMediumStyle, changeValueMediumStyle),
        SizedBox(height: 20),

        // ADJUST USE BACKGROUND CATEGORY TITLE -----------------------------------
        ReusableSetting.buildToggleSwitch(
            context,
            'Use background category title',
            isBackgroundCategoryTitle,
            changeValueBackgroundCategory),
        SizedBox(height: 20),

        // ADJUST USE FULL WIDTH CATEGORY TITLE -----------------------------------
        ReusableSetting.buildToggleSwitch(context,
            'Use full width category title', isFullWidth, changeValueFullWidth),
        SizedBox(height: 20),

        // ADJUST USE THUMBNAIL -----------------------------------
        ReusableSetting.buildToggleSwitch(context, 'Use Thumbnail product',
            isThumbnail, changeValueThumbnail),
        SizedBox(height: 20),

        // ADJUST THUMBNAIL URL -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Thumbnail product url', controllerThumbnailPath),
        SizedBox(height: 20),

        // ADJUST MAIN ALIGNMENT --------------------------------
        Align(
          alignment: Alignment.topLeft,
          child: customText('Main alignment ', mediumSize, textBold),
        ),
        ReusableSetting.buildDropdownMenu(
            context, selectedMainAxis!, changeValueMainAxis, mainAxisOptions),
        SizedBox(height: 20),

        // ADJUST RUN SPACING -----------------------------------
        Row(
          children: [
            SizedBox(
                width: 200,
                child: ReusableSetting.buildAdjustDouble(context, 'Run Spacing',
                    controllerRunSpacing, increaseValue, decreaseValue)),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitRunSpacing!,
                changeValueUnitRunSpacing, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        // ADJUST SPACE BETWEEN CATEGORY PRODUCT LIST -----------------------------------
        Row(
          children: [
            SizedBox(
                width: 200,
                child: ReusableSetting.buildAdjustDouble(
                    context,
                    'Space between category products',
                    controllerSpaceBetween,
                    increaseValue,
                    decreaseValue)),
            Spacer(),
            ReusableSetting.buildDropdownMenu(
                context,
                selectedUnitSpaceBetween!,
                changeValueUnitSpaceBetween,
                ['none', 'w', 'h', 'sp'])
          ],
        ),
      ],
    );
  }

  // This method is used to display list products that added.
  Widget buildListProduct() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: SingleChildScrollView(
        child: ListView.separated(
          shrinkWrap: true,
          itemCount: listProducts.length,
          separatorBuilder: (context, index) => SizedBox(height: 10),
          itemBuilder: (context, index) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.grey[300],
              ),
              child: Row(
                children: [
                  listProducts[index]['name'].isEmpty
                      ? Text('Product ${index + 1}')
                      : Text(listProducts[index]['name']),
                  Spacer(),
                  Row(
                    children: [
                      // BUTTON EDIT PRODUCT --------------------------------
                      IconButton(
                        onPressed: () => editProduct(index),
                        icon: Icon(Icons.edit, size: 18),
                        color: Colors.teal,
                      ),

                      // BUTTON REMOVE PRODUCT --------------------------------
                      IconButton(
                        onPressed: () => removeProduct(index),
                        icon: Icon(Icons.delete, size: 18),
                        color: Colors.red,
                      ),
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  // This method is used to display category that added.
  Widget buildCategory() {
    return Container(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              category.isEmpty || category['name'].isEmpty
                  ? Text('New category')
                  : Text(category['name']),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT CATEGORY --------------------------------
                  IconButton(
                    onPressed: editCategory,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE CATEGORY --------------------------------
                  IconButton(
                    onPressed: removeCategory,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }

  // This method is used to display a category that added.
  Widget buildImageBg() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              Text('New background image'),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT IMAGE --------------------------------
                  IconButton(
                    onPressed: editImageBg,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE IMAGE --------------------------------
                  IconButton(
                    onPressed: removeImageBg,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
