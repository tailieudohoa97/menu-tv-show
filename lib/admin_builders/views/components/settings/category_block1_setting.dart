import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/category_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';
import 'package:tv_menu/admin_builders/views/components/settings/framed_image_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/text_setting.dart';

class CategoryBlock1Setting extends StatefulWidget {
  const CategoryBlock1Setting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<CategoryBlock1Setting> createState() => _CategoryBlock1SettingState();
}

class _CategoryBlock1SettingState extends State<CategoryBlock1Setting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerHeight = TextEditingController();
  TextEditingController controllerWidth = TextEditingController();
  TextEditingController controllerElementGap = TextEditingController();
  TextEditingController controllerWidthFieldKey = TextEditingController();
  TextEditingController controllerHeightFieldKey = TextEditingController();
  TextEditingController controllerCatFontSizeFieldKey = TextEditingController();

  Map<String, dynamic> currentSetting = {};
  dynamic category = {}, categoryTextStyle = {}, categoryImage = {};
  DataSetting dataSetting = DataSetting();
  int currentIndexTab = 0, categoryItemRows = 0;
  late TabController _tabController;
  String? selectedUnitWidth, selectedUnitHeight, selectedUnitElementGap;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updatedCategory = {};

    if (currentSetting["category"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['category']) ?? {};
      updatedCategory = data;
    }

    setState(() {
      category = updatedCategory;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    initDataSetting(currentSetting);
    _tabController = TabController(length: 2, vsync: this);

    categoryImage = currentSetting['categoryImage'] ?? {};
    categoryItemRows = currentSetting['categoryItemRows'].length;
    categoryTextStyle = currentSetting['categoryTextStyle'] ?? {};
    controllerHeight.text = ConvertUnit.unitToString(currentSetting["height"]);
    controllerWidth.text = ConvertUnit.unitToString(currentSetting["width"]);
    controllerWidthFieldKey.text =
        ConvertUnit.unitToString(currentSetting["widthFromFieldKey"]);
    controllerHeightFieldKey.text =
        ConvertUnit.unitToString(currentSetting["heightFromFieldKey"]);
    controllerCatFontSizeFieldKey.text = ConvertUnit.unitToString(
        currentSetting["categoryFontSizeFromFieldKey"]);
    controllerElementGap.text =
        ConvertUnit.unitToString(currentSetting["elementGap"]);
    selectedUnitWidth = ConvertUnit.getUnit(currentSetting["width"]);
    selectedUnitHeight = ConvertUnit.getUnit(currentSetting["height"]);
    selectedUnitElementGap = ConvertUnit.getUnit(currentSetting["elementGap"]);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changeValueUnitWidth(String? value) {
    setState(() {
      selectedUnitWidth = value!;
    });
  }

  void changeValueUnitHeight(String? value) {
    setState(() {
      selectedUnitHeight = value!;
    });
  }

  void changeValueUnitElementGap(String? value) {
    setState(() {
      selectedUnitElementGap = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to add numbers categories.
  void addCategory() {
    setState(() {
      final cat = BuilderRepository().generateJsonCategoryData();
      category = cat;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeCategory() {
    // update dataSetting.
    dataSetting.removeDataOnSetting(category["id"]);
    setState(() {
      category = {};
    });
  }

  // This function is used to edit a product.
  void editCategory() {
    void updateCategory(dynamic data) {
      setState(() {
        category = data;
      });
      dataSetting.updateDataOnSetting(category["id"], category);
    }

    customDialog(
      context,
      CategoryDataSetting(
        widgetId: category['id'],
        bloc: widget.bloc,
        onUpdateCategory: updateCategory,
        currentCategory: category,
      ),
    );
  }

  // This function is used to add numbers categories.
  void addCategoryImage() {
    setState(() {
      final i = BuilderRepository().generateJsonFramedImage();
      categoryImage = i;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeCategoryImage() {
    setState(() {
      categoryImage = {};
    });
  }

  // This function is used to edit a product.
  void editCategoryImage() {
    void updateCategoryImage(dynamic data) {
      setState(() {
        categoryImage["widgetSetting"] = data;
      });
    }

    customDialog(
      context,
      FramedImageSetting(
        widgetId: categoryImage['id'],
        bloc: widget.bloc,
        onUpdateData: updateCategoryImage,
        currentData: categoryImage["widgetSetting"],
      ),
    );
  }

  // text style
  void editCategoryTextStyle() {
    if (categoryTextStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          categoryTextStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          categoryTextStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: categoryTextStyle,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "category": category['id'] ?? "",
      "categoryImage": categoryImage,
      "categoryFontSizeFromFieldKey": controllerCatFontSizeFieldKey.text,
      "heightFromFieldKey": controllerHeightFieldKey.text,
      "widthFromFieldKey": controllerWidthFieldKey.text,
      // "categoryItemRows": [],
      "categoryTextStyle": categoryTextStyle,
      "elementGap": ConvertUnit.stringToUnit(
          controllerElementGap.text, selectedUnitElementGap!),
      "height":
          ConvertUnit.stringToUnit(controllerHeight.text, selectedUnitHeight!),
      "width":
          ConvertUnit.stringToUnit(controllerWidth.text, selectedUnitWidth!),
    };

    if (category.isNotEmpty) {
      dataSetting.addDataToSetting(category['id'], category);
    }

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 440,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Category Block 1'),
          // Tab bar includes two tab: Categories and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD CATEGORIES
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              // ADD LIST CATEGORY ITEMS
              Row(
                children: [
                  SizedBox(
                    width: 300,
                    child: customText(
                        'Number products current: $categoryItemRows',
                        mediumSize,
                        textNormal),
                  )
                ],
              ),
              SizedBox(height: 20),

              Row(
                children: [
                  customText('Add new category', mediumSize, textNormal),

                  // BUTTON ADD CATEGORY --------------------------------
                  category.isEmpty
                      ? IconButton(
                          onPressed: addCategory,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              category.isNotEmpty ? buildCategory() : Text(''),
              category.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 0),

              Row(
                children: [
                  customText('Add framed image', mediumSize, textNormal),

                  // BUTTON ADD CATEGORY --------------------------------
                  categoryImage.isEmpty
                      ? IconButton(
                          onPressed: addCategoryImage,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              categoryImage.isNotEmpty ? buildCategoryImage() : Text(''),
              categoryImage.isNotEmpty
                  ? SizedBox(height: 20)
                  : SizedBox(height: 0),
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        ReusableSetting.buildInputSetting(context,
            'Category font size field key', controllerCatFontSizeFieldKey),
        SizedBox(height: 20),
        ReusableSetting.buildInputSetting(
            context, 'Width field key', controllerWidthFieldKey),
        SizedBox(height: 20),
        ReusableSetting.buildInputSetting(
            context, 'Height field key', controllerHeightFieldKey),
        SizedBox(height: 20),
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Width',
                  controllerWidth, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitWidth!,
                changeValueUnitWidth, ['none', 'w'])
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Height',
                  controllerHeight, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitHeight!,
                changeValueUnitHeight, ['none', 'h'])
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Element gap',
                  controllerElementGap, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitElementGap!,
                changeValueUnitElementGap, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            customText('Category text style ', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editCategoryTextStyle,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
        SizedBox(height: 20),
      ],
    );
  }

  // This method is used to display a category that added.
  Widget buildCategory() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              category.isEmpty || category['name'].isEmpty
                  ? Text('New category')
                  : Text(category['name']),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT CATEGORY --------------------------------
                  IconButton(
                    onPressed: editCategory,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE CATEGORY --------------------------------
                  IconButton(
                    onPressed: removeCategory,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }

  // This method is used to display a category that added.
  Widget buildCategoryImage() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              Text('Framed image'),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT CATEGORY --------------------------------
                  IconButton(
                    onPressed: editCategoryImage,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE CATEGORY --------------------------------
                  IconButton(
                    onPressed: removeCategoryImage,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
