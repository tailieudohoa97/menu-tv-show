import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

// Divider

class DividerSetting extends StatefulWidget {
  const DividerSetting(
      {super.key,
      this.widgetId,
      required this.bloc,
      this.onUpdateData,
      this.currentData});

  final String? widgetId;
  final BuilderBloc bloc;
  final Function? onUpdateData;
  final Map<String, dynamic>? currentData;

  @override
  State<DividerSetting> createState() => _DividerSettingState();
}

class _DividerSettingState extends State<DividerSetting> {
  TextEditingController controllerHeight = TextEditingController();
  TextEditingController controllerIndent = TextEditingController();
  TextEditingController controllerEndIndent = TextEditingController();
  TextEditingController controllerThickness = TextEditingController();

  TextEditingController controllerColorDivider = TextEditingController();

  String? selectedUnitHeight,
      selectedUnitIndent,
      selectedUnitEndIndent,
      selectedUnitThickness;

  Map<String, dynamic> currentSetting = {};
  @override
  void initState() {
    super.initState();

    if (widget.currentData != null) {
      currentSetting = widget.currentData!;

      controllerHeight.text =
          ConvertUnit.unitToString(currentSetting['height']);
      controllerIndent.text =
          ConvertUnit.unitToString(currentSetting['indent']);
      controllerEndIndent.text =
          ConvertUnit.unitToString(currentSetting['endIndent']);
      controllerThickness.text =
          ConvertUnit.unitToString(currentSetting['thickness']);
      controllerColorDivider.text = currentSetting['color'];

      selectedUnitHeight = ConvertUnit.getUnit(currentSetting["height"]);
      selectedUnitIndent = ConvertUnit.getUnit(currentSetting['indent']);
      selectedUnitEndIndent = ConvertUnit.getUnit(currentSetting['endIndent']);
      selectedUnitThickness = ConvertUnit.getUnit(currentSetting['thickness']);
    } else {
      currentSetting = widget.bloc.state.currentSetting['widgetSetting'];

      controllerHeight.text =
          ConvertUnit.unitToString(currentSetting['height']);
      controllerIndent.text =
          ConvertUnit.unitToString(currentSetting['indent']);
      controllerEndIndent.text =
          ConvertUnit.unitToString(currentSetting['endIndent']);
      controllerThickness.text =
          ConvertUnit.unitToString(currentSetting['thickness']);
      controllerColorDivider.text = currentSetting['color'];

      selectedUnitHeight = ConvertUnit.getUnit(currentSetting["height"]);
      selectedUnitIndent = ConvertUnit.getUnit(currentSetting['indent']);
      selectedUnitEndIndent = ConvertUnit.getUnit(currentSetting['endIndent']);
      selectedUnitThickness = ConvertUnit.getUnit(currentSetting['thickness']);
    }
  }

  void changeValueUnitHeight(String? value) {
    setState(() {
      selectedUnitHeight = value!;
    });
  }

  void changeValueUnitIndent(String? value) {
    setState(() {
      selectedUnitIndent = value!;
    });
  }

  void changeValueUnitEndIndent(String? value) {
    setState(() {
      selectedUnitEndIndent = value!;
    });
  }

  void changeValueUnitThickness(String? value) {
    setState(() {
      selectedUnitThickness = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "height":
          ConvertUnit.stringToUnit(controllerHeight.text, selectedUnitHeight!),
      "color": controllerColorDivider.text,
      "indent":
          ConvertUnit.stringToUnit(controllerIndent.text, selectedUnitIndent!),
      "endIndent": ConvertUnit.stringToUnit(
          controllerEndIndent.text, selectedUnitEndIndent!),
      "thickness": ConvertUnit.stringToUnit(
          controllerThickness.text, selectedUnitThickness!)
    };

    if (widget.onUpdateData != null) {
      widget.onUpdateData!(data);
    } else {
      widget.bloc
          .add(UpdateModuleEvent(widgetId: widget.widgetId!, model: data));
    }

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 460,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Divider'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADJUST DIVIDER COLOR -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Color divider', controllerColorDivider),
                    SizedBox(height: 20),

                    // ADJUST HEIGHT -----------------------------------
                    buildAdjustWidget(
                        ReusableSetting.buildAdjustDouble(context, 'Height',
                            controllerHeight, increaseValue, decreaseValue),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitHeight!,
                            changeValueUnitHeight,
                            ['none', 'w', 'h', 'sp'])),
                    SizedBox(height: 20),

                    // ADJUST INDENT -----------------------------------
                    buildAdjustWidget(
                        ReusableSetting.buildAdjustDouble(context, 'Indent',
                            controllerIndent, increaseValue, decreaseValue),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitIndent!,
                            changeValueUnitIndent,
                            ['none', 'w', 'h', 'sp'])),
                    SizedBox(height: 20),

                    // ADJUST END INDENT -----------------------------------
                    buildAdjustWidget(
                        ReusableSetting.buildAdjustDouble(context, 'End indent',
                            controllerEndIndent, increaseValue, decreaseValue),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitEndIndent!,
                            changeValueUnitEndIndent,
                            ['none', 'w', 'h', 'sp'])),
                    SizedBox(height: 20),

                    // ADJUST THICKNESS -----------------------------------
                    buildAdjustWidget(
                        ReusableSetting.buildAdjustDouble(context, 'Thickness',
                            controllerThickness, increaseValue, decreaseValue),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitThickness!,
                            changeValueUnitThickness,
                            ['none', 'w', 'h', 'sp'])),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  Row buildAdjustWidget(Widget widget1, Widget widget2) {
    return Row(
      children: [
        SizedBox(
          width: 200,
          child: widget1,
        ),
        Spacer(),
        widget2
      ],
    );
  }
}
