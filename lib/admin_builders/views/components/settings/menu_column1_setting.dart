import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';
import 'package:tv_menu/admin_builders/views/components/settings/text_setting.dart';

class MenuColumn1Setting extends StatefulWidget {
  const MenuColumn1Setting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<MenuColumn1Setting> createState() => _MenuColumn1SettingState();
}

class _MenuColumn1SettingState extends State<MenuColumn1Setting> {
  TextEditingController controllerBrandName = TextEditingController();
  TextEditingController controllerSlogan = TextEditingController();
  TextEditingController controllerBrandFontSizeFieldKey =
      TextEditingController();
  TextEditingController controllerSloganFontSizeFieldKey =
      TextEditingController();

  dynamic brandTextStyle = {}, sloganTextStyle = {};
  int menuItems = 0;

  Map<String, dynamic> currentSetting = {};

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();

    menuItems = currentSetting["menuItems"].length;
    controllerBrandName.text = currentSetting['brand'];
    controllerSlogan.text = currentSetting['slogan'];
    controllerBrandFontSizeFieldKey.text =
        currentSetting['brandFontSizeFromFieldKey'];
    controllerSloganFontSizeFieldKey.text =
        currentSetting['sloganFontSizeFromFieldKey'];
    brandTextStyle = currentSetting['brandTextStyle'] ?? {};
    sloganTextStyle = currentSetting['sloganTextStyle'] ?? {};
  }

  void editBrandTextStyle() {
    if (brandTextStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          brandTextStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          brandTextStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: brandTextStyle,
        ),
      );
    }
  }

  void editSloganTextStyle() {
    if (sloganTextStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          sloganTextStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          sloganTextStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: sloganTextStyle,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      // "menuItems": [],
      "brand": controllerBrandName.text,
      "slogan": controllerSlogan.text,
      "brandTextStyle": brandTextStyle,
      "sloganTextStyle": sloganTextStyle,
      "brandFontSizeFromFieldKey": controllerBrandFontSizeFieldKey.text,
      "sloganFontSizeFromFieldKey": controllerSloganFontSizeFieldKey.text,
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Menu Column 1'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADD LIST CATEGORY ITEMS
                    Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: customText(
                              'Number products current: $menuItems',
                              mediumSize,
                              textNormal),
                        )
                      ],
                    ),
                    SizedBox(height: 20),

                    ReusableSetting.buildInputSetting(
                        context,
                        "Brand font size field key",
                        controllerBrandFontSizeFieldKey),
                    SizedBox(height: 20),

                    ReusableSetting.buildInputSetting(
                        context,
                        "Slogan font size field key",
                        controllerSloganFontSizeFieldKey),
                    SizedBox(height: 20),

                    // ADJUST BRAND NAME -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, "Brand name", controllerBrandName),
                    SizedBox(height: 20),

                    // ADJUST SLOGAN -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, "Slogan", controllerSlogan),
                    SizedBox(height: 20),

                    Row(
                      children: [
                        customText('Brand text style ', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editBrandTextStyle,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                    SizedBox(height: 20),

                    Row(
                      children: [
                        customText('Slogan text style ', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editSloganTextStyle,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
