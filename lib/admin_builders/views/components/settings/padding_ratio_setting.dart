import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class PaddingRatioSetting extends StatefulWidget {
  const PaddingRatioSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<PaddingRatioSetting> createState() => _PaddingRatioSettingState();
}

class _PaddingRatioSettingState extends State<PaddingRatioSetting> {
  TextEditingController controllerPaddingFieldKey = TextEditingController();
  TextEditingController controllerTopPadding = TextEditingController();
  TextEditingController controllerBottomPadding = TextEditingController();
  TextEditingController controllerRightPadding = TextEditingController();
  TextEditingController controllerLeftPadding = TextEditingController();
  TextEditingController controllerVerticalPadding = TextEditingController();
  TextEditingController controllerHorizontalPadding = TextEditingController();

  Map<String, dynamic> currentSetting = {};

  @override
  void initState() {
    super.initState();
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];

    controllerPaddingFieldKey.text = currentSetting["paddingFieldKey"];
    controllerTopPadding.text = currentSetting["topPaddingFieldKey"];
    controllerBottomPadding.text = currentSetting["bottomPaddingFieldKey"];
    controllerRightPadding.text = currentSetting["rightPaddingFieldKey"];
    controllerLeftPadding.text = currentSetting["leftPaddingFieldKey"];
    controllerVerticalPadding.text = currentSetting["verticalPaddingFieldKey"];
    controllerHorizontalPadding.text =
        currentSetting["horizontalPaddingFieldKey"];
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "paddingFieldKey": controllerPaddingFieldKey.text,
      "topPaddingFieldKey": controllerTopPadding.text,
      "bottomPaddingFieldKey": controllerBottomPadding.text,
      "rightPaddingFieldKey": controllerRightPadding.text,
      "leftPaddingFieldKey": controllerLeftPadding.text,
      "verticalPaddingFieldKey": controllerVerticalPadding.text,
      "horizontalPaddingFieldKey": controllerHorizontalPadding.text,
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Padding By Ratio'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADJUST PRODUCT NAME -----------------------------------
                    ReusableSetting.buildInputSetting(context,
                        'Padding field key', controllerPaddingFieldKey),
                    SizedBox(height: 20),

                    // ADJUST PRODUCT NAME -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Top padding field key', controllerTopPadding),
                    SizedBox(height: 20),

                    // ADJUST PRODUCT NAME -----------------------------------
                    ReusableSetting.buildInputSetting(context,
                        'Bottom padding field key', controllerBottomPadding),
                    SizedBox(height: 20),

                    // ADJUST CATEGORY IMAGE -----------------------------------
                    ReusableSetting.buildInputSetting(context,
                        'Right padding field key', controllerTopPadding),
                    SizedBox(height: 20),

                    // ADJUST CATEGORY DESCRIPTION -----------------------------------
                    ReusableSetting.buildInputSetting(context,
                        'Left padding field key', controllerLeftPadding),
                    SizedBox(height: 20),

                    ReusableSetting.buildInputSetting(
                        context,
                        'Vertical padding field key',
                        controllerVerticalPadding),
                    SizedBox(height: 20),

                    ReusableSetting.buildInputSetting(
                        context,
                        'Horizontal padding field key',
                        controllerHorizontalPadding),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
