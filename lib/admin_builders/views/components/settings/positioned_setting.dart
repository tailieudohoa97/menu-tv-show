import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class PositionedSetting extends StatefulWidget {
  const PositionedSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<PositionedSetting> createState() => _PositionedSettingState();
}

class _PositionedSettingState extends State<PositionedSetting> {
  TextEditingController controllerTop = TextEditingController();
  TextEditingController controllerRight = TextEditingController();
  TextEditingController controllerBottom = TextEditingController();
  TextEditingController controllerLeft = TextEditingController();
  Map<String, dynamic> currentSetting = {};

  String? selectedUnitTop,
      selectedUnitRight,
      selectedUnitBottom,
      selectedUnitLeft;

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    controllerTop.text = ConvertUnit.unitToString(currentSetting['top']);
    controllerRight.text = ConvertUnit.unitToString(currentSetting['right']);
    controllerBottom.text = ConvertUnit.unitToString(currentSetting['bottom']);
    controllerLeft.text = ConvertUnit.unitToString(currentSetting['left']);

    selectedUnitTop = ConvertUnit.getUnit(currentSetting['top']);
    selectedUnitRight = ConvertUnit.getUnit(currentSetting['right']);
    selectedUnitBottom = ConvertUnit.getUnit(currentSetting['bottom']);
    selectedUnitLeft = ConvertUnit.getUnit(currentSetting['left']);
  }

  void changeValueUnitTop(String? value) {
    setState(() {
      selectedUnitTop = value!;
    });
  }

  void changeValueUnitRight(String? value) {
    setState(() {
      selectedUnitRight = value!;
    });
  }

  void changeValueUnitBottom(String? value) {
    setState(() {
      selectedUnitBottom = value!;
    });
  }

  void changeValueUnitLeft(String? value) {
    setState(() {
      selectedUnitLeft = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "top": ConvertUnit.stringToUnit(controllerTop.text, selectedUnitTop!),
      "right":
          ConvertUnit.stringToUnit(controllerRight.text, selectedUnitRight!),
      "bottom":
          ConvertUnit.stringToUnit(controllerBottom.text, selectedUnitBottom!),
      "left": ConvertUnit.stringToUnit(controllerLeft.text, selectedUnitLeft!)
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 394,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Positioned'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    buildAdjustPositioned(context),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // This method is used to adjust positioned
  Column buildAdjustPositioned(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(
                  context, 'Top', controllerTop, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitTop!,
                changeValueUnitTop, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Right',
                  controllerRight, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitRight!,
                changeValueUnitRight, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Bottom',
                  controllerBottom, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitBottom!,
                changeValueUnitBottom, ['none', 'w', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Left',
                  controllerLeft, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitLeft!,
                changeValueUnitLeft, ['none', 'w', 'h', 'sp'])
          ],
        ),
      ],
    );
  }
}
