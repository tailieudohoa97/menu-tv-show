import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class VerticalDividerSetting extends StatefulWidget {
  const VerticalDividerSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<VerticalDividerSetting> createState() => _VerticalDividerSettingState();
}

class _VerticalDividerSettingState extends State<VerticalDividerSetting> {
  TextEditingController controllerWidth = TextEditingController();
  TextEditingController controllerIndent = TextEditingController();
  TextEditingController controllerEndIndent = TextEditingController();
  TextEditingController controllerThickness = TextEditingController();
  TextEditingController controllerColorDivider = TextEditingController();

  String? selectedUnitWidth,
      selectedUnitIndent,
      selectedUnitEndIndent,
      selectedUnitThickness;
  Map<String, dynamic> currentSetting = {};

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    controllerWidth.text = ConvertUnit.unitToString(currentSetting['width']);
    controllerIndent.text = ConvertUnit.unitToString(currentSetting['indent']);
    controllerColorDivider.text = currentSetting["color"];
    controllerEndIndent.text =
        ConvertUnit.unitToString(currentSetting['endIndent']);
    controllerThickness.text =
        ConvertUnit.unitToString(currentSetting['thickness']);

    selectedUnitWidth = ConvertUnit.getUnit(currentSetting["width"]);
    selectedUnitIndent = ConvertUnit.getUnit(currentSetting['indent']);
    selectedUnitEndIndent = ConvertUnit.getUnit(currentSetting['endIndent']);
    selectedUnitThickness = ConvertUnit.getUnit(currentSetting['thickness']);
  }

  void changeValueUnitWidth(String? value) {
    setState(() {
      selectedUnitWidth = value!;
    });
  }

  void changeValueUnitIndent(String? value) {
    setState(() {
      selectedUnitIndent = value!;
    });
  }

  void changeValueUnitEndIndent(String? value) {
    setState(() {
      selectedUnitEndIndent = value!;
    });
  }

  void changeValueUnitThickness(String? value) {
    setState(() {
      selectedUnitThickness = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "height":
          ConvertUnit.stringToUnit(controllerWidth.text, selectedUnitWidth!),
      "color": controllerColorDivider.text,
      "indent":
          ConvertUnit.stringToUnit(controllerIndent.text, selectedUnitIndent!),
      "endIndent": ConvertUnit.stringToUnit(
          controllerEndIndent.text, selectedUnitEndIndent!),
      "thickness": ConvertUnit.stringToUnit(
          controllerThickness.text, selectedUnitThickness!)
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 460,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Vertical Divider'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADJUST DIVIDER COLOR -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Color divider', controllerColorDivider),
                    SizedBox(height: 20),

                    // ADJUST HEIGHT -----------------------------------
                    buildAdjustWidget(
                        ReusableSetting.buildAdjustDouble(context, 'Width',
                            controllerWidth, increaseValue, decreaseValue),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitWidth!,
                            changeValueUnitWidth,
                            ['none', 'w' , 'h', 'sp'])),
                    SizedBox(height: 20),

                    // ADJUST INDENT -----------------------------------
                    buildAdjustWidget(
                        ReusableSetting.buildAdjustDouble(context, 'Indent',
                            controllerIndent, increaseValue, decreaseValue),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitIndent!,
                            changeValueUnitIndent,
                            ['none', 'w', 'h', 'sp'])),
                    SizedBox(height: 20),

                    // ADJUST END INDENT -----------------------------------
                    buildAdjustWidget(
                        ReusableSetting.buildAdjustDouble(context, 'End indent',
                            controllerEndIndent, increaseValue, decreaseValue),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitEndIndent!,
                            changeValueUnitEndIndent,
                            ['none', 'w', 'h', 'sp'])),
                    SizedBox(height: 20),

                    // ADJUST THICKNESS -----------------------------------
                    buildAdjustWidget(
                        ReusableSetting.buildAdjustDouble(context, 'Thickness',
                            controllerThickness, increaseValue, decreaseValue),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitThickness!,
                            changeValueUnitThickness,
                            ['none', 'w', 'h', 'sp'])),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  Row buildAdjustWidget(Widget widget1, Widget widget2) {
    return Row(
      children: [
        SizedBox(
          width: 200,
          child: widget1,
        ),
        Spacer(),
        widget2
      ],
    );
  }
}
