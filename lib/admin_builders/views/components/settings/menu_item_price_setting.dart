import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';
import 'package:tv_menu/admin_builders/views/components/settings/text_setting.dart';

class MenuItemPriceSetting extends StatefulWidget {
  const MenuItemPriceSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<MenuItemPriceSetting> createState() => _MenuItemPriceSettingState();
}

class _MenuItemPriceSettingState extends State<MenuItemPriceSetting> {
  TextEditingController controllerPrice = TextEditingController();
  TextEditingController controllerPrefix = TextEditingController();

  Map<String, dynamic> currentSetting = {};
  dynamic textStyle = {};

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();

    controllerPrice.text = currentSetting["price"].substring(1);
    textStyle = currentSetting['textStyle'] ?? {};
    controllerPrefix.text = currentSetting['prefix'];
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  void editTextStyle() {
    if (textStyle.isEmpty) {
      final text = BuilderRepository().generateJsonText();

      void updateTextStyle(dynamic data) {
        text['widgetSetting']['style'] = data;
        setState(() {
          textStyle = text['widgetSetting']['style'];
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: text['widgetSetting']['style'],
        ),
      );
    } else {
      // category is exists
      void updateTextStyle(dynamic data) {
        setState(() {
          textStyle = data;
        });
      }

      customDialog(
        context,
        TextSetting(
          bloc: widget.bloc,
          onUpdateTextStyle: updateTextStyle,
          currentTextStyle: textStyle,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "price": controllerPrice.text,
      "prefix": controllerPrefix.text,
      "textStyle": textStyle,
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 340,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Menu Item Price'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADJUST PRICE
                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustNumber(
                              context,
                              'Price',
                              controllerPrice,
                              increaseValue,
                              decreaseValue),
                        )
                      ],
                    ),
                    SizedBox(height: 20),

                    // PREFIX OPTIONS -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, "Prefix", controllerPrefix),
                    SizedBox(height: 20),

                    // ADJUST TEXT STYLE
                    Row(
                      children: [
                        customText('Text style ', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editTextStyle,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
