import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/margin_padding_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

class ListViewSetting extends StatefulWidget {
  const ListViewSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<ListViewSetting> createState() => _ListViewSettingState();
}

class _ListViewSettingState extends State<ListViewSetting> {
  bool isReverse = false;
  bool isShrinkWrap = false;
  int children = 0;
  dynamic padding = {};

  Map<String, dynamic> currentSetting = {};

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();

    padding = currentSetting['padding'] ?? {};
    children = currentSetting["children"].length;
    isReverse = bool.parse(currentSetting['reverse']);
    isShrinkWrap = bool.parse(currentSetting['shrinkWrap']);
  }

  // This function is used to change use reverse value.
  void changeValueReverse(bool value) {
    setState(() {
      isReverse = value;
    });
  }

  // This function is used to change use shrinkWrap value.
  void changeValueShrinkWrap(bool value) {
    setState(() {
      isShrinkWrap = value;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // Border bottom side
  void editPaddingStyle() {
    // if borderSide current is empty
    if (padding.isEmpty) {
      // new setting border
      var p = BuilderRepository().generateJsonPaddingMargin();

      // update values after edit
      void updatePadding(dynamic data) {
        p = data;
        setState(() {
          padding = p;
        });
      }

      // pass new setting to MarginPaddingSetting
      customDialog(
        context,
        MarginPaddingSetting(
          type: 'Padding',
          bloc: widget.bloc,
          onUpdateData: updatePadding,
          currentData: p,
        ),
      );
    } else {
      // border side is exists
      void updatePadding(dynamic data) {
        setState(() {
          padding = data;
        });
      }

      customDialog(
        context,
        MarginPaddingSetting(
          type: 'Padding',
          bloc: widget.bloc,
          onUpdateData: updatePadding,
          currentData: padding,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "reverse": isReverse.toString(),
      "shrinkWrap": isShrinkWrap.toString(),
      "padding": padding,
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 343,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting ListView'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADD LIST CATEGORY ITEMS
                    Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: customText(
                              'Number products current: $children',
                              mediumSize,
                              textNormal),
                        )
                      ],
                    ),
                    SizedBox(height: 20),

                    // ADJUST USE REVERSE --------------------------------
                    ReusableSetting.buildToggleSwitch(
                        context, 'Reverse', isReverse, changeValueReverse),
                    SizedBox(height: 20),

                    // ADJUST USE SHRINK WRAP --------------------------------
                    ReusableSetting.buildToggleSwitch(context, 'Shrink wrap',
                        isShrinkWrap, changeValueShrinkWrap),
                    SizedBox(height: 20),

                    // ADJUST PADDING -----------------------------------
                    Row(
                      children: [
                        customText('Padding style ', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editPaddingStyle,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
