import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';

class ProductSetting extends StatefulWidget {
  const ProductSetting({super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<ProductSetting> createState() => _ProductSettingState();
}

class _ProductSettingState extends State<ProductSetting> {
  TextEditingController controllerName = TextEditingController();
  TextEditingController controllerPrice = TextEditingController();
  TextEditingController controllerSalePrice = TextEditingController();
  TextEditingController controllerDescription = TextEditingController();
  TextEditingController controllerThumbnailPath = TextEditingController();
  TextEditingController controllerMaxLineProductName = TextEditingController();
  TextEditingController controllerMaxLineProductDescription =
      TextEditingController();

  TextEditingController controllerColorPrice = TextEditingController();
  TextEditingController controllerColorProductName = TextEditingController();
  TextEditingController controllerColorDescription = TextEditingController();

  bool isUpperCase = false, isMediumStyle = false, isThumbnail = false;
  Map<String, dynamic> currentSetting = {};
  DataSetting dataSetting = DataSetting();

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    controllerName.text = currentSetting['name'];
    controllerPrice.text = currentSetting['price'];
    controllerSalePrice.text = currentSetting['salePrice'];
    controllerDescription.text = currentSetting['description'];
    controllerColorPrice.text = currentSetting["colorPrice"];
    controllerColorProductName.text = currentSetting["colorProductName"];
    controllerColorDescription.text = currentSetting["colorDescription"];
    isUpperCase = bool.parse(currentSetting["toUpperCaseNameProductName"]);
    isMediumStyle = bool.parse(currentSetting["useMediumStyle"]);
    isThumbnail = bool.parse(currentSetting["useThumbnailProduct"]);
    controllerThumbnailPath.text = currentSetting['pathThumbnailProduct'];
    controllerMaxLineProductName.text = currentSetting['maxLineProductName'];
    controllerMaxLineProductDescription.text =
        currentSetting['maxLineProductDescription'];
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to change use uppercase value.
  void changeValueUppercase(bool value) {
    setState(() {
      isUpperCase = value;
    });
  }

  // This function is used to change use medium style value.
  void changeValueMediumStyle(bool value) {
    setState(() {
      isMediumStyle = value;
    });
  }

  // This function is used to change use thumbnail value.
  void changeValueThumbnail(bool value) {
    setState(() {
      isThumbnail = value;
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "name": controllerName.text,
      "price": controllerPrice.text,
      "salePrice": controllerSalePrice.text,
      "description": controllerDescription.text,
      "colorPrice": controllerColorPrice.text,
      "colorProductName": controllerColorProductName.text,
      "colorDescription": controllerColorDescription.text,
      "toUpperCaseNameProductName": isUpperCase.toString(),
      "useMediumStyle": isMediumStyle.toString(),
      "useThumbnailProduct": isThumbnail.toString(),
      "pathThumbnailProduct": controllerThumbnailPath.text,
      "maxLineProductName": controllerMaxLineProductName.text,
      "maxLineProductDescription": controllerMaxLineProductDescription.text
    };

    dataSetting.addDataToSetting(widget.widgetId, {
      "id": widget.widgetId,
      "type": "product",
      "name": data["name"],
      "price": data["price"],
      "salePrice": data["salePrice"],
      "description": data["description"],
    });

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));

    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Product'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // FIELD NAME
                    ReusableSetting.buildInputSetting(
                        context, 'Name', controllerName),
                    SizedBox(height: 20),

                    // FIELD PRICE
                    ReusableSetting.buildInputSetting(
                        context, 'Price', controllerPrice),
                    SizedBox(height: 20),

                    // FIELD SALE PRICE
                    ReusableSetting.buildInputSetting(
                        context, 'Sale price', controllerSalePrice),
                    SizedBox(height: 20),

                    // FIELD DESCRIPTION
                    ReusableSetting.buildInputSetting(
                        context, 'Description', controllerDescription),
                    SizedBox(height: 20),

                    // ADJUST COLOR PRICE -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Color price', controllerColorPrice),
                    SizedBox(height: 20),

                    // ADJUST COLOR NAME PRODUCT -----------------------------------
                    ReusableSetting.buildInputSetting(context,
                        'Color product name', controllerColorProductName),
                    SizedBox(height: 20),

                    // ADJUST COLOR DESCRIPTION -----------------------------------
                    ReusableSetting.buildInputSetting(context,
                        'Color description', controllerColorDescription),
                    SizedBox(height: 20),

                    // ADJUST USE UPPERCASE NAME PRODUCT -----------------------------------
                    ReusableSetting.buildToggleSwitch(
                        context,
                        'Uppercase product name',
                        isUpperCase,
                        changeValueUppercase),
                    SizedBox(height: 20),

                    // ADJUST USE MEDIUM STYLE -----------------------------------
                    ReusableSetting.buildToggleSwitch(
                        context,
                        'Use Medium style',
                        isMediumStyle,
                        changeValueMediumStyle),
                    SizedBox(height: 20),

                    // ADJUST USE THUMBNAIL -----------------------------------
                    ReusableSetting.buildToggleSwitch(
                        context,
                        'Use Thumbnail product',
                        isThumbnail,
                        changeValueThumbnail),
                    SizedBox(height: 20),

                    // ADJUST THUMBNAIL URL -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Thumbnail url', controllerThumbnailPath),
                    SizedBox(height: 20),

                    // ADJUST MAX LINES OF PRODUCT NAME -----------------------------------
                    Row(
                      children: [
                        SizedBox(
                            width: 200,
                            child: ReusableSetting.buildAdjustNumber(
                                context,
                                'Max lines product name',
                                controllerMaxLineProductName,
                                increaseValue,
                                decreaseValue)),
                      ],
                    ),
                    SizedBox(height: 20),

                    // ADJUST MAX LINES OF DESCRIPTION -----------------------------------
                    Row(
                      children: [
                        SizedBox(
                            width: 200,
                            child: ReusableSetting.buildAdjustNumber(
                                context,
                                'Max lines description',
                                controllerMaxLineProductDescription,
                                increaseValue,
                                decreaseValue)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
