import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/image_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

class SimpleImageSetting extends StatefulWidget {
  const SimpleImageSetting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<SimpleImageSetting> createState() => _SimpleImageSettingState();
}

enum Fit { cover, contain, fill, none }

final Map<String, Fit> fitValues = {
  "cover": Fit.cover,
  "contain": Fit.contain,
  "fill": Fit.fill,
  "none": Fit.none
};

class _SimpleImageSettingState extends State<SimpleImageSetting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerImageWidth = TextEditingController();
  TextEditingController controllerImageHeight = TextEditingController();
  TextEditingController controllerImagePath = TextEditingController();
  Fit _fitSelected = Fit.cover;
  DataSetting dataSetting = DataSetting();
  dynamic image = {};
  int currentIndexTab = 0;
  late TabController _tabController;
  String? selectedUnitWidth, selectedUnitHeight;

  Map<String, dynamic> currentSetting = {};

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updatedImage = {};

    if (currentSetting["imageDataKey"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['imageDataKey']) ?? {};
      updatedImage = data;
    } else {
      updatedImage = {};
    }

    setState(() {
      image = updatedImage;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    _tabController = TabController(length: 2, vsync: this);
    initDataSetting(currentSetting);
    super.initState();
    controllerImageWidth.text =
        ConvertUnit.unitToString(currentSetting["height"]);
    controllerImageHeight.text =
        ConvertUnit.unitToString(currentSetting["width"]);
    controllerImagePath.text = currentSetting['imagePath'];
    _fitSelected = convertStringToFix(currentSetting['fit']);

    selectedUnitWidth = ConvertUnit.getUnit(currentSetting["width"]);
    selectedUnitHeight = ConvertUnit.getUnit(currentSetting["height"]);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changeValueUnitWidth(String? value) {
    setState(() {
      selectedUnitWidth = value!;
    });
  }

  void changeValueUnitHeight(String? value) {
    setState(() {
      selectedUnitHeight = value!;
    });
  }

  // Convert radio value to string value
  String convertFitToString() {
    // output is a string
    String selectedKey = '';
    // loop and find key has value equal _typeSelected current
    for (var entry in fitValues.entries) {
      if (entry.value == _fitSelected) {
        selectedKey = entry.key;
        break;
      }
    }
    return selectedKey;
  }

  Fit convertStringToFix(String fitData) {
    // output is a string
    Fit selectedValue = Fit.cover;
    // loop and find key has value equal _typeSelected current
    for (var entry in fitValues.entries) {
      if (entry.key == fitData) {
        selectedValue = entry.value;
        break;
      }
    }
    return selectedValue;
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to change fit option value.
  void changeFitValue(value) {
    setState(() {
      _fitSelected = value!;
    });
  }

  // This function is used to add numbers categories.
  void addImage() {
    setState(() {
      final i = BuilderRepository().generateJsonImageData();
      image = i;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeImage() {
    dataSetting.removeDataOnSetting(image['id']);
    setState(() {
      image = {};
    });
  }

  // This function is used to edit a product.
  void editImage() {
    void updateImage(dynamic data) {
      setState(() {
        image = data;
      });
      dataSetting.updateDataOnSetting(image['id'], image);
    }

    customDialog(
      context,
      ImageDataSetting(
        widgetId: image['id'],
        bloc: widget.bloc,
        onUpdateImage: updateImage,
        currentImage: image,
      ),
    );
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "imagePath": controllerImagePath.text,
      "imageDataKey": image['id'] ?? "",
      "height": ConvertUnit.stringToUnit(
          controllerImageHeight.text, selectedUnitHeight!),
      "width": ConvertUnit.stringToUnit(
          controllerImageWidth.text, selectedUnitWidth!),
      "fit": convertFitToString()
    };

    if (image.isNotEmpty) {
      dataSetting.addDataToSetting(image['id'], image);
    }

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 450,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Simple Image'),
          // Tab bar includes two tab: Categories and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD CATEGORIES
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              Row(
                children: [
                  customText('Add new image', mediumSize, textNormal),

                  // BUTTON ADD IMAGE --------------------------------
                  image.isEmpty
                      ? IconButton(
                          onPressed: addImage,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              image.isNotEmpty ? buildImage() : Container(),
              image.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 0),

              // DISPLAY IMAGE ADDED --------------------------------
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        // FIELD IMAGE PATH -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Image url', controllerImagePath),
        SizedBox(height: 20),

        // ADJUST WIDTH -----------------------------------
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Width',
                  controllerImageWidth, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitWidth!,
                changeValueUnitWidth, ['none', 'w'])
          ],
        ),
        SizedBox(height: 20),

        // ADJUST WIDTH AND HEIGHT -----------------------------------
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Height',
                  controllerImageHeight, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitHeight!,
                changeValueUnitHeight, ['none', 'h'])
          ],
        ),
        SizedBox(height: 20),

        // FIT IMAGE OPTIONS -----------------------------------
        Align(
          alignment: Alignment.topLeft,
          child: customText('Fit image', mediumSize, textBold),
        ),
        buildFitOptions(),
      ],
    );
  }

  // This method is used to display a category that added.
  Widget buildImage() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              Text('New image'),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT IMAGE --------------------------------
                  IconButton(
                    onPressed: editImage,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE IMAGE --------------------------------
                  IconButton(
                    onPressed: removeImage,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }

  // This method is used to build a box that allows to choose radio options.
  Row buildFitOptions() {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'Cover',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.cover,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'Contain',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.contain,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'Fill',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.fill,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'None',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.none,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
      ],
    );
  }
}
