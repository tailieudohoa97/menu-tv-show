import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/image_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

class ImageSetting extends StatefulWidget {
  const ImageSetting({super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<ImageSetting> createState() => _ImageSettingState();
}

class _ImageSettingState extends State<ImageSetting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerImageWidth = TextEditingController();
  TextEditingController controllerImageHeight = TextEditingController();
  bool isFixCover = false;
  DataSetting dataSetting = DataSetting();
  dynamic image = {};
  Map<String, dynamic> currentSetting = {};
  int currentIndexTab = 0;
  late TabController _tabController;
  String? selectedUnitWidth, selectedUnitHeight;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updatedImage = {};

    if (currentSetting["imageId"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['imageId']) ?? {};
      updatedImage = data;
    } else {
      updatedImage = {};
    }

    setState(() {
      image = updatedImage;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    _tabController = TabController(length: 2, vsync: this);
    initDataSetting(currentSetting);
    super.initState();

    controllerImageWidth.text =
        ConvertUnit.unitToString(currentSetting["height"]);
    controllerImageHeight.text =
        ConvertUnit.unitToString(currentSetting["width"]);
    isFixCover = bool.parse(currentSetting['fixCover']);

    selectedUnitWidth = ConvertUnit.getUnit(currentSetting["width"]);
    selectedUnitHeight = ConvertUnit.getUnit(currentSetting["height"]);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changeValueUnitWidth(String? value) {
    setState(() {
      selectedUnitWidth = value!;
    });
  }

  void changeValueUnitHeight(String? value) {
    setState(() {
      selectedUnitHeight = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to change use background category title value.
  void changeValueFixCover(bool value) {
    setState(() {
      isFixCover = value;
    });
  }

  // This function is used to add numbers categories.
  void addImage() {
    setState(() {
      final i = BuilderRepository().generateJsonImageData();
      image = i;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeImage() {
    dataSetting.removeDataOnSetting(image['id']);
    setState(() {
      image = {};
    });
  }

  // This function is used to edit a product.
  void editImage() {
    void updateImage(dynamic data) {
      setState(() {
        image = data;
      });
      dataSetting.updateDataOnSetting(image['id'], image);
    }

    customDialog(
      context,
      ImageDataSetting(
        widgetId: image['id'],
        bloc: widget.bloc,
        onUpdateImage: updateImage,
        currentImage: image,
      ),
    );
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "imageId": image['id'] ?? "",
      "fixCover": isFixCover.toString(),
      "height": ConvertUnit.stringToUnit(
          controllerImageHeight.text, selectedUnitHeight!),
      "width": ConvertUnit.stringToUnit(
          controllerImageWidth.text, selectedUnitWidth!),
    };

    if (image.isNotEmpty) {
      dataSetting.addDataToSetting(image['id'], image);
    }

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 360,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Image'),
          // Tab bar includes two tab: Categories and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD CATEGORIES
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              Row(
                children: [
                  customText('Add new image', mediumSize, textNormal),

                  // BUTTON ADD IMAGE --------------------------------
                  image.isEmpty
                      ? IconButton(
                          onPressed: addImage,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              image.isNotEmpty ? buildImage() : Container(),
              image.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 0),

              // DISPLAY IMAGE ADDED --------------------------------
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Width',
                  controllerImageWidth, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitWidth!,
                changeValueUnitWidth, ['none', 'w', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        // ADJUST WIDTH AND HEIGHT -----------------------------------
        Row(
          children: [
            SizedBox(
              width: 200,
              child: ReusableSetting.buildAdjustDouble(context, 'Height',
                  controllerImageHeight, increaseValue, decreaseValue),
            ),
            Spacer(),
            ReusableSetting.buildDropdownMenu(context, selectedUnitHeight!,
                changeValueUnitHeight, ['none', 'h', 'sp'])
          ],
        ),
        SizedBox(height: 20),

        ReusableSetting.buildToggleSwitch(
            context, 'Use fix cover', isFixCover, changeValueFixCover),
      ],
    );
  }

  // This method is used to display a category that added.
  Widget buildImage() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              Text('New image'),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT IMAGE --------------------------------
                  IconButton(
                    onPressed: editImage,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE IMAGE --------------------------------
                  IconButton(
                    onPressed: removeImage,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
