import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/image_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

class FramedImageSetting extends StatefulWidget {
  const FramedImageSetting(
      {super.key,
      required this.widgetId,
      required this.bloc,
      this.onUpdateData,
      this.currentData});

  final String widgetId;
  final BuilderBloc bloc;
  final Function? onUpdateData;
  final Map<String, dynamic>? currentData;

  @override
  State<FramedImageSetting> createState() => _FramedImageSettingState();
}

class _FramedImageSettingState extends State<FramedImageSetting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerWidth = TextEditingController();
  TextEditingController controllerHeight = TextEditingController();
  TextEditingController controllerBorderWidth = TextEditingController();
  TextEditingController controllerBorderRadiusOfImage = TextEditingController();
  TextEditingController controllerImage = TextEditingController();
  TextEditingController controllerImageFrame = TextEditingController();

  Map<String, dynamic> currentSetting = {};
  DataSetting dataSetting = DataSetting();
  dynamic image = {}, imageFrame = {};
  int currentIndexTab = 0;
  late TabController _tabController;
  String? selectedUnitWidth,
      selectedUnitHeight,
      selectedUnitBorderWidth,
      selectedUnitBorderRadius;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updatedImage = {}, updatedImageFrame = {};

    if (currentSetting["imageDataKey"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['imageDataKey']) ?? {};
      updatedImage = data;
    }

    if (currentSetting["imageFrameDataKey"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['imageFrameDataKey']) ??
              {};
      updatedImageFrame = data;
    }

    setState(() {
      image = updatedImage;
      imageFrame = updatedImageFrame;
    });
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);

    if (widget.currentData != null) {
      currentSetting = widget.currentData!;
      initDataSetting(currentSetting);

      controllerImage.text = currentSetting['image'];
      controllerImageFrame.text = currentSetting['imageFrame'];
      controllerBorderWidth.text =
          ConvertUnit.unitToString(currentSetting['borderWidth']);
      controllerBorderRadiusOfImage.text =
          ConvertUnit.unitToString(currentSetting['borderRadiusOfImage']);
      controllerHeight.text =
          ConvertUnit.unitToString(currentSetting["height"]);
      controllerWidth.text = ConvertUnit.unitToString(currentSetting["width"]);

      selectedUnitWidth = ConvertUnit.getUnit(currentSetting["width"]);
      selectedUnitHeight = ConvertUnit.getUnit(currentSetting["height"]);
      selectedUnitBorderWidth =
          ConvertUnit.getUnit(currentSetting['borderWidth']);
      selectedUnitBorderRadius =
          ConvertUnit.getUnit(currentSetting['borderRadiusOfImage']);
    } else {
      currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
      initDataSetting(currentSetting);

      controllerImage.text = currentSetting['image'];
      controllerImageFrame.text = currentSetting['imageFrame'];
      controllerBorderWidth.text =
          ConvertUnit.unitToString(currentSetting['borderWidth']);
      controllerBorderRadiusOfImage.text =
          ConvertUnit.unitToString(currentSetting['borderRadiusOfImage']);
      controllerHeight.text =
          ConvertUnit.unitToString(currentSetting["height"]);
      controllerWidth.text = ConvertUnit.unitToString(currentSetting["width"]);

      selectedUnitWidth = ConvertUnit.getUnit(currentSetting["width"]);
      selectedUnitHeight = ConvertUnit.getUnit(currentSetting["height"]);
      selectedUnitBorderWidth =
          ConvertUnit.getUnit(currentSetting['borderWidth']);
      selectedUnitBorderRadius =
          ConvertUnit.getUnit(currentSetting['borderRadiusOfImage']);
    }
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changeValueUnitWidth(String? value) {
    setState(() {
      selectedUnitWidth = value!;
    });
  }

  void changeValueUnitHeight(String? value) {
    setState(() {
      selectedUnitHeight = value!;
    });
  }

  void changeValueUnitBorderWidth(String? value) {
    setState(() {
      selectedUnitBorderWidth = value!;
    });
  }

  void changeValueUnitBorderRadius(String? value) {
    setState(() {
      selectedUnitBorderRadius = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to add numbers categories.
  void addImage() {
    setState(() {
      final i = BuilderRepository().generateJsonImageData();
      image = i;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeImage() {
    dataSetting.removeDataOnSetting(image['id']);
    setState(() {
      image = {};
    });
  }

  // This function is used to edit a product.
  void editImage() {
    void updateImage(dynamic data) {
      setState(() {
        image = data;
      });
      dataSetting.updateDataOnSetting(image['id'], image);
    }

    customDialog(
      context,
      ImageDataSetting(
        widgetId: image['id'],
        bloc: widget.bloc,
        onUpdateImage: updateImage,
        currentImage: image,
      ),
    );
  }

  // This function is used to add numbers categories.
  void addImageFrame() {
    setState(() {
      final i = BuilderRepository().generateJsonImageData();
      imageFrame = i;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeImageFrame() {
    dataSetting.removeDataOnSetting(imageFrame['id']);
    setState(() {
      imageFrame = {};
    });
  }

  // This function is used to edit a product.
  void editImageFrame() {
    void updateImage(dynamic data) {
      setState(() {
        imageFrame = data;
      });
      dataSetting.updateDataOnSetting(imageFrame['id'], imageFrame);
    }

    customDialog(
      context,
      ImageDataSetting(
        widgetId: imageFrame['id'],
        bloc: widget.bloc,
        onUpdateImage: updateImage,
        currentImage: imageFrame,
      ),
    );
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "imageDataKey": image['id'] ?? "",
      "imageFrameDataKey": imageFrame['id'] ?? '',
      "image": controllerImage.text,
      "imageFrame": controllerImageFrame.text,
      "height":
          ConvertUnit.stringToUnit(controllerHeight.text, selectedUnitHeight!),
      "width":
          ConvertUnit.stringToUnit(controllerWidth.text, selectedUnitWidth!),
      "borderRadiusOfImage": ConvertUnit.stringToUnit(
          controllerBorderRadiusOfImage.text, selectedUnitBorderRadius!),
      "borderWidth": ConvertUnit.stringToUnit(
          controllerBorderWidth.text, selectedUnitBorderWidth!)
    };

    if (widget.onUpdateData != null) {
      widget.onUpdateData!(data);
    }
    if (image.isNotEmpty) {
      dataSetting.addDataToSetting(image['id'], image);
    }
    if (imageFrame.isNotEmpty) {
      dataSetting.addDataToSetting(imageFrame['id'], imageFrame);
    }

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Framed Image'),
          // Tab bar includes two tab: Categories and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD CATEGORIES
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              Row(
                children: [
                  customText('Add new image', mediumSize, textNormal),

                  // BUTTON ADD IMAGE --------------------------------
                  image.isEmpty
                      ? IconButton(
                          onPressed: addImage,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              image.isNotEmpty ? buildImage() : Container(),
              image.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 0),
              Row(
                children: [
                  customText('Add new image frame', mediumSize, textNormal),

                  // BUTTON ADD IMAGE --------------------------------
                  imageFrame.isEmpty
                      ? IconButton(
                          onPressed: addImageFrame,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              imageFrame.isNotEmpty ? buildImageFrame() : Container(),
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        // IMAGE URL -----------------------------------
        ReusableSetting.buildInputSetting(
            context, "Image url", controllerImage),
        SizedBox(height: 20),
        // IMAGE URL -----------------------------------
        ReusableSetting.buildInputSetting(
            context, "Framed image url", controllerImageFrame),
        SizedBox(height: 20),

        // ADJUST WIDTH AND HEIGHT -----------------------------------
        Row(children: [
          SizedBox(
            width: 200,
            child: ReusableSetting.buildAdjustDouble(context, 'Width',
                controllerWidth, increaseValue, decreaseValue),
          ),
          Spacer(),
          ReusableSetting.buildDropdownMenu(
              context, selectedUnitWidth!, changeValueUnitWidth, ['none', 'w', 'sp'])
        ]),
        SizedBox(height: 20),

        Row(children: [
          SizedBox(
            width: 200,
            child: ReusableSetting.buildAdjustDouble(context, 'Height',
                controllerHeight, increaseValue, decreaseValue),
          ),
          Spacer(),
          ReusableSetting.buildDropdownMenu(context, selectedUnitHeight!,
              changeValueUnitHeight, ['none', 'h' , 'sp'])
        ]),
        SizedBox(height: 20),

        Row(children: [
          SizedBox(
            width: 200,
            child: ReusableSetting.buildAdjustDouble(context, 'Border width',
                controllerBorderWidth, increaseValue, decreaseValue),
          ),
          Spacer(),
          ReusableSetting.buildDropdownMenu(context, selectedUnitBorderWidth!,
              changeValueUnitBorderWidth, ['none', 'w', 'h', 'sp'])
        ]),
        SizedBox(height: 20),

        Row(children: [
          SizedBox(
            width: 200,
            child: ReusableSetting.buildAdjustDouble(
                context,
                'Border radius of image',
                controllerBorderRadiusOfImage,
                increaseValue,
                decreaseValue),
          ),
          Spacer(),
          ReusableSetting.buildDropdownMenu(context, selectedUnitBorderRadius!,
              changeValueUnitBorderRadius, ['none', 'w', 'h', 'sp'])
        ]),
      ],
    );
  }

  // This method is used to display a category that added.
  Widget buildImage() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              Text('New image'),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT IMAGE --------------------------------
                  IconButton(
                    onPressed: editImage,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE IMAGE --------------------------------
                  IconButton(
                    onPressed: removeImage,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }

  // This method is used to display a category that added.
  Widget buildImageFrame() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              Text('New image frame'),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT IMAGE --------------------------------
                  IconButton(
                    onPressed: editImageFrame,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE IMAGE --------------------------------
                  IconButton(
                    onPressed: removeImageFrame,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
