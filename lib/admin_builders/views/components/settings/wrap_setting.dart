import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/data/json_data_builder.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class WrapSetting extends StatefulWidget {
  const WrapSetting({super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<WrapSetting> createState() => _WrapSettingState();
}

class _WrapSettingState extends State<WrapSetting> {
  TextEditingController controllerRunSpacing = TextEditingController();
  TextEditingController controllerSpacing = TextEditingController();

  Map<String, dynamic> currentSetting = {};
  String? selectedCrossAxisAlignment;
  String? selectedDirection;
  String? selectedRunAlignment;
  String? selectedVerticalDirection;
  String? selectedAlignment;
  String? selectedClipBehavior;
  String? selectedUnitRunSpacing, selectedUnitSpacing;

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    selectedCrossAxisAlignment = currentSetting['crossAxisAlignment'];
    selectedDirection = currentSetting['direction'];
    selectedRunAlignment = currentSetting['runAlignment'];
    controllerRunSpacing.text =
        ConvertUnit.unitToString(currentSetting["runSpacing"]);
    controllerSpacing.text =
        ConvertUnit.unitToString(currentSetting["spacing"]);
    selectedVerticalDirection = currentSetting['verticalDirection'];
    selectedAlignment = currentSetting['alignment'];
    selectedClipBehavior = currentSetting['clipBehavior'];

    selectedUnitRunSpacing = ConvertUnit.getUnit(currentSetting["runSpacing"]);
    selectedUnitSpacing = ConvertUnit.getUnit(currentSetting["spacing"]);
  }

  // Get list blend mode options.
  List<String> wrapCrossAlignmentOptions =
      JsonDataBuilder.wrapCrossAlignmentOptions;
  // Get list blend mode options.
  List<String> directionOptions = JsonDataBuilder.wrapDirectionOptions;
  List<String> runAlignmentOptions = JsonDataBuilder.mainAxisOptions;
  List<String> verticalOptions = JsonDataBuilder.wrapVerticalOptions;
  List<String> alignmentOptions = JsonDataBuilder.mainAxisOptions;
  List<String> clipBehaviorOptions = JsonDataBuilder.clipBehaviorOptions;

  void changeValueUnitRunSpacing(String? value) {
    setState(() {
      selectedUnitRunSpacing = value!;
    });
  }

  void changeValueUnitSpacing(String? value) {
    setState(() {
      selectedUnitSpacing = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "crossAxisAlignment": selectedCrossAxisAlignment,
      "direction": selectedDirection,
      "runAlignment": selectedRunAlignment,
      "runSpacing": ConvertUnit.stringToUnit(
          controllerRunSpacing.text, selectedUnitRunSpacing!),
      "spacing": ConvertUnit.stringToUnit(
          controllerSpacing.text, selectedUnitSpacing!),
      "verticalDirection": selectedVerticalDirection,
      "alignment": selectedAlignment,
      "clipBehavior": selectedClipBehavior,
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  void changeValueCrossAxisAlignment(String? value) {
    setState(() {
      selectedCrossAxisAlignment = value!;
    });
  }

  void changeValueDirection(String? value) {
    setState(() {
      selectedDirection = value!;
    });
  }

  void changeRunAlignment(String? value) {
    setState(() {
      selectedRunAlignment = value!;
    });
  }

  void changeVerticalDirection(String? value) {
    setState(() {
      selectedVerticalDirection = value!;
    });
  }

  void changeAlignment(String? value) {
    setState(() {
      selectedAlignment = value!;
    });
  }

  void changeClipBehavior(String? value) {
    setState(() {
      selectedClipBehavior = value!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Wrap'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    // ADJUST Run Spacing & Spacing-----------------------------------
                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Run spacing',
                              controllerRunSpacing,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitRunSpacing!,
                            changeValueUnitRunSpacing,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),

                    // ADJUST SPACING -----------------------------------
                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Spacing',
                              controllerSpacing,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitSpacing!,
                            changeValueUnitSpacing,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),

                    // Cross Axis Alignment & Run Alignment-----------------------------------
                    Align(
                      alignment: Alignment.topLeft,
                      child: customText(
                          'Cross Axis Alignment', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(
                        context,
                        selectedCrossAxisAlignment!,
                        changeValueCrossAxisAlignment,
                        wrapCrossAlignmentOptions),
                    SizedBox(height: 20),

                    Align(
                      alignment: Alignment.topLeft,
                      child: customText('Run Alignment', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(
                        context,
                        selectedRunAlignment!,
                        changeRunAlignment,
                        runAlignmentOptions),
                    SizedBox(height: 20),

                    // Vertical Direction & Clip Behavior-----------------------------------
                    Align(
                      alignment: Alignment.topLeft,
                      child: customText(
                          'Vertical Direction', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(
                        context,
                        selectedVerticalDirection!,
                        changeVerticalDirection,
                        verticalOptions),
                    SizedBox(height: 20),

                    Align(
                      alignment: Alignment.topLeft,
                      child: customText('Clip Behavior', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(
                        context,
                        selectedClipBehavior!,
                        changeClipBehavior,
                        clipBehaviorOptions),
                    SizedBox(height: 20),

                    Align(
                      alignment: Alignment.topLeft,
                      child: customText('Direction', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(
                        context,
                        selectedDirection!,
                        changeValueDirection,
                        directionOptions),
                    SizedBox(height: 20),

                    // Alignment & Direction-----------------------------------
                    Align(
                      alignment: Alignment.topLeft,
                      child: customText('Alignment', mediumSize, textBold),
                    ),
                    ReusableSetting.buildDropdownMenu(context,
                        selectedAlignment!, changeAlignment, alignmentOptions),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
