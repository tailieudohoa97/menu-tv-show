import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/category_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/image_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/product_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

// CategoryTitleGridLayout

class CategoryTitleGrid1Setting extends StatefulWidget {
  const CategoryTitleGrid1Setting(
      {super.key, required this.widgetId, required this.bloc});

  final String widgetId;
  final BuilderBloc bloc;

  @override
  State<CategoryTitleGrid1Setting> createState() =>
      _CategoryTitleGrid1SettingState();
}

class _CategoryTitleGrid1SettingState extends State<CategoryTitleGrid1Setting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerNumberProducts = TextEditingController();
  TextEditingController controllerNumberImages = TextEditingController();

  TextEditingController controllerColorPrice = TextEditingController();
  TextEditingController controllerColorProductName = TextEditingController();
  TextEditingController controllerColorDescription = TextEditingController();
  TextEditingController controllerColorCategoryTitle = TextEditingController();
  TextEditingController controllerColorBgCatTitle = TextEditingController();
  TextEditingController controllerPathBgCat = TextEditingController();

  bool isFullWidth = false, isBackgroundCategoryTitle = false;
  List<dynamic> listProducts = [], listImages = [];
  dynamic category = {}, imageBg = {};

  Map<String, dynamic> currentSetting = {};
  DataSetting dataSetting = DataSetting();
  int currentIndexTab = 0;
  late TabController _tabController;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    List<dynamic> updatedListProducts = [], updatedListImages = [];
    dynamic updatedCategory = {}, updatedImageBg = {};

    if (currentSetting["imageId"].isNotEmpty) {
      var data =
          dataSetting.getDataFromSetting(currentSetting['imageId']) ?? {};
      updatedImageBg = data;
    }

    if (currentSetting["dataList"].isNotEmpty) {
      final dataList = currentSetting["dataList"];
      // get list product from data setting
      if (dataList.containsKey("product")) {
        for (var p in dataList['product']) {
          var data = dataSetting.getDataFromSetting(p);
          updatedListProducts.add(data);
        }
      }

      // get list image from data setting
      if (dataList.containsKey("image")) {
        for (var i in dataList['image']) {
          var data = dataSetting.getDataFromSetting(i);
          updatedListImages.add(data);
        }
      }

      // get category from data setting
      if (dataList.containsKey("category") && dataList['category'].isNotEmpty) {
        var data = dataSetting.getDataFromSetting(dataList["category"]);
        updatedCategory = data;
      }
    } else {
      updatedListProducts = [];
      updatedListImages = [];
      updatedCategory = {};
    }

    setState(() {
      listProducts = updatedListProducts;
      listImages = updatedListImages;
      category = updatedCategory;
      imageBg = updatedImageBg;
    });
  }

  @override
  void initState() {
    currentSetting = widget.bloc.state.currentSetting['widgetSetting'];
    super.initState();
    initDataSetting(currentSetting);
    _tabController = TabController(length: 2, vsync: this);

    controllerColorCategoryTitle.text = currentSetting['colorCategoryTitle'];
    controllerColorBgCatTitle.text =
        currentSetting['colorBackgroundCategoryTitle'];
    controllerColorPrice.text = currentSetting["colorPrice"];
    controllerColorProductName.text = currentSetting["colorProductName"];
    controllerColorDescription.text = currentSetting["colorDescription"];
    controllerPathBgCat.text = currentSetting["pathImageBgCategoryTitle"];
    isFullWidth = bool.parse(currentSetting["useFullWidthCategoryTitle"]);
    isBackgroundCategoryTitle =
        bool.parse(currentSetting["useBackgroundCategoryTitle"]);
    controllerNumberProducts.text = '1';
    controllerNumberImages.text = '1';
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to change use full width category title value.
  void changeValueFullWidth(bool value) {
    setState(() {
      isFullWidth = value;
    });
  }

  // This function is used to change use background category title value.
  void changeValueBackgroundCategory(bool value) {
    setState(() {
      isBackgroundCategoryTitle = value;
    });
  }

  // This function is used to add numbers categories.
  void addProducts() {
    setState(() {
      for (var i = 0; i < int.parse(controllerNumberProducts.text); i++) {
        final product = BuilderRepository().generateJsonProductData();
        listProducts.add(product);
      }
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeProduct(int index) {
    // update dataSetting.
    dataSetting.removeDataOnSetting(listProducts[index]['id']);
    setState(() {
      listProducts.removeAt(index);
    });
  }

  // This function is used to edit a product.
  void editProduct(int index) {
    void updateProduct(dynamic data) {
      setState(() {
        listProducts[index] = data;
      });
      dataSetting.updateDataOnSetting(
          listProducts[index]['id'], listProducts[index]);
    }

    customDialog(
      context,
      ProductDataSetting(
        widgetId: listProducts[index]['id'],
        bloc: widget.bloc,
        onUpdateProduct: updateProduct,
        currentProduct: listProducts[index],
      ),
    );
  }

  // This function is used to add numbers products.
  void addImages() {
    setState(() {
      for (var i = 0; i < int.parse(controllerNumberImages.text); i++) {
        final image = BuilderRepository().generateJsonImageData();
        listImages.add(image);
      }
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeImage(int index) {
    // update dataSetting.
    dataSetting.removeDataOnSetting(listImages[index]['id']);
    setState(() {
      listImages.removeAt(index);
    });
  }

  // This function is used to edit a product.
  void editImage(int index) {
    void updateImage(dynamic data) {
      setState(() {
        listImages[index] = data;
      });
      dataSetting.updateDataOnSetting(
          listImages[index]['id'], listImages[index]);
    }

    customDialog(
      context,
      ImageDataSetting(
        widgetId: listImages[index]['id'],
        bloc: widget.bloc,
        onUpdateImage: updateImage,
        currentImage: listImages[index],
      ),
    );
  }

  // This function is used to add numbers products.
  void addCategory() {
    setState(() {
      final cat = BuilderRepository().generateJsonCategoryData();
      category = cat;
    });
  }

  // This function is used to edit a category.
  void editCategory() {
    void updateCategory(dynamic data) {
      setState(() {
        category = data;
      });
      dataSetting.updateDataOnSetting(category["id"], category);
    }

    customDialog(
      context,
      CategoryDataSetting(
        widgetId: category['id'],
        bloc: widget.bloc,
        onUpdateCategory: updateCategory,
        currentCategory: category,
      ),
    );
  }

  // This function is used to remove category added.
  void removeCategory() {
    // update dataSetting.
    dataSetting.removeDataOnSetting(category["id"]);
    setState(() {
      category = {};
    });
  }

  // This function is used to add numbers products.
  void addImageBg() {
    setState(() {
      final i = BuilderRepository().generateJsonImageData();
      imageBg = i;
    });
  }

  // This function is used to edit a category.
  void editImageBg() {
    void updateImage(dynamic data) {
      setState(() {
        imageBg = data;
      });
      dataSetting.updateDataOnSetting(imageBg["id"], imageBg);
    }

    customDialog(
      context,
      ImageDataSetting(
        widgetId: imageBg['id'],
        bloc: widget.bloc,
        onUpdateImage: updateImage,
        currentImage: imageBg,
      ),
    );
  }

  // This function is used to remove category added.
  void removeImageBg() {
    // update dataSetting.
    dataSetting.removeDataOnSetting(imageBg["id"]);
    setState(() {
      imageBg = {};
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    dynamic dataList = {
      'category': category['id'] ?? "",
      'product': listProducts.map((e) => e['id']).toList(),
      'image': listImages.map((e) => e['id']).toList()
    };

    if (category.isNotEmpty) {
      dataSetting.addDataToSetting(category['id'], category);
    }
    if (imageBg.isNotEmpty) {
      dataSetting.addDataToSetting(imageBg['id'], imageBg);
    }
    for (var p in listProducts) {
      dataSetting.addDataToSetting(p['id'], p);
    }
    for (var i in listImages) {
      dataSetting.addDataToSetting(i['id'], i);
    }

    Map<String, dynamic> data = {
      "dataList": dataList,
      "useBackgroundCategoryTitle": isBackgroundCategoryTitle.toString(),
      "useFullWidthCategoryTitle": isFullWidth.toString(),
      "imageId": imageBg["id"] ?? "",
      "pathImageBgCategoryTitle": controllerPathBgCat.text,
      "colorCategoryTitle": controllerColorCategoryTitle.text,
      "colorPrice": controllerColorPrice.text,
      "colorProductName": controllerColorProductName.text,
      "colorDescription": controllerColorDescription.text,
      "colorBackgroundCategoryTitle": controllerColorBgCatTitle.text,
    };

    widget.bloc.add(UpdateModuleEvent(widgetId: widget.widgetId, model: data));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 520,
      height: 520,
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting Category Grid Layout 1'),
          // Tab bar includes two tab: Categories and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD CATEGORIES
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              // ADD NEW CATEGORY -----------------------------------
              Row(
                children: [
                  customText('Add a category', mediumSize, textNormal),
                  const SizedBox(width: 20),

                  // BUTTON ADD PRODUCTS --------------------------------
                  category.isEmpty
                      ? IconButton(
                          onPressed: addCategory,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              category.isNotEmpty ? buildCategory() : Container(),
              category.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 10),

              // ADD NEW CATEGORY -----------------------------------
              Row(
                children: [
                  customText('Add a background image', mediumSize, textNormal),
                  const SizedBox(width: 20),

                  // BUTTON ADD PRODUCTS --------------------------------
                  imageBg.isEmpty
                      ? IconButton(
                          onPressed: addImageBg,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              imageBg.isNotEmpty ? buildImageBg() : Container(),
              imageBg.isNotEmpty ? SizedBox(height: 20) : SizedBox(height: 10),

              Row(
                children: [
                  customText('List product: ', mediumSize, textNormal),
                  Spacer(),
                  customText('Number products: ${listProducts.length}',
                      smallSize, textNormal),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  // ADJUST NUMBER PRODUCT NEED ADD -----------------------------------
                  SizedBox(
                    width: 200,
                    child: Expanded(
                      child: ReusableSetting.buildAdjustNumber(
                          context,
                          'Number products',
                          controllerNumberProducts,
                          increaseValue,
                          decreaseValue),
                    ),
                  ),
                  const SizedBox(width: 20),

                  // BUTTON ADD PRODUCTS --------------------------------
                  IconButton(
                    onPressed: addProducts,
                    icon: Icon(Icons.add),
                    color: Colors.blue,
                  )
                ],
              ),
              // DISPLAY LIST PRODUCTS ADDED --------------------------------
              buildListProduct(),
              SizedBox(height: 20),

              Row(
                children: [
                  customText('List image: ', mediumSize, textNormal),
                  Spacer(),
                  customText('Number images: ${listImages.length}', smallSize,
                      textNormal),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  // ADJUST NUMBER PRODUCT NEED ADD -----------------------------------
                  SizedBox(
                    width: 200,
                    child: Expanded(
                      child: ReusableSetting.buildAdjustNumber(
                          context,
                          'Number images',
                          controllerNumberImages,
                          increaseValue,
                          decreaseValue),
                    ),
                  ),
                  const SizedBox(width: 20),

                  // BUTTON ADD PRODUCTS --------------------------------
                  IconButton(
                    onPressed: addImages,
                    icon: Icon(Icons.add),
                    color: Colors.blue,
                  )
                ],
              ),
              // DISPLAY LIST PRODUCTS ADDED --------------------------------
              buildListImages()
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        // ADJUST COLOR CATEGORY TITLE -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color category title', controllerColorCategoryTitle),
        SizedBox(height: 20),

        // ADJUST COLOR BACKGROUND CATEGORY TITLE -----------------------------------
        ReusableSetting.buildInputSetting(context,
            'Color background category title', controllerColorBgCatTitle),
        SizedBox(height: 20),

        // ADJUST COLOR PRICE -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color price', controllerColorPrice),
        SizedBox(height: 20),

        // ADJUST COLOR NAME PRODUCT -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color product name', controllerColorProductName),
        SizedBox(height: 20),

        // ADJUST COLOR DESCRIPTION -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Color description', controllerColorDescription),
        SizedBox(height: 20),

        // ADJUST PATH BACKGROUND -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Path background category', controllerPathBgCat),
        SizedBox(height: 20),

        // ADJUST USE BACKGROUND CATEGORY TITLE -----------------------------------
        ReusableSetting.buildToggleSwitch(
            context,
            'Use background category title',
            isBackgroundCategoryTitle,
            changeValueBackgroundCategory),
        SizedBox(height: 20),

        // ADJUST USE FULL WIDTH CATEGORY TITLE -----------------------------------
        ReusableSetting.buildToggleSwitch(context,
            'Use full width category title', isFullWidth, changeValueFullWidth),
      ],
    );
  }

  // This method is used to display list products that added.
  Widget buildListProduct() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: SingleChildScrollView(
        child: ListView.separated(
          shrinkWrap: true,
          itemCount: listProducts.length,
          separatorBuilder: (context, index) => SizedBox(height: 10),
          itemBuilder: (context, index) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.grey[300],
              ),
              child: Row(
                children: [
                  listProducts[index]['name'].isEmpty
                      ? Text('Product ${index + 1}')
                      : Text(listProducts[index]['name']),
                  Spacer(),
                  Row(
                    children: [
                      // BUTTON EDIT PRODUCT --------------------------------
                      IconButton(
                        onPressed: () => editProduct(index),
                        icon: Icon(Icons.edit, size: 18),
                        color: Colors.teal,
                      ),

                      // BUTTON REMOVE PRODUCT --------------------------------
                      IconButton(
                        onPressed: () => removeProduct(index),
                        icon: Icon(Icons.delete, size: 18),
                        color: Colors.red,
                      ),
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  // This method is used to display category that added.
  Widget buildCategory() {
    return Container(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              category.isEmpty || category['name'].isEmpty
                  ? Text('New category')
                  : Text(category['name']),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT CATEGORY --------------------------------
                  IconButton(
                    onPressed: editCategory,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE CATEGORY --------------------------------
                  IconButton(
                    onPressed: removeCategory,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }

  // IMAGES
  // This method is used to display list products that added.
  Widget buildListImages() {
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: SingleChildScrollView(
        child: ListView.separated(
          shrinkWrap: true,
          itemCount: listImages.length,
          separatorBuilder: (context, index) => SizedBox(height: 10),
          itemBuilder: (context, index) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.grey[300],
              ),
              child: Row(
                children: [
                  Text('Image ${index + 1}'),
                  Spacer(),
                  Row(
                    children: [
                      // BUTTON EDIT PRODUCT --------------------------------
                      IconButton(
                        onPressed: () => editImage(index),
                        icon: Icon(Icons.edit, size: 18),
                        color: Colors.teal,
                      ),

                      // BUTTON REMOVE PRODUCT --------------------------------
                      IconButton(
                        onPressed: () => removeImage(index),
                        icon: Icon(Icons.delete, size: 18),
                        color: Colors.red,
                      ),
                    ],
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  // This method is used to display category that added.
  // This method is used to display a category that added.
  Widget buildImageBg() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              Text('New background image'),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT IMAGE --------------------------------
                  IconButton(
                    onPressed: editImageBg,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE IMAGE --------------------------------
                  IconButton(
                    onPressed: removeImageBg,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
