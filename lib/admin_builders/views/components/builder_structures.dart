import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/repo/local_db.dart';

class BuilderStructures extends StatefulWidget {
  final Map<String, dynamic> json;
  final Function handleCloseStructures;
  final Function saveOpenStructures;

  const BuilderStructures(
      {super.key,
      required this.json,
      required this.handleCloseStructures,
      required this.saveOpenStructures});

  @override
  State<BuilderStructures> createState() => _BuilderStructuresState();
}

class _BuilderStructuresState extends State<BuilderStructures> {
  List<dynamic> expandedLevels = [];
  bool isAllExpanded = false;
  static const indentSize = 15.0;

  @override
  void initState() {
    super.initState();
    restoreExpandedLevels();
  }

  // get states expand levels from local
  void restoreExpandedLevels() async {
    final savedLevels = await LocalDB.getDataFromLocal("expandedLevels");

    setState(() {
      dynamic tmp = jsonDecode(savedLevels);
      tmp = tmp.map((e) => int.parse(e)).toList();
      expandedLevels = tmp;
    });
  }

  // save current state expand levels to local
  void saveExpandedLevels() async {
    final levelsToSave =
        expandedLevels.map((level) => level.toString()).toList();

    await LocalDB.saveDataToLocal('expandedLevels', jsonEncode(levelsToSave));
  }

  // add or remove level from list expand levels
  void handleExpandedLevels(bool isExpanded, int level) {
    setState(() {
      isExpanded ? expandedLevels.remove(level) : expandedLevels.add(level);
      saveExpandedLevels();
    });
  }

  // open expand all level
  void handleExpandAllLevels() {
    setState(() {
      if (isAllExpanded) {
        expandedLevels
            .clear(); // if expand all true then clear all levels that is expanded.
      } else {
        // update list expand levels to expand all
        final maxLevel = calculateMaxLevel(widget.json);
        expandedLevels = List.generate(maxLevel + 1, (index) => index);
      }

      isAllExpanded = !isAllExpanded; // revere state expand : expand / collapse
      saveExpandedLevels();
    });
  }

  // get total levels of json.
  int calculateMaxLevel(Map<String, dynamic> json) {
    int maxLevel = 0;

    void mapLevelsJson(Map<String, dynamic> json, int level) {
      if (json.containsKey('widgetSetting')) {
        final widgetSetting = json['widgetSetting'];

        if (widgetSetting.containsKey('child')) {
          mapLevelsJson(widgetSetting['child'], level + 1);
        }

        if (widgetSetting.containsKey('children')) {
          final children = widgetSetting['children'] as List<dynamic>;
          for (int i = 0; i < children.length; i++) {
            mapLevelsJson(children[i], level + 1);
          }
        }

        if (widgetSetting.containsKey('productRows')) {
          final productRows = widgetSetting['productRows'] as List<dynamic>;
          for (int i = 0; i < productRows.length; i++) {
            mapLevelsJson(productRows[i], level + 1);
          }
        }

        if (widgetSetting.containsKey('categoryItems')) {
          final categoryItems = widgetSetting['categoryItems'] as List<dynamic>;
          for (int i = 0; i < categoryItems.length; i++) {
            mapLevelsJson(categoryItems[i], level + 1);
          }
        }

        if (widgetSetting.containsKey('menuItems')) {
          final menuItems = widgetSetting['menuItems'] as List<dynamic>;
          for (int i = 0; i < menuItems.length; i++) {
            mapLevelsJson(menuItems[i], level + 1);
          }
        }

        if (widgetSetting.containsKey('categoryItemRows')) {
          final categoryItemRows =
              widgetSetting['categoryItemRows'] as List<dynamic>;
          for (int i = 0; i < categoryItemRows.length; i++) {
            mapLevelsJson(categoryItemRows[i], level + 1);
          }
        }

        maxLevel = level > maxLevel ? level : maxLevel;
      }
    }

    mapLevelsJson(json, 0);

    return maxLevel;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 2, color: const Color(0xFFEFEFEF))),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.maxFinite,
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(width: 1, color: Colors.black))),
              padding: const EdgeInsets.only(top: 10, bottom: 10, left: 5),
              child: Row(
                children: [
                  TextButton(
                    onPressed: handleExpandAllLevels,
                    child: Text(isAllExpanded ? 'Collapse All' : 'Expand All'),
                  ),
                  TextButton(
                      onPressed: () {
                        widget.handleCloseStructures();
                        widget.saveOpenStructures();
                      },
                      child: Text("Close"))
                ],
              ),
            ),

            // structure template
            widget.json != {} ? buildWidget(widget.json, 0, 0) : Container()
          ],
        ),
      ),
    );
  }

  // widget build levels of widget tree
  Widget buildWidget(Map<String, dynamic> json, double indent, int level) {
    indent = 15.0;
    final headerName = json['headerName'] ?? "";
    // hidden icon expand if current level don't have child and children
    final hasChildren = json.containsKey('widgetSetting') &&
        (json['widgetSetting'].containsKey('child') ||
            json['widgetSetting'].containsKey('children') ||
            json['widgetSetting'].containsKey('productRows') ||
            json['widgetSetting'].containsKey('categoryItems') ||
            json['widgetSetting'].containsKey('menuItems') ||
            json['widgetSetting'].containsKey('categoryItemRows'));

    final childrenWidgets = <Widget>[];

    if (json.containsKey('widgetSetting')) {
      final widgetSetting = json['widgetSetting'];

      if (widgetSetting.containsKey('child') &&
          widgetSetting['child'].isNotEmpty) {
        final child = widgetSetting['child'];
        childrenWidgets.add(buildWidget(child, indent + indentSize, level + 1));
      }

      if (widgetSetting.containsKey('children') &&
          widgetSetting['children'].isNotEmpty) {
        final children = widgetSetting['children'] as List<dynamic>;
        for (int i = 0; i < children.length; i++) {
          final child = children[i];
          childrenWidgets.add(
            buildWidget(child, indent + indentSize, level + 1),
          );
        }
      }

      if (widgetSetting.containsKey('productRows') &&
          widgetSetting['productRows'].isNotEmpty) {
        final productRows = widgetSetting['productRows'] as List<dynamic>;
        for (int i = 0; i < productRows.length; i++) {
          final child = productRows[i];
          childrenWidgets.add(
            buildWidget(child, indent + indentSize, level + 1),
          );
        }
      }

      if (widgetSetting.containsKey('categoryItems') &&
          widgetSetting['categoryItems'].isNotEmpty) {
        final categoryItems = widgetSetting['categoryItems'] as List<dynamic>;
        for (int i = 0; i < categoryItems.length; i++) {
          final child = categoryItems[i];
          childrenWidgets.add(
            buildWidget(child, indent + indentSize, level + 1),
          );
        }
      }

      if (widgetSetting.containsKey('menuItems') &&
          widgetSetting['menuItems'].isNotEmpty) {
        final menuItems = widgetSetting['menuItems'] as List<dynamic>;
        for (int i = 0; i < menuItems.length; i++) {
          final child = menuItems[i];
          childrenWidgets.add(
            buildWidget(child, indent + indentSize, level + 1),
          );
        }
      }

      if (widgetSetting.containsKey('categoryItemRows') &&
          widgetSetting['categoryItemRows'].isNotEmpty) {
        final categoryItemRows =
            widgetSetting['categoryItemRows'] as List<dynamic>;
        for (int i = 0; i < categoryItemRows.length; i++) {
          final child = categoryItemRows[i];
          childrenWidgets.add(
            buildWidget(child, indent + indentSize, level + 1),
          );
        }
      }
    }

    // set padding left 10 with level children
    final indentedChildren = Padding(
      padding: EdgeInsets.only(left: indent),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: childrenWidgets,
      ),
    );

    // check current level is expanding ?
    final isExpanded = expandedLevels.contains(level) && hasChildren;

    // display list level widget.
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () => handleExpandedLevels(isExpanded, level),
          child: Row(
            children: [
              Icon(
                isExpanded
                    ? Icons.arrow_drop_down
                    : !hasChildren
                        ? Icons.arrow_drop_down
                        : Icons.arrow_right,
                color: !hasChildren ? Colors.transparent : Colors.black,
              ),
              Flexible(
                  flex: 1,
                  child: Text(
                    headerName,
                    overflow: TextOverflow.ellipsis,
                    // [112304TIN] set text color is black
                    style: TextStyle(fontSize: 14, color: Colors.black87),
                  )),
            ],
          ),
        ),

        // show children levels with padding left 10
        if (isExpanded) indentedChildren,
      ],
    );
  }
}
