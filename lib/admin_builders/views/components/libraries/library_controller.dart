import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/categories/category_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/categories/category_event.dart';
import 'package:tv_menu/admin_builders/bloc/categories/category_state.dart';
import 'package:tv_menu/admin_builders/views/components/libraries/module_libraries.dart';

class LibraryController extends StatefulWidget {
  const LibraryController({Key? key, required this.bloc}) : super(key: key);
  // Get from BuilderLayout widget.
  final BuilderBloc bloc;

  @override
  State<LibraryController> createState() => _LibraryControllerState();
}

class _LibraryControllerState extends State<LibraryController> {
  // [092301TREE]

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: BlocProvider(
        create: (context) => CategoryBloc()
          ..add(SelectedCategoryEvent(cat: 'All'))
          ..add(GetLayoutByCategoryEvent(cat: 'All')),
        child: BlocBuilder<CategoryBloc, CategoryState>(
          builder: (context, state) {
            if (state.isLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            final catBloc = BlocProvider.of<CategoryBloc>(context);

            return ModuleLibraries(bloc: widget.bloc, catBloc: catBloc);
          },
        ),
      ),
    );
  }
}
