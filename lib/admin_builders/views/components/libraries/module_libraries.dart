import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/bloc/categories/category_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/categories/category_event.dart';
import 'package:tv_menu/admin_builders/bloc/categories/category_state.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class ModuleLibraries extends StatefulWidget {
  const ModuleLibraries({super.key, required this.bloc, required this.catBloc});
  // Get from LibraryController widget.
  final BuilderBloc bloc;
  final CategoryBloc catBloc;

  @override
  State<ModuleLibraries> createState() => _ModuleLibrariesState();
}

class _ModuleLibrariesState extends State<ModuleLibraries> {
  // This variable is used to get listLayout state in CategoryState.
  late dynamic listLayout;
  @override
  void initState() {
    listLayout = widget.catBloc.state.listLayout;
    super.initState();
  }

  // This function is used to choose layout and use it to edit.
  void chooseLayout(int index) {
    final data = listLayout[index]['layoutSettings'];
    final dataSetting = listLayout[index]['dataSetting'];
    final radioSetting = listLayout[index]['RatioSetting'];
    widget.bloc.add(ChangeSettingEvent(
        settings: data,
        dataSettings: dataSetting,
        radioSettings: radioSetting));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
      ),
      child: Column(children: [
        ReusableSetting.buildHeaderSetting(context, 'Load From Library'),
        Expanded(
          child: Container(
            padding: EdgeInsets.all(30),
            child: Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          customText(
                              'Categories', largeSize, textBold, kBlackColor),
                          SizedBox(height: 20),
                          buildMenuItem('All', widget.catBloc.state, context),
                          buildMenuItem('Food', widget.catBloc.state, context),
                          buildMenuItem(
                              'Business', widget.catBloc.state, context),
                          buildMenuItem(
                              'Education', widget.catBloc.state, context),
                        ])),
                SizedBox(width: 50),
                Expanded(
                  flex: 4,
                  child: Container(
                    alignment: Alignment.topLeft,
                    decoration: BoxDecoration(color: Colors.white),
                    child: SingleChildScrollView(
                      child: GridView.builder(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            crossAxisSpacing: 25,
                            mainAxisSpacing: 25,
                            childAspectRatio: 3 / 2),
                        shrinkWrap: true,
                        itemCount: listLayout.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () => chooseLayout(index),
                            child: Container(
                                alignment: Alignment.center,
                                height: 100,
                                child: GridTile(
                                    footer: Material(
                                      color: Colors.transparent,
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.vertical(
                                              bottom: Radius.circular(4))),
                                      clipBehavior: Clip.antiAlias,
                                      child: GridTileBar(
                                        backgroundColor: Colors.black45,
                                        title: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Flexible(
                                              child: Text(
                                                '${listLayout[index]['layoutName']}',
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 2,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13),
                                              ),
                                            ),
                                            Text('Free',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 13,
                                                    fontWeight:
                                                        FontWeight.bold))
                                          ],
                                        ),
                                      ),
                                    ),
                                    child: Image.network(
                                        listLayout[index]['layoutImage'],
                                        fit: BoxFit.cover))),
                          );
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }

  // This method is used to build menu items that includes handler functions and name of them.
  Widget buildMenuItem(String cat, CategoryState state, BuildContext context) {
    // This variable is used to store current menu item that is selecting.
    String currentState = state.selectedCat;

    return Container(
      height: 50,
      decoration: BoxDecoration(
        // Check if current state is title of menu item then highlight color of that menu item
        color: cat == currentState ? Colors.grey[300] : null,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Padding(
        padding: const EdgeInsets.all(4),
        child: ListTile(
          dense: true,
          title: Text(cat, style: TextStyle(fontSize: 16)),
          onTap: () async {
            widget.catBloc.add(SelectedCategoryEvent(cat: cat));
            await Future.delayed(const Duration(milliseconds: 500));
            widget.catBloc.add(GetLayoutByCategoryEvent(cat: cat));
          },
        ),
      ),
    );
  }
}
