import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/bloc/drafts/draft_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/drafts/draft_event.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/widget_helper.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';
import 'package:tv_menu/admin_builders/repo/local_db.dart';
import 'package:tv_menu/admin_builders/views/components/inserts/drafts_setting.dart';
import 'package:tv_menu/admin_builders/views/components/libraries/library_controller.dart';
import 'package:tv_menu/bloc/templates/templates_bloc.dart';
import 'package:tv_menu/bloc/templates/templates_event.dart';
import 'package:tv_menu/models/template.dart';

class BuilderLayout extends StatefulWidget {
  const BuilderLayout(
      {super.key,
      required this.bloc,
      required this.draftBloc,
      required this.handleOpenStructures,
      required this.saveOpenStructures,
      required this.template});

  // Get from builder controller
  final Template template;
  final BuilderBloc bloc;
  final DraftBloc draftBloc;
  final Function handleOpenStructures;
  final Function saveOpenStructures;

  @override
  State<BuilderLayout> createState() => _BuilderLayoutState();
}

class _BuilderLayoutState extends State<BuilderLayout> {
  // [091723TREE]
  // This variable is used to init states of builder states.
  late dynamic state;
  final itemKey = GlobalKey();

  // scroll to last module in settings
  Future<void> scrollToModule() async {
    await Future.delayed(const Duration(
        milliseconds:
            500)); // Đợi một khoảng thời gian sau khi widget build hoàn toàn
    final context = itemKey.currentContext;
    if (context != null) {
      await Scrollable.ensureVisible(context);
    }
  }

  @override
  void initState() {
    state = widget.bloc.state;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      SchedulerBinding.instance!.addPostFrameCallback((_) {
        scrollToModule();
      });
    });
    super.initState();
  }

  /* This function checks if:
     - Setting is empty then requires add new container.
     - Setting is exist then can clear current layout or move setting to drafts.   
  */
  void handleStartEditing() {
    // create a new container
    if (state.settings.isEmpty) {
      final container = BuilderRepository().generateJsonContainer();
      widget.bloc
          .add(AddModuleEvent(widgetId: container['id'], model: container));
    } else {
      // clear all
      customAlertDialog(
          context,
          'Clear layout',
          'Clear',
          'Do you want to clear the settings?\nIf you want to keep the draft for reuse, please choose \'Move to draft\'.',
          () => widget.bloc.add(ClearModulesEvent()),
          'Move to draft',
          () => {
                // move layout to draft's user
                widget.draftBloc.add(
                    MoveToDraftEvent(settings: jsonDecode(state.settings))),
                widget.bloc.add(ClearModulesEvent()),
              });
    }
  }

  // This function is used to view interface of template after edited.
  void viewUiTemplate(state) => showDialog(
        context: context,
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(30.0),
            child: Card(
                color: Colors.transparent,
                child: state.settings.isEmpty
                    ? Text('You do not have any builder template!!!')
                    : Container()),
          );
        },
      );

  // This function is used to open list drafts that saved.
  void openDraftSettings() => customDialog(
      context, DraftsSetting(draftBloc: widget.draftBloc, bloc: widget.bloc));

  // This function is used to open layout templates library.
  void openLayoutLibrary() =>
      customDialog(context, LibraryController(bloc: widget.bloc));

  // This function is used to open layout templates library.
  void openWidgetTree() {
    widget.handleOpenStructures();
    widget.saveOpenStructures();
  }

  void saveSettings() async {
    customAlertDialog(context, "Save settings", 'Confirm',
        'Do you want update dataSetting and widgetSetting ?', () async {
      final dataSetting = await LocalDB.getDataFromLocal("dataSetting");
      // remove child, children empty in the settings
      final widgetSetting = await LocalDB.getDataFromLocal("widget_settings");
      dynamic tmp;
      if (widgetSetting.isNotEmpty) {
        tmp =
            BuilderRepository().removeEmptySettings(jsonDecode(widgetSetting));
      }
      final dataWidgetSetting = jsonEncode(tmp ?? "");

      final radioSetting = await LocalDB.getDataFromLocal("radioSetting");

      if (dataSetting.isNotEmpty &&
          dataWidgetSetting.isNotEmpty &&
          radioSetting.isNotEmpty) {
        BlocProvider.of<TemplateBloc>(context).add(UpdateTemplateEvent(
            template: Template(
          id: widget.template.id,
          templateName: widget.template.templateName,
          templateSlug: widget.template.templateSlug,
          categoryId: widget.template.categoryId,
          dataSetting: dataSetting,
          ratioSetting: radioSetting,
          widgetSetting: dataWidgetSetting,
          isDeleted: false,
        )));
      } else {
        print("Can not update with empty values.");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final state = widget.bloc.state;
    return Column(
      children: [
        Expanded(
            child: SingleChildScrollView(
                padding: EdgeInsets.all(15),
                child: state.settings.isNotEmpty
                    ? WidgetHelper().renderWidgetLayout(
                        jsonDecode(state.settings), itemKey,
                        currentModuleId: state.currentModuleId)
                    : Text(
                        'Let make a new template !',
                        style: TextStyle(color: Colors.black87),
                      ))),
        Container(
          padding: EdgeInsets.all(16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // Open layouts library.
              buildActionsButton(openLayoutLibrary, Icons.open_in_browser),
              // Add or clear current layout.
              buildActionsButton(handleStartEditing, Icons.add, 'add_or_clear'),
              // Save current layout.
              buildActionsButton(saveSettings, Icons.save),
              // Open widgets tree
              buildActionsButton(openWidgetTree, Icons.folder),
              // View ui templates (need pass viewUiTemplate).
              // buildActionsButton(() {
              //   // customDialog(
              //   //   context,
              //   //   ModuleHelper.renderWidget(JsonDataBuilder.testTemplate),
              //   // );
              // }, Icons.visibility),
              // View drafts.
              buildActionsButton(openDraftSettings, Icons.drafts),
            ],
          ),
        )
      ],
    );
  }

  // This method is used to build buttons, each button includes handler function and icon of them.
  ElevatedButton buildActionsButton(Function handleAction, IconData icon,
      [String check = '']) {
    return ElevatedButton(
      onPressed: () => handleAction(),
      style: ElevatedButton.styleFrom(
        backgroundColor: check == 'add_or_clear'
            ? state.settings.isEmpty
                ? Color(0xFF6c2eb9)
                : kRedColor
            : Color(0xFF6c2eb9),
        shape: CircleBorder(),
        padding: EdgeInsets.all(20),
      ),
      child: Icon(
        check == 'add_or_clear'
            ? state.settings.isEmpty
                ? icon
                : Icons.delete
            : icon,
        size: 20,
        color: Colors.white,
      ),
    );
  }
}
