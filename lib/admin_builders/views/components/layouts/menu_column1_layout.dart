import 'package:tv_menu/admin_builders/models/is_expand_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/build_module_option.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/header_layout.dart';

class MenuColumn1Layout extends StatefulWidget {
  const MenuColumn1Layout(
      {super.key,
      this.listWidgetLayout,
      required this.widgetId,
      required this.moduleName});

  final List<Widget>? listWidgetLayout;
  final String widgetId;
  final String moduleName;

  @override
  State<MenuColumn1Layout> createState() => _MenuColumn1LayoutState();
}

class _MenuColumn1LayoutState extends State<MenuColumn1Layout> {
  /* [091423TREE]
  This widget is a column template.
  */
  List<Widget> children = [];
  BuilderBloc? bloc;
  ExpandedList expandedList = ExpandedList();
  dynamic isExpanded = true;

  @override
  void initState() {
    children = widget.listWidgetLayout ?? [];
    super.initState();
    bloc = BlocProvider.of<BuilderBloc>(context);
    getExpandedModule();
  }

  // get state expanded module from local
  void getExpandedModule() async {
    await expandedList.fetchExpandedList();
    var data = expandedList.getExpandedList(widget.widgetId);
    setState(() {
      isExpanded = data ?? true;
    });
  }

  // show / hide children in modules
  void toggleExpansion() async {
    setState(() {
      isExpanded = !isExpanded;
    });
    expandedList.addExpandedList(widget.widgetId, isExpanded);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: 2, color: Color(0xFFEFEFEF))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            HeaderLayout(
              typeLayout: 'MenuColumn1',
              moduleName: widget.moduleName,
              widgetId: widget.widgetId,
              toggleExpansion: toggleExpansion,
              isExpanded: isExpanded,
            ),
            Column(
              children: isExpanded
                  ? [
                      ...children,
                      Container(
                        padding: EdgeInsets.only(top: 10, bottom: 20),
                        alignment: Alignment.center,
                        child: buildButtonAdd(
                            context, widget.widgetId, bloc!, Color(0xFF074AA9)),
                      )
                    ]
                  : [],
            )
          ],
        ),
      ),
    );
  }
}
