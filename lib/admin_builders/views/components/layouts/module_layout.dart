import 'package:tv_menu/admin_builders/models/is_expand_list.dart';
import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/header_layout.dart';

class ModuleLayout extends StatefulWidget {
  const ModuleLayout(
      {super.key, required this.moduleName, required this.widgetId, required this.typeLayout});

  final String typeLayout;
  final String moduleName;
  final String widgetId;

  @override
  State<ModuleLayout> createState() => _ModuleLayoutState();
}

class _ModuleLayoutState extends State<ModuleLayout> {
  /* [091423TREE]
  This widget is a module layout.
  */
  ExpandedList expandedList = ExpandedList();
  dynamic isExpanded = true;

  // get state expanded module from local
  void getExpandedModule() async {
    await expandedList.fetchExpandedList();
    var data = expandedList.getExpandedList(widget.widgetId);
    setState(() {
      isExpanded = data ?? true;
    });
  }

  // show / hide children in modules
  void toggleExpansion() async {
    setState(() {
      isExpanded = !isExpanded;
    });
    expandedList.addExpandedList(widget.widgetId, isExpanded);
  }

  @override
  void initState() {
    super.initState();
    getExpandedModule();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            border: Border.all(width: 2, color: Color(0xFFEFEFEF))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            HeaderLayout(
              typeLayout: widget.typeLayout,
              moduleName: widget.moduleName,
              widgetId: widget.widgetId,
              toggleExpansion: toggleExpansion,
              isExpanded: isExpanded,
            ),
            isExpanded
                ? Padding(
                    padding: EdgeInsets.all(15),
                    child: Text(''),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
