import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/views/components/settings/category_block1_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/category_block2_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/category_column1_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/category_column2_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/category_title_grid_1_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/category_title_grid_3_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/category_title_grid_4_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/category_title_single_column_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/category_title_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/column_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/container_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/custom_container_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/divider_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/expanded_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/framed_image_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/image_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/indexed_menu_item_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/list_product_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/listview_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/menu_column1_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/menu_item_price_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/padding_ratio_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/padding_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/positioned_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/product_row_1_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/product_row_2_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/product_row_3_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/product_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/rotated_box_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/row_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/service_item_column_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/service_item_custom1_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/service_item_with_thumbnail_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/simple_image_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/sizedbox_ratio_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/sizedbox_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/text_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/underline_brand_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/vertical_divider_setting.dart';
import 'package:tv_menu/admin_builders/views/components/settings/wrap_setting.dart';

class HeaderLayout extends StatefulWidget {
  const HeaderLayout(
      {super.key,
      required this.typeLayout,
      required this.widgetId,
      required this.moduleName,
      required this.toggleExpansion,
      required this.isExpanded});

  final String typeLayout;
  final String moduleName;
  final String widgetId;
  final Function toggleExpansion;
  final bool isExpanded;

  @override
  State<HeaderLayout> createState() => _HeaderLayoutState();
}

class _HeaderLayoutState extends State<HeaderLayout> {
  // this variable is used to store list icons, each icon has a different function.
  List<IconData> listIcons = [
    Icons.settings,
    Icons.delete,
  ];

  // function get color of header with type
  Color getHeaderColor(String type) {
    switch (type) {
      case 'Container':
        return Color(0xFF2b87da);
      case 'Row':
        return Color(0xFF29c4a9);
      case 'Column':
        return Color(0xFFF44336);
      case 'Padding':
        return Color(0xFFFF9800);
      case 'PaddingByRatio':
        return Color(0xFFB5BB0D);
      case 'SizedBox':
        return Color(0xFF795548);
      case 'SizedBoxByRatio':
        return Color(0xFF41A069);
      case 'Center':
        return Color(0xFFE91E63);
      case 'Stack':
        return Color(0xFF8A1EE9);
      case 'Expanded':
        return Color(0xFF0ABF31);
      case 'IntrinsicHeight':
        return Color(0xFF5C0543);
      case 'CustomContainer':
        return Color(0xFFAD1457);
      case 'Wrap':
        return Color(0xFF009688);
      case 'ListView':
        return Color(0xFF7B07A9);
      case 'Positioned':
        return Color(0xFF0799A9);
      case 'RotatedBox':
        return Color(0xFF393253);
      case 'MenuColumn1':
        return Color(0xFF074AA9);
      case 'CategoryColumn1':
        return Color(0xFF7E92E1);
      case 'CategoryColumn2':
        return Color(0xFFEB93E2);
      case 'CategoryBlock1':
        return Color(0xFF31469A);
      case 'CategoryBlock2':
        return Color(0xFF881D48);
      default:
        return Color(0xFF4C5866);
    }
  }

  // This function is used to handle cases to open setting module.
  void handleSettingModule() async {
    final bloc = BlocProvider.of<BuilderBloc>(context);
    switch (widget.typeLayout) {
      case 'Container':
        customDialog(
            context, ContainerSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'Row':
        customDialog(
            context, RowSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'Column':
        customDialog(
            context, ColumnSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'SizedBox':
        customDialog(
            context, SizedBoxSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'SizedBoxByRatio':
        customDialog(context,
            SizedBoxRatioSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'Expanded':
        customDialog(
            context, ExpandedSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'Text':
        customDialog(
            context, TextSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'productItem':
        customDialog(
            context, ProductSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'productList':
        customDialog(
            context, ListProductSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'image':
        customDialog(
            context, ImageSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'SimpleImage':
        customDialog(
            context, SimpleImageSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'Padding':
        customDialog(
            context, PaddingSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'PaddingByRatio':
        customDialog(context,
            PaddingRatioSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'ListView':
        customDialog(
            context, ListViewSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'categoryTitleSingleColumn':
        customDialog(
            context,
            CategoryTitleSingleColumnSetting(
                widgetId: widget.widgetId, bloc: bloc));
      case 'categoryTitleGridLayout1':
        customDialog(context,
            CategoryTitleGrid1Setting(widgetId: widget.widgetId, bloc: bloc));
      case 'CategoryTitleGridLayout3':
        customDialog(context,
            CategoryTitleGrid3Setting(widgetId: widget.widgetId, bloc: bloc));
      case 'categoryTitleGridLayout4':
        customDialog(context,
            CategoryTitleGrid4Setting(widgetId: widget.widgetId, bloc: bloc));
      case 'categoryTitle':
        customDialog(context,
            CategoryTitleSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'CategoryColumn1':
        customDialog(context,
            CategoryColumn1Setting(widgetId: widget.widgetId, bloc: bloc));
      case 'CategoryColumn2':
        customDialog(context,
            CategoryColumn2Setting(widgetId: widget.widgetId, bloc: bloc));
      case 'CategoryBlock1':
        customDialog(context,
            CategoryBlock1Setting(widgetId: widget.widgetId, bloc: bloc));
      case 'CategoryBlock2':
        customDialog(context,
            CategoryBlock2Setting(widgetId: widget.widgetId, bloc: bloc));
      case 'IndexedMenuItem1':
        customDialog(context,
            IndexedMenuItemSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'ServiceItemCustom1':
        customDialog(context,
            ServiceItemCustom1Setting(widgetId: widget.widgetId, bloc: bloc));
      case 'Divider':
        customDialog(
            context, DividerSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'UnderlineBrand':
        customDialog(context,
            UnderlineBrandSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'VerticalDivider':
        customDialog(context,
            VerticalDividerSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'Positioned':
        customDialog(
            context, PositionedSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'RotatedBox':
        customDialog(
            context, RotatedBoxSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'ServiceItemColumn':
        customDialog(context,
            ServiceItemColumnSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'ServiceItemWithThumbnail':
        customDialog(
            context,
            ServiceItemWithThumbnailSetting(
                widgetId: widget.widgetId, bloc: bloc));
      case 'CustomContainer':
        customDialog(context,
            CustomContainerSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'Wrap':
        customDialog(
            context, WrapSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'MenuItemPrice':
        customDialog(context,
            MenuItemPriceSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'MenuColumn1':
        customDialog(
            context, MenuColumn1Setting(widgetId: widget.widgetId, bloc: bloc));
      case 'FramedImage':
        customDialog(
            context, FramedImageSetting(widgetId: widget.widgetId, bloc: bloc));
      case 'ProductRow1':
        customDialog(
            context, ProductRow1Setting(widgetId: widget.widgetId, bloc: bloc));
      case 'ProductRow2':
        customDialog(
            context, ProductRow2Setting(widgetId: widget.widgetId, bloc: bloc));
      case 'ProductRow3':
        customDialog(
            context, ProductRow3Setting(widgetId: widget.widgetId, bloc: bloc));
      default:
    }

    bloc.add(GetModuleEvent(widgetId: widget.widgetId));

    await Future.delayed(const Duration(milliseconds: 500));
  }

  // function remove a module
  void removeModule() {
    final bloc = BlocProvider.of<BuilderBloc>(context);
    if (widget.widgetId == jsonDecode(bloc.state.settings)['id']) {
      // hard code : remove container main when it covered another container.
      bloc.add(ClearModulesEvent());
    } else {
      bloc.add(RemoveModuleEvent(widgetId: widget.widgetId));
    }
  }

  // function change header name module
  void changeHeaderName() {
    final bloc = BlocProvider.of<BuilderBloc>(context);
    bloc.add(ChangeModuleNameEvent(
        moduleName: controllerHeaderName.text, widgetId: widget.widgetId));
  }

  TextEditingController controllerHeaderName = TextEditingController();
  @override
  void initState() {
    super.initState();
    controllerHeaderName.text = widget.moduleName;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final maxWidth = constraints.maxWidth;

        return Container(
          color: getHeaderColor(widget.typeLayout),
          padding: EdgeInsets.all(2),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // display list icon buttons
              Flexible(
                flex: 1,
                child: maxWidth > 340
                    ? Row(
                        children: [
                          IconButton(
                            icon: Icon(
                              Icons.settings,
                              size: 18,
                              color: Colors.white,
                            ),
                            onPressed: handleSettingModule,
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.delete,
                              size: 18,
                              color: Colors.white,
                            ),
                            onPressed: removeModule,
                          ),
                        ],
                      )
                    : buildOptionsModule(),
              ),
              // name of box
              Flexible(
                flex: 1,
                child: Container(
                  alignment: Alignment.center,
                  child: Tooltip(
                    message: controllerHeaderName.text,
                    child: TextSelectionTheme(
                      data:
                          TextSelectionThemeData(selectionColor: Colors.black),
                      child: TextField(
                        onSubmitted: (value) {
                          changeHeaderName();
                        },
                        style: TextStyle(color: Colors.white, fontSize: 15),
                        textAlign: TextAlign.center,
                        controller: controllerHeaderName,
                        maxLines: 1,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.all(8),
                            labelStyle: TextStyle(fontSize: 15)),
                      ),
                    ),
                  ),
                ),
              ),
              // icon view more or less
              Flexible(
                flex: 1,
                child: GestureDetector(
                  onTap: () => widget.toggleExpansion(),
                  child: Container(
                    alignment: Alignment.topRight,
                    child: Icon(
                      widget.isExpanded
                          ? Icons.keyboard_arrow_down
                          : Icons.keyboard_arrow_up,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Row buildOptionsModule() {
    return Row(
      children: [
        PopupMenuButton<String>(
          icon: Icon(
            Icons.more_vert,
            size: 18,
            color: Colors.white,
          ),
          onSelected: (value) {
            if (value == 'setting') {
              handleSettingModule();
            } else if (value == 'remove') {
              removeModule();
            }
          },
          itemBuilder: (BuildContext context) => [
            PopupMenuItem<String>(
              value: 'setting',
              child: Text('Setting', style: TextStyle(fontSize: 14)),
            ),
            PopupMenuItem<String>(
              value: 'remove',
              child: Text('Remove', style: TextStyle(fontSize: 14)),
            ),
          ],
        )
      ],
    );
  }
}
