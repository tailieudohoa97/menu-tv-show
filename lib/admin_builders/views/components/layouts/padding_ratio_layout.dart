import 'package:tv_menu/admin_builders/models/is_expand_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/build_module_option.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/header_layout.dart';

class PaddingRatioLayout extends StatefulWidget {
  const PaddingRatioLayout(
      {super.key, this.widgetLayout, required this.widgetId, required this.moduleName});

  final Widget? widgetLayout; // for loading layout
  final String widgetId;
  final String moduleName;

  @override
  State<PaddingRatioLayout> createState() => _PaddingRatioLayoutState();
}

class _PaddingRatioLayoutState extends State<PaddingRatioLayout> {
  /* [091423TREE]
  This widget is a padding layout.
  */
  Widget? child;
  BuilderBloc? bloc;
  ExpandedList expandedList = ExpandedList();
  dynamic isExpanded = true;

  @override
  void initState() {
    super.initState();
    child = widget.widgetLayout;
    bloc = BlocProvider.of<BuilderBloc>(context);
    getExpandedModule();
  }

  // get state expanded module from local
  void getExpandedModule() async {
    await expandedList.fetchExpandedList();
    var data = expandedList.getExpandedList(widget.widgetId);
    setState(() {
      isExpanded = data ?? true;
    });
  }

  // show / hide children in modules
  void toggleExpansion() async {
    setState(() {
      isExpanded = !isExpanded;
    });
    expandedList.addExpandedList(widget.widgetId, isExpanded);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(10),
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: 2, color: Color(0xFFEFEFEF))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            HeaderLayout(
              typeLayout: 'PaddingByRatio',
              moduleName: widget.moduleName,
              widgetId: widget.widgetId,
              toggleExpansion: toggleExpansion,
              isExpanded: isExpanded,
            ),
            Column(
              children: isExpanded
                  ? [
                      Container(child: child),
                      Container(
                        padding: EdgeInsets.only(top: 10, bottom: 20),
                        alignment: Alignment.center,
                        child: child != null
                            ? buildButtonAdd(context, widget.widgetId, bloc!,
                                Color(0xFFB5BB0D))
                            : null,
                      )
                    ]
                  : [],
            )
          ],
        ),
      ),
    );
  }
}
