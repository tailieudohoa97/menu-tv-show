import 'package:tv_menu/admin_builders/models/is_expand_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/build_module_option.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/header_layout.dart';

class RowLayout extends StatefulWidget {
  const RowLayout({super.key, this.listWidgetLayout, required this.widgetId, required this.moduleName});

  final List<Widget>? listWidgetLayout;
  final String widgetId;
  final String moduleName;

  @override
  State<RowLayout> createState() => _RowLayoutState();
}

class _RowLayoutState extends State<RowLayout> {
  List<Widget> children = [];
  BuilderBloc? bloc;
  ExpandedList expandedList = ExpandedList();
  dynamic isExpanded = true;

  @override
  void initState() {
    children = widget.listWidgetLayout ?? [];
    super.initState();
    bloc = BlocProvider.of<BuilderBloc>(context);
    getExpandedModule();
  }

  // check width module so that it's suitable for screen.
  double checkWidthChildren(double maxWidth) {
    int count = children.length;
    return maxWidth / count < 340 ? maxWidth : maxWidth / count;
  }

  // get state expanded module from local
  void getExpandedModule() async {
    await expandedList.fetchExpandedList();
    var data = expandedList.getExpandedList(widget.widgetId);
    setState(() {
      isExpanded = data ?? true;
    });
  }

  // show / hide children in modules
  void toggleExpansion() async {
    setState(() {
      isExpanded = !isExpanded;
    });
    expandedList.addExpandedList(widget.widgetId, isExpanded);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(width: 2, color: Color(0xFFEFEFEF))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            HeaderLayout(
              typeLayout: 'Row',
              moduleName: widget.moduleName,
              widgetId: widget.widgetId,
              isExpanded: isExpanded,
              toggleExpansion: toggleExpansion,
            ),
            Column(
              children: isExpanded
                  ? [
                      LayoutBuilder(
                        builder: (context, constraints) {
                          final maxWidth = constraints.maxWidth;
                          return SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: children.map(
                                (child) {
                                  return Container(
                                    constraints: BoxConstraints(
                                      maxWidth: checkWidthChildren(maxWidth),
                                    ),
                                    child: child,
                                  );
                                },
                              ).toList(),
                            ),
                          );
                        },
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10, bottom: 20),
                        alignment: Alignment.center,
                        child: buildButtonAdd(
                            context, widget.widgetId, bloc!, Color(0xFF29c4a9)),
                      )
                    ]
                  : [],
            )
          ],
        ),
      ),
    );
  }
}
