import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/controllers/builders_controller.dart';
import 'package:tv_menu/models/models.dart';

class BuilderPage extends StatefulWidget {
  const BuilderPage({Key? key, required this.template}) : super(key: key);

  final Template template;

  @override
  State<BuilderPage> createState() => _BuilderPageState();
}

class _BuilderPageState extends State<BuilderPage> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        // use dialog builder then add width and height, else remove
        width: 1500,
        height: 700,
        alignment: Alignment.center,
        color: Color(0xFFf1f5f9),
        child: BuildersController(template: widget.template),
      ),
    );
  }
}
