import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_state.dart';
import 'package:tv_menu/admin_builders/bloc/drafts/draft_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/drafts/draft_event.dart';
import 'package:tv_menu/admin_builders/bloc/drafts/draft_state.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';
import 'package:tv_menu/admin_builders/repo/local_db.dart';
import 'package:tv_menu/admin_builders/views/components/builder_layout.dart';
import 'package:tv_menu/admin_builders/views/components/builder_structures.dart';
import 'package:tv_menu/models/models.dart';

class BuildersController extends StatefulWidget {
  const BuildersController({Key? key, required this.template})
      : super(key: key);

  final Template template;

  @override
  State<BuildersController> createState() => _BuildersControllerState();
}

class _BuildersControllerState extends State<BuildersController> {
  double item1Width = 1 / 6;
  double item2Width = 5 / 6;
  dynamic isOpenStructure = false;

  void saveOpenStructures() async {
    await LocalDB.saveDataToLocal(
        'openStructures', jsonEncode(isOpenStructure));
  }

  void getOpenStructures() async {
    final data = await LocalDB.getDataFromLocal('openStructures');
    setState(() {
      if (data.isNotEmpty) {
        isOpenStructure = bool.parse(jsonDecode(data));
      }
    });
  }

  void handleOpenStructures() {
    setState(() {
      isOpenStructure = true;
    });
  }

  void handleCloseStructures() {
    setState(() {
      isOpenStructure = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getOpenStructures();
  }

  // [091723TREE]
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<BuilderBloc>(
          create: (context) => BuilderBloc(repository: BuilderRepository())
            ..add(GetSettingEvent(
                dataSetting: widget.template.dataSetting,
                widgetSetting: widget.template.widgetSetting,
                radioSetting: widget.template.ratioSetting)),
        ),
        BlocProvider<DraftBloc>(
          create: (context) => DraftBloc()..add(GetListDrafts()),
        ),
      ],
      child: BlocBuilder<BuilderBloc, BuilderState>(
        builder: (context, state) {
          if (state.isLoading) {
            return Center(child: CircularProgressIndicator());
          }

          // state DraftBloc
          return BlocBuilder<DraftBloc, DraftState>(
            builder: (context, draftState) {
              if (draftState.isLoading) {
                return Center(child: CircularProgressIndicator());
              }

              final bloc = BlocProvider.of<BuilderBloc>(context);
              final draftBloc = BlocProvider.of<DraftBloc>(context);

              return LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  final minWidthItem2 = constraints.maxWidth *
                      0.5; // min width item 2 = 50% screen width
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Item 1
                      isOpenStructure
                          ? buildLayoutWidgetTree(
                              constraints, minWidthItem2, bloc)
                          : Container(),
                      // Item 2
                      buildLayoutTemplate(
                          constraints, minWidthItem2, bloc, draftBloc),
                    ],
                  );
                },
              );
            },
          );
        },
      ),
    );
  }

  GestureDetector buildLayoutTemplate(BoxConstraints constraints,
      double minWidthItem2, BuilderBloc bloc, DraftBloc draftBloc) {
    return GestureDetector(
      onHorizontalDragUpdate: (details) {
        setState(() {
          if (isOpenStructure) {
            item2Width -= details.delta.dx / constraints.maxWidth;
            item2Width = item2Width.clamp(
                minWidthItem2 / constraints.maxWidth, 5 / 6); // min width item2
            item1Width = 1 - item2Width;
          }
        });
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        width: isOpenStructure
            ? constraints.maxWidth * item2Width
            : constraints.maxWidth,
        child: BuilderLayout(
          template: widget.template,
          bloc: bloc,
          draftBloc: draftBloc,
          saveOpenStructures: saveOpenStructures,
          handleOpenStructures: handleOpenStructures,
        ),
      ),
    );
  }

  GestureDetector buildLayoutWidgetTree(
      BoxConstraints constraints, double minWidthItem2, BuilderBloc bloc) {
    return GestureDetector(
      onHorizontalDragUpdate: (details) {
        setState(() {
          item1Width += details.delta.dx / constraints.maxWidth;
          item1Width = item1Width.clamp(1 / 6,
              1 - minWidthItem2 / constraints.maxWidth); // min width item 1
          item2Width = 1 - item1Width;
        });
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        width: constraints.maxWidth * item1Width,
        child: isOpenStructure
            ? BuilderStructures(
                saveOpenStructures: saveOpenStructures,
                handleCloseStructures: handleCloseStructures,
                json: bloc.state.settings != ""
                    ? jsonDecode(bloc.state.settings)
                    : {},
              )
            : Row(
                children: [
                  TextButton(
                      onPressed: () {
                        handleOpenStructures();
                        saveOpenStructures();
                      },
                      child: Text("Open widget tree"))
                ],
              ),
      ),
    );
  }
}
