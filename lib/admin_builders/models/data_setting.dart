import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/repo/local_db.dart';

/* [100623TREE]
Structure of dataSetting:
  {
    'id1': [{'id': '1'}, {'id': '2'}],
    'id2' : {'id': {''}}
  }
*/

class DataSetting {
  Map<String, dynamic> _dataSetting = {};

  // fetch data setting from local database
  Future<void> fetchDataSetting() async {
    try {
      final dataLocal = await LocalDB.getDataFromLocal('dataSetting');
      if (dataLocal.isNotEmpty) {
        final dataConverted = jsonDecode(dataLocal);
        _dataSetting = dataConverted;
      }
    } catch (e) {
      throw ErrorDescription(e.toString());
    }
  }

  DataSetting() {
    fetchDataSetting();
  }

  // getter
  Map<String, dynamic> get dataSetting {
    return _dataSetting;
  }

  // get data from data setting
  dynamic getDataFromSetting(String dataId) {
    dynamic data =
        _dataSetting.containsKey(dataId) ? _dataSetting[dataId] : null;
    return data;
  }

  // data to dataSetting, input is map or list data
  void addDataToSetting(String dataId, dynamic data) {
    _dataSetting.putIfAbsent(dataId, () => (data));
    updateLocalData();
  }

  // remove data from dataSetting
  void removeDataOnSetting(String dataId) {
    _dataSetting.remove(dataId);
    updateLocalData();
  }

  // update data in dataSetting
  void updateDataOnSetting(String dataId, dynamic dataUpdated) {
    _dataSetting[dataId] = dataUpdated;
    updateLocalData();
  }

  // update local database
  Future<void> updateLocalData() async {
    try {
      final jsonData = jsonEncode(_dataSetting);
      await LocalDB.saveDataToLocal('dataSetting', jsonData);
    } catch (e) {
      throw ErrorDescription(e.toString());
    }
  }
}
