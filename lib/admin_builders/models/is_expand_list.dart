import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/repo/local_db.dart';

class ExpandedList {
  Map<String, dynamic> _expandedList = {};

  // fetch data setting from local database
  Future<void> fetchExpandedList() async {
  try {
    final dataLocal = await LocalDB.getDataFromLocal('expandedList');
    dynamic dataConverted;
    if (dataLocal != null && dataLocal.isNotEmpty) {
      dataConverted = jsonDecode(dataLocal);
      _expandedList = dataConverted;
    }
  } catch (e) {
    throw ErrorDescription(e.toString());
  }
}

  ExpandedList() {
    fetchExpandedList();
  }

  // getter
  Map<String, dynamic> get expandedList {
    return _expandedList;
  }

  // get data from data setting
  dynamic getExpandedList(String dataId) {
    dynamic data =
        _expandedList.containsKey(dataId) ? _expandedList[dataId] : null;
    return data;
  }

  // data to dataSetting, input is map or list data
  void addExpandedList(String dataId, dynamic data) {
    if (_expandedList[dataId] == null) {
      _expandedList.putIfAbsent(dataId, () => (data));
    } else {
      _expandedList[dataId] = data;
    }
    updateLocalData();
  }

  // update local database
  Future<void> updateLocalData() async {
    try {
      final jsonData = jsonEncode(_expandedList);
      await LocalDB.saveDataToLocal('expandedList', jsonData);
    } catch (e) {
      throw ErrorDescription(e.toString());
    }
  }
}
