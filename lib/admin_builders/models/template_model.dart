/* [092301TREE]
command : flutter packages pub run build_runner build
- run command to generate adapter automatically, because this app store list data
*/

class TemplateModel {
  String? id;
  String name;

  String type;

  TemplateModel({this.id, required this.name, required this.type});

  factory TemplateModel.fromJson(Map<String, dynamic> json) {
    return TemplateModel(
      id: json["_id"].toString(),
      name: json["name"],
      type: json["type"],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'name': name,
      'type': type,
    };
  }
}
