import 'package:flutter/material.dart';

// URL
const apiUrl = 'http://localhost:8888/api/v1';

// COLORS
const kWhiteColor = Color(0xFFffffff);
const kBlackColor = Color(0xFF000000);
const kGrayColor = Color(0xFFbfc1c2);
const kBlueColor = Color(0xFF2196F3);
const kRedColor = Color(0xFFF44336);
const kTealColor = Color(0xFF009688);

// DISTANCES
const normalRadius = 10.0;
const largeRadius = 20.0;

// TEXT
// font size
const smallSize = 14.0;
const mediumSize = 16.0;
const largeSize = 20.0;
const bigSize = 30.0;

// font weight
const textBold = FontWeight.bold;
const textNormal = FontWeight.w500;
