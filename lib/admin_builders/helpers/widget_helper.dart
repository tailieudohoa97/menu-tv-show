import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/category_block1_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/category_block2_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/category_column1_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/category_column2_layou.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/center_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/column_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/custom_container_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/expanded_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/intrinsic_height_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/list_view_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/menu_column1_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/module_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/padding_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/padding_ratio_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/positioned_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/rotated_box_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/row_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/container_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/sizedbox_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/sizedbox_ratio_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/stack_layout.dart';
import 'package:tv_menu/admin_builders/views/components/layouts/wrap_layout.dart';

// Render the layout of modules when adding a new module
class WidgetHelper {
  bool check = true; // check that get settings local ?
  String idModuleLast = ""; // get id of the last module
  Map<String, dynamic> mainSettings = {}; // == settings in local db.

  // render module layout cases
  Widget renderWidgetLayout(dynamic setting, Key? itemKey,
      {String? currentModuleId}) {
    if (check) {
      mainSettings = setting;
      idModuleLast = currentModuleId ?? "";
      check = false;
    }

    if (setting.isEmpty || !setting.containsKey('widgetName')) {
      return Container();
    }
    // cases module
    switch (setting['widgetName']) {
      case 'Container':
        return containerWidget(setting, itemKey);
      case 'Row':
        return rowWidget(setting, itemKey);
      case 'Column':
        return columnWidget(setting, itemKey);
      case 'Expanded':
        return expandedWidget(setting, itemKey);
      case 'Padding':
        return paddingWidget(setting, itemKey);
      case 'PaddingByRatio':
        return paddingRatioWidget(setting, itemKey);
      case 'SizedBox':
        return sizedBoxWidget(setting, itemKey);
      case 'Center':
        return centerWidget(setting, itemKey);
      case 'SizedBoxByRatio':
        return sizedBoxRatioWidget(setting, itemKey);
      case 'Stack':
        return stackWidget(setting, itemKey);
      case 'IntrinsicHeight':
        return intrinsicHeightWidget(setting, itemKey);
      case 'CustomContainer':
        return customContainerWidget(setting, itemKey);
      case 'Wrap':
        return wrapWidget(setting, itemKey);
      case 'Positioned':
        return positionedWidget(setting, itemKey);
      case 'RotatedBox':
        return rotatedBoxWidget(setting, itemKey);
      case 'MenuColumn1':
        return menuColumn1Widget(setting, itemKey);
      case 'ListView':
        return listViewWidget(setting, itemKey);
      case 'CategoryColumn1':
        return categoryColumn1Widget(setting, itemKey);
      case 'CategoryColumn2':
        return categoryColumn2Widget(setting, itemKey);
      case 'CategoryBlock1':
        return categoryBlock1Widget(setting, itemKey);
      case 'CategoryBlock2':
        return categoryBlock2Widget(setting, itemKey);
      default:
        return modulesWidget(setting, itemKey);
    }
  }

  // render container layout
  Widget containerWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: ContainerLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       widgetLayout: setting['widgetSetting'].containsKey('child')
    //           ? renderWidgetLayout(setting['widgetSetting']['child'],
    //               itemKey)
    //           : null,
    //     ),
    //   );
    // } else {
    return ContainerLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      widgetLayout: setting['widgetSetting'].containsKey('child')
          ? renderWidgetLayout(setting['widgetSetting']['child'], itemKey)
          : null,
    );
    // }
  }

  // render row layout
  Widget rowWidget(Map<String, dynamic> setting, Key? itemKey) {
    List<dynamic> children = setting['widgetSetting'].containsKey('children')
        ? setting['widgetSetting']['children']
        : [];
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: RowLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       listWidgetLayout: children
    //           .map((item) => renderWidgetLayout(item,  itemKey))
    //           .toList(),
    //     ),
    //   );
    // } else {
    return RowLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      listWidgetLayout:
          children.map((item) => renderWidgetLayout(item, itemKey)).toList(),
    );
    // }
  }

  // render column layout
  Widget columnWidget(Map<String, dynamic> setting, Key? itemKey) {
    List<dynamic> children = setting['widgetSetting'].containsKey('children')
        ? setting['widgetSetting']['children']
        : [];
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: ColumnLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       listWidgetLayout: children
    //           .map((item) => renderWidgetLayout(item,  itemKey))
    //           .toList(),
    //     ),
    //   );
    // } else {
    return ColumnLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      listWidgetLayout:
          children.map((item) => renderWidgetLayout(item, itemKey)).toList(),
    );
    // }
  }

  // render menu column 1 layout
  Widget menuColumn1Widget(Map<String, dynamic> setting, Key? itemKey) {
    List<dynamic> children = setting['widgetSetting'].containsKey('menuItems')
        ? setting['widgetSetting']['menuItems']
        : [];
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: MenuColumn1Layout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       listWidgetLayout: children
    //           .map((item) => renderWidgetLayout(item,  itemKey))
    //           .toList(),
    //     ),
    //   );
    // } else {
    return MenuColumn1Layout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      listWidgetLayout:
          children.map((item) => renderWidgetLayout(item, itemKey)).toList(),
    );
    // }
  }

  // render list view layout
  Widget listViewWidget(Map<String, dynamic> setting, Key? itemKey) {
    List<dynamic> children = setting['widgetSetting'].containsKey('children')
        ? setting['widgetSetting']['children']
        : [];
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: ListViewLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       listWidgetLayout: children
    //           .map((item) => renderWidgetLayout(item,  itemKey))
    //           .toList(),
    //     ),
    //   );
    // } else {
    return ListViewLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      listWidgetLayout:
          children.map((item) => renderWidgetLayout(item, itemKey)).toList(),
    );
    // }
  }

  // render category column 1 layout
  Widget categoryColumn1Widget(Map<String, dynamic> setting, Key? itemKey) {
    List<dynamic> children = setting['widgetSetting'].containsKey('productRows')
        ? setting['widgetSetting']['productRows']
        : [];
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: CategoryColumn1Layout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       listWidgetLayout: children
    //           .map((item) => renderWidgetLayout(item,  itemKey))
    //           .toList(),
    //     ),
    //   );
    // } else {
    return CategoryColumn1Layout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      listWidgetLayout:
          children.map((item) => renderWidgetLayout(item, itemKey)).toList(),
    );
    // }
  }

  // render category column 2 layout
  Widget categoryColumn2Widget(Map<String, dynamic> setting, Key? itemKey) {
    List<dynamic> children =
        setting['widgetSetting'].containsKey('categoryItems')
            ? setting['widgetSetting']['categoryItems']
            : [];
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: CategoryColumn2Layout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       listWidgetLayout: children
    //           .map((item) => renderWidgetLayout(item,  itemKey))
    //           .toList(),
    //     ),
    //   );
    // } else {
    return CategoryColumn2Layout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      listWidgetLayout:
          children.map((item) => renderWidgetLayout(item, itemKey)).toList(),
    );
    // }
  }

  // render category block 1 layout
  Widget categoryBlock1Widget(Map<String, dynamic> setting, Key? itemKey) {
    List<dynamic> children =
        setting['widgetSetting'].containsKey('categoryItemRows')
            ? setting['widgetSetting']['categoryItemRows']
            : [];
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: CategoryBlock1Layout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       listWidgetLayout: children
    //           .map((item) => renderWidgetLayout(item,  itemKey))
    //           .toList(),
    //     ),
    //   );
    // } else {
    return CategoryBlock1Layout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      listWidgetLayout:
          children.map((item) => renderWidgetLayout(item, itemKey)).toList(),
    );
    // }
  }

  // render category column 2 layout
  Widget categoryBlock2Widget(Map<String, dynamic> setting, Key? itemKey) {
    List<dynamic> children =
        setting['widgetSetting'].containsKey('categoryItems')
            ? setting['widgetSetting']['categoryItems']
            : [];
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: CategoryBlock2Layout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       listWidgetLayout: children
    //           .map((item) => renderWidgetLayout(item,  itemKey))
    //           .toList(),
    //     ),
    //   );
    // } else {
    return CategoryBlock2Layout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      listWidgetLayout:
          children.map((item) => renderWidgetLayout(item, itemKey)).toList(),
    );
    // }
  }

  // render expanded layout
  Widget expandedWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: ExpandedLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       widgetLayout: setting['widgetSetting'].containsKey('child')
    //           ? renderWidgetLayout(setting['widgetSetting']['child'],
    //                itemKey)
    //           : null,
    //     ),
    //   );
    // } else {
    return ExpandedLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      widgetLayout: setting['widgetSetting'].containsKey('child')
          ? renderWidgetLayout(setting['widgetSetting']['child'], itemKey)
          : null,
    );
    // }
  }

  // render center layout
  Widget centerWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: CenterLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       widgetLayout: setting['widgetSetting'].containsKey('child')
    //           ? renderWidgetLayout(setting['widgetSetting']['child'],
    //                itemKey)
    //           : null,
    //     ),
    //   );
    // } else {
    return CenterLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      widgetLayout: setting['widgetSetting'].containsKey('child')
          ? renderWidgetLayout(setting['widgetSetting']['child'], itemKey)
          : null,
    );
    // }
  }

  // render positioned layout
  Widget positionedWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: PositionedLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       widgetLayout: setting['widgetSetting'].containsKey('child')
    //           ? renderWidgetLayout(setting['widgetSetting']['child'],
    //                itemKey)
    //           : null,
    //     ),
    //   );
    // } else {
    return PositionedLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      widgetLayout: setting['widgetSetting'].containsKey('child')
          ? renderWidgetLayout(setting['widgetSetting']['child'], itemKey)
          : null,
    );
    // }
  }

  // render positioned layout
  Widget rotatedBoxWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: RotatedBoxLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       widgetLayout: setting['widgetSetting'].containsKey('child')
    //           ? renderWidgetLayout(setting['widgetSetting']['child'],
    //                itemKey)
    //           : null,
    //     ),
    //   );
    // } else {
    return RotatedBoxLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      widgetLayout: setting['widgetSetting'].containsKey('child')
          ? renderWidgetLayout(setting['widgetSetting']['child'], itemKey)
          : null,
    );
    // }
  }

  // render padding layout
  Widget paddingWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: PaddingLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       widgetLayout: setting['widgetSetting'].containsKey('child')
    //           ? renderWidgetLayout(setting['widgetSetting']['child'],
    //                itemKey)
    //           : null,
    //     ),
    //   );
    // } else {
    return PaddingLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      widgetLayout: setting['widgetSetting'].containsKey('child')
          ? renderWidgetLayout(setting['widgetSetting']['child'], itemKey)
          : null,
    );
    // }
  }

  // render sized box layout
  Widget sizedBoxWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: SizedBoxLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       widgetLayout: setting['widgetSetting'].containsKey('child')
    //           ? renderWidgetLayout(setting['widgetSetting']['child'],
    //                itemKey)
    //           : null,
    //     ),
    //   );
    // } else {
    return SizedBoxLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      widgetLayout: setting['widgetSetting'].containsKey('child')
          ? renderWidgetLayout(setting['widgetSetting']['child'], itemKey)
          : null,
    );
    // }
  }

  // render sized box ratio layout
  Widget sizedBoxRatioWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: SizedBoxRatioLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       widgetLayout: setting['widgetSetting'].containsKey('child')
    //           ? renderWidgetLayout(setting['widgetSetting']['child'],
    //                itemKey)
    //           : null,
    //     ),
    //   );
    // } else {
    return SizedBoxRatioLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      widgetLayout: setting['widgetSetting'].containsKey('child')
          ? renderWidgetLayout(setting['widgetSetting']['child'], itemKey)
          : null,
    );
    // }
  }

  // render custom container layout
  Widget customContainerWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: CustomContainerLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       widgetLayout: setting['widgetSetting'].containsKey('child')
    //           ? renderWidgetLayout(setting['widgetSetting']['child'],
    //                itemKey)
    //           : null,
    //     ),
    //   );
    // } else {
    return CustomContainerLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      widgetLayout: setting['widgetSetting'].containsKey('child')
          ? renderWidgetLayout(setting['widgetSetting']['child'], itemKey)
          : null,
    );
    // }
  }

  // render wrap layout
  Widget wrapWidget(Map<String, dynamic> setting, Key? itemKey) {
    List<dynamic> children = setting['widgetSetting'].containsKey('children')
        ? setting['widgetSetting']['children']
        : [];
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: WrapLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       listWidgetLayout: children
    //           .map((item) => renderWidgetLayout(item,  itemKey))
    //           .toList(),
    //     ),
    //   );
    // } else {
    return WrapLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      listWidgetLayout:
          children.map((item) => renderWidgetLayout(item, itemKey)).toList(),
    );
    // }
  }

  // render stack layout
  Widget stackWidget(Map<String, dynamic> setting, Key? itemKey) {
    List<dynamic> children = setting['widgetSetting'].containsKey('children')
        ? setting['widgetSetting']['children']
        : [];
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: StackLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       listWidgetLayout: children
    //           .map((item) => renderWidgetLayout(item,  itemKey))
    //           .toList(),
    //     ),
    //   );
    // } else {
    return StackLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      listWidgetLayout:
          children.map((item) => renderWidgetLayout(item, itemKey)).toList(),
    );
    // }
  }

  // render intrinsic height layout
  Widget intrinsicHeightWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: IntrinsicHeightLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       widgetLayout: setting['widgetSetting'].containsKey('child')
    //           ? renderWidgetLayout(setting['widgetSetting']['child'],
    //                itemKey)
    //           : null,
    //     ),
    //   );
    // } else {
    return IntrinsicHeightLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      widgetLayout: setting['widgetSetting'].containsKey('child')
          ? renderWidgetLayout(setting['widgetSetting']['child'], itemKey)
          : null,
    );
    // }
  }

  // render padding ratio layout
  Widget paddingRatioWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: PaddingRatioLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       widgetId: setting['id'],
    //       moduleName: setting['headerName'],
    //       widgetLayout: setting['widgetSetting'].containsKey('child')
    //           ? renderWidgetLayout(setting['widgetSetting']['child'],
    //                itemKey)
    //           : null,
    //     ),
    //   );
    // } else {
    return PaddingRatioLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      widgetId: setting['id'],
      moduleName: setting['headerName'],
      widgetLayout: setting['widgetSetting'].containsKey('child')
          ? renderWidgetLayout(setting['widgetSetting']['child'], itemKey)
          : null,
    );
    // }
  }

  // render module content layout
  Widget modulesWidget(Map<String, dynamic> setting, Key? itemKey) {
    // if (checkParentWidget(mainSettings, setting['id'])) {
    //   return Expanded(
    //     child: ModuleLayout(
    //       key: setting["id"] == idModuleLast ? itemKey : null,
    //       typeLayout: setting['widgetName'],
    //       moduleName: setting['headerName'],
    //       widgetId: setting['id'],
    //     ),
    //   );
    // } else {
    return ModuleLayout(
      key: setting["id"] == idModuleLast ? itemKey : null,
      typeLayout: setting['widgetName'],
      moduleName: setting['headerName'],
      widgetId: setting['id'],
    );
  }
  // }
}

// check parent widget and cover expanded
bool checkParentWidget(Map<String, dynamic> widget, String targetId) {
  final Map<String, dynamic> parentWidget = findParentWidget(widget, targetId);
  if (parentWidget.isNotEmpty && parentWidget['widgetName'] == 'Row') {
    return true;
  } else {
    return false;
  }
}

// find parent widget of current widget by widgetParentId
Map<String, dynamic> findParentWidget(
    Map<String, dynamic> widget, String targetId) {
  if (widget.containsKey('widgetSetting')) {
    final Map<String, dynamic>? widgetSetting = widget['widgetSetting'];

    if (widgetSetting!.containsKey("children") &&
        widgetSetting['children'] is List) {
      final List<dynamic> children = widgetSetting['children'];

      for (final child in children) {
        if (child is Map<String, dynamic> && child['id'] == targetId) {
          return widget;
        } else {
          final Map<String, dynamic> result = findParentWidget(child, targetId);
          if (result.isNotEmpty) {
            return result;
          }
        }
      }
    } else if (widgetSetting!.containsKey("productRows") &&
        widgetSetting['productRows'] is List) {
      final List<dynamic> productRows = widgetSetting['productRows'];

      for (final child in productRows) {
        if (child is Map<String, dynamic> && child['id'] == targetId) {
          return widget;
        } else {
          final Map<String, dynamic> result = findParentWidget(child, targetId);
          if (result.isNotEmpty) {
            return result;
          }
        }
      }
    } else if (widgetSetting!.containsKey("categoryItems") &&
        widgetSetting['categoryItems'] is List) {
      final List<dynamic> categoryItems = widgetSetting['categoryItems'];

      for (final child in categoryItems) {
        if (child is Map<String, dynamic> && child['id'] == targetId) {
          return widget;
        } else {
          final Map<String, dynamic> result = findParentWidget(child, targetId);
          if (result.isNotEmpty) {
            return result;
          }
        }
      }
    } else if (widgetSetting!.containsKey("menuItems") &&
        widgetSetting['menuItems'] is List) {
      final List<dynamic> menuItems = widgetSetting['menuItems'];

      for (final child in menuItems) {
        if (child is Map<String, dynamic> && child['id'] == targetId) {
          return widget;
        } else {
          final Map<String, dynamic> result = findParentWidget(child, targetId);
          if (result.isNotEmpty) {
            return result;
          }
        }
      }
    } else if (widgetSetting!.containsKey("categoryItemRows") &&
        widgetSetting['categoryItemRows'] is List) {
      final List<dynamic> categoryItemRows = widgetSetting['categoryItemRows'];

      for (final child in categoryItemRows) {
        if (child is Map<String, dynamic> && child['id'] == targetId) {
          return widget;
        } else {
          final Map<String, dynamic> result = findParentWidget(child, targetId);
          if (result.isNotEmpty) {
            return result;
          }
        }
      }
    } else if (widgetSetting.containsKey("child") &&
        widgetSetting['child'] is Map<String, dynamic>) {
      final Map<String, dynamic>? child = widgetSetting['child'];

      if (child != null && child['id'] == targetId) {
        return widget;
      } else {
        final Map<String, dynamic> result = findParentWidget(child!, targetId);
        if (result.isNotEmpty) {
          return result;
        }
      }
    }
  }

  return {};
}

// loop through json setting and get the last module
dynamic getLastModule(dynamic setting) {
  if (setting.containsKey('widgetSetting') &&
      setting['widgetSetting'].containsKey('child')) {
    final child = setting['widgetSetting']['child'];
    if (child.isNotEmpty) {
      return getLastModule(child);
    }
  } else if (setting.containsKey('widgetSetting') &&
      setting['widgetSetting'].containsKey('productRows')) {
    List<dynamic> productRows = setting['widgetSetting']['productRows'];
    if (productRows.isNotEmpty) {
      return getLastModule(productRows.last);
    }
  } else if (setting.containsKey('widgetSetting') &&
      setting['widgetSetting'].containsKey('children')) {
    List<dynamic> children = setting['widgetSetting']['children'];
    if (children.isNotEmpty) {
      return getLastModule(children.last);
    }
  } else if (setting.containsKey('widgetSetting') &&
      setting['widgetSetting'].containsKey('categoryItems')) {
    List<dynamic> categoryItems = setting['widgetSetting']['categoryItems'];
    if (categoryItems.isNotEmpty) {
      return getLastModule(categoryItems.last);
    }
  } else if (setting.containsKey('widgetSetting') &&
      setting['widgetSetting'].containsKey('menuItems')) {
    List<dynamic> menuItems = setting['widgetSetting']['menuItems'];
    if (menuItems.isNotEmpty) {
      return getLastModule(menuItems.last);
    }
  } else if (setting.containsKey('widgetSetting') &&
      setting['widgetSetting'].containsKey('categoryItemRows')) {
    List<dynamic> categoryItemRows =
        setting['widgetSetting']['categoryItemRows'];
    if (categoryItemRows.isNotEmpty) {
      return getLastModule(categoryItemRows.last);
    }
  }
  return setting;
}
