import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/data/json_data_builder.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/border_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/image_data_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/models/data_setting.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

class BoxDecorationSetting extends StatefulWidget {
  const BoxDecorationSetting(
      {super.key,
      this.widgetId,
      required this.bloc,
      this.onUpdateData,
      this.currentData});

  final String? widgetId;
  final BuilderBloc bloc;
  final Function? onUpdateData;
  final Map<String, dynamic>? currentData;

  @override
  State<BoxDecorationSetting> createState() => _BoxDecorationSettingState();
}

enum Fit { cover, contain, fill, none }

final Map<String, Fit> fitValues = {
  "cover": Fit.cover,
  "contain": Fit.contain,
  "fill": Fit.fill,
  "none": Fit.none
};

class _BoxDecorationSettingState extends State<BoxDecorationSetting>
    with SingleTickerProviderStateMixin {
  TextEditingController controllerBorderRadius = TextEditingController();
  TextEditingController controllerImageUrl = TextEditingController();
  TextEditingController controllerColorBg = TextEditingController();

  String? selectedBlendMode;
  Fit _fitSelected = Fit.cover;
  dynamic borderSide = {}, image = {};
  int currentIndexTab = 0;
  late TabController _tabController;
  DataSetting dataSetting = DataSetting();
  Map<String, dynamic> currentSetting = {};
  String? selectedUnitBorderRadius;

  void initDataSetting(dynamic currentSetting) async {
    await dataSetting.fetchDataSetting();
    dynamic updatedImage = {};

    if (currentSetting["image"].isNotEmpty) {
      var data = dataSetting.getDataFromSetting(currentSetting['image']) ?? {};
      updatedImage = data;
    }

    setState(() {
      image = updatedImage;
    });
  }

  @override
  void initState() {
    currentSetting = widget.currentData!;
    _tabController = TabController(length: 2, vsync: this);
    initDataSetting(currentSetting);
    super.initState();

    borderSide = currentSetting['border'] ?? {};
    controllerImageUrl.text = currentSetting['imageUrl']; // image : key
    controllerBorderRadius.text =
        ConvertUnit.unitToString(currentSetting['borderRadius']);
    _fitSelected = convertStringToFix(currentSetting['boxFit']);
    controllerColorBg.text = currentSetting['color'];
    selectedBlendMode = currentSetting['backgroundBlendMode'];

    selectedUnitBorderRadius =
        ConvertUnit.getUnit(currentSetting['borderRadius']);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void changeValueUnitBorderRadius(String? value) {
    setState(() {
      selectedUnitBorderRadius = value!;
    });
  }

  // Convert radio value to string value
  String convertFitToString() {
    // output is a string
    String selectedKey = '';
    // loop and find key has value equal _typeSelected current
    for (var entry in fitValues.entries) {
      if (entry.value == _fitSelected) {
        selectedKey = entry.key;
        break;
      }
    }
    return selectedKey;
  }

  Fit convertStringToFix(String fitData) {
    // output is a string
    Fit selectedValue = Fit.cover;
    // loop and find key has value equal _typeSelected current
    for (var entry in fitValues.entries) {
      if (entry.key == fitData) {
        selectedValue = entry.value;
        break;
      }
    }
    return selectedValue;
  }

  // This function is used to change fit option value.
  void changeFitValue(value) {
    setState(() {
      _fitSelected = value!;
    });
  }

  // Get list blend mode options.
  List<String> blendModeOptions = JsonDataBuilder.blendModeOptions;

  // This function is used to change blend mode option value.
  void changeValueBlendMode(String? value) {
    setState(() {
      selectedBlendMode = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // Border bottom side
  void editBorderSide() {
    // if borderSide current is empty
    if (borderSide.isEmpty) {
      // new setting border
      var border = BuilderRepository().generateJsonBorderData();

      // update values after edit
      void updateBorderSide(dynamic data) {
        border = data;
        setState(() {
          borderSide = border;
        });
      }

      // pass new setting to BorderDataSetting
      customDialog(
        context,
        BorderDataSetting(
          bloc: widget.bloc,
          onUpdateBorder: updateBorderSide,
          currentBorder: border,
        ),
      );
    } else {
      // border side is exists
      void updateBorderSide(dynamic data) {
        setState(() {
          borderSide = data;
        });
      }

      customDialog(
        context,
        BorderDataSetting(
          bloc: widget.bloc,
          onUpdateBorder: updateBorderSide,
          currentBorder: borderSide,
        ),
      );
    }
  }

  // This function is used to add numbers categories.
  void addImage() {
    setState(() {
      final i = BuilderRepository().generateJsonImageData();
      image = i;
    });
  }

  // This function is used to remove a product from the list of products added.
  void removeImage() {
    dataSetting.removeDataOnSetting(image['id']);
    setState(() {
      image = {};
    });
  }

  // This function is used to edit a product.
  void editImage() {
    void updateImage(dynamic data) {
      setState(() {
        image = data;
      });
      dataSetting.updateDataOnSetting(image['id'], image);
    }

    customDialog(
      context,
      ImageDataSetting(
        widgetId: image['id'],
        bloc: widget.bloc,
        onUpdateImage: updateImage,
        currentImage: image,
      ),
    );
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    // String bgColorData = '0x${bgColor.value.toRadixString(16)}';

    Map<String, dynamic> data = {
      "imageUrl": controllerImageUrl.text,
      "image": image['id'] ?? '',
      "boxFit": convertFitToString(),
      "color": controllerColorBg.text,
      "border": borderSide,
      "borderRadius": ConvertUnit.stringToUnit(
          controllerBorderRadius.text, selectedUnitBorderRadius!),
      "backgroundBlendMode": selectedBlendMode
    };

    if (image.isNotEmpty) {
      dataSetting.addDataToSetting(image['id'], image);
    }

    widget.onUpdateData!(data);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 520,
      height: 520,
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Box Decoration'),
          // Tab bar includes two tab: Categories and Settings
          DefaultTabController(
            length: 2,
            child: Container(
              alignment: Alignment.topLeft,
              height: 36,
              decoration: BoxDecoration(color: Color(0xFF7e3BD0)),
              child: TabBar(
                  controller: _tabController,
                  labelColor: Colors.white,
                  indicator: BoxDecoration(color: Color(0xFF8f42ec)),
                  tabs: const [
                    Tab(
                      child: Text("Data", style: TextStyle(fontSize: 16)),
                    ),
                    Tab(
                      child: Text('Settings', style: TextStyle(fontSize: 16)),
                    )
                  ]),
            ),
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: TabBarView(
                controller: _tabController,
                children: [
                  // TAB ADD CATEGORIES
                  buildTabData(context),
                  // TAB CATEGORY SETTINGS
                  buildTabSettings(context)
                ],
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }

  // TAB ADD PRODUCTS
  ListView buildTabData(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(right: 10),
          child: Column(
            children: [
              Row(
                children: [
                  customText(
                      'Add new background image', mediumSize, textNormal),

                  // BUTTON ADD IMAGE --------------------------------
                  image.isEmpty
                      ? IconButton(
                          onPressed: addImage,
                          icon: Icon(Icons.add),
                          color: Colors.blue,
                        )
                      : Container()
                ],
              ),
              image.isNotEmpty ? buildImage() : Container(),
            ],
          ),
        ),
      ],
    );
  }

  // TAB CATEGORY SETTINGS
  ListView buildTabSettings(BuildContext context) {
    return ListView(
      padding: EdgeInsets.all(18),
      children: [
        // IMAGE URL -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Image url', controllerImageUrl),
        SizedBox(height: 20),

        // FIT IMAGE OPTIONS -----------------------------------
        Align(
          alignment: Alignment.topLeft,
          child: customText('Fit image', mediumSize, textBold),
        ),
        buildFitOptions(),

        // ADJUST BACKGROUND COLOR -----------------------------------
        ReusableSetting.buildInputSetting(
            context, 'Background color', controllerColorBg),
        SizedBox(height: 20),

        // ALIGNMENT -----------------------------------
        Align(
          alignment: Alignment.topLeft,
          child: customText('Background blend mode', mediumSize, textBold),
        ),
        ReusableSetting.buildDropdownMenu(context, selectedBlendMode!,
            changeValueBlendMode, blendModeOptions),
        SizedBox(height: 20),

        Row(children: [
          SizedBox(
            width: 200,
            child: ReusableSetting.buildAdjustDouble(context, 'Border radius',
                controllerBorderRadius, increaseValue, decreaseValue),
          ),
          Spacer(),
          ReusableSetting.buildDropdownMenu(context, selectedUnitBorderRadius!,
              changeValueUnitBorderRadius, ['none', 'w', 'h', 'sp'])
        ]),
        SizedBox(height: 20),

        Row(
          children: [
            customText('Border style', mediumSize, textBold),
            SizedBox(width: 10),
            IconButton(
                onPressed: editBorderSide,
                icon: Icon(Icons.settings, size: 18, color: Colors.teal))
          ],
        ),
      ],
    );
  }

  // This method is used to display a category that added.
  Widget buildImage() {
    return Padding(
        padding: EdgeInsets.only(top: 10),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey[300],
          ),
          child: Row(
            children: [
              Text(
                'New background image',
                // [112302TIN] makeup
                style: TextStyle(
                  color: Colors.black87,
                ),
              ),
              Spacer(),
              Row(
                children: [
                  // BUTTON EDIT IMAGE --------------------------------
                  IconButton(
                    onPressed: editImage,
                    icon: Icon(Icons.edit, size: 18),
                    color: Colors.teal,
                  ),

                  // BUTTON REMOVE IMAGE --------------------------------
                  IconButton(
                    onPressed: removeImage,
                    icon: Icon(Icons.delete, size: 18),
                    color: Colors.red,
                  ),
                ],
              )
            ],
          ),
        ));
  }

  // This method is used to build a box that allows to choose radio options.
  Row buildFitOptions() {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'Cover',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.cover,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'Contain',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.contain,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'Fill',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.fill,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'None',
              style: TextStyle(fontSize: 14),
            ),
            leading: Radio(
              value: Fit.none,
              groupValue: _fitSelected,
              onChanged: changeFitValue,
            ),
          ),
        ),
      ],
    );
  }
}
