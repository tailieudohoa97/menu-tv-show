import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class MarginPaddingSetting extends StatefulWidget {
  const MarginPaddingSetting(
      {super.key,
      required this.bloc,
      this.onUpdateData,
      this.currentData,
      required this.type});

  final String type;
  final BuilderBloc bloc;
  final Function? onUpdateData;
  final Map<String, dynamic>? currentData;

  @override
  State<MarginPaddingSetting> createState() => _MarginPaddingSettingState();
}

class _MarginPaddingSettingState extends State<MarginPaddingSetting> {
  TextEditingController controllerAll = TextEditingController();
  TextEditingController controllerVertical = TextEditingController();
  TextEditingController controllerHorizontal = TextEditingController();
  TextEditingController controllerTop = TextEditingController();
  TextEditingController controllerRight = TextEditingController();
  TextEditingController controllerBottom = TextEditingController();
  TextEditingController controllerLeft = TextEditingController();

  String? selectedUnitAll,
      selectedUnitVertical,
      selectedUnitHorizontal,
      selectedUnitTop,
      selectedUnitRight,
      selectedUnitBottom,
      selectedUnitLeft;

  @override
  void initState() {
    super.initState();
    final currentSetting = widget.currentData!;

    controllerAll.text = ConvertUnit.unitToString(currentSetting['all']);
    controllerVertical.text =
        ConvertUnit.unitToString(currentSetting['vertical']);
    controllerHorizontal.text =
        ConvertUnit.unitToString(currentSetting['horizontal']);
    controllerTop.text = ConvertUnit.unitToString(currentSetting['top']);
    controllerRight.text = ConvertUnit.unitToString(currentSetting['right']);
    controllerBottom.text = ConvertUnit.unitToString(currentSetting['bottom']);
    controllerLeft.text = ConvertUnit.unitToString(currentSetting['left']);

    selectedUnitAll = ConvertUnit.getUnit(currentSetting['all']);
    selectedUnitVertical = ConvertUnit.getUnit(currentSetting['vertical']);
    selectedUnitHorizontal = ConvertUnit.getUnit(currentSetting['horizontal']);
    selectedUnitTop = ConvertUnit.getUnit(currentSetting['top']);
    selectedUnitRight = ConvertUnit.getUnit(currentSetting['right']);
    selectedUnitBottom = ConvertUnit.getUnit(currentSetting['bottom']);
    selectedUnitLeft = ConvertUnit.getUnit(currentSetting['left']);
  }

  void changeValueUnitAll(String? value) {
    setState(() {
      selectedUnitAll = value!;
    });
  }

  void changeValueUnitVertical(String? value) {
    setState(() {
      selectedUnitVertical = value!;
    });
  }

  void changeValueUnitHorizontal(String? value) {
    setState(() {
      selectedUnitHorizontal = value!;
    });
  }

  void changeValueUnitTop(String? value) {
    setState(() {
      selectedUnitTop = value!;
    });
  }

  void changeValueUnitRight(String? value) {
    setState(() {
      selectedUnitRight = value!;
    });
  }

  void changeValueUnitBottom(String? value) {
    setState(() {
      selectedUnitBottom = value!;
    });
  }

  void changeValueUnitLeft(String? value) {
    setState(() {
      selectedUnitLeft = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "all": ConvertUnit.stringToUnit(controllerAll.text, selectedUnitAll!),
      "vertical": ConvertUnit.stringToUnit(
          controllerVertical.text, selectedUnitVertical!),
      "horizontal": ConvertUnit.stringToUnit(
          controllerHorizontal.text, selectedUnitHorizontal!),
      "top": ConvertUnit.stringToUnit(controllerTop.text, selectedUnitTop!),
      "right":
          ConvertUnit.stringToUnit(controllerRight.text, selectedUnitRight!),
      "bottom":
          ConvertUnit.stringToUnit(controllerBottom.text, selectedUnitBottom!),
      "left": ConvertUnit.stringToUnit(controllerLeft.text, selectedUnitLeft!)
    };

    widget.onUpdateData!(data);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
    child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(
              context, 'Setting ${widget.type} Data'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'All',
                              controllerAll,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitAll!,
                            changeValueUnitAll,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),

                    //
                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Vertical',
                              controllerVertical,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitVertical!,
                            changeValueUnitVertical,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),

                    //
                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Horizontal',
                              controllerHorizontal,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitHorizontal!,
                            changeValueUnitHorizontal,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),

                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Top',
                              controllerTop,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitTop!,
                            changeValueUnitTop,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),

                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Right',
                              controllerRight,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitRight!,
                            changeValueUnitRight,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),

                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Bottom',
                              controllerBottom,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitBottom!,
                            changeValueUnitBottom,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),

                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Left',
                              controllerLeft,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitLeft!,
                            changeValueUnitLeft,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
