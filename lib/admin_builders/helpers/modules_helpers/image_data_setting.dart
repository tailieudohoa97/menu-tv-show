import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class ImageDataSetting extends StatefulWidget {
  const ImageDataSetting(
      {super.key,
      required this.widgetId,
      required this.bloc,
      this.onUpdateImage,
      this.currentImage});

  final String widgetId;
  final BuilderBloc bloc;
  final Function? onUpdateImage;
  final Map<String, dynamic>? currentImage;

  @override
  State<ImageDataSetting> createState() => _ImageDataSettingState();
}

class _ImageDataSettingState extends State<ImageDataSetting> {
  TextEditingController controllerImagePath = TextEditingController();

  @override
  void initState() {
    super.initState();

    final currentSetting = widget.currentImage!;
    controllerImagePath.text = currentSetting["image"];
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "id": widget.widgetId,
      "type": "image",
      "image": controllerImagePath.text,
    };

    widget.onUpdateImage!(data);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 200,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Image Data'),
          Expanded(
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.all(18),
              child:
                  // ADJUST PRODUCT NAME -----------------------------------
                  ReusableSetting.buildInputSetting(
                      context, 'Image path', controllerImagePath),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
