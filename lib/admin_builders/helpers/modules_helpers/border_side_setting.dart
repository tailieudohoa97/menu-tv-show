import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class BorderSideSetting extends StatefulWidget {
  const BorderSideSetting(
      {super.key,
      required this.bloc,
      this.onUpdateBorderSide,
      this.currentBorderSide});

  final BuilderBloc bloc;
  final Function? onUpdateBorderSide;
  final Map<String, dynamic>? currentBorderSide;

  @override
  State<BorderSideSetting> createState() => _BorderSideSettingState();
}

class _BorderSideSettingState extends State<BorderSideSetting> {
  TextEditingController controllerBorderWidth = TextEditingController();
  TextEditingController controllerColorBorder = TextEditingController();

  String? selectedUnitWidth;

  @override
  void initState() {
    super.initState();
    final currentSetting = widget.currentBorderSide!;

    controllerBorderWidth.text =
        ConvertUnit.unitToString(currentSetting['width']);
    controllerColorBorder.text = currentSetting['color'];

    selectedUnitWidth = ConvertUnit.getUnit(currentSetting['width']);
  }

  void changeValueUnitWidth(String? value) {
    setState(() {
      selectedUnitWidth = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "width": ConvertUnit.stringToUnit(
          controllerBorderWidth.text, selectedUnitWidth!),
      "color": controllerColorBorder.text
    };

    widget.onUpdateBorderSide!(data);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 258,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Border Side'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADJUST BORDER COLOR
                    ReusableSetting.buildInputSetting(
                        context, 'Border color', controllerColorBorder),
                    SizedBox(height: 20),

                    // ADJUST BORDER WIDTH -----------------------------------
                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Border width',
                              controllerBorderWidth,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitWidth!,
                            changeValueUnitWidth,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
