import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class CategoryDataSetting extends StatefulWidget {
  const CategoryDataSetting(
      {super.key,
      required this.widgetId,
      required this.bloc,
      this.onUpdateCategory,
      this.currentCategory});

  final String widgetId;
  final BuilderBloc bloc;
  final Function? onUpdateCategory;
  final Map<String, dynamic>? currentCategory;

  @override
  State<CategoryDataSetting> createState() => _CategoryDataSettingState();
}

class _CategoryDataSettingState extends State<CategoryDataSetting> {
  TextEditingController controllerCatName = TextEditingController();
  TextEditingController controllerCatImage = TextEditingController();
  TextEditingController controllerCatDescription = TextEditingController();

  @override
  void initState() {
    super.initState();
    final currentSetting = widget.currentCategory!;

    controllerCatName.text = currentSetting["name"];
    controllerCatImage.text = currentSetting["image"];
    controllerCatDescription.text = currentSetting["description"];
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "id": widget.widgetId,
      "type": "category",
      "name": controllerCatName.text,
      "image": controllerCatImage.text,
      "description": controllerCatDescription.text
    };

    widget.onUpdateCategory!(data);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 325,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Category Data'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADJUST CATEGORY NAME -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Category name', controllerCatName),
                    SizedBox(height: 20),

                    // ADJUST CATEGORY IMAGE -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Category image', controllerCatImage),
                    SizedBox(height: 20),

                    // ADJUST CATEGORY DESCRIPTION -----------------------------------
                    ReusableSetting.buildInputSetting(context,
                        'Category description', controllerCatDescription),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
