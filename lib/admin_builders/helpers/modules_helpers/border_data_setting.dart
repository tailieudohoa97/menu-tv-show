import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';
import 'package:tv_menu/admin_builders/helpers/modules_helpers/border_side_setting.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/convert_unit.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

class BorderDataSetting extends StatefulWidget {
  const BorderDataSetting(
      {super.key, required this.bloc, this.onUpdateBorder, this.currentBorder});

  final BuilderBloc bloc;
  final Function? onUpdateBorder;
  final Map<String, dynamic>? currentBorder;

  @override
  State<BorderDataSetting> createState() => _BorderDataSettingState();
}

class _BorderDataSettingState extends State<BorderDataSetting> {
  TextEditingController controllerBorderWidth = TextEditingController();
  TextEditingController controllerBorderColor = TextEditingController();

  String? selectedUnitWidth;
  dynamic borderAll = {},
      borderTop = {},
      borderRight = {},
      borderBottom = {},
      borderLeft = {};

  @override
  void initState() {
    super.initState();
    final currentSetting = widget.currentBorder!;

    borderAll = currentSetting["all"] ?? {};
    borderTop = currentSetting["top"] ?? {};
    borderRight = currentSetting["right"] ?? {};
    borderBottom = currentSetting["bottom"] ?? {};
    borderLeft = currentSetting["left"] ?? {};

    controllerBorderWidth.text =
        ConvertUnit.unitToString(currentSetting['width']);
    controllerBorderColor.text = currentSetting['color'];

    selectedUnitWidth = ConvertUnit.getUnit(currentSetting['width']);
  }

  void changeValueUnitWidth(String? value) {
    setState(() {
      selectedUnitWidth = value!;
    });
  }

  // This function is used to increase value of current text controller.
  void increaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue++;
      currentController.text = (currentValue).toString(); // incrementing value
    });
  }

  // This function is used to decrease value of current text controller.
  void decreaseValue(
      int currentValue, TextEditingController currentController) {
    setState(() {
      currentValue--;
      currentController.text = (currentValue > 0 ? currentValue : 0)
          .toString(); // decrementing value
    });
  }

  // Border bottom side
  void editBorderAll() {
    // if borderSide current is empty
    if (borderAll.isEmpty) {
      // new setting border
      var border = BuilderRepository().generateJsonBorderSide();

      // update values after edit
      void updateBorderSide(dynamic data) {
        border = data;
        setState(() {
          borderAll = border;
        });
      }

      // pass new setting to BorderSideSetting
      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: border,
        ),
      );
    } else {
      // border side is exists
      void updateBorderSide(dynamic data) {
        setState(() {
          borderAll = data;
        });
      }

      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: borderAll,
        ),
      );
    }
  }

  void editBorderTop() {
    // if borderSide current is empty
    if (borderTop.isEmpty) {
      // new setting border
      var border = BuilderRepository().generateJsonBorderSide();

      // update values after edit
      void updateBorderSide(dynamic data) {
        border = data;
        setState(() {
          borderTop = border;
        });
      }

      // pass new setting to BorderSideSetting
      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: border,
        ),
      );
    } else {
      // border side is exists
      void updateBorderSide(dynamic data) {
        setState(() {
          borderTop = data;
        });
      }

      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: borderTop,
        ),
      );
    }
  }

  void editBorderRight() {
    // if borderSide current is empty
    if (borderRight.isEmpty) {
      // new setting border
      var border = BuilderRepository().generateJsonBorderSide();

      // update values after edit
      void updateBorderSide(dynamic data) {
        border = data;
        setState(() {
          borderRight = border;
        });
      }

      // pass new setting to BorderSideSetting
      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: border,
        ),
      );
    } else {
      // border side is exists
      void updateBorderSide(dynamic data) {
        setState(() {
          borderRight = data;
        });
      }

      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: borderRight,
        ),
      );
    }
  }

  void editBorderBottom() {
    // if borderSide current is empty
    if (borderBottom.isEmpty) {
      // new setting border
      var border = BuilderRepository().generateJsonBorderSide();

      // update values after edit
      void updateBorderSide(dynamic data) {
        border = data;
        setState(() {
          borderBottom = border;
        });
      }

      // pass new setting to BorderSideSetting
      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: border,
        ),
      );
    } else {
      // border side is exists
      void updateBorderSide(dynamic data) {
        setState(() {
          borderBottom = data;
        });
      }

      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: borderBottom,
        ),
      );
    }
  }

  void editBorderLeft() {
    // if borderSide current is empty
    if (borderLeft.isEmpty) {
      // new setting border
      var border = BuilderRepository().generateJsonBorderSide();

      // update values after edit
      void updateBorderSide(dynamic data) {
        border = data;
        setState(() {
          borderLeft = border;
        });
      }

      // pass new setting to BorderSideSetting
      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: border,
        ),
      );
    } else {
      // border side is exists
      void updateBorderSide(dynamic data) {
        setState(() {
          borderLeft = data;
        });
      }

      customDialog(
        context,
        BorderSideSetting(
          bloc: widget.bloc,
          onUpdateBorderSide: updateBorderSide,
          currentBorderSide: borderLeft,
        ),
      );
    }
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "color": controllerBorderColor.text,
      "width": ConvertUnit.stringToUnit(
          controllerBorderWidth.text, selectedUnitWidth!),
      "all": borderAll,
      "top": borderTop,
      "right": borderRight,
      "bottom": borderBottom,
      "left": borderLeft
    };

    widget.onUpdateBorder!(data);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 520,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Setting Border Data'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    ReusableSetting.buildInputSetting(
                        context, "Border color", controllerBorderColor),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        SizedBox(
                          width: 200,
                          child: ReusableSetting.buildAdjustDouble(
                              context,
                              'Border width',
                              controllerBorderWidth,
                              increaseValue,
                              decreaseValue),
                        ),
                        Spacer(),
                        ReusableSetting.buildDropdownMenu(
                            context,
                            selectedUnitWidth!,
                            changeValueUnitWidth,
                            ['none', 'w', 'h', 'sp'])
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        customText('Border all', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editBorderAll,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        customText('Border top', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editBorderTop,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        customText('Border right', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editBorderRight,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        customText('Border bottom', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editBorderBottom,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        customText('Border left', mediumSize, textBold),
                        SizedBox(width: 10),
                        IconButton(
                            onPressed: editBorderLeft,
                            icon: Icon(Icons.settings,
                                size: 18, color: Colors.teal))
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
