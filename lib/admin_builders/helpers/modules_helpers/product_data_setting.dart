import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/helpers/reusable/reusable_settings.dart';

class ProductDataSetting extends StatefulWidget {
  const ProductDataSetting(
      {super.key,
      required this.widgetId,
      required this.bloc,
      this.onUpdateProduct,
      this.currentProduct});

  final String widgetId;
  final BuilderBloc bloc;
  final Function? onUpdateProduct;
  final Map<String, dynamic>? currentProduct;

  @override
  State<ProductDataSetting> createState() => _ProductDataSettingState();
}

class _ProductDataSettingState extends State<ProductDataSetting> {
  TextEditingController controllerName = TextEditingController();
  TextEditingController controllerPrice = TextEditingController();
  TextEditingController controllerSalePrice = TextEditingController();
  TextEditingController controllerImage = TextEditingController();
  TextEditingController controllerDescription = TextEditingController();

  @override
  void initState() {
    super.initState();
    final currentSetting = widget.currentProduct!;

    controllerName.text = currentSetting["name"];
    controllerPrice.text = currentSetting["price"];
    controllerSalePrice.text = currentSetting["salePrice"];
    controllerDescription.text = currentSetting["description"];
  }

  // This function is used to save and submit the changes of setting.
  void saveSettings() {
    Map<String, dynamic> data = {
      "id": widget.widgetId,
      "type": "product",
      "name": controllerName.text,
      "price": controllerPrice.text,
      "salePrice": controllerSalePrice.text,
      "image": controllerImage.text,
      "description": controllerDescription.text
    };

    widget.onUpdateProduct!(data);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 520,
      height: 460,
      constraints: BoxConstraints(maxHeight: 520),
      child: Column(
        children: [
          ReusableSetting.buildHeaderSetting(context, 'Product Data'),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.all(18),
                child: Column(
                  children: [
                    // ADJUST PRODUCT NAME -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Name', controllerName),
                    SizedBox(height: 20),

                    // ADJUST PRODUCT PRICE -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Price', controllerPrice),
                    SizedBox(height: 20),

                    // ADJUST PRODUCT SALE PRICE -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Sale price', controllerSalePrice),
                    SizedBox(height: 20),

                    // ADJUST PRODUCT IMAGE -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Image', controllerImage),
                    SizedBox(height: 20),

                    // ADJUST PRODUCT DESCRIPTION -----------------------------------
                    ReusableSetting.buildInputSetting(
                        context, 'Description', controllerDescription),
                  ],
                ),
              ),
            ),
          ),
          ReusableSetting.buildActionsForm(context, saveSettings),
        ],
      ),
    );
  }
}
