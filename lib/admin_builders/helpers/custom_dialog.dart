import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/constants/app_text.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';

/* [092301TREE]
1. This function receives parameters: context, title, action, description, handleTask
- context: where the function is called
- title: the title of the alert dialog
- action: the name of button to confirm alert
- description: the description for the alert dialog
- handleTask: the function is called when confirm the alert dialog
*/

void customAlertDialog(BuildContext context, String title, String action,
    String description, Function handleTask,
    [String titleDraft = '', Function? moveToDraft]) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(title),
        content: Text(description),
        actions: [
          // button cancel
          TextButton(
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(kGrayColor),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: customText(cancelText, smallSize, textNormal, kWhiteColor),
            ),
          ),
          titleDraft.isNotEmpty
              ? TextButton(
                  style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(kTealColor),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                    moveToDraft!();
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: customText(
                        titleDraft, smallSize, textNormal, kWhiteColor),
                  ),
                )
              : Text(''),
          // button confirm
          TextButton(
            style: const ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(kTealColor),
            ),
            onPressed: () {
              handleTask();
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: customText(action, smallSize, textNormal, kWhiteColor),
            ),
          ),
        ],
      );
    },
  );
}

void customDialog(BuildContext context, Widget widget) {
  showDialog(
    barrierColor: Colors.white.withOpacity(0),
    context: context,
    builder: (context) {
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 5,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            // [112302TIN] use light theme for builder dialog
            child: Theme(
              data: ThemeData.light(),
              child: widget,
            )),
      );
    },
  );
}

// alert options add module
void optionAddModule(BuildContext context, Widget widget) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        contentPadding: EdgeInsets.all(10),
        content: IntrinsicHeight(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                alignment: Alignment.topRight,
                child: InkWell(
                  borderRadius: BorderRadius.circular(16),
                  onTap: () => Navigator.pop(context),
                  child: Padding(
                    padding: EdgeInsets.all(8),
                    child: Icon(
                      Icons.close,
                      size: 20,
                      color: Colors.red,
                    ),
                  ),
                ),
              ),
              widget
            ],
          ),
        ),
      );
    },
  );
}
