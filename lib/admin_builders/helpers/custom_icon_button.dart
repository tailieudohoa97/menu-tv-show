import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/constants/app_text.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';

/* [092305TREE]
Custom simple text button to reuse everywhere
*/
Widget customIconBtn(String title, Function submitButton) {
  // function get color button
  Color getBgColor() {
    switch (title) {
      case visibleType:
        return kBlackColor;
      case addType:
        return kBlueColor;
      case editType:
        return kTealColor;
      case deleteType:
        return kRedColor;
      case editBuilder:
        return kBlueColor;
      default:
        return kGrayColor;
    }
  }

  // function get icon button
  IconData getIcon() {
    switch (title) {
      case visibleType:
        return Icons.visibility;
      case addType:
        return Icons.add;
      case editType:
        return Icons.edit;
      case deleteType:
        return Icons.delete;
      case editBuilder:
        return Icons.build_circle;
      default:
        return Icons.close; // default fake icon
    }
  }

  return IconButton(
      onPressed: () {
        submitButton();
      },
      icon: Icon(getIcon(), size: 20, color: getBgColor()));
}
