import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';

class ReusableSetting {
  // This method is used to build header setting with its title.
  static Widget buildHeaderSetting(BuildContext context, String title) {
    return Container(
      color: Color(0xFF6c2eb9),
      padding: EdgeInsets.all(12),
      child: Row(
        children: [
          customText(title, largeSize, textBold, kWhiteColor),
          Spacer(),
          IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(
              Icons.close,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }

  // This method is used to build actions submit, includes cancel and save.
  static Widget buildActionsForm(BuildContext context, Function()? submitForm) {
    return Row(
      children: [
        Expanded(
            flex: 1,
            child: Container(
              color: Color(0xFFef5555),
              alignment: Alignment.center,
              child: IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: Icon(Icons.close, color: Colors.white)),
            )),
        Expanded(
          flex: 1,
          child: Container(
            color: Color(0xFF29c4a9),
            alignment: Alignment.center,
            child: IconButton(
                onPressed: submitForm,
                icon: Icon(Icons.done, color: Colors.white)),
          ),
        ),
      ],
    );
  }

  // This method is used to build a dropdown menu, with many options.
  static Widget buildDropdownMenu(
    BuildContext context,
    String selectedValue,
    Function(String?) onChanged,
    List<String> options,
  ) {
    return Container(
      alignment: Alignment.topLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DropdownButton<String>(
            value: selectedValue,
            onChanged: onChanged,
            items: options.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }

  // This method is used to build a box color that allows to adjust and choose color.
  // static Widget buildAdjustColor(BuildContext context, String title,
  //     Color color, Function(Color) onChanged) {
  //   return Container(
  //     alignment: Alignment.topLeft,
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: [
  //         customText(title, mediumSize, textBold),
  //         SizedBox(height: 10),
  //         Row(
  //           children: [
  //             Container(
  //               decoration: BoxDecoration(
  //                   shape: BoxShape.circle,
  //                   color: color,
  //                   border: Border.all(width: 0.2, color: Colors.black)),
  //               width: 30,
  //               height: 30,
  //             ),
  //             SizedBox(width: 20),
  //             ElevatedButton(
  //               onPressed: () {
  //                 showDialog(
  //                   context: context,
  //                   builder: (BuildContext context) {
  //                     return AlertDialog(
  //                       title: Text('Select a color'),
  //                       content: SingleChildScrollView(
  //                         child: ColorPicker(
  //                           hexInputBar: true,
  //                           pickerColor: color,
  //                           onColorChanged: onChanged,
  //                           pickerAreaHeightPercent: 0.8,
  //                         ),
  //                       ),
  //                       actions: [
  //                         TextButton(
  //                           onPressed: () {
  //                             Navigator.pop(context);
  //                           },
  //                           child: Text('OK'),
  //                         ),
  //                       ],
  //                     );
  //                   },
  //                 );
  //               },
  //               child: Text(
  //                 'Pick a color',
  //                 style: TextStyle(fontSize: 14),
  //               ),
  //             ),
  //           ],
  //         )
  //       ],
  //     ),
  //   );
  // }

  // This method is used to build a box that allows to adjust increase or decrease number.
  static Widget buildAdjustNumber(BuildContext context, String title,
      TextEditingController controller, Function increase, Function decrease) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: TextFormField(
            textAlign: TextAlign.center,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.zero,
                label: customText(title, mediumSize, textBold)),
            controller: controller,
            keyboardType: TextInputType.numberWithOptions(
              decimal: false,
              signed: true,
            ),
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
          ),
        ),
        SizedBox(
          // width: 40.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 0.5,
                    ),
                  ),
                ),
                child: InkWell(
                  child: Icon(
                    Icons.arrow_drop_up,
                    size: 18.0,
                  ),
                  onTap: () {
                    int currentValue = int.parse(controller.text);
                    increase(currentValue, controller);
                  },
                ),
              ),
              InkWell(
                child: Icon(
                  Icons.arrow_drop_down,
                  size: 18.0,
                ),
                onTap: () {
                  int currentValue = int.parse(controller.text);
                  decrease(currentValue, controller);
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  // This method is used to build a box that allows to choose radio options.
  static Widget buildAdjustDouble(BuildContext context, String title,
      TextEditingController controller, Function increase, Function decrease,
      {double? max}) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: TextFormField(
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.zero,
              label: customText(title, mediumSize, textBold),
            ),
            controller: controller,
            keyboardType: TextInputType.numberWithOptions(
              decimal: true,
              signed: true,
            ),
            // inputFormatters: <TextInputFormatter>[
            //   FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,1}')),
            // ],
          ),
        ),
        SizedBox(
          // width: 40.0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 0.5,
                    ),
                  ),
                ),
                child: InkWell(
                  child: Icon(
                    Icons.arrow_drop_up,
                    size: 18.0,
                  ),
                  onTap: () {
                    double currentValue = double.parse(controller.text);
                    double newValue = (currentValue + 0.1).toDouble();
                    controller.text = newValue.toStringAsFixed(1);
                    // adjust opacity
                    if (max != null && max > 1) {
                      newValue = 1.0;
                    } else {
                      increase(newValue, controller);
                    }
                  },
                ),
              ),
              InkWell(
                child: Icon(
                  Icons.arrow_drop_down,
                  size: 18.0,
                ),
                onTap: () {
                  double currentValue = double.parse(controller.text);
                  double newValue = (currentValue - 0.1).toDouble();
                  if (newValue < 0) {
                    newValue = 0;
                  }
                  controller.text = newValue.toStringAsFixed(1);
                  decrease(newValue, controller);
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  // This method is used to build a switch on, off.
  static Widget buildToggleSwitch(BuildContext context, String title,
      bool isToggle, Function(bool)? onChanged) {
    return Row(
      children: [
        customText(title, mediumSize, textBold),
        const SizedBox(width: 10),
        Switch(
          value: isToggle,
          onChanged: onChanged,
        )
      ],
    );
  }

  // This method is used to build a switch on, off.
  static Widget buildInputSetting(BuildContext context, String labelText,
      TextEditingController controllerText) {
    return TextField(
      controller: controllerText,
      maxLines: null,
      decoration: InputDecoration(
        labelText: labelText,
        contentPadding: EdgeInsets.all(5),
        labelStyle: TextStyle(fontSize: 16),
      ),
    );
  }
}
