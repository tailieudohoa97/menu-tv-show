// build ui each module: includes icon and name
import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_bloc.dart';
import 'package:tv_menu/admin_builders/helpers/custom_dialog.dart';
import 'package:tv_menu/admin_builders/views/components/inserts/insert_module.dart';

// build ui each module: includes icon and name
Widget buildModuleOption(IconData icon, String text) {
  return Container(
    color: Color(0xFFf1f5f9),
    alignment: Alignment.center,
    child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Icon(
        icon,
        color: Color(0xFF2b87da),
      ),
      SizedBox(height: 4),
      Text(
        text,
        // [112302TIN] makeup
        style: TextStyle(fontSize: 14, color: Colors.black54),
      )
    ]),
  );
}

// button add module
ElevatedButton buildButtonAdd(
    BuildContext context, String widgetId, BuilderBloc bloc, Color color) {
  return ElevatedButton(
    onPressed: () {
      customDialog(context, InsertModule(widgetId: widgetId, bloc: bloc));
    },
    style: ElevatedButton.styleFrom(
      backgroundColor: color,
      shape: CircleBorder(),
      padding: EdgeInsets.all(12),
    ),
    child: Icon(
      Icons.add,
      size: 16,
      color: Colors.white,
    ),
  );
}
