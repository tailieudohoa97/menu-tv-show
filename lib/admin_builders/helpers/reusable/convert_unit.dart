class ConvertUnit {
  static String unitToString(String input) {
    if (input.endsWith('.w')) {
      return input.substring(0, input.length - 2);
    } else if (input.endsWith('.h')) {
      return input.substring(0, input.length - 2);
    } else if (input.endsWith('.sp')) {
      return input.substring(0, input.length - 3);
    } else {
      return input;
    }
  }

  static String stringToUnit(String input, String unit) {
    if (unit == "w") {
      return '$input.$unit';
    } else if (unit == "h") {
      return '$input.$unit';
    } else if (unit == "sp") {
      return '$input.$unit';
    } else {
      return input.toString();
    }
  }

  static String getUnit(String input) {
    if (input.endsWith(".w")) {
      return "w";
    } else if (input.endsWith(".h")) {
      return "h";
    } else if (input.endsWith(".sp")) {
      return "sp";
    } else {
      return "none";
    }
  }
}
