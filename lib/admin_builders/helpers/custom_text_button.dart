import 'package:flutter/material.dart';
import 'package:tv_menu/admin_builders/constants/app_text.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/helpers/custom_text.dart';

/* [092305TREE]
Custom simple text button to reuse everywhere
*/
Widget customTextBtn(String title, Function submitButton) {
  // function get color button
  Color getBgColor() {
    switch (title) {
      case addTemplateAction:
        return kTealColor;
      case editTemplateAction:
        return kTealColor;
      case editType:
        return kTealColor;
      case deleteType:
        return kRedColor;
      default:
        return kBlueColor;
    }
  }

  return TextButton(
    style: ButtonStyle(
      backgroundColor: MaterialStatePropertyAll(getBgColor()),
    ),
    onPressed: () {
      submitButton();
    },
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: customText(title, smallSize, textNormal, kWhiteColor),
    ),
  );
}
