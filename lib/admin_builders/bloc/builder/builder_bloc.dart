import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_event.dart';
import 'package:tv_menu/admin_builders/bloc/builder/builder_state.dart';
import 'package:tv_menu/admin_builders/helpers/widget_helper.dart';
import 'package:tv_menu/admin_builders/repo/local_db.dart';
import 'package:tv_menu/admin_builders/repo/builders/builder_repo.dart';

class BuilderBloc extends Bloc<BuilderEvent, BuilderState> {
  final BuilderRepository repository;

  BuilderBloc({required this.repository}) : super(const BuilderState()) {
    on<GetSettingEvent>(_onGetSettingEvent);
    on<ChangeSettingEvent>(_onChangeSettingEvent);
    on<AddModuleEvent>(_onAddModuleEvent);
    on<RemoveModuleEvent>(_onRemoveWidget);
    on<ClearModulesEvent>(_onClearModules);
    on<UpdateModuleEvent>(_onUpdateModule);
    on<GetModuleEvent>(_onGetModule);
    on<ChangeModuleNameEvent>(_onChangeModuleName);
  }
  // get current setting
  void _onGetSettingEvent(
      GetSettingEvent event, Emitter<BuilderState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      // get setting from local storage
      if (event.dataSetting != null && event.dataSetting!.isNotEmpty) {
        await LocalDB.saveDataToLocal('dataSetting', event.dataSetting!);
      }
      if (event.widgetSetting != null && event.widgetSetting!.isNotEmpty) {
        var settingCurrent =
            repository.addEmptySettings(jsonDecode(event.widgetSetting!));
        await LocalDB.saveDataToLocal(
            'widget_settings', jsonEncode(settingCurrent));
      }
      if (event.radioSetting != null && event.radioSetting!.isNotEmpty) {
        await LocalDB.saveDataToLocal('radioSetting', event.radioSetting!);
      }
      var settings = await LocalDB.getDataFromLocal('widget_settings');
      // update state
      await Future.delayed(const Duration(milliseconds: 500));
      emit(state.copyWith(
        settings: settings,
        isLoading: false,
      ));
    } catch (e) {
      emit(state.copyWith(isLoading: false));
    }
  }

  // get current setting
  void _onChangeSettingEvent(
      ChangeSettingEvent event, Emitter<BuilderState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      await LocalDB.saveDataToLocal(
          'widget_settings', (jsonEncode(event.settings)));
      await LocalDB.saveDataToLocal(
          'dataSetting', jsonEncode(event.dataSettings));
      await LocalDB.saveDataToLocal(
          'radioSetting', jsonEncode(event.radioSettings));
      // get setting from local storage
      final settings = await LocalDB.getDataFromLocal('widget_settings');
      // update state
      await Future.delayed(const Duration(milliseconds: 500));
      // print(settings);
      emit(state.copyWith(
        settings: settings,
        isLoading: false,
      ));
    } catch (e) {
      emit(state.copyWith(isLoading: false));
    }
  }

  // add section event
  void _onAddModuleEvent(
      AddModuleEvent event, Emitter<BuilderState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      // get current setting from local
      final settingLocal = await LocalDB.getDataFromLocal('widget_settings');
      // check if setting is empty
      if (settingLocal.isEmpty) {
        // remove key empty or null
        var tmp = repository.removeEmptyValues(event.model['widgetSetting']);

        event.model['widgetSetting'].addAll(tmp);
        await LocalDB.saveDataToLocal(
            'widget_settings', jsonEncode(event.model));
        await Future.delayed(const Duration(milliseconds: 500));
        // get setting again from local storage
        final newSettings = await LocalDB.getDataFromLocal('widget_settings');
        //update states
        emit(state.copyWith(
            isLoading: false,
            settings: newSettings,
            currentModuleId: event.model["id"]));
      } else {
        // give container to widget current
        final settings = await LocalDB.getDataFromLocal('widget_settings');
        final updatedSettings = jsonDecode(settings.toString());
        var currentSetting = repository.findId(updatedSettings, event.widgetId);
        dynamic widgetSetting = currentSetting['widgetSetting'];

        var tmp = repository.removeEmptyValues(event.model['widgetSetting']);
        event.model['widgetSetting'].addAll(tmp);
        // add new module to settings
        if (widgetSetting.containsKey('child')) {
          widgetSetting["child"] = event.model;
        } else if (widgetSetting?.containsKey('children')) {
          widgetSetting["children"].add(event.model);
        } else if (widgetSetting?.containsKey('productRows')) {
          widgetSetting["productRows"].add(event.model);
        } else if (widgetSetting?.containsKey('categoryItems')) {
          widgetSetting["categoryItems"].add(event.model);
        } else if (widgetSetting?.containsKey('menuItems')) {
          widgetSetting["menuItems"].add(event.model);
        } else if (widgetSetting?.containsKey('categoryItemRows')) {
          widgetSetting["categoryItemRows"].add(event.model);
        } else {
          return;
        }

        // store it to local storage
        await LocalDB.saveDataToLocal(
            'widget_settings', jsonEncode(updatedSettings));
        await Future.delayed(const Duration(milliseconds: 500));
        // get setting again from local storage
        final newSettings = await LocalDB.getDataFromLocal('widget_settings');
        // update states
        emit(state.copyWith(
            isLoading: false,
            settings: newSettings,
            currentModuleId: event.model["id"]));
      }
    } catch (error) {
      emit(state.copyWith(isLoading: false));
    }
  }

  // remove item layout
  void _onRemoveWidget(
      RemoveModuleEvent event, Emitter<BuilderState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      // get setting from local storage
      final settings = await LocalDB.getDataFromLocal('widget_settings');
      final updatedSettings = jsonDecode(settings.toString());
      final parentModule = findParentWidget(updatedSettings, event.widgetId);

      // remove widget
      repository.removeWidgetById(updatedSettings, event.widgetId);

      await Future.delayed(const Duration(milliseconds: 500));
      // Save the updated settings
      await LocalDB.saveDataToLocal(
          'widget_settings', jsonEncode(updatedSettings));
      final newSettings = await LocalDB.getDataFromLocal('widget_settings');
      // update state
      emit(state.copyWith(
          isLoading: false,
          settings: newSettings,
          currentModuleId: parentModule["id"]));
      // }
    } catch (error) {
      emit(state.copyWith(isLoading: false));
    }
  }

  // clear layout
  void _onClearModules(
      ClearModulesEvent event, Emitter<BuilderState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      // get setting from local storage
      final settings = await LocalDB.getDataFromLocal('widget_settings');
      final updatedSettings = jsonDecode(settings.toString());
      // clear data local
      repository.removeWidgetById(updatedSettings, updatedSettings['id']);
      // clear states local
      await LocalDB.clearDataLocal('widget_settings');
      await LocalDB.clearDataLocal('openStructures');
      await LocalDB.clearDataLocal('expandedList');
      await LocalDB.clearDataLocal('dataSetting');
      await LocalDB.clearDataLocal('radioSetting');

      await Future.delayed(const Duration(milliseconds: 500));
      final newSettings = await LocalDB.getDataFromLocal('widget_settings');

      // update state
      emit(state.copyWith(isLoading: false, settings: newSettings));
    } catch (error) {
      emit(state.copyWith(isLoading: false));
    }
  }

  // update setting module
  void _onUpdateModule(
      UpdateModuleEvent event, Emitter<BuilderState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      // get setting from local storage
      final settings = await LocalDB.getDataFromLocal('widget_settings');
      final updatedSettings = jsonDecode(settings.toString());
      // get module need update
      var module = await repository.findId(updatedSettings, event.widgetId);
      // update setting of module

      module['widgetSetting'].addAll(event.model);

      // remove key empty or null
      var tmp = repository.removeEmptyValues(module['widgetSetting']);

      module['widgetSetting'].addAll(tmp);

      await LocalDB.saveDataToLocal(
          'widget_settings', jsonEncode(updatedSettings));
      final newSettings = await LocalDB.getDataFromLocal('widget_settings');
      emit(state.copyWith(
          isLoading: false,
          currentSetting: module,
          settings: newSettings,
          currentModuleId: event.widgetId));
    } catch (e) {
      emit(state.copyWith(isLoading: false));
    }
  }

  // get module by id
  void _onGetModule(GetModuleEvent event, Emitter<BuilderState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      // get settings from local
      final settings = await LocalDB.getDataFromLocal('widget_settings');
      final updatedSettings = jsonDecode(settings.toString());
      Map<String, dynamic> currentSetting =
          repository.findId(updatedSettings, event.widgetId);

      var jsonDefault =
          repository.jsonDefaultCase(currentSetting['widgetName']);

      Map<String, dynamic> mergedJson = repository.mergeJsonProperties(
          jsonDefault['widgetSetting'], currentSetting['widgetSetting']);

      currentSetting['widgetSetting'] = mergedJson;
      // update state
      emit(state.copyWith(
          isLoading: false,
          currentSetting: currentSetting,
          currentModuleId: event.widgetId));
    } catch (error) {
      emit(state.copyWith(isLoading: false));
    }
  }

  // get current setting
  void _onChangeModuleName(
      ChangeModuleNameEvent event, Emitter<BuilderState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      // get settings from local
      final settings = await LocalDB.getDataFromLocal('widget_settings');
      final updatedSettings = jsonDecode(settings.toString());
      var module = await repository.findId(updatedSettings, event.widgetId);
      // change module name
      if (event.moduleName.isNotEmpty) {
        module["headerName"] = event.moduleName.toString();
      }
      await LocalDB.saveDataToLocal(
          'widget_settings', jsonEncode(updatedSettings));
      await Future.delayed(const Duration(milliseconds: 500));
      final newSettings = await LocalDB.getDataFromLocal('widget_settings');
      // update state
      emit(state.copyWith(
        settings: newSettings,
        currentModuleId: event.widgetId,
        isLoading: false,
      ));
    } catch (e) {
      emit(state.copyWith(isLoading: false));
    }
  }
}
