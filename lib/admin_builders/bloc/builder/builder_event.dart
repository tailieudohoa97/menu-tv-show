import 'package:equatable/equatable.dart';

abstract class BuilderEvent extends Equatable {}

// get setting event
class GetSettingEvent extends BuilderEvent {
  final String? dataSetting;
  final String? widgetSetting;
  final String? radioSetting;

  GetSettingEvent({this.dataSetting, this.widgetSetting, this.radioSetting});

  @override
  List<Object> get props => [];
}

// set setting event
class ChangeSettingEvent extends BuilderEvent {
  final Map<String, dynamic> settings;
  final Map<String, dynamic>? dataSettings;
  final Map<String, dynamic>? radioSettings;

  ChangeSettingEvent({required this.settings,  this.dataSettings, this.radioSettings});

  @override
  List<Object> get props => [];
}

// add module
class AddModuleEvent extends BuilderEvent {
  final String widgetId;
  final Map<String, dynamic> model;

  AddModuleEvent({required this.widgetId, required this.model});

  @override
  List<Object> get props => [];
}

// remove layout item
class RemoveModuleEvent extends BuilderEvent {
  final String widgetId;
  RemoveModuleEvent({required this.widgetId});

  @override
  List<Object> get props => [];
}

// clear modules
class ClearModulesEvent extends BuilderEvent {
  @override
  List<Object> get props => [];
}

// update module
class UpdateModuleEvent extends BuilderEvent {
  final String widgetId;
  final Map<String, dynamic> model;

  UpdateModuleEvent({required this.widgetId, required this.model});

  @override
  List<Object> get props => [];
}

// get module by id
class GetModuleEvent extends BuilderEvent {
  final String widgetId;

  GetModuleEvent({required this.widgetId});

  @override
  List<Object> get props => [];
}

class ChangeModuleNameEvent extends BuilderEvent {
  final String moduleName, widgetId;

  ChangeModuleNameEvent({required this.moduleName, required this.widgetId});

  @override
  List<Object> get props => [];
}
