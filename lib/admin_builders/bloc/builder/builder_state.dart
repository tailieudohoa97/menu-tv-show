import 'package:equatable/equatable.dart';

class BuilderState extends Equatable {
  final String settings;
  final String currentModuleId;
  final Map<String, dynamic> currentSetting;
  final bool isLoading;

  const BuilderState(
      {this.settings = '',
      this.currentModuleId = '',
      this.currentSetting = const {},
      this.isLoading = false});

  BuilderState copyWith(
      {String? settings,
      String? currentModuleId,
      Map<String, dynamic>? currentSetting,
      bool? isLoading}) {
    return BuilderState(
      settings: settings ?? this.settings,
      currentModuleId: currentModuleId ?? this.currentModuleId,
      currentSetting: currentSetting ?? this.currentSetting,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  @override
  List<Object> get props =>
      [isLoading, settings, currentSetting, currentModuleId];
}
