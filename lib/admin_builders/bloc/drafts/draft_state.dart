import 'package:equatable/equatable.dart';

class DraftState extends Equatable {
  final dynamic draftSetting;
  final bool isLoading;

  const DraftState({this.draftSetting = const [], this.isLoading = false});

  DraftState copyWith({dynamic draftSetting, bool? isLoading}) {
    return DraftState(
      draftSetting: draftSetting ?? this.draftSetting,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  @override
  List<Object> get props => [
        isLoading,
        draftSetting,
      ];
}
