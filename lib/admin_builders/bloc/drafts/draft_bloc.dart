import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/drafts/draft_event.dart';
import 'package:tv_menu/admin_builders/bloc/drafts/draft_state.dart';
import 'package:intl/intl.dart';
import 'package:tv_menu/admin_builders/repo/local_db.dart';

class DraftBloc extends Bloc<DraftEvent, DraftState> {
  DraftBloc() : super(const DraftState()) {
    on<MoveToDraftEvent>(_onMoveToDraft);
    on<GetListDrafts>(_onGetListDrafts);
    on<RemoveDraftEvent>(_onRemoveDraft);
    on<ClearDraftEvent>(_onClearDraft);
  }

  // get list drafts
  void _onGetListDrafts(GetListDrafts event, Emitter<DraftState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      // get from local
      final draftSettings = await LocalDB.getDataFromLocal('draft_settings');
      // update state
      await Future.delayed(const Duration(milliseconds: 500));
      dynamic tmp;
      if (draftSettings.isNotEmpty) {
        tmp = jsonDecode(draftSettings);
      }

      emit(state.copyWith(
        draftSetting: tmp,
        isLoading: false,
      ));
      // print(state.draftSetting);
    } catch (e) {
      emit(state.copyWith(isLoading: false));
    }
  }

  // move to draft
  void _onMoveToDraft(MoveToDraftEvent event, Emitter<DraftState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      final draftSettings = await LocalDB.getDataFromLocal('draft_settings');
      dynamic listDraft = [];
      // draft time created
      DateTime dateTime = DateTime.now();
      String formattedTime =
          DateFormat('EEE, M/d/yyyy hh:mm:ss a').format(dateTime);
      // add to list
      if (draftSettings.isNotEmpty) {
        listDraft = jsonDecode(draftSettings.toString());
        listDraft.add({
          'draftId': listDraft[listDraft.length - 1]['draftId'] + 1,
          'draftSettings': event.settings,
          'draftTime': formattedTime
        });
      } else {
        listDraft.add({
          'draftId': 1,
          'draftSettings': event.settings,
          'draftTime': formattedTime
        });
      }
      await LocalDB.saveDataToLocal('draft_settings', jsonEncode(listDraft));
      // update state
      await Future.delayed(const Duration(milliseconds: 500));

      print(listDraft.length);
      emit(state.copyWith(
        draftSetting: listDraft,
        isLoading: false,
      ));
    } catch (e) {
      emit(state.copyWith(isLoading: false));
    }
  }

  // clear layout
  void _onRemoveDraft(RemoveDraftEvent event, Emitter<DraftState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      // get setting from local storage
      final draftSettings = await LocalDB.getDataFromLocal('draft_settings');
      final updateDraft = jsonDecode(draftSettings.toString());
      // remove draft by index
      updateDraft.removeAt(event.index);
      // clear data local
      await Future.delayed(const Duration(milliseconds: 500));
      // update local
      await LocalDB.saveDataToLocal('draft_settings', jsonEncode(updateDraft));
      // update state
      emit(state.copyWith(
        draftSetting: updateDraft,
        isLoading: false,
      ));
    } catch (error) {
      emit(state.copyWith(isLoading: false));
    }
  }

  // clear layout
  void _onClearDraft(ClearDraftEvent event, Emitter<DraftState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      final draftSettings = await LocalDB.getDataFromLocal('draft_settings');
      final updateDraft = jsonDecode(draftSettings.toString());
      updateDraft.clear();
      // get setting from local storage
      await LocalDB.clearDataLocal('draft_settings');
      // clear data local

      await Future.delayed(const Duration(milliseconds: 500));

      // update state
      emit(state.copyWith(
        draftSetting: updateDraft,
        isLoading: false,
      ));
    } catch (error) {
      emit(state.copyWith(isLoading: false));
    }
  }
}
