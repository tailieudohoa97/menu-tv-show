import 'package:equatable/equatable.dart';

abstract class DraftEvent extends Equatable {}

// move to draft
class GetListDrafts extends DraftEvent {
  @override
  List<Object> get props => [];
}

// move to draft
class MoveToDraftEvent extends DraftEvent {
  final Map<String, dynamic> settings;

  MoveToDraftEvent({required this.settings});

  @override
  List<Object> get props => [];
}

// move to draft
class RemoveDraftEvent extends DraftEvent {
  final int index;

  RemoveDraftEvent({required this.index});

  @override
  List<Object> get props => [];
}

// move to draft
class ClearDraftEvent extends DraftEvent {
  @override
  List<Object> get props => [];
}
