import 'package:equatable/equatable.dart';

abstract class CategoryEvent extends Equatable {}

// move to draft
class SelectedCategoryEvent extends CategoryEvent {
  final String cat;

  SelectedCategoryEvent({required this.cat});

  @override
  List<Object> get props => [];
}

// get layout of category
class GetLayoutByCategoryEvent extends CategoryEvent {
  final String cat;

  GetLayoutByCategoryEvent({required this.cat});

  @override
  List<Object> get props => [];
}
