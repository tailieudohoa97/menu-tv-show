import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/admin_builders/bloc/categories/category_event.dart';
import 'package:tv_menu/admin_builders/bloc/categories/category_state.dart';
import 'package:tv_menu/admin_builders/data/json_data_builder.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  CategoryBloc() : super(const CategoryState()) {
    on<SelectedCategoryEvent>(_onSelectedCategory);
    on<GetLayoutByCategoryEvent>(_onGetLayoutByCategory);
  }

  // get list drafts
  void _onSelectedCategory(
      SelectedCategoryEvent event, Emitter<CategoryState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      // get from local
      final selectedCat = event.cat;

      emit(state.copyWith(
        selectedCat: selectedCat,
        isLoading: false,
      ));
      // print(state.draftSetting);
    } catch (e) {
      emit(state.copyWith(isLoading: false));
    }
  }

  // get list drafts
  void _onGetLayoutByCategory(
      GetLayoutByCategoryEvent event, Emitter<CategoryState> emit) async {
    try {
      emit(state.copyWith(isLoading: true));
      // get from local
      final selectedCat = event.cat;
      List<dynamic> listLayout = JsonDataBuilder().listLayout;

      if (selectedCat != 'All') {
        listLayout = listLayout
            .where((e) => e['layoutCategory'] == selectedCat)
            .toList();
      }

      await Future.delayed(const Duration(milliseconds: 500));

      emit(state.copyWith(
        selectedCat: selectedCat,
        listLayout: listLayout,
        isLoading: false,
      ));
      // print(state.draftSetting);
    } catch (e) {
      emit(state.copyWith(isLoading: false));
    }
  }
}
