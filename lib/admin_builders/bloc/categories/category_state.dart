import 'package:equatable/equatable.dart';

class CategoryState extends Equatable {
  final String selectedCat;
  final bool isLoading;
  final dynamic listLayout;

  const CategoryState(
      {this.selectedCat = "",
      this.isLoading = false,
      this.listLayout = const []});

  CategoryState copyWith(
      {String? selectedCat, bool? isLoading, dynamic listLayout}) {
    return CategoryState(
      selectedCat: selectedCat ?? this.selectedCat,
      isLoading: isLoading ?? this.isLoading,
      listLayout: listLayout ?? this.listLayout,
    );
  }

  @override
  List<Object> get props => [isLoading, selectedCat, listLayout];
}
