import 'package:shared_preferences/shared_preferences.dart';

class LocalDB {
  // save data to local storage
  static Future<void> saveDataToLocal(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
    print('Data saved to SharedPreferences');
  }

  // get data from local storage
  static Future<String> getDataFromLocal(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String value = prefs.getString(key) ?? '';
    return value;
  }

  // clear data local storage
  static Future<void> clearDataLocal(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.remove(key);
  }
}
