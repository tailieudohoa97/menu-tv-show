import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tv_menu/admin_builders/constants/index.dart';
import 'package:tv_menu/admin_builders/models/template_model.dart';

// [092301TREE]
class TemplateRepository {
  int total = 0;

  // calculate total page
  int calculateTotalPage() {
    const perPage = 10;
    final totalPages = (total / perPage).ceil();
    return totalPages;
  }

  // get all templates
  Future<List<TemplateModel>> getAllTemplates(int page) async {
    final url = Uri.parse('$apiUrl/templates?page=$page');
    try {
      final response = await http.get(url);
      final jsonData = jsonDecode(response.body);
      final results = jsonData['templates'];
      total = jsonData['total'];
      final List<TemplateModel> data = results
          .map((e) => TemplateModel.fromJson(e))
          .toList()
          .cast<TemplateModel>();
      return data;
    } catch (e) {
      throw Exception(e);
    }
  }

  // get one post by id
  Future<TemplateModel> getPostById(TemplateModel template) async {
    final url = Uri.parse('$apiUrl/templates/${template.id}');
    try {
      final response =
          await http.get(url, headers: {'Content-Type': 'application/json'});
      final jsonData = jsonDecode(response.body);
      final result = jsonData['template'];
      final TemplateModel data = TemplateModel.fromJson(result);
      return data;
    } catch (e) {
      throw Exception(e);
    }
  }

  // create new template
  Future<void> createTemplate(TemplateModel template) async {
    final url = Uri.parse('$apiUrl/templates');
    try {
      await http.post(url,
          headers: {'Content-Type': 'application/json'},
          body: jsonEncode(template.toJson()));
    } catch (e) {
      throw Exception(e);
    }
  }

  // update template
  Future<void> updateTemplate(TemplateModel template) async {
    final url = Uri.parse('$apiUrl/templates/${template.id.toString()}');
    final data = {"name": template.name, "type": template.type};
    try {
      await http.patch(
        url,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode(data),
      );
    } catch (e) {
      throw Exception(e);
    }
  }

  // update template
  Future<void> removeTemplates(List<TemplateModel> templates) async {
    final url = Uri.parse('$apiUrl/templates');
    final listRemoved = templates.map((e) => e.id.toString()).toList();
    final data = {"listRemoved": listRemoved};
    try {
      await http.delete(
        url,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode(data),
      );
    } catch (e) {
      throw Exception(e);
    }
  }

  // update template
  Future<List<TemplateModel>> searchTemplates(String keyword) async {
    final url = Uri.parse('$apiUrl/templates/search?keyword=$keyword');
    try {
      final response = await http.get(
        url,
        headers: {'Content-Type': 'application/json'},
      );
      final jsonData = jsonDecode(response.body);
      final results = jsonData['results'];
      final List<TemplateModel> data = results
          .map((e) => TemplateModel.fromJson(e))
          .toList()
          .cast<TemplateModel>();
      return data;
    } catch (e) {
      throw Exception(e);
    }
  }
}
