import 'package:uuid/uuid.dart';

// [092301TREE]
class BuilderRepository {
  // allow generating unique id
  var uuid = Uuid();

  Map<String, dynamic> addEmptySettings(Map<String, dynamic> json) {
    if (json == null || !json.containsKey('widgetName')) {
      return json;
    }

    String widgetName = json['widgetName'];

    // Kiểm tra và thêm key child, children, menuItems nếu phù hợp
    if (json.containsKey('widgetSetting')) {
      if ((widgetName == 'Container' ||
              widgetName == 'Padding' ||
              widgetName == 'Expanded' ||
              widgetName == 'SizedBox' ||
              widgetName == 'Center' ||
              widgetName == 'PaddingByRatio' ||
              widgetName == 'SizedBoxByRatio' ||
              widgetName == 'CustomContainer' ||
              widgetName == 'IntrinsicHeight' ||
              widgetName == 'Positioned' ||
              widgetName == 'RotatedBox') &&
          !json["widgetSetting"].containsKey('child')) {
        json['widgetSetting']['child'] = {};
      } else if ((widgetName == 'Row' ||
              widgetName == 'Column' ||
              widgetName == 'ListView' ||
              widgetName == 'Wrap' ||
              widgetName == 'Stack') &&
          !json["widgetSetting"].containsKey('children')) {
        json['widgetSetting']['children'] = [];
      } else if (widgetName == 'CategoryColumn1' &&
          !json["widgetSetting"].containsKey('productRows')) {
        json['widgetSetting']['productRows'] = [];
      } else if ((widgetName == 'CategoryColumn2' ||
              widgetName == 'CategoryBlock2') &&
          !json["widgetSetting"].containsKey('categoryItems')) {
        json['widgetSetting']['categoryItems'] = [];
      } else if (widgetName == 'CategoryBlock1' &&
          !json['widgetSetting'].containsKey('categoryItemRows')) {
        json['widgetSetting']['categoryItemRows'] = [];
      } else if (widgetName == 'MenuColumn1' &&
          !json['widgetSetting'].containsKey('menuItems')) {
        json['widgetSetting']['menuItems'] = [];
      }
    }

    if (json.containsKey('widgetSetting')) {
      final widgetSetting = json['widgetSetting'];

      if (widgetSetting.containsKey('child')) {
        widgetSetting['child'] =
            addEmptySettings(Map<String, dynamic>.from(widgetSetting['child']));
      }

      if (widgetSetting.containsKey('children')) {
        final children = widgetSetting['children'] as List<dynamic>;
        for (int i = 0; i < children.length; i++) {
          children[i] =
              addEmptySettings(Map<String, dynamic>.from(children[i]));
        }
      }

      if (widgetSetting.containsKey('productRows')) {
        final productRows = widgetSetting['productRows'] as List<dynamic>;
        for (int i = 0; i < productRows.length; i++) {
          productRows[i] =
              addEmptySettings(Map<String, dynamic>.from(productRows[i]));
        }
      }

      if (widgetSetting.containsKey('categoryItems')) {
        final categoryItems = widgetSetting['categoryItems'] as List<dynamic>;
        for (int i = 0; i < categoryItems.length; i++) {
          categoryItems[i] =
              addEmptySettings(Map<String, dynamic>.from(categoryItems[i]));
        }
      }

      if (widgetSetting.containsKey('menuItems')) {
        final menuItems = widgetSetting['menuItems'] as List<dynamic>;
        for (int i = 0; i < menuItems.length; i++) {
          menuItems[i] =
              addEmptySettings(Map<String, dynamic>.from(menuItems[i]));
        }
      }

      if (widgetSetting.containsKey('categoryItemRows')) {
        final categoryItemRows =
            widgetSetting['categoryItemRows'] as List<dynamic>;
        for (int i = 0; i < categoryItemRows.length; i++) {
          categoryItemRows[i] =
              addEmptySettings(Map<String, dynamic>.from(categoryItemRows[i]));
        }
      }
    }

    return json;
  }

  // remove child, children in settings
  Map<String, dynamic> removeEmptySettings(Map<String, dynamic> json) {
    json.removeWhere((key, value) {
      if (value is Map<String, dynamic>) {
        value = removeEmptySettings(value);
        return value.isEmpty;
      } else if (value is List<dynamic>) {
        value.removeWhere((item) =>
            item is Map<String, dynamic> && removeEmptySettings(item).isEmpty);
        return value.isEmpty;
      } else if (value == null || value == "") {
        return true;
      }
      return false;
    });

    return json;
  }

  // remove key-value empty or not valid
  Map<String, dynamic> removeEmptyValues(Map<String, dynamic> json) {
    Map<String, dynamic> updatedJson = json;

    // List keys that need remove.
    List<String> keysToRemove = [];

    updatedJson.forEach((key, value) {
      // don't remove key are child and children...
      if (key != 'child' &&
          key != 'children' &&
          key != 'productRows' &&
          key != 'categoryItems' &&
          key != 'menuItems' &&
          key != 'categoryItemRows') {
        if (value == null ||
            value.isEmpty ||
            value == "0" ||
            value == "no use") {
          keysToRemove.add(key);
        }

        if (value is Map<String, dynamic>) {
          Map<String, dynamic> updatedValue = removeEmptyValues(value);
          if (updatedValue.isEmpty) {
            keysToRemove.add(key);
          } else {
            updatedJson[key] = updatedValue;
          }
        }
      }
    });

    // remove keys empty
    for (String key in keysToRemove) {
      updatedJson.remove(key);
    }

    return updatedJson;
  }

  // set default setting for module.
  Map<String, dynamic> mergeJsonProperties(
      Map<String, dynamic> jsonDefault, Map<String, dynamic> jsonNew) {
    Map<String, dynamic> jsonUpdated = jsonNew;

    jsonDefault.forEach((key, value) {
      if (!jsonNew.containsKey(key)) {
        jsonUpdated[key] = value;
      } else if (jsonDefault[key] is Map<String, dynamic> &&
          jsonNew[key] is Map<String, dynamic>) {
        jsonUpdated[key] = mergeJsonProperties(jsonDefault[key], jsonNew[key]);
      }
    });

    return jsonUpdated;
  }

  // json border data
  Map<String, dynamic> generateJsonBorderData() {
    return {
      "color": "",
      "width": "0",
      "all": generateJsonBorderSide(),
      "top": generateJsonBorderSide(),
      "right": generateJsonBorderSide(),
      "bottom": generateJsonBorderSide(),
      "left": generateJsonBorderSide(),
    };
  }

  // json border side
  Map<String, dynamic> generateJsonBorderSide() {
    return {"color": "", "width": "0"};
  }

  // json box decoration
  Map<String, dynamic> generateJsonBoxDecoration() {
    return {
      "color": "",
      "borderRadius": "0",
      // Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
      "image": "",
      "imageUrl": "",
      "boxFit": "fill",
      "border": generateJsonBorderData(),
      "backgroundBlendMode": "no use"
    };
  }

  // json container
  Map<String, dynamic> generateJsonContainer() {
    String id = uuid.v4();
    Map<String, dynamic> container = {
      "id": id,
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "width": "0",
        "height": "0",
        "margin": generateJsonPaddingMargin(), // generateJsonPaddingMargin()
        "padding": generateJsonPaddingMargin(), // generateJsonPaddingMargin()
        "decoration": generateJsonBoxDecoration(),
        "child": {}
      }
    };

    return container;
  }

  // json row
  Map<String, dynamic> generateJsonRow() {
    String id = uuid.v4();
    Map<String, dynamic> row = {
      "id": id,
      "headerName": "Row",
      "widgetName": "Row",
      "widgetSetting": {
        "textRightToLeft": "false",
        "mainAxisAlignment": "no use",
        "crossAxisAlignment": "no use",
        "children": []
      }
    };
    return row;
  }

  // json column
  Map<String, dynamic> generateJsonColumn() {
    String id = uuid.v4();
    Map<String, dynamic> column = {
      "id": id,
      "headerName": "Column",
      "widgetName": "Column",
      "widgetSetting": {
        "mainAxisAlignment": "no use",
        "crossAxisAlignment": "no use",
        "children": []
      }
    };
    return column;
  }

  // json expanded
  Map<String, dynamic> generateJsonExpanded() {
    String id = uuid.v4();

    Map<String, dynamic> expanded = {
      "id": id,
      "headerName": "Expanded",
      "widgetName": "Expanded",
      "widgetSetting": {"flex": "1", "child": {}}
    };

    return expanded;
  }

  // json padding
  Map<String, dynamic> generateJsonPadding() {
    String id = uuid.v4();
    Map<String, dynamic> padding = {
      "id": id,
      "headerName": "Padding",
      "widgetName": "Padding",
      "widgetSetting": {"padding": generateJsonPaddingMargin(), "child": {}}
    };
    return padding;
  }

  // json sized box
  Map<String, dynamic> generateJsonSizedBox() {
    String id = uuid.v4();

    Map<String, dynamic> sizedBox = {
      "id": id,
      "headerName": "SizedBox",
      "widgetName": "SizedBox",
      "widgetSetting": {"width": "0", "height": "0", "child": {}}
    };

    return sizedBox;
  }

  // json image data
  Map<String, dynamic> generateJsonImageData() {
    String id = uuid.v4();
    return {"id": id, "type": "image", "image": ""};
  }

  // json image
  Map<String, dynamic> generateJsonImage() {
    String id = uuid.v4();

    Map<String, dynamic> image = {
      "id": id,
      "headerName": "image",
      "widgetName": "image",
      "widgetSetting": {
        "imageId": "", // pass to imageId in dataSetting.
        "fixCover": "true",
        "width": "0",
        "height": "0"
      }
    };

    return image;
  }

  // json text style
  Map<String, dynamic> generateJsonTextStyle() {
    return {
      "fontSize": "0",
      "fontWeight": "w400",
      "height": "0",
      "fontFamily": "no use",
      "color": "",
      "letterSpacing": "0",
      "fontStyle": "no use"
    };
  }

  // json text
  Map<String, dynamic> generateJsonText() {
    String id = uuid.v4();
    Map<String, dynamic> text = {
      "id": id,
      "headerName": "Text",
      "widgetName": "Text",
      "widgetSetting": {
        "data": "",
        "fontSizeFromFieldKey": "",
        "textAlign": "start",
        "style": generateJsonTextStyle(),
      }
    };
    return text;
  }

  // json product data
  Map<String, dynamic> generateJsonProductData() {
    String id = uuid.v4();
    return {
      "id": id,
      "type": "product",
      "name": "product",
      "price": "12",
      "salePrice": "",
      "description": "this is a description"
    };
  }

  // json product
  Map<String, dynamic> generateJsonProduct() {
    String id = uuid.v4();
    Map<String, dynamic> product = {
      "id": id,
      "headerName": "productItem",
      "widgetName": "productItem",
      "widgetSetting": {
        "name": "product 1",
        "price": "12",
        "salePrice": "12", // setting old
        // "image": "",
        "description": "Lorem Ipsum is simply du…nd typesetting industry",
        "colorPrice": "",
        "colorProductName": "",
        "toUpperCaseNameProductName": "true",
        "useMediumStyle": "false",
        "colorDescription": "",
        "useThumbnailProduct": "false",
        "pathThumbnailProduct": "",
        "maxLineProductName": "1",
        "maxLineProductDescription": "2"
      }
    };
    return product;
  }

  // json list products
  Map<String, dynamic> generateJsonProductList() {
    String id = uuid.v4();
    Map<String, dynamic> productList = {
      "id": id,
      "headerName": "productList",
      "widgetName": "productList",
      "widgetSetting": {
        "dataList": [],
        "mainAxisAlignment": "no use",
        "crossAxisAlignment": "no use",
        "colorPrice": "",
        "colorProductName": "",
        "toUpperCaseNameProductName": "true",
        "useMediumStyle": "false",
        "colorDescription": "",
        "useThumbnailProduct": "false",
        "pathThumbnailProduct": "",
        "maxLineProductName": "1",
        "maxLineProductDescription": "2"
      }
    };
    return productList;
  }

  // json list view
  Map<String, dynamic> generateJsonListView() {
    String id = uuid.v4();

    Map<String, dynamic> listView = {
      'id': id,
      "headerName": "ListView",
      "widgetName": "ListView",
      "widgetSetting": {
        "reverse": "false",
        "shrinkWrap": "true",
        "padding": generateJsonPaddingMargin(),
        "children": []
      }
    };

    return listView;
  }

  // json center
  Map<String, dynamic> generateJsonCenter() {
    String id = uuid.v4();

    Map<String, dynamic> center = {
      'id': id,
      "headerName": "Center",
      "widgetName": "Center",
      "widgetSetting": {"child": {}}
    };
    return center;
  }

  // json category title single column
  Map<String, dynamic> generateJsonCategoryTitleSingleColumn() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "categoryTitleSingleColumn",
      "widgetName": "categoryTitleSingleColumn",
      "widgetSetting": {
        "dataList": {},
        // "indexCategory": "",
        "useBackgroundCategoryTitle": "false",
        // "pathImageBgCategoryTitle": "", // pass imageId here
        "imageId": "",
        "colorCategoryTitle": "",
        "colorBackgroundCategoryTitle": "",
        "colorPrice": "",
        "colorProductName": "",
        "colorDescription": "",
        "useThumbnailProduct": "false",
        "pathThumbnailProduct": "",
        "toUpperCaseNameProductName": "false",
        "useMediumStyle": "false",
        "useFullWidthCategoryTitle": "false",
        "runSpacing": "0",
        "mainAxisAlignment": "no use",
        "spaceBetweenCategoryProductList": "0"
      }
    };

    return data;
  }

  // json category title
  Map<String, dynamic> generateJsonCategoryTitle() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "categoryTitle",
      "widgetName": "categoryTitle",
      "widgetSetting": {
        "categoryId": "",
        "useBackground": "false",
        "useFullWidth": "false",
        "imageId": "",
        "color": "",
        "colorBackground": ""
      }
    };

    return data;
  }

  // json category data
  Map<String, dynamic> generateJsonCategoryData() {
    String id = uuid.v4();
    return {
      "id": id,
      "type": "category",
      "name": "",
      "image": "",
      "description": "",
    };
  }

  // json category block 1
  Map<String, dynamic> generateJsonCategoryBlock1() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "CategoryBlock1",
      "widgetName": "CategoryBlock1",
      "widgetSetting": {
        "categoryFontSizeFromFieldKey": "",
        "heightFromFieldKey": "",
        "widthFromFieldKey": "",
        "category": {},
        "categoryImage": {},
        "categoryItemRows": [],
        "categoryTextStyle": generateJsonTextStyle(),
        "elementGap": "0",
        "height": "0",
        "width": "0"
      }
    };

    return data;
  }

  // json category block 2
  Map<String, dynamic> generateJsonCategoryBlock2() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "CategoryBlock2",
      "widgetName": "CategoryBlock2",
      "widgetSetting": {
        "category": {},
        "categoryNameFontSizeFromFieldKey": "",
        "categoryDesFontSizeFromFieldKey": "",
        "categoryDescriptionTextStyle": generateJsonTextStyle(),
        "categoryItemGap": "0",
        "categoryItemSectionPadding": "0",
        "categoryItems": [],
        "categoryNameTextStyle": generateJsonTextStyle(),
        "divider": generateJsonDivider(),
        "indentCategoryDescription": "0",
        "upperCaseCategoryName": "true"
      }
    };

    return data;
  }

  // json category column 1
  Map<String, dynamic> generateJsonCategoryColumn1() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "CategoryColumn1",
      "widgetName": "CategoryColumn1",
      "widgetSetting": {
        "category": {}, // generateJsonCategoryData()
        "categoryImageWidthFromFieldKey": "",
        "categoryImageHeightFromFieldKey": "",
        "categoryFontSizeFromFieldKey": "",
        "categoryImageHeight": "0",
        "categoryImageWidth": "0",
        "categoryTextStyle": generateJsonTextStyle(), // generateJsonTextStyle()
        "elementGap": "0",
        "upperCaseCategoryName": "true",
        "productRows": [],
        "mainAxisAlignment": "no use", // new
        "crossAxisAlignment": "no use" // new
      }
    };

    return data;
  }

  // json category column 2
  Map<String, dynamic> generateJsonCategoryColumn2() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "CategoryColumn2",
      "widgetName": "CategoryColumn2",
      "widgetSetting": {
        "category": {},
        "categoryNameFontSizeFromFieldKey": "",
        "categoryImageHeightFromFieldKey": "",
        "categoryImageWidthFromFieldKey": "",
        "elementGapFromFieldKey": "",
        "categoryImageHeight": "0",
        "categoryImageWidth": "0",
        "elementGap": "0",
        "categoryItems": [],
        "categoryNameContainerDecoration": generateJsonBoxDecoration(),
        "categoryImageBorderRadius": "0",
        "categoryNameContainerPadding": generateJsonPaddingMargin(),
        "categoryNameTextStyle": generateJsonTextStyle(),
        "upperCaseCategoryName": "false",
        "hideScrollbarWhenOverflow": "true"
      }
    };

    return data;
  }

  // json category title grid layout 1
  Map<String, dynamic> generateJsonCategoryTitleGridLayout1() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "CategoryTitleGridLayout1",
      "widgetName": "CategoryTitleGridLayout1",
      "widgetSetting": {
        "dataList": {"category": "", "product": [], "image": []}, // imageList
        "useBackgroundCategoryTitle": "false",
        "useFullWidthCategoryTitle": "false",
        "imageId": "",
        "pathImageBgCategoryTitle": "",
        "colorCategoryTitle": "",
        "colorPrice": "",
        "colorProductName": "",
        "colorDescription": "",
        "colorBackgroundCategoryTitle": ""
      }
    };

    return data;
  }

  // json category title grid layout
  Map<String, dynamic> generateJsonCategoryTitleGridLayout3() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "CategoryTitleGridLayout3",
      "widgetName": "CategoryTitleGridLayout3",
      "widgetSetting": {
        "dataList": {"category": "", "product": [], "image": []}, // imageList
        "useBackgroundCategoryTitle": "false",
        "useFullWidthCategoryTitle": "false",
        "imageId": "",
        "pathImageBgCategoryTitle": "",
        "colorCategoryTitle": "",
        "colorPrice": "",
        "colorProductName": "",
        "colorDescription": "",
        "colorBackgroundCategoryTitle": ""
      }
    };

    return data;
  }

  // json category title grid layout
  Map<String, dynamic> generateJsonCategoryTitleGridLayout4() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "CategoryTitleGridLayout4",
      "widgetName": "CategoryTitleGridLayout4",
      "widgetSetting": {
        "dataList": {"category": "", "product": [], "image": []}, // imageList
        "useBackgroundCategoryTitle": "false",
        "useFullWidthCategoryTitle": "false",
        "imageId": "",
        "pathImageBgCategoryTitle": "",
        "colorCategoryTitle": "",
        "colorPrice": "",
        "colorProductName": "",
        "colorDescription": "",
        "colorBackgroundCategoryTitle": ""
      }
    };

    return data;
  }

  // json divider
  Map<String, dynamic> generateJsonDivider() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "Divider",
      "widgetName": "Divider",
      "widgetSetting": {
        "height": "0",
        "color": "",
        "indent": "0",
        "endIndent": "0",
        "thickness": "0"
      }
    };

    return data;
  }

  // json padding / margin data
  Map<String, dynamic> generateJsonPaddingMargin() {
    return {
      "all": "0",
      "vertical": "0",
      "horizontal": "0",
      "top": "0",
      "right": "0",
      "bottom": "0",
      "left": "0"
    };
  }

  // json custom container
  Map<String, dynamic> generateJsonCustomContainer() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "CustomContainer",
      "widgetName": "CustomContainer",
      "widgetSetting": {
        "opacity": "1",
        "image": "", // image url
        "fit": "cover",
        "alignment": "no use",
        "color": "",
        "blendMode": "no use",
        "width": "0",
        "height": "0",
        "margin": generateJsonPaddingMargin(), // generateJsonPaddingMargin()
        "padding": generateJsonPaddingMargin(), // generateJsonPaddingMargin()
        "child": {},
      }
    };

    return data;
  }

  // json framed image
  Map<String, dynamic> generateJsonFramedImage() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "FramedImage",
      "widgetName": "FramedImage",
      "widgetSetting": {
        "image": "",
        "imageFrame": "",
        "imageDataKey": "",
        "imageFrameDataKey": "", // no setting
        "borderRadiusOfImage": "0",
        "borderWidth": "0",
        "height": "0",
        "width": "0"
      }
    };

    return data;
  }

  // json indexed menu item
  Map<String, dynamic> generateJsonIndexedMenuItem() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "IndexedMenuItem1",
      "widgetName": "IndexedMenuItem1",
      "widgetSetting": {
        "index": "0",
        "product": {}, // product json
        "priceFontSizeFromFieldKey": "",
        "fontSizeFromFieldKey": "",
        "indexChipRadiusFromFieldKey": "",
        "elementGap": "0",
        "indexChipBgColor": "",
        "indexChipFgColor": "",
        "indexChipRadius": "0",
        "priceTextStyle": generateJsonTextStyle(), // text style json
        "textStyle": generateJsonTextStyle() // text style json
      }
    };

    return data;
  }

  // json intrinsic height
  Map<String, dynamic> generateJsonIntrinsicHeight() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "IntrinsicHeight",
      "widgetName": "IntrinsicHeight",
      "widgetSetting": {"child": {}}
    };

    return data;
  }

  // json menu column 1
  Map<String, dynamic> generateJsonMenuColumn1() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "MenuColumn1",
      "widgetName": "MenuColumn1",
      "widgetSetting": {
        "menuItems": [],
        "brand": "",
        "brandFontSizeFromFieldKey": "",
        "sloganFontSizeFromFieldKey": "",
        "slogan": "",
        "brandTextStyle":
            generateJsonTextStyle(), // generateJsonTextStyle(), go to setting text
        "sloganTextStyle": generateJsonTextStyle() //generateJsonTextStyle()
      }
    };

    return data;
  }

  // json menu item price
  Map<String, dynamic> generateJsonMenuItemPrice() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "MenuItemPrice",
      "widgetName": "MenuItemPrice",
      "widgetSetting": {
        "price": "",
        "prefix": "",
        "textStyle": generateJsonTextStyle()
      }
    };

    return data;
  }

  // json padding by ratio
  Map<String, dynamic> generateJsonPaddingByRatio() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "PaddingByRatio",
      "widgetName": "PaddingByRatio",
      "widgetSetting": {
        "paddingFieldKey": "",
        "topPaddingFieldKey": "",
        "bottomPaddingFieldKey": "",
        "rightPaddingFieldKey": "",
        "leftPaddingFieldKey": "",
        "verticalPaddingFieldKey": "",
        "horizontalPaddingFieldKey": "",
        "child": {},
      }
    };

    return data;
  }

  // json product row 1
  Map<String, dynamic> generateJsonProductRow1() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "ProductRow1",
      "widgetName": "ProductRow1",
      "widgetSetting": {
        "product": {},
        "border": generateJsonBorderData(), // generateJsonBorderSide()
        "productNameFontSizeFromFieldKey": "",
        "productPriceFontSizeFromFieldKey": "",
        "fractionDigitsForPrice": "2",
        "maxLineOfProductName": "3",
        "padding": generateJsonPaddingMargin(), // generateJsonPaddingMargin()
        "prefixProductPrice": "",
        "subfixProductPrice": "",
        "upperCaseProductName": "true",
        "productNameTextStyle": generateJsonTextStyle(),
        "productPriceTextStyle": generateJsonTextStyle()
      }
    };

    return data;
  }

  // json product row 2
  Map<String, dynamic> generateJsonProductRow2() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "ProductRow2",
      "widgetName": "ProductRow2",
      "widgetSetting": {
        "product": {},
        "productNameFontSizeFromFieldKey": "",
        "productPriceFontSizeFromFieldKey": "",
        "productDesFontSizeFromFieldKey": "",
        "fractionDigitsForPrice": "2",
        "maxLineOfProductName": "3",
        "prefixProductPrice": "",
        "subfixProductPrice": "",
        "upperCaseProductName": "true",
        "productNameTextStyle": generateJsonTextStyle(),
        "productPriceTextStyle": generateJsonTextStyle(),
        "divider": generateJsonDivider(),
        "productDescriptionTextStyle": generateJsonTextStyle()
      }
    };

    return data;
  }

  // json product row 3
  Map<String, dynamic> generateJsonProductRow3() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "ProductRow3",
      "widgetName": "ProductRow3",
      "widgetSetting": {
        "product": {},
        "productNameFontSizeFromFieldKey": "",
        "productPriceFontSizeFromFieldKey": "",
        "productDesFontSizeFromFieldKey": "",
        "fractionDigitsForPrice": "2",
        "maxLineOfProductName": "3",
        "prefixProductPrice": "",
        "subfixProductPrice": "",
        "upperCaseProductName": "true",
        "productNameTextStyle": generateJsonTextStyle(),
        "productPriceTextStyle": generateJsonTextStyle(),
        "elementGap": "0",
        "productDescriptionTextStyle": generateJsonTextStyle(),
        "imageBorderRadius": "0",
        "horizontalDivider": generateJsonDivider(),
        "imageHeight": "0",
        "imageWidth": "0",
        "imageHeightFromFieldKey": "",
        "imageWidthFromFieldKey": ""
      }
    };

    return data;
  }

  // json service item column
  Map<String, dynamic> generateJsonServiceItemColumn() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "ServiceItemColumn",
      "widgetName": "ServiceItemColumn",
      "widgetSetting": {
        "productId": "",
        "toUpperCaseNameProductName": "false",
        "useMediumStyle": "false",
        "colorPrice": "",
        "colorProductName": "",
        "colorDescription": "",
        "maxLineProductName": "2",
        "width": "0",
      }
    };

    return data;
  }

  // json service item column
  Map<String, dynamic> generateJsonServiceCustom1() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "ServiceItemCustom1",
      "widgetName": "ServiceItemCustom1",
      "widgetSetting": {
        "productId": "",
        "toUpperCaseNameProductName": "false",
        "useMediumStyle": "false",
        "colorPrice": "",
        "colorProductName": "",
        "colorDescription": "",
        "maxLineProductName": "2",
        "width": "0",
        "fontWeight": "w400"
      }
    };

    return data;
  }

  // json service item column
  Map<String, dynamic> generateJsonServiceItemWithThumbnail() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "ServiceItemWithThumbnail",
      "widgetName": "ServiceItemWithThumbnail",
      "widgetSetting": {
        "productId": "",
        "toUpperCaseNameProductName": "false",
        "useMediumStyle": "false",
        "colorPrice": "",
        "colorProductName": "",
        "colorDescription": "",
        "maxLineProductName": "2",
        "maxLineProductDescription": "2"
      }
    };

    return data;
  }

  // json simple image
  Map<String, dynamic> generateJsonSimpleImage() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "SimpleImage",
      "widgetName": "SimpleImage",
      "widgetSetting": {
        "imagePath": "",
        "imageDataKey": "", // hidden setting
        "height": "0",
        "width": "0",
        "fit": "cover"
      }
    };

    return data;
  }

  // json sized box by ratio
  Map<String, dynamic> generateJsonSizedBoxByRatio() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "SizedBoxByRatio",
      "widgetName": "SizedBoxByRatio",
      "widgetSetting": {
        "width": "0",
        "widthFromFieldKey": "", // hidden setting
        "height": "0",
        "heightFromFieldKey": "", // hidden setting
        "child": {}
      }
    };

    return data;
  }

// json stack
  Map<String, dynamic> generateJsonStack() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "Stack",
      "widgetName": "Stack",
      "widgetSetting": {"children": []}
    };

    return data;
  }

  // json underline brand
  Map<String, dynamic> generateJsonUnderlineBrand() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "UnderlineBrand",
      "widgetName": "UnderlineBrand",
      "widgetSetting": {
        "brand": "",
        "fontSizeFromFieldKey": "",
        "bottomBorderSide": generateJsonBorderSide(),
        "textStyle": generateJsonTextStyle()
      }
    };

    return data;
  }

  // json wrap
  Map<String, dynamic> generateJsonWrap() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "Wrap",
      "widgetName": "Wrap",
      "widgetSetting": {
        "crossAxisAlignment": "no use",
        "direction": "no use",
        "runAlignment": "no use",
        "runSpacing": "0",
        "spacing": "0",
        "verticalDirection": "no use",
        "alignment": "no use",
        "clipBehavior": "none",
        "children": []
      }
    };

    return data;
  }

  // json vertical divider
  Map<String, dynamic> generateJsonVerticalDivider() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "VerticalDivider",
      "widgetName": "VerticalDivider",
      "widgetSetting": {
        "width": "0",
        "color": "",
        "indent": "0",
        "endIndent": "0",
        "thickness": "0"
      }
    };

    return data;
  }

  // json positioned
  Map<String, dynamic> generateJsonPositioned() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "Positioned",
      "widgetName": "Positioned",
      "widgetSetting": {
        "top": "0",
        "left": "0",
        "right": "0",
        "bottom": "0",
        "child": {}
      }
    };

    return data;
  }

  // json rotate box
  Map<String, dynamic> generateJsonRotateBox() {
    String id = uuid.v4();

    Map<String, dynamic> data = {
      "id": id,
      "headerName": "RotatedBox",
      "widgetName": "RotatedBox",
      "widgetSetting": {"quarterTurns": "0", "child": {}}
    };

    return data;
  }

  // find module by id
  dynamic findId(Map<String, dynamic> data, String id) {
    dynamic result;
    // condition stop
    if (data.containsKey("id") && data["id"] == id) {
      return data;
    }

    data.values.forEach((value) {
      // for map
      if (value is Map<String, dynamic>) {
        dynamic foundItem = findId(value, id);
        if (foundItem != null) {
          result = foundItem;
          return;
        }
      }
      // for list
      else if (value is List<dynamic>) {
        for (var item in value) {
          if (item is Map<String, dynamic>) {
            dynamic foundItem = findId(item, id);
            if (foundItem != null) {
              result = foundItem;
              return;
            }
          }
        }
      }
    });

    return result;
  }

  // remove item with id
  void removeWidgetById(Map<String, dynamic> widgetData, String widgetId) {
    final stack = <Map<String, dynamic>>[widgetData];
    while (stack.isNotEmpty) {
      final current = stack.removeLast();
      if (current.containsKey("id") && current["id"] == widgetId) {
        current.clear();
        break;
      }
      current.values.whereType<Map<String, dynamic>>().forEach(stack.add);
      current.values.whereType<List>().forEach((list) {
        for (int i = list.length - 1; i >= 0; i--) {
          if (list[i] is Map<String, dynamic>) {
            stack.add(list[i]);
            if (list[i].containsKey("id") && list[i]["id"] == widgetId) {
              list.removeAt(i);
            }
          }
        }
      });
    }
  }

  // generate json default
  Map<String, dynamic> jsonDefaultCase(String widgetName) {
    switch (widgetName) {
      case 'Container':
        return generateJsonContainer();
      case 'Row':
        return generateJsonRow();
      case 'Column':
        return generateJsonColumn();
      case 'Expanded':
        return generateJsonExpanded();
      case 'Padding':
        return generateJsonPadding();
      case 'SizedBox':
        return generateJsonSizedBox();
      case 'PaddingByRatio':
        return generateJsonPaddingByRatio();
      case 'SizedBoxByRatio':
        return generateJsonSizedBoxByRatio();
      case 'image':
        return generateJsonImage();
      case 'Text':
        return generateJsonText();
      case 'productItem':
        return generateJsonProduct();
      case 'productList':
        return generateJsonProductList();
      case 'ListView':
        return generateJsonListView();
      case 'Center':
        return generateJsonCenter();
      case 'categoryTitleSingleColumn':
        return generateJsonCategoryTitleSingleColumn();
      case 'categoryTitle':
        return generateJsonCategoryTitle();
      case 'CategoryBlock1':
        return generateJsonCategoryBlock1();
      case 'CategoryBlock2':
        return generateJsonCategoryBlock2();
      case 'CategoryColumn1':
        return generateJsonCategoryColumn1();
      case 'CategoryColumn2':
        return generateJsonCategoryColumn2();
      case 'categoryTitleGridLayout1':
        return generateJsonCategoryTitleGridLayout1();
      case 'CategoryTitleGridLayout3':
        return generateJsonCategoryTitleGridLayout3();
      case 'categoryTitleGridLayout4':
        return generateJsonCategoryTitleGridLayout4();
      case 'Divider':
        return generateJsonDivider();
      case 'CustomContainer':
        return generateJsonCustomContainer();
      case 'FramedImage':
        return generateJsonFramedImage();
      case 'IndexedMenuItem1':
        return generateJsonIndexedMenuItem();
      case 'MenuColumn1':
        return generateJsonMenuColumn1();
      case 'MenuItemPrice':
        return generateJsonMenuItemPrice();
      case 'ProductRow1':
        return generateJsonProductRow1();
      case 'ProductRow2':
        return generateJsonProductRow2();
      case 'ProductRow3':
        return generateJsonProductRow3();
      case 'ServiceItemColumn':
        return generateJsonServiceItemColumn();
      case 'ServiceItemWithThumbnail':
        return generateJsonServiceItemWithThumbnail();
      case 'SimpleImage':
        return generateJsonSimpleImage();
      case 'ServiceItemCustom1':
        return generateJsonServiceCustom1();
      case 'UnderlineBrand':
        return generateJsonUnderlineBrand();
      case 'Wrap':
        return generateJsonWrap();
      case 'VerticalDivider':
        return generateJsonVerticalDivider();
      case 'Positioned':
        return generateJsonPositioned();
      case 'RotatedBox':
        return generateJsonRotateBox();
      default:
        return {};
    }
  }
}
