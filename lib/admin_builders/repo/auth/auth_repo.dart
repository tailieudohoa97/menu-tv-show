import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tv_menu/admin_builders/constants/index.dart';

class AuthRepository {
  String errorMessage = '';
  // login
  Future<bool> login(data) async {
    final url = Uri.parse('$apiUrl/auth/login');
    try {
      final response = await http.post(url,
          headers: {'Content-Type': 'application/json'},
          body: jsonEncode(data));
      final jsonData = jsonDecode(response.body);
      final isAuth = jsonData['isAdmin'];
      // store isAuth to local storage to keep login
      final prefs = await SharedPreferences.getInstance();
      await prefs.setBool('isAuth', isAuth);

      return isAuth;
    } catch (e) {
      throw Exception(e);
    }
  }

  // logout
  Future<bool> logout() async {
    final prefs = await SharedPreferences.getInstance();
    // clear local storage
    await prefs.clear();
    final isAuth = prefs.getBool('isAuth') ?? false;
    return isAuth;
  }
}
