import 'package:flutter/material.dart';

class JsonDataBuilder {
  // data illustrates the module content
  static final listInsertModules = [
    {'Text': Icons.text_fields},
    {'Product': Icons.stop},
    {'Image': Icons.image},
    {'Simple Image': Icons.six_mp_outlined},
    {'List Product': Icons.list_alt},
    {'Category Title Column': Icons.category},
    {'Category Title Grid 1': Icons.category},
    {'Category Title Grid 3': Icons.category},
    {'Category Title Grid 4': Icons.category},
    {'Category Title': Icons.title_sharp},
    {'Divider': Icons.safety_divider},
    {'Menu Item Price': Icons.price_change},
    {'Vertical Divider': Icons.vertical_align_bottom},
    {'Indexed Menu Item': Icons.menu},
    {'Underline Brand': Icons.branding_watermark},
    {'Service Item Column': Icons.room_service},
    {'Service Item Custom1': Icons.room_service},
    {'Service Item Thumbnail': Icons.integration_instructions},
    {'Framed Image': Icons.filter_frames},
    {'Product Row 1': Icons.add_box},
    {'Product Row 2': Icons.add_box},
    {'Product Row 3': Icons.add_box},
  ];

  static final template1 = {
    "_id": "64ee744398c3d4605b477abc",
    "templateSlug": "temp_restaurant_1",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        '21/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '0.9.w',
          'nameProductStyleMedium': '0.7.w',
          'priceProductStyle': '0.9.w',
          'priceProductStyleMedium': '0.7.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.5.w',
          'imageHeight': '13.h',
          'marginBottomImage': '1.5.w',
          'marginBottomProduct': '1.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        '16/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '1.5.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '16/10': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '1.7.w',
          'nameProductStyleMedium': '1.5.w',
          'priceProductStyle': '1.7.w',
          'priceProductStyleMedium': '1.5.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '18.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.5.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        '4/3': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '1.8.w',
          'nameProductStyleMedium': '1.5.w',
          'priceProductStyle': '1.8.w',
          'priceProductStyleMedium': '1.5.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.7.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
        },
      },
    },
    "dataSetting": {
      "d4343fa1-cc8d-4c2d-b45f-8cb41bca96c7": {
        "id": "d4343fa1-cc8d-4c2d-b45f-8cb41bca96c7",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/BG.png"
      },
      "317e055d-dd3e-41a4-98f0-9ab68b38af20": {
        "id": "317e055d-dd3e-41a4-98f0-9ab68b38af20",
        "type": "category",
        "name": "Category 1",
        "image": "/temp_restaurant_2/food(1).png",
        "description": ""
      },
      "d07b26a0-8fc0-491e-a4e8-2464fea7c2b7": {
        "id": "d07b26a0-8fc0-491e-a4e8-2464fea7c2b7",
        "type": "product",
        "name": "Tea",
        "price": "12",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "34ec1384-fbc3-43f2-be58-0d8fcfb4e513": {
        "id": "34ec1384-fbc3-43f2-be58-0d8fcfb4e513",
        "type": "product",
        "name": "Coffee",
        "price": "20",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "19c7241d-9022-4b2c-811f-1f4f8da72bdd": {
        "id": "19c7241d-9022-4b2c-811f-1f4f8da72bdd",
        "type": "product",
        "name": "Bread",
        "price": "34",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "195cf4d5-dbdf-4fb0-b784-a8cccde02b0c": {
        "id": "195cf4d5-dbdf-4fb0-b784-a8cccde02b0c",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/food2.png"
      },
      "19075b7e-e7a5-4b0a-8f1f-87e999533e4a": {
        "id": "19075b7e-e7a5-4b0a-8f1f-87e999533e4a",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/food4.png"
      },
      "47ab4fe3-80a5-4055-a17b-035cf454ad7b": {
        "id": "47ab4fe3-80a5-4055-a17b-035cf454ad7b",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/food8.png"
      },
      "0c901079-da7a-4e67-bfcf-802364be3c50": {
        "id": "0c901079-da7a-4e67-bfcf-802364be3c50",
        "type": "category",
        "name": "Category 2",
        "image": "/temp_restaurant_2/food(2).png",
        "description": ""
      },
      "b20d40ba-707e-49c9-a14b-c085c7bb6192": {
        "id": "b20d40ba-707e-49c9-a14b-c085c7bb6192",
        "type": "product",
        "name": "product 1",
        "price": "12",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "699a9a12-00fa-47d8-b271-76a29e9f6a24": {
        "id": "699a9a12-00fa-47d8-b271-76a29e9f6a24",
        "type": "product",
        "name": "product 2",
        "price": "20",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "cf148d7d-bdf5-49b1-a013-7d40c326efeb": {
        "id": "cf148d7d-bdf5-49b1-a013-7d40c326efeb",
        "type": "product",
        "name": "product 3",
        "price": "40",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "08810b23-41d1-4780-928b-f0cd671facfc": {
        "id": "08810b23-41d1-4780-928b-f0cd671facfc",
        "type": "product",
        "name": "product 4",
        "price": "78",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "6f1877ef-235d-4c21-8158-94bf2a63ee03": {
        "id": "6f1877ef-235d-4c21-8158-94bf2a63ee03",
        "type": "product",
        "name": "product 5",
        "price": "34",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "ee10ec6b-d6ec-4a1d-8354-3aa9e14bc380": {
        "id": "ee10ec6b-d6ec-4a1d-8354-3aa9e14bc380",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg1.jpg"
      },
      "d8d7d50e-b8fa-4ed4-8499-5ec2756e7636": {
        "id": "d8d7d50e-b8fa-4ed4-8499-5ec2756e7636",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg2.jpg"
      },
      "637a8b27-5524-4af6-9ba3-4f815feea51c": {
        "id": "637a8b27-5524-4af6-9ba3-4f815feea51c",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg3.jpg"
      },
      "64d79ca2-986b-411b-96ae-f0ec63c2c9f1": {
        "id": "64d79ca2-986b-411b-96ae-f0ec63c2c9f1",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg4.jpg"
      },
      "2677019a-2389-40c9-a2a1-573ce6af2e3d": {
        "id": "2677019a-2389-40c9-a2a1-573ce6af2e3d",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg5.jpg"
      },
      "5bb4f8ce-d4c8-41c5-8ed6-9726b83008ab": {
        "id": "5bb4f8ce-d4c8-41c5-8ed6-9726b83008ab",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food-bg6.jpg"
      },
      "aec4fb3f-0cae-4187-a9ef-2f593630f054": {
        "id": "aec4fb3f-0cae-4187-a9ef-2f593630f054",
        "type": "category",
        "name": "Category 3",
        "image": "/temp_restaurant_2/food(3).png",
        "description": ""
      },
      "4978d86b-6d74-4674-8517-b9f5a8f0d361": {
        "id": "4978d86b-6d74-4674-8517-b9f5a8f0d361",
        "type": "product",
        "name": "product 1",
        "price": "12",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "7eb3df57-ac5e-40e2-b822-42217d4a871b": {
        "id": "7eb3df57-ac5e-40e2-b822-42217d4a871b",
        "type": "product",
        "name": "product 2",
        "price": "20",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "72b39886-cbd5-41c9-b617-12c49e078b67": {
        "id": "72b39886-cbd5-41c9-b617-12c49e078b67",
        "type": "product",
        "name": "product 3",
        "price": "45",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "c1837fce-4299-4bf8-83d3-91dd2b0097cb": {
        "id": "c1837fce-4299-4bf8-83d3-91dd2b0097cb",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/Seperator-Line.png"
      },
      "91f0881d-997b-4aa4-8c05-a8d650a8f1ff": {
        "id": "91f0881d-997b-4aa4-8c05-a8d650a8f1ff",
        "type": "category",
        "name": "Category 4",
        "image": "",
        "description": "/temp_restaurant_2/food(4).png"
      },
      "a9797427-5f99-4c9b-8ddb-dc2f0fbb809a": {
        "id": "a9797427-5f99-4c9b-8ddb-dc2f0fbb809a",
        "type": "product",
        "name": "product 1",
        "price": "122",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "789e5f24-ce17-4971-b74f-3aa22de0fdf8": {
        "id": "789e5f24-ce17-4971-b74f-3aa22de0fdf8",
        "type": "product",
        "name": "product 2",
        "price": "45",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "49c86400-dc89-42bc-b036-31b18b4163ff": {
        "id": "49c86400-dc89-42bc-b036-31b18b4163ff",
        "type": "product",
        "name": "product 3",
        "price": "99",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "3ee735a3-155e-4b1c-9dc7-46e83be1c330": {
        "id": "3ee735a3-155e-4b1c-9dc7-46e83be1c330",
        "type": "category",
        "name": "Category 5",
        "image": "/temp_restaurant_2/food(5).png",
        "description": ""
      },
      "21a6a688-4f8b-4098-b640-1168aa6d1dfe": {
        "id": "21a6a688-4f8b-4098-b640-1168aa6d1dfe",
        "type": "product",
        "name": "product 1",
        "price": "11",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "2b90b1ac-cb21-41cc-a9d5-a87752361d62": {
        "id": "2b90b1ac-cb21-41cc-a9d5-a87752361d62",
        "type": "product",
        "name": "product 2",
        "price": "12",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "96a25345-51e0-42e7-a1a2-c897212b9188": {
        "id": "96a25345-51e0-42e7-a1a2-c897212b9188",
        "type": "product",
        "name": "product 3",
        "price": "44",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "85b8f2be-9f3c-40ef-96fa-4d1b6137a035": {
        "id": "85b8f2be-9f3c-40ef-96fa-4d1b6137a035",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/food7.png"
      }
    },
    "widgets_setting": {
      "id": "ffb527a7-5a32-4587-8c45-956afb2f7e26",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image": "d4343fa1-cc8d-4c2d-b45f-8cb41bca96c7",
          "boxFit": "fill",
        },
        "child": {
          "id": "b83a617c-feff-48ed-8f0b-5105cb9a256f",
          "headerName": "Row",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                "id": "00ac6177-aa24-4cfe-a500-58d794ec3a1e",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "642a0a5c-a630-4e3a-a0b4-4d84d83350a9",
                    "headerName": "Padding",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "id": "fe01308a-0664-480e-bc81-8c73543d0f32",
                        "headerName": "Column",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "dba67986-8f47-487d-8f70-8ac9b3785583",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "7446be8f-f107-47aa-8819-a22668699c6e",
                                  "headerName": "Container",
                                  "widgetName": "Container",
                                  "widgetSetting": {
                                    "margin": {"bottom": "1.w"},
                                    "decoration": {
                                      "boxFit": "none",
                                      // "color": "0x0"
                                    },
                                    "child": {
                                      "id":
                                          "17e317c5-6937-412d-a9a7-4f52be33df70",
                                      "headerName": "Row",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "textRightToLeft": "false",
                                        "children": [
                                          {
                                            "id":
                                                "bd305429-80b8-4e55-a75f-066461da05d1",
                                            "headerName": "Expanded",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "2",
                                              "child": {
                                                "id":
                                                    "bf39cdb7-131a-43fe-9695-ebd017d69f06",
                                                "headerName": "Container",
                                                "widgetName": "Container",
                                                "widgetSetting": {
                                                  "decoration": {
                                                    "boxFit": "none",
                                                    "color": "0x18FFFFFF",
                                                    "borderRadius": "30"
                                                  },
                                                  "child": {
                                                    "id":
                                                        "f3b06dd6-32d4-481c-a2af-168720e4a942",
                                                    "headerName":
                                                        "CategoryTitleSingleColumn",
                                                    "widgetName":
                                                        "CategoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      "dataList": {
                                                        "category":
                                                            "317e055d-dd3e-41a4-98f0-9ab68b38af20",
                                                        "product": [
                                                          "d07b26a0-8fc0-491e-a4e8-2464fea7c2b7",
                                                          "34ec1384-fbc3-43f2-be58-0d8fcfb4e513",
                                                          "19c7241d-9022-4b2c-811f-1f4f8da72bdd"
                                                        ]
                                                      },
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "colorCategoryTitle":
                                                          "0xffea872f",
                                                      "useThumbnailProduct":
                                                          "false",
                                                      "toUpperCaseNameProductName":
                                                          "false",
                                                      "useMediumStyle": "false",
                                                      "useFullWidthCategoryTitle":
                                                          "false"
                                                    }
                                                  },
                                                  "padding": {"all": "1.5.w"}
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "c75ea2ae-e49d-4876-8522-23b502605eed",
                                            "headerName": "SizedBox",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "width": "1.5.w",
                                              "child": {}
                                            }
                                          },
                                          {
                                            "id":
                                                "871726a6-84a2-4cfc-a0a0-66bb8966d1ec",
                                            "headerName": "Expanded",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "1",
                                              "child": {
                                                "id":
                                                    "e5fff551-cbf6-482e-ad28-ea4d522d2e9f",
                                                "headerName": "Column",
                                                "widgetName": "Column",
                                                "widgetSetting": {
                                                  "mainAxisAlignment": "start",
                                                  "children": [
                                                    {
                                                      "id":
                                                          "f16c1294-96be-4c2a-b63f-f6384dedf649",
                                                      "headerName": "Expanded",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "flex": "1",
                                                        "child": {
                                                          "id":
                                                              "b8415e53-5cf5-406e-b5e5-e197c79f2f04",
                                                          "headerName": "Image",
                                                          "widgetName": "Image",
                                                          "widgetSetting": {
                                                            "imageId":
                                                                "195cf4d5-dbdf-4fb0-b784-a8cccde02b0c",
                                                            "fixCover": "false"
                                                          }
                                                        }
                                                      }
                                                    },
                                                    {
                                                      "id":
                                                          "2c236fed-e9fa-4f3a-937e-e154d8a67492",
                                                      "headerName": "Expanded",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "flex": "1",
                                                        "child": {
                                                          "id":
                                                              "4204d042-77c3-4ea9-95c1-628fe13cab50",
                                                          "headerName": "Image",
                                                          "widgetName": "Image",
                                                          "widgetSetting": {
                                                            "imageId":
                                                                "19075b7e-e7a5-4b0a-8f1f-87e999533e4a",
                                                            "fixCover": "false"
                                                          }
                                                        }
                                                      }
                                                    },
                                                    {
                                                      "id":
                                                          "e2271be9-f3c1-4cba-8f8f-120c02c3e20e",
                                                      "headerName": "Expanded",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "flex": "1",
                                                        "child": {
                                                          "id":
                                                              "dfc130de-e403-4b8a-b9a3-e4d3e898a6ee",
                                                          "headerName": "Image",
                                                          "widgetName": "Image",
                                                          "widgetSetting": {
                                                            "imageId":
                                                                "47ab4fe3-80a5-4055-a17b-035cf454ad7b",
                                                            "fixCover": "false"
                                                          }
                                                        }
                                                      }
                                                    }
                                                  ]
                                                }
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "f931e4b2-56be-4648-b41a-7c01140ec764",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"height": "2.h", "child": {}}
                            },
                            {
                              "id": "fba34fed-6293-4180-844c-ca1875c77a23",
                              "headerName": "CategoryTitleGridLayout1",
                              "widgetName": "CategoryTitleGridLayout1",
                              "widgetSetting": {
                                "dataList": {
                                  "category":
                                      "0c901079-da7a-4e67-bfcf-802364be3c50",
                                  "product": [
                                    "b20d40ba-707e-49c9-a14b-c085c7bb6192",
                                    "699a9a12-00fa-47d8-b271-76a29e9f6a24",
                                    "cf148d7d-bdf5-49b1-a013-7d40c326efeb",
                                    "08810b23-41d1-4780-928b-f0cd671facfc",
                                    "6f1877ef-235d-4c21-8158-94bf2a63ee03"
                                  ],
                                  "image": [
                                    "ee10ec6b-d6ec-4a1d-8354-3aa9e14bc380",
                                    "d8d7d50e-b8fa-4ed4-8499-5ec2756e7636",
                                    "637a8b27-5524-4af6-9ba3-4f815feea51c"
                                  ]
                                },
                                "useBackgroundCategoryTitle": "false",
                                "useFullWidthCategoryTitle": "false",
                                "colorPrice": "0xFFF47B22",
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              {
                "id": "b555259e-a6f7-45e0-bf1a-5c7b28b019fb",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "fbd4037c-8f2d-45be-92ab-247194e41436",
                    "headerName": "Padding",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "id": "a9016126-a202-473a-973f-01cd91478b80",
                        "headerName": "Column",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "mainAxisAlignment": "start",
                          "children": [
                            {
                              "id": "c10ec9ee-985b-48f6-91c1-921a54324c94",
                              "headerName": "Row",
                              "widgetName": "Row",
                              "widgetSetting": {
                                "textRightToLeft": "false",
                                "mainAxisAlignment": "start",
                                "crossAxisAlignment": "start",
                                "children": [
                                  {
                                    "id":
                                        "c2e766af-2ce0-41e1-a16d-83461cf04cfb",
                                    "headerName": "Expanded",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "flex": "1",
                                      "child": {
                                        "id":
                                            "e8157803-ed6a-4d88-89c4-4cac1b546acd",
                                        "headerName": "Image",
                                        "widgetName": "Image",
                                        "widgetSetting": {
                                          "imageId":
                                              "64d79ca2-986b-411b-96ae-f0ec63c2c9f1",
                                          "fixCover": "true"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "a1c7ece4-9d14-4318-9dc8-33500769f96a",
                                    "headerName": "SizedBox",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "width": "1.w",
                                      "child": {}
                                    }
                                  },
                                  {
                                    "id":
                                        "a838f20d-e7e8-4362-8731-e37d882f383b",
                                    "headerName": "Expanded",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "flex": "1",
                                      "child": {
                                        "id":
                                            "48c335d7-7df0-4f2f-a7b5-a43992778690",
                                        "headerName": "Image",
                                        "widgetName": "Image",
                                        "widgetSetting": {
                                          "imageId":
                                              "2677019a-2389-40c9-a2a1-573ce6af2e3d",
                                          "fixCover": "true"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "45bd5657-119f-4eac-87e3-c6cdfcdab3db",
                                    "headerName": "SizedBox",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "width": "1.w",
                                      "child": {}
                                    }
                                  },
                                  {
                                    "id":
                                        "0a59af8b-ef3a-4632-b4c7-85686a321e7e",
                                    "headerName": "Expanded",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "flex": "1",
                                      "child": {
                                        "id":
                                            "0bee142f-f3f3-4bb7-a730-ea533067ca80",
                                        "headerName": "Image",
                                        "widgetName": "Image",
                                        "widgetSetting": {
                                          "imageId":
                                              "5bb4f8ce-d4c8-41c5-8ed6-9726b83008ab",
                                          "fixCover": "true"
                                        }
                                      }
                                    }
                                  }
                                ]
                              }
                            },
                            {
                              "id": "e81d4e18-a007-4c8d-97cc-20098b4c2c85",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "69ede24a-78ac-4bd6-801b-f6aefde46d0e",
                                  "headerName": "Row",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      {
                                        "id":
                                            "8e84a69c-9bde-459c-b851-67ae80216a2e",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "cdd33d38-5f7d-414a-8090-a896f02c79b5",
                                            "headerName":
                                                "CategoryTitleSingleColumn",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category":
                                                    "aec4fb3f-0cae-4187-a9ef-2f593630f054",
                                                "product": [
                                                  "4978d86b-6d74-4674-8517-b9f5a8f0d361",
                                                  "7eb3df57-ac5e-40e2-b822-42217d4a871b",
                                                  "72b39886-cbd5-41c9-b617-12c49e078b67"
                                                ]
                                              },
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "colorPrice": "0xffe28918",
                                              "useThumbnailProduct": "false",
                                              "toUpperCaseNameProductName":
                                                  "false",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "a642dd92-4026-480d-9b99-807880ff5ed5",
                                        "headerName": "Container",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"},
                                          "decoration": {
                                            "boxFit": "none",
                                          },
                                          "child": {
                                            "id":
                                                "46f3f7cd-3393-4f99-b747-d154a28d3c30",
                                            "headerName": "Image",
                                            "widgetName": "Image",
                                            "widgetSetting": {
                                              "imageId":
                                                  "c1837fce-4299-4bf8-83d3-91dd2b0097cb",
                                              "fixCover": "false"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "969ffad1-e4ae-4f25-aafd-aad30a5a72bc",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "a56a0edd-dd00-4005-a63c-045938e3e30d",
                                            "headerName":
                                                "CategoryTitleSingleColumn",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category":
                                                    "91f0881d-997b-4aa4-8c05-a8d650a8f1ff",
                                                "product": [
                                                  "a9797427-5f99-4c9b-8ddb-dc2f0fbb809a",
                                                  "789e5f24-ce17-4971-b74f-3aa22de0fdf8",
                                                  "49c86400-dc89-42bc-b036-31b18b4163ff"
                                                ]
                                              },
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "colorPrice": "0xeef4ad2e",
                                              "useThumbnailProduct": "false",
                                              "toUpperCaseNameProductName":
                                                  "false",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },
                            {
                              "id": "1bc43bc8-4e86-4310-ad2c-1f325eefe7b8",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "e3866190-042c-4119-9450-0dcddd20e0d7",
                                  "headerName": "Row",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      {
                                        "id":
                                            "231ba90e-a5ed-4725-9578-afe272630997",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "4702252d-cf22-45ce-8573-5b54bc03af29",
                                            "headerName":
                                                "CategoryTitleSingleColumn",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category":
                                                    "3ee735a3-155e-4b1c-9dc7-46e83be1c330",
                                                "product": [
                                                  "21a6a688-4f8b-4098-b640-1168aa6d1dfe",
                                                  "2b90b1ac-cb21-41cc-a9d5-a87752361d62",
                                                  "96a25345-51e0-42e7-a1a2-c897212b9188"
                                                ]
                                              },
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "colorPrice": "0xf6e3691b",
                                              "useThumbnailProduct": "false",
                                              "toUpperCaseNameProductName":
                                                  "false",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "15d9240f-99f0-49a4-8f9d-35769abd5629",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "95217090-3de2-4daf-bc63-1cc9dc5df1c5",
                                            "headerName": "Padding",
                                            "widgetName": "Padding",
                                            "widgetSetting": {
                                              "padding": {"all": "1.w"},
                                              "child": {
                                                "id":
                                                    "f8f8058c-b325-4f5b-b18d-780d7a599b98",
                                                "headerName": "Image",
                                                "widgetName": "Image",
                                                "widgetSetting": {
                                                  "imageId":
                                                      "85b8f2be-9f3c-40ef-96fa-4d1b6137a035",
                                                  "fixCover": "false"
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              }
            ]
          }
        }
      }
    }
  };

  static final template2 = {
    "_id": "64ee744398c3d4605b477abd",
    "templateSlug": "temp_restaurant_2",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'w900',
          'fontFamily': 'Open Sans',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '0.8.w',
          'priceProductStyle': '0.8.w',
          'discriptionProductStyle': '0.7.w',
          'imageHeight': '25.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '21/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '0.9.w',
          'priceProductStyle': '0.9.w',
          'discriptionProductStyle': '0.8.w',
          'imageHeight': '30.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '16/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.w',
          'imageHeight': '30.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '16/10': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.w',
          'imageHeight': '40.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '4/3': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.8.w',
          'priceProductStyle': '1.8.w',
          'discriptionProductStyle': '1.3.w',
          'imageHeight': '40.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.w',
          'imageHeight': '37.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
      },
    },
    "dataSetting": {
      "f6bc6e94-5ad0-441f-8042-b3b8a87a5304": {
        "id": "f6bc6e94-5ad0-441f-8042-b3b8a87a5304",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-2.png"
      },
      "d9dc8a77-d603-42cc-8aa5-76b40ac9c199": {
        "id": "d9dc8a77-d603-42cc-8aa5-76b40ac9c199",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description": ""
      },
      "306c5d54-fd32-412e-9d35-411a08a17ff3": {
        "id": "306c5d54-fd32-412e-9d35-411a08a17ff3",
        "type": "image",
        "image": ""
      },
      "06636cc6-7624-4ea0-b7ed-17b8d982b55a": {
        "id": "06636cc6-7624-4ea0-b7ed-17b8d982b55a",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food1.png"
      },
      "30003d2f-2e5e-4281-9da7-335d28163637": {
        "id": "30003d2f-2e5e-4281-9da7-335d28163637",
        "type": "product",
        "name": "product 1",
        "price": "12",
        "salePrice": "",
        "image": "",
        "description": "description"
      },
      "98112f43-da02-4d0e-b49a-373b51a8f3c4": {
        "id": "98112f43-da02-4d0e-b49a-373b51a8f3c4",
        "type": "product",
        "name": "product 2",
        "price": "45",
        "salePrice": "",
        "image": "",
        "description": "description"
      },
      "aaa60e17-42b5-417e-934d-122ae7becc3c": {
        "id": "aaa60e17-42b5-417e-934d-122ae7becc3c",
        "type": "product",
        "name": "product 3",
        "price": "33",
        "salePrice": "",
        "image": "",
        "description": "description"
      },
      "9a15ea8f-fcc4-465a-9f4e-041cb6c3c01a": {
        "id": "9a15ea8f-fcc4-465a-9f4e-041cb6c3c01a",
        "type": "product",
        "name": "product 4",
        "price": "77",
        "salePrice": "",
        "image": "",
        "description": "description"
      },
      "cafaaf19-0537-4e60-b42f-08877634adfe": {
        "id": "cafaaf19-0537-4e60-b42f-08877634adfe",
        "type": "category",
        "name": "Category 2",
        "image": "",
        "description": ""
      },
      "59d4b4ed-0a18-4b5a-9261-894b85cc5f48": {
        "id": "59d4b4ed-0a18-4b5a-9261-894b85cc5f48",
        "type": "image",
        "image": ""
      },
      "1d4bdcf1-672a-487d-bfd3-491d20dc5a66": {
        "id": "1d4bdcf1-672a-487d-bfd3-491d20dc5a66",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food2-1.png"
      },
      "2c3fa2f8-b140-4bed-abc5-4c16d8f80d20": {
        "id": "2c3fa2f8-b140-4bed-abc5-4c16d8f80d20",
        "type": "product",
        "name": "product 5",
        "price": "11",
        "salePrice": "",
        "image": "",
        "description": "description"
      },
      "367caf8c-6097-411e-8e14-048a59b760df": {
        "id": "367caf8c-6097-411e-8e14-048a59b760df",
        "type": "product",
        "name": "product 6",
        "price": "12",
        "salePrice": "",
        "image": "",
        "description": "description"
      },
      "417ca7ef-522d-44a6-be60-877f4a324a6b": {
        "id": "417ca7ef-522d-44a6-be60-877f4a324a6b",
        "type": "product",
        "name": "product 7",
        "price": "99",
        "salePrice": "",
        "image": "",
        "description": "description"
      },
      "5c8a9c4a-7338-433c-9b60-843f62ef27b4": {
        "id": "5c8a9c4a-7338-433c-9b60-843f62ef27b4",
        "type": "product",
        "name": "product 8",
        "price": "88",
        "salePrice": "",
        "image": "",
        "description": "description"
      },
      "02e02fd8-47ca-4734-8277-c67002480053": {
        "id": "02e02fd8-47ca-4734-8277-c67002480053",
        "type": "category",
        "name": "Category 3",
        "image": "",
        "description": ""
      },
      "c3bf8955-f773-48a3-8b06-18840ce857ad": {
        "id": "c3bf8955-f773-48a3-8b06-18840ce857ad",
        "type": "image",
        "image": ""
      },
      "4ba3df63-1313-4027-9bd8-0739d1d63567": {
        "id": "4ba3df63-1313-4027-9bd8-0739d1d63567",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food3.png"
      },
      "20097e7c-f32d-499b-9a2c-7e70c6a1c38d": {
        "id": "20097e7c-f32d-499b-9a2c-7e70c6a1c38d",
        "type": "product",
        "name": "product 9",
        "price": "17",
        "salePrice": "",
        "image": "",
        "description": "descrtiption"
      },
      "31f5a463-6fcf-4547-9cf8-218ea4ab1960": {
        "id": "31f5a463-6fcf-4547-9cf8-218ea4ab1960",
        "type": "product",
        "name": "product 10",
        "price": "90",
        "salePrice": "",
        "image": "",
        "description": "description"
      },
      "fc3c1d9e-ac7a-40ab-9c9a-05a34f745bf7": {
        "id": "fc3c1d9e-ac7a-40ab-9c9a-05a34f745bf7",
        "type": "product",
        "name": "product 11",
        "price": "78",
        "salePrice": "",
        "image": "",
        "description": "description"
      },
      "7943d333-cd1a-46e1-a476-4b6c5917ed7a": {
        "id": "7943d333-cd1a-46e1-a476-4b6c5917ed7a",
        "type": "product",
        "name": "product 12",
        "price": "90",
        "salePrice": "",
        "image": "",
        "description": "description"
      }
    },
    "widgets_setting": {
      "id": "d74f052f-38e7-472e-aec8-b50fdbe24227",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image": "f6bc6e94-5ad0-441f-8042-b3b8a87a5304",
          "boxFit": "cover"
        },
        "child": {
          "id": "28ba6c9d-2135-4364-875f-352baf444e2b",
          "headerName": "Row",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                "id": "d488e150-6263-4f2a-8358-2a5087f88259",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "6baa3c1e-6260-462a-bda5-2ccc7738a4f0",
                    "headerName": "Padding",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "child": {
                        "id": "1a1bd127-c8e5-4638-ae63-ea273f4787ba",
                        "headerName": "Column",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "853830c2-818e-4876-abda-6fd60d87addf",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "id": "e0c0a49b-e61b-4e12-85b2-e193221df830",
                                  "headerName": "Column",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "fa6e64d7-1a53-4b6c-bfee-65ae55b2576a",
                                        "headerName": "CategoryTitle",
                                        "widgetName": "CategoryTitle",
                                        "widgetSetting": {
                                          "useBackground": "true",
                                          "useFullWidth": "true",
                                          "categoryId":
                                              "d9dc8a77-d603-42cc-8aa5-76b40ac9c199",
                                          "imageId":
                                              "306c5d54-fd32-412e-9d35-411a08a17ff3",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E"
                                        }
                                      },
                                      {
                                        "id":
                                            "6bfd960e-b0d0-4e0c-a3d0-fb935f2d2d10",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "9ba07ad3-c571-4b45-b7cd-1dd10bf46ee0",
                                            "headerName": "Container",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "decoration": {
                                                "boxFit": "none",
                                                "borderRadius": "30"
                                              },
                                              "child": {
                                                "id":
                                                    "a4a37878-31bd-4c09-8eb8-6c4cd06970ea",
                                                "headerName": "Center",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "id":
                                                        "33b14af6-17c9-41a3-9ae9-46399324bd73",
                                                    "headerName": "Image",
                                                    "widgetName": "Image",
                                                    "widgetSetting": {
                                                      "fixCover": "false",
                                                      "imageId":
                                                          "06636cc6-7624-4ea0-b7ed-17b8d982b55a"
                                                    }
                                                  }
                                                }
                                              },
                                              "height": "30.h",
                                              "padding": {"vertical": "1.5.w"},
                                              "margin": {"bottom": "0.h"}
                                            }
                                          }
                                        }
                                      }
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center"
                                  }
                                }
                              }
                            },
                            {
                              "id": "d932cef5-f87a-49bf-894a-d316e62c1837",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "id": "fcb68fa0-8009-4d31-88ec-e97dfca1758b",
                                  "headerName": "ProductList",
                                  "widgetName": "ProductList",
                                  "widgetSetting": {
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "useThumbnailProduct": "false",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2",
                                    "dataList": [
                                      "30003d2f-2e5e-4281-9da7-335d28163637",
                                      "98112f43-da02-4d0e-b49a-373b51a8f3c4",
                                      "aaa60e17-42b5-417e-934d-122ae7becc3c",
                                      "9a15ea8f-fcc4-465a-9f4e-041cb6c3c01a"
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      },
                      "padding": {"all": "2.w"}
                    }
                  }
                }
              },
              {
                "id": "9b1363bb-e37a-4155-b356-1568edb8bf90",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "56522058-2784-43a6-934b-30c470e7ba72",
                    "headerName": "Padding",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "child": {
                        "id": "38ed2e06-8d8e-4350-a4b7-e85674c0f7a2",
                        "headerName": "Column",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "44242d4c-c96f-4ea3-8bdb-2e2a89cdd06f",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "id": "b64853f4-5432-4d79-80ce-033817ead5d3",
                                  "headerName": "Column",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "24692cf7-f45d-48ff-977d-e7a3eb86efd2",
                                        "headerName": "CategoryTitle",
                                        "widgetName": "CategoryTitle",
                                        "widgetSetting": {
                                          "useBackground": "true",
                                          "useFullWidth": "true",
                                          "categoryId":
                                              "cafaaf19-0537-4e60-b42f-08877634adfe",
                                          "imageId":
                                              "59d4b4ed-0a18-4b5a-9261-894b85cc5f48",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E"
                                        }
                                      },
                                      {
                                        "id":
                                            "4ff950e5-57ec-4df5-89f1-a66b0094432e",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "7f786e91-8264-40ea-95c0-6c6c14e36993",
                                            "headerName": "Container",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "decoration": {
                                                "boxFit": "none",
                                                "borderRadius": "30"
                                              },
                                              "child": {
                                                "id":
                                                    "2490aef5-93de-4958-91a8-f2ff1d04e810",
                                                "headerName": "Center",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "id":
                                                        "d42648a6-e958-494b-839b-5d40dbd38895",
                                                    "headerName": "Image",
                                                    "widgetName": "Image",
                                                    "widgetSetting": {
                                                      "fixCover": "false",
                                                      "imageId":
                                                          "1d4bdcf1-672a-487d-bfd3-491d20dc5a66"
                                                    }
                                                  }
                                                }
                                              },
                                              "height": "30.h",
                                              "padding": {"vertical": "1.5.w"},
                                              "margin": {"bottom": "0.h"}
                                            }
                                          }
                                        }
                                      }
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center"
                                  }
                                }
                              }
                            },
                            {
                              "id": "1b0b27de-fc47-4974-a4b4-d4e8c46963df",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "id": "172e5392-4287-4275-9263-00353aad61b5",
                                  "headerName": "ProductList",
                                  "widgetName": "ProductList",
                                  "widgetSetting": {
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "useThumbnailProduct": "false",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2",
                                    "dataList": [
                                      "2c3fa2f8-b140-4bed-abc5-4c16d8f80d20",
                                      "367caf8c-6097-411e-8e14-048a59b760df",
                                      "417ca7ef-522d-44a6-be60-877f4a324a6b",
                                      "5c8a9c4a-7338-433c-9b60-843f62ef27b4"
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      },
                      "padding": {"all": "2.w"}
                    }
                  }
                }
              },
              {
                "id": "b25a1453-77bc-457b-b0ee-7f7c6cfe81ff",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "c8b4fec0-a5f2-4417-9e68-39d211d6edee",
                    "headerName": "Padding",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "child": {
                        "id": "b144eb2c-a4fc-4723-8ab1-aaa30f85ee41",
                        "headerName": "Column",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "c9546100-1255-40d2-947b-7f493b81e4d4",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "id": "41774f25-e1f8-4470-ba6e-b12d2d7feba3",
                                  "headerName": "Column",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "dca8c78a-044f-418b-8214-ad47a5efa72a",
                                        "headerName": "CategoryTitle",
                                        "widgetName": "CategoryTitle",
                                        "widgetSetting": {
                                          "useBackground": "true",
                                          "useFullWidth": "true",
                                          "categoryId":
                                              "02e02fd8-47ca-4734-8277-c67002480053",
                                          "imageId":
                                              "c3bf8955-f773-48a3-8b06-18840ce857ad",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E"
                                        }
                                      },
                                      {
                                        "id":
                                            "2c84f118-f604-4ba0-9756-98cbbb6ea3f1",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "411bece7-c92f-4406-aa00-90672fc6f8d6",
                                            "headerName": "Container",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "decoration": {
                                                "boxFit": "none",
                                                "borderRadius": "30"
                                              },
                                              "child": {
                                                "id":
                                                    "92e6e263-e618-434b-8cfc-0072d9df0126",
                                                "headerName": "Center",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "id":
                                                        "213405e2-65b8-4e83-a138-bedd9fdf8120",
                                                    "headerName": "Image",
                                                    "widgetName": "Image",
                                                    "widgetSetting": {
                                                      "fixCover": "false",
                                                      "imageId":
                                                          "4ba3df63-1313-4027-9bd8-0739d1d63567"
                                                    }
                                                  }
                                                }
                                              },
                                              "height": "30.h",
                                              "padding": {"vertical": "1.5.w"},
                                              "margin": {"bottom": "0.h"}
                                            }
                                          }
                                        }
                                      }
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center"
                                  }
                                }
                              }
                            },
                            {
                              "id": "d9b63001-eeb2-4c1d-ae03-fc9cbcff0f6d",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "id": "6b5d2b76-d143-44a2-b211-b465da62d711",
                                  "headerName": "ProductList",
                                  "widgetName": "ProductList",
                                  "widgetSetting": {
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "useThumbnailProduct": "false",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2",
                                    "dataList": [
                                      "20097e7c-f32d-499b-9a2c-7e70c6a1c38d",
                                      "31f5a463-6fcf-4547-9cf8-218ea4ab1960",
                                      "fc3c1d9e-ac7a-40ab-9c9a-05a34f745bf7",
                                      "7943d333-cd1a-46e1-a476-4b6c5917ed7a"
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      },
                      "padding": {"all": "2.w"}
                    }
                  }
                }
              }
            ]
          }
        }
      }
    }
  };

  static final template3 = {
    "_id": "64ee744398c3d4605b477abe",
    "templateSlug": "temp_restaurant_3",
    "RatioSetting": {
      "textStyle": {
        'categoryNameStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Alfa Slab One',
          'letterSpacing': '5',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Poppins',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Poppins',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Poppins',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Poppins',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Poppins',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.w',
          'nameProductStyle': '0.7.w',
          'nameProductStyleMedium': '0.5.w',
          'priceProductStyle': '0.7.w',
          'priceProductStyleMedium': '0.5.w',
          'discriptionProductStyle': '0.5.w',
          'discriptionProductStyleMedium': '0.3.w',
          'imageHeight': '50.h',
          'marginBottomImage': '0',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '5.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '21/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '1.w',
          'nameProductStyleMedium': '0.7.w',
          'priceProductStyle': '1.w',
          'priceProductStyleMedium': '0.7.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '50.h',
          'marginBottomImage': '0',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '5.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '16/9': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '20.h',
          'marginBottomImage': '0',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '5.h',
          'paddingVerticalCategorTilte': '0.5.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '16/10': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '50.h',
          'marginBottomImage': '0',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '5.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '4/3': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '2.w',
          'nameProductStyleMedium': '1.5.w',
          'priceProductStyle': '2.w',
          'priceProductStyleMedium': '1.5.w',
          'discriptionProductStyle': '1.5.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '50.h',
          'marginBottomImage': '0',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '5.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        'default': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '20.h',
          'marginBottomImage': '0',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '5.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
      },
    },
    "dataSetting": {
      "17597a74-9735-4c11-9ae8-56cfd9b87ca7": {
        "id": "17597a74-9735-4c11-9ae8-56cfd9b87ca7",
        "type": "image",
        "image": "/temp_restaurant_3/food-bg(4).jpg"
      },
      "045fc8f3-e032-49de-8398-6fc04e4d2c70": {
        "id": "045fc8f3-e032-49de-8398-6fc04e4d2c70",
        "type": "category",
        "name": "Category 1",
        "image": "/temp_restaurant_3/food_temp3(1).png",
        "description": ""
      },
      "805ebbe0-0202-4586-b4c3-04598498a38a": {
        "id": "805ebbe0-0202-4586-b4c3-04598498a38a",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "83f453c1-a7be-49e5-82d8-102f3187f39b": {
        "id": "83f453c1-a7be-49e5-82d8-102f3187f39b",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "aee6c93d-be84-487f-8dfe-fb516b2ec7a2": {
        "id": "aee6c93d-be84-487f-8dfe-fb516b2ec7a2",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "ce4db955-a38a-4370-9fd1-d64293558f75": {
        "id": "ce4db955-a38a-4370-9fd1-d64293558f75",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "f2755749-7021-4d7f-ab55-e3f0f98c6e50": {
        "id": "f2755749-7021-4d7f-ab55-e3f0f98c6e50",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "36d2e3a8-a54f-40f1-a780-d6281520990a": {
        "id": "36d2e3a8-a54f-40f1-a780-d6281520990a",
        "type": "image",
        "image": "/temp_restaurant_3/food-temp3(1).png"
      },
      "e9c2ec66-c84a-4061-9d74-494cf64c99ff": {
        "id": "e9c2ec66-c84a-4061-9d74-494cf64c99ff",
        "type": "image",
        "image": "/temp_restaurant_3/food-temp3(2).png"
      },
      "dcf2a4ab-a075-4d72-a076-e3c72c89be02": {
        "id": "dcf2a4ab-a075-4d72-a076-e3c72c89be02",
        "type": "image",
        "image": "/temp_restaurant_3/food-temp3(3).png"
      },
      "45093371-1cdc-4c82-a652-0575aa3ce68f": {
        "id": "45093371-1cdc-4c82-a652-0575aa3ce68f",
        "type": "category",
        "name": "Category 2",
        "image": "/temp_restaurant_2/food(3).png",
        "description": ""
      },
      "71ef4a61-975e-4615-832d-085ec13f2622": {
        "id": "71ef4a61-975e-4615-832d-085ec13f2622",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "d0ad3719-7613-4e2c-915a-4dfcbc4dd84e": {
        "id": "d0ad3719-7613-4e2c-915a-4dfcbc4dd84e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "89dbe1a3-7e66-4a1e-97d3-17cb8c35a6b8": {
        "id": "89dbe1a3-7e66-4a1e-97d3-17cb8c35a6b8",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "64e639d1-aaf3-47ff-a01a-d820c09c4ff7": {
        "id": "64e639d1-aaf3-47ff-a01a-d820c09c4ff7",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "33419811-4719-4a7b-a229-6962459a9550": {
        "id": "33419811-4719-4a7b-a229-6962459a9550",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "94004bc0-c777-4c38-88f3-7654874475dc": {
        "id": "94004bc0-c777-4c38-88f3-7654874475dc",
        "type": "image",
        "image": "/temp_restaurant_3/food-temp3(4).png"
      },
      "b5e4c87a-3467-471b-8a8f-21e8e481979f": {
        "id": "b5e4c87a-3467-471b-8a8f-21e8e481979f",
        "type": "image",
        "image": "/temp_restaurant_3/food-temp3(5).png"
      },
      "749368eb-7166-4cd1-8122-123ed0e02ca8": {
        "id": "749368eb-7166-4cd1-8122-123ed0e02ca8",
        "type": "image",
        "image": "/temp_restaurant_3/food-temp3(6).png"
      },
      "7a32cdae-7dee-4565-9f6b-487f13c431c9": {
        "id": "7a32cdae-7dee-4565-9f6b-487f13c431c9",
        "type": "category",
        "name": "Category 3",
        "image": "/temp_restaurant_2/food(3).png",
        "description": ""
      },
      "24f7518f-1106-44e9-9a5b-b371370a5506": {
        "id": "24f7518f-1106-44e9-9a5b-b371370a5506",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "128c5e6d-f3de-4159-8bbe-d4db369719f5": {
        "id": "128c5e6d-f3de-4159-8bbe-d4db369719f5",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "f9944cb3-7ce0-4c41-9a9d-241573360def": {
        "id": "f9944cb3-7ce0-4c41-9a9d-241573360def",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "60b554be-8504-43ca-8ab0-8bd23e942333": {
        "id": "60b554be-8504-43ca-8ab0-8bd23e942333",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "60b554be-8504-43ca-8ab0-8bd23e942666": {
        "id": "60b554be-8504-43ca-8ab0-8bd23e942333",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "3f353d7a-766b-4dff-b225-f3501011c17f": {
        "id": "3f353d7a-766b-4dff-b225-f3501011c17f",
        "type": "image",
        "image": "/temp_restaurant_3/food-temp3(7).png"
      },
      "2b98c8e4-b753-45a3-b406-316f0395c6c7": {
        "id": "2b98c8e4-b753-45a3-b406-316f0395c6c7",
        "type": "image",
        "image": "/temp_restaurant_3/food-temp3(8).png"
      },
      "8a4efda5-f1b7-41b1-ac0e-bb207e814f77": {
        "id": "8a4efda5-f1b7-41b1-ac0e-bb207e814f77",
        "type": "image",
        "image": "/temp_restaurant_3/food-temp3(9).png"
      }
    },
    "widgets_setting": {
      "id": "a8311bd8-9761-4d11-afc6-89c354c01b70",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image": "17597a74-9735-4c11-9ae8-56cfd9b87ca7",
          "boxFit": "cover"
        },
        "child": {
          "id": "710f64a1-9d33-42c1-8b25-3522040b16b5",
          "headerName": "Container",
          "widgetName": "Container",
          "widgetSetting": {
            "decoration": {"boxFit": "none", "color": "0xe5000000"},
            "child": {
              "id": "8254a86c-8835-4fdd-915a-f519700afbea",
              "headerName": "Row",
              "widgetName": "Row",
              "widgetSetting": {
                "textRightToLeft": "false",
                "children": [
                  {
                    "id": "cba3ccea-9d21-4fc8-a351-3cf51774f2a0",
                    "headerName": "Expanded",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "id": "7cbbf0b5-da19-4133-a390-defb7c61294e",
                        "headerName": "Padding",
                        "widgetName": "Padding",
                        "widgetSetting": {
                          "child": {
                            "id": "3f2cd69c-2e49-4ada-a32a-0e0d982c1e30",
                            "headerName": "Column",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "children": [
                                {
                                  "id": "b88374ea-4f16-49c0-a57c-cf968e67e1fd",
                                  "headerName": "CategoryTitleSingleColumn",
                                  "widgetName": "CategoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "dataList": {
                                      "category":
                                          "045fc8f3-e032-49de-8398-6fc04e4d2c70",
                                      "product": [
                                        "805ebbe0-0202-4586-b4c3-04598498a38a",
                                        "83f453c1-a7be-49e5-82d8-102f3187f39b",
                                        "aee6c93d-be84-487f-8dfe-fb516b2ec7a2",
                                        "ce4db955-a38a-4370-9fd1-d64293558f75",
                                        "f2755749-7021-4d7f-ab55-e3f0f98c6e50"
                                      ]
                                    },
                                    "useBackgroundCategoryTitle": "true",
                                    "useThumbnailProduct": "false",
                                    "toUpperCaseNameProductName": "false",
                                    "useMediumStyle": "false",
                                    "useFullWidthCategoryTitle": "true",
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff"
                                  }
                                },
                                {
                                  "id": "d69aafcf-1b93-4eec-ad76-d30711a3576b",
                                  "headerName": "PaddingByRatio",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "child": {},
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                {
                                  "id": "bdd59e37-7c3c-4c50-abd8-3c85c8b6731e",
                                  "headerName": "Expanded",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "flex": "1",
                                    "child": {
                                      "id":
                                          "148f97cc-6f87-47da-b3c3-9aef6952ebd5",
                                      "headerName": "Row",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "textRightToLeft": "false",
                                        "children": [
                                          {
                                            "id":
                                                "5d194e38-fb3c-4b02-98b2-d8d39f58e169",
                                            "headerName": "Expanded",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "1",
                                              "child": {
                                                "id":
                                                    "5b336bf0-4e21-40e5-bd35-efc955353558",
                                                "headerName": "SimpleImage",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "fit": "contain",
                                                  "imageDataKey":
                                                      "36d2e3a8-a54f-40f1-a780-d6281520990a"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "906a531f-8aff-4c84-98d7-e9d910060a82",
                                            "headerName": "SizedBox",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "child": {},
                                              "width": "2.w"
                                            }
                                          },
                                          {
                                            "id":
                                                "ad9843de-ac83-427e-b60e-487175c86e3a",
                                            "headerName": "Expanded",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "1",
                                              "child": {
                                                "id":
                                                    "62f8dd29-91f3-4a7b-acb3-12d8de1a7189",
                                                "headerName": "SimpleImage",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "fit": "contain",
                                                  "imageDataKey":
                                                      "e9c2ec66-c84a-4061-9d74-494cf64c99ff"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "c951f2f5-8b28-4c35-b541-c9b68cb4c45b",
                                            "headerName": "SizedBox",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "child": {},
                                              "width": "2.w"
                                            }
                                          },
                                          {
                                            "id":
                                                "34c85fa7-db21-44b7-b551-70d0c2caf1b1",
                                            "headerName": "Expanded",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "1",
                                              "child": {
                                                "id":
                                                    "913b3898-749d-4bdd-bcb4-cc6821a8279d",
                                                "headerName": "SimpleImage",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "fit": "contain",
                                                  "imageDataKey":
                                                      "dcf2a4ab-a075-4d72-a076-e3c72c89be02"
                                                }
                                              }
                                            }
                                          }
                                        ],
                                        "crossAxisAlignment": "start"
                                      }
                                    }
                                  }
                                }
                              ],
                              "mainAxisAlignment": "start"
                            }
                          },
                          "padding": {"all": "2.w"}
                        }
                      }
                    }
                  },
                  {
                    "id": "03f48d25-3fca-4caa-a5d3-bf44c0645042",
                    "headerName": "Expanded",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "id": "19b1180e-43a3-4c13-9ff8-2a213ff48823",
                        "headerName": "Container",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "decoration": {
                            "boxFit": "none",
                            "color": "0x09ffffff"
                          },
                          "child": {
                            "id": "e884ea4f-e136-4ce7-a6f1-dc46864036f9",
                            "headerName": "Column",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "children": [
                                {
                                  "id": "be557420-2c58-4d03-ae93-788d8056c5bb",
                                  "headerName": "CategoryTitleSingleColumn",
                                  "widgetName": "CategoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "dataList": {
                                      "category":
                                          "45093371-1cdc-4c82-a652-0575aa3ce68f",
                                      "product": [
                                        "71ef4a61-975e-4615-832d-085ec13f2622",
                                        "d0ad3719-7613-4e2c-915a-4dfcbc4dd84e",
                                        "89dbe1a3-7e66-4a1e-97d3-17cb8c35a6b8",
                                        "64e639d1-aaf3-47ff-a01a-d820c09c4ff7",
                                        "33419811-4719-4a7b-a229-6962459a9550"
                                      ]
                                    },
                                    "useBackgroundCategoryTitle": "true",
                                    "useThumbnailProduct": "false",
                                    "toUpperCaseNameProductName": "false",
                                    "useMediumStyle": "false",
                                    "useFullWidthCategoryTitle": "true",
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff"
                                  }
                                },
                                {
                                  "id": "c244ab92-e2a1-460b-bb76-ae03eebe660a",
                                  "headerName": "PaddingByRatio",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "child": {},
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                {
                                  "id": "1028eb22-6a7b-4c87-8b52-ab2d0f29eb48",
                                  "headerName": "Expanded",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "flex": "1",
                                    "child": {
                                      "id":
                                          "97f09a71-4413-436b-ad27-4a7b67b26f5a",
                                      "headerName": "Row",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "textRightToLeft": "false",
                                        "children": [
                                          {
                                            "id":
                                                "11aa8b2c-9f01-4df5-8d59-73b8be38d40b",
                                            "headerName": "Expanded",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "1",
                                              "child": {
                                                "id":
                                                    "01c72594-241a-4192-ad87-917cd6ae9b87",
                                                "headerName": "SimpleImage",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "fit": "contain",
                                                  "imageDataKey":
                                                      "94004bc0-c777-4c38-88f3-7654874475dc"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "24fdb05a-7511-467e-bee0-fe763c19cb14",
                                            "headerName": "SizedBox",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "child": {},
                                              "width": "2.w"
                                            }
                                          },
                                          {
                                            "id":
                                                "6f66f328-905d-4178-b02e-aaf99376bd10",
                                            "headerName": "Expanded",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "1",
                                              "child": {
                                                "id":
                                                    "5f7265bd-37c1-4e94-a0a5-ee9a17ec1f0c",
                                                "headerName": "SimpleImage",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "fit": "contain",
                                                  "imageDataKey":
                                                      "b5e4c87a-3467-471b-8a8f-21e8e481979f"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "1eddf062-3ab0-40f8-863e-80c74f1428ec",
                                            "headerName": "SizedBox",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "child": {},
                                              "width": "2.w"
                                            }
                                          },
                                          {
                                            "id":
                                                "b9783e18-907e-4494-b9e7-1ed33ddc76e4",
                                            "headerName": "Expanded",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "1",
                                              "child": {
                                                "id":
                                                    "06125113-1026-4d53-865e-1689abf1c7b2",
                                                "headerName": "SimpleImage",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "fit": "contain",
                                                  "imageDataKey":
                                                      "749368eb-7166-4cd1-8122-123ed0e02ca8"
                                                }
                                              }
                                            }
                                          }
                                        ],
                                        "crossAxisAlignment": "start"
                                      }
                                    }
                                  }
                                }
                              ],
                              "mainAxisAlignment": "start"
                            }
                          },
                          "padding": {"all": "2.w"}
                        }
                      }
                    }
                  },
                  {
                    "id": "912d48a2-a11e-499a-b378-7b1c69873d35",
                    "headerName": "Expanded",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "id": "c37a4fad-0842-49d2-935e-876d2d0682f4",
                        "headerName": "Padding",
                        "widgetName": "Padding",
                        "widgetSetting": {
                          "child": {
                            "id": "33623844-53c6-47fb-af28-c698b2f97185",
                            "headerName": "Column",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "children": [
                                {
                                  "id": "ef2da498-e80a-421e-9759-c7557aa060b3",
                                  "headerName": "CategoryTitleSingleColumn",
                                  "widgetName": "CategoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "useBackgroundCategoryTitle": "true",
                                    "useThumbnailProduct": "false",
                                    "toUpperCaseNameProductName": "false",
                                    "useMediumStyle": "false",
                                    "useFullWidthCategoryTitle": "true",
                                    "dataList": {
                                      "category":
                                          "7a32cdae-7dee-4565-9f6b-487f13c431c9",
                                      "product": [
                                        "24f7518f-1106-44e9-9a5b-b371370a5506",
                                        "128c5e6d-f3de-4159-8bbe-d4db369719f5",
                                        "f9944cb3-7ce0-4c41-9a9d-241573360def",
                                        "60b554be-8504-43ca-8ab0-8bd23e942333",
                                        "60b554be-8504-43ca-8ab0-8bd23e942666"
                                      ]
                                    },
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff"
                                  }
                                },
                                {
                                  "id": "912eb0e2-f769-4cfc-921c-cb64cddd8b43",
                                  "headerName": "PaddingByRatio",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "child": {},
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                {
                                  "id": "ecdbab82-9cae-4982-b5b8-0b7da0b54969",
                                  "headerName": "Expanded",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "flex": "1",
                                    "child": {
                                      "id":
                                          "33dc80c9-a97f-4cb9-b379-82f53f8d464d",
                                      "headerName": "Row",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "textRightToLeft": "false",
                                        "children": [
                                          {
                                            "id":
                                                "f83347c3-3774-414d-a567-8d671f04f33b",
                                            "headerName": "Expanded",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "1",
                                              "child": {
                                                "id":
                                                    "e930dbd8-2d7a-46b6-8c06-d142e989ac39",
                                                "headerName": "SimpleImage",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "fit": "contain",
                                                  "imageDataKey":
                                                      "3f353d7a-766b-4dff-b225-f3501011c17f"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "c224a12a-a958-44fe-9b7f-ac539b7d2ed5",
                                            "headerName": "SizedBox",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "child": {},
                                              "width": "2.w"
                                            }
                                          },
                                          {
                                            "id":
                                                "1663d511-1f18-49fc-ad46-f43157e20bb5",
                                            "headerName": "Expanded",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "1",
                                              "child": {
                                                "id":
                                                    "ea8e2eb7-3646-41bb-b83d-c747c7e7ce12",
                                                "headerName": "SimpleImage",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "fit": "contain",
                                                  "imageDataKey":
                                                      "2b98c8e4-b753-45a3-b406-316f0395c6c7"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "dc657e75-2503-45a4-b8b4-25788ec76869",
                                            "headerName": "SizedBox",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "child": {},
                                              "width": "2.w"
                                            }
                                          },
                                          {
                                            "id":
                                                "d2c94b5f-7a47-47f8-b191-df0f864698b7",
                                            "headerName": "Expanded",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "1",
                                              "child": {
                                                "id":
                                                    "eb3ec0e6-6207-4a3e-a01c-38c747941ece",
                                                "headerName": "SimpleImage",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "fit": "contain",
                                                  "imageDataKey":
                                                      "8a4efda5-f1b7-41b1-ac0e-bb207e814f77"
                                                }
                                              }
                                            }
                                          }
                                        ],
                                        "crossAxisAlignment": "start"
                                      }
                                    }
                                  }
                                }
                              ],
                              "mainAxisAlignment": "start"
                            }
                          },
                          "padding": {"all": "2.w"}
                        }
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      }
    }
  };

  static final template4 = {
    "_id": "64ee744398c3d4605b477abf",
    "templateSlug": "temp_restaurant_4",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '1.w',
          'nameProductStyleMedium': '0.6.w',
          'priceProductStyle': '1.w',
          'priceProductStyleMedium': '0.6.w',
          'discriptionProductStyle': '0.6.w',
          'discriptionProductStyleMedium': '0.5.w',
          'imageHeight': '40.h',
          'marginBottomImage': '0.7.w',
          'marginBottomProduct': '1.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '21/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.3.w',
          'nameProductStyleMedium': '0.7.w',
          'priceProductStyle': '1.3.w',
          'priceProductStyleMedium': '0.7.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '1.5.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '16/9': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '16/10': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '4/3': {
          'categoryNameStyle': '3.5.w',
          'nameProductStyle': '2.w',
          'nameProductStyleMedium': '1.3.w',
          'priceProductStyle': '2.w',
          'priceProductStyleMedium': '1.3.w',
          'discriptionProductStyle': '1.3.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.3.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        'default': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
      },
    },
    "dataSetting": {
      "9ec1f49f-3e8a-40fe-b06b-4455d4d7e838": {
        "id": "9ec1f49f-3e8a-40fe-b06b-4455d4d7e838",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/BG-2-1.png"
      },
      "c251f687-24f3-4c01-af21-e3f52125553c": {
        "id": "c251f687-24f3-4c01-af21-e3f52125553c",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description": ""
      },
      "169abe4c-3650-4683-912c-5e1f1f2e8205": {
        "id": "169abe4c-3650-4683-912c-5e1f1f2e8205",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/BG-title-2.png"
      },
      "149217d0-96f0-414e-9154-bb84d4f8f27d": {
        "id": "149217d0-96f0-414e-9154-bb84d4f8f27d",
        "type": "product",
        "name": "product 1",
        "price": "12",
        "salePrice": "",
        "description": "this is a description 1"
      },
      "e118bd63-44db-42dd-ad4f-1a735118067e": {
        "id": "e118bd63-44db-42dd-ad4f-1a735118067e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "33cc8041-49db-46a0-be40-05f1b8487be5": {
        "id": "33cc8041-49db-46a0-be40-05f1b8487be5",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "5dd8470a-71cf-4467-a7de-efdfe5b84b24": {
        "id": "5dd8470a-71cf-4467-a7de-efdfe5b84b24",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "5eb0badc-5279-4851-9452-aeb820969174": {
        "id": "5eb0badc-5279-4851-9452-aeb820969174",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "aba82142-f045-4367-9615-60260169e9fe": {
        "id": "aba82142-f045-4367-9615-60260169e9fe",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "6da3eaa4-dfde-466d-9f64-10c32af49880": {
        "id": "6da3eaa4-dfde-466d-9f64-10c32af49880",
        "type": "category",
        "name": "Category 2",
        "image": "",
        "description": ""
      },
      "a4fc9694-0ad9-4983-ae49-ada23bbb2bfd": {
        "id": "a4fc9694-0ad9-4983-ae49-ada23bbb2bfd",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/BG-title-2.png"
      },
      "f3e39143-2abe-4d7a-82cc-c6df3678db73": {
        "id": "f3e39143-2abe-4d7a-82cc-c6df3678db73",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg1-1.jpg"
      },
      "c01645f2-0158-4b78-b728-6032332bceed": {
        "id": "c01645f2-0158-4b78-b728-6032332bceed",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg2-1.jpg"
      },
      "f404cec1-81f1-45b0-b2c7-3e5436f1f78e": {
        "id": "f404cec1-81f1-45b0-b2c7-3e5436f1f78e",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg3-1.jpg"
      },
      "91fa118f-52fb-4bf0-920c-c796235a6577": {
        "id": "91fa118f-52fb-4bf0-920c-c796235a6577",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "2fb25cad-3c3f-426c-9d82-ca9849c55ce0": {
        "id": "2fb25cad-3c3f-426c-9d82-ca9849c55ce0",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "266cd7bc-6545-46d9-b5d5-4eb290ae0392": {
        "id": "266cd7bc-6545-46d9-b5d5-4eb290ae0392",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "92b528cb-264f-4571-9e14-17993cba4b5f": {
        "id": "92b528cb-264f-4571-9e14-17993cba4b5f",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg4-2-scaled.jpg"
      },
      "081deac2-2128-4d20-86fd-b5b0cfad6a39": {
        "id": "081deac2-2128-4d20-86fd-b5b0cfad6a39",
        "type": "category",
        "name": "Category 3",
        "image": "",
        "description": ""
      },
      "b769377b-e8f2-477a-bde5-28bf2c0de633": {
        "id": "b769377b-e8f2-477a-bde5-28bf2c0de633",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/BG-title-2.png"
      },
      "c08d226b-5f65-4932-a350-c060b896f5dc": {
        "id": "c08d226b-5f65-4932-a350-c060b896f5dc",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "2e6ecb04-a95d-4441-a523-3892b4ea310d": {
        "id": "2e6ecb04-a95d-4441-a523-3892b4ea310d",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "87bf3d76-08f5-452b-9c9d-9d19d535f59e": {
        "id": "87bf3d76-08f5-452b-9c9d-9d19d535f59e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "46169dda-dfd6-4866-8966-adb520dd4a64": {
        "id": "46169dda-dfd6-4866-8966-adb520dd4a64",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg5-1-scaled.jpg"
      },
      "a1d8b6be-242a-40a1-bf80-dfff0b5f524b": {
        "id": "a1d8b6be-242a-40a1-bf80-dfff0b5f524b",
        "type": "category",
        "name": "Category 4",
        "image": "",
        "description": ""
      },
      "eb99e70a-2d51-431b-97c9-d70a4e8e1a3c": {
        "id": "eb99e70a-2d51-431b-97c9-d70a4e8e1a3c",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/BG-title-2.png"
      },
      "70c223a0-8d92-472d-a6f2-6e4f1404c852": {
        "id": "70c223a0-8d92-472d-a6f2-6e4f1404c852",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "77dcf92a-ffce-41ff-a9a4-0feb1818b254": {
        "id": "77dcf92a-ffce-41ff-a9a4-0feb1818b254",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "31c3a0f2-bd85-40fd-887c-524682244ae0": {
        "id": "31c3a0f2-bd85-40fd-887c-524682244ae0",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "a1c67448-add9-4269-93c2-a8035350063a": {
        "id": "a1c67448-add9-4269-93c2-a8035350063a",
        "type": "category",
        "name": "Category 5",
        "image": "",
        "description": ""
      },
      "b75f4c81-1be6-4266-9821-b44098795ac5": {
        "id": "b75f4c81-1be6-4266-9821-b44098795ac5",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/BG-title-2.png"
      },
      "edbc5d59-b144-4b4c-9ab5-ea37ad935e50": {
        "id": "edbc5d59-b144-4b4c-9ab5-ea37ad935e50",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "97a7b3e8-e672-434a-943e-44296ec84fc5": {
        "id": "97a7b3e8-e672-434a-943e-44296ec84fc5",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "c4a5201c-d4ef-4c03-a864-3232670b9d2e": {
        "id": "c4a5201c-d4ef-4c03-a864-3232670b9d2e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      }
    },
    "widgets_setting": {
      "id": "25bb0234-d9ab-4b40-8ac7-b9d1c23c3c1d",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image": "9ec1f49f-3e8a-40fe-b06b-4455d4d7e838",
          "boxFit": "fill"
        },
        "child": {
          "id": "ab292e98-668c-4030-88b0-160f9b453857",
          "headerName": "Row",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                "id": "a15100a6-bf9c-495d-8eee-8a196b64967e",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "dea77f64-1b8d-44ac-9a2e-82ea9a55af72",
                    "headerName": "Padding",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "child": {
                        "id": "b30b1c10-b4bc-473b-9f93-cc33f3eff171",
                        "headerName": "Column",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "4af2e1ae-c19c-41f3-8cf0-18bff350941f",
                              "headerName": "CategoryTitleGridLayout3",
                              "widgetName":
                                  "CategoryTitleGridLayout3", //CategoryTitleGridLayout3
                              "widgetSetting": {
                                "useBackgroundCategoryTitle": "true",
                                "useFullWidthCategoryTitle": "false",
                                "dataList": {
                                  "category":
                                      "c251f687-24f3-4c01-af21-e3f52125553c",
                                  "product": [
                                    "149217d0-96f0-414e-9154-bb84d4f8f27d",
                                    "e118bd63-44db-42dd-ad4f-1a735118067e",
                                    "33cc8041-49db-46a0-be40-05f1b8487be5",
                                    "5dd8470a-71cf-4467-a7de-efdfe5b84b24",
                                    "5eb0badc-5279-4851-9452-aeb820969174",
                                    "aba82142-f045-4367-9615-60260169e9fe"
                                  ]
                                },
                                "imageId":
                                    "169abe4c-3650-4683-912c-5e1f1f2e8205",
                                "colorCategoryTitle": "0xFF121212",
                                "colorPrice": "0xFF8d1111",
                                "colorProductName": "0xFF121212",
                                "colorDescription": "0xFF121212",
                                "colorBackgroundCategoryTitle": "0x00121212"
                              }
                            },
                            {
                              "id": "c2c20821-f231-4676-9775-a442a22db0fe",
                              "headerName": "PaddingByRatio",
                              "widgetName": "PaddingByRatio",
                              "widgetSetting": {
                                "child": {},
                                "bottomPaddingFieldKey":
                                    "marginBottomCategoryTitle"
                              }
                            },
                            {
                              "id": "2c8aa657-bcc3-4a56-9843-c9d7e85f0310",
                              "headerName": "CategoryTitleGridLayout4",
                              "widgetName":
                                  "CategoryTitleGridLayout4", // CategoryTitleGridLayout4
                              "widgetSetting": {
                                "useBackgroundCategoryTitle": "true",
                                "useFullWidthCategoryTitle": "false",
                                "dataList": {
                                  "category":
                                      "6da3eaa4-dfde-466d-9f64-10c32af49880",
                                  "product": [
                                    "91fa118f-52fb-4bf0-920c-c796235a6577",
                                    "2fb25cad-3c3f-426c-9d82-ca9849c55ce0",
                                    "266cd7bc-6545-46d9-b5d5-4eb290ae0392"
                                  ],
                                  "image": [
                                    "f3e39143-2abe-4d7a-82cc-c6df3678db73",
                                    "c01645f2-0158-4b78-b728-6032332bceed",
                                    "f404cec1-81f1-45b0-b2c7-3e5436f1f78e"
                                  ]
                                },
                                "imageId":
                                    "a4fc9694-0ad9-4983-ae49-ada23bbb2bfd",
                                "colorCategoryTitle": "0xFF121212",
                                "colorPrice": "0xFF8d1111",
                                "colorProductName": "0xFF121212",
                                "colorDescription": "0xFF121212",
                                "colorBackgroundCategoryTitle": "0x00212121"
                              }
                            }
                          ],
                          "mainAxisAlignment": "start"
                        }
                      },
                      "padding": {"all": "2.w"}
                    }
                  }
                }
              },
              {
                "id": "947770c6-2ea5-4a9d-966f-04e42656bc18",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "fb529f1f-b49d-46ac-9c01-6c19d6b3c861",
                    "headerName": "Padding",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "child": {
                        "id": "66e4f373-9acb-48a5-9b0a-2ac7c8e0d340",
                        "headerName": "Row",
                        "widgetName": "Row",
                        "widgetSetting": {
                          "textRightToLeft": "false",
                          "children": [
                            {
                              "id": "ccc2ae47-c4dd-4b3a-b476-5fb54a145514",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "f0220aa0-89d9-440a-9fcd-998aeed57495",
                                  "headerName": "Column",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "3e1508d7-9e14-4702-b072-d6682c2f5d1e",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "23fad144-cc5f-43da-a3ce-6887d09f8e4e",
                                            "headerName": "Row",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "textRightToLeft": "false",
                                              "children": [
                                                {
                                                  "id":
                                                      "5900ccd2-9890-45b9-a550-f70af4135a30",
                                                  "headerName": "Expanded",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "flex": "1",
                                                    "child": {
                                                      "id":
                                                          "eb7a8b1f-0eda-4178-a910-1872cbbd9d3e",
                                                      "headerName": "Image",
                                                      "widgetName": "Image",
                                                      "widgetSetting": {
                                                        "fixCover": "false",
                                                        "imageId":
                                                            "92b528cb-264f-4571-9e14-17993cba4b5f"
                                                      }
                                                    }
                                                  }
                                                }
                                              ],
                                              "crossAxisAlignment": "start"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "c762371c-22a5-46ce-8117-4e754b4e46f1",
                                        "headerName": "PaddingByRatio",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "child": {},
                                          "bottomPaddingFieldKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },
                                      {
                                        "id":
                                            "ff9f60ef-e2a2-427f-bdec-35454fc92e75",
                                        "headerName":
                                            "CategoryTitleSingleColumn",
                                        "widgetName":
                                            "CategoryTitleSingleColumn",
                                        "widgetSetting": {
                                          "useBackgroundCategoryTitle": "true",
                                          "useThumbnailProduct": "false",
                                          "toUpperCaseNameProductName": "false",
                                          "useMediumStyle": "false",
                                          "useFullWidthCategoryTitle": "false",
                                          "dataList": {
                                            "category":
                                                "081deac2-2128-4d20-86fd-b5b0cfad6a39",
                                            "product": [
                                              "c08d226b-5f65-4932-a350-c060b896f5dc",
                                              "2e6ecb04-a95d-4441-a523-3892b4ea310d",
                                              "87bf3d76-08f5-452b-9c9d-9d19d535f59e"
                                            ]
                                          },
                                          "imageId":
                                              "b769377b-e8f2-477a-bde5-28bf2c0de633",
                                          "colorCategoryTitle": "0xFF121212",
                                          "colorPrice": "0xFF8d1111",
                                          "colorProductName": "0xFF121212",
                                          "colorDescription": "0xFF121212"
                                        }
                                      },
                                      {
                                        "id":
                                            "8b0b5434-3000-4e27-970d-2f0fd55e98e8",
                                        "headerName": "PaddingByRatio",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "child": {},
                                          "bottomPaddingFieldKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },
                                      {
                                        "id":
                                            "d9434fa2-5031-4edc-8755-d87c8c60d23c",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "ccb6668b-5f80-42fa-843d-0fa706796448",
                                            "headerName": "Row",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "textRightToLeft": "false",
                                              "children": [
                                                {
                                                  "id":
                                                      "661cee24-4fc0-4cc4-8f48-d0c486bec7bb",
                                                  "headerName": "Expanded",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "flex": "1",
                                                    "child": {
                                                      "id":
                                                          "a0ef35ce-cc0d-433c-9d86-14ef93cb6251",
                                                      "headerName": "Image",
                                                      "widgetName": "Image",
                                                      "widgetSetting": {
                                                        "fixCover": "false",
                                                        "imageId":
                                                            "46169dda-dfd6-4866-8966-adb520dd4a64"
                                                      }
                                                    }
                                                  }
                                                }
                                              ],
                                              "crossAxisAlignment": "start"
                                            }
                                          }
                                        }
                                      }
                                    ],
                                    "mainAxisAlignment": "start"
                                  }
                                }
                              }
                            },
                            {
                              "id": "3ada7b51-a670-4453-8088-42100d84b4b4",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"width": "3.w"}
                            },
                            {
                              "id": "b7ba8042-ca94-458a-8979-739f4065fea7",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "fd31fdbf-2c20-4d3b-b4de-ecd0016a3e70",
                                  "headerName": "Column",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "e99d650a-a6a7-4454-8fe9-6f32813f9d1f",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "bc8017ee-e680-4ad9-8db1-79064ebdeb5d",
                                            "headerName":
                                                "CategoryTitleSingleColumn",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "useThumbnailProduct": "false",
                                              "toUpperCaseNameProductName":
                                                  "false",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false",
                                              "dataList": {
                                                "category":
                                                    "a1d8b6be-242a-40a1-bf80-dfff0b5f524b",
                                                "product": [
                                                  "70c223a0-8d92-472d-a6f2-6e4f1404c852",
                                                  "77dcf92a-ffce-41ff-a9a4-0feb1818b254",
                                                  "31c3a0f2-bd85-40fd-887c-524682244ae0"
                                                ]
                                              },
                                              "imageId":
                                                  "eb99e70a-2d51-431b-97c9-d70a4e8e1a3c",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorPrice": "0xFF8d1111",
                                              "colorProductName": "0xFF121212",
                                              "colorDescription": "0xFF121212"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "a4d39f1e-0c79-4248-a7be-903117ad2b0d",
                                        "headerName": "PaddingByRatio",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "child": {},
                                          "bottomPaddingFieldKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },
                                      {
                                        "id":
                                            "a7740fe8-e49b-4adc-8454-3acd3108348a",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "443c269f-5a7d-48f4-9339-cafa77cef00b",
                                            "headerName":
                                                "CategoryTitleSingleColumn",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "useThumbnailProduct": "false",
                                              "toUpperCaseNameProductName":
                                                  "false",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false",
                                              "dataList": {
                                                "category":
                                                    "a1c67448-add9-4269-93c2-a8035350063a",
                                                "product": [
                                                  "edbc5d59-b144-4b4c-9ab5-ea37ad935e50",
                                                  "97a7b3e8-e672-434a-943e-44296ec84fc5",
                                                  "c4a5201c-d4ef-4c03-a864-3232670b9d2e"
                                                ]
                                              },
                                              "imageId":
                                                  "b75f4c81-1be6-4266-9821-b44098795ac5",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorPrice": "0xFF8d1111",
                                              "colorProductName": "0xFF121212",
                                              "colorDescription": "0xFF121212"
                                            }
                                          }
                                        }
                                      }
                                    ],
                                    "mainAxisAlignment": "start"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      },
                      "padding": {"all": "2.w"}
                    }
                  }
                }
              }
            ]
          }
        },
        "height": "100.h",
        "width": "100.w"
      }
    }
  };

  static final template5 = {
    "_id": "64ee744398c3d4605b477abg",
    "templateSlug": "temp_restaurant_5",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.w',
          'nameProductStyle': '0.8.w',
          'nameProductStyleMedium': '0.4.w',
          'priceProductStyle': '0.8.w',
          'priceProductStyleMedium': '0.4.w',
          'discriptionProductStyle': '0.4.w',
          'discriptionProductStyleMedium': '0.3.w',
          'imageHeight': '50.h',
          'marginBottomImage': '0.4.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.4.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '21/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.w',
          'nameProductStyleMedium': '0.5.w',
          'priceProductStyle': '1.w',
          'priceProductStyleMedium': '0.5.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '50.h',
          'marginBottomImage': '0.5.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.5.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '16/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '16/10': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.7.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '4/3': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '2.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '2.w',
          'priceProductStyleMedium': '1.3.w',
          'discriptionProductStyle': '1.3.w',
          'discriptionProductStyleMedium': '0.7.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
      },
    },
    "dataSetting": {
      "44c19ede-337b-4aa4-8598-e77e2b47c6ba": {
        "id": "44c19ede-337b-4aa4-8598-e77e2b47c6ba",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-1-1.png"
      },
      "1c62822f-3dc4-4205-a7a8-b9366bf80989": {
        "id": "1c62822f-3dc4-4205-a7a8-b9366bf80989",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description": ""
      },
      "ce2ae34a-d85f-4491-a096-fbb4c1085833": {
        "id": "ce2ae34a-d85f-4491-a096-fbb4c1085833",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-title.png"
      },
      "082b4cff-4401-4419-a78a-c072bea0088d": {
        "id": "082b4cff-4401-4419-a78a-c072bea0088d",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "1d7bc174-28a0-4873-878f-b2a6ce6bf83d": {
        "id": "1d7bc174-28a0-4873-878f-b2a6ce6bf83d",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "83f6868d-7e3c-4bb4-ae52-eb824b97a954": {
        "id": "83f6868d-7e3c-4bb4-ae52-eb824b97a954",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "67e4fa42-1ac9-4bc3-bc48-546b42f85a5d": {
        "id": "67e4fa42-1ac9-4bc3-bc48-546b42f85a5d",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/hamburger1.png"
      },
      "e4c98bb0-8380-4cb0-8b59-d96e8f447866": {
        "id": "e4c98bb0-8380-4cb0-8b59-d96e8f447866",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-title.png"
      },
      "dae84985-a570-418b-8e8c-43452e601a1a": {
        "id": "dae84985-a570-418b-8e8c-43452e601a1a",
        "type": "category",
        "name": "Category 2",
        "image": "",
        "description": ""
      },
      "185d290f-6021-4584-9e43-ac624e55a0c7": {
        "id": "185d290f-6021-4584-9e43-ac624e55a0c7",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "66bfd554-a344-4ef6-ad01-a672db9ff28e": {
        "id": "66bfd554-a344-4ef6-ad01-a672db9ff28e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "71b7245b-f320-445f-bd7e-c183316618b6": {
        "id": "71b7245b-f320-445f-bd7e-c183316618b6",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "04a04dbe-d8ea-4e03-b89b-f1afbd7f3c47": {
        "id": "04a04dbe-d8ea-4e03-b89b-f1afbd7f3c47",
        "type": "category",
        "name": "Category 2",
        "image": "",
        "description": ""
      },
      "21c561e7-3176-452d-aad7-322e0607a0ab": {
        "id": "21c561e7-3176-452d-aad7-322e0607a0ab",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-title.png"
      },
      "72807582-1f06-442b-9198-d9caa4982bc9": {
        "id": "72807582-1f06-442b-9198-d9caa4982bc9",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "f89f4782-db87-4c9c-aed2-7cc43d34f6ab": {
        "id": "f89f4782-db87-4c9c-aed2-7cc43d34f6ab",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "9d322829-82e3-4684-808d-a085787c5c53": {
        "id": "9d322829-82e3-4684-808d-a085787c5c53",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "6ef63d2b-3dab-410d-9553-ae827e79a0f1": {
        "id": "6ef63d2b-3dab-410d-9553-ae827e79a0f1",
        "type": "category",
        "name": "Category 3",
        "image": "",
        "description": ""
      },
      "275feb92-f2c3-48ad-b58d-4d81a1937abc": {
        "id": "275feb92-f2c3-48ad-b58d-4d81a1937abc",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-title.png"
      },
      "6b1d095a-ed8b-4249-b44c-0ed035550a77": {
        "id": "6b1d095a-ed8b-4249-b44c-0ed035550a77",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "23f9f415-7472-4f17-9733-05956d37f3c6": {
        "id": "23f9f415-7472-4f17-9733-05956d37f3c6",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "6d476c52-651b-4e99-ba4d-3ff745e8869a": {
        "id": "6d476c52-651b-4e99-ba4d-3ff745e8869a",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "19ce7b47-1ac7-4849-8000-a2f2e0a70c02": {
        "id": "19ce7b47-1ac7-4849-8000-a2f2e0a70c02",
        "type": "category",
        "name": "Category 4",
        "image": "",
        "description": ""
      },
      "64c43379-02ce-4786-9cea-4b47aee234b0": {
        "id": "64c43379-02ce-4786-9cea-4b47aee234b0",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-title.png"
      },
      "6c831f06-f789-4a99-b113-2db52aa0f196": {
        "id": "6c831f06-f789-4a99-b113-2db52aa0f196",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "5c93f5b9-864c-43a8-9e85-da32a0b32eb4": {
        "id": "5c93f5b9-864c-43a8-9e85-da32a0b32eb4",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "53c276a5-af7b-413b-b3c9-34fade7973ee": {
        "id": "53c276a5-af7b-413b-b3c9-34fade7973ee",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "3ba9e32d-11f0-44a6-9348-f650b381d02c": {
        "id": "3ba9e32d-11f0-44a6-9348-f650b381d02c",
        "type": "category",
        "name": "Category 5",
        "image": "",
        "description": ""
      },
      "6e8221b3-b2db-40f6-8a4c-6e62bd1eda73": {
        "id": "6e8221b3-b2db-40f6-8a4c-6e62bd1eda73",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-title.png"
      },
      "cea43873-952b-4b87-899a-c531d39c961e": {
        "id": "cea43873-952b-4b87-899a-c531d39c961e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "0317c8d6-f2ff-4e30-8fc4-4f37ea1ddd99": {
        "id": "0317c8d6-f2ff-4e30-8fc4-4f37ea1ddd99",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "f9ff6e87-d00f-432e-9ecd-a2c76872f503": {
        "id": "f9ff6e87-d00f-432e-9ecd-a2c76872f503",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "9839d56f-297b-40e9-bb3f-2b4c6ef7d636": {
        "id": "9839d56f-297b-40e9-bb3f-2b4c6ef7d636",
        "type": "category",
        "name": "Category 6",
        "image": "",
        "description": ""
      },
      "780a30c2-1139-404f-ab08-73ae769f9fc0": {
        "id": "780a30c2-1139-404f-ab08-73ae769f9fc0",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-title.png"
      },
      "e09f847c-2d2c-463e-a932-d4c0949433ee": {
        "id": "e09f847c-2d2c-463e-a932-d4c0949433ee",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "78323379-0360-4654-8a8e-cd92a975731d": {
        "id": "78323379-0360-4654-8a8e-cd92a975731d",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "768e587a-8a30-4436-b0cf-357b9df5d700": {
        "id": "768e587a-8a30-4436-b0cf-357b9df5d700",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "ae6052d4-990f-40ec-940b-e56e5063a930": {
        "id": "ae6052d4-990f-40ec-940b-e56e5063a930",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/hamburger2.png"
      },
      "747ae1ef-d5b8-460d-b149-748e1413a0fb": {
        "id": "747ae1ef-d5b8-460d-b149-748e1413a0fb",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/hamburger2.png"
      }
    },
    "widgets_setting": {
      "id": "e0fb595c-b772-425c-b325-cc2ff6a14d8d",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image": "44c19ede-337b-4aa4-8598-e77e2b47c6ba",
          "boxFit": "fill"
        },
        "child": {
          "id": "51701867-905e-4ee4-9f80-86a0e1e33732",
          "headerName": "Row",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                "id": "b218df6c-1981-466f-8815-0c6f81bac505",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "f0f9b186-a34a-4d1b-a8d2-977ce1921b41",
                    "headerName": "Padding",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "child": {
                        "id": "4eeb1462-72d2-49d5-af47-d6b0b2c5f817",
                        "headerName": "Column",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "4ab5b379-fa17-4983-8ba5-eb83c5697516",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "06ead92d-8b83-46f3-9a37-9d3615f8610e",
                                  "headerName": "Column",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "fe649699-1c13-4b0d-97ac-25d96e4627db",
                                        "headerName": "CategoryTitle",
                                        "widgetName": "CategoryTitle",
                                        "widgetSetting": {
                                          "useBackground": "true",
                                          "useFullWidth": "false",
                                          "categoryId":
                                              "1c62822f-3dc4-4205-a7a8-b9366bf80989",
                                          "imageId":
                                              "ce2ae34a-d85f-4491-a096-fbb4c1085833",
                                          "color": "0xFF121212",
                                          "colorBackground": "0x00121212"
                                        }
                                      },
                                      {
                                        "id":
                                            "0e1a73bb-b75c-4919-8b99-e4062ca607a2",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "93171f62-cb39-4324-99f9-dea0c49f69e0",
                                            "headerName": "Row",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "textRightToLeft": "false",
                                              "children": [
                                                {
                                                  "id":
                                                      "709106c8-8d5d-4895-9765-9fa920ec73fc",
                                                  "headerName": "Expanded",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "flex": "1",
                                                    "child": {
                                                      "id":
                                                          "203ac1e0-913e-4447-a09b-34d5cc780bf5",
                                                      "headerName":
                                                          "ProductList",
                                                      "widgetName":
                                                          "ProductList",
                                                      "widgetSetting": {
                                                        "toUpperCaseNameProductName":
                                                            "true",
                                                        "useMediumStyle":
                                                            "false",
                                                        "useThumbnailProduct":
                                                            "false",
                                                        "maxLineProductName":
                                                            "1",
                                                        "maxLineProductDescription":
                                                            "2",
                                                        "dataList": [
                                                          "082b4cff-4401-4419-a78a-c072bea0088d",
                                                          "1d7bc174-28a0-4873-878f-b2a6ce6bf83d",
                                                          "83f6868d-7e3c-4bb4-ae52-eb824b97a954"
                                                        ],
                                                        "mainAxisAlignment":
                                                            "start",
                                                        "crossAxisAlignment":
                                                            "center",
                                                        "colorPrice":
                                                            "0xFFf4b91a",
                                                        "colorProductName":
                                                            "0xFFFFFFFF",
                                                        "colorDescription":
                                                            "0xFFFFFFFF"
                                                      }
                                                    }
                                                  }
                                                },
                                                {
                                                  "id":
                                                      "e47a6075-548a-4e11-ae21-3c8a9b7eda1d",
                                                  "headerName": "Container",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "decoration": {
                                                      "imageUrl":
                                                          "https://foodiemenu.co/wp-content/uploads/2023/09/Seperator-Line-1.png",
                                                      "boxFit": "contain"
                                                    },
                                                    "child": {},
                                                    "width": "5.w",
                                                    "padding": {
                                                      "vertical": "2.w"
                                                    }
                                                  }
                                                },
                                                {
                                                  "id":
                                                      "00ea4300-62c6-4623-b50d-4e94014f18e3",
                                                  "headerName": "Expanded",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "flex": "1",
                                                    "child": {
                                                      "id":
                                                          "520021fc-92ea-4a43-ae99-9274af772783",
                                                      "headerName": "Padding",
                                                      "widgetName": "Padding",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "id":
                                                              "414cb119-49c3-42ac-9c91-1e0971bdbc5d",
                                                          "headerName": "Image",
                                                          "widgetName": "Image",
                                                          "widgetSetting": {
                                                            "fixCover": "false",
                                                            "imageId":
                                                                "67e4fa42-1ac9-4bc3-bc48-546b42f85a5d"
                                                          }
                                                        },
                                                        "padding": {
                                                          "all": "1.w"
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              ],
                                              "crossAxisAlignment": "stretch"
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },
                            {
                              "id": "14954fac-f2ab-4a6e-8ed2-0199d0b18ebb",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "46a6cc05-95f1-4973-a0ba-f316bc92494c",
                                  "headerName": "Row",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "children": [
                                      {
                                        "id":
                                            "c2d082d8-c55e-493b-9fef-e2d995b4ce3d",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "10a488f7-2d74-4eac-b092-494c9a51bb2c",
                                            "headerName":
                                                "CategoryTitleSingleColumn",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "useThumbnailProduct": "false",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false",
                                              "dataList": {
                                                "category":
                                                    "04a04dbe-d8ea-4e03-b89b-f1afbd7f3c47",
                                                "product": [
                                                  "72807582-1f06-442b-9198-d9caa4982bc9",
                                                  "f89f4782-db87-4c9c-aed2-7cc43d34f6ab",
                                                  "9d322829-82e3-4684-808d-a085787c5c53"
                                                ]
                                              },
                                              "imageId":
                                                  "21c561e7-3176-452d-aad7-322e0607a0ab",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "16703e8d-db36-4c07-aec9-8c1b1f394a0c",
                                        "headerName": "Container",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "decoration": {
                                            "imageUrl":
                                                "https://foodiemenu.co/wp-content/uploads/2023/09/Seperator-Line-1.png",
                                            "boxFit": "contain"
                                          },
                                          "child": {},
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"}
                                        }
                                      },
                                      {
                                        "id":
                                            "8d8b4fa4-1bec-4223-b218-b4c85479e51a",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "1e8c9a02-329d-4dea-8aa4-7944e4295c24",
                                            "headerName":
                                                "CategoryTitleSingleColumn",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "useThumbnailProduct": "false",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false",
                                              "dataList": {
                                                "category":
                                                    "6ef63d2b-3dab-410d-9553-ae827e79a0f1",
                                                "product": [
                                                  "6b1d095a-ed8b-4249-b44c-0ed035550a77",
                                                  "23f9f415-7472-4f17-9733-05956d37f3c6",
                                                  "6d476c52-651b-4e99-ba4d-3ff745e8869a"
                                                ]
                                              },
                                              "imageId":
                                                  "275feb92-f2c3-48ad-b58d-4d81a1937abc",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF"
                                            }
                                          }
                                        }
                                      }
                                    ],
                                    "crossAxisAlignment": "stretch"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      },
                      "padding": {"all": "3.w"}
                    }
                  }
                }
              },
              {
                "id": "f590bca8-ab90-4b1e-8017-ade47c93a551",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "d6997aa5-2ac9-4183-9271-57805e682511",
                    "headerName": "Padding",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "child": {
                        "id": "abb6db0e-27bc-4078-8e14-d3b4cbf2207e",
                        "headerName": "Column",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "3785cf64-aae7-4913-8d2b-8dae7bfaf17b",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "cb7a3221-b63c-4c48-ac73-fdfa731d8d89",
                                  "headerName": "Row",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "children": [
                                      {
                                        "id":
                                            "05678130-4461-497e-956b-9ad82bd2289a",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "7b1c4b65-d252-477a-9a7c-0342103e820f",
                                            "headerName":
                                                "CategoryTitleSingleColumn",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "useThumbnailProduct": "false",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false",
                                              "dataList": {
                                                "category":
                                                    "19ce7b47-1ac7-4849-8000-a2f2e0a70c02",
                                                "product": [
                                                  "6c831f06-f789-4a99-b113-2db52aa0f196",
                                                  "5c93f5b9-864c-43a8-9e85-da32a0b32eb4",
                                                  "53c276a5-af7b-413b-b3c9-34fade7973ee"
                                                ]
                                              },
                                              "imageId":
                                                  "64c43379-02ce-4786-9cea-4b47aee234b0",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "d337d189-0746-4698-96d0-f3142b6a2df9",
                                        "headerName": "Container",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "decoration": {
                                            "imageUrl":
                                                "https://foodiemenu.co/wp-content/uploads/2023/09/Seperator-Line-1.png",
                                            "boxFit": "contain"
                                          },
                                          "child": {},
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"}
                                        }
                                      },
                                      {
                                        "id":
                                            "886e7f22-a84c-4b17-a270-b7d63566fd11",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "74cb9e64-8e33-495a-b5ff-3787e1fc3316",
                                            "headerName":
                                                "CategoryTitleSingleColumn",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "useThumbnailProduct": "false",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false",
                                              "dataList": {
                                                "category":
                                                    "3ba9e32d-11f0-44a6-9348-f650b381d02c",
                                                "product": [
                                                  "cea43873-952b-4b87-899a-c531d39c961e",
                                                  "0317c8d6-f2ff-4e30-8fc4-4f37ea1ddd99",
                                                  "f9ff6e87-d00f-432e-9ecd-a2c76872f503"
                                                ]
                                              },
                                              "imageId":
                                                  "6e8221b3-b2db-40f6-8a4c-6e62bd1eda73",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF"
                                            }
                                          }
                                        }
                                      }
                                    ],
                                    "crossAxisAlignment": "stretch"
                                  }
                                }
                              }
                            },
                            {
                              "id": "5022bd9f-6b10-43e8-ab08-6a94307746b7",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "f6ebee99-050f-46cd-9eb2-9e5382d3bd46",
                                  "headerName": "Row",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "children": [
                                      {
                                        "id":
                                            "ffb18a02-01cd-4753-87cc-6d9da6590d53",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "e17a5134-da55-4aeb-93c7-5e8d8582511c",
                                            "headerName":
                                                "CategoryTitleSingleColumn",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "useThumbnailProduct": "false",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false",
                                              "dataList": {
                                                "category":
                                                    "9839d56f-297b-40e9-bb3f-2b4c6ef7d636",
                                                "product": [
                                                  "e09f847c-2d2c-463e-a932-d4c0949433ee",
                                                  "78323379-0360-4654-8a8e-cd92a975731d",
                                                  "768e587a-8a30-4436-b0cf-357b9df5d700"
                                                ]
                                              },
                                              "imageId":
                                                  "780a30c2-1139-404f-ab08-73ae769f9fc0",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "caa72be3-34f3-456e-b404-c0fb8635d5a1",
                                        "headerName": "Expanded",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "5b6f18c3-622f-42c5-b159-2ebddb3e42a2",
                                            "headerName": "Padding",
                                            "widgetName": "Padding",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "dd151e27-a006-4f30-b9f0-03e8549a23a4",
                                                "headerName": "Image",
                                                "widgetName": "Image",
                                                "widgetSetting": {
                                                  "fixCover": "false",
                                                  "imageId":
                                                      "747ae1ef-d5b8-460d-b149-748e1413a0fb"
                                                }
                                              },
                                              "padding": {"all": "1.w"}
                                            }
                                          }
                                        }
                                      }
                                    ],
                                    "crossAxisAlignment": "stretch"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      },
                      "padding": {"all": "3.w"}
                    }
                  }
                }
              }
            ]
          }
        }
      }
    }
  };

  static final template6 = {
    "_id": "64ee744398c3d4605b477abh",
    "templateSlug": "temp_nail_1",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'normal',
          'fontFamily': 'Gravitas One',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Titillium Web',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Titillium Web',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Titillium Web',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'nameProductStyle': '1.w',
          'priceProductStyle': '1.w',
          'categoryNameStyle': '1.4.w',
          'discriptionProductStyle': '0.8.w',
        },
        '21/9': {
          'nameProductStyle': '1.2.w',
          'priceProductStyle': '1.2.w',
          'categoryNameStyle': '1.4.w',
          'discriptionProductStyle': '0.85.w',
        },
        '16/9': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '4.h',
          'discriptionProductStyle': '2.h',
        },
        '16/10': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '3.w',
          'discriptionProductStyle': '2.h',
        },
        '4/3': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '4.h',
          'discriptionProductStyle': '2.h',
        },
        'default': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '4.h',
          'discriptionProductStyle': '2.h',
        },
      },
    },
    "dataSetting": {
      "85b8b5cf-533d-4aa0-ba06-8deb6620054c": {
        "id": "85b8b5cf-533d-4aa0-ba06-8deb6620054c",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_img_landscape-scaled.jpg"
      },
      "d680fb22-7ee1-4f92-9d95-6b46288e9b51": {
        "id": "d680fb22-7ee1-4f92-9d95-6b46288e9b51",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/logo2-scaled.jpg"
      },
      "1149761c-f4bc-4db2-b431-1e8ef93e9268": {
        "id": "1149761c-f4bc-4db2-b431-1e8ef93e9268",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description": ""
      },
      "a263a7a1-44b0-4a54-8f2c-fa6eb1677532": {
        "id": "a263a7a1-44b0-4a54-8f2c-fa6eb1677532",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "fee998fa-2080-47e9-8357-94d2ac33eadc": {
        "id": "fee998fa-2080-47e9-8357-94d2ac33eadc",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "750bbe5c-3c1c-4488-88d7-263536dad8b7": {
        "id": "750bbe5c-3c1c-4488-88d7-263536dad8b7",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "86842abe-0048-45a6-b402-3fced2aa63d7": {
        "id": "86842abe-0048-45a6-b402-3fced2aa63d7",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "095565df-3644-4a09-8d6f-15a9e04eefb2": {
        "id": "095565df-3644-4a09-8d6f-15a9e04eefb2",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "ce14eabd-c1c4-4785-83ed-8ce372ce1018": {
        "id": "ce14eabd-c1c4-4785-83ed-8ce372ce1018",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "c1c510e5-4fc6-43b5-91a4-54754450bbec": {
        "id": "c1c510e5-4fc6-43b5-91a4-54754450bbec",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "dfd56dbd-0e5d-41d8-850a-f9912ce058a3": {
        "id": "dfd56dbd-0e5d-41d8-850a-f9912ce058a3",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      }
    },
    "widgets_setting": {
      "id": "eb532192-b26e-48f6-bace-bc455ad8127f",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"boxFit": "fill"},
        "child": {
          "id": "fcb5535f-432f-45a7-8ee5-9c7ae346fe5c",
          "headerName": "Row",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                "id": "2c7e7694-7ffa-4db5-ae41-29011dad52a2",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "ff327199-ba16-48f0-8756-4c61fa6f4aa9",
                    "headerName": "Stack",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "id": "1e57e78b-55e4-44b7-8860-51fdaecbbbdc",
                          "headerName": "Positioned",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "child": {
                              "id": "1a13575d-7635-4000-8e4c-e16a569d9532",
                              "headerName": "Container",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "decoration": {"boxFit": "none"},
                                "child": {
                                  "id": "e5c2aac2-5d70-47d5-8b34-1081b0663402",
                                  "headerName": "Image",
                                  "widgetName": "Image",
                                  "widgetSetting": {
                                    "fixCover": "true",
                                    "imageId":
                                        "85b8b5cf-533d-4aa0-ba06-8deb6620054c"
                                  }
                                },
                                "height": "90.h",
                                "width": "23.7.w"
                              }
                            },
                            "top": "5.h",
                            "right": "6.3.w"
                          }
                        },
                        {
                          "id": "1e74b265-7371-42a0-be6e-cbf988ec9829",
                          "headerName": "Positioned",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "child": {
                              "id": "36b39a8a-ab60-415c-bcfa-5d59ae6be372",
                              "headerName": "Container",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "decoration": {
                                  "boxFit": "none",
                                  "color": "0xEEECEFF1"
                                },
                                "child": {
                                  "id": "2f4faa59-397f-4d13-b99b-b0c0cf203c40",
                                  "headerName": "Column",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "a64b5220-61e2-4978-8fdb-1b9682449463",
                                        "headerName": "SizedBox",
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "179293ad-4de5-498e-9abb-f030d6debea1",
                                            "headerName": "Image",
                                            "widgetName": "Image",
                                            "widgetSetting": {
                                              "fixCover": "false",
                                              "imageId":
                                                  "d680fb22-7ee1-4f92-9d95-6b46288e9b51"
                                            }
                                          },
                                          "width": "20.w",
                                          "height": "20.h"
                                        }
                                      },
                                      {
                                        "id":
                                            "0a768004-d3bb-4603-992e-e4bdbae630cf",
                                        "headerName": "Text",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "textAlgin": "start",
                                          "style": {
                                            "fontSize": "10.h",
                                            "fontWeight": "w400",
                                            "fontFamily": "Gravitas One",
                                            "letterSpacing": "1.w"
                                          },
                                          "data": "SPA"
                                        }
                                      },
                                      {
                                        "id":
                                            "e77dfb1f-faa0-4c66-82d5-ebe2567591b3",
                                        "headerName": "Text",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "textAlgin": "start",
                                          "style": {
                                            "fontSize": "2.w",
                                            "fontWeight": "w800",
                                            "fontFamily": "Arvo"
                                          },
                                          "data": "CARE YOUR BODY"
                                        }
                                      }
                                    ]
                                  }
                                },
                                "height": "70.h",
                                "width": "23.4.w"
                              }
                            },
                            "right": "3.w",
                            "top": "0.w"
                          }
                        },
                        {
                          "id": "813008e3-b836-4111-8ed9-97d301c3ef24",
                          "headerName": "Positioned",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "child": {
                              "id": "4dd5f012-1ad5-4ebd-b1f6-d11a8bc6e8fd",
                              "headerName": "Container",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "decoration": {
                                  "boxFit": "none",
                                  "color": "0xCCFCE4EC"
                                },
                                "child": {
                                  "id": "b4a05816-ab6d-46f2-b259-cc1d5e51df43",
                                  "headerName": "Center",
                                  "widgetName": "Center",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "21c243ec-936e-4b8f-ab20-50a55f166c54",
                                      "headerName": "Text",
                                      "widgetName": "Text",
                                      "widgetSetting": {
                                        "textAlgin": "start",
                                        "style": {
                                          "fontSize": "3.h",
                                          "fontWeight": "w800",
                                          "fontFamily": "Arvo"
                                        },
                                        "data": "OPEN FROM 9AM TO 6PM"
                                      }
                                    }
                                  }
                                },
                                "height": "25.h",
                                "width": "23.4.w"
                              }
                            },
                            "right": "3.w",
                            "bottom": "0.w"
                          }
                        }
                      ]
                    }
                  }
                }
              },
              {
                "id": "b497e70f-4155-40d0-b55a-97abfbc07b09",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "be247496-3316-4ba2-9ce6-0b9fb9047a10",
                    "headerName": "Center",
                    "widgetName": "Center",
                    "widgetSetting": {
                      "child": {
                        "id": "0b9b5aa8-cedb-4320-8b5c-8213cf42661c",
                        "headerName": "CategoryTitleSingleColumn",
                        "widgetName": "CategoryTitleSingleColumn",
                        "widgetSetting": {
                          "useBackgroundCategoryTitle": "false",
                          "useThumbnailProduct": "false",
                          "toUpperCaseNameProductName": "false",
                          "useMediumStyle": "false",
                          "useFullWidthCategoryTitle": "false",
                          "dataList": {
                            "category": "1149761c-f4bc-4db2-b431-1e8ef93e9268",
                            "product": [
                              "a263a7a1-44b0-4a54-8f2c-fa6eb1677532",
                              "fee998fa-2080-47e9-8357-94d2ac33eadc",
                              "750bbe5c-3c1c-4488-88d7-263536dad8b7",
                              "86842abe-0048-45a6-b402-3fced2aa63d7",
                              "095565df-3644-4a09-8d6f-15a9e04eefb2",
                              "ce14eabd-c1c4-4785-83ed-8ce372ce1018",
                              "c1c510e5-4fc6-43b5-91a4-54754450bbec",
                              "dfd56dbd-0e5d-41d8-850a-f9912ce058a3"
                            ]
                          },
                          "colorCategoryTitle": "0xFF000000",
                          "colorPrice": "0xFF000000",
                          "colorProductName": "0xFF000000",
                          "colorDescription": "0xFF000000",
                          "runSpacing": "1.h",
                          "mainAxisAlignment": "center"
                        }
                      }
                    }
                  }
                }
              },
              {
                "id": "5ec45e4e-a8e7-4dc4-bb05-028069f5510c",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "f2155415-531b-4ad8-92c8-91189f1abd68",
                    "headerName": "Stack",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "id": "76b05d1b-5822-4893-a7fd-a463b351d646",
                          "headerName": "Positioned",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "child": {
                              "id": "26964710-09f2-4487-9925-ee2ceef1391b",
                              "headerName": "Container",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "decoration": {
                                  "imageUrl":
                                      "http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_illustrate2.png",
                                  "boxFit": "cover"
                                },
                                "child": {},
                                "height": "44.h",
                                "width": "27.3.w"
                              }
                            },
                            "top": "4.h",
                            "left": "3.w"
                          }
                        },
                        {
                          "id": "986e5561-038f-46cc-982f-4b50898bed4f",
                          "headerName": "Positioned",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "child": {
                              "id": "29afc9b0-71b5-4b1a-ac9e-0fe563fb3804",
                              "headerName": "Container",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "decoration": {
                                  "imageUrl":
                                      "http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_illustrate1-scaled.jpg",
                                  "boxFit": "cover"
                                },
                                "child": {},
                                "height": "44.h",
                                "width": "27.3.w"
                              }
                            },
                            "bottom": "4.h",
                            "left": "3.w"
                          }
                        },
                        {
                          "id": "118709a8-8094-4001-8cfc-fab2b6a34eb7",
                          "headerName": "Positioned",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "child": {
                              "id": "6c221b94-dbaa-4903-9cb0-e4242a989a3c",
                              "headerName": "Container",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "decoration": {
                                  "boxFit": "none",
                                  "color": "0xEEECEFF1"
                                },
                                "child": {
                                  "id": "f37704c8-dac8-4b0b-98bf-651af85623ac",
                                  "headerName": "RotatedBox",
                                  "widgetName": "RotatedBox",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "0e381d2d-27fa-4b81-8e4c-d4d0c151fde8",
                                      "headerName": "Row",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "textRightToLeft": "false",
                                        "children": [
                                          {
                                            "id":
                                                "c92074e8-02bd-4a3d-8d41-b200f4c91d41",
                                            "headerName": "Text",
                                            "widgetName": "Text",
                                            "widgetSetting": {
                                              "textAlgin": "start",
                                              "style": {
                                                "fontSize": "1.2.w",
                                                "fontWeight": "w400",
                                                "fontFamily": "Arvo"
                                              },
                                              "data": "wwww.salon.wwww"
                                            }
                                          },
                                          {
                                            "id":
                                                "c81c4425-8a83-4b4c-95c7-ee3d982e18bf",
                                            "headerName": "Text",
                                            "widgetName": "Text",
                                            "widgetSetting": {
                                              "textAlgin": "start",
                                              "style": {
                                                "fontSize": "1.2.w",
                                                "fontWeight": "w400",
                                                "fontFamily": "Arvo"
                                              },
                                              "data": "@salon.com"
                                            }
                                          }
                                        ],
                                        "mainAxisAlignment": "spaceAround"
                                      }
                                    },
                                    "quarterTurns": "3"
                                  }
                                },
                                "height": "100.h",
                                "width": "5.w"
                              }
                            },
                            "right": "6.w"
                          }
                        }
                      ]
                    }
                  }
                }
              }
            ]
          }
        }
      }
    }
  };

  static final template7 = {
    "_id": "64ee744398c3d4605b477abi",
    "templateSlug": "temp_nail_2",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'w700',
          'fontFamily': 'Nunito Sans',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.w',
          'nameProductStyle': '1.w',
          'priceProductStyle': '0.9.w',
          "discriptionProductStyle": '0.55.w',
        },
        '21/9': {
          'categoryNameStyle': '1.6.w',
          'nameProductStyle': '1.4.w',
          'priceProductStyle': '1.4.w',
          "discriptionProductStyle": '0.8.w',
        },
        '16/9': {
          'categoryNameStyle': '1.7.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          "discriptionProductStyle": '1.3.w',
        },
        '16/10': {
          'categoryNameStyle': '1.9.w',
          'nameProductStyle': '1.7.w',
          'priceProductStyle': '1.7.w',
          "discriptionProductStyle": '1.1.w',
        },
        '4/3': {
          'categoryNameStyle': '2.8.w',
          'nameProductStyle': '2.4.w',
          'priceProductStyle': '2.4.w',
          "discriptionProductStyle": '1.2.w',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.25.w',
          'priceProductStyle': '1.25.w',
        },
      },
    },
    "dataSetting": {
      "b3b6a205-2b5d-4050-b2a8-b959eac6cf63": {
        "id": "b3b6a205-2b5d-4050-b2a8-b959eac6cf63",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description": ""
      },
      "015c1dac-efd6-4318-8ac3-2dab7967a5e0": {
        "id": "015c1dac-efd6-4318-8ac3-2dab7967a5e0",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "image": "",
        "description": "this is a description"
      },
      "75662457-56fe-4436-8ac8-5af8c1a61c0e": {
        "id": "75662457-56fe-4436-8ac8-5af8c1a61c0e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "10154ee3-0b64-42b3-b3fd-c0f0ba0639ea": {
        "id": "10154ee3-0b64-42b3-b3fd-c0f0ba0639ea",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "32849d93-cdb4-47ef-89eb-3dc1e7b4698b": {
        "id": "32849d93-cdb4-47ef-89eb-3dc1e7b4698b",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "141bb36e-165a-47c0-979b-3abe1f1af29c": {
        "id": "141bb36e-165a-47c0-979b-3abe1f1af29c",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "aebcb2e0-dfe4-4030-b50d-877537ca8420": {
        "id": "aebcb2e0-dfe4-4030-b50d-877537ca8420",
        "type": "category",
        "name": "Category 2",
        "image": "",
        "description": ""
      },
      "59915f4e-973f-4e00-9d42-6b64c880476b": {
        "id": "59915f4e-973f-4e00-9d42-6b64c880476b",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "02ea7c25-d803-4b1c-8dc3-dac6e073a1d2": {
        "id": "02ea7c25-d803-4b1c-8dc3-dac6e073a1d2",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "08109379-3bca-4317-8364-719ab670454d": {
        "id": "08109379-3bca-4317-8364-719ab670454d",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "2852150c-9f53-434f-bed9-e8bc6d13668f": {
        "id": "2852150c-9f53-434f-bed9-e8bc6d13668f",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "f1ceebc6-f408-4eb7-b245-d3918ae7501d": {
        "id": "f1ceebc6-f408-4eb7-b245-d3918ae7501d",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "21e4e8b0-e1f6-4768-8c30-0296abf896b4": {
        "id": "21e4e8b0-e1f6-4768-8c30-0296abf896b4",
        "type": "category",
        "name": "Category 3",
        "image": "",
        "description": ""
      },
      "0e778bc5-22db-4770-b925-a832901ad35a": {
        "id": "0e778bc5-22db-4770-b925-a832901ad35a",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "80b3143f-8dae-451d-afa2-33552aa63127": {
        "id": "80b3143f-8dae-451d-afa2-33552aa63127",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      }
    },
    "widgets_setting": {
      "id": "ce34c2be-0fc6-4ec1-b103-6f4656b4b542",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"boxFit": "fill"},
        "child": {
          "id": "3aebc0ad-5f53-4a58-98ea-b16fd29c5f47",
          "headerName": "CustomContainer",
          "widgetName": "CustomContainer",
          "widgetSetting": {
            "opacity": "1",
            "fit": "none",
            "child": {
              "id": "40864fef-dcad-4e90-8a0f-319c113d2037",
              "headerName": "Row",
              "widgetName": "Row",
              "widgetSetting": {
                "textRightToLeft": "false",
                "children": [
                  {
                    "id": "4e67e3ff-9ea3-4541-bdbf-bd0ebf383b9d",
                    "headerName": "Container",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "decoration": {"boxFit": "none"},
                      "child": {
                        "id": "ff63ef8f-2470-4dce-b431-ed866cf247f4",
                        "headerName": "Column",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "c9f64226-77d3-4231-8f45-b8c55ef03c65",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"child": {}, "height": "3.h"}
                            },
                            {
                              "id": "c924b668-5d59-426d-9493-07b09096e795",
                              "headerName": "Divider",
                              "widgetName": "Divider",
                              "widgetSetting": {
                                "height": "6.h",
                                "color": "0x8F634201",
                                "thickness": "2"
                              }
                            },
                            {
                              "id": "ec8ed686-ba0d-4a69-8e51-866c6879dd07",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "1d75dc2a-dab3-4c33-9283-677d7dc7d7de",
                                  "headerName": "Column",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "8ac544a6-6e63-4e2d-b833-d4adb99ab204",
                                        "headerName": "CategoryTitle",
                                        "widgetName": "CategoryTitle",
                                        "widgetSetting": {
                                          "useBackground": "false",
                                          "useFullWidth": "false",
                                          "categoryId":
                                              "b3b6a205-2b5d-4050-b2a8-b959eac6cf63",
                                          "color": "0xFF895B3A"
                                        }
                                      },
                                      {
                                        "id":
                                            "a13c31fb-698e-43cd-b981-d22578132c9f",
                                        "headerName": "ServiceItemColumn",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "toUpperCaseNameProductName": "false",
                                          "useMediumStyle": "false",
                                          "maxLineProductName": "2",
                                          "productId":
                                              "015c1dac-efd6-4318-8ac3-2dab7967a5e0",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w"
                                        }
                                      },
                                      {
                                        "id":
                                            "2a5194f1-2c5c-4676-b7b0-6c758d5d5d43",
                                        "headerName": "ServiceItemColumn",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "toUpperCaseNameProductName": "false",
                                          "useMediumStyle": "false",
                                          "maxLineProductName": "2",
                                          "productId":
                                              "75662457-56fe-4436-8ac8-5af8c1a61c0e",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w"
                                        }
                                      },
                                      {
                                        "id":
                                            "b87aabfa-a933-41df-b638-5f54e1e5cdc5",
                                        "headerName": "ServiceItemColumn",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "toUpperCaseNameProductName": "false",
                                          "useMediumStyle": "false",
                                          "maxLineProductName": "2",
                                          "productId":
                                              "10154ee3-0b64-42b3-b3fd-c0f0ba0639ea",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w"
                                        }
                                      },
                                      {
                                        "id":
                                            "60bbb073-0115-49ad-a5ba-f1a32f34aa33",
                                        "headerName": "ServiceItemColumn",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "toUpperCaseNameProductName": "false",
                                          "useMediumStyle": "false",
                                          "maxLineProductName": "2",
                                          "productId":
                                              "32849d93-cdb4-47ef-89eb-3dc1e7b4698b",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w"
                                        }
                                      },
                                      {
                                        "id":
                                            "9aa71750-50f7-4df3-9110-fe488b2840d0",
                                        "headerName": "ServiceItemColumn",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "toUpperCaseNameProductName": "false",
                                          "useMediumStyle": "false",
                                          "maxLineProductName": "2",
                                          "productId":
                                              "141bb36e-165a-47c0-979b-3abe1f1af29c",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w"
                                        }
                                      }
                                    ],
                                    "mainAxisAlignment": "spaceBetween"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      },
                      "width": "33.3.w",
                      "padding": {"horizontal": "3.w", "bottom": "4.5.h"}
                    }
                  },
                  {
                    "id": "e449b739-96f5-44d2-a7b7-3fafec3dcefe",
                    "headerName": "VerticalDivider",
                    "widgetName": "VerticalDivider",
                    "widgetSetting": {"color": "0x8F634201", "thickness": "2"}
                  },
                  {
                    "id": "e4074051-1cb7-4ea6-83f5-7e7420a605d5",
                    "headerName": "Container",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "decoration": {"boxFit": "none"},
                      "child": {
                        "id": "555324fc-f5f5-461a-8437-8f68b6f7b52a",
                        "headerName": "Column",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "371fb7b3-f4ae-473b-8c37-72de9a2c67fe",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"child": {}, "height": "3.h"}
                            },
                            {
                              "id": "91d880e1-7d66-4807-9008-d24aaae152e5",
                              "headerName": "Row",
                              "widgetName": "Row",
                              "widgetSetting": {
                                "textRightToLeft": "false",
                                "children": [
                                  {
                                    "id":
                                        "61f60902-5bf2-4908-910d-511cdb843b37",
                                    "headerName": "Expanded",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "flex": "1",
                                      "child": {
                                        "id":
                                            "52fce401-74c7-4fcc-8b8d-a67817bf191c",
                                        "headerName": "Divider",
                                        "widgetName": "Divider",
                                        "widgetSetting": {
                                          "height": "6.h",
                                          "color": "0x8F634201",
                                          "thickness": "2"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "9148bc1d-1d7b-40a1-9a91-4b12fe751b65",
                                    "headerName": "Center",
                                    "widgetName": "Center",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "97691bcc-77a6-4b50-970a-7171825d8019",
                                        "headerName": "Container",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "decoration": {
                                            "boxFit": "fill",
                                            "color": "0x00000000"
                                          },
                                          "child": {
                                            "id":
                                                "024a8c3f-0f72-446a-acb0-36ab7467b863",
                                            "headerName": "Center",
                                            "widgetName": "Center",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "77fa4c2b-49f4-44f5-bb7f-31b8fba1f0c9",
                                                "headerName": "Text",
                                                "widgetName": "Text",
                                                "widgetSetting": {
                                                  "textAlgin": "start",
                                                  "style": {
                                                    "fontSize": "3.h",
                                                    "fontWeight": "w700",
                                                    "fontFamily": "Nunito Sans",
                                                    "color": "0xFF895B3A"
                                                  },
                                                  "data": "OUR TREATMENTS"
                                                }
                                              }
                                            }
                                          },
                                          "height": "6.h",
                                          "padding": {"horizontal": "3.w"}
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "e78ba872-61e1-4c38-b098-4d19de4df9ad",
                                    "headerName": "Expanded",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "flex": "1",
                                      "child": {
                                        "id":
                                            "d69653e4-478d-49df-9e4d-6c3debf5d372",
                                        "headerName": "Divider",
                                        "widgetName": "Divider",
                                        "widgetSetting": {
                                          "height": "6.h",
                                          "color": "0x8F634201",
                                          "thickness": "2"
                                        }
                                      }
                                    }
                                  }
                                ]
                              }
                            },
                            {
                              "id": "d9199ea4-15ba-401f-b7e5-e7612ccf0432",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "b4fb9ef0-e129-42ed-a12c-cf9ff486b752",
                                  "headerName": "Column",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "95c51f51-02f4-4128-a05d-dc4f74dd858d",
                                        "headerName": "CategoryTitle",
                                        "widgetName": "CategoryTitle",
                                        "widgetSetting": {
                                          "useBackground": "false",
                                          "useFullWidth": "false",
                                          "categoryId":
                                              "aebcb2e0-dfe4-4030-b50d-877537ca8420",
                                          "color": "0xFF895B3A"
                                        }
                                      },
                                      {
                                        "id":
                                            "90d7c056-4d00-40cb-88a8-0185e1c3e8e7",
                                        "headerName": "ServiceItemColumn",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "toUpperCaseNameProductName": "false",
                                          "useMediumStyle": "false",
                                          "maxLineProductName": "2",
                                          "productId":
                                              "59915f4e-973f-4e00-9d42-6b64c880476b",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w"
                                        }
                                      },
                                      {
                                        "id":
                                            "7435a274-ac6b-4d64-9259-cab357f139d5",
                                        "headerName": "ServiceItemColumn",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "toUpperCaseNameProductName": "false",
                                          "useMediumStyle": "false",
                                          "maxLineProductName": "2",
                                          "productId":
                                              "02ea7c25-d803-4b1c-8dc3-dac6e073a1d2",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w"
                                        }
                                      },
                                      {
                                        "id":
                                            "5f0fe2e3-a618-4fa3-a9da-d7f4e1ecf35c",
                                        "headerName": "ServiceItemColumn",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "toUpperCaseNameProductName": "false",
                                          "useMediumStyle": "false",
                                          "maxLineProductName": "2",
                                          "productId":
                                              "08109379-3bca-4317-8364-719ab670454d",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w"
                                        }
                                      },
                                      {
                                        "id":
                                            "7223335b-d741-4b7b-a09b-6ee354050924",
                                        "headerName": "ServiceItemColumn",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "toUpperCaseNameProductName": "false",
                                          "useMediumStyle": "false",
                                          "maxLineProductName": "2",
                                          "productId":
                                              "2852150c-9f53-434f-bed9-e8bc6d13668f",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w"
                                        }
                                      },
                                      {
                                        "id":
                                            "a3341f22-f21e-4baf-85c5-eb11cf0d41d4",
                                        "headerName": "ServiceItemColumn",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "toUpperCaseNameProductName": "false",
                                          "useMediumStyle": "false",
                                          "maxLineProductName": "2",
                                          "productId":
                                              "f1ceebc6-f408-4eb7-b245-d3918ae7501d",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w"
                                        }
                                      }
                                    ],
                                    "mainAxisAlignment": "spaceBetween"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      },
                      "width": "33.3.w",
                      "padding": {"horizontal": "3.w", "bottom": "4.5.h"}
                    }
                  },
                  {
                    "id": "c9cc1543-90c1-4f95-9ee3-d6590865c245",
                    "headerName": "VerticalDivider",
                    "widgetName": "VerticalDivider",
                    "widgetSetting": {"color": "0x8F634201", "thickness": "2"}
                  },
                  {
                    "id": "4c5eb215-9499-4a0f-b64e-031f383a1544",
                    "headerName": "Expanded",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "id": "ccb918c6-6078-4b06-b065-e1504131e8da",
                        "headerName": "Padding",
                        "widgetName": "Padding",
                        "widgetSetting": {
                          "child": {
                            "id": "66815db5-b071-4522-896c-07326b466f8b",
                            "headerName": "Column",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "children": [
                                {
                                  "id": "824f76d6-15af-41ad-a8b2-749b17d09eb5",
                                  "headerName": "SizedBox",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {
                                    "child": {},
                                    "height": "3.h"
                                  }
                                },
                                {
                                  "id": "37287baa-8acd-4137-85a8-1722fa9d66e1",
                                  "headerName": "Divider",
                                  "widgetName": "Divider",
                                  "widgetSetting": {
                                    "height": "6.h",
                                    "color": "0x8F634201",
                                    "thickness": "2"
                                  }
                                },
                                {
                                  "id": "1a31e11f-5e40-4a79-bb6b-c718ca6b341c",
                                  "headerName": "Expanded",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "flex": "1",
                                    "child": {
                                      "id":
                                          "5bf091b6-f01b-4671-bb4c-86d4b1a6ad3f",
                                      "headerName": "Column",
                                      "widgetName": "Column",
                                      "widgetSetting": {
                                        "children": [
                                          {
                                            "id":
                                                "cfcc34dd-4273-48b9-95b9-7fc86b7b5056",
                                            "headerName": "CategoryTitle",
                                            "widgetName": "CategoryTitle",
                                            "widgetSetting": {
                                              "useBackground": "false",
                                              "useFullWidth": "false",
                                              "categoryId":
                                                  "21e4e8b0-e1f6-4768-8c30-0296abf896b4",
                                              "color": "0xFF895B3A"
                                            }
                                          },
                                          {
                                            "id":
                                                "f6d2b160-a325-44c7-a1b3-13ef3b07e535",
                                            "headerName": "ServiceItemColumn",
                                            "widgetName": "ServiceItemColumn",
                                            "widgetSetting": {
                                              "toUpperCaseNameProductName":
                                                  "false",
                                              "useMediumStyle": "false",
                                              "maxLineProductName": "2",
                                              "productId":
                                                  "0e778bc5-22db-4770-b925-a832901ad35a",
                                              "colorPrice": "0xFFA76744",
                                              "colorProductName": "0xFFA76744",
                                              "colorDescription": "0xFF000000",
                                              "width": "23.w"
                                            }
                                          },
                                          {
                                            "id":
                                                "cb33efa0-e769-4e63-92df-80347adccd3c",
                                            "headerName": "ServiceItemColumn",
                                            "widgetName": "ServiceItemColumn",
                                            "widgetSetting": {
                                              "toUpperCaseNameProductName":
                                                  "false",
                                              "useMediumStyle": "false",
                                              "maxLineProductName": "2",
                                              "productId":
                                                  "80b3143f-8dae-451d-afa2-33552aa63127",
                                              "colorPrice": "0xFFA76744",
                                              "colorProductName": "0xFFA76744",
                                              "colorDescription": "0xFF000000",
                                              "width": "23.w"
                                            }
                                          }
                                        ],
                                        "mainAxisAlignment": "spaceAround"
                                      }
                                    }
                                  }
                                },
                                {
                                  "id": "fa06d034-62bc-44f7-90a8-dfd68ae4d26a",
                                  "headerName": "Container",
                                  "widgetName": "Container",
                                  "widgetSetting": {
                                    "decoration": {
                                      "boxFit": "none",
                                      "color": "0xFF895B3A"
                                    },
                                    "child": {
                                      "id":
                                          "ff4cdfe4-e07e-4a27-a043-3311ddce86c2",
                                      "headerName": "Center",
                                      "widgetName": "Center",
                                      "widgetSetting": {
                                        "child": {
                                          "id":
                                              "565f6852-f651-42b2-b568-3d83aba0af91",
                                          "headerName": "Text",
                                          "widgetName": "Text",
                                          "widgetSetting": {
                                            "textAlgin": "start",
                                            "style": {
                                              "fontSize": "1.5.w",
                                              "fontWeight": "w500",
                                              "fontFamily": "Nunito Sans",
                                              "color": "0xFFFFFFFF"
                                            },
                                            "data":
                                                "GET 10% OFF \\nYOUR FIRST PAYMENT"
                                          }
                                        }
                                      }
                                    },
                                    "height": "18.h",
                                    "width": "20.w"
                                  }
                                },
                                {
                                  "id": "76776346-5fb2-4385-8af9-d33e493081f4",
                                  "headerName": "SizedBox",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {
                                    "child": {},
                                    "height": "5.h"
                                  }
                                },
                                {
                                  "id": "f00b7638-1f88-4cd9-beaa-22a7d5f35d68",
                                  "headerName": "Container",
                                  "widgetName": "Container",
                                  "widgetSetting": {
                                    "decoration": {
                                      "imageUrl":
                                          "http://foodiemenu.co/wp-content/uploads/2023/09/image1.jpg",
                                      "boxFit": "cover"
                                    },
                                    "child": {},
                                    "height": "20.h",
                                    "width": "26.w"
                                  }
                                }
                              ]
                            }
                          },
                          "padding": {"horizontal": "3.w", "bottom": "4.5.h"}
                        }
                      }
                    }
                  }
                ]
              }
            },
            "height": "100.h",
            "width": "100.w",
            "image":
                "http://foodiemenu.co/wp-content/uploads/2023/09/background-scaled.jpg",
            "color": "0x4400539C",
            "blendMode": "dstATop"
          }
        }
      }
    }
  };

  static final template8 = {
    "_id": "64ee744398c3d4605b477abk",
    "templateSlug": "temp_nail_3",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Cormorant Garamond',
        },
        'nameProductStyle': {
          'fontWeight': 'w600',
          'fontFamily': 'Cormorant Garamond',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Cormorant Garamond',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Lato',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Lato',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Cormorant Garamond',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Cormorant Garamond',
        },
      },
      'renderScreen': {
        '32/9': {
          'nameProductStyle': '1.w',
          'priceProductStyle': '1.w',
          'discriptionProductStyle': '0.9.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '16.h',
        },
        '21/9': {
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.2.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '18.h',
        },
        '16/9': {
          'nameProductStyle': '1.75.w',
          'priceProductStyle': '1.75.w',
          'imageHeight': '25.h',
          'discriptionProductStyle': '1.25.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '20.h',
        },
        '16/10': {
          'nameProductStyle': '1.8.w',
          'priceProductStyle': '1.8.w',
          'imageHeight': '25.h',
          'discriptionProductStyle': '1.25.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '20.h',
        },
        '4/3': {
          'nameProductStyle': '2.w',
          'priceProductStyle': '2.w',
          'discriptionProductStyle': '1.5.w',

          // 'widthThumnailProduct': '28.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '22.h',
        },
        'default': {
          'nameProductStyle': '1.75.w',
          'priceProductStyle': '1.75.w',
          'imageHeight': '25.h',
          'discriptionProductStyle': '1.25.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '20.h',
        },
      },
    },
    "dataSetting": {
      "06474b49-1b7a-402b-8687-cda3724f7f1e": {
        "id": "06474b49-1b7a-402b-8687-cda3724f7f1e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_1.jpg",
        "description": "this is a description"
      },
      "969b1894-ac92-44a4-a04a-59fa356f07ce": {
        "id": "969b1894-ac92-44a4-a04a-59fa356f07ce",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_2.jpg",
        "description": "this is a description"
      },
      "ac310d98-6cb9-46ce-b3b2-1918ce2b2d92": {
        "id": "ac310d98-6cb9-46ce-b3b2-1918ce2b2d92",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_3.jpg",
        "description": "this is a description"
      },
      "af445bc7-1e82-4189-a93b-b9d8b8fdfb32": {
        "id": "af445bc7-1e82-4189-a93b-b9d8b8fdfb32",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_4.jpg",
        "description": "this is a description"
      },
      "23760a2d-b097-4124-8d46-3d00ebe9afc3": {
        "id": "23760a2d-b097-4124-8d46-3d00ebe9afc3",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_5.jpg",
        "description": "this is a description"
      },
      "73b34cb6-1cb3-4e06-97d1-fc480398a2e4": {
        "id": "73b34cb6-1cb3-4e06-97d1-fc480398a2e4",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_6.jpg",
        "description": "this is a description"
      }
    },
    "widgets_setting": {
      "id": "bc0a4315-ae42-4f54-a151-35a2940ea6a1",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"boxFit": "none", "color": "0xFFF2EFEA"},
        "child": {
          "id": "de5b9c3b-40a8-45b7-ac05-15c35bd82136",
          "headerName": "Column",
          "widgetName": "Column",
          "widgetSetting": {
            "children": [
              {
                "id": "fe34ed4f-9067-4257-b458-569207edb70c",
                "headerName": "Divider",
                "widgetName": "Divider",
                "widgetSetting": {
                  "height": "4.h",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "endIndent": "20.w",
                  "thickness": "2"
                }
              },
              {
                "id": "6c11418a-8ec9-44fe-bf5d-16c7ff7973d3",
                "headerName": "Text",
                "widgetName": "Text",
                "widgetSetting": {
                  "textAlgin": "start",
                  "style": {
                    "fontSize": "4.h",
                    "fontWeight": "w800",
                    "fontFamily": "Cormorant Garamond",
                    "color": "0xFF616148"
                  },
                  "data": "NAIL SALON"
                }
              },
              {
                "id": "3704770f-266a-4237-b9d2-64fa143a8851",
                "headerName": "Text",
                "widgetName": "Text",
                "widgetSetting": {
                  "textAlgin": "start",
                  "style": {
                    "fontSize": "3.h",
                    "fontWeight": "w800",
                    "fontFamily": "Cormorant Garamond"
                  },
                  "data": "NAIL IT!"
                }
              },
              {
                "id": "5869518c-224b-449b-ace9-54bb1e21adff",
                "headerName": "Divider",
                "widgetName": "Divider",
                "widgetSetting": {
                  "height": "4.h",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "endIndent": "20.w",
                  "thickness": "2"
                }
              },
              {
                "id": "36edbc50-2277-40b2-8c76-efb7d52f68ad",
                "headerName": "Wrap",
                "widgetName": "Wrap",
                "widgetSetting": {
                  "clipBehavior": "none",
                  "children": [
                    {
                      "id": "16dc3492-9612-406a-8145-39806b1391de",
                      "headerName": "ServiceItemWithThumbnail",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "toUpperCaseNameProductName": "true",
                        "useMediumStyle": "false",
                        "maxLineProductName": "2",
                        "maxLineProductDescription": "2",
                        "productId": "06474b49-1b7a-402b-8687-cda3724f7f1e",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    },
                    {
                      "id": "342edf67-9db3-44ef-bdf6-b34d78320a23",
                      "headerName": "ServiceItemWithThumbnail",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "toUpperCaseNameProductName": "true",
                        "useMediumStyle": "false",
                        "maxLineProductName": "2",
                        "maxLineProductDescription": "2",
                        "productId": "969b1894-ac92-44a4-a04a-59fa356f07ce",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    },
                    {
                      "id": "aa56a761-810b-4c73-b23b-151af92f465b",
                      "headerName": "ServiceItemWithThumbnail",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "toUpperCaseNameProductName": "true",
                        "useMediumStyle": "false",
                        "maxLineProductName": "2",
                        "maxLineProductDescription": "2",
                        "productId": "ac310d98-6cb9-46ce-b3b2-1918ce2b2d92",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    },
                    {
                      "id": "fcebedaa-97bf-47ba-af5e-28d86b351069",
                      "headerName": "ServiceItemWithThumbnail",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "toUpperCaseNameProductName": "true",
                        "useMediumStyle": "false",
                        "maxLineProductName": "2",
                        "maxLineProductDescription": "2",
                        "productId": "af445bc7-1e82-4189-a93b-b9d8b8fdfb32",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    },
                    {
                      "id": "0159d843-34b8-4fdd-8baa-f9f9a2250e0d",
                      "headerName": "ServiceItemWithThumbnail",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "toUpperCaseNameProductName": "true",
                        "useMediumStyle": "false",
                        "maxLineProductName": "2",
                        "maxLineProductDescription": "2",
                        "productId": "23760a2d-b097-4124-8d46-3d00ebe9afc3",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    },
                    {
                      "id": "fda1994f-5516-4252-99ed-6c53887ef497",
                      "headerName": "ServiceItemWithThumbnail",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "toUpperCaseNameProductName": "true",
                        "useMediumStyle": "false",
                        "maxLineProductName": "2",
                        "maxLineProductDescription": "2",
                        "productId": "73b34cb6-1cb3-4e06-97d1-fc480398a2e4",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    }
                  ],
                  "runSpacing": "2.h",
                  "spacing": "4.w"
                }
              },
              {
                "id": "694dae62-dc04-469d-a394-13cff4f5f368",
                "headerName": "Divider",
                "widgetName": "Divider",
                "widgetSetting": {
                  "height": "4.h",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "endIndent": "20.w",
                  "thickness": "2"
                }
              },
              {
                "id": "c0cd1014-9e41-4168-aa61-25c0ee5a7454",
                "headerName": "Row",
                "widgetName": "Row",
                "widgetSetting": {
                  "textRightToLeft": "false",
                  "children": [
                    {
                      "id": "a4e2f8a6-1d44-4570-8e1c-ac935b2ffb69",
                      "headerName": "Text",
                      "widgetName": "Text",
                      "widgetSetting": {
                        "textAlgin": "start",
                        "style": {
                          "fontSize": "3.h",
                          "fontWeight": "w400",
                          "fontFamily": "Cormorant Garamond",
                          "color": "0xFF616148"
                        },
                        "data": "salon@hairdresser"
                      }
                    },
                    {
                      "id": "fab7ce8a-d449-41bf-a032-7e1f567753ec",
                      "headerName": "Text",
                      "widgetName": "Text",
                      "widgetSetting": {
                        "textAlgin": "start",
                        "style": {
                          "fontSize": "3.h",
                          "fontWeight": "w400",
                          "fontFamily": "Neuton",
                          "color": "0xFF616148"
                        },
                        "data": "+012345689"
                      }
                    },
                    {
                      "id": "837a61ae-34ff-4f92-bf07-c31df0ce5e64",
                      "headerName": "Text",
                      "widgetName": "Text",
                      "widgetSetting": {
                        "textAlgin": "start",
                        "style": {
                          "fontSize": "3.h",
                          "fontWeight": "w400",
                          "fontFamily": "Cormorant Garamond",
                          "color": "0xFF616148"
                        },
                        "data": "hairdresser.com"
                      }
                    }
                  ],
                  "mainAxisAlignment": "spaceAround"
                }
              }
            ]
          }
        },
        "height": "100.h",
        "width": "100.w",
        "padding": {"vertical": "3.h"}
      }
    }
  };

  static final template9 = {
    "_id": "64ee744398c3d4605b477abl",
    "templateSlug": "temp_nail_4",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Playfair Display',
        },
        'nameProductStyle': {
          'fontWeight': 'w500',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w500',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.3.w',
          'nameProductStyle': '1.1.w',
          'priceProductStyle': '1.1.w',
        },
        '21/9': {
          'categoryNameStyle': '1.9.w',
          'nameProductStyle': '1.7.w',
          'priceProductStyle': '1.7.w',
        },
        '16/9': {
          'categoryNameStyle': '3.2.h',
          'nameProductStyle': '2.6.h',
          'priceProductStyle': '2.6.h',
        },
        '16/10': {
          'categoryNameStyle': '3.h',
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
        },
        '4/3': {
          'categoryNameStyle': '3.2.w',
          'nameProductStyle': '2.2.w',
          'priceProductStyle': '2.2.w',
        },
        'default': {
          'categoryNameStyle': '3.h',
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
        },
      },
    },
    "dataSetting": {
      "e0c274bf-b998-4d12-b895-33f902202328": {
        "id": "e0c274bf-b998-4d12-b895-33f902202328",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description": ""
      },
      "a6174efa-14db-4f80-9eb7-77d378592a7f": {
        "id": "a6174efa-14db-4f80-9eb7-77d378592a7f",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "70df0560-a884-4f3e-a0a4-9d52216b476e": {
        "id": "70df0560-a884-4f3e-a0a4-9d52216b476e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "17a40507-b820-46f3-a87a-9b309e1a159e": {
        "id": "17a40507-b820-46f3-a87a-9b309e1a159e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "9134c090-70f7-440a-b2ad-ff2c0464888d": {
        "id": "9134c090-70f7-440a-b2ad-ff2c0464888d",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "197250ba-68cd-410e-95aa-ad1056015737": {
        "id": "197250ba-68cd-410e-95aa-ad1056015737",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "cbb513d6-8a69-4eab-b99d-c9748c9394cf": {
        "id": "cbb513d6-8a69-4eab-b99d-c9748c9394cf",
        "type": "category",
        "name": "Category 2",
        "image": "",
        "description": ""
      },
      "3c895e5f-2c50-4186-a608-75e1a6218473": {
        "id": "3c895e5f-2c50-4186-a608-75e1a6218473",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "b02909ab-2512-46fc-98b9-619617f705c6": {
        "id": "b02909ab-2512-46fc-98b9-619617f705c6",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "d4b07a84-d41b-4512-9619-c944ef31e912": {
        "id": "d4b07a84-d41b-4512-9619-c944ef31e912",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "589358ba-9346-497a-bab8-773ac5486d7b": {
        "id": "589358ba-9346-497a-bab8-773ac5486d7b",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "4a78c136-3038-443e-ad0d-649fe8fd3c8f": {
        "id": "4a78c136-3038-443e-ad0d-649fe8fd3c8f",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "71a2a88c-f3a7-47ef-90ef-26aaf72819b0": {
        "id": "71a2a88c-f3a7-47ef-90ef-26aaf72819b0",
        "type": "category",
        "name": "Category 3",
        "image": "",
        "description": ""
      },
      "78ef2c41-df98-4eb2-9e63-9aa2f4fb201e": {
        "id": "78ef2c41-df98-4eb2-9e63-9aa2f4fb201e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "0f5aa0b4-67ea-4bd0-bb98-9923c1b42bd0": {
        "id": "0f5aa0b4-67ea-4bd0-bb98-9923c1b42bd0",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "6855f9b2-11e7-43bd-84bf-84b03ce94141": {
        "id": "6855f9b2-11e7-43bd-84bf-84b03ce94141",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "0676ec65-e67f-4458-9b9b-acdfc3811932": {
        "id": "0676ec65-e67f-4458-9b9b-acdfc3811932",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "50468ff6-9dea-4acc-9e8a-5cdfef8b554e": {
        "id": "50468ff6-9dea-4acc-9e8a-5cdfef8b554e",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "8f06bb9a-0be4-4d43-be79-e9820db9ded3": {
        "id": "8f06bb9a-0be4-4d43-be79-e9820db9ded3",
        "type": "category",
        "name": "Category 4",
        "image": "",
        "description": ""
      },
      "fdb057da-5550-4638-b643-928cf32cf6c5": {
        "id": "fdb057da-5550-4638-b643-928cf32cf6c5",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "d4b1a107-4f40-44c5-b227-0ce2c775c1f3": {
        "id": "d4b1a107-4f40-44c5-b227-0ce2c775c1f3",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "2b4eb461-f3ab-4c3b-bbe8-4535633b0922": {
        "id": "2b4eb461-f3ab-4c3b-bbe8-4535633b0922",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "6749e622-2425-47d3-a7f9-ea878a6d6fdf": {
        "id": "6749e622-2425-47d3-a7f9-ea878a6d6fdf",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "8a55a5b0-5df6-4ce9-8d5b-c4cb0715114d": {
        "id": "8a55a5b0-5df6-4ce9-8d5b-c4cb0715114d",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      }
    },
    "widgets_setting": {
      "id": "5b871228-df52-4db6-9511-198c2c19b835",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"boxFit": "fill", "color": "0xFFF1F1EF"},
        "child": {
          "id": "39c46aef-239a-48c1-a5a8-1a15e19918fc",
          "headerName": "Stack",
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              {
                "id": "ea66c198-33f6-455d-bda8-0715abe02445",
                "headerName": "Row",
                "widgetName": "Row",
                "widgetSetting": {
                  "textRightToLeft": "false",
                  "children": [
                    {
                      "id": "186a7e1b-1bf8-46af-8c16-b205678335f3",
                      "headerName": "Expanded",
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": "30",
                        "child": {
                          "id": "e6dab514-7ee5-4cbd-9870-6e1362899d2a",
                          "headerName": "Padding",
                          "widgetName": "Padding",
                          "widgetSetting": {
                            "child": {
                              "id": "bf82766e-bae1-4ab5-8fa2-ae5f71142065",
                              "headerName": "Column",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "children": [
                                  {
                                    "id":
                                        "99682d6b-eb5b-4bb8-88da-ce75f2912161",
                                    "headerName": "CategoryTitleSingleColumn",
                                    "widgetName": "CategoryTitleSingleColumn",
                                    "widgetSetting": {
                                      "useBackgroundCategoryTitle": "false",
                                      "useThumbnailProduct": "false",
                                      "toUpperCaseNameProductName": "false",
                                      "useMediumStyle": "false",
                                      "useFullWidthCategoryTitle": "false",
                                      "dataList": {
                                        "category":
                                            "e0c274bf-b998-4d12-b895-33f902202328",
                                        "product": [
                                          "a6174efa-14db-4f80-9eb7-77d378592a7f",
                                          "70df0560-a884-4f3e-a0a4-9d52216b476e",
                                          "17a40507-b820-46f3-a87a-9b309e1a159e",
                                          "9134c090-70f7-440a-b2ad-ff2c0464888d",
                                          "197250ba-68cd-410e-95aa-ad1056015737"
                                        ]
                                      },
                                      "colorCategoryTitle": "0xFF333A32",
                                      "colorPrice": "0xFF333A32",
                                      "colorProductName": "0xFF333A32",
                                      "colorDescription": "0xFF333A32",
                                      "spaceBetweenCategoryProductList": "3.h"
                                    }
                                  },
                                  {
                                    "id":
                                        "111d90a8-1477-4045-b5ab-d68aff1fc56f",
                                    "headerName": "CategoryTitleSingleColumn",
                                    "widgetName": "CategoryTitleSingleColumn",
                                    "widgetSetting": {
                                      "useBackgroundCategoryTitle": "false",
                                      "useThumbnailProduct": "false",
                                      "toUpperCaseNameProductName": "false",
                                      "useMediumStyle": "false",
                                      "useFullWidthCategoryTitle": "false",
                                      "dataList": {
                                        "category":
                                            "cbb513d6-8a69-4eab-b99d-c9748c9394cf",
                                        "product": [
                                          "3c895e5f-2c50-4186-a608-75e1a6218473",
                                          "b02909ab-2512-46fc-98b9-619617f705c6",
                                          "d4b07a84-d41b-4512-9619-c944ef31e912",
                                          "589358ba-9346-497a-bab8-773ac5486d7b",
                                          "4a78c136-3038-443e-ad0d-649fe8fd3c8f"
                                        ]
                                      },
                                      "colorCategoryTitle": "0xFF333A32",
                                      "colorPrice": "0xFF333A32",
                                      "colorProductName": "0xFF333A32",
                                      "colorDescription": "0xFF333A32",
                                      "spaceBetweenCategoryProductList": "3.h"
                                    }
                                  }
                                ],
                                "mainAxisAlignment": "spaceAround"
                              }
                            },
                            "padding": {"horizontal": "2.w", "top": "5.h"}
                          }
                        }
                      }
                    },
                    {
                      "id": "6bfcf9f3-1ef5-4c4c-81c6-0ea1b47f4165",
                      "headerName": "VerticalDivider",
                      "widgetName": "VerticalDivider",
                      "widgetSetting": {
                        "color": "0xFFBCBCBA ",
                        "thickness": "1"
                      }
                    },
                    {
                      "id": "f27a99b8-abcc-4578-b7af-9059e239b9b9",
                      "headerName": "Expanded",
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": "55",
                        "child": {
                          "id": "93879173-cd56-4025-832f-459eff6961e4",
                          "headerName": "Stack",
                          "widgetName": "Stack",
                          "widgetSetting": {
                            "children": [
                              {
                                "id": "363ab89e-e399-45f8-83e1-6e8fdde675c2",
                                "headerName": "Row",
                                "widgetName": "Row",
                                "widgetSetting": {
                                  "textRightToLeft": "false",
                                  "children": [
                                    {
                                      "id":
                                          "dee6b8d0-44cf-4c05-a101-133fb34584b1",
                                      "headerName": "Expanded",
                                      "widgetName": "Expanded",
                                      "widgetSetting": {
                                        "flex": "6",
                                        "child": {
                                          "id":
                                              "fbfcca52-bdb8-41c6-9e79-ea6ab0cc65b7",
                                          "headerName": "Text",
                                          "widgetName": "Text",
                                          "widgetSetting": {
                                            "textAlgin": "start",
                                            "style": {
                                              "fontSize": "14",
                                              "fontWeight": "w400"
                                            }
                                          }
                                        }
                                      }
                                    },
                                    {
                                      "id":
                                          "b9f342c3-596b-468e-9d31-5f5752a2ce43",
                                      "headerName": "Expanded",
                                      "widgetName": "Expanded",
                                      "widgetSetting": {
                                        "flex": "4",
                                        "child": {
                                          "id":
                                              "4fa0ad87-109a-4ebc-897d-c92ab6707373",
                                          "headerName": "Container",
                                          "widgetName": "Container",
                                          "widgetSetting": {
                                            "decoration": {
                                              "boxFit": "fill",
                                              "color": "0xFFCDC6B6"
                                            },
                                            "child": {
                                              "id":
                                                  "a0975396-a007-4f73-ba8c-d387bc93635d",
                                              "headerName": "Text",
                                              "widgetName": "Text",
                                              "widgetSetting": {
                                                "textAlgin": "start",
                                                "style": {
                                                  "fontSize": "14",
                                                  "fontWeight": "w400"
                                                }
                                              }
                                            },
                                            "height": "100.h"
                                          }
                                        }
                                      }
                                    }
                                  ]
                                }
                              },
                              {
                                "id": "5b7d1bd5-e93b-40a1-8f73-07243eacb9a2",
                                "headerName": "Padding",
                                "widgetName": "Padding",
                                "widgetSetting": {
                                  "child": {
                                    "id":
                                        "0114ec3c-c998-4f57-bd81-16543429632b",
                                    "headerName": "Row",
                                    "widgetName": "Row",
                                    "widgetSetting": {
                                      "textRightToLeft": "false",
                                      "children": [
                                        {
                                          "id":
                                              "2753c5cd-4ed4-4d58-be14-02a4d922cfda",
                                          "headerName": "Expanded",
                                          "widgetName": "Expanded",
                                          "widgetSetting": {
                                            "flex": "1",
                                            "child": {
                                              "id":
                                                  "22dabb9a-15ed-48b1-bc52-a0918e6f986c",
                                              "headerName": "Column",
                                              "widgetName": "Column",
                                              "widgetSetting": {
                                                "children": [
                                                  {
                                                    "id":
                                                        "13d1e4e8-2bcc-4495-8f79-c85c56cfdd10",
                                                    "headerName":
                                                        "CategoryTitleSingleColumn",
                                                    "widgetName":
                                                        "CategoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "useThumbnailProduct":
                                                          "false",
                                                      "toUpperCaseNameProductName":
                                                          "false",
                                                      "useMediumStyle": "false",
                                                      "useFullWidthCategoryTitle":
                                                          "false",
                                                      "dataList": {
                                                        "category":
                                                            "71a2a88c-f3a7-47ef-90ef-26aaf72819b0",
                                                        "product": [
                                                          "78ef2c41-df98-4eb2-9e63-9aa2f4fb201e",
                                                          "0f5aa0b4-67ea-4bd0-bb98-9923c1b42bd0",
                                                          "6855f9b2-11e7-43bd-84bf-84b03ce94141",
                                                          "0676ec65-e67f-4458-9b9b-acdfc3811932",
                                                          "50468ff6-9dea-4acc-9e8a-5cdfef8b554e"
                                                        ]
                                                      },
                                                      "colorCategoryTitle":
                                                          "0xFF333A32",
                                                      "colorPrice":
                                                          "0xFF333A32",
                                                      "colorProductName":
                                                          "0xFF333A32",
                                                      "colorDescription":
                                                          "0xFF333A32"
                                                    }
                                                  },
                                                  {
                                                    "id":
                                                        "75d6057b-e002-4c0b-981c-f702c57b0c86",
                                                    "headerName":
                                                        "CategoryTitleSingleColumn",
                                                    "widgetName":
                                                        "CategoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "useThumbnailProduct":
                                                          "false",
                                                      "toUpperCaseNameProductName":
                                                          "false",
                                                      "useMediumStyle": "false",
                                                      "useFullWidthCategoryTitle":
                                                          "false",
                                                      "dataList": {
                                                        "category":
                                                            "8f06bb9a-0be4-4d43-be79-e9820db9ded3",
                                                        "product": [
                                                          "fdb057da-5550-4638-b643-928cf32cf6c5",
                                                          "d4b1a107-4f40-44c5-b227-0ce2c775c1f3",
                                                          "2b4eb461-f3ab-4c3b-bbe8-4535633b0922",
                                                          "6749e622-2425-47d3-a7f9-ea878a6d6fdf",
                                                          "8a55a5b0-5df6-4ce9-8d5b-c4cb0715114d"
                                                        ]
                                                      },
                                                      "colorCategoryTitle":
                                                          "0xFF333A32",
                                                      "colorPrice":
                                                          "0xFF333A32",
                                                      "colorProductName":
                                                          "0xFF333A32",
                                                      "colorDescription":
                                                          "0xFF333A32"
                                                    }
                                                  }
                                                ],
                                                "mainAxisAlignment":
                                                    "spaceAround"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "8feec1c8-d4d1-4b32-9dc6-401cb6b6edeb",
                                          "headerName": "Expanded",
                                          "widgetName": "Expanded",
                                          "widgetSetting": {
                                            "flex": "1",
                                            "child": {
                                              "id":
                                                  "580524ec-15ba-45da-8738-b1e2f51ada0e",
                                              "headerName": "Column",
                                              "widgetName": "Column",
                                              "widgetSetting": {
                                                "children": [
                                                  {
                                                    "id":
                                                        "819d248a-e3d0-417b-ab12-a3dde3fb2996",
                                                    "headerName": "Container",
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "decoration": {
                                                        "imageUrl":
                                                            "https://foodiemenu.co/wp-content/uploads/2023/09/treatment_1.jpg",
                                                        "boxFit": "cover"
                                                      },
                                                      "child": {},
                                                      "height": "25.h",
                                                      "width": "23.w"
                                                    }
                                                  },
                                                  {
                                                    "id":
                                                        "c337a2d8-47b6-4613-970e-89d9dac1c094",
                                                    "headerName": "Container",
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "decoration": {
                                                        "imageUrl":
                                                            "https://foodiemenu.co/wp-content/uploads/2023/09/treatment_2.jpg",
                                                        "boxFit": "cover"
                                                      },
                                                      "child": {},
                                                      "height": "25.h",
                                                      "width": "23.w"
                                                    }
                                                  },
                                                  {
                                                    "id":
                                                        "39db8bed-5299-45ba-9b38-12364339c6b9",
                                                    "headerName": "Container",
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "decoration": {
                                                        "imageUrl":
                                                            "https://foodiemenu.co/wp-content/uploads/2023/09/treatment_3.jpg",
                                                        "boxFit": "cover"
                                                      },
                                                      "child": {},
                                                      "height": "25.h",
                                                      "width": "23.w"
                                                    }
                                                  }
                                                ],
                                                "mainAxisAlignment":
                                                    "spaceAround"
                                              }
                                            }
                                          }
                                        }
                                      ]
                                    }
                                  },
                                  "padding": {"top": "5.h", "left": "2.w"}
                                }
                              }
                            ]
                          }
                        }
                      }
                    },
                    {
                      "id": "8e1876e4-e957-4cce-a7b2-327f9e5427a9",
                      "headerName": "Expanded",
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": "15",
                        "child": {
                          "id": "17d99545-7739-439b-8e5b-f940bd4030db",
                          "headerName": "RotatedBox",
                          "widgetName": "RotatedBox",
                          "widgetSetting": {
                            "child": {
                              "id": "6e1a0242-b5ef-47e2-ae67-09250f61235d",
                              "headerName": "Center",
                              "widgetName": "Center",
                              "widgetSetting": {
                                "child": {
                                  "id": "3e423376-f57b-4537-82d9-e3f7e545fe38",
                                  "headerName": "Text",
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "textAlgin": "start",
                                    "style": {
                                      "fontSize": "5.w",
                                      "fontWeight": "w700",
                                      "fontFamily": "Playfair Display"
                                    },
                                    "data": "PRO NAIL SALON"
                                  }
                                }
                              }
                            },
                            "quarterTurns": "3"
                          }
                        }
                      }
                    }
                  ]
                }
              }
            ]
          }
        },
        "height": "100.h",
        "width": "100.w"
      }
    }
  };

  static final template10 = {
    "_id": "64ee744398c3d4605b477abm",
    "templateSlug": "temp_nail_5",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'DM Serif Display',
        },
        'nameProductStyle': {
          'fontWeight': 'w500',
          'fontFamily': 'DM Serif Display',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'bold',
          'fontFamily': 'DM Serif Display',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'DM Serif Text',
          'height': '1.5',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w600',
          'fontFamily': 'DM Serif Display',
        },
        'priceProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyleMedium': '0.8.w',
          'priceProductStyleMedium': '1.2.w',
          'discriptionProductStyleMedium': '0.6.w',
        },
        '21/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyleMedium': '1.2.w',
          'priceProductStyleMedium': '1.5.w',
          'discriptionProductStyleMedium': '0.9.w',
        },
        '16/9': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyleMedium': '1.5.w',
          'priceProductStyleMedium': '2.4.w',
          'discriptionProductStyleMedium': '1.w',
        },
        '16/10': {
          'categoryNameStyleMedium': '3.w',
          'nameProductStyleMedium': '1.4.w',
          'priceProductStyleMedium': '2.3.w',
          'discriptionProductStyleMedium': '1.w',
        },
        '4/3': {
          'categoryNameStyle': '1.8.w',
          'nameProductStyleMedium': '2.w',
          'priceProductStyleMedium': '3.w',
          'discriptionProductStyleMedium': '1.5.w',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyleMedium': '2.w',
          'priceProductStyleMedium': '2.w',
        },
      },
    },
    "dataSetting": {
      "a7aed668-20db-4e34-987e-2ffbf8f46f99": {
        "id": "a7aed668-20db-4e34-987e-2ffbf8f46f99",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "0691c940-d270-4f9f-80d6-22471368ead2": {
        "id": "0691c940-d270-4f9f-80d6-22471368ead2",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "fbb9ef0f-960d-4be7-b331-3236483a6c0a": {
        "id": "fbb9ef0f-960d-4be7-b331-3236483a6c0a",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "d2f5a95c-7558-47a6-a536-59ad3e013564": {
        "id": "d2f5a95c-7558-47a6-a536-59ad3e013564",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "85eaf1c0-34e8-4b7f-ad27-0dac4def3641": {
        "id": "85eaf1c0-34e8-4b7f-ad27-0dac4def3641",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "3bf08934-7160-4d43-8e2d-e7f87ba83e66": {
        "id": "3bf08934-7160-4d43-8e2d-e7f87ba83e66",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "6569270a-7afa-490b-8e02-dd706b3167cb": {
        "id": "6569270a-7afa-490b-8e02-dd706b3167cb",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "7aa5e661-8518-4183-b67f-95ed17fd0599": {
        "id": "7aa5e661-8518-4183-b67f-95ed17fd0599",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "fd07f166-25b0-48e9-b1db-62ae529d2596": {
        "id": "fd07f166-25b0-48e9-b1db-62ae529d2596",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/treatment_1.jpg"
      }
    },
    "widgets_setting": {
      "id": "bef2d359-fdd9-4310-8afb-6f885aaf5664",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"boxFit": "fill", "color": "0xFFFFFFFF"},
        "child": {
          "id": "67a26145-e8d5-4cd2-bdfb-37534a9e5b04",
          "headerName": "Stack",
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              {
                "id": "74dd9fe7-bca9-45c6-b534-2ef11ff6b345",
                "headerName": "Column",
                "widgetName": "Column",
                "widgetSetting": {
                  "children": [
                    {
                      "id": "12a5d443-555d-4458-a4f0-960d6e2cabb0",
                      "headerName": "Container",
                      "widgetName": "Container",
                      "widgetSetting": {
                        "decoration": {"boxFit": "fill", "color": "0xFFA76744"},
                        "child": {},
                        "height": "5.h"
                      }
                    },
                    {
                      "id": "9e1f6851-2da2-4e1c-b82c-86c6b4d02cdd",
                      "headerName": "Expanded",
                      "widgetName": "Expanded",
                      "widgetSetting": {"flex": "1", "child": {}}
                    },
                    {
                      "id": "d15561ea-33e3-4e60-9a11-bfe5945a09cb",
                      "headerName": "Container",
                      "widgetName": "Container",
                      "widgetSetting": {
                        "decoration": {"boxFit": "fill", "color": "0xFF2E292D"},
                        "child": {
                          "id": "afefd28b-36da-4bbe-85ae-ba85f6d1a01b",
                          "headerName": "Padding",
                          "widgetName": "Padding",
                          "widgetSetting": {
                            "child": {
                              "id": "f16a7ade-f27e-44ed-992d-11d482b8ebf3",
                              "headerName": "Column",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "children": [
                                  {
                                    "id":
                                        "1cef16b9-a0cf-4878-a285-d75a39ab46b4",
                                    "headerName": "Row",
                                    "widgetName": "Row",
                                    "widgetSetting": {
                                      "textRightToLeft": "false",
                                      "children": [
                                        {
                                          "id":
                                              "d2e9cdcc-d4cb-4811-981e-8d01f430624a",
                                          "headerName": "CustomContainer",
                                          "widgetName": "CustomContainer",
                                          "widgetSetting": {
                                            "opacity": "1",
                                            "fit": "cover",
                                            "child": {},
                                            "height": "8.h",
                                            "width": "8.w",
                                            "image":
                                                "http://foodiemenu.co/wp-content/uploads/2023/09/white-globe-icon-24-2.png",
                                            "color": "0xFFA76744",
                                            "blendMode": "modulate"
                                          }
                                        },
                                        {
                                          "id":
                                              "727424f8-1241-48e5-8532-cb14f1a64ae6",
                                          "headerName": "Text",
                                          "widgetName": "Text",
                                          "widgetSetting": {
                                            "textAlgin": "start",
                                            "style": {
                                              "fontSize": "2.5.h",
                                              "fontWeight": "w400",
                                              "fontFamily": "DM Serif Display",
                                              "color": "0xFFFFFFFF"
                                            },
                                            "data": "WWW.SALON.WWW"
                                          }
                                        },
                                        {
                                          "id":
                                              "5b3b599f-220a-47c1-b471-1b0b48b67a0f",
                                          "headerName": "SizedBox",
                                          "widgetName": "SizedBox",
                                          "widgetSetting": {
                                            "child": {},
                                            "width": "5.w"
                                          }
                                        },
                                        {
                                          "id":
                                              "f16978ed-95dc-4fc0-8947-3518eb525ebb",
                                          "headerName": "CustomContainer",
                                          "widgetName": "CustomContainer",
                                          "widgetSetting": {
                                            "opacity": "1",
                                            "fit": "cover",
                                            "child": {},
                                            "height": "6.h",
                                            "width": "6.w",
                                            "image":
                                                "http://foodiemenu.co/wp-content/uploads/2023/09/pinpng.com-contact-icon-png-666849.png",
                                            "color": "0xFFA76744",
                                            "blendMode": "modulate"
                                          }
                                        },
                                        {
                                          "id":
                                              "19edd77b-b34f-4697-97e3-7de228da6b17",
                                          "headerName": "Text",
                                          "widgetName": "Text",
                                          "widgetSetting": {
                                            "textAlgin": "start",
                                            "style": {
                                              "fontSize": "2.5.h",
                                              "fontWeight": "w400",
                                              "fontFamily": "DM Serif Display",
                                              "color": "0xFFFFFFFF"
                                            },
                                            "data": "+1223 334 444"
                                          }
                                        }
                                      ]
                                    }
                                  }
                                ],
                                "mainAxisAlignment": "spaceAround",
                                "crossAxisAlignment": "start"
                              }
                            },
                            "padding": {
                              "top": "2.h",
                              "bottom": "2.h",
                              "left": "5.w"
                            }
                          }
                        },
                        "height": "20.h"
                      }
                    }
                  ]
                }
              },
              {
                "id": "f2326dd4-d3fe-49c2-8257-f88e3ac560b9",
                "headerName": "Padding",
                "widgetName": "Padding",
                "widgetSetting": {
                  "child": {
                    "id": "7af78992-62ea-4209-9170-9687dc9ceeee",
                    "headerName": "Row",
                    "widgetName": "Row",
                    "widgetSetting": {
                      "textRightToLeft": "false",
                      "children": [
                        {
                          "id": "31456fb3-6724-4c42-a28e-9a10cf19b130",
                          "headerName": "Expanded",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "flex": "1",
                            "child": {
                              "id": "b8c0790a-6979-4dd4-9ca9-88943bd8f26d",
                              "headerName": "Center",
                              "widgetName": "Center",
                              "widgetSetting": {
                                "child": {
                                  "id": "49073af5-d223-4536-b021-3867d031dc47",
                                  "headerName": "Column",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "5b478f40-91a3-4fb6-978d-b8f314d5b4fd",
                                        "headerName": "SizedBox",
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {
                                          "child": {},
                                          "height": "3.h"
                                        }
                                      },
                                      {
                                        "id":
                                            "791596b7-96a6-4d9d-9b4b-274730f81136",
                                        "headerName": "Text",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "textAlgin": "start",
                                          "style": {
                                            "fontSize": "5.h",
                                            "fontWeight": "w700",
                                            "color": "0xFF000000"
                                          },
                                          "data": "BEAUTY SERVICES"
                                        }
                                      },
                                      {
                                        "id":
                                            "87a18139-a6bb-4684-8755-6355ab68e14d",
                                        "headerName": "SizedBox",
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {
                                          "child": {},
                                          "height": "2.h"
                                        }
                                      },
                                      {
                                        "id":
                                            "11d1a5f5-3a15-4573-8b82-21659d84fb98",
                                        "headerName": "Wrap",
                                        "widgetName": "Wrap",
                                        "widgetSetting": {
                                          "clipBehavior": "none",
                                          "children": [
                                            {
                                              "id":
                                                  "9ddd76bb-21fa-43f3-9bf0-32af9fb1d375",
                                              "headerName":
                                                  "ServiceItemCustom1",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "toUpperCaseNameProductName":
                                                    "false",
                                                "useMediumStyle": "true",
                                                "maxLineProductName": "2",
                                                "fontWeight": "w400",
                                                "productId":
                                                    "a7aed668-20db-4e34-987e-2ffbf8f46f99",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "colorDescription":
                                                    "0xFF000000",
                                                "width": "23.w"
                                              }
                                            },
                                            {
                                              "id":
                                                  "014d3f22-fdf6-4faf-be53-6a0852d59492",
                                              "headerName":
                                                  "ServiceItemCustom1",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "toUpperCaseNameProductName":
                                                    "false",
                                                "useMediumStyle": "true",
                                                "maxLineProductName": "2",
                                                "fontWeight": "w400",
                                                "productId":
                                                    "0691c940-d270-4f9f-80d6-22471368ead2",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "colorDescription":
                                                    "0xFF000000",
                                                "width": "23.w"
                                              }
                                            },
                                            {
                                              "id":
                                                  "25e7b7a8-135b-453d-85ee-1d4d690cbb4e",
                                              "headerName":
                                                  "ServiceItemCustom1",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "toUpperCaseNameProductName":
                                                    "false",
                                                "useMediumStyle": "true",
                                                "maxLineProductName": "2",
                                                "fontWeight": "w400",
                                                "productId":
                                                    "fbb9ef0f-960d-4be7-b331-3236483a6c0a",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "colorDescription":
                                                    "0xFF000000",
                                                "width": "23.w"
                                              }
                                            },
                                            {
                                              "id":
                                                  "a6190051-69b5-4dd6-9ec2-25a38fe71fc1",
                                              "headerName":
                                                  "ServiceItemCustom1",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "toUpperCaseNameProductName":
                                                    "false",
                                                "useMediumStyle": "true",
                                                "maxLineProductName": "2",
                                                "fontWeight": "w400",
                                                "productId":
                                                    "d2f5a95c-7558-47a6-a536-59ad3e013564",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "colorDescription":
                                                    "0xFF000000",
                                                "width": "23.w"
                                              }
                                            },
                                            {
                                              "id":
                                                  "8d77336c-698a-4d19-9d69-b197c58ea573",
                                              "headerName":
                                                  "ServiceItemCustom1",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "toUpperCaseNameProductName":
                                                    "false",
                                                "useMediumStyle": "true",
                                                "maxLineProductName": "2",
                                                "fontWeight": "w400",
                                                "productId":
                                                    "85eaf1c0-34e8-4b7f-ad27-0dac4def3641",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "colorDescription":
                                                    "0xFF000000",
                                                "width": "23.w"
                                              }
                                            },
                                            {
                                              "id":
                                                  "702c03c5-ac14-425a-a274-d252aac07bdb",
                                              "headerName":
                                                  "ServiceItemCustom1",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "toUpperCaseNameProductName":
                                                    "false",
                                                "useMediumStyle": "true",
                                                "maxLineProductName": "2",
                                                "fontWeight": "w400",
                                                "productId":
                                                    "3bf08934-7160-4d43-8e2d-e7f87ba83e66",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "colorDescription":
                                                    "0xFF000000",
                                                "width": "23.w"
                                              }
                                            },
                                            {
                                              "id":
                                                  "97ac12e7-85dc-4030-a03f-e546b661acca",
                                              "headerName":
                                                  "ServiceItemCustom1",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "toUpperCaseNameProductName":
                                                    "false",
                                                "useMediumStyle": "true",
                                                "maxLineProductName": "2",
                                                "fontWeight": "w400",
                                                "productId":
                                                    "6569270a-7afa-490b-8e02-dd706b3167cb",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "colorDescription":
                                                    "0xFF000000",
                                                "width": "23.w"
                                              }
                                            },
                                            {
                                              "id":
                                                  "c1f655e5-9900-4c7b-a98c-5434643d8047",
                                              "headerName":
                                                  "ServiceItemCustom1",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "toUpperCaseNameProductName":
                                                    "false",
                                                "useMediumStyle": "true",
                                                "maxLineProductName": "2",
                                                "fontWeight": "w400",
                                                "productId":
                                                    "7aa5e661-8518-4183-b67f-95ed17fd0599",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "colorDescription":
                                                    "0xFF000000",
                                                "width": "23.w"
                                              }
                                            }
                                          ],
                                          "runSpacing": "4.h",
                                          "spacing": "2.w"
                                        }
                                      }
                                    ],
                                    "crossAxisAlignment": "start"
                                  }
                                }
                              }
                            }
                          }
                        },
                        {
                          "id": "6b1abe87-5460-448b-9ffc-854ffe1ba173",
                          "headerName": "SizedBox",
                          "widgetName": "SizedBox",
                          "widgetSetting": {
                            "child": {
                              "id": "434dc347-5cac-4216-b926-50a1f011ef8c",
                              "headerName": "Column",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "children": [
                                  {
                                    "id":
                                        "25f5f1ad-caaf-4b1e-ad90-0d883e706b43",
                                    "headerName": "SizedBox",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "child": {},
                                      "height": "5.h"
                                    }
                                  },
                                  {
                                    "id":
                                        "32c4c712-0e2b-4377-860b-f7f08bab0a24",
                                    "headerName": "SizedBox",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "cdceaaed-cc20-420c-9990-e922f4c3d57a",
                                        "headerName": "Row",
                                        "widgetName": "Row",
                                        "widgetSetting": {
                                          "textRightToLeft": "false",
                                          "children": [
                                            {
                                              "id":
                                                  "5ae303f1-2d19-4ee3-b775-7f8a1e965c3e",
                                              "headerName": "Expanded",
                                              "widgetName": "Expanded",
                                              "widgetSetting": {
                                                "flex": "1",
                                                "child": {
                                                  "id":
                                                      "390bd972-0909-46bb-af16-f58e79289841",
                                                  "headerName": "Column",
                                                  "widgetName": "Column",
                                                  "widgetSetting": {
                                                    "children": [
                                                      {
                                                        "id":
                                                            "011b9cb8-10eb-4a25-9f25-b9a24c55ca35",
                                                        "headerName": "Text",
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "textAlign": "left",
                                                          "style": {
                                                            "fontSize": "3.5.h",
                                                            "fontWeight":
                                                                "w700",
                                                            "fontFamily":
                                                                "DM Serif Display",
                                                            "color":
                                                                "0xFFA76744"
                                                          },
                                                          "data": "Our Missions"
                                                        }
                                                      },
                                                      {
                                                        "id":
                                                            "d5324815-e383-445d-9a5d-882449637722",
                                                        "headerName": "Text",
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "textAlign": "left",
                                                          "style": {
                                                            "fontSize": "2.h",
                                                            "fontWeight":
                                                                "w400",
                                                            "fontFamily":
                                                                "DM Serif Text",
                                                            "color":
                                                                "0xFF000000"
                                                          },
                                                          "data":
                                                              "Lorem ipsum dolor sit amet consectetur, Lorem ipsum dolor sit amet, consectetur, Lorem ipsum dolor sit amet, consectetur"
                                                        }
                                                      }
                                                    ],
                                                    "crossAxisAlignment":
                                                        "start"
                                                  }
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "1db69782-8f06-430a-98cd-f3fe77911942",
                                              "headerName": "SizedBox",
                                              "widgetName": "SizedBox",
                                              "widgetSetting": {
                                                "child": {},
                                                "width": "4.w"
                                              }
                                            },
                                            {
                                              "id":
                                                  "c4f1a549-e8af-44a6-bab4-6e7b7cd71a6f",
                                              "headerName": "Expanded",
                                              "widgetName": "Expanded",
                                              "widgetSetting": {
                                                "flex": "1",
                                                "child": {
                                                  "id":
                                                      "ee1e196c-d01f-4d22-9b82-efbfad06f9e7",
                                                  "headerName": "Column",
                                                  "widgetName": "Column",
                                                  "widgetSetting": {
                                                    "children": [
                                                      {
                                                        "id":
                                                            "ce102af5-5d25-4c68-ba9c-320c05340c79",
                                                        "headerName": "Text",
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "textAlign": "left",
                                                          "style": {
                                                            "fontSize": "3.5.h",
                                                            "fontWeight":
                                                                "w700",
                                                            "fontFamily":
                                                                "DM Serif Display",
                                                            "color":
                                                                "0xFFA76744"
                                                          },
                                                          "data": "Our Missions"
                                                        }
                                                      },
                                                      {
                                                        "id":
                                                            "26a557e3-2106-428c-bc09-857fe63c307c",
                                                        "headerName": "Text",
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "textAlign": "left",
                                                          "style": {
                                                            "fontSize": "2.h",
                                                            "fontWeight":
                                                                "w400",
                                                            "fontFamily":
                                                                "DM Serif Text",
                                                            "color":
                                                                "0xFF000000"
                                                          },
                                                          "data":
                                                              "Lorem ipsum dolor sit amet con sectetur, , Lorem ipsum dolor sit amet, consectetur, Lorem ipsum dolor sit amet, consectetur"
                                                        }
                                                      }
                                                    ],
                                                    "crossAxisAlignment":
                                                        "start"
                                                  }
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "09483e0d-ff43-477e-aeb1-f5312f0c7a4b",
                                              "headerName": "SizedBox",
                                              "widgetName": "SizedBox",
                                              "widgetSetting": {
                                                "child": {},
                                                "width": "4.w"
                                              }
                                            }
                                          ],
                                          "mainAxisAlignment": "spaceBetween"
                                        }
                                      },
                                      "height": "20.h"
                                    }
                                  },
                                  {
                                    "id":
                                        "8d5a4bd5-f720-4c52-aada-4803c6e9f990",
                                    "headerName": "SizedBox",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "ce8cd71d-f1e3-4995-9f1c-e0374951547f",
                                        "headerName": "Stack",
                                        "widgetName": "Stack",
                                        "widgetSetting": {
                                          "children": [
                                            {
                                              "id":
                                                  "f3fbe486-6309-4232-a8d6-23def6e3b5fb",
                                              "headerName": "CustomContainer",
                                              "widgetName": "CustomContainer",
                                              "widgetSetting": {
                                                "opacity": "1",
                                                "fit": "contain",
                                                "child": {},
                                                "height": "50.h",
                                                "alignment": "topLeft",
                                                "image":
                                                    "https://foodiemenu.co/wp-content/uploads/2023/10/outline-vector-flower-illustration-corner-border-frame-floral-elements-coloring-page-outline-vector-flower-illustration-189504420-e1696476093637.webp",
                                                "color": "0xFFA76744",
                                                "blendMode": "color"
                                              }
                                            },
                                            {
                                              "id":
                                                  "4078ceec-a90c-4656-8536-0554c4276736",
                                              "headerName": "Container",
                                              "widgetName": "Container",
                                              "widgetSetting": {
                                                "decoration": {
                                                  "boxFit": "fill",
                                                  "color": "0xCCFFFFFF"
                                                },
                                                "child": {},
                                                "height": "50.h"
                                              }
                                            },
                                            {
                                              "id":
                                                  "621a2836-aab8-4f3d-b1b3-87a5a1ac70a0",
                                              "headerName": "Positioned",
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "b33ef2bb-c186-46bd-b613-056d75ff92ef",
                                                  "headerName": "Container",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "decoration": {
                                                      "boxFit": "fill",
                                                      "color": "0xFFA76744"
                                                    },
                                                    "child": {},
                                                    "height": "50.h",
                                                    "width": "30.w"
                                                  }
                                                },
                                                "right": "0.w",
                                                "bottom": "0.h"
                                              }
                                            },
                                            {
                                              "id":
                                                  "071919a9-3547-4ceb-b4c9-ff048eb51ed5",
                                              "headerName": "Positioned",
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "4ede0dbe-7d27-45e8-88f6-356823dad9c7",
                                                  "headerName": "Container",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "decoration": {
                                                      "boxFit": "fill"
                                                    },
                                                    "child": {
                                                      "id":
                                                          "1b447814-aece-4086-9e7c-7620a6be8bf0",
                                                      "headerName": "Center",
                                                      "widgetName": "Center",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "id":
                                                              "1cd7b7ca-d280-46d1-8585-a7dd942747c7",
                                                          "headerName": "Text",
                                                          "widgetName": "Text",
                                                          "widgetSetting": {
                                                            "textAlgin":
                                                                "start",
                                                            "style": {
                                                              "fontSize":
                                                                  "2.5.sp",
                                                              "fontWeight":
                                                                  "w400",
                                                              "fontFamily":
                                                                  "DM Serif Text",
                                                              "color":
                                                                  "0xFFFFFFFF"
                                                            },
                                                            "data":
                                                                "DISCOUNT OF\nTHE MONTH"
                                                          }
                                                        }
                                                      }
                                                    },
                                                    "height": "9.h",
                                                    "width": "10.w"
                                                  }
                                                },
                                                "top": "5.5.h",
                                                "right": "21.5.w"
                                              }
                                            },
                                            {
                                              "id":
                                                  "7a9a9402-b9e0-41f7-862a-a60c0c4ec5e8",
                                              "headerName": "Positioned",
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "b1e2f918-f664-496c-bed9-df61833e3111",
                                                  "headerName": "Container",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "decoration": {
                                                      "boxFit": "cover",
                                                      "image":
                                                          "fd07f166-25b0-48e9-b1db-62ae529d2596"
                                                    },
                                                    "child": {},
                                                    "height": "37.5.h",
                                                    "width": "40.w"
                                                  }
                                                },
                                                "right": "0.w",
                                                "bottom": "2.5.h"
                                              }
                                            },
                                            {
                                              "id":
                                                  "ec48cacd-794e-46d9-b406-a185aeed19a6",
                                              "headerName": "Positioned",
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "a8ca83be-94f7-4dfc-b094-c187d6e85eb6",
                                                  "headerName": "Container",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "decoration": {
                                                      "boxFit": "fill",
                                                      "color": "0xFFFFFFFF"
                                                    },
                                                    "child": {
                                                      "id":
                                                          "7fde5330-12bb-4117-a597-58c252588af3",
                                                      "headerName": "Center",
                                                      "widgetName": "Center",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "id":
                                                              "a71b4d8e-406d-4b83-8bac-0aaf8e53ce78",
                                                          "headerName": "Text",
                                                          "widgetName": "Text",
                                                          "widgetSetting": {
                                                            "textAlgin":
                                                                "start",
                                                            "style": {
                                                              "fontSize": "6.h",
                                                              "fontWeight":
                                                                  "w700",
                                                              "fontFamily":
                                                                  "DM Serif Display",
                                                              "color":
                                                                  "0xFFA76744"
                                                            },
                                                            "data": "50% OFF"
                                                          }
                                                        }
                                                      }
                                                    },
                                                    "height": "9.h",
                                                    "width": "23.w"
                                                  }
                                                },
                                                "top": "5.5.h",
                                                "right": "0.w"
                                              }
                                            }
                                          ]
                                        }
                                      },
                                      "height": "55.h"
                                    }
                                  }
                                ]
                              }
                            },
                            "width": "45.w"
                          }
                        }
                      ]
                    }
                  },
                  "padding": {"top": "5.h"}
                }
              }
            ]
          }
        },
        "height": "100.h",
        "width": "100.w"
      }
    }
  };

  static final template11 = {
    "_id": "64ee744398c3d4605b477abn",
    "templateSlug": "temp_pho_1",
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "21/9": {
          "categoryImageHeight": "25.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.sp",
          "productPriceFontSize": "2.sp",
          "normalFontSize": "2.sp",
          "brandFontSize": "10.sp",
          "verticalFramePadding": "4.sp",
          "horizontalFramePadding": "4.sp",
        },
        "16/9": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "16/10": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "5.4.sp",
          "productNameFontSize": "3.5.sp",
          "productPriceFontSize": "3.5.sp",
          "normalFontSize": "4.8.sp",
          "brandFontSize": "18.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "4/3": {
          "categoryImageHeight": "45.sp",
          "categoryFontSize": "7.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp",
          "normalFontSize": "5.8.sp",
          "brandFontSize": "22.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "default": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
      },
    },
    "dataSetting": {
      "e1f6d5f0-e0c0-405b-b503-d46a8f05cfeb": {
        "id": "e1f6d5f0-e0c0-405b-b503-d46a8f05cfeb",
        "type": "category",
        "name": "Category 1",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1.jpg",
        "description": ""
      },
      "e1f6d5f0-e0c0-405b-b503-d46a8f05cfec": {
        "id": "e1f6d5f0-e0c0-405b-b503-d46a8f05cfeb",
        "type": "category",
        "name": "Category 2",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2.jpg",
        "description": ""
      },
      "e1f6d5f0-e0c0-405b-b503-d46a8f05cfed": {
        "id": "e1f6d5f0-e0c0-405b-b503-d46a8f05cfeb",
        "type": "category",
        "name": "Category 3",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3.jpg",
        "description": ""
      },
      "e1f6d5f0-e0c0-405b-b503-d46a8f05cfee": {
        "id": "e1f6d5f0-e0c0-405b-b503-d46a8f05cfeb",
        "type": "category",
        "name": "Category 4",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_4.jpg",
        "description": ""
      },
      "b3a48792-edc5-4ce3-ae89-02384d38dada": {
        "id": "b3a48792-edc5-4ce3-ae89-02384d38dada",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "043f353f-5ae7-4a65-9c61-ba02abc4adc3": {
        "id": "043f353f-5ae7-4a65-9c61-ba02abc4adc3",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "30bd6052-e293-46b4-bb74-cd0227714dc6": {
        "id": "30bd6052-e293-46b4-bb74-cd0227714dc6",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "828435c1-63fe-4250-9b2e-5b6e29554b21": {
        "id": "828435c1-63fe-4250-9b2e-5b6e29554b21",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "b43b4e59-e460-473b-a122-01bd4153866d": {
        "id": "b43b4e59-e460-473b-a122-01bd4153866d",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "44558621-07c1-491e-9ccb-38c1dbc18802": {
        "id": "44558621-07c1-491e-9ccb-38c1dbc18802",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "f047df88-0c7b-4627-aa30-72c4126657c7": {
        "id": "f047df88-0c7b-4627-aa30-72c4126657c7",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      }
    },
    "widgets_setting": {
      "id": "f7469f67-be41-4072-b0aa-b3357efe1326",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"boxFit": "fill", "color": "0xffffffff"},
        "child": {
          "id": "2d35117f-f2fa-4bc4-8a15-8e5f0ec379a9",
          "headerName": "PaddingByRatio",
          "widgetName": "PaddingByRatio",
          "widgetSetting": {
            "child": {
              "id": "bbb77651-4b18-47b5-a59f-4dd08e211dd6",
              "headerName": "Center",
              "widgetName": "Center",
              "widgetSetting": {
                "child": {
                  "id": "3bb31b4e-ef59-452c-ac51-bca6b9210de3",
                  "headerName": "Column",
                  "widgetName": "Column",
                  "widgetSetting": {
                    "children": [
                      {
                        "id": "3a617e0b-9cf1-412c-9eb0-a09b82e426af",
                        "headerName": "Container",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "decoration": {"boxFit": "fill"},
                          "child": {
                            "id": "6b7e8daa-1408-4d5c-9e04-3e963d7641ff",
                            "headerName": "Row",
                            "widgetName": "Row",
                            "widgetSetting": {
                              "textRightToLeft": "false",
                              "children": [
                                {
                                  "id": "e8889084-3b25-4ada-af4d-3eb340a61844",
                                  "headerName": "Text",
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "textAlign": "start",
                                    "style": {
                                      "fontWeight": "w400",
                                      "color": "0xff404042"
                                    },
                                    "data": "4889 Princess St",
                                    "fontSizeFromFieldKey": "normalFontSize"
                                  }
                                },
                                {
                                  "id": "b78b56b7-7264-4df3-bf99-4e2f4c2cb1af",
                                  "headerName": "Text",
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "textAlign": "start",
                                    "style": {
                                      "fontWeight": "w400",
                                      "color": "0xff404042"
                                    },
                                    "data": "EST 1990",
                                    "fontSizeFromFieldKey": "normalFontSize"
                                  }
                                }
                              ],
                              "mainAxisAlignment": "spaceBetween"
                            }
                          },
                          "padding": {"vertical": "2.sp"}
                        }
                      },
                      {
                        "id": "62ba836e-18cd-4dad-b0e0-d9212fbffb3f",
                        "headerName": "Container",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "decoration": {
                            "boxFit": "fill",
                            "border": {"width": "0.1.sp", "color": "0xff404042"}
                          },
                          "child": {},
                          "height": "1"
                        }
                      },
                      {
                        "id": "f5613d71-c037-4083-964f-ec44801083f3",
                        "headerName": "UnderlineBrand",
                        "widgetName": "UnderlineBrand",
                        "widgetSetting": {
                          "textStyle": {
                            "fontWeight": "w700",
                            "color": "0xff404042"
                          },
                          "brand": "PHO BRAND",
                          "fontSizeFromFieldKey": "brandFontSize",
                          "bottomBorderSide": {
                            "width": "1.8.sp",
                            "color": "0xff404042 "
                          }
                        }
                      },
                      {
                        "id": "5a73dc51-96bf-48af-9a26-9e8f6d2faa33",
                        "headerName": "SizedBox",
                        "widgetName": "SizedBox",
                        "widgetSetting": {"child": {}, "height": "2.h"}
                      },
                      {
                        "id": "eb887491-ffc0-455e-a06f-cca00ddd7d5b",
                        "headerName": "SizedBox",
                        "widgetName": "SizedBox",
                        "widgetSetting": {
                          "child": {
                            "id": "fa89c13e-d9bc-4b6b-b87e-6e27d9317537",
                            "headerName": "Row",
                            "widgetName": "Row",
                            "widgetSetting": {
                              "textRightToLeft": "false",
                              "children": [
                                {
                                  "id": "d5ab4b37-19d0-4688-a70f-d1b538a77715",
                                  "headerName": "Expanded",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "flex": "1",
                                    "child": {
                                      "id":
                                          "a17e3778-74f8-484d-b5fe-5a184b63a66e",
                                      "headerName": "CategoryColumn1",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000"
                                        },
                                        "upperCaseCategoryName": "true",
                                        "productRows": [
                                          {
                                            "id":
                                                "fcb3eba7-1a20-4942-ba14-fa8e1333682f",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "b3a48792-edc5-4ce3-ae89-02384d38dada",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "ffe66c40-9b36-4cf2-879a-93e140f6951c",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "043f353f-5ae7-4a65-9c61-ba02abc4adc3",
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "af998b44-df96-46b5-a774-985e031fb2f7",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "30bd6052-e293-46b4-bb74-cd0227714dc6",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "cf55e19d-12f6-469b-8815-0a56d506cedc",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "828435c1-63fe-4250-9b2e-5b6e29554b21",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "6d282712-656f-4a75-a0a9-9a9cfbdefaf1",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "b43b4e59-e460-473b-a122-01bd4153866d",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "a8dcf4a4-6623-4c6c-9c7b-406d5694ab8f",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "44558621-07c1-491e-9ccb-38c1dbc18802",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "e53a4b65-4a29-4503-87c7-22eef94f2cbf",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "f047df88-0c7b-4627-aa30-72c4126657c7",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          }
                                        ],
                                        "category":
                                            "e1f6d5f0-e0c0-405b-b503-d46a8f05cfeb",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize"
                                      }
                                    }
                                  }
                                },
                                {
                                  "id": "4f91ea78-2e64-45ef-8f60-d3095b0a88ea",
                                  "headerName": "SizedBox",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"child": {}, "width": "2.w"}
                                },
                                {
                                  "id": "d5ab4b37-19d0-4688-a70f-d1b538a77716",
                                  "headerName": "Expanded",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "flex": "1",
                                    "child": {
                                      "id":
                                          "a17e3778-74f8-484d-b5fe-5a184b63a66f",
                                      "headerName": "CategoryColumn1",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000"
                                        },
                                        "upperCaseCategoryName": "true",
                                        "productRows": [
                                          {
                                            "id":
                                                "fcb3eba7-1a20-4942-ba14-fa8e1333682g",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "b3a48792-edc5-4ce3-ae89-02384d38dada",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "ffe66c40-9b36-4cf2-879a-93e140f6951d",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "043f353f-5ae7-4a65-9c61-ba02abc4adc3",
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "af998b44-df96-46b5-a774-985e031fb2f8",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "30bd6052-e293-46b4-bb74-cd0227714dc6",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "cf55e19d-12f6-469b-8815-0a56d506cedc",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "828435c1-63fe-4250-9b2e-5b6e29554b21",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "6d282712-656f-4a75-a0a9-9a9cfbdefaf1",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "b43b4e59-e460-473b-a122-01bd4153866d",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "a8dcf4a4-6623-4c6c-9c7b-406d5694ab8f",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "44558621-07c1-491e-9ccb-38c1dbc18802",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "e53a4b65-4a29-4503-87c7-22eef94f2cbf",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "f047df88-0c7b-4627-aa30-72c4126657c7",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          }
                                        ],
                                        "category":
                                            "e1f6d5f0-e0c0-405b-b503-d46a8f05cfec",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize"
                                      }
                                    }
                                  }
                                },
                                {
                                  "id": "4f91ea78-2e64-45ef-8f60-d3095b0a88ea",
                                  "headerName": "SizedBox",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"child": {}, "width": "2.w"}
                                },
                                {
                                  "id": "d5ab4b37-19d0-4688-a70f-d1b538a77715",
                                  "headerName": "Expanded",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "flex": "1",
                                    "child": {
                                      "id":
                                          "a17e3778-74f8-484d-b5fe-5a184b63a66e",
                                      "headerName": "CategoryColumn1",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000"
                                        },
                                        "upperCaseCategoryName": "true",
                                        "productRows": [
                                          {
                                            "id":
                                                "fcb3eba7-1a20-4942-ba14-fa8e1333682f",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "b3a48792-edc5-4ce3-ae89-02384d38dada",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "ffe66c40-9b36-4cf2-879a-93e140f6951c",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "043f353f-5ae7-4a65-9c61-ba02abc4adc3",
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "af998b44-df96-46b5-a774-985e031fb2f7",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "30bd6052-e293-46b4-bb74-cd0227714dc6",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "cf55e19d-12f6-469b-8815-0a56d506cedc",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "828435c1-63fe-4250-9b2e-5b6e29554b21",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "6d282712-656f-4a75-a0a9-9a9cfbdefaf1",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "b43b4e59-e460-473b-a122-01bd4153866d",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "a8dcf4a4-6623-4c6c-9c7b-406d5694ab8f",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "44558621-07c1-491e-9ccb-38c1dbc18802",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "e53a4b65-4a29-4503-87c7-22eef94f2cbf",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "f047df88-0c7b-4627-aa30-72c4126657c7",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          }
                                        ],
                                        "category":
                                            "e1f6d5f0-e0c0-405b-b503-d46a8f05cfed",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize"
                                      }
                                    }
                                  }
                                },
                                {
                                  "id": "4f91ea78-2e64-45ef-8f60-d3095b0a88ea",
                                  "headerName": "SizedBox",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"child": {}, "width": "2.w"}
                                },
                                {
                                  "id": "d5ab4b37-19d0-4688-a70f-d1b538a77715",
                                  "headerName": "Expanded",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "flex": "1",
                                    "child": {
                                      "id":
                                          "a17e3778-74f8-484d-b5fe-5a184b63a66e",
                                      "headerName": "CategoryColumn1",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000"
                                        },
                                        "upperCaseCategoryName": "true",
                                        "productRows": [
                                          {
                                            "id":
                                                "fcb3eba7-1a20-4942-ba14-fa8e1333682f",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "b3a48792-edc5-4ce3-ae89-02384d38dada",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "ffe66c40-9b36-4cf2-879a-93e140f6951c",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "043f353f-5ae7-4a65-9c61-ba02abc4adc3",
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "af998b44-df96-46b5-a774-985e031fb2f7",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "30bd6052-e293-46b4-bb74-cd0227714dc6",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "cf55e19d-12f6-469b-8815-0a56d506cedc",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "828435c1-63fe-4250-9b2e-5b6e29554b21",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "6d282712-656f-4a75-a0a9-9a9cfbdefaf1",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "b43b4e59-e460-473b-a122-01bd4153866d",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "a8dcf4a4-6623-4c6c-9c7b-406d5694ab8f",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "44558621-07c1-491e-9ccb-38c1dbc18802",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          },
                                          {
                                            "id":
                                                "e53a4b65-4a29-4503-87c7-22eef94f2cbf",
                                            "headerName": "ProductRow1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "fractionDigitsForPrice": "2",
                                              "maxLineOfProductName": "3",
                                              "prefixProductPrice": "\$",
                                              "subfixProductPrice": "",
                                              "upperCaseProductName": "true",
                                              "productNameTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "fontWeight": "w400",
                                                "color": "0xff000000"
                                              },
                                              "product":
                                                  "f047df88-0c7b-4627-aa30-72c4126657c7",
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              },
                                              "padding": {"vertical": "2.2.sp"}
                                            }
                                          }
                                        ],
                                        "category":
                                            "e1f6d5f0-e0c0-405b-b503-d46a8f05cfee",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize"
                                      }
                                    }
                                  }
                                },
                              ],
                              "mainAxisAlignment": "spaceBetween"
                            }
                          },
                          "width": "100.w"
                        }
                      }
                    ],
                    "mainAxisAlignment": "center"
                  }
                }
              }
            },
            "verticalPaddingFieldKey": "verticalFramePadding",
            "horizontalPaddingFieldKey": "horizontalFramePadding"
          }
        },
        "height": "100.h",
        "width": "100.w"
      }
    }
  };

  static final template12 = {
    "_id": "64ee744398c3d4605b477abo",
    "templateSlug": "temp_pho_2",
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "3.sp",
          "priceFontSize": "3.5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "100.sp",
          "imageWidth": "100.sp",
        },
        "21/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "7.sp",
          "productFontSize": "3.sp",
          "priceFontSize": "3.5.sp",
          "sloganFontSize": "5.sp",
          "categoryFontSize": "5.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "85.sp",
          "imageWidth": "85.sp",
        },
        "16/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "3.5.sp",
          "priceFontSize": "4.2.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "3.5.sp",
          "imageHeight": "120.sp",
          "imageWidth": "120.sp",
        },
        "16/10": {
          "categoryColumnVerticalPadding": "5.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "4.5.sp",
          "priceFontSize": "5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "4.2.sp",
          "imageHeight": "125.sp",
          "imageWidth": "125.sp",
        },
        "4/3": {
          "categoryColumnVerticalPadding": "12.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "5.sp",
          "priceFontSize": "5.5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "4.5.sp",
          "imageHeight": "130.sp",
          "imageWidth": "130.sp",
        },
        "default": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "7.sp",
          "productFontSize": "2.5.sp",
          "priceFontSize": "3.sp",
          "sloganFontSize": "5.sp",
          "categoryFontSize": "5.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "100.sp",
          "imageWidth": "100.sp",
        },
      },
    },
    "dataSetting": {
      "92cfb6f8-9ff9-4575-91a3-72c7ddc16796": {
        "id": "92cfb6f8-9ff9-4575-91a3-72c7ddc16796",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/background-1.jpg"
      },
      "bb2895c1-c4cf-4a07-959f-934a75ad06f1": {
        "id": "bb2895c1-c4cf-4a07-959f-934a75ad06f1",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_1.png"
      },
      "0234a24b-7d1f-47c4-8841-b4d08eade15e": {
        "id": "0234a24b-7d1f-47c4-8841-b4d08eade15e",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description": ""
      },
      "abc18a81-ef71-4c23-92be-5ba945e318e7": {
        "id": "abc18a81-ef71-4c23-92be-5ba945e318e7",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "3f44e492-3932-4aef-9ae9-a2439808b8d4": {
        "id": "3f44e492-3932-4aef-9ae9-a2439808b8d4",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "abc2f921-6728-4518-9849-dddbd6219904": {
        "id": "abc2f921-6728-4518-9849-dddbd6219904",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "1513f0ff-4763-45bf-8827-600e39336e42": {
        "id": "1513f0ff-4763-45bf-8827-600e39336e42",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "79059ccc-84a6-474b-acc7-1e3661758331": {
        "id": "79059ccc-84a6-474b-acc7-1e3661758331",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_4.png"
      },
      "3598cf38-35d9-4b04-ba34-6206f80c3995": {
        "id": "3598cf38-35d9-4b04-ba34-6206f80c3995",
        "type": "category",
        "name": "Category 2",
        "image": "",
        "description": ""
      },
      "a942f479-30de-4698-a8b9-bbbcdcab9c6d": {
        "id": "a942f479-30de-4698-a8b9-bbbcdcab9c6d",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      }
    },
    "widgets_setting": {
      "id": "d60bcd91-669e-4cbe-b488-c91938066966",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image": "92cfb6f8-9ff9-4575-91a3-72c7ddc16796",
          "boxFit": "cover"
        },
        "child": {
          "id": "010d1053-fc95-4985-9255-44bea4fb4089",
          "headerName": "Row",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                "id": "8c73643e-ea5f-4efc-8e91-ff88bc6b9968",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "f6a6cef7-c9d5-4d83-bf8a-33f04a06b4cb",
                    "headerName": "Stack",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "id": "060bd9ca-36fe-4c85-936f-6a4837a01ae9",
                          "headerName": "Positioned",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "child": {
                              "id": "34ed17a6-9b04-4586-b4c8-f91074bc122d",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "9ded1f57-a8eb-4101-842e-9e3a416e0ebc",
                                  "headerName": "SimpleImage",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "fit": "contain",
                                    "imageDataKey":
                                        "bb2895c1-c4cf-4a07-959f-934a75ad06f1"
                                  }
                                },
                                "widthFromFieldKey": "imageWidth",
                                "heightFromFieldKey": "imageHeight"
                              }
                            },
                            "bottom": "-15.sp",
                            "left": "-30.sp"
                          }
                        },
                        {
                          "id": "b8f7fe85-5d31-408d-aef9-3a369520eb75",
                          "headerName": "PaddingByRatio",
                          "widgetName": "PaddingByRatio",
                          "widgetSetting": {
                            "child": {
                              "id": "fa5b4140-c4be-4695-9be1-5a93981a1b92",
                              "headerName": "CategoryColumn1",
                              "widgetName": "CategoryColumn1",
                              "widgetSetting": {
                                "categoryTextStyle": {
                                  "fontWeight": "w700",
                                  "fontFamily": "Roboto Slab",
                                  "color": "0xffda2f15"
                                },
                                "upperCaseCategoryName": "true",
                                "productRows": [
                                  {
                                    "id":
                                        "803d71c5-de6f-4a4f-a311-8f8be2fb4314",
                                    "headerName": "Padding",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "16393ec4-8781-4a06-8a9e-03859d9a7a15",
                                        "headerName": "IndexedMenuItem1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "priceTextStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "textStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xffffffff",
                                            "letterSpacing": "1"
                                          },
                                          "index": "1",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "product":
                                              "3f44e492-3932-4aef-9ae9-a2439808b8d4",
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "elementGap": "2.2.sp"
                                        }
                                      },
                                      "padding": {"vertical": "1.sp"}
                                    }
                                  },
                                  {
                                    "id":
                                        "992b4868-6ab9-4ed2-9a13-f94e86ae7d74",
                                    "headerName": "Padding",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "42214207-a038-49d8-98b0-5b0e4fb91f7f",
                                        "headerName": "IndexedMenuItem1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "priceTextStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "textStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xffffffff",
                                            "letterSpacing": "1"
                                          },
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "product":
                                              "abc2f921-6728-4518-9849-dddbd6219904",
                                          "elementGap": "2.2.sp",
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "index": "2"
                                        }
                                      },
                                      "padding": {"vertical": "1.sp"}
                                    }
                                  },
                                  {
                                    "id":
                                        "992b4868-6ab9-4ed2-9a13-f94e86ae7d74",
                                    "headerName": "Padding",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "42214207-a038-49d8-98b0-5b0e4fb91f7f",
                                        "headerName": "IndexedMenuItem1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "priceTextStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "textStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xffffffff",
                                            "letterSpacing": "1"
                                          },
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "product":
                                              "abc2f921-6728-4518-9849-dddbd6219904",
                                          "elementGap": "2.2.sp",
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "index": "2"
                                        }
                                      },
                                      "padding": {"vertical": "1.sp"}
                                    }
                                  },
                                  {
                                    "id":
                                        "992b4868-6ab9-4ed2-9a13-f94e86ae7d74",
                                    "headerName": "Padding",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "42214207-a038-49d8-98b0-5b0e4fb91f7f",
                                        "headerName": "IndexedMenuItem1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "priceTextStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "textStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xffffffff",
                                            "letterSpacing": "1"
                                          },
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "product":
                                              "abc2f921-6728-4518-9849-dddbd6219904",
                                          "elementGap": "2.2.sp",
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "index": "2"
                                        }
                                      },
                                      "padding": {"vertical": "1.sp"}
                                    }
                                  },
                                  {
                                    "id":
                                        "992b4868-6ab9-4ed2-9a13-f94e86ae7d74",
                                    "headerName": "Padding",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "42214207-a038-49d8-98b0-5b0e4fb91f7f",
                                        "headerName": "IndexedMenuItem1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "priceTextStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "textStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xffffffff",
                                            "letterSpacing": "1"
                                          },
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "product":
                                              "abc2f921-6728-4518-9849-dddbd6219904",
                                          "elementGap": "2.2.sp",
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "index": "2"
                                        }
                                      },
                                      "padding": {"vertical": "1.sp"}
                                    }
                                  }
                                ],
                                "category":
                                    "0234a24b-7d1f-47c4-8841-b4d08eade15e",
                                "categoryFontSizeFromFieldKey":
                                    "categoryFontSize",
                                "crossAxisAlignment": "center"
                              }
                            },
                            "verticalPaddingFieldKey":
                                "categoryColumnVerticalPadding",
                            "horizontalPaddingFieldKey":
                                "categoryColumnHorizontalPadding"
                          }
                        }
                      ]
                    }
                  }
                }
              },
              {
                "id": "d80b7ba6-6908-459e-86a4-9a2b0ad502d2",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "c0353009-6ae6-43b9-a600-c7cb39f88f3f",
                    "headerName": "PaddingByRatio",
                    "widgetName": "PaddingByRatio",
                    "widgetSetting": {
                      "child": {
                        "id": "e982bd85-2f5f-425f-bf05-c37ff26cd98d",
                        "headerName": "MenuColumn1",
                        "widgetName": "MenuColumn1",
                        "widgetSetting": {
                          "menuItems": [
                            {
                              "id": "afae1c7a-e6a2-42ac-8d2f-9f86475508f0",
                              "headerName": "Padding",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "child": {
                                  "id": "58ff3674-e580-4eb3-ba5e-105cca8dcea4",
                                  "headerName": "IndexedMenuItem1",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "priceTextStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xfffc9100"
                                    },
                                    "textStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xffffffff",
                                      "letterSpacing": "1"
                                    },
                                    "index": "6",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "product":
                                        "1513f0ff-4763-45bf-8827-600e39336e42",
                                    "elementGap": "2.2.sp",
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff"
                                  }
                                },
                                "padding": {"vertical": "1.sp"}
                              }
                            },
                            {
                              "id": "afae1c7a-e6a2-42ac-8d2f-9f86475508f0",
                              "headerName": "Padding",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "child": {
                                  "id": "58ff3674-e580-4eb3-ba5e-105cca8dcea4",
                                  "headerName": "IndexedMenuItem1",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "priceTextStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xfffc9100"
                                    },
                                    "textStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xffffffff",
                                      "letterSpacing": "1"
                                    },
                                    "index": "6",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "product":
                                        "1513f0ff-4763-45bf-8827-600e39336e42",
                                    "elementGap": "2.2.sp",
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff"
                                  }
                                },
                                "padding": {"vertical": "1.sp"}
                              }
                            },
                            {
                              "id": "afae1c7a-e6a2-42ac-8d2f-9f86475508f0",
                              "headerName": "Padding",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "child": {
                                  "id": "58ff3674-e580-4eb3-ba5e-105cca8dcea4",
                                  "headerName": "IndexedMenuItem1",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "priceTextStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xfffc9100"
                                    },
                                    "textStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xffffffff",
                                      "letterSpacing": "1"
                                    },
                                    "index": "6",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "product":
                                        "1513f0ff-4763-45bf-8827-600e39336e42",
                                    "elementGap": "2.2.sp",
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff"
                                  }
                                },
                                "padding": {"vertical": "1.sp"}
                              }
                            },
                            {
                              "id": "afae1c7a-e6a2-42ac-8d2f-9f86475508f0",
                              "headerName": "Padding",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "child": {
                                  "id": "58ff3674-e580-4eb3-ba5e-105cca8dcea4",
                                  "headerName": "IndexedMenuItem1",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "priceTextStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xfffc9100"
                                    },
                                    "textStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xffffffff",
                                      "letterSpacing": "1"
                                    },
                                    "index": "6",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "product":
                                        "1513f0ff-4763-45bf-8827-600e39336e42",
                                    "elementGap": "2.2.sp",
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff"
                                  }
                                },
                                "padding": {"vertical": "1.sp"}
                              }
                            },
                            {
                              "id": "afae1c7a-e6a2-42ac-8d2f-9f86475508f0",
                              "headerName": "Padding",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "child": {
                                  "id": "58ff3674-e580-4eb3-ba5e-105cca8dcea4",
                                  "headerName": "IndexedMenuItem1",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "priceTextStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xfffc9100"
                                    },
                                    "textStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xffffffff",
                                      "letterSpacing": "1"
                                    },
                                    "index": "6",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "product":
                                        "1513f0ff-4763-45bf-8827-600e39336e42",
                                    "elementGap": "2.2.sp",
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff"
                                  }
                                },
                                "padding": {"vertical": "1.sp"}
                              }
                            },
                            {
                              "id": "afae1c7a-e6a2-42ac-8d2f-9f86475508f0",
                              "headerName": "Padding",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "child": {
                                  "id": "58ff3674-e580-4eb3-ba5e-105cca8dcea4",
                                  "headerName": "IndexedMenuItem1",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "priceTextStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xfffc9100"
                                    },
                                    "textStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xffffffff",
                                      "letterSpacing": "1"
                                    },
                                    "index": "6",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "product":
                                        "1513f0ff-4763-45bf-8827-600e39336e42",
                                    "elementGap": "2.2.sp",
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff"
                                  }
                                },
                                "padding": {"vertical": "1.sp"}
                              }
                            },
                            {
                              "id": "afae1c7a-e6a2-42ac-8d2f-9f86475508f0",
                              "headerName": "Padding",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "child": {
                                  "id": "58ff3674-e580-4eb3-ba5e-105cca8dcea4",
                                  "headerName": "IndexedMenuItem1",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "priceTextStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xfffc9100"
                                    },
                                    "textStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xffffffff",
                                      "letterSpacing": "1"
                                    },
                                    "index": "6",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "product":
                                        "1513f0ff-4763-45bf-8827-600e39336e42",
                                    "elementGap": "2.2.sp",
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff"
                                  }
                                },
                                "padding": {"vertical": "1.sp"}
                              }
                            },
                            {
                              "id": "afae1c7a-e6a2-42ac-8d2f-9f86475508f0",
                              "headerName": "Padding",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "child": {
                                  "id": "58ff3674-e580-4eb3-ba5e-105cca8dcea4",
                                  "headerName": "IndexedMenuItem1",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "priceTextStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xfffc9100"
                                    },
                                    "textStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xffffffff",
                                      "letterSpacing": "1"
                                    },
                                    "index": "6",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "product":
                                        "1513f0ff-4763-45bf-8827-600e39336e42",
                                    "elementGap": "2.2.sp",
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff"
                                  }
                                },
                                "padding": {"vertical": "1.sp"}
                              }
                            },
                            {
                              "id": "afae1c7a-e6a2-42ac-8d2f-9f86475508f0",
                              "headerName": "Padding",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "child": {
                                  "id": "58ff3674-e580-4eb3-ba5e-105cca8dcea4",
                                  "headerName": "IndexedMenuItem1",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "priceTextStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xfffc9100"
                                    },
                                    "textStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xffffffff",
                                      "letterSpacing": "1"
                                    },
                                    "index": "6",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "product":
                                        "1513f0ff-4763-45bf-8827-600e39336e42",
                                    "elementGap": "2.2.sp",
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff"
                                  }
                                },
                                "padding": {"vertical": "1.sp"}
                              }
                            },
                            {
                              "id": "afae1c7a-e6a2-42ac-8d2f-9f86475508f0",
                              "headerName": "Padding",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "child": {
                                  "id": "58ff3674-e580-4eb3-ba5e-105cca8dcea4",
                                  "headerName": "IndexedMenuItem1",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "priceTextStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xfffc9100"
                                    },
                                    "textStyle": {
                                      "fontWeight": "w700",
                                      "fontFamily": "openSans",
                                      "color": "0xffffffff",
                                      "letterSpacing": "1"
                                    },
                                    "index": "6",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "product":
                                        "1513f0ff-4763-45bf-8827-600e39336e42",
                                    "elementGap": "2.2.sp",
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff"
                                  }
                                },
                                "padding": {"vertical": "1.sp"}
                              }
                            }
                          ],
                          "brandTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Pacifico",
                            "color": "0xffe1b971"
                          },
                          "sloganTextStyle": {
                            "fontWeight": "w700",
                            "fontFamily": "Roboto Slab",
                            "color": "0xffda2f15"
                          },
                          "brand": "PHO BRAND",
                          "slogan": "Pho, noodle and more",
                          "brandFontSizeFromFieldKey": "brandFontSize",
                          "sloganFontSizeFromFieldKey": "sloganFontSize"
                        }
                      },
                      "verticalPaddingFieldKey":
                          "categoryColumnVerticalPadding",
                      "horizontalPaddingFieldKey":
                          "categoryColumnHorizontalPadding"
                    }
                  }
                }
              },
              {
                "id": "fb8a5747-8145-4425-8b61-3367b843599e",
                "headerName": "Expanded",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "3848a78b-9c97-4fe6-8286-ca897301b320",
                    "headerName": "Stack",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "id": "63e4ee03-696d-45ec-a874-5cabc4bf3836",
                          "headerName": "Positioned",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "child": {
                              "id": "9ec6b94c-b128-410e-908a-8e91da5d1218",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "4714344c-fc5f-414d-a1bf-3e3d5f5279be",
                                  "headerName": "SimpleImage",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "fit": "contain",
                                    "imageDataKey":
                                        "79059ccc-84a6-474b-acc7-1e3661758331"
                                  }
                                },
                                "widthFromFieldKey": "imageWidth",
                                "heightFromFieldKey": "imageHeight"
                              }
                            },
                            "top": "-15.sp",
                            "right": "-30.sp"
                          }
                        },
                        {
                          "id": "7407555a-f007-43da-b49f-514b4b1b4b09",
                          "headerName": "PaddingByRatio",
                          "widgetName": "PaddingByRatio",
                          "widgetSetting": {
                            "child": {
                              "id": "e7b2efaa-065c-47db-846a-1fa9d5166265",
                              "headerName": "CategoryColumn1",
                              "widgetName": "CategoryColumn1",
                              "widgetSetting": {
                                "categoryTextStyle": {
                                  "fontWeight": "w700",
                                  "fontFamily": "Roboto Slab",
                                  "color": "0xffda2f15"
                                },
                                "upperCaseCategoryName": "true",
                                "productRows": [
                                  {
                                    "id":
                                        "b20ac8e5-d73e-46f7-9cfa-8082fb4e32e0",
                                    "headerName": "Padding",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "265278dd-ec53-440a-9259-9dd50f5e2406",
                                        "headerName": "IndexedMenuItem1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "priceTextStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "textStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "index": "16",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "product":
                                              "a942f479-30de-4698-a8b9-bbbcdcab9c6d",
                                          "elementGap": "2.2.sp",
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff"
                                        }
                                      },
                                      "padding": {"vertical": "1.sp"}
                                    }
                                  },
                                  {
                                    "id":
                                        "b20ac8e5-d73e-46f7-9cfa-8082fb4e32e0",
                                    "headerName": "Padding",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "265278dd-ec53-440a-9259-9dd50f5e2406",
                                        "headerName": "IndexedMenuItem1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "priceTextStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "textStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "index": "16",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "product":
                                              "a942f479-30de-4698-a8b9-bbbcdcab9c6d",
                                          "elementGap": "2.2.sp",
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff"
                                        }
                                      },
                                      "padding": {"vertical": "1.sp"}
                                    }
                                  },
                                  {
                                    "id":
                                        "b20ac8e5-d73e-46f7-9cfa-8082fb4e32e0",
                                    "headerName": "Padding",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "265278dd-ec53-440a-9259-9dd50f5e2406",
                                        "headerName": "IndexedMenuItem1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "priceTextStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "textStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "index": "16",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "product":
                                              "a942f479-30de-4698-a8b9-bbbcdcab9c6d",
                                          "elementGap": "2.2.sp",
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff"
                                        }
                                      },
                                      "padding": {"vertical": "1.sp"}
                                    }
                                  },
                                  {
                                    "id":
                                        "b20ac8e5-d73e-46f7-9cfa-8082fb4e32e0",
                                    "headerName": "Padding",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "265278dd-ec53-440a-9259-9dd50f5e2406",
                                        "headerName": "IndexedMenuItem1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "priceTextStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "textStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "index": "16",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "product":
                                              "a942f479-30de-4698-a8b9-bbbcdcab9c6d",
                                          "elementGap": "2.2.sp",
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff"
                                        }
                                      },
                                      "padding": {"vertical": "1.sp"}
                                    }
                                  },
                                  {
                                    "id":
                                        "b20ac8e5-d73e-46f7-9cfa-8082fb4e32e0",
                                    "headerName": "Padding",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "265278dd-ec53-440a-9259-9dd50f5e2406",
                                        "headerName": "IndexedMenuItem1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "priceTextStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "textStyle": {
                                            "fontWeight": "w700",
                                            "fontFamily": "openSans",
                                            "color": "0xfffc9100"
                                          },
                                          "index": "16",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "product":
                                              "a942f479-30de-4698-a8b9-bbbcdcab9c6d",
                                          "elementGap": "2.2.sp",
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff"
                                        }
                                      },
                                      "padding": {"vertical": "1.sp"}
                                    }
                                  }
                                ],
                                "category":
                                    "3598cf38-35d9-4b04-ba34-6206f80c3995",
                                "categoryFontSizeFromFieldKey":
                                    "categoryFontSize",
                                "mainAxisAlignment": "end",
                                "crossAxisAlignment": "center"
                              }
                            },
                            "verticalPaddingFieldKey":
                                "categoryColumnVerticalPadding",
                            "horizontalPaddingFieldKey":
                                "categoryColumnHorizontalPadding"
                          }
                        }
                      ]
                    }
                  }
                }
              }
            ],
            "mainAxisAlignment": "spaceBetween",
            "crossAxisAlignment": "start"
          }
        },
        "height": "100.h",
        "width": "100.w"
      }
    }
  };

  static final template13 = {
    "_id": "64ee744398c3d4605b477abp",
    "templateSlug": "temp_pho_3",
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
        "21/9": {
          "categoryFontSize": "3.5.sp",
          "verticalFramePadding": "2.sp",
          "horizontalFramePadding": "2.sp",
          "categoryBlockHeight": "25.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
        "16/9": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
        "16/10": {
          "categoryFontSize": "6.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "23.h",
          "brandFontSize": "12.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
        },
        "4/3": {
          "categoryFontSize": "7.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "23.h",
          "brandFontSize": "15.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp",
        },
        "default": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
      },
    },
    "dataSetting": {
      "9e2d1aeb-1a48-47a1-a8d9-c62061dd9298": {
        "id": "9e2d1aeb-1a48-47a1-a8d9-c62061dd9298",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description": ""
      },
      "1ac4b269-050f-45a4-a824-111676cc3a81": {
        "id": "1ac4b269-050f-45a4-a824-111676cc3a81",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "eb6eed23-c2f5-4673-8b57-c3f8d20bd3eb": {
        "id": "eb6eed23-c2f5-4673-8b57-c3f8d20bd3eb",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/bottom_deco.png"
      },
      "02cb5eab-7b12-4c90-8755-fc1d4c7127dc": {
        "id": "02cb5eab-7b12-4c90-8755-fc1d4c7127dc",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/left_top_deco.png"
      },
      "5edf3154-588f-4514-a4d2-38a4d57947d2": {
        "id": "5edf3154-588f-4514-a4d2-38a4d57947d2",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/right_top_deco.png"
      },
      "0762cbdb-07bb-439b-8d18-ac424cf69981": {
        "id": "0762cbdb-07bb-439b-8d18-ac424cf69981",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1-1.jpg"
      },
      "819322cd-d309-4d4d-b0c1-6a18d78bda7f": {
        "id": "819322cd-d309-4d4d-b0c1-6a18d78bda7f",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/circle_border.png"
      },
      "0762cbdb-07bb-439b-8d18-ac424cf69982": {
        "id": "0762cbdb-07bb-439b-8d18-ac424cf69982",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2-1.jpg"
      },
      "0762cbdb-07bb-439b-8d18-ac424cf69983": {
        "id": "819322cd-d309-4d4d-b0c1-6a18d78bda7f",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3-1.jpg"
      }
    },
    "widgets_setting": {
      "id": "e3ffb1c4-f619-4f9e-b138-f1371e837371",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"boxFit": "fill"},
        "child": {
          "id": "09902032-3ac6-4c5c-9b81-ed8f1fb1bdbc",
          "headerName": "Stack",
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              {
                "id": "c528157c-0ec3-4f54-a7f9-764f2f4d2541",
                "headerName": "Container",
                "widgetName": "Container",
                "widgetSetting": {
                  "decoration": {"boxFit": "fill", "color": "0xff0d507e"},
                  "child": {
                    "id": "356063b4-15eb-48b4-bbd4-1f9aaa7fb7cb",
                    "headerName": "PaddingByRatio",
                    "widgetName": "PaddingByRatio",
                    "widgetSetting": {
                      "child": {
                        "id": "7787b375-01f7-4d52-a0c9-6ea8b7fa9550",
                        "headerName": "Column",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "1535b27b-f4c6-4a30-adb5-7572052d86cf",
                              "headerName": "Text",
                              "widgetName": "Text",
                              "widgetSetting": {
                                "textAlign": "start",
                                "style": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Staatliches",
                                  "color": "0xffffaf45"
                                },
                                "data": "PHO BRAND",
                                "fontSizeFromFieldKey": "brandFontSize"
                              }
                            },
                            {
                              "id": "ca4b8c2c-16cf-4f83-a2a4-4e499c180878",
                              "headerName": "Expanded",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "e9999e95-4aac-445b-aa28-b82194a8c78c",
                                  "headerName": "Wrap",
                                  "widgetName": "Wrap",
                                  "widgetSetting": {
                                    "clipBehavior": "hardEdge",
                                    "children": [
                                      {
                                        "id":
                                            "39740d33-324f-4a78-ba98-e0c5206e9fd1",
                                        "headerName": "CategoryBlock1",
                                        "widgetName": "CategoryBlock1",
                                        "widgetSetting": {
                                          "categoryItemRows": [
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81",
                                                    "border": {
                                                      "all": {
                                                        "width": "0.w",
                                                        "color": "0"
                                                      }
                                                    },
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            }
                                          ],
                                          "categoryTextStyle": {
                                            "fontWeight": "w400",
                                            "fontFamily": "Staatliches",
                                            "color": "0xffffffff"
                                          },
                                          "category":
                                              "9e2d1aeb-1a48-47a1-a8d9-c62061dd9298",
                                          "categoryFontSizeFromFieldKey":
                                              "categoryFontSize",
                                          "heightFromFieldKey":
                                              "categoryBlockHeight",
                                          "elementGap": "5.sp",
                                          "width": "40.w",
                                          "categoryImage": {
                                            "id":
                                                "10a60c0e-084e-443d-8b2e-5c87bccd9d29",
                                            "headerName": "FramedImage",
                                            "widgetName": "FramedImage",
                                            "widgetSetting": {
                                              "imageDataKey":
                                                  "0762cbdb-07bb-439b-8d18-ac424cf69981",
                                              "imageFrameDataKey":
                                                  "819322cd-d309-4d4d-b0c1-6a18d78bda7f",
                                              "height": "32.sp",
                                              "width": "32.sp",
                                              "borderRadiusOfImage": "32.sp",
                                              "borderWidth": "2.sp"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "39740d33-324f-4a78-ba98-e0c5206e9fd1",
                                        "headerName": "CategoryBlock1",
                                        "widgetName": "CategoryBlock1",
                                        "widgetSetting": {
                                          "categoryItemRows": [
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            }
                                          ],
                                          "categoryTextStyle": {
                                            "fontWeight": "w400",
                                            "fontFamily": "Staatliches",
                                            "color": "0xffffffff"
                                          },
                                          "category":
                                              "9e2d1aeb-1a48-47a1-a8d9-c62061dd9298",
                                          "categoryFontSizeFromFieldKey":
                                              "categoryFontSize",
                                          "heightFromFieldKey":
                                              "categoryBlockHeight",
                                          "elementGap": "5.sp",
                                          "width": "40.w",
                                          "categoryImage": {
                                            "id":
                                                "10a60c0e-084e-443d-8b2e-5c87bccd9d29",
                                            "headerName": "FramedImage",
                                            "widgetName": "FramedImage",
                                            "widgetSetting": {
                                              "imageDataKey":
                                                  "0762cbdb-07bb-439b-8d18-ac424cf69982",
                                              "imageFrameDataKey":
                                                  "819322cd-d309-4d4d-b0c1-6a18d78bda7f",
                                              "height": "32.sp",
                                              "width": "32.sp",
                                              "borderRadiusOfImage": "32.sp",
                                              "borderWidth": "2.sp"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "39740d33-324f-4a78-ba98-e0c5206e9fd1",
                                        "headerName": "CategoryBlock1",
                                        "widgetName": "CategoryBlock1",
                                        "widgetSetting": {
                                          "categoryItemRows": [
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            }
                                          ],
                                          "categoryTextStyle": {
                                            "fontWeight": "w400",
                                            "fontFamily": "Staatliches",
                                            "color": "0xffffffff"
                                          },
                                          "category":
                                              "9e2d1aeb-1a48-47a1-a8d9-c62061dd9298",
                                          "categoryFontSizeFromFieldKey":
                                              "categoryFontSize",
                                          "heightFromFieldKey":
                                              "categoryBlockHeight",
                                          "elementGap": "5.sp",
                                          "width": "40.w",
                                          "categoryImage": {
                                            "id":
                                                "10a60c0e-084e-443d-8b2e-5c87bccd9d29",
                                            "headerName": "FramedImage",
                                            "widgetName": "FramedImage",
                                            "widgetSetting": {
                                              "imageDataKey":
                                                  "0762cbdb-07bb-439b-8d18-ac424cf69983",
                                              "imageFrameDataKey":
                                                  "819322cd-d309-4d4d-b0c1-6a18d78bda7f",
                                              "height": "32.sp",
                                              "width": "32.sp",
                                              "borderRadiusOfImage": "32.sp",
                                              "borderWidth": "2.sp"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "39740d33-324f-4a78-ba98-e0c5206e9fd1",
                                        "headerName": "CategoryBlock1",
                                        "widgetName": "CategoryBlock1",
                                        "widgetSetting": {
                                          "categoryItemRows": [
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            }
                                          ],
                                          "categoryTextStyle": {
                                            "fontWeight": "w400",
                                            "fontFamily": "Staatliches",
                                            "color": "0xffffffff"
                                          },
                                          "category":
                                              "9e2d1aeb-1a48-47a1-a8d9-c62061dd9298",
                                          "categoryFontSizeFromFieldKey":
                                              "categoryFontSize",
                                          "heightFromFieldKey":
                                              "categoryBlockHeight",
                                          "elementGap": "5.sp",
                                          "width": "40.w",
                                          "categoryImage": {
                                            "id":
                                                "10a60c0e-084e-443d-8b2e-5c87bccd9d29",
                                            "headerName": "FramedImage",
                                            "widgetName": "FramedImage",
                                            "widgetSetting": {
                                              "imageDataKey":
                                                  "0762cbdb-07bb-439b-8d18-ac424cf69982",
                                              "imageFrameDataKey":
                                                  "819322cd-d309-4d4d-b0c1-6a18d78bda7f",
                                              "height": "32.sp",
                                              "width": "32.sp",
                                              "borderRadiusOfImage": "32.sp",
                                              "borderWidth": "2.sp"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "39740d33-324f-4a78-ba98-e0c5206e9fd1",
                                        "headerName": "CategoryBlock1",
                                        "widgetName": "CategoryBlock1",
                                        "widgetSetting": {
                                          "categoryItemRows": [
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            }
                                          ],
                                          "categoryTextStyle": {
                                            "fontWeight": "w400",
                                            "fontFamily": "Staatliches",
                                            "color": "0xffffffff"
                                          },
                                          "category":
                                              "9e2d1aeb-1a48-47a1-a8d9-c62061dd9298",
                                          "categoryFontSizeFromFieldKey":
                                              "categoryFontSize",
                                          "heightFromFieldKey":
                                              "categoryBlockHeight",
                                          "elementGap": "5.sp",
                                          "width": "40.w",
                                          "categoryImage": {
                                            "id":
                                                "10a60c0e-084e-443d-8b2e-5c87bccd9d29",
                                            "headerName": "FramedImage",
                                            "widgetName": "FramedImage",
                                            "widgetSetting": {
                                              "imageDataKey":
                                                  "0762cbdb-07bb-439b-8d18-ac424cf69981",
                                              "imageFrameDataKey":
                                                  "819322cd-d309-4d4d-b0c1-6a18d78bda7f",
                                              "height": "32.sp",
                                              "width": "32.sp",
                                              "borderRadiusOfImage": "32.sp",
                                              "borderWidth": "2.sp"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "39740d33-324f-4a78-ba98-e0c5206e9fd1",
                                        "headerName": "CategoryBlock1",
                                        "widgetName": "CategoryBlock1",
                                        "widgetSetting": {
                                          "categoryItemRows": [
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9f4f05d5-5f0b-4792-b7f9-2c8de409a3f5",
                                              "headerName": "Padding",
                                              "widgetName": "Padding",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "cc0ad00b-1de8-427f-815c-40109c4d52ef",
                                                  "headerName": "ProductRow1",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "fractionDigitsForPrice":
                                                        "1",
                                                    "maxLineOfProductName": "3",
                                                    "prefixProductPrice": "\$",
                                                    "subfixProductPrice": "",
                                                    "upperCaseProductName":
                                                        "false",
                                                    "productNameTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffffff"
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontWeight": "w400",
                                                      "fontFamily": "Poppins",
                                                      "color": "0xffffaf45"
                                                    },
                                                    "product":
                                                        "1ac4b269-050f-45a4-a824-111676cc3a81"
                                                  }
                                                },
                                                "padding": {
                                                  "vertical": "0.5.sp"
                                                }
                                              }
                                            }
                                          ],
                                          "categoryTextStyle": {
                                            "fontWeight": "w400",
                                            "fontFamily": "Staatliches",
                                            "color": "0xffffffff"
                                          },
                                          "category":
                                              "9e2d1aeb-1a48-47a1-a8d9-c62061dd9298",
                                          "categoryFontSizeFromFieldKey":
                                              "categoryFontSize",
                                          "heightFromFieldKey":
                                              "categoryBlockHeight",
                                          "elementGap": "5.sp",
                                          "width": "40.w",
                                          "categoryImage": {
                                            "id":
                                                "10a60c0e-084e-443d-8b2e-5c87bccd9d29",
                                            "headerName": "FramedImage",
                                            "widgetName": "FramedImage",
                                            "widgetSetting": {
                                              "imageDataKey":
                                                  "0762cbdb-07bb-439b-8d18-ac424cf69982",
                                              "imageFrameDataKey":
                                                  "819322cd-d309-4d4d-b0c1-6a18d78bda7f",
                                              "height": "32.sp",
                                              "width": "32.sp",
                                              "borderRadiusOfImage": "32.sp",
                                              "borderWidth": "2.sp"
                                            }
                                          }
                                        }
                                      }
                                    ],
                                    "direction": "vertical",
                                    "runSpacing": "10.sp",
                                    "spacing": "5.sp"
                                  }
                                }
                              }
                            }
                          ],
                          "mainAxisAlignment": "center"
                        }
                      },
                      "verticalPaddingFieldKey": "verticalFramePadding",
                      "horizontalPaddingFieldKey": "horizontalFramePadding"
                    }
                  },
                  "height": "100.h",
                  "width": "100.w"
                }
              },
              {
                "id": "6d722af7-cecb-47fd-b04d-7ba3ef24e149",
                "headerName": "Positioned",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "child": {
                    "id": "750c35b0-f120-48a9-adae-620879585ec3",
                    "headerName": "Image",
                    "widgetName": "Image",
                    "widgetSetting": {
                      "fixCover": "true",
                      "imageId": "eb6eed23-c2f5-4673-8b57-c3f8d20bd3eb",
                      "height": "100.h",
                      "width": "100.w"
                    }
                  },
                  "bottom": "-14.sp"
                }
              },
              {
                "id": "4dfe6745-32d5-46b5-adf5-a67308f2d45a",
                "headerName": "Positioned",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "child": {
                    "id": "2b167c2f-37b8-493d-b386-9c7b02ade587",
                    "headerName": "Image",
                    "widgetName": "Image",
                    "widgetSetting": {
                      "fixCover": "false",
                      "imageId": "02cb5eab-7b12-4c90-8755-fc1d4c7127dc",
                      "height": "30.sp",
                      "width": "30.sp"
                    }
                  },
                  "left": "0.w"
                }
              },
              {
                "id": "14e631ad-8311-4845-a770-ef3c0aa7bb56",
                "headerName": "Positioned",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "child": {
                    "id": "20a99bf0-a9e0-4d43-abbd-0a88c349cd3b",
                    "headerName": "Image",
                    "widgetName": "Image",
                    "widgetSetting": {
                      "fixCover": "false",
                      "imageId": "5edf3154-588f-4514-a4d2-38a4d57947d2",
                      "height": "30.sp",
                      "width": "30.sp"
                    }
                  },
                  "right": "0.w"
                }
              }
            ]
          }
        }
      }
    }
  };

  static final template14 = {
    "_id": "64ee744398c3d4605b477abq",
    "templateSlug": "temp_pho_4",
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "21/9": {
          "categoryNameFontSize": "5.sp",
          "categoryDesFontSize": "3.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "16/9": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
          "productDesFontSize": "2.5.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "16/10": {
          "categoryNameFontSize": "10.sp",
          "categoryDesFontSize": "4.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "2.8.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "4/3": {
          "categoryNameFontSize": "12.sp",
          "categoryDesFontSize": "5.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp",
          "productDesFontSize": "3.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "default": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
      },
    },
    "dataSetting": {
      "7fe16b77-f67f-4b06-9c30-d32031c16dd7": {
        "id": "7fe16b77-f67f-4b06-9c30-d32031c16dd7",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description": ""
      },
      "5ebd26b9-a116-4561-bb3b-fe847e0850a5": {
        "id": "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "4d659aba-bbb5-47fb-85b1-84836dae12b5": {
        "id": "4d659aba-bbb5-47fb-85b1-84836dae12b5",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "331d59ca-c839-4274-b502-0dbbce5af23a": {
        "id": "331d59ca-c839-4274-b502-0dbbce5af23a",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "0274e09a-20c6-4222-8fba-f68f459dd50a": {
        "id": "0274e09a-20c6-4222-8fba-f68f459dd50a",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1.png"
      },
      "8fd6dec3-2418-43b4-9e88-84b3f0504e17": {
        "id": "8fd6dec3-2418-43b4-9e88-84b3f0504e17",
        "type": "category",
        "name": "Category 2",
        "image": "",
        "description": ""
      },
      "aa6f9fd6-7344-4efe-9079-2205f9ddb845": {
        "id": "aa6f9fd6-7344-4efe-9079-2205f9ddb845",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2.png"
      },
      "aa6f9fd6-7344-4efe-9079-2205f9ddb846": {
        "id": "aa6f9fd6-7344-4efe-9079-2205f9ddb846",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3.png"
      },
      "aa6f9fd6-7344-4efe-9079-2205f9ddb847": {
        "id": "aa6f9fd6-7344-4efe-9079-2205f9ddb847",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_4.png"
      },
      "ec5bb2d8-4182-460b-b790-4c82bc3422e6": {
        "id": "ec5bb2d8-4182-460b-b790-4c82bc3422e6",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      },
      "9b65bb30-652f-49f7-bbf6-454496c8f63d": {
        "id": "9b65bb30-652f-49f7-bbf6-454496c8f63d",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description": "this is a description"
      }
    },
    "widgets_setting": {
      "id": "0570149d-f4e7-4d3c-bd81-fa3333c2f318",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"boxFit": "fill", "color": "0xffece3d8"},
        "child": {
          "id": "1e932768-9685-4a42-8104-7ce95faa4771",
          "headerName": "PaddingByRatio",
          "widgetName": "PaddingByRatio",
          "widgetSetting": {
            "child": {
              "id": "1359e7e7-fee5-4a8f-a259-48a968d6cbb4",
              "headerName": "Wrap",
              "widgetName": "Wrap",
              "widgetSetting": {
                "clipBehavior": "none",
                "children": [
                  {
                    "id": "e6fc635a-cc39-4afb-800b-141960fc5973",
                    "headerName": "SizedBox",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "child": {
                        "id": "5bd71f48-30cd-44a6-8087-17384b5c49fc",
                        "headerName": "CategoryBlock2",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "categoryDescriptionTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Dancing Script",
                            "fontStyle": "italic"
                          },
                          "categoryItems": [
                            {
                              "id": "1c81a878-7a25-4f99-983f-0324171de4a7",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "e4c3c827-94f0-487d-901f-897da055c70f",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            },
                            {
                              "id": "1c81a878-7a25-4f99-983f-0324171de4a7",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "e4c3c827-94f0-487d-901f-897da055c70f",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            },
                            {
                              "id": "1c81a878-7a25-4f99-983f-0324171de4a7",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "e4c3c827-94f0-487d-901f-897da055c70f",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            },
                            {
                              "id": "3e95a45a-ca38-4b50-9552-02261f4e0023",
                              "headerName": "Container",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "decoration": {"boxFit": "fill"},
                                "child": {
                                  "id": "3fba4efa-db33-41d3-b984-60c324e41a3d",
                                  "headerName": "SimpleImage",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "fit": "contain",
                                    "imageDataKey":
                                        "0274e09a-20c6-4222-8fba-f68f459dd50a",
                                    "width": "20.w"
                                  }
                                },
                                "height": "25.h",
                                "width": "20.w",
                                "padding": {"all": "2.sp"}
                              }
                            }
                          ],
                          "categoryNameTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Roboto Slab",
                            "color": "0xff583618"
                          },
                          "upperCaseCategoryName": "true",
                          "category": "7fe16b77-f67f-4b06-9c30-d32031c16dd7",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "id": "divider",
                            "headerName": "Divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize"
                        }
                      },
                      "width": "45.w",
                      "height": "45.h"
                    }
                  },
                  {
                    "id": "aa0e1f41-bf85-4d59-8ebb-15305c680223",
                    "headerName": "SizedBox",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "child": {
                        "id": "1503157a-34d5-469a-97e0-56573bd26b3a",
                        "headerName": "CategoryBlock2",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "categoryDescriptionTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Dancing Script",
                            "fontStyle": "italic"
                          },
                          "categoryItems": [
                            {
                              "id": "62a2a1c4-daa0-4523-8bd9-9986eba29833",
                              "headerName": "Container",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "decoration": {"boxFit": "fill"},
                                "child": {
                                  "id": "f96d6d9d-0f37-4905-a5f9-45655bc3a3ed",
                                  "headerName": "SimpleImage",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "fit": "contain",
                                    "imageDataKey":
                                        "aa6f9fd6-7344-4efe-9079-2205f9ddb845",
                                    "width": "20.w"
                                  }
                                },
                                "height": "25.h",
                                "width": "20.w",
                                "padding": {"all": "2.sp"}
                              }
                            },
                            {
                              "id": "5cb220d9-7a6f-4e0b-811c-6e909c8c92c2",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "a207b27f-54f9-4740-b30c-8fb5fa077a22",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            },
                            {
                              "id": "5cb220d9-7a6f-4e0b-811c-6e909c8c92c2",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "a207b27f-54f9-4740-b30c-8fb5fa077a22",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            },
                            {
                              "id": "5cb220d9-7a6f-4e0b-811c-6e909c8c92c2",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "a207b27f-54f9-4740-b30c-8fb5fa077a22",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            }
                          ],
                          "categoryNameTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Roboto Slab",
                            "color": "0xff583618"
                          },
                          "upperCaseCategoryName": "true",
                          "category": "8fd6dec3-2418-43b4-9e88-84b3f0504e17",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "id": "divider",
                            "headerName": "Divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize"
                        }
                      },
                      "width": "45.w",
                      "height": "45.h"
                    }
                  },
                  {
                    "id": "e6fc635a-cc39-4afb-800b-141960fc5973",
                    "headerName": "SizedBox",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "child": {
                        "id": "5bd71f48-30cd-44a6-8087-17384b5c49fc",
                        "headerName": "CategoryBlock2",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "categoryDescriptionTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Dancing Script",
                            "fontStyle": "italic"
                          },
                          "categoryItems": [
                            {
                              "id": "1c81a878-7a25-4f99-983f-0324171de4a7",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "e4c3c827-94f0-487d-901f-897da055c70f",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            },
                            {
                              "id": "1c81a878-7a25-4f99-983f-0324171de4a7",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "e4c3c827-94f0-487d-901f-897da055c70f",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            },
                            {
                              "id": "1c81a878-7a25-4f99-983f-0324171de4a7",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "e4c3c827-94f0-487d-901f-897da055c70f",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            },
                            {
                              "id": "3e95a45a-ca38-4b50-9552-02261f4e0023",
                              "headerName": "Container",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "decoration": {"boxFit": "fill"},
                                "child": {
                                  "id": "3fba4efa-db33-41d3-b984-60c324e41a3d",
                                  "headerName": "SimpleImage",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "fit": "contain",
                                    "imageDataKey":
                                        "aa6f9fd6-7344-4efe-9079-2205f9ddb846",
                                    "width": "20.w"
                                  }
                                },
                                "height": "25.h",
                                "width": "20.w",
                                "padding": {"all": "2.sp"}
                              }
                            }
                          ],
                          "categoryNameTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Roboto Slab",
                            "color": "0xff583618"
                          },
                          "upperCaseCategoryName": "true",
                          "category": "7fe16b77-f67f-4b06-9c30-d32031c16dd7",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "id": "divider",
                            "headerName": "Divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize"
                        }
                      },
                      "width": "45.w",
                      "height": "45.h"
                    }
                  },
                  {
                    "id": "aa0e1f41-bf85-4d59-8ebb-15305c680223",
                    "headerName": "SizedBox",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "child": {
                        "id": "1503157a-34d5-469a-97e0-56573bd26b3a",
                        "headerName": "CategoryBlock2",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "categoryDescriptionTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Dancing Script",
                            "fontStyle": "italic"
                          },
                          "categoryItems": [
                            {
                              "id": "62a2a1c4-daa0-4523-8bd9-9986eba29833",
                              "headerName": "Container",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "decoration": {"boxFit": "fill"},
                                "child": {
                                  "id": "f96d6d9d-0f37-4905-a5f9-45655bc3a3ed",
                                  "headerName": "SimpleImage",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "fit": "contain",
                                    "imageDataKey":
                                        "aa6f9fd6-7344-4efe-9079-2205f9ddb847",
                                    "width": "20.w"
                                  }
                                },
                                "height": "25.h",
                                "width": "20.w",
                                "padding": {"all": "2.sp"}
                              }
                            },
                            {
                              "id": "5cb220d9-7a6f-4e0b-811c-6e909c8c92c2",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "a207b27f-54f9-4740-b30c-8fb5fa077a22",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            },
                            {
                              "id": "5cb220d9-7a6f-4e0b-811c-6e909c8c92c2",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "a207b27f-54f9-4740-b30c-8fb5fa077a22",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            },
                            {
                              "id": "5cb220d9-7a6f-4e0b-811c-6e909c8c92c2",
                              "headerName": "SizedBox",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "child": {
                                  "id": "a207b27f-54f9-4740-b30c-8fb5fa077a22",
                                  "headerName": "ProductRow2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product":
                                        "5ebd26b9-a116-4561-bb3b-fe847e0850a5",
                                    "fractionDigitsForPrice": "2",
                                    "maxLineOfProductName": "3",
                                    "upperCaseProductName": "true",
                                    "productNameTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productPriceTextStyle": {
                                      "fontWeight": "w400"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontWeight": "w400"
                                    }
                                  }
                                },
                                "width": "20.w"
                              }
                            }
                          ],
                          "categoryNameTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Roboto Slab",
                            "color": "0xff583618"
                          },
                          "upperCaseCategoryName": "true",
                          "category": "8fd6dec3-2418-43b4-9e88-84b3f0504e17",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "id": "divider",
                            "headerName": "Divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize"
                        }
                      },
                      "width": "45.w",
                      "height": "45.h"
                    }
                  }
                ],
                "direction": "vertical",
                "runAlignment": "center",
                "alignment": "center"
              }
            },
            "verticalPaddingFieldKey": "verticalFramePadding",
            "horizontalPaddingFieldKey": "horizontalFramePadding"
          }
        },
        "height": "100.h",
        "width": "100.w"
      }
    }
  };

  static final template15 = {
    "_id": "64ee744398c3d4605b477abr",
    "templateSlug": "temp_pho_5",
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryNameFontSize": "5.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp",
        },
        "21/9": {
          "categoryNameFontSize": "4.sp",
          "productNameFontSize": "2.5.sp",
          "productPriceFontSize": "2.5.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "13.h",
          "columnGap": "2.sp",
        },
        "16/9": {
          "categoryNameFontSize": "6.sp",
          "productNameFontSize": "3.5.sp",
          "productPriceFontSize": "3.5.sp",
          "productDesFontSize": "2.5.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "35.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp",
        },
        "16/10": {
          "categoryNameFontSize": "8.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "2.8.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "35.sp",
          "productRowHeight": "12.5.h",
          "columnGap": "5.sp",
        },
        "4/3": {
          "categoryNameFontSize": "8.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "3.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "40.sp",
          "productRowHeight": "12.h",
          "columnGap": "6.sp",
        },
        "default": {
          "categoryNameFontSize": "5.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp",
        },
      },
    },
    "dataSetting": {
      "a5f0eb18-a1b8-4e4d-849f-fc2fbb5b7d25": {
        "id": "a5f0eb18-a1b8-4e4d-849f-fc2fbb5b7d25",
        "type": "category",
        "name": "Category 1",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/cat_1-2.jpg",
        "description": ""
      },
      "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb": {
        "id": "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "48389214-68e0-47a4-aa2b-d5d93bba82ed": {
        "id": "48389214-68e0-47a4-aa2b-d5d93bba82ed",
        "type": "category",
        "name": "Category 2",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/cat_2-2.jpg",
        "description": ""
      },
      "6113dd78-3852-4bc9-abf5-3c9c923ec020": {
        "id": "6113dd78-3852-4bc9-abf5-3c9c923ec020",
        "type": "product",
        "name": "product",
        "price": "12",
        "salePrice": "",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/pho_1.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      }
    },
    "widgets_setting": {
      "id": "65f3700f-e0cc-4813-9b48-cf5387ef5f32",
      "headerName": "Container",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"boxFit": "fill", "color": "0xfffdf6e9"},
        "child": {
          "id": "78f959e2-040e-458e-a751-72eb2c0380ce",
          "headerName": "Center",
          "widgetName": "Center",
          "widgetSetting": {
            "child": {
              "id": "e6455d81-1e18-4c8b-b74c-834f74d4f77a",
              "headerName": "Row",
              "widgetName": "Row",
              "widgetSetting": {
                "textRightToLeft": "false",
                "children": [
                  {
                    "id": "884fe860-ee9a-49c4-8f67-11fe3635c95c",
                    "headerName": "Container",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "decoration": {"boxFit": "fill"},
                      "child": {
                        "id": "0cadb615-3c36-447b-9002-65edf6aeeb54",
                        "headerName": "CategoryColumn2",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "categoryItems": [
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            }
                          ],
                          "categoryNameContainerDecoration": {
                            "boxFit": "fill",
                            "color": "0xff561828",
                            "borderRadius": "1.5.sp"
                          },
                          "categoryNameTextStyle": {"fontWeight": "w400"},
                          "upperCaseCategoryName": "true",
                          "hideScrollbarWhenOverflow": "false",
                          "category": "a5f0eb18-a1b8-4e4d-849f-fc2fbb5b7d25",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp"
                          },
                          "categoryNameContainerPadding": {"vertical": "1.sp"},
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "elementGapFromFieldKey": "columnGap"
                        }
                      },
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"}
                    }
                  },
                  {
                    "id": "f9ca6083-b05c-459c-92e0-c088d88b4bb0",
                    "headerName": "Container",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "decoration": {"boxFit": "fill"},
                      "child": {
                        "id": "e71db8f4-3d8d-4859-b291-f8da80724286",
                        "headerName": "CategoryColumn2",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "categoryItems": [
                            {
                              "id": "6c9f58ba-6ce6-4f5b-9ea9-35566357c975",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "64f6ad14-16ae-4175-ad7e-19bd22f37298",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "3a700a06-5ce2-4971-a482-994695bb3fb8",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "dfdb8e2f-f925-44f4-be48-968f28aebc27",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "6113dd78-3852-4bc9-abf5-3c9c923ec020",
                                        "elementGap": "2.sp",
                                        "imageHeight": "15.sp",
                                        "imageWidth": "20.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "6c9f58ba-6ce6-4f5b-9ea9-35566357c975",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "64f6ad14-16ae-4175-ad7e-19bd22f37298",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "3a700a06-5ce2-4971-a482-994695bb3fb8",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "dfdb8e2f-f925-44f4-be48-968f28aebc27",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "6113dd78-3852-4bc9-abf5-3c9c923ec020",
                                        "elementGap": "2.sp",
                                        "imageHeight": "15.sp",
                                        "imageWidth": "20.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "6c9f58ba-6ce6-4f5b-9ea9-35566357c975",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "64f6ad14-16ae-4175-ad7e-19bd22f37298",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "3a700a06-5ce2-4971-a482-994695bb3fb8",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "dfdb8e2f-f925-44f4-be48-968f28aebc27",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "6113dd78-3852-4bc9-abf5-3c9c923ec020",
                                        "elementGap": "2.sp",
                                        "imageHeight": "15.sp",
                                        "imageWidth": "20.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "6c9f58ba-6ce6-4f5b-9ea9-35566357c975",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "64f6ad14-16ae-4175-ad7e-19bd22f37298",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "3a700a06-5ce2-4971-a482-994695bb3fb8",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "dfdb8e2f-f925-44f4-be48-968f28aebc27",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "6113dd78-3852-4bc9-abf5-3c9c923ec020",
                                        "elementGap": "2.sp",
                                        "imageHeight": "15.sp",
                                        "imageWidth": "20.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "6c9f58ba-6ce6-4f5b-9ea9-35566357c975",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "64f6ad14-16ae-4175-ad7e-19bd22f37298",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "3a700a06-5ce2-4971-a482-994695bb3fb8",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "dfdb8e2f-f925-44f4-be48-968f28aebc27",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "6113dd78-3852-4bc9-abf5-3c9c923ec020",
                                        "elementGap": "2.sp",
                                        "imageHeight": "15.sp",
                                        "imageWidth": "20.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                          ],
                          "categoryNameContainerDecoration": {
                            "boxFit": "fill",
                            "color": "0xff561828",
                            "borderRadius": "1.5.sp"
                          },
                          "categoryNameTextStyle": {"fontWeight": "w400"},
                          "upperCaseCategoryName": "false",
                          "hideScrollbarWhenOverflow": "true",
                          "category": "48389214-68e0-47a4-aa2b-d5d93bba82ed",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp"
                          },
                          "categoryNameContainerPadding": {"vertical": "1.sp"},
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "elementGapFromFieldKey": "columnGap"
                        }
                      },
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"}
                    }
                  },
                  {
                    "id": "884fe860-ee9a-49c4-8f67-11fe3635c95c",
                    "headerName": "Container",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "decoration": {"boxFit": "fill"},
                      "child": {
                        "id": "0cadb615-3c36-447b-9002-65edf6aeeb54",
                        "headerName": "CategoryColumn2",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "categoryItems": [
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            }
                          ],
                          "categoryNameContainerDecoration": {
                            "boxFit": "fill",
                            "color": "0xff561828",
                            "borderRadius": "1.5.sp"
                          },
                          "categoryNameTextStyle": {"fontWeight": "w400"},
                          "upperCaseCategoryName": "true",
                          "hideScrollbarWhenOverflow": "false",
                          "category": "a5f0eb18-a1b8-4e4d-849f-fc2fbb5b7d25",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp"
                          },
                          "categoryNameContainerPadding": {"vertical": "1.sp"},
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "elementGapFromFieldKey": "columnGap"
                        }
                      },
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"}
                    }
                  },
                  {
                    "id": "884fe860-ee9a-49c4-8f67-11fe3635c95c",
                    "headerName": "Container",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "decoration": {"boxFit": "fill"},
                      "child": {
                        "id": "0cadb615-3c36-447b-9002-65edf6aeeb54",
                        "headerName": "CategoryColumn2",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "categoryItems": [
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            },
                            {
                              "id": "c7e56e4c-5cf1-4973-9857-62c388092c43",
                              "headerName": "SizedBoxByRatio",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "child": {
                                  "id": "a9320957-9034-426a-b8a2-2ad876ace79d",
                                  "headerName": "Padding",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "e09f5648-ddba-4369-b107-ef1317d4f9ff",
                                      "headerName": "ProductRow3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "upperCaseProductName": "true",
                                        "productNameTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productPriceTextStyle": {
                                          "fontWeight": "w700",
                                          "fontFamily": "Staatliches",
                                          "letterSpacing": "0.2.sp"
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontWeight": "w400",
                                          "fontFamily": "Open Sans",
                                          "color": "0xff66635a",
                                          "fontStyle": "italic"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b22d2e79-4e1b-4d2b-9e43-6454d9f639d4",
                                          "headerName": "Divider",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "product":
                                            "308eb72d-0a2b-4311-8f81-1f9d35ef0cfb",
                                        "elementGap": "2.sp",
                                        "imageHeight": "20.sp",
                                        "imageWidth": "15.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize"
                                      }
                                    },
                                    "padding": {"vertical": "2.sp"}
                                  }
                                },
                                "heightFromFieldKey": "productRowHeight"
                              }
                            }
                          ],
                          "categoryNameContainerDecoration": {
                            "boxFit": "fill",
                            "color": "0xff561828",
                            "borderRadius": "1.5.sp"
                          },
                          "categoryNameTextStyle": {"fontWeight": "w400"},
                          "upperCaseCategoryName": "true",
                          "hideScrollbarWhenOverflow": "false",
                          "category": "a5f0eb18-a1b8-4e4d-849f-fc2fbb5b7d25",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryTextStyle": {
                            "fontWeight": "w400",
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp"
                          },
                          "categoryNameContainerPadding": {"vertical": "1.sp"},
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "elementGapFromFieldKey": "columnGap"
                        }
                      },
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"}
                    }
                  }
                ],
                "mainAxisAlignment": "center",
                "crossAxisAlignment": "start"
              }
            }
          }
        },
        "height": "100.h",
        "width": "100.w",
        "padding": {"all": "5.sp"}
      }
    }
  };

  // fake list layout for library layout
  List<dynamic> listLayout = [
    {
      'layoutId': 1,
      'layoutName': template1['templateSlug'],
      'RatioSetting': template1['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_restaurant_1.png",
      'layoutSettings': template1['widgets_setting'],
      'dataSetting': template1['dataSetting'],
      'layoutCategory': 'Food'
    },
    {
      'layoutId': 2,
      'layoutName': template2['templateSlug'],
      'RatioSetting': template2['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_restaurant_2.png",
      'layoutSettings': template2['widgets_setting'],
      'dataSetting': template2['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 3,
      'layoutName': template3['templateSlug'],
      'RatioSetting': template3['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/template_restaurant_3.png",
      'layoutSettings': template3['widgets_setting'],
      'dataSetting': template3['dataSetting'],
      'layoutCategory': 'Education'
    },
    {
      'layoutId': 4,
      'layoutName': template4['templateSlug'],
      'RatioSetting': template4['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/template_restaurant_3.png",
      'layoutSettings': template4['widgets_setting'],
      'dataSetting': template4['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 5,
      'layoutName': template5['templateSlug'],
      'RatioSetting': template5['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/template_restaurant_3.png",
      'layoutSettings': template5['widgets_setting'],
      'dataSetting': template5['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 6,
      'layoutName': template6['templateSlug'],
      'RatioSetting': template6['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_nail_1.png",
      'layoutSettings': template6['widgets_setting'],
      'dataSetting': template6['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 7,
      'layoutName': template7['templateSlug'],
      'RatioSetting': template7['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_nail_2.png",
      'layoutSettings': template7['widgets_setting'],
      'dataSetting': template7['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 8,
      'layoutName': template8['templateSlug'],
      'RatioSetting': template8['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_nail_3.png",
      'layoutSettings': template8['widgets_setting'],
      'dataSetting': template8['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 9,
      'layoutName': template9['templateSlug'],
      'RatioSetting': template9['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_nail_4.png",
      'layoutSettings': template9['widgets_setting'],
      'dataSetting': template9['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 10,
      'layoutName': template10['templateSlug'],
      'RatioSetting': template10['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_nail_5.png",
      'layoutSettings': template10['widgets_setting'],
      'dataSetting': template10['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 11,
      'layoutName': template11['templateSlug'],
      'RatioSetting': template11['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_pho_1.png",
      'layoutSettings': template11['widgets_setting'],
      'dataSetting': template11['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 12,
      'layoutName': template12['templateSlug'],
      'RatioSetting': template12['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_pho_2.png",
      'layoutSettings': template12['widgets_setting'],
      'dataSetting': template12['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 13,
      'layoutName': template13['templateSlug'],
      'RatioSetting': template13['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_pho_3.png",
      'layoutSettings': template13['widgets_setting'],
      'dataSetting': template13['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 14,
      'layoutName': template14['templateSlug'],
      'RatioSetting': template14['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_pho_4.png",
      'layoutSettings': template14['widgets_setting'],
      'dataSetting': template14['dataSetting'],
      'layoutCategory': 'Business'
    },
    {
      'layoutId': 15,
      'layoutName': template15['templateSlug'],
      'RatioSetting': template15['RatioSetting'],
      'layoutImage':
          "https://foodiemenu.co//wp-content/uploads/2023/11/temp_pho_5.png",
      'layoutSettings': template15['widgets_setting'],
      'dataSetting': template15['dataSetting'],
      'layoutCategory': 'Business'
    }
  ];

  // data illustrates the module layout
  static final listInsertLayout = [
    {'Container': Icons.add_box},
    {'Row': Icons.add_box},
    {'Column': Icons.add_box},
    {'Padding': Icons.add_box},
    {'Padding Ratio': Icons.add_box},
    {'Category Column 1': Icons.add_box},
    {'Category Column 2': Icons.add_box},
    {'Category Block 1': Icons.add_box},
    {'Category Block 2': Icons.add_box},
    {'Expanded': Icons.add_box},
    {'Sized Box': Icons.add_box},
    {'List View': Icons.add_box},
    {'Sized Box Ratio': Icons.add_box},
    {'Center': Icons.add_box},
    {'Stack': Icons.add_box},
    {'Positioned': Icons.add_box},
    {'Rotated Box': Icons.add_box},
    {'Menu Column 1': Icons.add_box},
    {'Intrinsic Height': Icons.add_box},
    {'Custom Container': Icons.add_box},
    {'Wrap': Icons.add_box},
  ];

  // data align options
  static List<String> alignOptions = [
    'no use',
    'bottomLeft',
    'bottomCenter',
    'bottomRight',
    'centerLeft',
    'center',
    'centerRight',
    'topLeft',
    'topCenter',
    'topRight',
  ];
  // data blend mode options
  static List<String> blendModeOptions = [
    'no use',
    'hue',
    'dstATop',
    'color',
    'darken',
    'lighten',
    'hardLight',
    'modulate'
  ];

  // data cross axis options
  static List<String> crossAxisOptions = [
    'no use',
    'start',
    'center',
    'end',
    'baseline',
    'stretch',
  ];

  // data main axis options
  static List<String> mainAxisOptions = [
    'no use',
    'start',
    'center',
    'end',
    'spaceEvenly',
    'spaceBetween',
    'spaceAround',
  ];

  // data font weight options
  static List<String> fontWeightOptions = [
    'w200',
    'w300',
    'w400',
    'w500',
    'w600',
    'w700',
    'w800',
    'w900',
  ];

  // data text align options
  static List<String> textAlignOptions = [
    'no use',
    'start',
    'center',
    'left',
    'right',
    'justify',
    'end',
  ];

  // data text align options
  static List<String> wrapCrossAlignmentOptions = [
    'no use',
    'start',
    'center',
    'end',
  ];

  // data clip behavior options
  static List<String> clipBehaviorOptions = [
    'none',
    'hardEdge',
    'antiAlias',
    'antiAliasWithSaveLayer',
  ];

  // data clip behavior options
  static List<String> fontFamilyOptions = [
    'no use',
    'Bebas Neue',
    'Poppins',
    'Gravitas One',
    'Open Sans',
    'Arvo',
    'Staatliches',
    'Roboto Slab',
    'Cormorant Garamond',
    'Playfair Display',
    'Pacifico',
    'DM Serif Text',
    'DM Serif Display',
    'Neuton',
    'montserrat',
    'Dancing Script',
    'Nunito Sans',
  ];

  static List<String> wrapVerticalOptions = ["no use", "down", "up"];

  static List<String> wrapDirectionOptions = [
    "no use",
    "horizontal",
    "vertical"
  ];

  static List<String> fontStyleOptions = ['no use', 'normal', 'italic'];
}
