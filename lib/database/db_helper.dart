import 'package:mongo_dart/mongo_dart.dart';

class MongoDBHelper {
  final String _uri;
  late Db _db;

  MongoDBHelper(this._uri) {
    _db = Db(_uri);
  }

  Future<void> openConnection() async {
    await _db.open();
  }

  Future<void> closeConnection() async {
    await _db.close();
  }

  Future<void> insertTemplate(Map<String, dynamic> templateData) async {
    await _db.collection('templates').insert(templateData);
  }

  Future<List<Map<String, dynamic>>> getTemplates() async {
    final cursor = await _db.collection('templates').find();
    print(cursor);
    return await cursor.toList();
  }
}
