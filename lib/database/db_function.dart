import 'package:tv_menu/database/db_helper.dart';

class DatabaseController {
  late MongoDBHelper dbHelper;

  DatabaseController() {
    dbHelper = MongoDBHelper(
        'mongodb+srv://admin:admin@cluster0.zey4jgk.mongodb.net/tv-menu');
  }

  // Get all template from collection templates
  List<dynamic> getTemplates({limit = 20, page = 1, query}) {
    dbHelper.openConnection(); //Open connect
    // Get list template
    final templates = dbHelper.getTemplates() as List;
    dbHelper.closeConnection(); //Close connecting
    return templates;
  }

  // Check from collection templates
  Future<void> checkConnect() async {
    dbHelper.openConnection(); //Open connect
    // Get list template
    // final templates = await dbHelper.getTemplates();
    dbHelper.getTemplates();
    dbHelper.closeConnection(); //Close connecting
  }
}
