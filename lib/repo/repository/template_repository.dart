import 'dart:convert';

import 'package:hive/hive.dart';
import 'package:tv_menu/data/data_template.dart';
import 'package:tv_menu/data/enum.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/repo/request/general_request.dart';
import 'package:tv_menu/repo/request/template_request.dart';

// [092301TREE]
class TemplateRepository {
  static const int perPage = 10;

  // get all templates
  Future<void> getAllTemplates() async {
    try {
      dynamic jsonData = await TemplateRequest().fetchListTemplate();
      final data = jsonData.map((t) => Template.fromMap(t)).toList().reversed;
      Helpers.saveLocalDb(BOX.app, "templateList", data.toList());
    } catch (e) {
      throw Exception(e);
    }
  }

  // get and convert json to list object
  List<Template> getListConverted() {
    // get string json from box
    final templateListJson =
        Helpers.getLocalDb(BOX.app)!.get("templateList").toString();
    // convert to list object
    List<Template> templateList;
    if (templateListJson != "" && templateListJson != "null") {
      templateList = (jsonDecode(templateListJson) as List)
          .map((item) => Template.fromMap(item))
          .toList();
    } else {
      templateList = [];
    }

    return templateList;
  }

  // save templates to box
  Future<void> saveTemplatesToBox(List<Template> templates) async {
    final updatedTemplateListJson = templates;
    Helpers.saveLocalDb(BOX.app, "templateList", updatedTemplateListJson);
  }

  // get templates from box
  Future<List<Template>> getTemplatesFromBox(int currentPage,
      [List<Template>? templates]) async {
    // get lis string from box
    final templateListJson =
        Helpers.getLocalDb(BOX.app)!.get("templateList").toString();
    // convert to list object
    final List<dynamic> templateList = json.decode(templateListJson);
    // check is last page, update endIndex to get all items con lai

    // paginate
    // paginate for search results

    // paginate for list in box
    final List<Template> values =
        templateList.map((item) => Template.fromJson(item)).toList();
    return values;
  }

  // add new template to box
  Future<void> addOrEditTemplateOfBox(Template template, String type) async {
    // get list converted
    final templateList = getListConverted();
    // handle add template
    if (type == "addType") {
      dynamic result = await TemplateRequest().addNewTemplate(template);
      if (result == true) {
        await getAllTemplates();
      }
    }
    // handle edit template
    if (type == "editType") {
      dynamic result = await TemplateRequest().editTemplate(template);
      if (result == true) {
        await getAllTemplates();
      }
    }
    // save to box
  }

//  delete one, many template box
  Future<void> changeStatusTemplate(String templateId, bool status) async {
    // get list converted
    final templateList = getListConverted();
    // remove template from list

    // remove the template with the specified templateId
    final index = templateList.indexWhere((t) => t.id == templateId);
    if (index != -1) {
      dynamic result =
          await TemplateRequest().changeStatusTemplate(templateId, status);
      if (result == true) {
        await getAllTemplates();
      }
    }

    // save to box
  }

  //  delete one, many template box
  Future<void> deleteTemplate(String templateId) async {
    // get list converted
    final templateList = getListConverted();
    // remove template from list

    // remove the template with the specified templateId
    final index = templateList.indexWhere((t) => t.id == templateId);
    if (index != -1) {
      dynamic result = await TemplateRequest().deleteTemplate(templateId);
      if (result == true) {
        await getAllTemplates();
      }
    }

    // save to box
  }

  // search templates
  Future<List<dynamic>> searchTemplates(String keyword) async {
    // get list converted
    dynamic result =
        await TemplateRequest().searchTemplate(keyword, "", "", "");
    if (result != null) {
      return result;
    } else {
      return [];
    }
  }
}
