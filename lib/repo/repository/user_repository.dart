import 'dart:convert';

import 'package:hive/hive.dart';
import 'package:tv_menu/data/data_user.dart';
import 'package:tv_menu/data/enum.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/repo/request/user_request.dart';

// [092301TREE]
class UserRepository {
  static const int perPage = 10;

  // get all users
  Future<void> getAllUsers() async {
    try {
      dynamic jsonData = await UserRequest().fetchListUser();
      final data = jsonData.map((t) => User.fromMap(t)).toList().reversed;
      Helpers.saveLocalDb(BOX.app, "userList", data.toList());
    } catch (e) {
      throw Exception(e);
    }
  }

  // get and convert json to list object
  List<User> getListConverted() {
    // get string json from box
    final userListJson =
        Helpers.getLocalDb(BOX.app)!.get("userList").toString();
    // convert to list object
    List<User> userList;
    if (userListJson != "" && userListJson != "null") {
      userList = (jsonDecode(userListJson) as List)
          .map((item) => User.fromMap(item))
          .toList();
    } else {
      userList = [];
    }

    return userList;
  }

  // save users to box
  Future<void> saveUsersToBox(List<User> users) async {
    final updatedUserListJson = users;
    Helpers.saveLocalDb(BOX.app, "userList", updatedUserListJson);
  }

  // get users from box
  Future<List<User>> getUsersFromBox(int currentPage,
      [List<User>? users]) async {
    // get lis string from box
    final userListJson =
        Helpers.getLocalDb(BOX.app)!.get("userList").toString();
    // convert to list object
    final List<dynamic> userList = json.decode(userListJson);
    // check is last page, update endIndex to get all items con lai

    // paginate
    // paginate for search results

    // paginate for list in box
    final List<User> values =
        userList.map((item) => User.fromJson(item)).toList();
    return values;
  }

  // add new user to box
  Future<void> addOrEditUserOfBox(User user, String type) async {
    // get list converted
    dynamic userList = getListConverted();
    // handle add user
    if (type == "addType") {
      dynamic result = await UserRequest().addNewUser(user);
      if (result == true) {
        await getAllUsers();
      }
    }
    // handle edit user
    if (type == "editType") {
      final index = userList.indexWhere((t) => t.id == user.id);
      if (index != -1) {
        dynamic result = await UserRequest().editUser(user);
        if (result == true) {
          await getAllUsers();
        }
      }
    }
    // save to box
  }

  //  delete one, many user box
  Future<void> deleteUser(String userId) async {
    // get list converted
    dynamic userList = getListConverted();
    // remove user from list

    // remove the user with the specified userId
    final index = userList.indexWhere((t) => t.id == userId);
    if (index != -1) {
      dynamic result = await UserRequest().deleteUser(userId);
      if (result == true) {
        await getAllUsers();
      }
    }

    // save to box
  }

  // search templates
  Future<List<dynamic>> searchUsers(String keyword) async {
    // get list converted
    dynamic result = await UserRequest().searchUser(keyword);
    if (result != null) {
      return result;
    } else {
      return [];
    }
  }
}
