import 'dart:convert';

import 'package:hive/hive.dart';

import 'package:tv_menu/data/enum.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/repo/request/general_request.dart';

import 'package:tv_menu/repo/request/template_request.dart';
import 'package:tv_menu/repo/request/user_template_request.dart';

// [092301TREE]
class UserTemplateRepository {
  static const int perPage = 10;

  // get all UserTemplates
  Future<void> getAllUserTemplates() async {
    try {
      dynamic jsonData = await UserTemplateRequest().fetchListUserTemplate();
      final data =
          jsonData.map((t) => UserTemplate.fromMap(t)).toList().reversed;
      Helpers.saveLocalDb(BOX.app, "userTemplateList", data.toList());
    } catch (e) {
      throw Exception(e);
    }
  }

  // get and convert json to list object
  List<UserTemplate> getListConverted() {
    // get string json from box
    final userTemplateListJson =
        Helpers.getLocalDb(BOX.app)!.get("userTemplateList").toString();
    // convert to list object
    List<UserTemplate> userTemplateList;
    if (userTemplateListJson != "" && userTemplateListJson != "null") {
      userTemplateList = (jsonDecode(userTemplateListJson) as List)
          .map((item) => UserTemplate.fromMap(item))
          .toList();
    } else {
      userTemplateList = [];
    }

    return userTemplateList;
  }

  // save UserTemplates to box
  Future<void> saveUserTemplatesToBox(List<UserTemplate> UserTemplates) async {
    final updateduserTemplateListJson = UserTemplates;
    Helpers.saveLocalDb(
        BOX.app, "userTemplateList", updateduserTemplateListJson);
  }

  // get UserTemplates from box
  Future<List<UserTemplate>> getUserTemplatesFromBox(int currentPage,
      [List<UserTemplate>? UserTemplates]) async {
    // get lis string from box
    final userTemplateListJson =
        Helpers.getLocalDb(BOX.app)!.get("userTemplateList").toString();
    // convert to list object
    final List<dynamic> userTemplateList = json.decode(userTemplateListJson);
    // check is last page, update endIndex to get all items con lai

    // paginate
    // paginate for search results

    // paginate for list in box
    final List<UserTemplate> values =
        userTemplateList.map((item) => UserTemplate.fromJson(item)).toList();
    return values;
  }

  // add new UserTemplate to box
  Future<void> addOrEditUserTemplateOfBox(
      UserTemplate UserTemplate, String type) async {
    // get list converted
    final userTemplateList = getListConverted();
    // handle add UserTemplate
    if (type == "addType") {
      dynamic result =
          await UserTemplateRequest().addNewUserTemplate(UserTemplate);
      if (result == true) {
        await getAllUserTemplates();
      }
    }
    // handle edit UserTemplate
    if (type == "editType") {
      dynamic result =
          await UserTemplateRequest().editUserTemplate(UserTemplate);
      if (result == true) {
        await getAllUserTemplates();
      }
    }
    // save to box
  }

  //  delete one, many UserTemplate box
  Future<void> deleteUserTemplate(String UserTemplateId) async {
    // get list converted
    final userTemplateList = getListConverted();
    // remove UserTemplate from list

    // remove the UserTemplate with the specified UserTemplateId
    // final index = userTemplateList.indexWhere((t) => t.id == UserTemplateId);
    // if (index != -1) {
    //   dynamic result = await UserTemplateRequest().deleteUserTemplate(UserTemplateId);
    //   if (result == true) {
    //     await getAllUserTemplates();
    //   }
    // }

    // save to box
  }

  // search UserTemplates
  Future<List<UserTemplate>> searchUserTemplates(String keyword) async {
    // get list converted
    dynamic result =
        await UserTemplateRequest().searchUserTemplate(keyword, "", "", "");
    if (result == true) {
      return result();
    } else {
      return [];
    }
  }
}
