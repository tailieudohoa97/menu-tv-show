import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tv_menu/data/enum.dart';
import 'package:tv_menu/helper/app_exception.dart';
import 'package:tv_menu/helper/app_setting.dart';
import 'package:tv_menu/helper/encrypt_random.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/models/worker.dart';

class TemplateRequest {
  Future<dynamic> fetchListTemplate() async {
    final randomToken = EncryptRandom().encryptRandom();
    Uri reqUrl = Uri.parse("${AppSetting.listTemplate}?eid=$randomToken");
    Helpers.debugLog('RepoRequest > fetchListTemplate web url', reqUrl);

    dynamic callback = false;

    try {
      var webdata = await http.get(reqUrl);
      if (webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > fetchListTemplate web callback > fail',
            webdata.statusCode);
      }
      callback = json.decode(webdata.body);
    } catch (e) {
      Helpers.debugLog('RepoRequest > fetchListTemplate web > fail', e);
    }
    return callback;
  }

  //function add new template
  Future<dynamic> addNewTemplate(Template template) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.addNewTemplate);
    Helpers.debugLog('RepoRequest > addNewTemplate web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog('RepoRequest > addNewTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > addNewTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'templateName': template.templateName,
            'templateSlug': template.templateSlug,
            'ratioSetting': template.ratioSetting,
            'widgetSetting': template.widgetSetting,
            'dataSetting': template.dataSetting,
            'categoryId': template.categoryId
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > addNewTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = true;
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > addNewTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > addNewTemplate web callback > callback', callback);
    return callback;
  }

  //function edit template
  Future<dynamic> editTemplate(Template template) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.editTemplate);
    Helpers.debugLog('RepoRequest > editTemplate web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog('RepoRequest > editTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > editTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'templateId': template.id,
            'templateName': template.templateName,
            'templateSlug': template.templateSlug,
            'ratioSetting': template.ratioSetting,
            'widgetSetting': template.widgetSetting,
            'dataSetting': template.dataSetting,
            'categoryId': template.categoryId
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > editTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = true;
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > editTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > editTemplate web callback > callback', callback);
    return callback;
  }

  //function delete template
  Future<dynamic> deleteTemplate(String templateId) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.deleteTemplate);
    Helpers.debugLog('RepoRequest > deleteTemplate web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog('RepoRequest > deleteTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > deleteTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'templateId': templateId,
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > deleteTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = true;
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > deleteTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > deleteTemplate web callback > callback', callback);
    return callback;
  }

  //function delete template
  Future<dynamic> changeStatusTemplate(String templateId, bool status) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri reqUrl;
    if (status) {
      reqUrl = Uri.parse(AppSetting.changeStatusInActiveTemplate);
    } else {
      reqUrl = Uri.parse(AppSetting.changeStatusActiveTemplate);
    }

    Helpers.debugLog('RepoRequest > changeStatusTemplate web url', reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog(
          'RepoRequest > changeStatusTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog(
        'RepoRequest > changeStatusTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.post(reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'idTemplate': templateId,
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog(
            'RepoRequest > changeStatusTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = true;
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > changeStatusTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > changeStatusTemplate web callback > callback', callback);
    return callback;
  }

  //function search template
  Future<dynamic> searchTemplate(String keyword, String? categoryIds,
      String? ratios, String? limit) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));

    Uri _reqUrl = Uri.parse("${AppSetting.getListSearch}?search=$keyword");
    Helpers.debugLog('RepoRequest > searchTemplate web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog('RepoRequest > searchTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > searchTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.get(
        _reqUrl,
      );
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > searchTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = json.decode(_webdata.body);
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > searchTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > searchTemplate web callback > callback', callback);
    return callback;
  }
}
