import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tv_menu/data/enum.dart';
import 'package:tv_menu/helper/app_exception.dart';
import 'package:tv_menu/helper/app_setting.dart';
import 'package:tv_menu/helper/encrypt_random.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/models/worker.dart';

class UserRequest {
  Future<dynamic> fetchListUser() async {
    final randomToken = EncryptRandom().encryptRandom();
    Uri reqUrl = Uri.parse("${AppSetting.listUser}?eid=$randomToken");
    Helpers.debugLog('RepoRequest > fetchListUser web url', reqUrl);

    dynamic callback = false;

    try {
      var webdata = await http.get(reqUrl);
      if (webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > fetchListUser web callback > fail',
            webdata.statusCode);
      }
      callback = json.decode(webdata.body);
    } catch (e) {
      Helpers.debugLog('RepoRequest > fetchListUser web > fail', e);
    }
    return callback;
  }

  //function add new user
  Future<dynamic> addNewUser(User user) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.addNewUser);
    Helpers.debugLog('RepoRequest > addNewUser web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog('RepoRequest > addNewUser web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > addNewUser web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'username': user.userName,
            'password': user.passWord,
            'email': user.email,
            'subscriptionLevel': user.subscriptionLevel
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > addNewUser web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = true;
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > addNewUser web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > addNewUser web callback > callback', callback);
    return callback;
  }

  //function edit user
  Future<dynamic> editUser(User user) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.editUser);
    Helpers.debugLog('RepoRequest > editUser web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog('RepoRequest > editUser web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > editUser web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'id': user.id,
            'username': user.userName,
            'password': user.passWord,
            'email': user.email,
            'subscriptionLevel': user.subscriptionLevel
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog(
            'RepoRequest > editUser web callback > fail', _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = true;
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > editUser web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > editUser web callback > callback', callback);
    return callback;
  }

  //function delete user
  Future<dynamic> deleteUser(String userId) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.deleteUser);
    Helpers.debugLog('RepoRequest > deleteUser web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog('RepoRequest > deleteUser web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > deleteUser web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'id': userId,
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > deleteUser web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = true;
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > deleteUser web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > deleteUser web callback > callback', callback);
    return callback;
  }

  //function search user
  Future<dynamic> searchUser(String keyword) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));

    Uri _reqUrl = Uri.parse("${AppSetting.getListUserSearch}?search=$keyword");
    Helpers.debugLog('RepoRequest > searchUser web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog('RepoRequest > searchUser web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > searchUser web nonce', worker.nonce);
    try {
      var _webdata = await http.get(
        _reqUrl,
      );
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > searchUser web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = json.decode(_webdata.body);
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > searchUser web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > searchUser web callback > callback', callback);
    return callback;
  }
}
