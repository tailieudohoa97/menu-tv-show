import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tv_menu/data/enum.dart';
import 'package:tv_menu/helper/app_exception.dart';
import 'package:tv_menu/helper/app_setting.dart';
import 'package:tv_menu/helper/encrypt_random.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/models/worker.dart';

class GeneralRequest {
  Future<Worker?> fetchLogin(String username, String password) async {
    Worker? worker;

    // Uri _reqUrl = Uri.parse(AppSetting.reqWorker + AppSetting.testFetchAcct);
    // 230306LH Update login using fetch data from WP plugin API
    Uri reqUrl = Uri.parse(AppSetting.loginUser);
    Helpers.debugLog('RepoRequest > fetchLogin web url', reqUrl);
    // var callback = (RepoData.data["workers"] as List).first;
    try {
      var webdata = await http.post(reqUrl,
          // headers: AppSetting.headerReq,
          body: {'username': username, 'password': password});
      if (webdata.statusCode != 200) {
        Helpers.debugLog(
            'RepoRequest > fetchLogin web callback > fail', webdata.statusCode);
        if (webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      }
      //callback without worker info
      else if (webdata.statusCode == 200) {
        var callback = jsonDecode(webdata.body.toString());
        // var callback = jsonDecode(RepoData.data["worker"].first);
        worker = Worker.fromMap(callback);
        Helpers.saveLocalDb(BOX.app, 'worker', worker);
      }

      // Save worker response data to session
      //storageSession.write('worker', callback);
    } catch (e) {
      Helpers.debugLogV2("fetchLogin", e, true, LOGTYPE.ERROR);
    }
    // try {
    //   worker = Cashier.fromMap(callback);
    // } catch (e) {
    //   Helpers.debugLogV2("fetchWorker", e, true, LOGTYPE.ERROR);
    // }

    return worker;
  }

  //function add new template
  Future<dynamic> addNewTemplate(Template template) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.addNewTemplate);
    Helpers.debugLog('RepoRequest > addNewTemplate web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog('RepoRequest > addNewTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > addNewTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'templateName': template.templateName,
            'templateSlug': template.templateSlug,
            'ratioSetting': template.ratioSetting,
            'widgetSetting': template.widgetSetting,
            'dataSetting': template.dataSetting,
            'categoryId': template.categoryId
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > addNewTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = jsonDecode(_webdata.body);
        if (!callback) {
          Helpers.debugLog(
              'RepoRequest > addNewTemplate web callback > fail', "");
        }
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > addNewTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > addNewTemplate web callback > callback', callback);
    return callback;
  }

  //function edit template
  Future<dynamic> editTemplate(Template template) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.editTemplate);
    Helpers.debugLog('RepoRequest > editTemplate web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog('RepoRequest > editTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > editTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'templateId': template.id,
            'templateName': template.templateName,
            'templateSlug': template.templateSlug,
            'ratioSetting': template.ratioSetting,
            'widgetSetting': template.widgetSetting,
            'dataSetting': template.dataSetting,
            'categoryId': template.categoryId
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > editTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = jsonDecode(_webdata.body);
        if (!callback) {
          Helpers.debugLog(
              'RepoRequest > editTemplate web callback > fail', "");
        }
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > editTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > editTemplate web callback > callback', callback);
    return callback;
  }

  //function delete template
  Future<dynamic> deleteTemplate(String templateId) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.deleteTemplate);
    Helpers.debugLog('RepoRequest > deleteTemplate web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog('RepoRequest > deleteTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > deleteTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'templateId': templateId,
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > deleteTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = jsonDecode(_webdata.body);
        if (!callback) {
          Helpers.debugLog(
              'RepoRequest > deleteTemplate web callback > fail', "");
        }
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > deleteTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > deleteTemplate web callback > callback', callback);
    return callback;
  }

  Future<Map<String, dynamic>> getTemplateData(String templateID) async {
    final ramdonToken = EncryptRandom().encryptRandom();
    final response = await http.get(Uri.parse(
        'https://foodiemenu.co/wp-json/tv-menu-api/v1/templates/delete-template?templateId=$templateID&token=$ramdonToken'));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      if (response.body.isNotEmpty) {
        return json.decode(response.body);
      } else {
        return {};
      }
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw AppException();
    }
  }

  // function fetch list worker from server
  Future<dynamic> fetchListWorkers() async {
    dynamic prods;

    Uri reqUrl = Uri.parse(
        "https://staging.lawrencesmoke.ca/wp-json/pos-api/v1/products/"
        '?'
        'limit=15&page=1&order=DESC&sales=true');

    try {
      var webdata = await http.get(reqUrl);
      if (webdata.statusCode != 200) {
        Helpers.debugLog(
            'RepoRequest > fetchProds web callback > fail', webdata.statusCode);
      }
      var callback = jsonDecode(webdata.body);
      prods = callback['products'];
    } catch (e) {
      Helpers.debugLog('RepoRequest > fetchProds web > fail', e);
    }
    return prods;
  }
}
