import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tv_menu/data/enum.dart';
import 'package:tv_menu/helper/app_exception.dart';
import 'package:tv_menu/helper/app_setting.dart';
import 'package:tv_menu/helper/encrypt_random.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/models/worker.dart';

class UserTemplateRequest {
  Future<dynamic> fetchListUserTemplate() async {
    final randomToken = EncryptRandom().encryptRandom();
    Uri reqUrl = Uri.parse("${AppSetting.listUserTemplate}?eid=$randomToken");
    Helpers.debugLog('RepoRequest > fetchListUserTemplate web url', reqUrl);

    dynamic callback = false;

    try {
      var webdata = await http.get(reqUrl);
      if (webdata.statusCode != 200) {
        Helpers.debugLog(
            'RepoRequest > fetchListUserTemplate web callback > fail',
            webdata.statusCode);
      }
      callback = json.decode(webdata.body);
    } catch (e) {
      Helpers.debugLog('RepoRequest > fetchListUserTemplate web > fail', e);
    }
    return callback;
  }

  //function add new UserTemplate
  Future<dynamic> addNewUserTemplate(UserTemplate UserTemplate) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.addNewTemplate);
    Helpers.debugLog('RepoRequest > addNewUserTemplate web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog(
          'RepoRequest > addNewUserTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog(
        'RepoRequest > addNewUserTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {});
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > addNewUserTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = true;
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > addNewUserTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > addNewUserTemplate web callback > callback', callback);
    return callback;
  }

  //function edit UserTemplate
  Future<dynamic> editUserTemplate(UserTemplate UserTemplate) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.editTemplate);
    Helpers.debugLog('RepoRequest > editUserTemplate web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog(
          'RepoRequest > editUserTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog('RepoRequest > editUserTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {});
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > editUserTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = true;
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > editUserTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > editUserTemplate web callback > callback', callback);
    return callback;
  }

  //function delete UserTemplate
  Future<dynamic> deleteUserTemplate(String UserTemplateId) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.deleteTemplate);
    Helpers.debugLog('RepoRequest > deleteUserTemplate web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog(
          'RepoRequest > deleteUserTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog(
        'RepoRequest > deleteUserTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'UserTemplateId': UserTemplateId,
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog('RepoRequest > deleteUserTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = true;
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > deleteUserTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > deleteUserTemplate web callback > callback', callback);
    return callback;
  }

  //function search UserTemplate
  Future<dynamic> searchUserTemplate(String keyword, String? categoryIds,
      String? ratios, String? limit) async {
    Worker worker =
        Worker.fromMap(jsonDecode(Helpers.getLocalDb(BOX.app)!.get("worker")));
    Uri _reqUrl = Uri.parse(AppSetting.getListSearch);
    Helpers.debugLog(
        'RepoRequest > getListCustomUserTemplate web url', _reqUrl);

    dynamic callback = false;

    if (worker.nonce == null) {
      Helpers.debugLog(
          'RepoRequest > getListCustomUserTemplate web > fail', worker.nonce);
      return callback;
    }

    Helpers.debugLog(
        'RepoRequest > getListCustomUserTemplate web nonce', worker.nonce);
    try {
      var _webdata = await http.post(_reqUrl,
          // headers: AppSetting.headerReq,
          body: {
            'search': keyword,
            'categoryIds': categoryIds,
            'ratios': ratios,
          });
      if (_webdata.statusCode != 200) {
        Helpers.debugLog(
            'RepoRequest > getListCustomUserTemplate web callback > fail',
            _webdata.statusCode);
        if (_webdata.statusCode == 401) {
          Helpers.saveLocalDb(BOX.app, "status_code", "401");
        }
      } else {
        ///submit success to server
        ///save to local storage
        ///callback with the key
        callback = json.decode(_webdata.body);
        // callback = _webdata.body;
      }
    } catch (e) {
      Helpers.debugLog('RepoRequest > getListCustomUserTemplate web > fail', e);
    }
    Helpers.debugLog(
        'RepoRequest > getListCustomUserTemplate web callback > callback',
        callback);
    return callback;
  }
}
