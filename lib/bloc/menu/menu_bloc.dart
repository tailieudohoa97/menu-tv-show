import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/bloc/menu/menu_event.dart';
import 'package:tv_menu/bloc/menu/menu_state.dart';

class MenuBloc extends Bloc<MenuEvent, MenuState> {
  MenuBloc() : super(InitMenuItem(menuItem: 'Templates')) {
    on<SelectMenuItemEvent>((event, emit) async {
      try {
        emit(LoadingMenuItem());

        // await Future.delayed(const Duration(milliseconds: 200));

        emit(InitMenuItem(menuItem: event.menuItem));
      } catch (e) {
        emit(ErrorMenuItem(errorMessage: 'Something went wrong.'));
      }
    });
  }
}
