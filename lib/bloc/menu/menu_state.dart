import 'package:equatable/equatable.dart';

abstract class MenuState extends Equatable {}

class InitMenuItem extends MenuState {
  final String menuItem;

  InitMenuItem({required this.menuItem});

  @override
  List<Object> get props => [menuItem];
}

class LoadingMenuItem extends MenuState {
  @override
  List<Object> get props => [];
}

class ErrorMenuItem extends MenuState {
  final String errorMessage;

  ErrorMenuItem({required this.errorMessage});

  @override
  List<Object> get props => [];
}
