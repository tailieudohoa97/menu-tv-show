import 'package:equatable/equatable.dart';

abstract class MenuEvent extends Equatable {}

class SelectMenuItemEvent extends MenuEvent {
  final String menuItem;

  SelectMenuItemEvent(this.menuItem);

  @override
  List<Object> get props => [menuItem];
}
