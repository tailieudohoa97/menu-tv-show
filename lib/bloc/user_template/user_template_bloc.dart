import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/bloc/user_template/user_template_event.dart';
import 'package:tv_menu/bloc/user_template/user_template_state.dart';
import 'package:tv_menu/repo/repository/user_template_repository.dart';

class UserTemplateBloc extends Bloc<UserTemplateEvent, UserTemplateState> {
  final UserTemplateRepository repository;

  // [092302TREE]

  UserTemplateBloc({required this.repository}) : super(UserTemplateState()) {
    on<UserTemplateFetchEvent>(_onUserTemplateFetch);
    on<AddUserTemplateEvent>(_onAddUserTemplate);
    on<UpdateUserTemplateEvent>(_onUpdateUserTemplate);
    on<DeleteUserTemplateEvent>(_onDeleteUserTemplate);
    on<SearchUserTemplateEvent>(_onSearchUserTemplate);
  }

  // fetch UserTemplates event
  void _onUserTemplateFetch(
      UserTemplateFetchEvent event, Emitter<UserTemplateState> emit) async {
    try {
      // if status is initial

      // loading state
      emit(state.copyWith(isLoading: true));
      // get list converted
      dynamic listConverted;
      listConverted = repository.getListConverted();
      // check list current in box is empty , true -> fetch 17 items
      if (listConverted.isEmpty) {
        await repository.getAllUserTemplates();
        listConverted = repository.getListConverted();
      }
      // get UserTemplates with page equal current page = 0 in initial status

      // update states
      emit(state.copyWith(
        status: UserTemplateStatus.success,
        UserTemplates: listConverted,
        isLoading: false,
      ));
    } catch (error) {
      // got error

      emit(state.copyWith(
          status: UserTemplateStatus.failure,
          isLoading: false,
          error: error.toString()));
    }
  }

  // add new UserTemplate event
  void _onAddUserTemplate(
      AddUserTemplateEvent event, Emitter<UserTemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // add new UserTemplate to box
      await repository.addOrEditUserTemplateOfBox(
          event.UserTemplate, "addType");
      // get UserTemplates after adding new UserTemplate
      final UserTemplates = repository.getListConverted();

      // delayed loading during one second

      // update states
      emit(state.copyWith(
        status: UserTemplateStatus.success,
        UserTemplates: UserTemplates,
        isLoading: false,
      ));
    } catch (_) {
      emit(
          state.copyWith(status: UserTemplateStatus.failure, isLoading: false));
    }
  }

  // update UserTemplate event
  void _onUpdateUserTemplate(
      UpdateUserTemplateEvent event, Emitter<UserTemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // edit UserTemplate to box
      await repository.addOrEditUserTemplateOfBox(
          event.UserTemplate, "editType");
      // get UserTemplates after updating
      dynamic UserTemplates = repository.getListConverted();
      // delayed loading during one second

      // update states
      emit(state.copyWith(
        status: UserTemplateStatus.success,
        UserTemplates: UserTemplates,
        isLoading: false,
      ));
    } catch (_) {
      emit(
          state.copyWith(status: UserTemplateStatus.failure, isLoading: false));
    }
  }

  // delete UserTemplate event
  void _onDeleteUserTemplate(
      DeleteUserTemplateEvent event, Emitter<UserTemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // delete one, many UserTemplates
      await repository.deleteUserTemplate(event.UserTemplateId);
      // get UserTemplates after deleting
      final UserTemplates = repository.getListConverted();
      // update states
      emit(state.copyWith(
        status: UserTemplateStatus.success,
        UserTemplates: UserTemplates,
        isLoading: false,
      ));
    } catch (_) {
      emit(
          state.copyWith(status: UserTemplateStatus.failure, isLoading: false));
    }
  }

  // search UserTemplate event
  void _onSearchUserTemplate(
      SearchUserTemplateEvent event, Emitter<UserTemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // delete one, many UserTemplates
      await repository.searchUserTemplates(event.keyword);
      // get UserTemplates after deleting
      final UserTemplates = repository.getListConverted();
      // update states
      emit(state.copyWith(
        status: UserTemplateStatus.success,
        UserTemplates: UserTemplates,
        isLoading: false,
      ));
    } catch (_) {
      emit(
          state.copyWith(status: UserTemplateStatus.failure, isLoading: false));
    }
  }
}
