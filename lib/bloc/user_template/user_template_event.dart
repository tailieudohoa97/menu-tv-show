import 'package:equatable/equatable.dart';
import 'package:tv_menu/models/models.dart';

// [092302TREE]

abstract class UserTemplateEvent extends Equatable {
  @override
  List<Object> get props => [];
}

// fetch all UserTemplates
class UserTemplateFetchEvent extends UserTemplateEvent {}

// fetch previous page
class PreviousPageEvent extends UserTemplateEvent {}

// fetch next page
class NextPageEvent extends UserTemplateEvent {}

// fetch single page
class SpecificUserTemplateEvent extends UserTemplateEvent {
  final int UserTemplateId;

  SpecificUserTemplateEvent({required this.UserTemplateId});

  @override
  List<Object> get props => [UserTemplateId];
}

// new UserTemplate
class AddUserTemplateEvent extends UserTemplateEvent {
  final dynamic UserTemplate;

  AddUserTemplateEvent({required this.UserTemplate});

  @override
  List<Object> get props => [UserTemplate];
}

// update UserTemplate
class UpdateUserTemplateEvent extends UserTemplateEvent {
  final dynamic UserTemplate;

  UpdateUserTemplateEvent({required this.UserTemplate});

  @override
  List<Object> get props => [UserTemplate];
}

// delete UserTemplate
class DeleteUserTemplateEvent extends UserTemplateEvent {
  final String UserTemplateId;

  DeleteUserTemplateEvent({required this.UserTemplateId});

  @override
  List<Object> get props => [UserTemplateId];
}

// search UserTemplate
class SearchUserTemplateEvent extends UserTemplateEvent {
  final String keyword;

  SearchUserTemplateEvent({required this.keyword});

  @override
  List<Object> get props => [keyword];
}

// selected UserTemplates changed
class SelectedUserTemplateEvent extends UserTemplateEvent {
  final List<UserTemplate> UserTemplates;

  SelectedUserTemplateEvent({required this.UserTemplates});

  @override
  List<Object> get props => [UserTemplates];
}
