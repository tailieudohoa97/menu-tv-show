import 'package:equatable/equatable.dart';
import 'package:tv_menu/models/models.dart';

// [092302TREE]

enum UserTemplateStatus { initial, success, failure }

class UserTemplateState extends Equatable {
  final UserTemplateStatus status;
  final List<UserTemplate> UserTemplates;

  final bool hasLoadedAll;

  final bool isLoading;

  final String error;

  const UserTemplateState({
    this.status = UserTemplateStatus.initial,
    this.UserTemplates = const <UserTemplate>[],
    this.hasLoadedAll = false,
    this.isLoading = false,
    this.error = '',
  });

  UserTemplateState copyWith({
    UserTemplateStatus? status,
    List<UserTemplate>? UserTemplates,
    bool? hasLoadedAll,
    bool? isLoading,
    String? error,
  }) {
    return UserTemplateState(
      status: status ?? this.status,
      UserTemplates: UserTemplates ?? this.UserTemplates,
      hasLoadedAll: hasLoadedAll ?? this.hasLoadedAll,
      isLoading: isLoading ?? this.isLoading,
      error: error ?? this.error,
    );
  }

  @override
  List<Object> get props => [
        status,
        UserTemplates,
        hasLoadedAll,
        isLoading,
        error,
      ];
}
