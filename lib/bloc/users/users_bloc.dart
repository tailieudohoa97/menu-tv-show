import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/bloc/users/users_event.dart';
import 'package:tv_menu/bloc/users/users_state.dart';
import 'package:tv_menu/repo/repository/user_repository.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final UserRepository repository;

  // [092302TREE]

  UserBloc({required this.repository}) : super(UserState()) {
    on<UserFetchEvent>(_onUserFetch);
    on<AddUserEvent>(_onAddUser);
    on<UpdateUserEvent>(_onUpdateUser);
    on<DeleteUserEvent>(_onDeleteUser);
    on<SpecificUserEvent>(_onSpecificUser);
  }

  // fetch users event
  void _onUserFetch(UserFetchEvent event, Emitter<UserState> emit) async {
    try {
      // if status is initial

      // loading state
      emit(state.copyWith(isLoading: true));
      // get list converted
      dynamic listConverted;
      listConverted = repository.getListConverted();
      // check list current in box is empty , true -> fetch 17 items
      if (listConverted.isEmpty) {
        await repository.getAllUsers();
        listConverted = repository.getListConverted();
      }
      // get users with page equal current page = 0 in initial status

      // update states
      emit(state.copyWith(
        status: UserStatus.success,
        users: listConverted,
        isLoading: false,
      ));
    } catch (error) {
      // got error

      emit(state.copyWith(
          status: UserStatus.failure,
          isLoading: false,
          error: error.toString()));
    }
  }

  // add new user event
  void _onAddUser(AddUserEvent event, Emitter<UserState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // add new user to box
      await repository.addOrEditUserOfBox(event.user, "addType");
      // get users after adding new user
      final users = repository.getListConverted();

      // delayed loading during one second

      // update states
      emit(state.copyWith(
        status: UserStatus.success,
        users: users,
        isLoading: false,
      ));
    } catch (_) {
      emit(state.copyWith(status: UserStatus.failure, isLoading: false));
    }
  }

  // update user event
  void _onUpdateUser(UpdateUserEvent event, Emitter<UserState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // edit user to box
      await repository.addOrEditUserOfBox(event.user, "editType");
      // get users after updating
      final users = repository.getListConverted();
      // delayed loading during one second

      // update states
      emit(state.copyWith(
        status: UserStatus.success,
        users: users,
        isLoading: false,
      ));
    } catch (_) {
      emit(state.copyWith(status: UserStatus.failure, isLoading: false));
    }
  }

  // delete user event
  void _onDeleteUser(DeleteUserEvent event, Emitter<UserState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // delete one, many users
      await repository.deleteUser(event.userId);
      // get users after deleting
      final users = repository.getListConverted();
      // update states
      emit(state.copyWith(
        status: UserStatus.success,
        users: users,
        isLoading: false,
      ));
    } catch (_) {
      emit(state.copyWith(status: UserStatus.failure, isLoading: false));
    }
  }

  // delete user event
  void _onSpecificUser(SpecificUserEvent event, Emitter<UserState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // get one user with user id

      // update states
      emit(state.copyWith(status: UserStatus.success, isLoading: false));
    } catch (_) {
      emit(state.copyWith(status: UserStatus.failure, isLoading: false));
    }
  }
}
