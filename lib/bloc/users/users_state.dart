import 'package:equatable/equatable.dart';
import 'package:tv_menu/models/models.dart';

// [092302TREE]

enum UserStatus { initial, success, failure }

class UserState extends Equatable {
  final UserStatus status;
  final List<User> users;

  final bool hasLoadedAll;

  final bool isLoading;

  final String error;

  const UserState({
    this.status = UserStatus.initial,
    this.users = const <User>[],
    this.hasLoadedAll = false,
    this.isLoading = false,
    this.error = '',
  });

  UserState copyWith({
    UserStatus? status,
    List<User>? users,
    bool? hasLoadedAll,
    bool? isLoading,
    String? error,
  }) {
    return UserState(
      status: status ?? this.status,
      users: users ?? this.users,
      hasLoadedAll: hasLoadedAll ?? this.hasLoadedAll,
      isLoading: isLoading ?? this.isLoading,
      error: error ?? this.error,
    );
  }

  @override
  List<Object> get props => [
        status,
        users,
        hasLoadedAll,
        isLoading,
        error,
      ];
}
