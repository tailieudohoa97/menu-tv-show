import 'package:equatable/equatable.dart';
import 'package:tv_menu/models/models.dart';

// [092302TREE]

abstract class UserEvent extends Equatable {
  @override
  List<Object> get props => [];
}

// fetch all users
class UserFetchEvent extends UserEvent {}

// fetch previous page
class PreviousPageEvent extends UserEvent {}

// fetch next page
class NextPageEvent extends UserEvent {}

// fetch single page
class SpecificUserEvent extends UserEvent {
  final int userId;

  SpecificUserEvent({required this.userId});

  @override
  List<Object> get props => [userId];
}

// new user
class AddUserEvent extends UserEvent {
  final User user;

  AddUserEvent({required this.user});

  @override
  List<Object> get props => [user];
}

// update user
class UpdateUserEvent extends UserEvent {
  final User user;

  UpdateUserEvent({required this.user});

  @override
  List<Object> get props => [user];
}

// delete user
class DeleteUserEvent extends UserEvent {
  final String userId;

  DeleteUserEvent({required this.userId});

  @override
  List<Object> get props => [userId];
}

// search user
class SearchUserEvent extends UserEvent {
  final String keyword;

  SearchUserEvent({required this.keyword});

  @override
  List<Object> get props => [keyword];
}

// selected users changed
class SelectedUserEvent extends UserEvent {
  final List<User> users;

  SelectedUserEvent({required this.users});

  @override
  List<Object> get props => [users];
}
