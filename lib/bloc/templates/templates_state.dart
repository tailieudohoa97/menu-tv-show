import 'package:equatable/equatable.dart';
import 'package:tv_menu/models/models.dart';

// [092302TREE]

enum TemplateStatus { initial, success, failure }

class TemplateState extends Equatable {
  final TemplateStatus status;
  final List<Template> templates;

  final bool hasLoadedAll;

  final bool isLoading;

  final String error;

  const TemplateState({
    this.status = TemplateStatus.initial,
    this.templates = const <Template>[],
    this.hasLoadedAll = false,
    this.isLoading = false,
    this.error = '',
  });

  TemplateState copyWith({
    TemplateStatus? status,
    List<Template>? templates,
    bool? hasLoadedAll,
    bool? isLoading,
    String? error,
  }) {
    return TemplateState(
      status: status ?? this.status,
      templates: templates ?? this.templates,
      hasLoadedAll: hasLoadedAll ?? this.hasLoadedAll,
      isLoading: isLoading ?? this.isLoading,
      error: error ?? this.error,
    );
  }

  @override
  List<Object> get props => [
        status,
        templates,
        hasLoadedAll,
        isLoading,
        error,
      ];
}
