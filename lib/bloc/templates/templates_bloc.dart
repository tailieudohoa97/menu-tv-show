import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/bloc/templates/templates_event.dart';
import 'package:tv_menu/bloc/templates/templates_state.dart';
import 'package:tv_menu/repo/repository/template_repository.dart';

class TemplateBloc extends Bloc<TemplateEvent, TemplateState> {
  final TemplateRepository repository;

  // [092302TREE]

  TemplateBloc({required this.repository}) : super(TemplateState()) {
    on<TemplateFetchEvent>(_onTemplateFetch);
    on<AddTemplateEvent>(_onAddTemplate);
    on<UpdateTemplateEvent>(_onUpdateTemplate);
    on<DeleteTemplateEvent>(_onDeleteTemplate);
    on<ChangeStatusTemplateEvent>(_onChangeStatusTemplate);
    on<SearchTemplateEvent>(_onSearchTemplate);
  }

  // fetch templates event
  void _onTemplateFetch(
      TemplateFetchEvent event, Emitter<TemplateState> emit) async {
    try {
      // if status is initial

      // loading state
      emit(state.copyWith(isLoading: true));
      // get list converted
      dynamic listConverted;
      listConverted = repository.getListConverted();
      // check list current in box is empty , true -> fetch 17 items
      if (listConverted.isEmpty) {
        await repository.getAllTemplates();
        listConverted = repository.getListConverted();
      }
      // get templates with page equal current page = 0 in initial status

      // update states
      emit(state.copyWith(
        status: TemplateStatus.success,
        templates: listConverted,
        isLoading: false,
      ));
    } catch (error) {
      // got error

      emit(state.copyWith(
          status: TemplateStatus.failure,
          isLoading: false,
          error: error.toString()));
    }
  }

  // add new template event
  void _onAddTemplate(
      AddTemplateEvent event, Emitter<TemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // add new template to box
      await repository.addOrEditTemplateOfBox(event.template, "addType");
      // get templates after adding new template
      final templates = repository.getListConverted();

      // delayed loading during one second

      // update states
      emit(state.copyWith(
        status: TemplateStatus.success,
        templates: templates,
        isLoading: false,
      ));
    } catch (_) {
      emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
    }
  }

  // update template event
  void _onUpdateTemplate(
      UpdateTemplateEvent event, Emitter<TemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // edit template to box
      await repository.addOrEditTemplateOfBox(event.template, "editType");
      // get templates after updating
      dynamic templates = repository.getListConverted();
      // delayed loading during one second

      // update states
      emit(state.copyWith(
        status: TemplateStatus.success,
        templates: templates,
        isLoading: false,
      ));
    } catch (_) {
      emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
    }
  }

  // delete template event
  void _onDeleteTemplate(
      DeleteTemplateEvent event, Emitter<TemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // delete one, many templates
      await repository.deleteTemplate(event.templateId);
      // get templates after deleting
      final templates = repository.getListConverted();
      // update states
      emit(state.copyWith(
        status: TemplateStatus.success,
        templates: templates,
        isLoading: false,
      ));
    } catch (_) {
      emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
    }
  }

  // delete template event
  void _onChangeStatusTemplate(
      ChangeStatusTemplateEvent event, Emitter<TemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // delete one, many templates
      await repository.changeStatusTemplate(event.templateId, event.status);
      // get templates after deleting
      final templates = repository.getListConverted();
      // update states
      emit(state.copyWith(
        status: TemplateStatus.success,
        templates: templates,
        isLoading: false,
      ));
    } catch (_) {
      emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
    }
  }

  // search template event
  void _onSearchTemplate(
      SearchTemplateEvent event, Emitter<TemplateState> emit) async {
    // loading state
    emit(state.copyWith(isLoading: true));
    try {
      // delete one, many templates
      await repository.searchTemplates(event.keyword);
      // get templates after deleting
      final templates = repository.getListConverted();
      // update states
      emit(state.copyWith(
        status: TemplateStatus.success,
        templates: templates,
        isLoading: false,
      ));
    } catch (_) {
      emit(state.copyWith(status: TemplateStatus.failure, isLoading: false));
    }
  }
}
