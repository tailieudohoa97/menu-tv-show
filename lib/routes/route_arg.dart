import '../data/enum.dart';

class RouteArguments {
  ARGSKEY key;
  dynamic val;
  RouteArguments({
    required this.key,
    required this.val,
  });
}
