class Routes {
  static const String HomeRoute = "/";
  static const String TemplateScreenRoute = "/template";
  static const String HomeScreenRoute = "/home";
  static const String AdminLoginRoute = "/admin/login";
  static const String forGotPasswordRouter = "/admin/forgotpassword";
  static const String AdminDashboardRoute = "/admin/dashboard";
  static const String AdminBuilderRoute = "/admin/builder";
}
