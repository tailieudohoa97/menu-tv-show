import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../controller/template_controller.dart';
import '../swapper_page.dart';

class RouterConfigApp {
  static GoRouter router = GoRouter(
    routes: <RouteBase>[
      GoRoute(
          path: '/',
          builder: (context, state) {
            return SwapperScreen();
          }),
      //Note: Example url: http://localhost:52486/#/template/2 or http://localhost:52486/#/template/1
      GoRoute(
        path: '/template/:id',
        builder: (BuildContext context, GoRouterState state) {
          return Scaffold(
              body: TemplateControllerState(
            idTemplate: state.pathParameters['id']!,
            userId: '',
          ));
        },
      ),
    ],
  );
}
