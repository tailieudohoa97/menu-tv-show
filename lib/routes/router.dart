import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import 'package:tv_menu/helper/check_param_page.dart';
import 'package:tv_menu/swapper_page.dart';

class RouterConfigApp {
  static GoRouter router = GoRouter(
    routes: <RouteBase>[
      GoRoute(
          path: '/',
          builder: (context, state) {
            return SwapperScreen();
          }),
      //Note: Example url: http://localhost:12345/#/template/2 or http://localhost:12345/#/template/0xAAA566B79BAF999B69DB2C8ED2FC5758
      GoRoute(
        path: '/template/:tokenUserTemplateOrTemplateId',
        builder: (BuildContext context, GoRouterState state) {
          return Scaffold(
              body: CheckParamScreen(
            param: state.pathParameters['tokenUserTemplateOrTemplateId']!,
          ));
        },
      ),
    ],
  );
}
