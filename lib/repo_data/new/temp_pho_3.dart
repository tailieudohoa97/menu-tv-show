class JsonDataTemplate {
  // [112321TIN] run ok - ratio ok
  static final temppho3 = {
    "id": "13",
    "templateName": "temp pho 3",
    "templateSlug": "temp_pho_3",
    "description": null,
    "subscriptionLevel": "Medal",
    "image": "https://foodiemenu.co//wp-content/uploads/2023/11/temp_pho_3.png",
    "categoryId": "3",
    "ratios": "16:9,16:10,21:9,4:3",
    "ratioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp"
        },
        "21/9": {
          "categoryFontSize": "3.5.sp",
          "verticalFramePadding": "2.sp",
          "horizontalFramePadding": "2.sp",
          "categoryBlockHeight": "25.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp"
        },
        "16/9": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp"
        },
        "16/10": {
          "categoryFontSize": "6.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "23.h",
          "brandFontSize": "12.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp"
        },
        "4/3": {
          "categoryFontSize": "7.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "23.h",
          "brandFontSize": "15.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp"
        },
        "default": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp"
        }
      }
    },
    "widgetSetting": {
      "id": "b955d3aa-8a09-47bd-a509-39a95b3d113a",
      "widgetName": "Stack",
      "widgetSetting": {
        "children": [
          {
            "id": "c8526528-de4d-4267-96e7-b86ccf7b1dc9",
            "widgetName": "Container",
            "widgetSetting": {
              "height": "100.h",
              "width": "100.w",
              "decoration": {"color": "0xff0d507e"},
              "child": {
                "id": "1a41a0f7-368e-488e-879f-97b47b091fc3",
                "widgetName": "PaddingByRatio",
                "widgetSetting": {
                  "verticalPaddingFieldKey": "verticalFramePadding",
                  "horizontalPaddingFieldKey": "horizontalFramePadding",
                  "child": {
                    "id": "3f6ce101-79b4-445b-8558-17c59b6edfea",
                    "widgetName": "Column",
                    "widgetSetting": {
                      "mainAxisAlignment": "center",
                      "children": [
                        {
                          "id": "569a1e85-f5bb-4981-a257-14efd76d2c21",
                          "widgetName": "Text",
                          "widgetSetting": {
                            "data": "PHO BRAND",
                            "fontSizeFromFieldKey": "brandFontSize",
                            "style": {
                              "fontFamily": "Staatliches",
                              "color": "0xffffaf45"
                            }
                          }
                        },
                        {
                          "id": "06d83380-b1f0-46cc-8657-7731fcb5c423",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "id": "1ac1992d-2e32-42fb-9024-c7661df23e41",
                              "widgetName": "Wrap",
                              "widgetSetting": {
                                "direction": "vertical",
                                "spacing": "5.sp",
                                "runSpacing": "10.sp",
                                "clipBehavior": "hardEdge",
                                "children": [
                                  {
                                    "id":
                                        "2d24efac-c527-4328-9c9a-7df6bb9e944c",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      "categoryImage": {
                                        "id":
                                            "9d9fbd52-1ca2-4d86-890b-38ec753bdeaf",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat1Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp"
                                        }
                                      },
                                      "category": "category1",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      "categoryItemRows": [
                                        {
                                          "id":
                                              "5f4f5200-e8b4-496e-9f0e-18817e3cfc3d",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "768a25c7-b2f2-431a-998d-36be2ddc20a5",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product1",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "1b58031b-d319-4494-81a0-1b9592da2e83",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "47b60b52-6b11-4b73-ad0f-0963b3b6fe39",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product2",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "9cec7627-a346-4576-91e9-39476e0a7be3",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "aaed778d-4b8f-486e-a4cb-7ea75cce136f",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product3",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "6b5c659e-573a-476b-a6cd-b2bd93bfa299",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "7dbceefb-9209-4760-af52-932ca5ab5a03",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product4",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "ac8567e0-d8db-434f-a840-41457e48e18c",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "b8743e11-0917-4c3c-ac0a-b1ec63c1ca89",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product5",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        }
                                      ]
                                    }
                                  },
                                  {
                                    "id":
                                        "d566658c-5741-4a6f-934c-5fb4f0b9a99e",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      "categoryImage": {
                                        "id":
                                            "ac2ae1b7-5756-4027-8f2d-4eddac5dd1ca",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat2Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp"
                                        }
                                      },
                                      "category": "category2",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      "categoryItemRows": [
                                        {
                                          "id":
                                              "b02870ac-9050-4727-925f-44dfad22ffe3",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "3ba25396-c638-4aee-901f-c89ced57ae4d",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product6",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "eec207de-f171-4915-a7eb-6770daf1fa5f",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "e708d394-bd07-4455-9dd0-def085beb26e",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product7",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "45fc5ff3-4197-480c-ab1c-6067aa070034",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "4911a284-7d9a-49d6-b753-ac7a4038a148",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product8",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "93b1742f-e899-46f4-80ca-5e8fceb842b6",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "ad2fed65-855b-4b19-81b2-f25030c5424d",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product9",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "8df788fc-4446-494e-9ab3-63d96ae77895",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "bc97ce22-f361-41e7-b679-a7be7081085a",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product10",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        }
                                      ]
                                    }
                                  },
                                  {
                                    "id":
                                        "b78e6b2c-f4c6-4514-b1d4-43c760db01fa",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      "categoryImage": {
                                        "id":
                                            "e9ba2f43-4f51-4cfd-ac73-743c973ae993",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat3Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp"
                                        }
                                      },
                                      "category": "category3",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      "categoryItemRows": [
                                        {
                                          "id":
                                              "789fae65-1f56-4d61-82cb-115d7ce49b7a",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "094f349a-58e5-4762-a291-a4d21cfcc222",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product11",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "ebed48d7-31d0-4e6d-a8a2-07060f321f37",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "b68c4ac6-6668-41e3-a592-e7435de194cc",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product12",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "c2c336b7-ef44-495e-aef6-3d061e9ad029",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "3e20867a-a7b4-4ca0-a186-fbbba2e013aa",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product13",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "04071c4b-3628-4e6e-8c83-b06480e7d408",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "e4d01a4f-7f52-49c6-adb8-cac504bc9474",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product14",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "d2e35f17-dc1c-4074-b9a2-d56f7867ad9a",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "cfbfd54b-1ac2-4c00-98a3-fcd565dffff0",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product15",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        }
                                      ]
                                    }
                                  },
                                  {
                                    "id":
                                        "391da0b6-944b-4d5b-8cfb-48dcb27269d4",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      "categoryImage": {
                                        "id":
                                            "efb14690-397c-4d72-830e-ac369bcd32e4",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat3Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp"
                                        }
                                      },
                                      "category": "category4",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      "categoryItemRows": [
                                        {
                                          "id":
                                              "a39f8d8d-c45b-40f6-a4ed-97e6b8844cf8",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "2d531c71-3ab5-4f9b-8233-94a9e02dc4ac",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product16",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "dde1b8e4-322c-4ce5-846c-806cbf039dde",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "40552e09-9ec3-4ccc-a2d7-cfd334dfbf4c",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product17",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "ff0a2bc3-f8b5-4b80-9c15-951ba905dae4",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "1a8ffce1-c151-45d8-a750-0d0b7d278cae",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product18",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "fc0eea46-cd09-4b93-8647-cbfec7fada8f",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "49985df3-0ded-4978-8668-2a7f36f56cdb",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product19",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "76c69fcf-9673-4680-88ad-4f93ed503620",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "43f5d970-6d0e-4d7a-80fe-339855354da5",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product20",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        }
                                      ]
                                    }
                                  },
                                  {
                                    "id":
                                        "45c9edd8-7633-4d95-8a66-58caa212d1ad",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      "categoryImage": {
                                        "id":
                                            "1927b8f6-3e59-4b1a-bc1a-ba98332cdb31",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat1Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp"
                                        }
                                      },
                                      "category": "category5",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      "categoryItemRows": [
                                        {
                                          "id":
                                              "0871ceca-cdf6-4761-a3c7-ace73c15d4b7",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "4d9edb1e-1252-4e9c-aafe-7597fb506f6d",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product21",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "335a9a10-e4bc-45bd-9135-3f041ad0f9bc",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "d1cfb40e-948b-43a8-ba3d-db193b0d720e",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product22",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "9cdc7708-8258-4ef1-8321-760724ca33e6",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "90b55edd-7cbe-4877-95b9-a9872ab60965",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product23",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "5be19b8b-fed8-46ce-8801-6c2f4f0882f2",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "19a0c9d1-5152-42bf-b527-482e6caa4bfe",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product24",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "3fb0d303-d6fc-4263-ac02-58446281713d",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "8f0c601b-8a66-4995-914e-a9f5aef58718",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product25",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        }
                                      ]
                                    }
                                  },
                                  {
                                    "id":
                                        "3147146d-21ad-47f9-9112-0a01d19f50e4",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      "categoryImage": {
                                        "id":
                                            "96aa7b76-07f0-4bcc-9a34-81a5f22c71f2",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat2Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp"
                                        }
                                      },
                                      "category": "category6",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      "categoryItemRows": [
                                        {
                                          "id":
                                              "9d5f768b-2645-4db5-b6a8-e43698eb31cd",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "eae26318-32ba-4e85-9540-5d1dfbdab4fe",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product26",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "a6f4881f-8d7e-4050-9ca1-739bbfe6ec32",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "4b525061-b158-42e9-ba09-def0392933ae",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product27",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "6db3c468-633a-4990-9911-d700abbb87f0",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "f285492d-0079-4737-8189-b9f2886b73eb",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product28",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "e280aab3-cc45-477e-ad96-31050861678f",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "b3894abf-24c0-4b24-bffb-c6e3f502898a",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product29",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "0719a545-9217-4236-8e8d-4b701a9ca53c",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "id":
                                                  "4818bcc3-ed24-46df-b797-dbef434a22b4",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product30",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins"
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins"
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1"
                                              }
                                            }
                                          }
                                        }
                                      ]
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              }
            }
          },
          {
            "id": "48d4fa61-f2df-4d04-b426-eb2679a6d20e",
            "widgetName": "Positioned",
            "widgetSetting": {
              "bottom": "-14.sp",
              "child": {
                "id": "786cf7bc-b938-4388-b19a-adb183459a77",
                "widgetName": "Image",
                "widgetSetting": {
                  "imageId": "bottomDecoImage",
                  "fixCover": "true",
                  "width": "100.w",
                  "height": "100.h"
                }
              }
            }
          },
          {
            "id": "856dd544-0617-4fb7-b90e-9f58ba11a0dd",
            "widgetName": "Positioned",
            "widgetSetting": {
              "left": "0",
              "child": {
                "id": "36c904e4-422b-4705-b895-0f5af91e7c19",
                "widgetName": "Image",
                "widgetSetting": {
                  "imageId": "leftTopDecoImage",
                  "fixCover": "false",
                  "width": "30.sp",
                  "height": "30.sp"
                }
              }
            }
          },
          {
            "id": "451dd23e-abc6-4e11-ae2e-a6a7b611ec17",
            "widgetName": "Positioned",
            "widgetSetting": {
              "right": "0",
              "child": {
                "id": "67ea4360-eba0-4ef4-9896-a7f5b5239128",
                "widgetName": "Image",
                "widgetSetting": {
                  "imageId": "rightTopDecoImage",
                  "fixCover": "false",
                  "width": "30.sp",
                  "height": "30.sp"
                }
              }
            }
          }
        ]
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": ""
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": ""
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": ""
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": ""
      },
      "category5": {
        "key": "category5",
        "type": "category",
        "name": "Category 5",
        "image": ""
      },
      "category6": {
        "key": "category6",
        "type": "category",
        "name": "Category 6",
        "image": ""
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product21": {
        "key": "product21",
        "type": "product",
        "name": "Product 21",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product22": {
        "key": "product22",
        "type": "product",
        "name": "Product 22",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product23": {
        "key": "product23",
        "type": "product",
        "name": "Product 23",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product24": {
        "key": "product24",
        "type": "product",
        "name": "Product 24",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product25": {
        "key": "product25",
        "type": "product",
        "name": "Product 25",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product26": {
        "key": "product26",
        "type": "product",
        "name": "Product 26",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product27": {
        "key": "product27",
        "type": "product",
        "name": "Product 27",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product28": {
        "key": "product28",
        "type": "product",
        "name": "Product 28",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product29": {
        "key": "product29",
        "type": "product",
        "name": "Product 29",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product30": {
        "key": "product30",
        "type": "product",
        "name": "Product 30",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "leftTopDecoImage": {
        "key": "leftTopDecoImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/left_top_deco.png"
      },
      "rightTopDecoImage": {
        "key": "rightTopDecoImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/right_top_deco.png"
      },
      "bottomDecoImage": {
        "key": "bottomDecoImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/bottom_deco.png"
      },
      "cat1Image": {
        "key": "cat1Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1-1.jpg"
      },
      "cat2Image": {
        "key": "cat2Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2-1.jpg"
      },
      "cat3Image": {
        "key": "cat3Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3-1.jpg"
      },
      "circleBorderImage": {
        "key": "circleBorderImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/circle_border.png"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
