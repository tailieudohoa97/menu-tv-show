import "temp_res_1.dart" as temp_res_1;
import "temp_res_2.dart" as temp_res_2;
import "temp_res_3.dart" as temp_res_3;
import "temp_res_4.dart" as temp_res_4;
import "temp_res_5.dart" as temp_res_5;
import "temp_nail_1.dart" as temp_nail_1;
import "temp_nail_2.dart" as temp_nail_2;
import "temp_nail_3.dart" as temp_nail_3;
import "temp_nail_4.dart" as temp_nail_4;
import "temp_nail_5.dart" as temp_nail_5;
import "temp_pho_1.dart" as temp_pho_1;
import "temp_pho_2.dart" as temp_pho_2;
import "temp_pho_3.dart" as temp_pho_3;
import "temp_pho_4.dart" as temp_pho_4;
import "temp_pho_5.dart" as temp_pho_5;

class JsonDataTemplate {
  static final res_1_json = temp_res_1.JsonDataTemplate.tempres1;
  static final res_2_json = temp_res_2.JsonDataTemplate.tempres2;
  static final res_3_json = temp_res_3.JsonDataTemplate.tempres3;
  static final res_4_json = temp_res_4.JsonDataTemplate.tempres4;
  static final res_5_json = temp_res_5.JsonDataTemplate.tempres5;

  static final nail_1_json = temp_nail_1.JsonDataTemplate.tempnail1;
  static final nail_2_json = temp_nail_2.JsonDataTemplate.tempnail2;
  static final nail_3_json = temp_nail_3.JsonDataTemplate.tempnail3;
  static final nail_4_json = temp_nail_4.JsonDataTemplate.tempnail4;
  static final nail_5_json = temp_nail_5.JsonDataTemplate.tempnail5;

  static final pho_1_json = temp_pho_1.JsonDataTemplate.temppho1;
  static final pho_2_json = temp_pho_2.JsonDataTemplate.temppho2;
  static final pho_3_json = temp_pho_3.JsonDataTemplate.temppho3;
  static final pho_4_json = temp_pho_4.JsonDataTemplate.temppho4;
  static final pho_5_json = temp_pho_5.JsonDataTemplate.temppho5;
}
