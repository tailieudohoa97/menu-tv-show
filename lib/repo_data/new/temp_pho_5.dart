class JsonDataTemplate {
  // [112321TIN] run ok - ratio ok
  static final temppho5 = {
    "id": "15",
    "templateName": "Nhà hàng Phở Hoàng Giang Canada",
    "templateSlug": "temp_pho_5",
    "description": null,
    "subscriptionLevel": "Medal",
    "image": "https://foodiemenu.co//wp-content/uploads/2023/11/temp_pho_5.png",
    "categoryId": "3",
    "ratios": "16:9,16:10,21:9,4:3",
    "ratioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryNameFontSize": "5.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp"
        },
        "21/9": {
          "categoryNameFontSize": "4.sp",
          "productNameFontSize": "2.5.sp",
          "productPriceFontSize": "2.5.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "13.h",
          "columnGap": "2.sp"
        },
        "16/9": {
          "categoryNameFontSize": "6.sp",
          "productNameFontSize": "3.5.sp",
          "productPriceFontSize": "3.5.sp",
          "productDesFontSize": "2.5.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "35.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp"
        },
        "16/10": {
          "categoryNameFontSize": "8.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "2.8.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "35.sp",
          "productRowHeight": "12.5.h",
          "columnGap": "5.sp"
        },
        "4/3": {
          "categoryNameFontSize": "8.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "3.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "40.sp",
          "productRowHeight": "12.h",
          "columnGap": "6.sp"
        },
        "default": {
          "categoryNameFontSize": "5.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp"
        }
      }
    },
    "widgetSetting": {
      "id": "6b4f2a25-1ba9-4929-86b2-d9ff3418a07e",
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "padding": {"all": "5.sp"},
        "decoration": {"color": "0xfffdf6e9"},
        "child": {
          "id": "441aa617-87ed-4600-ac7a-ba496b85f18a",
          "widgetName": "Center",
          "widgetSetting": {
            "child": {
              "id": "6f05ab81-70fd-4c94-9103-91c4d1a2c84e",
              "widgetName": "Row",
              "widgetSetting": {
                "crossAxisAlignment": "start",
                "mainAxisAlignment": "center",
                "children": [
                  {
                    "id": "e3dce3c8-bcef-4722-be66-9c8e8808c820",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "id": "2fbb398a-b7df-4d6c-8448-15f3223e1535",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category1",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828"
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp"
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            {
                              "id": "c7e7c6fe-5b01-4571-91c4-62b90778fa9f",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "7cb46ada-6999-4b62-affa-4450a91c3c61",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "65a8e38a-036e-4b0f-81c8-ba89b074aab9",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product1",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "92f0fbd6-3a7a-4d86-acaa-ca45a7e141c3",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "dfa7fa50-6d91-403f-9494-8bf240ff1825",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "b652fdd9-3070-43d1-8e1e-a3a69502034c",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "261e97b9-60f8-46f4-b16f-4f033065fa5f",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product2",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "f4ce491d-2e15-41ba-a802-d86c58e3d287",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "eb5badc9-d57d-42d5-9224-d0aeb3f212ab",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "3bd813e1-7d91-4574-a39e-8327442c733b",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "372b46f9-0f65-4ce2-b44f-b4c9ffe56b82",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product3",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "4eefeb9a-9bca-4cd7-98ca-dc67da37747a",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "cd1ab3a5-b121-41f3-85c1-088fa9d4ec5d",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "441d13ef-5ea6-4484-aa7a-695fbb3b8307",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "b148dd1d-be9c-4929-ba36-ddbcb6b87351",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product4",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "32b976ff-a2de-4af3-ac86-48ce9ecb06f8",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "ee2bde9d-59d7-442d-83c3-7bacb794d535",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "5d7d8ee4-3555-455a-bb66-7239fc5e3bb8",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "947f0b05-3322-4ad5-9651-a78a5cb240b2",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product5",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "eed00599-6fb3-4b2e-b235-c885235e4be9",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  },
                  {
                    "id": "a19a1b4e-79fb-450d-90f8-f30d1521a65d",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "id": "bf6531fe-572a-4663-bc38-4c8390a8a579",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category2",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828"
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp"
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            {
                              "id": "087a618e-4b53-4827-8019-29338860f12c",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "42226af7-457a-4060-aa32-4701c0432b8b",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "9ec6dc8e-1854-4114-87ba-4e973d3cd5ab",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product6",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "a0456f21-0bb6-4b96-b869-888088876374",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "c89ada90-0eb4-4282-a7fa-ea95c6b22682",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "child": {
                                  "id": "de82b869-b5c9-4b49-9664-ec9edb2e56ea",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "74e52fe9-0852-4b65-9a4c-23b67aaa1534",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product7",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "a284f064-0a6f-490b-842e-9e584a28ddf5",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "28ac5674-eb77-48fb-9454-00eeac6311ff",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "2a414632-1d88-4d77-bf1b-d29062a35168",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "9953831b-5094-4695-a55f-c95eee27fb71",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product8",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "5ce70738-f876-4b6c-af61-98c0048b6211",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "6578403a-f3cb-44da-9e4b-dc48977f377e",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "bf7a7fd1-9e60-4c33-a30c-51f338b99e88",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "cc329c67-3afe-439f-bbf7-a066a1f009f0",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product9",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "efdcc3d7-4e55-41ca-bbb1-3fd630048853",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "7a424610-c066-4920-9eed-046f0d366c09",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "cabe5b73-5b02-4e5d-92d5-b8b3c5f4d09e",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "4d7fb8cc-d352-461b-b392-0f8ce4598ff3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product10",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "76a404e3-fcb0-4a6a-8c9c-cb2521c6117a",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  },
                  {
                    "id": "0598a268-0fcc-437a-abd5-c177259ac2f4",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "id": "629aaa8e-589b-4c4d-99a2-0ec584f02486",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category3",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828"
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp"
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            {
                              "id": "1780ae88-ef0f-4e3a-b482-2078751b388b",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "b45f2b3f-3d00-4668-829e-7a00a21d70bb",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "9dd99f11-1d64-4351-af01-e9192a9057d7",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product11",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "21dcdcdb-e967-48cc-9b8d-97d5c470c5b6",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "8e01fdc7-7c22-4602-92a9-c2de66e4e1a9",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "0e106c0f-3629-4afa-a89a-defd4a21f6f8",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "d48c4f77-50da-42a8-9a06-4f0b0e828f36",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product12",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "612271f8-6fe3-4af4-a484-f2dac4909616",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "8d55d584-6339-44b4-a392-69aebd6cfa7f",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "a68b80ea-7614-46e5-abd3-38f955a4b7fc",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "c2010ade-4a0c-4e45-9405-101f5451e3eb",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product13",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "485805cc-a715-4241-9946-1776fbf61198",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "97e1ad0d-1e9b-4d8b-abdc-6d3034a7316a",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "cd5d796e-c776-4f95-b779-64f00cc1b6ef",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "02fc6b93-2339-4123-87fc-5a2d4bd339d9",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product14",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "535e60e4-cf7d-4414-848a-2a4b8966db78",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "8401a4a2-b573-4117-8ebe-cf747e17bab5",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "a43064f6-adfb-4a2d-b59d-6d29b3b7d084",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "6155e556-ab7f-4f3d-9ede-ce90858673d9",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product15",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "c300b043-cc3f-406c-baad-35a754bfc94e",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  },
                  {
                    "id": "2a2b4e79-d75e-419d-a29f-e06644b4161f",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "id": "85c86281-b2b7-45e8-adc5-6db3db6e492d",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category4",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828"
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp"
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            {
                              "id": "5ebc2f37-e872-4daa-b4db-694bb6bdd5c5",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "a5bffaf4-722a-4923-b7f5-d6475cbadc6d",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "dd430722-b541-486b-ab21-9a41dbe610f9",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product16",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "43fe3552-d566-47e3-a586-cdbf5f33cfae",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "b02ff1b9-92b1-4708-a52c-808f7b1b5ca3",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "24df0d11-3359-441a-94d8-94d62b4cb1ec",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "5bacf0fe-4f75-4bc3-bfa8-3e9a5114d2d0",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product17",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "c386e0db-79b2-400d-a60a-89d66e25adbf",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "e230d0c1-f129-45f4-9eb4-ffc478c0835c",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "a83b8d70-01ee-46c5-9dac-0cebb15210e5",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "61f21fae-3884-4b21-830b-6d1dcc9c51d7",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product18",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "b147e2f1-5f6b-4433-9920-634057e44cbb",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "ade148ad-8215-466a-b0dd-13033302eb4d",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "c9928467-126d-4dd1-9064-68f61d1012b9",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "cd70f223-0276-4575-bd10-8bb2eaf8d74e",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product19",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "c8a4c5c6-e4db-41c1-b481-871eac6bbc9c",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "fc3932de-3e99-45cd-82f5-f43a8215ff6a",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "id": "2436b690-69cb-4c0f-adb7-68994d559a9f",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "id":
                                          "d4f60fa9-2884-420f-acd5-b71efc7e21a1",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product20",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                          "color": "0xff000000"
                                        },
                                        "horizontalDivider": {
                                          "id":
                                              "7f9d6351-0661-46f0-9453-2b7150076c94",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a"
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true"
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/cat_1-2.jpg"
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/cat_2-2.jpg"
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/cat_3-2.jpg"
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/cat_4-1.jpg"
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/pho_1.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/pho_2.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/pho_3.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/pho_4.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/pho_1.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": ""
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": ""
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": ""
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
