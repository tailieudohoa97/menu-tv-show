class JsonDataTemplate {
  // [112321TIN] run ok - ratio ok
  static final tempnail2 = {
    "id": "7",
    "templateName": "temp nail 2",
    "templateSlug": "temp_nail_2",
    "description": null,
    "subscriptionLevel": "Medal",
    "image":
        "https://foodiemenu.co//wp-content/uploads/2023/11/temp_nail_2.png",
    "categoryId": "2",
    "ratios": "16:9,16:10,21:9,32:9,4:3",
    "ratioSetting": {
      "textStyle": {
        "categoryNameStyle": {
          "fontWeight": "w700",
          "fontFamily": "Nunito Sans"
        },
        "nameProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "Nunito Sans",
          "height": "1.5"
        },
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Nunito Sans",
          "height": "1.5"
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Nunito Sans",
          "height": "1.5"
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "Nunito Sans",
          "height": "1.5"
        },
        "priceProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "Nunito Sans"
        },
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Nunito Sans"
        }
      },
      "renderScreen": {
        "32/9": {
          "categoryNameStyle": "1.w",
          "nameProductStyle": "1.w",
          "priceProductStyle": "0.9.w",
          "discriptionProductStyle": "0.55.w"
        },
        "21/9": {
          "categoryNameStyle": "1.6.w",
          "nameProductStyle": "1.4.w",
          "priceProductStyle": "1.4.w",
          "discriptionProductStyle": "0.8.w"
        },
        "16/9": {
          "categoryNameStyle": "1.7.w",
          "nameProductStyle": "1.5.w",
          "priceProductStyle": "1.5.w",
          "discriptionProductStyle": "1.3.w"
        },
        "16/10": {
          "categoryNameStyle": "1.9.w",
          "nameProductStyle": "1.7.w",
          "priceProductStyle": "1.7.w",
          "discriptionProductStyle": "1.1.w"
        },
        "4/3": {
          "categoryNameStyle": "2.8.w",
          "nameProductStyle": "2.4.w",
          "priceProductStyle": "2.4.w",
          "discriptionProductStyle": "1.2.w"
        },
        "default": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.25.w",
          "priceProductStyle": "1.25.w"
        }
      }
    },
    "widgetSetting": {
      "id": "b7919952-c560-44e2-bf62-e4e85d7ded3a",
      "widgetName": "CustomContainer",
      "widgetSetting": {
        "color": "0x4400539C",
        "blendMode": "dstATop",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/background-scaled.jpg",
        "width": "100.w",
        "height": "100.h",
        "child": {
          "id": "e4a72cd8-c10c-40f3-9c84-33af20889f35",
          "widgetName": "Row",
          "widgetSetting": {
            "children": [
              {
                "id": "e358ccd1-35a2-42ed-a525-036cdce06d6a",
                "widgetName": "Container",
                "widgetSetting": {
                  "width": "33.33.w",
                  "padding": {"bottom": "4.5.h", "horizontal": "3.w"},
                  "child": {
                    "id": "faf822a0-7e6a-4c56-a062-2660e27fb9c0",
                    "widgetName": "Column",
                    "widgetSetting": {
                      "children": [
                        {
                          "id": "7c59b40c-7f28-49d1-b0e2-f70364988867",
                          "widgetName": "SizedBox",
                          "widgetSetting": {"height": "3.h"}
                        },
                        {
                          "id": "82bd1fda-0e2d-468e-aa62-aafe87c8e321",
                          "widgetName": "Divider",
                          "widgetSetting": {
                            "thickness": "2",
                            "height": "6.h",
                            "color": "0x8F634201"
                          }
                        },
                        {
                          "id": "9c660924-3d31-4b0c-9e41-fa3a055ed7d9",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "id": "184392ba-51e1-49b4-931b-8d4ac709087a",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "mainAxisAlignment": "spaceBetween",
                                "children": [
                                  {
                                    "id":
                                        "a9d2a03d-992b-4067-9a43-43e84aea504f",
                                    "widgetName": "CategoryTitle",
                                    "widgetSetting": {
                                      "categoryId": "category1",
                                      "color": "0xFF895B3A"
                                    }
                                  },
                                  {
                                    "id":
                                        "1a0afa48-91fb-4860-a66c-847f9b1b3ad2",
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product1",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false"
                                    }
                                  },
                                  {
                                    "id":
                                        "6db3cf6a-7387-42eb-a45d-40c045207977",
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product2",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false"
                                    }
                                  },
                                  {
                                    "id":
                                        "791882e0-385e-4380-85ef-1fa739490a34",
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product3",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false"
                                    }
                                  },
                                  {
                                    "id":
                                        "70990e24-3aeb-4c84-89ec-77889fc2dc17",
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product4",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false"
                                    }
                                  },
                                  {
                                    "id":
                                        "bb2a47c9-5fd0-4562-877b-8d1c5e76b52e",
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product5",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false"
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              },
              {
                "id": "6d8d7e9d-dfca-4258-af82-d5be8ced2dca",
                "widgetName": "VerticalDivider",
                "widgetSetting": {"thickness": "2", "color": "0x8F634201"}
              },
              {
                "id": "5f9983a3-6b27-4d61-a470-c18f6946b3fc",
                "widgetName": "Container",
                "widgetSetting": {
                  "width": "33.33.w",
                  "padding": {"bottom": "4.5.h", "horizontal": "3.w"},
                  "child": {
                    "id": "1a42d542-f2df-4ffe-b31d-a23c3dbaac20",
                    "widgetName": "Column",
                    "widgetSetting": {
                      "children": [
                        {
                          "id": "96ab61ca-ac3e-409d-adef-8ffb4b823bbb",
                          "widgetName": "SizedBox",
                          "widgetSetting": {"height": "3.h"}
                        },
                        {
                          "id": "1d7406c0-407e-4bb8-bac0-f1648dd090d2",
                          "widgetName": "Row",
                          "widgetSetting": {
                            "children": [
                              {
                                "id": "23d19586-2874-46b3-ab3b-82a5670768fb",
                                "widgetName": "Expanded",
                                "widgetSetting": {
                                  "child": {
                                    "id":
                                        "bec3e089-62e2-4233-a601-d42a1b296d2f",
                                    "widgetName": "Divider",
                                    "widgetSetting": {
                                      "thickness": "2",
                                      "height": "6.h",
                                      "color": "0x8F634201"
                                    }
                                  }
                                }
                              },
                              {
                                "id": "0c901e6c-cccd-487a-916e-ff7258b747a1",
                                "widgetName": "Center",
                                "widgetSetting": {
                                  "child": {
                                    "id":
                                        "6df9d8ae-b918-4dd8-8f5d-cf9c5f43effc",
                                    "widgetName": "Container",
                                    "widgetSetting": {
                                      "padding": {"horizontal": "3.w"},
                                      "color": "0x00000000",
                                      "height": "6.h",
                                      "child": {
                                        "id":
                                            "5c15100b-0c68-4d7b-b500-6f86cee0873f",
                                        "widgetName": "Center",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "5ea5476e-79cb-441e-a923-b04625135e6b",
                                            "widgetName": "Text",
                                            "widgetSetting": {
                                              "data": "OUR TREATMENTS",
                                              "style": {
                                                "fontSize": "3.h",
                                                "fontWeight": "w700",
                                                "color": "0xFF895B3A",
                                                "fontFamily": "Nunito Sans"
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              },
                              {
                                "id": "c131c3a4-f20c-492a-b71d-1f3abe05f3c1",
                                "widgetName": "Expanded",
                                "widgetSetting": {
                                  "child": {
                                    "id":
                                        "1b6be339-1844-40d1-a8da-5369ef5dd3b1",
                                    "widgetName": "Divider",
                                    "widgetSetting": {
                                      "thickness": "2",
                                      "height": "6.h",
                                      "color": "0x8F634201"
                                    }
                                  }
                                }
                              }
                            ]
                          }
                        },
                        {
                          "id": "a97fada5-daf1-48aa-ad01-727354b07123",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "id": "924074bc-8947-4bbf-828f-2df60eceaeee",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "mainAxisAlignment": "spaceBetween",
                                "children": [
                                  {
                                    "id":
                                        "eef5101d-7ea3-4cc7-8595-34ac6cfbefff",
                                    "widgetName": "CategoryTitle",
                                    "widgetSetting": {
                                      "categoryId": "category2",
                                      "color": "0xFF895B3A"
                                    }
                                  },
                                  {
                                    "id":
                                        "d1b366c9-6217-495d-9c3a-70aed2780bd7",
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product6",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false"
                                    }
                                  },
                                  {
                                    "id":
                                        "58624086-fba4-4bb8-80b1-ec4ec0973f4b",
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product7",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false"
                                    }
                                  },
                                  {
                                    "id":
                                        "166b0839-f32e-46d6-99f8-850a20c14fed",
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product8",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false"
                                    }
                                  },
                                  {
                                    "id":
                                        "43b0fb2a-e6a1-4b4f-8bd7-6f2f2cd56b55",
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product9",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false"
                                    }
                                  },
                                  {
                                    "id":
                                        "378b03c2-0aea-4fad-9d70-780ce53641b7",
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product10",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false"
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              },
              {
                "id": "e110af04-7e90-403c-a8d6-c2a75010978f",
                "widgetName": "VerticalDivider",
                "widgetSetting": {"thickness": "2", "color": "0x8F634201"}
              },
              {
                "id": "a299dae5-7799-46e9-9271-93a44d268d82",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "9bfeaf5f-d23d-413a-be4d-1cb475a2a660",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"bottom": "4.5.h", "horizontal": "3.w"},
                      "child": {
                        "id": "3bc8a2ba-4467-475d-8f4d-da1bc9eb034d",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "3ff7e054-a2af-4b6c-81f6-feb56c328d9d",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"height": "3.h"}
                            },
                            {
                              "id": "a9251dd1-4b44-415b-9491-eebe7b0ee83f",
                              "widgetName": "Divider",
                              "widgetSetting": {
                                "thickness": "2",
                                "height": "6.h",
                                "color": "0x8F634201"
                              }
                            },
                            {
                              "id": "b0dbe67a-a643-4cd2-9d0d-b2845401233d",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "id": "911426be-1e0d-4e05-ac52-10e7e1f681c0",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "spaceAround",
                                    "children": [
                                      {
                                        "id":
                                            "90765a70-532d-45ea-ace5-15b8c69d54c1",
                                        "widgetName": "CategoryTitle",
                                        "widgetSetting": {
                                          "categoryId": "category3",
                                          "color": "0xFF895B3A"
                                        }
                                      },
                                      {
                                        "id":
                                            "efff6ec2-1181-401f-b0aa-cb0c87098b71",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "productId": "product11",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w",
                                          "useMediumStyle": "false"
                                        }
                                      },
                                      {
                                        "id":
                                            "3c0857f2-4e90-4bc3-9d93-be82dae581ef",
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "productId": "product12",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w",
                                          "useMediumStyle": "false"
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },
                            {
                              "id": "5206c751-8fc9-447d-9be2-045f0fc2ad29",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "color": "0xFF895B3A",
                                "height": "18.h",
                                "width": "20.w",
                                "child": {
                                  "id": "fd98b150-f3c3-43b2-922e-55194931c27c",
                                  "widgetName": "Center",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "a8f78def-82e9-43f5-9e3b-d2184c3f7c3e",
                                      "widgetName": "Text",
                                      "widgetSetting": {
                                        "data":
                                            "GET 10% OFF \nYOUR FIRST PAYMENT",
                                        "style": {
                                          "color": "0xFFFFFFFF",
                                          "fontSize": "1.5.w",
                                          "fontWeight": "w500",
                                          "fontFamily": "Nunito Sans"
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "6a5ed3fe-12f5-43d8-8b22-665e598b9bd7",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"height": "5.h"}
                            },
                            {
                              "id": "ba6637bf-1481-4789-b6a8-44583b32b8bf",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "20.h",
                                "width": "26.w",
                                "decoration": {
                                  "image": "image1",
                                  "boxFit": "cover"
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              }
            ]
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": ""
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": ""
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": ""
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$25",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 2.1",
        "price": "\$32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 2.2",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "image1": {
        "id": "image1",
        "key": "image1",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/image1.jpg"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
