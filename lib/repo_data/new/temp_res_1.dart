class JsonDataTemplate {
  // [112321TIN] run ok - ratio ok
  static final tempres1 = {
    "id": "1",
    "templateName": "temp restaurant 1",
    "templateSlug": "temp_restaurant_1",
    "description": null,
    "subscriptionLevel": "Medal",
    "image":
        "https://foodiemenu.co//wp-content/uploads/2023/11/temp_restaurant_1.png",
    "categoryId": "1",
    "ratios": "16:9,16:10,21:9,4:3",
    "ratioSetting": {
      "textStyle": {
        "categoryNameStyle": {"fontWeight": "bold", "fontFamily": "Bebas Neue"},
        "nameProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "Bebas Neue",
          "height": "1.5"
        },
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Bebas Neue",
          "height": "1.5"
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans",
          "height": "1.5"
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans",
          "height": "1.5"
        },
        "priceProductStyle": {"fontWeight": "w800", "fontFamily": "Bebas Neue"},
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Bebas Neue"
        }
      },
      "renderScreen": {
        "32/9": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.6.w",
          "imageHeight": "14.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "100",
          "marginBottomCategoryTitle": "1.h",
          "paddingVerticalCategorTilte": "0",
          "paddingHorizontalCategorTilte": "0"
        },
        "21/9": {
          "categoryNameStyle": "1.5.w",
          "nameProductStyle": "0.9.w",
          "nameProductStyleMedium": "0.7.w",
          "priceProductStyle": "0.9.w",
          "priceProductStyleMedium": "0.7.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.5.w",
          "imageHeight": "13.h",
          "marginBottomImage": "1.5.w",
          "marginBottomProduct": "1.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "1.h",
          "paddingVerticalCategorTilte": "0",
          "paddingHorizontalCategorTilte": "0"
        },
        "16/9": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.6.w",
          "imageHeight": "14.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "1.5.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "2.h",
          "paddingVerticalCategorTilte": "1.h",
          "paddingHorizontalCategorTilte": "1.h"
        },
        "16/10": {
          "categoryNameStyle": "2.5.w",
          "nameProductStyle": "1.7.w",
          "nameProductStyleMedium": "1.5.w",
          "priceProductStyle": "1.7.w",
          "priceProductStyleMedium": "1.5.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.6.w",
          "imageHeight": "18.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "2.5.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "1.h",
          "paddingVerticalCategorTilte": "0",
          "paddingHorizontalCategorTilte": "0"
        },
        "4/3": {
          "categoryNameStyle": "2.5.w",
          "nameProductStyle": "1.8.w",
          "nameProductStyleMedium": "1.5.w",
          "priceProductStyle": "1.8.w",
          "priceProductStyleMedium": "1.5.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.7.w",
          "imageHeight": "14.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "1.h",
          "paddingVerticalCategorTilte": "0",
          "paddingHorizontalCategorTilte": "0"
        },
        "default": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.6.w",
          "imageHeight": "14.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "2.h"
        }
      }
    },
    "widgetSetting": {
      "id": "f94dd04a-3e3b-4955-a2f3-2a3194a8af0a",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"image": "imageBackground", "boxFit": "fill"},
        "child": {
          "id": "0978758d-25f1-409d-b573-af25ffc428ab",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                "id": "b8f67722-f5a8-408d-90e4-280944f15740",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "3d01dec2-f18c-478a-b87d-f9fbbae6d42d",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "id": "731945a1-ecb3-4669-9905-6fa85202661e",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "89ad23d8-9bc4-478a-91a8-243f8cfabad7",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "id": "ea508cf5-e383-4776-b6dc-fdc7fb75299d",
                                  "widgetName": "Container",
                                  "widgetSetting": {
                                    "margin": {"bottom": "1.w"},
                                    "child": {
                                      "id":
                                          "ff3e78b5-7ceb-4f66-a14d-cf0cd40e7ac6",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "children": [
                                          {
                                            "id":
                                                "02ca00ba-38e3-4b05-9734-6312b94194b8",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "2",
                                              "child": {
                                                "id":
                                                    "065d7803-d91b-4e6f-9438-653035ad3961",
                                                "widgetName": "Container",
                                                "widgetSetting": {
                                                  "decoration": {
                                                    "color": "0x18FFFFFF",
                                                    "borderRadius": "30"
                                                  },
                                                  "padding": {"all": "1.5.w"},
                                                  "child": {
                                                    "id":
                                                        "37c1ff51-ad72-4b14-aa1b-841cb2a96306",
                                                    "widgetName":
                                                        "CategoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      "dataList": {
                                                        "category": "category1",
                                                        "product": [
                                                          "product1",
                                                          "product2",
                                                          "product3"
                                                        ]
                                                      },
                                                      "indexCategory": "0",
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "colorCategoryTitle":
                                                          "0xFFF47B22",
                                                      "startIndexProduct": "0",
                                                      "endIndexProduct": "3"
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "9591c15f-3082-4dd9-890d-c589ae201484",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "1.5.w"}
                                          },
                                          {
                                            "id":
                                                "cb18c428-8d85-4625-a4fe-36bc4e929053",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "64a52e00-fdaa-4386-bc17-2e6a8db7240a",
                                                "widgetName": "Column",
                                                "widgetSetting": {
                                                  "mainAxisAlignment": "start",
                                                  "children": [
                                                    {
                                                      "id":
                                                          "9d3ae484-767e-47fa-ac0a-63f7bde77a83",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "id":
                                                              "fc871008-2ef0-4e4b-b0e0-da9d3cbd255b",
                                                          "widgetName": "Image",
                                                          "widgetSetting": {
                                                            "imageId": "image1",
                                                            "fixCover": "false"
                                                          }
                                                        }
                                                      }
                                                    },
                                                    {
                                                      "id":
                                                          "2adadf39-2abd-41b4-b778-92d6c964e1ce",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "id":
                                                              "deb9f3fa-c065-4306-a46f-7eda0f53f623",
                                                          "widgetName": "Image",
                                                          "widgetSetting": {
                                                            "imageId": "image2",
                                                            "fixCover": "false"
                                                          }
                                                        }
                                                      }
                                                    },
                                                    {
                                                      "id":
                                                          "c753132e-577b-4245-9707-a060893f12e2",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "id":
                                                              "06eef3ef-a1a7-4343-ae8a-4879d5efbccd",
                                                          "widgetName": "Image",
                                                          "widgetSetting": {
                                                            "imageId": "image3",
                                                            "fixCover": "false"
                                                          }
                                                        }
                                                      }
                                                    }
                                                  ]
                                                }
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "id": "cb9158c5-919a-4354-a9c2-3f42a3dc5248",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"height": "2.h"}
                            },
                            {
                              "id": "7c13a575-9c8f-4b02-9497-7bbde1d42e64",
                              "widgetName": "CategoryTitleGridLayout1",
                              "widgetSetting": {
                                "dataList": {
                                  "category": "category2",
                                  "product": [
                                    "product4",
                                    "product5",
                                    "product6",
                                    "product7",
                                    "product8"
                                  ],
                                  "image": ["image4", "image5", "image6"]
                                },
                                "useBackgroundCategoryTitle": "false",
                                "colorPrice": "0xFFF47B22"
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              {
                "id": "f5301f46-e25d-43aa-99e7-b592245e364b",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "4aad666c-becc-477e-8317-61a31123119b",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "id": "d1cf3540-25fb-4a34-b4bb-4c05148e5ae7",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "mainAxisAlignment": "start",
                          "children": [
                            {
                              "id": "2e678628-7528-423e-9afb-8353a105d851",
                              "widgetName": "Row",
                              "widgetSetting": {
                                "textRightToLeft": "false",
                                "crossAxisAlignment": "start",
                                "mainAxisAlignment": "start",
                                "children": [
                                  {
                                    "id":
                                        "09ecaa8f-a095-4c81-a0c7-28de0cd6b532",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "b59b32f4-0864-4c2f-b22a-ae59fac11510",
                                        "widgetName": "Image",
                                        "widgetSetting": {
                                          "imageId": "image7",
                                          "fixCover": "true"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "78c6e18c-60a3-4beb-8ce4-15321e6e9802",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {"width": "1.w"}
                                  },
                                  {
                                    "id":
                                        "e33a9d30-843e-418b-b3fe-1e87b162e19d",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "eeb43d35-71ee-4411-8764-86d94f81d4ea",
                                        "widgetName": "Image",
                                        "widgetSetting": {
                                          "imageId": "image8",
                                          "fixCover": "true"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "2bf7b256-8637-4de3-a579-b98250f6eb52",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {"width": "1.w"}
                                  },
                                  {
                                    "id":
                                        "a0b93aac-4b60-4dc6-8fb0-503b7b2bf27d",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "id":
                                            "bebe5c60-63f6-48b8-adce-3d372f2c7692",
                                        "widgetName": "Image",
                                        "widgetSetting": {
                                          "imageId": "image9",
                                          "fixCover": "true"
                                        }
                                      }
                                    }
                                  }
                                ]
                              }
                            },
                            {
                              "id": "bffac8be-3c4c-41ee-9142-3d7e45a33da3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "id": "a0b3bfb1-047e-4b93-b568-d9233b6845b5",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      {
                                        "id":
                                            "676f1f02-b523-4075-9a94-b13f2b2ba696",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "445a6706-7215-4893-8937-59849d65ffae",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category3",
                                                "product": [
                                                  "product9",
                                                  "product10",
                                                  "product11"
                                                ]
                                              },
                                              "indexCategory": "2",
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "startIndexProduct": "0",
                                              "endIndexProduct": " 3",
                                              "colorPrice": "0xFFf47b22"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "cb8fd84f-6054-423d-b129-f5e2f081b743",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"},
                                          "child": {
                                            "id":
                                                "cb1947c9-0c86-48ea-9f90-917847fb8a24",
                                            "widgetName": "Image",
                                            "widgetSetting": {
                                              "imageId": "imageSeperatorLine",
                                              "fixCover": "false"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "ecd1f5b1-be66-4c32-a32d-15f515888530",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "f4725efb-f4ad-4df0-bd20-fe4402d7ad33",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category4",
                                                "product": [
                                                  "product12",
                                                  "product13",
                                                  "product14"
                                                ]
                                              },
                                              "indexCategory": "3",
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "startIndexProduct": "0",
                                              "endIndexProduct": "3",
                                              "colorPrice": "0xFFf47b22"
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },
                            {
                              "id": "acf26616-8541-4d13-83d2-68aa74febfe5",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "id": "dafc21bc-5d59-488f-9303-6413f5b08739",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      {
                                        "id":
                                            "2976a744-b9b4-4a5d-afce-7257e7c675b7",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "e8fce72f-09e9-4c8a-9d17-877cf73a8f3c",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category5",
                                                "product": [
                                                  "product15",
                                                  "product16",
                                                  "product17"
                                                ]
                                              },
                                              "indexCategory": "4",
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "startIndexProduct": "0",
                                              "endIndexProduct": "3",
                                              "colorPrice": "0xFFf47b22"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "e373056a-be98-4729-9eea-b9c380d9b447",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "9160f9fa-ce57-4a8f-ab08-0c23ad20ddb7",
                                            "widgetName": "Padding",
                                            "widgetSetting": {
                                              "padding": {"all": "1.w"},
                                              "child": {
                                                "id":
                                                    "9c08a09d-5351-4e77-8c6b-eb7513524d64",
                                                "widgetName": "Image",
                                                "widgetSetting": {
                                                  "imageId": "image10",
                                                  "fixCover": "false"
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              }
            ]
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "/temp_restaurant_2/food(1).png"
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "/temp_restaurant_2/food(2).png"
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "/temp_restaurant_2/food(3).png"
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "/temp_restaurant_2/food(4).png"
      },
      "category5": {
        "key": "category5",
        "type": "category",
        "name": "Category 5",
        "image": "/temp_restaurant_2/food(5).png"
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 2.1",
        "price": "\$21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 2.2",
        "price": "\$22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 2.3",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 2.4",
        "price": "\$24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 2.5",
        "price": "\$25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 3.1",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 3.2",
        "price": "\$32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 3.3",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 4.1",
        "price": "\$41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 4.2",
        "price": "\$42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 4.3",
        "price": "\$43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 5.1",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 5.2",
        "price": "\$41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 5.3",
        "price": "\$51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "imageBackground": {
        "key": "imageBackground",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG.png"
      },
      "imageSeperatorLine": {
        "key": "imageSeperatorLine",
        "type": "image",
        "fixed": "true",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/Seperator-Line.png"
      },
      "image1": {
        "key": "image1",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food2.png"
      },
      "image2": {
        "key": "image2",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food4.png"
      },
      "image3": {
        "key": "image3",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food8.png"
      },
      "image4": {
        "key": "image4",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food-bg1.jpg"
      },
      "image5": {
        "key": "image5",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food-bg2.jpg"
      },
      "image6": {
        "key": "image6",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food-bg3.jpg"
      },
      "image7": {
        "key": "image7",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food-bg4.jpg"
      },
      "image8": {
        "key": "image8",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food-bg5.jpg"
      },
      "image9": {
        "key": "image9",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food-bg6.jpg"
      },
      "image10": {
        "key": "image10",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food7.png"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
