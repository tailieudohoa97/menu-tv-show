class JsonDataTemplate {
  static final tempnail5 = {
    "id": "10",
    "templateName": "temp nail 5",
    "templateSlug": "temp_nail_5",
    "description": null,
    "subscriptionLevel": "Medal",
    "image":
        "https://foodiemenu.co//wp-content/uploads/2023/11/temp_nail_5.png",
    "categoryId": "2",
    "ratios": "16:9,16:10,21:9,32:9,4:3",
    "ratioSetting": {
      "textStyle": {
        "categoryNameStyle": {
          "fontWeight": "bold",
          "fontFamily": "DM Serif Display"
        },
        "nameProductStyle": {
          "fontWeight": "w500",
          "fontFamily": "DM Serif Display",
          "height": "1.5"
        },
        "nameProductStyleMedium": {
          "fontWeight": "bold",
          "fontFamily": "DM Serif Display",
          "height": "1.5"
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans",
          "height": "1.5"
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "DM Serif Text",
          "height": "1.5"
        },
        "priceProductStyleMedium": {
          "fontWeight": "w600",
          "fontFamily": "DM Serif Display"
        },
        "priceProductStyle": {"fontWeight": "w300", "fontFamily": "Open Sans"}
      },
      "renderScreen": {
        "32/9": {
          "categoryNameStyle": "2.w",
          "nameProductStyleMedium": "0.8.w",
          "priceProductStyleMedium": "1.2.w",
          "discriptionProductStyleMedium": "0.6.w"
        },
        "21/9": {
          "categoryNameStyle": "2.w",
          "nameProductStyleMedium": "1.2.w",
          "priceProductStyleMedium": "1.5.w",
          "discriptionProductStyleMedium": "0.9.w"
        },
        "16/9": {
          "categoryNameStyle": "2.5.w",
          "nameProductStyleMedium": "1.5.w",
          "priceProductStyleMedium": "2.4.w",
          "discriptionProductStyleMedium": "1.w"
        },
        "16/10": {
          "categoryNameStyleMedium": "3.w",
          "nameProductStyleMedium": "1.4.w",
          "priceProductStyleMedium": "2.3.w",
          "discriptionProductStyleMedium": "1.w"
        },
        "4/3": {
          "categoryNameStyle": "1.8.w",
          "nameProductStyleMedium": "2.w",
          "priceProductStyleMedium": "3.w",
          "discriptionProductStyleMedium": "1.5.w"
        },
        "default": {
          "categoryNameStyle": "2.w",
          "nameProductStyleMedium": "2.w",
          "priceProductStyleMedium": "2.w"
        }
      }
    },
    "widgetSetting": {
      "id": "e309416f-3d36-4fde-b3f9-f144257c9d95",
      "widgetName": "Container",
      "widgetSetting": {
        "width": "100.w",
        "height": "100.h",
        "color": "0xFFFFFFFF",
        "child": {
          "id": "f944854b-9181-4293-be54-a40bc24193ba",
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              {
                "id": "1865b9ff-c86a-4c22-b366-dc79f1bd3a5c",
                "widgetName": "Column",
                "widgetSetting": {
                  "children": [
                    {
                      "id": "a93a10af-f6cf-4f47-9316-db492c62a73b",
                      "widgetName": "Container",
                      "widgetSetting": {"height": "5.h", "color": "0xFFA76744"}
                    },
                    {
                      "id": "61440f5e-6486-4265-9e88-8958e50544aa",
                      "widgetName": "Expanded",
                      "widgetSetting": {"": ""}
                    },
                    {
                      "id": "cf52ad5f-517f-45e9-ad1c-087f697dbdff",
                      "widgetName": "Container",
                      "widgetSetting": {
                        "height": "20.h",
                        "color": "0xFF2E292D",
                        "child": {
                          "id": "304399fc-e070-4832-962e-ac0efdbab9ef",
                          "widgetName": "Padding",
                          "widgetSetting": {
                            "padding": {
                              "left": "5.w",
                              "top": "2.h",
                              "bottom": "2.h"
                            },
                            "child": {
                              "id": "923e1c59-7b91-45c4-a799-85b1db80e54c",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "crossAxisAlignment": "start",
                                "mainAxisAlignment": "spaceAround",
                                "children": [
                                  {
                                    "id":
                                        "5bc7a371-ac3a-44ac-b19e-e9fd8b8e377e",
                                    "widgetName": "Row",
                                    "widgetSetting": {
                                      "children": [
                                        {
                                          "id":
                                              "7c6cd458-1147-4cc1-9cc4-14e4c9a3ed7c",
                                          "widgetName": "CustomContainer",
                                          "widgetSetting": {
                                            "color": "0xFFA76744",
                                            "blendMode": "modulate",
                                            "height": "8.h",
                                            "width": "8.h",
                                            "image":
                                                "http://foodiemenu.co/wp-content/uploads/2023/09/white-globe-icon-24-2.png"
                                          }
                                        },
                                        {
                                          "id":
                                              "8e6a7779-e643-4bed-b5ec-7bfd38a534fe",
                                          "widgetName": "Text",
                                          "widgetSetting": {
                                            "data": "WWW.SALON.WWW",
                                            "style": {
                                              "fontFamily": "DM Serif Display",
                                              "color": "0xFFFFFFFF",
                                              "fontSize": "2.5.h"
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "c7e8b845-bdd6-499b-9989-31b047aa26c0",
                                          "widgetName": "SizedBox",
                                          "widgetSetting": {"width": "5.w"}
                                        },
                                        {
                                          "id":
                                              "b669f3fb-5c0d-4559-ab9e-5fe9cb2504a1",
                                          "widgetName": "CustomContainer",
                                          "widgetSetting": {
                                            "color": "0xFFA76744",
                                            "blendMode": "modulate",
                                            "height": "6.h",
                                            "width": "6.h",
                                            "image":
                                                "http://foodiemenu.co/wp-content/uploads/2023/09/pinpng.com-contact-icon-png-666849.png"
                                          }
                                        },
                                        {
                                          "id":
                                              "d5142dc0-352e-4917-bf74-ef24efaf0507",
                                          "widgetName": "Text",
                                          "widgetSetting": {
                                            "data": "+1223 334 444",
                                            "style": {
                                              "fontFamily": "DM Serif Display",
                                              "color": "0xFFFFFFFF",
                                              "fontSize": "2.5.h"
                                            }
                                          }
                                        }
                                      ]
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        }
                      }
                    }
                  ]
                }
              },
              {
                "id": "fdb5235b-f900-4c3f-a910-8689f14601d9",
                "widgetName": "Padding",
                "widgetSetting": {
                  "padding": {"top": "5.h"},
                  "child": {
                    "id": "05cd27cc-1632-4063-bdce-7d20b47ed0fd",
                    "widgetName": "Row",
                    "widgetSetting": {
                      "children": [
                        {
                          "id": "b8217a24-44d6-4930-878c-d9fd9adac1c2",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "id": "da791a8a-2ede-4703-95ea-b6a9782209bd",
                              "widgetName": "Center",
                              "widgetSetting": {
                                "child": {
                                  "id": "df948b9e-eb7a-404b-809a-e0d6686be8d7",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "crossAxisAlignment": "start",
                                    "children": [
                                      {
                                        "id":
                                            "72908756-37bd-4cae-a93e-ef9379c0e893",
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {"height": "3.h"}
                                      },
                                      {
                                        "id":
                                            "56d28cbe-da2a-40d9-b4a9-b936c8312593",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "BEAUTY SERVICES",
                                          "style": {
                                            "fontFamily": "DM Serif Display",
                                            "color": "0xFF000000",
                                            "fontSize": "5.h",
                                            "fontWeight": "bold"
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "bfbeec4d-61f7-4fc4-af5a-73bae33f44af",
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {"height": "2.h"}
                                      },
                                      {
                                        "id":
                                            "f61588d2-bede-41a1-bed1-e34ec2caeae3",
                                        "widgetName": "Wrap",
                                        "widgetSetting": {
                                          "spacing": "2.w",
                                          "runSpacing": "4.h",
                                          "children": [
                                            {
                                              "id":
                                                  "9e495117-404e-4f56-8f4f-282e701f5419",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "productId": "product1",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "colorDescription":
                                                    "0xFF000000",
                                                "width": "23.w",
                                                "useMediumStyle": "true"
                                              }
                                            },
                                            {
                                              "id":
                                                  "7f4a05e4-a080-4150-a726-ecdde347063c",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product2",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true"
                                              }
                                            },
                                            {
                                              "id":
                                                  "ef7db4f4-2a1d-4f14-8e84-042b3dd0d8c7",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product3",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true"
                                              }
                                            },
                                            {
                                              "id":
                                                  "da0ad32c-ce17-4627-8dbd-fcc489011710",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product4",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true"
                                              }
                                            },
                                            {
                                              "id":
                                                  "68e77843-0e96-40c3-8aac-caeff828dbec",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product5",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true"
                                              }
                                            },
                                            {
                                              "id":
                                                  "059ee8bf-fb56-4b7f-a087-c39d9d98152d",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product6",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true"
                                              }
                                            },
                                            {
                                              "id":
                                                  "4a049613-24c8-428a-a626-64f439640aa8",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product7",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true"
                                              }
                                            },
                                            {
                                              "id":
                                                  "292a423b-5516-4973-80f2-f35d0c6f0532",
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product8",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true"
                                              }
                                            }
                                          ]
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            }
                          }
                        },
                        {
                          "id": "c00159e4-823b-4f23-a8a6-099b7005ba64",
                          "widgetName": "SizedBox",
                          "widgetSetting": {
                            "width": "45.w",
                            "child": {
                              "id": "73060ff8-b988-4e91-8132-c50fa0c1b852",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "children": [
                                  {
                                    "id":
                                        "81d2a4eb-4204-4b29-9fb9-06eede449792",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {"height": "5.h"}
                                  },
                                  {
                                    "id":
                                        "e4711713-1a9f-494f-8a38-4c386093c387",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "height": "20.h",
                                      "child": {
                                        "id":
                                            "fa517741-f4d0-4a78-bcd8-a669cfadbd55",
                                        "widgetName": "Row",
                                        "widgetSetting": {
                                          "mainAxisAlignment": "spaceBetween",
                                          "children": [
                                            {
                                              "id":
                                                  "342ae514-6a72-4789-98e0-a00675e11990",
                                              "widgetName": "Expanded",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "a443643b-3270-4f25-93cf-b99da80731f5",
                                                  "widgetName": "Column",
                                                  "widgetSetting": {
                                                    "crossAxisAlignment":
                                                        "start",
                                                    "children": [
                                                      {
                                                        "id":
                                                            "d307d682-1b53-4dc9-a1dc-7e52225ea464",
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data":
                                                              "Our Missions",
                                                          "style": {
                                                            "color":
                                                                "0xFFA76744",
                                                            "fontWeight":
                                                                "bold",
                                                            "fontSize": "3.5.h",
                                                            "fontFamily":
                                                                "DM Serif Display"
                                                          },
                                                          "textAlign": "left"
                                                        }
                                                      },
                                                      {
                                                        "id":
                                                            "bca8c962-fe20-4e8c-a192-fc49da445f10",
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data":
                                                              "Lorem ipsum dolor sit amet consectetur, Lorem ipsum dolor sit amet, consectetur, Lorem ipsum dolor sit amet, consectetur",
                                                          "style": {
                                                            "fontFamily":
                                                                "DM Serif Text",
                                                            "fontSize": "2.h",
                                                            "color":
                                                                "0xFF000000"
                                                          },
                                                          "textAlign": "left"
                                                        }
                                                      }
                                                    ]
                                                  }
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "9d40ccf3-b36b-46b1-a0cb-884e67fe9292",
                                              "widgetName": "SizedBox",
                                              "widgetSetting": {"width": "4.w"}
                                            },
                                            {
                                              "id":
                                                  "a122ce9b-154a-42a1-8ed5-0c22e4595c61",
                                              "widgetName": "Expanded",
                                              "widgetSetting": {
                                                "child": {
                                                  "id":
                                                      "18cc59d8-2b31-4556-868c-bb4c369ea89b",
                                                  "widgetName": "Column",
                                                  "widgetSetting": {
                                                    "crossAxisAlignment":
                                                        "start",
                                                    "children": [
                                                      {
                                                        "id":
                                                            "195750ce-9558-4827-ad55-e2466e49d9e8",
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data":
                                                              "Our Missions",
                                                          "style": {
                                                            "color":
                                                                "0xFFA76744",
                                                            "fontWeight":
                                                                "bold",
                                                            "fontSize": "3.5.h",
                                                            "fontFamily":
                                                                "DM Serif Display"
                                                          },
                                                          "textAlign": "left"
                                                        }
                                                      },
                                                      {
                                                        "id":
                                                            "1e43f1e6-f06c-4744-8d86-5b69357086c0",
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data":
                                                              "Lorem ipsum dolor sit amet con sectetur, , Lorem ipsum dolor sit amet, consectetur, Lorem ipsum dolor sit amet, consectetur",
                                                          "style": {
                                                            "fontFamily":
                                                                "DM Serif Text",
                                                            "fontSize": "2.h",
                                                            "color":
                                                                "0xFF000000"
                                                          },
                                                          "textAlign": "left"
                                                        }
                                                      }
                                                    ]
                                                  }
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "444c6822-953a-4631-9a38-3d826792c402",
                                              "widgetName": "SizedBox",
                                              "widgetSetting": {"width": "4.w"}
                                            }
                                          ]
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "1682dfec-3b88-4cf3-87ed-da2d79c0d8d7",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "height": "55.h",
                                      "child": {
                                        "id":
                                            "fc0e8ad1-ee38-4fa1-883d-fc749675a91a",
                                        "widgetName": "Stack",
                                        "widgetSetting": {
                                          "children": [
                                            {
                                              "id":
                                                  "ed850a25-236b-453a-a193-47198f2580b6",
                                              "widgetName": "CustomContainer",
                                              "widgetSetting": {
                                                "height": "50.h",
                                                "opacity": "1",
                                                "blendMode": "color",
                                                "fit": "contain",
                                                "alignment": "topLeft",
                                                "color": "0xFFA76744",
                                                "image":
                                                    "https://foodiemenu.co/wp-content/uploads/2023/10/outline-vector-flower-illustration-corner-border-frame-floral-elements-coloring-page-outline-vector-flower-illustration-189504420-e1696476093637.webp"
                                              }
                                            },
                                            {
                                              "id":
                                                  "009c6acf-c0e8-42cb-abf8-e0779ec70709",
                                              "widgetName": "Container",
                                              "widgetSetting": {
                                                "height": "50.h",
                                                "decoration": {
                                                  "color": "0xCCFFFFFF",
                                                  "opacity": "1.0"
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "88e31c95-d57d-4344-9aa3-81a52872410f",
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "right": "0",
                                                "bottom": "0",
                                                "child": {
                                                  "id":
                                                      "c8e10fa7-fb09-48e7-b54a-311565977c06",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "height": "50.h",
                                                    "width": "30.w",
                                                    "color": "0xFFA76744"
                                                  }
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "79fbcd0a-a7ef-4778-b085-af1d867d0ff5",
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "right": "21.5.w",
                                                "top": "5.5.h",
                                                "child": {
                                                  "id":
                                                      "c043ae5d-9442-4d04-a368-9fee64f882fb",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "height": "9.h",
                                                    "width": "10.w",
                                                    "child": {
                                                      "id":
                                                          "28ca8305-0b55-454d-8524-cdb4b96db193",
                                                      "widgetName": "Center",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "id":
                                                              "2b979fc4-e237-48fb-ba1f-4fc12354dcf3",
                                                          "widgetName": "Text",
                                                          "widgetSetting": {
                                                            "data":
                                                                "DISCOUNT OF\nTHE MONTH",
                                                            "style": {
                                                              "fontFamily":
                                                                  "DM Serif Text",
                                                              "color":
                                                                  "0xFFFFFFFF",
                                                              "fontSize":
                                                                  "2.5.sp"
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "8a7291c5-81dd-485c-b24c-0cb1883559dc",
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "right": "0",
                                                "bottom": "2.5.h",
                                                "child": {
                                                  "id":
                                                      "f6284395-0b08-4139-81ac-3678a04e6920",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "height": "37.5.h",
                                                    "width": "40.w",
                                                    "decoration": {
                                                      "image": "image1",
                                                      "boxFit": "cover"
                                                    }
                                                  }
                                                }
                                              }
                                            },
                                            {
                                              "id":
                                                  "7acace79-0268-4d18-9d08-208b3563b6a3",
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "top": "5.5.h",
                                                "right": "0",
                                                "child": {
                                                  "id":
                                                      "ac0defe0-20b9-4cec-898b-33140be2d9e6",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "color": "0xFFFFFFFF",
                                                    "height": "9.h",
                                                    "width": "23.w",
                                                    "child": {
                                                      "id":
                                                          "112340b3-949d-4ea2-8595-b89e5948e37b",
                                                      "widgetName": "Center",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "id":
                                                              "384c7cfb-4082-476c-a9be-871fcef1937b",
                                                          "widgetName": "Text",
                                                          "widgetSetting": {
                                                            "data": "50% OFF",
                                                            "style": {
                                                              "color":
                                                                  "0xFFA76744",
                                                              "fontWeight":
                                                                  "bold",
                                                              "fontSize": "6.h",
                                                              "fontFamily":
                                                                  "DM Serif Display"
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          ]
                                        }
                                      }
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              }
            ]
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": ""
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": ""
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": ""
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": ""
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$23.0",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$23.0",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$23.0",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$24.0",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$25.0",
        "image": "",
        "description":
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor"
      },
      "image1": {
        "id": "image1",
        "key": "image1",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/treatment_1.jpg"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
