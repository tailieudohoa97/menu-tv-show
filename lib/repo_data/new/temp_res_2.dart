class JsonDataTemplate {
  // [112321TIN] run ok - ratio (fail at 21:9, 16:9)
  static final tempres2 = {
    "id": "2",
    "templateName": "temp restaurant 2",
    "templateSlug": "temp_restaurant_2",
    "description": null,
    "subscriptionLevel": "Medal",
    "image":
        "https://foodiemenu.co//wp-content/uploads/2023/11/temp_restaurant_2.png",
    "categoryId": "1",
    "ratios": "16:9,16:10,21:9,32:9,4:3",
    "ratioSetting": {
      "textStyle": {
        "categoryNameStyle": {"fontWeight": "w900", "fontFamily": "Open Sans"},
        "nameProductStyle": {"fontWeight": "w800", "fontFamily": "Open Sans"},
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Open Sans"
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans"
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans"
        },
        "priceProductStyle": {"fontWeight": "w800", "fontFamily": "Open Sans"},
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Open Sans"
        }
      },
      "renderScreen": {
        "32/9": {
          "categoryNameStyle": "1.5.w",
          "nameProductStyle": "0.8.w",
          "priceProductStyle": "0.8.w",
          "discriptionProductStyle": "0.7.w",
          "imageHeight": "25.h",
          "marginBottomImage": "0.h",
          "marginBottomProduct": "3.h",
          "boderRadiusStyle": "100",
          "marginBottomCategoryTitle": "0.h",
          "paddingVerticalCategorTilte": "2.h",
          "paddingHorizontalCategorTilte": "1.h"
        },
        "21/9": {
          "categoryNameStyle": "1.5.w",
          "nameProductStyle": "0.9.w",
          "priceProductStyle": "0.9.w",
          "discriptionProductStyle": "0.8.w",
          "imageHeight": "30.h",
          "marginBottomImage": "0.h",
          "marginBottomProduct": "3.h",
          "boderRadiusStyle": "100",
          "marginBottomCategoryTitle": "0.h",
          "paddingVerticalCategorTilte": "2.h",
          "paddingHorizontalCategorTilte": "1.h"
        },
        "16/9": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.5.w",
          "priceProductStyle": "1.5.w",
          "discriptionProductStyle": "1.w",
          "imageHeight": "30.h",
          "marginBottomImage": "0.h",
          "marginBottomProduct": "3.h",
          "boderRadiusStyle": "100",
          "marginBottomCategoryTitle": "0.h",
          "paddingVerticalCategorTilte": "2.h",
          "paddingHorizontalCategorTilte": "1.h"
        },
        "16/10": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.5.w",
          "priceProductStyle": "1.5.w",
          "discriptionProductStyle": "1.w",
          "imageHeight": "40.h",
          "marginBottomImage": "0.h",
          "marginBottomProduct": "3.h",
          "boderRadiusStyle": "100",
          "marginBottomCategoryTitle": "0.h",
          "paddingVerticalCategorTilte": "2.h",
          "paddingHorizontalCategorTilte": "1.h"
        },
        "4/3": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.8.w",
          "priceProductStyle": "1.8.w",
          "discriptionProductStyle": "1.3.w",
          "imageHeight": "40.h",
          "marginBottomImage": "0.h",
          "marginBottomProduct": "3.h",
          "boderRadiusStyle": "100",
          "marginBottomCategoryTitle": "0.h",
          "paddingVerticalCategorTilte": "2.h",
          "paddingHorizontalCategorTilte": "1.h"
        },
        "default": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.5.w",
          "priceProductStyle": "1.5.w",
          "discriptionProductStyle": "1.w",
          "imageHeight": "37.h",
          "marginBottomImage": "0.h",
          "marginBottomProduct": "3.h",
          "boderRadiusStyle": "100",
          "marginBottomCategoryTitle": "0.h",
          "paddingVerticalCategorTilte": "2.h",
          "paddingHorizontalCategorTilte": "1.h"
        }
      }
    },
    "widgetSetting": {
      "id": "51732e4c-0c26-47ea-ab77-725bc92f3233",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"image": "imageBackground", "boxFit": "cover"},
        "child": {
          "id": "a82c9fd2-7392-4740-9497-c9b8ed1d3916",
          "widgetName": "Row",
          "widgetSetting": {
            "children": [
              {
                "id": "d3369644-e9a0-4eae-b3f3-a3fd5fb8c8e3",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "85ca665e-5053-42e3-ac05-3685a557b36b",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "id": "e8e87390-a287-405e-b928-6dde27ef58ff",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "ff2efb65-e6b4-4f8f-a2f8-52567f096771",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "id": "ad95feb7-1037-4cc6-b6cd-9d656b7361f5",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "children": [
                                      {
                                        "id":
                                            "04599167-a7d3-4415-b87b-fe8e5be8b94e",
                                        "widgetName": "CategoryTitle",
                                        "widgetSetting": {
                                          "categoryId": "category1",
                                          "useBackground": "true",
                                          "imageId": "imageBackgroundTitle",
                                          "useFullWidth": "true",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E"
                                        }
                                      },
                                      {
                                        "id":
                                            "2945225b-b731-4d7b-a5bc-a2925561702a",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "d7aa79ab-ff76-434d-83a0-56c9847f2fa7",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "height": "30.h",
                                              "margin": {"bottom": "0.h"},
                                              "padding": {"vertical": "1.5.w"},
                                              "decoration": {
                                                "borderRadius": "30"
                                              },
                                              "child": {
                                                "id":
                                                    "20e97d45-96ec-4f9e-b759-7903e892ad87",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "id":
                                                        "ed3e51cb-5d45-4605-903e-fd8d53edee06",
                                                    "widgetName": "Image",
                                                    "widgetSetting": {
                                                      "imageId": "image1",
                                                      "fixCover": "false"
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },
                            {
                              "id": "d12465d7-d566-4144-a49a-583d69ab162d",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "id": "402cbdb3-5fcd-4499-8762-f620863f5341",
                                  "widgetName": "ProductList",
                                  "widgetSetting": {
                                    "dataList": [
                                      "product1",
                                      "product2",
                                      "product3",
                                      "product4"
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22",
                                    "colorProductName": "",
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "colorDescription": "",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              {
                "id": "42ed80e6-dc96-4f45-9dbc-76cc98d5e4bf",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "280d1d9d-f414-438f-813c-b6f19f23d195",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "id": "ab382583-cdbf-4ab1-a39d-ceedd3b90f7e",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "47e4becc-7070-48fd-b454-8c4882a8e07e",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "id": "c4fa060e-177c-4de1-b553-4e926e5de80e",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "children": [
                                      {
                                        "id":
                                            "e60e2d51-c8e7-4a24-9b43-6f55804248d9",
                                        "widgetName": "CategoryTitle",
                                        "widgetSetting": {
                                          "categoryId": "category2",
                                          "useBackground": "true",
                                          "imageId": "imageBackgroundTitle",
                                          "useFullWidth": "true",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E"
                                        }
                                      },
                                      {
                                        "id":
                                            "441fc3ad-1023-44a8-a500-b4ce6bfef9b5",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "9d6d3d64-72db-4884-ad25-463524f3fdeb",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "height": "30.h",
                                              "margin": {"bottom": "0.h"},
                                              "decoration": {
                                                "borderRadius": "30"
                                              },
                                              "padding": {"vertical": "1.5.w"},
                                              "child": {
                                                "id":
                                                    "1efedce0-1e1e-4e27-bcc8-b6867dcc7023",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "id":
                                                        "978333ce-ea7e-4de2-85ae-a40d411c7ae5",
                                                    "widgetName": "Image",
                                                    "widgetSetting": {
                                                      "imageId": "image2",
                                                      "fixCover": "false"
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },
                            {
                              "id": "313c6c2d-1e20-4b6e-a017-7de71a9350f3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "id": "b1d24156-9dea-419e-a3df-b45079b057a5",
                                  "widgetName": "ProductList",
                                  "widgetSetting": {
                                    "dataList": [
                                      "product5",
                                      "product6",
                                      "product7",
                                      "product8"
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22",
                                    "colorProductName": "",
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "colorDescription": "",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              {
                "id": "7dc95232-450d-4a10-8034-b93e1cd85818",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "aa07ecc1-94b0-4207-8d75-652f9daa50ed",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "id": "3089b1ea-08a0-4551-9541-01e75a26af5f",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "14dad558-ebfa-429d-9337-60f642989881",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "id": "abd5aa71-b0d5-48a1-a1fd-5d32e30908db",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "children": [
                                      {
                                        "id":
                                            "b0f65bc2-e3e0-4f37-b5ce-d1e0cde1da30",
                                        "widgetName": "CategoryTitle",
                                        "widgetSetting": {
                                          "categoryId": "category3",
                                          "useBackground": "true",
                                          "imageId": "imageBackgroundTitle",
                                          "useFullWidth": "true",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E"
                                        }
                                      },
                                      {
                                        "id":
                                            "51767acf-cd05-4d45-9567-aabd47519262",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "2a8c3cf7-d3ac-4d40-a0b8-e64f4dc85b64",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "height": "30.h",
                                              "margin": {"bottom": "0.h"},
                                              "decoration": {
                                                "borderRadius": "30"
                                              },
                                              "padding": {"vertical": "1.5.w"},
                                              "child": {
                                                "id":
                                                    "ca7270af-7429-4118-a52f-916ceea82f55",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "id":
                                                        "a74a71f9-ee55-45ef-8b73-12d40e1a187d",
                                                    "widgetName": "Image",
                                                    "widgetSetting": {
                                                      "imageId": "image3",
                                                      "fixCover": "false"
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },
                            {
                              "id": "0b5be906-f9c7-407c-9570-b42d826f7ed9",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "id": "9fe445f4-45e1-49bb-a35a-1793f3539506",
                                  "widgetName": "ProductList",
                                  "widgetSetting": {
                                    "dataList": [
                                      "product9",
                                      "product10",
                                      "product11",
                                      "product12"
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22",
                                    "colorProductName": "",
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "colorDescription": "",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              }
            ]
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": ""
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": ""
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": ""
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": ""
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "\$32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "\$41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "imageBackground": {
        "key": "imageBackground",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-2.png"
      },
      "imageBackgroundTitle": {
        "key": "imageBackgroundTitle",
        "type": "image",
        "image": ""
      },
      "image1": {
        "key": "image1",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food1.png"
      },
      "image2": {
        "key": "image2",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food2-1.png"
      },
      "image3": {
        "key": "image3",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/food3.png"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
