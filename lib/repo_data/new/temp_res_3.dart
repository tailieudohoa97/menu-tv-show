class JsonDataTemplate {
  // [112321TIN] run ok - ratio ok
  static final tempres3 = {
    "id": "3",
    "templateName": "temp restaurant 3",
    "templateSlug": "temp_restaurant_3",
    "description": null,
    "subscriptionLevel": "Medal",
    "image":
        "https://foodiemenu.co//wp-content/uploads/2023/11/template_restaurant_3.png",
    "categoryId": "1",
    "ratios": "16:9,16:10,21:9,32:9,4:3",
    "ratioSetting": {
      "textStyle": {
        "categoryNameStyle": {
          "fontWeight": "w300",
          "fontFamily": "Alfa Slab One",
          "letterSpacing": "5"
        },
        "nameProductStyle": {"fontWeight": "w800", "fontFamily": "Poppins"},
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Poppins"
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Poppins"
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "Poppins"
        },
        "priceProductStyle": {"fontWeight": "w800", "fontFamily": "Poppins"},
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Open Sans"
        }
      },
      "renderScreen": {
        "32/9": {
          "categoryNameStyle": "1.w",
          "nameProductStyle": "0.7.w",
          "nameProductStyleMedium": "0.5.w",
          "priceProductStyle": "0.7.w",
          "priceProductStyleMedium": "0.5.w",
          "discriptionProductStyle": "0.5.w",
          "discriptionProductStyleMedium": "0.3.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        },
        "21/9": {
          "categoryNameStyle": "1.5.w",
          "nameProductStyle": "1.w",
          "nameProductStyleMedium": "0.7.w",
          "priceProductStyle": "1.w",
          "priceProductStyleMedium": "0.7.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        },
        "16/9": {
          "categoryNameStyle": "2.5.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "20.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.5.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        },
        "16/10": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "1.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        },
        "4/3": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "2.w",
          "nameProductStyleMedium": "1.5.w",
          "priceProductStyle": "2.w",
          "priceProductStyleMedium": "1.5.w",
          "discriptionProductStyle": "1.5.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        },
        "default": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "20.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        }
      }
    },
    "widgetSetting": {
      "id": "29ae8bbb-365b-4620-95b9-22cb4542df04",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"image": "imageBackground", "boxFit": "cover"},
        "child": {
          "id": "ff8be9aa-854f-48cd-b3e9-4af8d403d62e",
          "widgetName": "Container",
          "widgetSetting": {
            "color": "0xe5000000",
            "child": {
              "id": "2108cb65-dec2-4834-ba69-9d0019898368",
              "widgetName": "Row",
              "widgetSetting": {
                "textRightToLeft": "false",
                "children": [
                  {
                    "id": "0848ab90-25a5-4b2b-9810-80c96f90f604",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "id": "10b92af9-fe6e-4986-835f-36fc07e4304e",
                        "widgetName": "Padding",
                        "widgetSetting": {
                          "padding": {"all": "2.w"},
                          "child": {
                            "id": "aff1fc72-aede-4a33-ba2c-3c58a3f1d31d",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "mainAxisAlignment": "start",
                              "children": [
                                {
                                  "id": "6a0cf523-24c3-49d9-a081-d491a6edad2e",
                                  "widgetName": "CategoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "dataList": {
                                      "category": "category1",
                                      "product": [
                                        "product1",
                                        "product2",
                                        "product3",
                                        "product4",
                                        "product5"
                                      ]
                                    },
                                    "toUpperCaseNameProductName": "false",
                                    "useBackgroundCategoryTitle": "true",
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "useFullWidthCategoryTitle": "true"
                                  }
                                },
                                {
                                  "id": "236fb79a-90a0-4410-8741-ddba94d09d2a",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                {
                                  "id": "d6aef1c8-f21f-42e9-b657-7386504bb8e0",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "4987ab83-16b5-4c09-a1f0-3d82bafa8052",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "crossAxisAlignment": "start",
                                        "children": [
                                          {
                                            "id":
                                                "68b940e2-867a-47ee-ac25-5dbd3f449ea5",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "447ef339-e782-4c52-9ddf-1f7682666447",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image1",
                                                  "fit": "contain"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "bd0ec17c-a36b-423a-8888-2b502be0068e",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          {
                                            "id":
                                                "9e90278b-c028-4146-aba5-7b3324258841",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "ef68273e-20f9-4499-8255-62fa148ec9d6",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image2",
                                                  "fit": "contain"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "4da129fa-3090-4551-b0c1-7e6f3aa6c243",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          {
                                            "id":
                                                "d2460af1-a6f2-41ba-a8a3-0a5bfc8d87d7",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "77ae23f3-f298-424f-8244-428ff2c0b15a",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image3",
                                                  "fit": "contain"
                                                }
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  {
                    "id": "ef9ab199-708c-411f-83c6-51c67b8e15c2",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "id": "0b7e35ce-cb7a-469b-b436-cad4e708a09f",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "padding": {"all": "2.w"},
                          "color": "0x09ffffff",
                          "child": {
                            "id": "36aa04b9-ca2e-4f29-9198-a006a23ae3f3",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "mainAxisAlignment": "start",
                              "children": [
                                {
                                  "id": "d8f6ac43-a8ba-4f9a-841d-ec68f42ada5c",
                                  "widgetName": "CategoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "dataList": {
                                      "category": "category2",
                                      "product": [
                                        "product6",
                                        "product7",
                                        "product8",
                                        "product9",
                                        "product10"
                                      ]
                                    },
                                    "toUpperCaseNameProductName": "false",
                                    "useBackgroundCategoryTitle": "true",
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "useFullWidthCategoryTitle": "true"
                                  }
                                },
                                {
                                  "id": "b5852e91-d941-461d-8d6b-7df070db28ff",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                {
                                  "id": "5e7b7a7c-b49e-4b99-a4ec-e3ec6ed211e5",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "27ce0daa-9f44-4600-80ec-a07b701bd0d4",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "crossAxisAlignment": "start",
                                        "children": [
                                          {
                                            "id":
                                                "ec24f9f6-105d-4ef7-b945-a52e6f348522",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "b38406da-1cea-475e-b167-d9608e4f6583",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image4",
                                                  "fit": "contain"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "3bf22831-9871-4c32-b53a-e70b1e4cee73",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          {
                                            "id":
                                                "fdbec0ae-f896-4ae8-bfd0-9ba7764b4e95",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "4edefee6-41b6-49c3-9250-538d5bbdf872",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image5",
                                                  "fit": "contain"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "ce0d2bcf-72fb-42fb-b1b7-12b7e9dd44e0",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          {
                                            "id":
                                                "4abb9402-883a-4d0f-993f-f02f54c483ba",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "13c801a5-a90c-497d-ba00-f8e70b64dabb",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image6",
                                                  "fit": "contain"
                                                }
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  {
                    "id": "28e5e243-de88-4a38-a96c-232ad309044c",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "id": "c7940e6f-ed8d-412a-a5ae-3392f66d988e",
                        "widgetName": "Padding",
                        "widgetSetting": {
                          "padding": {"all": "2.w"},
                          "child": {
                            "id": "d9ddd1d5-d62a-41f7-932c-84bcbb68b1d3",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "mainAxisAlignment": "start",
                              "children": [
                                {
                                  "id": "73463770-7b6c-4641-9bb8-e268cbc42b0d",
                                  "widgetName": "CategoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "dataList": {
                                      "category": "category3",
                                      "product": [
                                        "product11",
                                        "product12",
                                        "product13",
                                        "product14",
                                        "product15"
                                      ]
                                    },
                                    "toUpperCaseNameProductName": "false",
                                    "useBackgroundCategoryTitle": "true",
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "useFullWidthCategoryTitle": "true"
                                  }
                                },
                                {
                                  "id": "5819b7c8-d515-447b-a28c-78cff72e14c4",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                {
                                  "id": "4091cdd9-db5e-42b3-ab33-3a78d6cae543",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "7a0efb0d-9ce5-4d20-939a-69b7d6ae1d6d",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "crossAxisAlignment": "start",
                                        "children": [
                                          {
                                            "id":
                                                "c46441be-a259-48d5-bea1-3944200fa5a9",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "53931bb0-23a3-4b85-bf20-a4dd5235f606",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image7",
                                                  "fit": "contain"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "2a25e1aa-8ea3-49c7-abce-95beb68d0fb2",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          {
                                            "id":
                                                "45d8c308-03db-4b90-b3dc-771bec8df7b0",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "f0541c39-1a5c-4755-b25e-9f83e5c12ca8",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image8",
                                                  "fit": "contain"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "b414abb2-2cc3-4ebe-8a42-087e81f27c39",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          {
                                            "id":
                                                "9ed18b40-c642-4d51-bbff-be40b09b01b4",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "id":
                                                    "ecae9f4c-2cef-441b-9f32-b01341addbd4",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image9",
                                                  "fit": "contain"
                                                }
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          }
                        }
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": ""
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": ""
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": ""
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 2.1",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 2.2",
        "price": "\$24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 2.3",
        "price": "\$25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 2.4",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 2.5",
        "price": "\$32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 3.1",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 3.2",
        "price": "\$41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 3.3",
        "price": "\$42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 3.4",
        "price": "\$43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 3.5",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "imageBackground": {
        "key": "imageBackground",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/food-bg4-1-scaled.jpg"
      },
      "image1": {
        "key": "image1",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/food-temp31.png"
      },
      "image2": {
        "key": "image2",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/food-temp32.png"
      },
      "image3": {
        "key": "image3",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/food-temp33.png"
      },
      "image4": {
        "key": "image4",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/food-temp34.png"
      },
      "image5": {
        "key": "image5",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/food-temp35.png"
      },
      "image6": {
        "key": "image6",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/food-temp36.png"
      },
      "image7": {
        "key": "image7",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/food-temp37.png"
      },
      "image8": {
        "key": "image8",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/food-temp39.png"
      },
      "image9": {
        "key": "image9",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/food-temp38.png"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
