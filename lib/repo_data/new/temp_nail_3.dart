class JsonDataTemplate {
  // [112321TIN] run ok - ratio ok
  static final tempnail3 = {
    "id": "8",
    "templateName": "temp nail 3",
    "templateSlug": "temp_nail_3",
    "description": null,
    "subscriptionLevel": "Medal",
    "image":
        "https://foodiemenu.co//wp-content/uploads/2023/11/temp_nail_3.png",
    "categoryId": "2",
    "ratios": "16:9,16:10,21:9,32:9,4:3",
    "ratioSetting": {
      "textStyle": {
        "categoryNameStyle": {
          "fontWeight": "bold",
          "fontFamily": "Cormorant Garamond"
        },
        "nameProductStyle": {
          "fontWeight": "w600",
          "fontFamily": "Cormorant Garamond",
          "height": "1.5"
        },
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Cormorant Garamond",
          "height": "1.5"
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Lato",
          "height": "1.5"
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "Lato",
          "height": "1.5"
        },
        "priceProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "Cormorant Garamond"
        },
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Cormorant Garamond"
        }
      },
      "renderScreen": {
        "32/9": {
          "nameProductStyle": "1.w",
          "priceProductStyle": "1.w",
          "discriptionProductStyle": "0.9.w",
          "widthThumnailProduct": "28.w",
          "heightThumnailProduct": "16.h"
        },
        "21/9": {
          "nameProductStyle": "1.5.w",
          "priceProductStyle": "1.5.w",
          "discriptionProductStyle": "1.2.w",
          "widthThumnailProduct": "28.w",
          "heightThumnailProduct": "18.h"
        },
        "16/9": {
          "nameProductStyle": "1.75.w",
          "priceProductStyle": "1.75.w",
          "imageHeight": "25.h",
          "discriptionProductStyle": "1.25.w",
          "widthThumnailProduct": "28.w",
          "heightThumnailProduct": "20.h"
        },
        "16/10": {
          "nameProductStyle": "1.8.w",
          "priceProductStyle": "1.8.w",
          "imageHeight": "25.h",
          "discriptionProductStyle": "1.25.w",
          "widthThumnailProduct": "28.w",
          "heightThumnailProduct": "20.h"
        },
        "4/3": {
          "nameProductStyle": "2.w",
          "priceProductStyle": "2.w",
          "discriptionProductStyle": "1.5.w",
          "widthThumnailProduct": "28.w",
          "heightThumnailProduct": "22.h"
        },
        "default": {
          "nameProductStyle": "1.75.w",
          "priceProductStyle": "1.75.w",
          "imageHeight": "25.h",
          "discriptionProductStyle": "1.25.w",
          "widthThumnailProduct": "28.w",
          "heightThumnailProduct": "20.h"
        }
      }
    },
    "widgetSetting": {
      "id": "e26c179d-10ad-4093-9e18-f96270fca7aa",
      "widgetName": "Container",
      "widgetSetting": {
        "color": "0xFFF2EFEA",
        "width": "100.w",
        "height": "100.h",
        "padding": {"vertical": "3.h"},
        "child": {
          "id": "18ff408a-212c-4b3d-b1db-d12ec11f04a8",
          "widgetName": "Column",
          "widgetSetting": {
            "children": [
              {
                "id": "c605b698-e1d8-4daa-8546-cd2c43d79cbf",
                "widgetName": "Divider",
                "widgetSetting": {
                  "thickness": "2",
                  "height": "4.h",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "endIndent": "20.w"
                }
              },
              {
                "id": "789a71e3-c6d0-4f3a-9a29-751b92d6c946",
                "widgetName": "Text",
                "widgetSetting": {
                  "data": "NAIL SALON",
                  "style": {
                    "fontSize": "4.h",
                    "color": "0xFF616148",
                    "fontWeight": "w800",
                    "fontFamily": "Cormorant Garamond"
                  }
                }
              },
              {
                "id": "89ff302d-a4d9-42a4-a79d-4011eca7c484",
                "widgetName": "Text",
                "widgetSetting": {
                  "data": "NAIL IT!",
                  "style": {
                    "fontSize": "3.h",
                    "color": "0xFF616148",
                    "fontWeight": "w800",
                    "fontFamily": "Cormorant Garamond"
                  }
                }
              },
              {
                "id": "14c3bfc6-736d-4f2a-a1ef-606277b9dc4a",
                "widgetName": "Divider",
                "widgetSetting": {
                  "thickness": "2",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "height": "4.h",
                  "endIndent": "20.w"
                }
              },
              {
                "id": "b99ecb94-4469-4b0a-abcc-393785471c67",
                "widgetName": "Wrap",
                "widgetSetting": {
                  "runSpacing": "2.h",
                  "spacing": "4.w",
                  "children": [
                    {
                      "id": "cdb4728a-f4c8-42d7-8158-94f86e95a8c4",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product1",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    },
                    {
                      "id": "70f404ee-9abd-4c74-99dc-dab8452a175b",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product2",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    },
                    {
                      "id": "360a85e6-3731-4e77-9d15-78d54150effd",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product3",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    },
                    {
                      "id": "497183e4-3c22-47d5-83c3-eb926a7c597c",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product4",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    },
                    {
                      "id": "17dd9785-4c1b-4e3e-9ff0-6a021a8af2d8",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product5",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    },
                    {
                      "id": "03a63e7a-0422-4acf-b939-665c72c13b95",
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product6",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F"
                      }
                    }
                  ]
                }
              },
              {
                "id": "20df153d-a6b6-44fa-a7a8-c34a6c76f683",
                "widgetName": "Divider",
                "widgetSetting": {
                  "thickness": "2",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "height": "4.h",
                  "endIndent": "20.w"
                }
              },
              {
                "id": "bd27bf61-f94d-4803-848b-5163fb3eba55",
                "widgetName": "Row",
                "widgetSetting": {
                  "mainAxisAlignment": "spaceAround",
                  "children": [
                    {
                      "id": "29646d1f-c25f-410f-b8a9-19c97635ee22",
                      "widgetName": "Text",
                      "widgetSetting": {
                        "data": "salon@hairdresser",
                        "style": {
                          "color": "0xFF616148",
                          "fontSize": "3.h",
                          "fontFamily": "Cormorant Garamond"
                        }
                      }
                    },
                    {
                      "id": "df2c1c90-db78-4456-9beb-1ce6e4e4a1be",
                      "widgetName": "Text",
                      "widgetSetting": {
                        "data": "+012345689",
                        "style": {
                          "color": "0xFF616148",
                          "fontSize": "3.h",
                          "fontFamily": "Neuton"
                        }
                      }
                    },
                    {
                      "id": "42b0d915-e74a-4875-a6cc-6c5d05f1c7f5",
                      "widgetName": "Text",
                      "widgetSetting": {
                        "data": "hairdresser.com",
                        "style": {
                          "color": "0xFF616148",
                          "fontSize": "3.h",
                          "fontFamily": "Cormorant Garamond"
                        }
                      }
                    }
                  ]
                }
              }
            ]
          }
        }
      }
    },
    "dataSetting": {
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_1.jpg",
        "description":
            "Lorem ipsum dolor sit am adasda sda ting dolore magna al"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_2.jpg",
        "description":
            "Lorem ipsum dolor sit am adasda sda ting dolore magna al"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_3.jpg",
        "description":
            "Lorem ipsum dolor sit am adasda sda ting dolore magna al"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$23.0",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_4.jpg",
        "description":
            "Lorem ipsum dolor sit am adasda sda ting dolore magna al"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$23.0",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_5.jpg",
        "description":
            "Lorem ipsum dolor sit am adasda sda ting dolore magna al"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$23.0",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_6.jpg",
        "description":
            "Lorem ipsum dolor sit am adasda sda ting dolore magna al"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
