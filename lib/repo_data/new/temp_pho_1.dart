class JsonDataTemplate {
  // [112321TIN] run ok - ratio ok
  static final temppho1 = {
    "id": "11",
    "templateName": "temp pho 1",
    "templateSlug": "temp_pho_1",
    "description": null,
    "subscriptionLevel": "Medal",
    "image": "https://foodiemenu.co//wp-content/uploads/2023/11/temp_pho_1.png",
    "categoryId": "3",
    "ratios": "16:9,16:10,21:9,4:3",
    "ratioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp"
        },
        "21/9": {
          "categoryImageHeight": "25.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.sp",
          "productPriceFontSize": "2.sp",
          "normalFontSize": "2.sp",
          "brandFontSize": "10.sp",
          "verticalFramePadding": "4.sp",
          "horizontalFramePadding": "4.sp"
        },
        "16/9": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp"
        },
        "16/10": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "5.4.sp",
          "productNameFontSize": "3.5.sp",
          "productPriceFontSize": "3.5.sp",
          "normalFontSize": "4.8.sp",
          "brandFontSize": "18.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp"
        },
        "4/3": {
          "categoryImageHeight": "45.sp",
          "categoryFontSize": "7.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp",
          "normalFontSize": "5.8.sp",
          "brandFontSize": "22.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp"
        },
        "default": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp"
        }
      }
    },
    "widgetSetting": {
      "id": "2e5e0eef-0cce-4c2e-ab33-6a2bf1d15a06",
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "decoration": {"color": "0xffffffff"},
        "child": {
          "id": "d1a60516-ec75-484e-b699-7b3ebd8c5176",
          "widgetName": "PaddingByRatio",
          "widgetSetting": {
            "verticalPaddingFieldKey": "verticalFramePadding",
            "horizontalPaddingFieldKey": "horizontalFramePadding",
            "child": {
              "id": "e022348b-59ad-4ca8-b2ee-268bacd777a2",
              "widgetName": "Center",
              "widgetSetting": {
                "child": {
                  "id": "ffc36a4c-2d60-49d3-9742-af67953a7716",
                  "widgetName": "Column",
                  "widgetSetting": {
                    "mainAxisAlignment": "center",
                    "children": [
                      {
                        "id": "458e6c92-3300-44b5-8d23-6b746913f05e",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "padding": {"vertical": "2.sp"},
                          "child": {
                            "id": "6b9342c9-ef68-4efa-b625-ce4d09d96c71",
                            "widgetName": "Row",
                            "widgetSetting": {
                              "mainAxisAlignment": "spaceBetween",
                              "children": [
                                {
                                  "id": "a37b8ebb-3cb8-47e3-9477-1d0641c27658",
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "data": "4889 Princess St",
                                    "fontSizeFromFieldKey": "normalFontSize",
                                    "style": {"color": "0xff404042"}
                                  }
                                },
                                {
                                  "id": "f2102c1d-aefa-4da2-b5f2-da6d33d8ec0e",
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "data": "EST 1990",
                                    "fontSizeFromFieldKey": "normalFontSize",
                                    "style": {"color": "0xff404042"}
                                  }
                                }
                              ]
                            }
                          }
                        }
                      },
                      {
                        "id": "b03618ff-11d4-41ba-b228-3a3f1d4c7a89",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "height": "1",
                          "decoration": {
                            "border": {"color": "0xff404042", "width": "0.1.sp"}
                          }
                        }
                      },
                      {
                        "id": "b484689a-816e-4d1e-8974-4b24f36723c2",
                        "widgetName": "UnderlineBrand",
                        "widgetSetting": {
                          "brand": "PHO BRAND",
                          "fontSizeFromFieldKey": "brandFontSize",
                          "textStyle": {
                            "fontWeight": "w700",
                            "color": "0xff404042"
                          },
                          "bottomBorderSide": {
                            "width": "1.8.sp",
                            "color": "0xff404042 "
                          }
                        }
                      },
                      {
                        "id": "05b0b7b0-9e87-4973-8687-d82da834532f",
                        "widgetName": "SizedBox",
                        "widgetSetting": {"height": "2.h"}
                      },
                      {
                        "id": "04e87893-0659-4581-a591-43631080bf20",
                        "widgetName": "SizedBox",
                        "widgetSetting": {
                          "width": "100.w",
                          "child": {
                            "id": "9482cdf3-b7fe-4157-86d6-2e099700bd9f",
                            "widgetName": "Row",
                            "widgetSetting": {
                              "direction": "horizontal",
                              "alignment": "spaceBetween",
                              "spacing": "1.sp",
                              "children": [
                                {
                                  "id": "352bd6d9-34a1-4d03-9d30-eb9698320c01",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "id":
                                          "b803597e-c9ff-4ec5-95f5-219a6825b514",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category1",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000"
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          {
                                            "id":
                                                "2cd50dec-0296-4783-a836-06573d23ff7a",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product1",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "a647e55c-f529-4c42-9e01-c10ba06f2541",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product2",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "faa50065-c6ca-4865-b48d-f7f54bb8b520",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product3",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "1a50e35d-4f74-41df-8db8-d7be3b14691f",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product4",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "5929648b-bd1d-4483-8e7f-ef5bf072be55",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product5",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "715e7a35-97ca-4cb3-a6f5-b4edca7f12a9",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product6",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "922342e3-2e40-45e0-99c7-adf506a51359",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product7",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                },
                                {
                                  "id": "56b03ca5-ee87-4ceb-aaff-35ee0ce5b1b2",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"width": "2.w"}
                                },
                                {
                                  "id": "e265bb97-7039-49af-bcc0-2ad7257c8b73",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "id":
                                          "4f945c53-2f76-41be-8784-5d6590386ff4",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category2",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000"
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          {
                                            "id":
                                                "0040d556-59a9-48dc-acda-c5c47d29a744",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product8",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "093e9e2f-9ce8-456e-a686-66d671da93f5",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product9",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "812e24d8-6092-47a2-a476-bc21da65d907",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product10",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "194b32ab-fa96-4d30-8194-601b0d5da40d",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product11",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "3ab9dd8f-559b-4d88-ac83-0a38c79063ec",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product12",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "944b650b-d164-42d7-a482-87e45f2a6ff3",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product13",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "9f7add25-7df5-489a-ad0a-9d1c8e152d0e",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product14",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                },
                                {
                                  "id": "64175712-ff51-4a58-a8c4-03df08e4adba",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"width": "2.w"}
                                },
                                {
                                  "id": "6d1df1f1-9554-4015-bae9-9c9fde7bcf43",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "id":
                                          "855d013d-435c-4be6-9fd4-ac801ea60e7f",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category3",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000"
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          {
                                            "id":
                                                "5546af9a-77aa-4d1b-aad4-adabd64d64f2",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product15",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "413a796e-e17d-455f-9f8c-ec9b7d77f389",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product16",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "99a46dea-70c3-47c0-99e1-3fa6a1bb3a14",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product17",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "87bf09e2-cbb9-44e5-a253-c3faf94c0c74",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product18",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "d6f9cb0f-5949-41b0-ab7d-2cadf4740d76",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product19",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "a2a77259-7ef3-4273-859f-0b0fd0e351e1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product20",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "25073100-ffbd-4afc-a4a7-0f09c22ebc97",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product21",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                },
                                {
                                  "id": "d49fbea9-8594-4940-afe8-4771b9aa1c86",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"width": "2.w"}
                                },
                                {
                                  "id": "3d740576-3ac8-4928-aeb3-df8257109a58",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "id":
                                          "d6b5a0d8-c466-4c46-b010-9aa1c95ee096",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category4",
                                        "elementGap": "2.2.sp",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000"
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          {
                                            "id":
                                                "7c87549a-61ef-4e8a-92d7-28eb16573282",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product22",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "2e07ffe5-0489-4ca3-ac31-64426461a7f4",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product23",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "8313edfa-71e4-41de-9e1a-c3acd40fe6bd",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product24",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "3cc48c33-1e1c-49f4-9e14-b1c9e75aa897",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product25",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "8245772e-3ed5-46c2-b8dc-5918b5a03957",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product26",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "4cd09032-c4d0-400c-81dd-8b57bd1b2e96",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product27",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000"
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "6865902a-4c82-4e9d-8e61-adfaa5750d7f",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product28",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000"
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.1.sp",
                                                  "color": "0xff000000"
                                                }
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          }
                        }
                      }
                    ]
                  }
                }
              }
            }
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1.jpg"
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2.jpg"
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3.jpg"
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_4.jpg"
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product21": {
        "key": "product21",
        "type": "product",
        "name": "Product 21",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product22": {
        "key": "product22",
        "type": "product",
        "name": "Product 22",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product23": {
        "key": "product23",
        "type": "product",
        "name": "Product 23",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product24": {
        "key": "product24",
        "type": "product",
        "name": "Product 24",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product25": {
        "key": "product25",
        "type": "product",
        "name": "Product 25",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product26": {
        "key": "product26",
        "type": "product",
        "name": "Product 26",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product27": {
        "key": "product27",
        "type": "product",
        "name": "Product 27",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product28": {
        "key": "product28",
        "type": "product",
        "name": "Product 28",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
