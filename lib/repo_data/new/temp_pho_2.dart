class JsonDataTemplate {
  // [112321TIN] run ok - ratio ok
  static final temppho2 = {
    "id": "12",
    "templateName": "temp pho 2",
    "templateSlug": "temp_pho_2",
    "description": null,
    "subscriptionLevel": "Medal",
    "image": "https://foodiemenu.co//wp-content/uploads/2023/11/temp_pho_2.png",
    "categoryId": "3",
    "ratios": "16:9,16:10,21:9,4:3",
    "ratioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "3.sp",
          "priceFontSize": "3.5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "100.sp",
          "imageWidth": "100.sp"
        },
        "21/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "7.sp",
          "productFontSize": "3.sp",
          "priceFontSize": "3.5.sp",
          "sloganFontSize": "5.sp",
          "categoryFontSize": "5.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "85.sp",
          "imageWidth": "85.sp"
        },
        "16/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "3.5.sp",
          "priceFontSize": "4.2.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "3.5.sp",
          "imageHeight": "120.sp",
          "imageWidth": "120.sp"
        },
        "16/10": {
          "categoryColumnVerticalPadding": "5.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "4.5.sp",
          "priceFontSize": "5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "4.2.sp",
          "imageHeight": "125.sp",
          "imageWidth": "125.sp"
        },
        "4/3": {
          "categoryColumnVerticalPadding": "12.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "5.sp",
          "priceFontSize": "5.5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "4.5.sp",
          "imageHeight": "130.sp",
          "imageWidth": "130.sp"
        },
        "default": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "7.sp",
          "productFontSize": "2.5.sp",
          "priceFontSize": "3.sp",
          "sloganFontSize": "5.sp",
          "categoryFontSize": "5.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "100.sp",
          "imageWidth": "100.sp"
        }
      }
    },
    "widgetSetting": {
      "id": "126554d1-54ec-4569-a992-15709fb852da",
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "decoration": {"image": "backgroundImage", "fit": "cover"},
        "child": {
          "id": "8de6cd3c-393d-4994-8fa1-fcd69b2e5f92",
          "widgetName": "Row",
          "widgetSetting": {
            "mainAxisAlignment": "spaceBetween",
            "crossAxisAlignment": "start",
            "children": [
              {
                "id": "a192b5ae-15c1-46f0-b9b4-68c88fbc9981",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "e73011c3-69bb-4d62-a29f-e39fce661f59",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "id": "d15291f7-3b68-4b83-97e5-ab480fca4b58",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "left": "-30.sp",
                            "bottom": "-15.sp",
                            "child": {
                              "id": "d7a54233-db40-4f39-aa63-3b95e703d0e1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "widthFromFieldKey": "imageWidth",
                                "heightFromFieldKey": "imageHeight",
                                "child": {
                                  "id": "67ef7aaf-418b-423a-840b-131ebfc90825",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "imageDataKey": "leftImage",
                                    "fit": "contain"
                                  }
                                }
                              }
                            }
                          }
                        },
                        {
                          "id": "16cf6edc-ee68-49c3-9a53-e91a9a2b738b",
                          "widgetName": "PaddingByRatio",
                          "widgetSetting": {
                            "verticalPaddingFieldKey":
                                "categoryColumnVerticalPadding",
                            "horizontalPaddingFieldKey":
                                "categoryColumnHorizontalPadding",
                            "child": {
                              "id": "0d863920-8538-4d1e-97f9-9889c08b9f50",
                              "widgetName": "CategoryColumn1",
                              "widgetSetting": {
                                "category": "category1",
                                "categoryFontSizeFromFieldKey":
                                    "categoryFontSize",
                                "categoryTextStyle": {
                                  "fontFamily": "Roboto Slab",
                                  "color": "0xffda2f15",
                                  "fontWeight": "w700"
                                },
                                "crossAxisAlignment": "center",
                                "productRows": [
                                  {
                                    "id":
                                        "ed4b3e73-dc6b-425b-affe-4eb3c7adb41d",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "id":
                                            "fe8e01fd-e300-4a16-8d15-9e1214742d5d",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "1",
                                          "product": "product1",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1"
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700"
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "d5ec8038-7506-4ee5-8db7-bfca5b6eca47",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "id":
                                            "5c6b6fda-354e-4b3c-944c-2ef79672a90b",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "2",
                                          "product": "product2",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1"
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700"
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "9a056732-62f5-4485-a6df-2fea909f4893",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "id":
                                            "37173df0-f893-41ec-80d0-e0caad15bc6c",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "3",
                                          "product": "product3",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1"
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700"
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "fb906698-ca97-4894-8a88-56d10df9e072",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "id":
                                            "12c9da0e-1aab-4075-adfc-10e3b765d4b3",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "4",
                                          "product": "product4",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1"
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700"
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "81b1d4b0-2e39-4458-b2c2-b0a5b4f382aa",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "id":
                                            "cdea72fe-85ac-48ee-9332-d431d9470478",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "5",
                                          "product": "product5",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1"
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700"
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp"
                                        }
                                      }
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              },
              {
                "id": "cb4947f4-7752-4080-98c9-36cce995d223",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "7dd42b7a-f130-40f7-b3f1-eafe7ae9ffed",
                    "widgetName": "PaddingByRatio",
                    "widgetSetting": {
                      "verticalPaddingFieldKey":
                          "categoryColumnVerticalPadding",
                      "horizontalPaddingFieldKey":
                          "categoryColumnHorizontalPadding",
                      "child": {
                        "id": "e5086404-bab1-4e5c-842a-66f1122b5fa4",
                        "widgetName": "MenuColumn1",
                        "widgetSetting": {
                          "brand": "PHO BRAND",
                          "slogan": "Pho, noodle and more",
                          "brandFontSizeFromFieldKey": "brandFontSize",
                          "sloganFontSizeFromFieldKey": "sloganFontSize",
                          "brandTextStyle": {
                            "fontFamily": "Pacifico",
                            "color": "0xffe1b971"
                          },
                          "sloganTextStyle": {
                            "fontFamily": "Roboto Slab",
                            "color": "0xffda2f15",
                            "fontWeight": "w700"
                          },
                          "menuItems": [
                            {
                              "id": "226b4122-8ae5-4e46-963f-023b37a152f2",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "id": "3dc3b155-daa0-4137-8945-bc3aeb1e27c8",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "6",
                                    "product": "product6",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1"
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700"
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp"
                                  }
                                }
                              }
                            },
                            {
                              "id": "362db2bd-8cb9-411b-a582-661f6c08199a",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "id": "e46965e9-5154-4c51-97aa-64c3b30b9599",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "7",
                                    "product": "product7",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1"
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700"
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp"
                                  }
                                }
                              }
                            },
                            {
                              "id": "3882ac17-498e-488c-a3b1-78bb84952b08",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "id": "fbe6741a-f354-4a07-a854-8e1acd62e3e7",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "8",
                                    "product": "product8",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1"
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700"
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp"
                                  }
                                }
                              }
                            },
                            {
                              "id": "5d932495-76dd-48ec-a0b1-2c32515418d4",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "id": "1c73789d-f9cf-410c-83c0-7e9f3ee8ef03",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "9",
                                    "product": "product9",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1"
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700"
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp"
                                  }
                                }
                              }
                            },
                            {
                              "id": "d93b4b94-4fb7-4a76-aa3b-3011de2713b2",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "id": "c47d5889-59bc-4662-bcf8-2cdd1b51b3e3",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "10",
                                    "product": "product10",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1"
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700"
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp"
                                  }
                                }
                              }
                            },
                            {
                              "id": "11ef76af-80d1-4c24-a391-3b8d06bc1982",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "id": "9b68f05e-6c0f-4288-8882-23886a543b9e",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "11",
                                    "product": "product11",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1"
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700"
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp"
                                  }
                                }
                              }
                            },
                            {
                              "id": "b49c0719-c09f-4392-a477-7ffd85be9932",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "id": "70ef510f-2ee9-4f70-b1ee-2ef334932298",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "12",
                                    "product": "product12",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1"
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700"
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp"
                                  }
                                }
                              }
                            },
                            {
                              "id": "a1ba0618-2ceb-4a2e-8ee4-2d2aef8ad849",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "id": "92c7b022-a8e2-4fcd-8ede-0eaadad9ef08",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "13",
                                    "product": "product13",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1"
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700"
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp"
                                  }
                                }
                              }
                            },
                            {
                              "id": "260830c3-9ccc-48a1-8bfc-c15ff4bac2a6",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "id": "aecdd7f0-005f-4c36-aec9-6fe94c3e5f37",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "14",
                                    "product": "product14",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1"
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700"
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp"
                                  }
                                }
                              }
                            },
                            {
                              "id": "5995389e-2844-49d9-bc68-be734566e8c2",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "id": "ff8793a5-993b-4a1b-b8c7-9842bbc34e6b",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "15",
                                    "product": "product15",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1"
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700"
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              {
                "id": "4d8745c2-b240-4b6b-aef4-3741df9d4dec",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "f7efb190-663c-4a8d-b9d9-0a4ebba24211",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "id": "466c9cc4-3983-40cf-9203-b41bf094923c",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "right": "-30.sp",
                            "top": "-15.sp",
                            "child": {
                              "id": "462f0549-3840-4bf1-8a56-b7d47d8232c2",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "widthFromFieldKey": "imageWidth",
                                "heightFromFieldKey": "imageHeight",
                                "child": {
                                  "id": "d2d958f0-a4cf-4d1b-930f-5d8e04d178c7",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "imageDataKey": "rightImage",
                                    "fit": "contain"
                                  }
                                }
                              }
                            }
                          }
                        },
                        {
                          "id": "d5cee625-d964-418b-b82f-2afa6182ba7f",
                          "widgetName": "PaddingByRatio",
                          "widgetSetting": {
                            "verticalPaddingFieldKey":
                                "categoryColumnVerticalPadding",
                            "horizontalPaddingFieldKey":
                                "categoryColumnHorizontalPadding",
                            "child": {
                              "id": "87901d03-d0b9-46a8-bced-49a94182d04f",
                              "widgetName": "CategoryColumn1",
                              "widgetSetting": {
                                "category": "category2",
                                "categoryFontSizeFromFieldKey":
                                    "categoryFontSize",
                                "categoryTextStyle": {
                                  "fontFamily": "Roboto Slab",
                                  "color": "0xffda2f15",
                                  "fontWeight": "w700"
                                },
                                "crossAxisAlignment": "center",
                                "mainAxisAlignment": "end",
                                "productRows": [
                                  {
                                    "id":
                                        "6b555cd5-41ad-4647-a5a8-ace30d57c7fb",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "id":
                                            "7f27a07a-0850-411c-9080-867466502bf1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "16",
                                          "product": "product16",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1"
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700"
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "ee692015-7d0a-407d-9101-e54c018a0391",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "id":
                                            "b1859feb-6a84-4fd6-8c0e-776259d7e49c",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "17",
                                          "product": "product17",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1"
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700"
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "aba7f8ee-8c4f-40fe-b0d0-4c1842ba32da",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "id":
                                            "4d859cf0-7b4b-4284-9c63-7243ab2889a8",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "18",
                                          "product": "product18",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1"
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700"
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "b0bc22a6-2686-4514-b5b7-03b2b692385f",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "id":
                                            "671bf490-8a49-457e-9f12-4d2f963f587e",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "19",
                                          "product": "product19",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1"
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700"
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp"
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "id":
                                        "1370ac57-d3d6-42fb-9c3b-ffb3a1245df3",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "id":
                                            "40893c8a-2bdd-43c7-9788-26c5ee395471",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "20",
                                          "product": "product20",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1"
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700"
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp"
                                        }
                                      }
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              }
            ]
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": ""
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": ""
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "backgroundImage": {
        "key": "backgroundImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/background-1.jpg"
      },
      "leftImage": {
        "key": "leftImage",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_1.png"
      },
      "rightImage": {
        "key": "rightImage",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_4.png"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
