class JsonDataTemplate {
  // [112321TIN] run ok - ratio ok
  static final temppho4 = {
    "id": "14",
    "templateName": "temp pho 4",
    "templateSlug": "temp_pho_4",
    "description": null,
    "subscriptionLevel": "Medal",
    "image": "https://foodiemenu.co//wp-content/uploads/2023/11/temp_pho_4.png",
    "categoryId": "3",
    "ratios": "16:9,16:10,21:9,4:3",
    "ratioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp"
        },
        "21/9": {
          "categoryNameFontSize": "5.sp",
          "categoryDesFontSize": "3.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp"
        },
        "16/9": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
          "productDesFontSize": "2.5.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp"
        },
        "16/10": {
          "categoryNameFontSize": "10.sp",
          "categoryDesFontSize": "4.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "2.8.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp"
        },
        "4/3": {
          "categoryNameFontSize": "12.sp",
          "categoryDesFontSize": "5.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp",
          "productDesFontSize": "3.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp"
        },
        "default": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp"
        }
      }
    },
    "widgetSetting": {
      "id": "1dc16759-de4b-4f01-8f40-cf947570ff92",
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "decoration": {"color": "0xffece3d8"},
        "child": {
          "id": "cfb1830b-55bf-4900-9796-6e5c4bb42134",
          "widgetName": "PaddingByRatio",
          "widgetSetting": {
            "verticalPaddingFieldKey": "verticalFramePadding",
            "horizontalPaddingFieldKey": "horizontalFramePadding",
            "child": {
              "id": "8c428750-4288-4923-b67e-3fe2d88499fb",
              "widgetName": "Wrap",
              "widgetSetting": {
                "direction": "vertical",
                "alignment": "center",
                "runAlignment": "center",
                "children": [
                  {
                    "id": "c77a3ada-9715-4fb1-9255-303755783f91",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "id": "2c36f6c0-afa8-4ca8-b754-27f18c3b2125",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category1",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            "color": "0xff583618"
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            "fontStyle": "italic",
                            "color": "0xff000000"
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "id": "783cbb39-b472-4a87-a819-d4f73413d29f",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w"
                            }
                          },
                          "categoryItems": [
                            {
                              "id": "01a8657e-6404-4cf7-80c8-cba39d40fa4b",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "a60dec1f-efa5-4415-93c1-3d23e3c43ec1",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product1",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            },
                            {
                              "id": "b3ed0f1a-4cf8-4dc5-bf6a-0b3f965b6e14",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "c9cc6b98-f12d-4465-b1fd-0894ef124a7c",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product2",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            },
                            {
                              "id": "3d04009b-dd61-468f-a659-5a52b7c0c1c3",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "9d11441f-4f89-4c07-8c82-d30c06f6621c",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product3",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            },
                            {
                              "id": "114ff43c-95fb-48b5-83c2-92d2b89e5179",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "id": "0d678f82-921a-4e49-a633-0d5e67eabb8a",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat1Image",
                                    "fit": "contain"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  },
                  {
                    "id": "2029d32e-6e53-4a9e-8493-d7a793c2db42",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "id": "7a7b6245-af27-4a1e-bc82-6951ebe0789f",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category2",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            "color": "0xff583618"
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            "fontStyle": "italic",
                            "color": "0xff000000"
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "id": "f678aeb0-b31a-4f03-8b43-7c355555d730",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w"
                            }
                          },
                          "categoryItems": [
                            {
                              "id": "c95fdab5-606b-4ede-96fe-d7461f5f0544",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "id": "0624fe93-f31a-40db-b003-c72a5c23940d",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat2Image",
                                    "fit": "contain"
                                  }
                                }
                              }
                            },
                            {
                              "id": "af5e19b5-b0b3-4321-acf3-55017ea40231",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "84b2c6f9-4cee-47bc-8a0a-59c8d6ad1f1a",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product4",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            },
                            {
                              "id": "ff0b442b-91b5-4a5b-83a3-9bf3784bdb88",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "62bd84fe-4809-4769-8b5c-5bd4a8cbb11a",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product5",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            },
                            {
                              "id": "53a16686-93d3-4478-944c-f8a077186675",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "426d6170-cc52-4b3e-a895-b6708b59223d",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product6",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  },
                  {
                    "id": "cc8d8371-6dcd-4b38-90fb-1c72993f2cda",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "id": "c3b7250d-7397-409a-8b15-2b178c6f1241",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category3",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            "color": "0xff583618"
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            "fontStyle": "italic",
                            "color": "0xff000000"
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "id": "efc3a7e2-801b-49fd-bfc8-fc684da97993",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w"
                            }
                          },
                          "categoryItems": [
                            {
                              "id": "d8feacac-f69f-42c2-bd71-c14bf8112c97",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "20394e0d-e697-464d-8140-9c1572771514",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product7",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            },
                            {
                              "id": "498a46a6-0e67-4eb2-a6b0-dbf3ba63430f",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "a92d391c-91f9-4c82-987d-e4173288958f",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product8",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            },
                            {
                              "id": "0cc02840-98c6-4bba-a529-dc8fc7686139",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "e9eab195-cbd7-4d85-833a-a57284aaa9e6",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product9",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            },
                            {
                              "id": "ae7f75a4-8792-4456-ab63-332a54992786",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "id": "57370709-77cb-4211-a2f6-6719872c130c",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat3Image",
                                    "fit": "contain"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  },
                  {
                    "id": "7aacb367-19ed-4699-88a2-13793fc546bc",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "id": "412a45be-b8c1-460b-9c41-6bf6c8b0c1ac",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category4",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            "color": "0xff583618"
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            "fontStyle": "italic",
                            "color": "0xff000000"
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "id": "6e0bc9dd-80b3-4ded-9f04-974c3e53bed1",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w"
                            }
                          },
                          "categoryItems": [
                            {
                              "id": "75a23058-bd4a-4a0b-a945-c38be76844e4",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "id": "23ca2f81-e23b-4718-b2ae-55a43bce0fe1",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat4Image",
                                    "fit": "contain"
                                  }
                                }
                              }
                            },
                            {
                              "id": "e40acc43-d6f9-4a70-8a37-7381604266c1",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "a3043515-9e96-4e3a-82ab-a56eff203a2b",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product10",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            },
                            {
                              "id": "abbde254-2e2f-4bd3-a92e-c18f650593ee",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "918043c0-233b-4d81-8a6d-e78cf0828125",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product11",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            },
                            {
                              "id": "727e7f8d-ebd3-460a-8afb-979d55b15711",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "id": "68464732-4fcd-4104-8dd5-74b7714e00d0",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product12",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "fontWeight": "w700",
                                      "color": "0xff000000"
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      "color": "0xff000000"
                                    },
                                    "fractionDigitsForPrice": "2"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "cat1Image": {
        "key": "cat1Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1.png"
      },
      "cat2Image": {
        "key": "cat2Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2.png"
      },
      "cat3Image": {
        "key": "cat3Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3.png"
      },
      "cat4Image": {
        "key": "cat4Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_4.png"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
