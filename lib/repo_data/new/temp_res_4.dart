class JsonDataTemplate {
  // [112321TIN] run ok - ratio ok
  static final tempres4 = {
    "id": "4",
    "templateName": "temp restaurant 4",
    "templateSlug": "temp_restaurant_4",
    "description": null,
    "subscriptionLevel": "Medal",
    "image":
        "https://foodiemenu.co//wp-content/uploads/2023/11/template_restaurant_4.png",
    "categoryId": "1",
    "ratios": "16:9,16:10,21:9,4:3",
    "ratioSetting": {
      "textStyle": {
        "categoryNameStyle": {"fontWeight": "bold", "fontFamily": "Bebas Neue"},
        "nameProductStyle": {"fontWeight": "w800", "fontFamily": "Bebas Neue"},
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Bebas Neue"
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans"
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans"
        },
        "priceProductStyle": {"fontWeight": "w800", "fontFamily": "Bebas Neue"},
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Bebas Neue"
        }
      },
      "renderScreen": {
        "32/9": {
          "categoryNameStyle": "1.5.w",
          "nameProductStyle": "1.w",
          "nameProductStyleMedium": "0.6.w",
          "priceProductStyle": "1.w",
          "priceProductStyleMedium": "0.6.w",
          "discriptionProductStyle": "0.6.w",
          "discriptionProductStyleMedium": "0.5.w",
          "imageHeight": "40.h",
          "marginBottomImage": "0.7.w",
          "marginBottomProduct": "1.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "2.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        },
        "21/9": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.3.w",
          "nameProductStyleMedium": "0.7.w",
          "priceProductStyle": "1.3.w",
          "priceProductStyleMedium": "0.7.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "40.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "1.5.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "2.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        },
        "16/9": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "40.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "3.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        },
        "16/10": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "40.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "3.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        },
        "4/3": {
          "categoryNameStyle": "3.5.w",
          "nameProductStyle": "2.w",
          "nameProductStyleMedium": "1.3.w",
          "priceProductStyle": "2.w",
          "priceProductStyleMedium": "1.3.w",
          "discriptionProductStyle": "1.3.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "40.h",
          "marginBottomImage": "1.3.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "3.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        },
        "default": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "40.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "3.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w"
        }
      }
    },
    "widgetSetting": {
      "id": "52ab9e82-7b26-4499-8009-cd2204f6ac8d",
      "widgetName": "Container",
      "widgetSetting": {
        "widht": "100.w",
        "height": "100.h",
        "decoration": {"image": "imageBackground", "boxFit": "fill"},
        "child": {
          "id": "2362a13a-ea9e-4801-85cd-3e845f6859ae",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                "id": "8c328722-f8a0-4caf-88e2-740e4e8c29f6",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "8edb7b91-213b-4bbe-8dac-25fc2552cd29",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "id": "4c98a862-8539-45e8-8418-08810720eda1",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "mainAxisAlignment": "start",
                          "children": [
                            {
                              "id": "b1aee9fd-d1e5-4c12-9c95-2c8c7f87407e",
                              "widgetName": "CategoryTitleGridLayout3",
                              "widgetSetting": {
                                "dataList": {
                                  "category": "category1",
                                  "product": [
                                    "product1",
                                    "product2",
                                    "product3",
                                    "product4",
                                    "product5",
                                    "product6"
                                  ]
                                },
                                "useBackgroundCategoryTitle": "true",
                                "imageId": "imageBgCategoryTitle",
                                "colorBackgroundCategoryTitle": "0x00121212",
                                "colorCategoryTitle": "0xFF121212",
                                "colorProductName": "0xFF121212",
                                "colorPrice": "0xFF8d1111",
                                "colorDescription": "0xFF121212",
                                "useFullWidthCategoryTitle": "false"
                              }
                            },
                            {
                              "id": "04ec9db9-9f6d-4435-81aa-ee8a4ef42780",
                              "widgetName": "PaddingByRatio",
                              "widgetSetting": {
                                "bottomPaddingDataKey":
                                    "marginBottomCategoryTitle"
                              }
                            },
                            {
                              "id": "ff19a0f7-955e-4a2a-bd7d-3276e9189ee1",
                              "widgetName": "CategoryTitleGridLayout4",
                              "widgetSetting": {
                                "dataList": {
                                  "category": "category2",
                                  "product": [
                                    "product7",
                                    "product8",
                                    "product9"
                                  ],
                                  "image": ["image1", "image2", "image3"]
                                },
                                "useBackgroundCategoryTitle": "true",
                                "imageId": "imageBgCategoryTitle",
                                "colorBackgroundCategoryTitle": "0x00212121",
                                "colorCategoryTitle": "0xFF121212",
                                "colorProductName": "0xFF121212",
                                "colorPrice": "0xFF8d1111",
                                "colorDescription": "0xFF121212"
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              {
                "id": "9e123ed6-9166-4240-83bd-3d634c5d7281",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "id": "739d0e45-18bf-4ae6-aa34-b09b673d24c5",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "id": "a913156c-67b4-4397-81d9-d28881166e49",
                        "widgetName": "Row",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "8af1d124-dace-489e-99f7-ac30598cf217",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "e7a2cda9-5fe6-4522-8dbd-f6a0cec4c275",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "children": [
                                      {
                                        "id":
                                            "f139ef41-14ac-4f2f-90f6-cbe517a47f1c",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "3f3841d2-cc84-4350-9b55-4e894b106299",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "start",
                                              "children": [
                                                {
                                                  "id":
                                                      "ae29c91f-28b9-46a0-b8d7-e1d82da52ba6",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "id":
                                                          "8414f033-db5a-4a2b-8e38-b28eea8d408a",
                                                      "widgetName": "Image",
                                                      "widgetSetting": {
                                                        "imageId": "image4"
                                                      }
                                                    }
                                                  }
                                                }
                                              ]
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "de04f5b1-fbaf-4431-be3b-e5e689a50f88",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "bottomPaddingDataKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },
                                      {
                                        "id":
                                            "60321622-187e-407d-9d0e-fbe2976f22a5",
                                        "widgetName":
                                            "CategoryTitleSingleColumn",
                                        "widgetSetting": {
                                          "dataList": {
                                            "category": "category3",
                                            "product": [
                                              "product10",
                                              "product11",
                                              "product12"
                                            ]
                                          },
                                          "useBackgroundCategoryTitle": "true",
                                          "imageId": "imageBgCategoryTitle",
                                          "colorCategoryTitle": "0xFF121212",
                                          "colorProductName": "0xFF121212",
                                          "colorPrice": "0xFF8d1111",
                                          "colorDescription": "0xFF121212"
                                        }
                                      },
                                      {
                                        "id":
                                            "a70293b2-582e-4356-8873-71aa8329865f",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "bottomPaddingDataKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },
                                      {
                                        "id":
                                            "ad7dfc5b-fc84-4518-bdd8-137ce7fbf106",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "29bc8e0e-d288-4dfe-bb98-fa24d3917cdb",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "start",
                                              "children": [
                                                {
                                                  "id":
                                                      "9e20447a-03e5-46e5-9bce-b1aa1813d280",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "id":
                                                          "5efe6c2f-e4da-4672-8569-410cef340655",
                                                      "widgetName": "Image",
                                                      "widgetSetting": {
                                                        "imageId": "image5"
                                                      }
                                                    }
                                                  }
                                                }
                                              ]
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },
                            {
                              "id": "1faa3b87-d743-4e0b-9edf-71f312b11959",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"width": "3.w"}
                            },
                            {
                              "id": "69fa2485-33d7-4d7d-8016-a936d15abffb",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "52170ee5-ee92-45f3-a0a4-b5dfce70f739",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "children": [
                                      {
                                        "id":
                                            "19bce3b9-16db-4355-a51c-a1db4391fcac",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "a07780d6-7ef6-4601-bedc-56d6454ef793",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category4",
                                                "product": [
                                                  "product13",
                                                  "product14",
                                                  "product15"
                                                ]
                                              },
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "imageId": "imageBgCategoryTitle",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorProductName": "0xFF121212",
                                              "colorPrice": "0xFF8d1111",
                                              "colorDescription": "0xFF121212"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "2b237ba8-50d4-47cc-a5cd-d42a589529ad",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "bottomPaddingDataKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },
                                      {
                                        "id":
                                            "dcc64cbc-3c4c-4939-8fc8-4629d3fb6200",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "369c0aec-e3c5-42f0-bda4-4ac43bdf5811",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category5",
                                                "product": [
                                                  "product16",
                                                  "product17",
                                                  "product18"
                                                ]
                                              },
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "imageId": "imageBgCategoryTitle",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorProductName": "0xFF121212",
                                              "colorPrice": "0xFF8d1111",
                                              "colorDescription": "0xFF121212"
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              }
            ]
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": ""
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": ""
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": ""
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": ""
      },
      "category5": {
        "key": "category5",
        "type": "category",
        "name": "Category 5",
        "image": ""
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$40.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$50.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$60.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$70.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "\$10.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "\$41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "\$42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "\$43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "\$16.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "\$17.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "\$18.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "imageBackground": {
        "key": "imageBackground",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/BG-2-1.png"
      },
      "imageBgCategoryTitle": {
        "key": "imageBgCategoryTitle",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/BG-title-2.png"
      },
      "image1": {
        "key": "image1",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg1-1.jpg"
      },
      "image2": {
        "key": "image2",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg2-1.jpg"
      },
      "image3": {
        "key": "image3",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg3-1.jpg"
      },
      "image4": {
        "key": "image4",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg4-2-scaled.jpg"
      },
      "image5": {
        "key": "image5",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-bg5-1-scaled.jpg"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
