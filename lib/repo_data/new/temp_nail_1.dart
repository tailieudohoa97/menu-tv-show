class JsonDataTemplate {
  static final tempnail1 = {
    "id": "6",
    "templateName": "temp nail 1",
    "templateSlug": "temp_nail_1",
    "description": null,
    "subscriptionLevel": "Medal",
    "image":
        "https://foodiemenu.co//wp-content/uploads/2023/11/temp_nail_1.png",
    "categoryId": "2",
    "ratios": "16:9,16:10,21:9,32:9,4:3",
    "ratioSetting": {
      "textStyle": {
        "categoryNameStyle": {
          "fontWeight": "normal",
          "fontFamily": "Gravitas One"
        },
        "nameProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "Titillium Web",
          "height": "1.5"
        },
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Open Sans",
          "height": "1.5"
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Titillium Web",
          "height": "1.5"
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans",
          "height": "1.5"
        },
        "priceProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "Titillium Web"
        },
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Open Sans"
        }
      },
      "renderScreen": {
        "32/9": {
          "nameProductStyle": "1.w",
          "priceProductStyle": "1.w",
          "categoryNameStyle": "1.4.w",
          "discriptionProductStyle": "0.8.w"
        },
        "21/9": {
          "nameProductStyle": "1.2.w",
          "priceProductStyle": "1.2.w",
          "categoryNameStyle": "1.4.w",
          "discriptionProductStyle": "0.85.w"
        },
        "16/9": {
          "nameProductStyle": "2.5.h",
          "priceProductStyle": "2.5.h",
          "categoryNameStyle": "4.h",
          "discriptionProductStyle": "2.h"
        },
        "16/10": {
          "nameProductStyle": "2.5.h",
          "priceProductStyle": "2.5.h",
          "categoryNameStyle": "3.w",
          "discriptionProductStyle": "2.h"
        },
        "4/3": {
          "nameProductStyle": "2.5.h",
          "priceProductStyle": "2.5.h",
          "categoryNameStyle": "4.h",
          "discriptionProductStyle": "2.h"
        },
        "default": {
          "nameProductStyle": "2.5.h",
          "priceProductStyle": "2.5.h",
          "categoryNameStyle": "4.h",
          "discriptionProductStyle": "2.h"
        }
      }
    },
    "widgetSetting": {
      "id": "1ae33826-71b7-47dd-8a6a-34ae03c1ba8d",
      "widgetName": "Container",
      "widgetSetting": {
        "child": {
          "id": "1b31401a-3c6b-4f11-a3ae-297975253b31",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                "id": "46f02ec3-b9fe-464c-826a-726d2ca17080",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "62fd1b74-d3b5-40c0-afc4-7a6ed452ef50",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "id": "a92e2ad3-b8b9-401b-85d1-4eb04cf90bfb",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "top": "5.h",
                            "right": "6.3.w",
                            "child": {
                              "id": "6782a655-ee21-4bee-9fbe-5a98b2b622c0",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "90.h",
                                "width": "23.7.w",
                                "child": {
                                  "id": "0f337850-188d-479a-9056-dd6fb5f50ca2",
                                  "widgetName": "Image",
                                  "widgetSetting": {
                                    "imageId": "image2",
                                    "fixCover": "true"
                                  }
                                }
                              }
                            }
                          }
                        },
                        {
                          "id": "6931df0f-0f67-487b-9466-024379ecb40e",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "top": "0",
                            "right": "3.w",
                            "child": {
                              "id": "4a1ca972-2db8-4285-bfa7-b8e846d55e5a",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "70.h",
                                "width": "23.4.w",
                                "color": "0xEEECEFF1",
                                "child": {
                                  "id": "11e0f618-0b2d-4ec3-8945-09a8f9982961",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "f6387e6d-3592-4708-ac3e-223796c6f29b",
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {
                                          "width": "20.h",
                                          "height": "20.h",
                                          "child": {
                                            "id":
                                                "6cb26352-c203-49d8-af9a-f111a638b8ae",
                                            "widgetName": "Image",
                                            "widgetSetting": {
                                              "imageId": "image1",
                                              "fixCover": "false"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "86a2ae2c-be20-458b-a37f-a6263d69e439",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "SPA",
                                          "style": {
                                            "letterSpacing": "1.w",
                                            "fontSize": "10.h",
                                            "fontFamily": "Gravitas One",
                                            "fontWeight": "normal"
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "8f220b7e-82eb-44fd-bd26-81c18005beb4",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "CARE YOUR BODY",
                                          "style": {
                                            "fontSize": "2.w",
                                            "fontWeight": "w800",
                                            "fontFamily": "Arvo"
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            }
                          }
                        },
                        {
                          "id": "77df68f3-fd94-4d7c-9cae-bd0786f44960",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "bottom": "0",
                            "right": "3.w",
                            "child": {
                              "id": "59bf584b-f753-45d3-8016-1d12561dc689",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "25.h",
                                "width": "23.4.w",
                                "color": "0xCCFCE4EC",
                                "child": {
                                  "id": "033b9b61-2cdf-4e06-8cba-9cbfdee29407",
                                  "widgetName": "Center",
                                  "widgetSetting": {
                                    "child": {
                                      "id":
                                          "aa4ed349-c703-4586-838d-b3c3d69664f2",
                                      "widgetName": "Text",
                                      "widgetSetting": {
                                        "data": "OPEN FROM 9AM TO 6PM",
                                        "style": {
                                          "fontSize": "3.h",
                                          "fontWeight": "w800",
                                          "fontFamily": "Arvo"
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              },
              {
                "id": "7779b434-5cfe-4b40-904e-8eea9354d662",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "fd452500-cc8d-4377-bfba-0e5e0b4f68b1",
                    "widgetName": "Center",
                    "widgetSetting": {
                      "child": {
                        "id": "76ec32c5-2bbf-408b-90b9-31eaeaf6b31d",
                        "widgetName": "CategoryTitleSingleColumn",
                        "widgetSetting": {
                          "colorCategoryTitle": "0xFF000000",
                          "colorProductName": "0xFF000000",
                          "colorDescription": "0xFF000000",
                          "runSpacing": "1.h",
                          "mainAxisAlignment": "center",
                          "dataList": {
                            "category": "category1",
                            "product": [
                              "product1",
                              "product2",
                              "product3",
                              "product4",
                              "product5",
                              "product6",
                              "product7",
                              "product8"
                            ]
                          },
                          "useBackgroundCategoryTitle": "false",
                          "colorPrice": "0xFF000000"
                        }
                      }
                    }
                  }
                }
              },
              {
                "id": "43c3f77c-bc8a-430f-a302-4aeb27f6a896",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "b1b905d5-e193-45a5-a862-55beb1096e6c",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "id": "b4c442e2-27bf-465d-b9fb-47a66c3e0fa8",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "top": "4.h",
                            "left": "3.w",
                            "child": {
                              "id": "d295b9c6-3ca6-4683-a9e1-c12f02730e29",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "44.h",
                                "width": "27.3.w",
                                "decoration": {
                                  "image": "image3",
                                  "boxFit": "cover"
                                }
                              }
                            }
                          }
                        },
                        {
                          "id": "2b72068a-6005-4bc0-a60d-140eaa62e2fb",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "bottom": "4.h",
                            "left": "3.w",
                            "child": {
                              "id": "ae796189-fdb9-4c55-bc42-a404340f09a6",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "44.h",
                                "width": "27.3.w",
                                "decoration": {
                                  "image": "image4",
                                  "boxFit": "cover"
                                }
                              }
                            }
                          }
                        },
                        {
                          "id": "927ecd48-6219-4c10-a15c-3b9b83d9b984",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "right": "6.w",
                            "child": {
                              "id": "28ff54d3-ac00-4953-90e1-74438957ff4e",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "5.w",
                                "height": "100.h",
                                "color": "0xEEECEFF1",
                                "child": {
                                  "id": "e627522c-749a-412d-a991-ed5c1f8099ce",
                                  "widgetName": "RotatedBox",
                                  "widgetSetting": {
                                    "quarterTurns": "3",
                                    "child": {
                                      "id":
                                          "eed3e5a9-ce0c-47e0-bc81-b05dea80c7ee",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "mainAxisAlignment": "spaceAround",
                                        "children": [
                                          {
                                            "id":
                                                "2c21d7d8-3adc-4047-95b3-c297ebe5335b",
                                            "widgetName": "Text",
                                            "widgetSetting": {
                                              "data": "wwww.salon.wwww",
                                              "style": {
                                                "fontSize": "1.2.w",
                                                "fontFamily": "Arvo"
                                              }
                                            }
                                          },
                                          {
                                            "id":
                                                "c00c7c0e-d50a-4416-8d8c-b65f4bc40f7e",
                                            "widgetName": "Text",
                                            "widgetSetting": {
                                              "data": "@salon.com",
                                              "style": {
                                                "fontSize": "1.2.w",
                                                "fontFamily": "Arvo"
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              }
            ]
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": ""
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "image1": {
        "id": "logo",
        "key": "logo",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/logo2-scaled.jpg"
      },
      "image2": {
        "key": "image2",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_img_landscape-scaled.jpg"
      },
      "image3": {
        "key": "image3",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_illustrate2.png"
      },
      "image4": {
        "key": "image4",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_illustrate1-scaled.jpg"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
