class JsonDataTemplate {
  // [112321TIN] run ok - ratio (fail at 32:9, 21:9)
  static final tempnail4 = {
    "id": "9",
    "templateName": "temp nail 4",
    "templateSlug": "temp_nail_4",
    "description": null,
    "subscriptionLevel": "Medal",
    "image":
        "https://foodiemenu.co//wp-content/uploads/2023/11/temp_nail_4.png",
    "categoryId": "2",
    "ratios": "16:9,16:10,21:9,32:9,4:3",
    "ratioSetting": {
      "textStyle": {
        "categoryNameStyle": {
          "fontWeight": "w800",
          "fontFamily": "Playfair Display"
        },
        "nameProductStyle": {
          "fontWeight": "w500",
          "fontFamily": "Open Sans",
          "height": "1.5"
        },
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Open Sans",
          "height": "1.5"
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans",
          "height": "1.5"
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans",
          "height": "1.5"
        },
        "priceProductStyle": {"fontWeight": "w500", "fontFamily": "Open Sans"},
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Open Sans"
        }
      },
      "renderScreen": {
        "32/9": {
          "categoryNameStyle": "1.3.w",
          "nameProductStyle": "1.1.w",
          "priceProductStyle": "1.1.w"
        },
        "21/9": {
          "categoryNameStyle": "1.9.w",
          "nameProductStyle": "1.7.w",
          "priceProductStyle": "1.7.w"
        },
        "16/9": {
          "categoryNameStyle": "3.2.h",
          "nameProductStyle": "2.5.h",
          "priceProductStyle": "2.5.h"
        },
        "16/10": {
          "categoryNameStyle": "3.h",
          "nameProductStyle": "2.5.h",
          "priceProductStyle": "2.5.h"
        },
        "4/3": {
          "categoryNameStyle": "3.2.w",
          "nameProductStyle": "2.2.w",
          "priceProductStyle": "2.2.w"
        },
        "default": {
          "categoryNameStyle": "3.h",
          "nameProductStyle": "2.5.h",
          "priceProductStyle": "2.5.h"
        }
      }
    },
    "widgetSetting": {
      "id": "a0400281-6fbf-4fec-90c1-59de1e9fff64",
      "widgetName": "Container",
      "widgetSetting": {
        "width": "100.w",
        "height": "100.h",
        "color": "0xFFF1F1EF",
        "child": {
          "id": "5cd2b6ec-9406-422f-824d-1be1c442a641",
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              {
                "id": "2229ab34-757e-49de-8d3f-b3cc18cccd61",
                "widgetName": "Row",
                "widgetSetting": {
                  "children": [
                    {
                      "id": "076d0847-f680-4b61-8fb5-3a4eb36c2397",
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": "30",
                        "child": {
                          "id": "034893b2-bc5d-44a5-bf3d-99f6c647a2d1",
                          "widgetName": "Padding",
                          "widgetSetting": {
                            "padding": {"horizontal": "2.w", "top": "5.h"},
                            "child": {
                              "id": "02465901-64db-4c49-8ee9-6d2b0a7b7417",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "mainAxisAlignment": "spaceAround",
                                "children": [
                                  {
                                    "id":
                                        "2aafa1a5-7953-4437-ae6a-217d4e2b9edd",
                                    "widgetName": "CategoryTitleSingleColumn",
                                    "widgetSetting": {
                                      "spaceBetweenCategoryProductList": "3.h",
                                      "colorCategoryTitle": "0xFF333A32",
                                      "colorProductName": "0xFF333A32",
                                      "colorDescription": "0xFF333A32",
                                      "dataList": {
                                        "category": "category1",
                                        "product": [
                                          "product1",
                                          "product2",
                                          "product3",
                                          "product4",
                                          "product5"
                                        ]
                                      },
                                      "indexCategory": "0",
                                      "useBackgroundCategoryTitle": "false",
                                      "startIndexProduct": "0",
                                      "endIndexProduct": "8",
                                      "colorPrice": "0xFF333A32"
                                    }
                                  },
                                  {
                                    "id":
                                        "5dcda769-4a5c-4955-b705-bbe5400ee772",
                                    "widgetName": "CategoryTitleSingleColumn",
                                    "widgetSetting": {
                                      "spaceBetweenCategoryProductList": "3.h",
                                      "colorCategoryTitle": "0xFF333A32",
                                      "colorProductName": "0xFF333A32",
                                      "colorDescription": "0xFF333A32",
                                      "colorPrice": "0xFF333A32",
                                      "dataList": {
                                        "category": "category2",
                                        "product": [
                                          "product6",
                                          "product7",
                                          "product8",
                                          "product9",
                                          "product10"
                                        ]
                                      },
                                      "indexCategory": "0",
                                      "useBackgroundCategoryTitle": "false",
                                      "startIndexProduct": "0",
                                      "endIndexProduct": "8"
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        }
                      }
                    },
                    {
                      "id": "e2ea60c6-6554-4c98-9a9b-d06bd06d6285",
                      "widgetName": "VerticalDivider",
                      "widgetSetting": {
                        "thickness": "1",
                        "color": "0xFFBCBCBA "
                      }
                    },
                    {
                      "id": "f19cf913-4e4d-4a5a-9981-37f9c71957ee",
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": "55",
                        "child": {
                          "id": "86fff630-a979-43de-8227-942de1c6afe8",
                          "widgetName": "Stack",
                          "widgetSetting": {
                            "children": [
                              {
                                "id": "ad310837-3f40-42cc-bacc-00362e71e7ae",
                                "widgetName": "Row",
                                "widgetSetting": {
                                  "children": [
                                    {
                                      "id":
                                          "4cdb5676-d38f-429b-aef7-a49f444799da",
                                      "widgetName": "Expanded",
                                      "widgetSetting": {
                                        "flex": "6",
                                        "child": {
                                          "id":
                                              "abd74806-1d0b-4ff0-af69-fbc89935f611",
                                          "widgetName": "Text",
                                          "widgetSetting": {"data": ""}
                                        }
                                      }
                                    },
                                    {
                                      "id":
                                          "e1d981f5-cfc6-455e-9e9e-204575dd9be7",
                                      "widgetName": "Expanded",
                                      "widgetSetting": {
                                        "flex": "4",
                                        "child": {
                                          "id":
                                              "affc8679-f662-49fc-af2d-f5a649afc69c",
                                          "widgetName": "Container",
                                          "widgetSetting": {
                                            "height": "100.h",
                                            "color": "0xFFCDC6B6",
                                            "child": {
                                              "id":
                                                  "706c21df-72e3-4889-97b9-1c4324336b2e",
                                              "widgetName": "Text",
                                              "widgetSetting": {"data": ""}
                                            }
                                          }
                                        }
                                      }
                                    }
                                  ]
                                }
                              },
                              {
                                "id": "0d34f214-f4ff-42c4-a78c-967c435b8382",
                                "widgetName": "Padding",
                                "widgetSetting": {
                                  "padding": {"top": "5.h", "left": "2.w"},
                                  "child": {
                                    "id":
                                        "c5cb7858-5e79-4aed-a3a2-104d2b0291d0",
                                    "widgetName": "Row",
                                    "widgetSetting": {
                                      "children": [
                                        {
                                          "id":
                                              "96858190-c244-47fe-a2e6-2796ef91f223",
                                          "widgetName": "Expanded",
                                          "widgetSetting": {
                                            "flex": "1",
                                            "child": {
                                              "id":
                                                  "c96c01b8-bcfb-49df-bd34-771860f20e96",
                                              "widgetName": "Column",
                                              "widgetSetting": {
                                                "mainAxisAlignment":
                                                    "spaceAround",
                                                "children": [
                                                  {
                                                    "id":
                                                        "15889bb1-1581-42ac-8b9e-ac3169de1727",
                                                    "widgetName":
                                                        "CategoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      "spaceBetweenCategoryProductList":
                                                          "3.h",
                                                      "colorCategoryTitle":
                                                          "0xFF333A32",
                                                      "colorProductName":
                                                          "0xFF333A32",
                                                      "colorDescription":
                                                          "0xFF333A32",
                                                      "dataList": {
                                                        "category": "category3",
                                                        "product": [
                                                          "product11",
                                                          "product12",
                                                          "product13",
                                                          "product14",
                                                          "product15"
                                                        ]
                                                      },
                                                      "indexCategory": "0",
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "startIndexProduct": "0",
                                                      "endIndexProduct": "8",
                                                      "colorPrice": "0xFF333A32"
                                                    }
                                                  },
                                                  {
                                                    "id":
                                                        "7450f1ef-c032-407f-b3ee-4fa17fa30c18",
                                                    "widgetName":
                                                        "CategoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      "spaceBetweenCategoryProductList":
                                                          "3.h",
                                                      "colorCategoryTitle":
                                                          "0xFF333A32",
                                                      "colorProductName":
                                                          "0xFF333A32",
                                                      "colorDescription":
                                                          "0xFF333A32",
                                                      "dataList": {
                                                        "category": "category4",
                                                        "product": [
                                                          "product16",
                                                          "product17",
                                                          "product18",
                                                          "product19",
                                                          "product20"
                                                        ]
                                                      },
                                                      "indexCategory": "0",
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "startIndexProduct": "0",
                                                      "endIndexProduct": "8",
                                                      "colorPrice": "0xFF333A32"
                                                    }
                                                  }
                                                ]
                                              }
                                            }
                                          }
                                        },
                                        {
                                          "id":
                                              "88bc9d8a-2039-4006-ac8c-9ac15b256803",
                                          "widgetName": "Expanded",
                                          "widgetSetting": {
                                            "flex": "1",
                                            "child": {
                                              "id":
                                                  "5ca85325-75f8-4e13-b33a-26610ef8203d",
                                              "widgetName": "Column",
                                              "widgetSetting": {
                                                "mainAxisAlignment":
                                                    "spaceAround",
                                                "children": [
                                                  {
                                                    "id":
                                                        "cf6dc7ef-fda3-4c61-bfa0-75f8259d547b",
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "height": "25.h",
                                                      "width": "23.w",
                                                      "decoration": {
                                                        "image": "image1",
                                                        "boxFit": "cover"
                                                      }
                                                    }
                                                  },
                                                  {
                                                    "id":
                                                        "0ac43a94-fea8-4a8c-aceb-cbcebbf7127a",
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "height": "25.h",
                                                      "width": "23.w",
                                                      "decoration": {
                                                        "image": "image2",
                                                        "boxFit": "cover"
                                                      }
                                                    }
                                                  },
                                                  {
                                                    "id":
                                                        "f6047646-6370-4b77-b923-ccc0f7a494cc",
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "height": "25.h",
                                                      "width": "23.w",
                                                      "decoration": {
                                                        "image": "image3",
                                                        "boxFit": "cover"
                                                      }
                                                    }
                                                  }
                                                ]
                                              }
                                            }
                                          }
                                        }
                                      ]
                                    }
                                  }
                                }
                              }
                            ]
                          }
                        }
                      }
                    },
                    {
                      "id": "6d12af6e-13f5-49e0-9bf9-6aa023bd1f25",
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": "15",
                        "child": {
                          "id": "9a765fb1-6f2e-405d-8022-41d32ceffd8b",
                          "widgetName": "RotatedBox",
                          "widgetSetting": {
                            "quarterTurns": "3",
                            "child": {
                              "id": "f8fedece-ba42-46e0-8a4f-518f80c25245",
                              "widgetName": "Center",
                              "widgetSetting": {
                                "child": {
                                  "id": "cc9147dd-2653-4727-874c-b2dcbd0a9e76",
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "data": "PRO NAIL SALON",
                                    "style": {
                                      "fontFamily": "Playfair Display",
                                      "fontSize": "5.w",
                                      "fontWeight": "bold"
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  ]
                }
              }
            ]
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": ""
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": ""
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": ""
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": ""
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 2.1",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 2.2",
        "price": "\$24.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 2.3",
        "price": "\$25.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 2.4",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 2.5",
        "price": "\$32.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 3.1",
        "price": "\$32.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 3.2",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 3.3",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 3.4",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 3.5",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 4.1",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 4.2",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product18": {
        "key": "product13",
        "type": "product",
        "name": "Product 4.3",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 4.4",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "product20": {
        "key": "product19",
        "type": "product",
        "name": "Product 4.5",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description": ""
      },
      "image1": {
        "id": "image1",
        "key": "image1",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/treatment_1.jpg"
      },
      "image2": {
        "id": "image2",
        "key": "image2",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_2.jpg"
      },
      "image3": {
        "id": "image3",
        "key": "image3",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/treatment_3.jpg"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
