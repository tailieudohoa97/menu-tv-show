class JsonDataTemplate {
  // [112321TIN] run ok - ratio (fail at 21:9)
  static final tempres5 = {
    "id": "5",
    "templateName": "temp restaurant 5",
    "templateSlug": "temp_restaurant_5",
    "description": null,
    "subscriptionLevel": "Medal",
    "image":
        "https://foodiemenu.co//wp-content/uploads/2023/11/template_restaurant_5.png",
    "categoryId": "1",
    "ratios": "16:9,16:10,21:9,32:9,4:3",
    "ratioSetting": {
      "textStyle": {
        "categoryNameStyle": {"fontWeight": "bold", "fontFamily": "Bebas Neue"},
        "nameProductStyle": {"fontWeight": "w800", "fontFamily": "Bebas Neue"},
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Bebas Neue"
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans"
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "Open Sans"
        },
        "priceProductStyle": {"fontWeight": "w800", "fontFamily": "Bebas Neue"},
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Bebas Neue"
        }
      },
      "renderScreen": {
        "32/9": {
          "categoryNameStyle": "1.w",
          "nameProductStyle": "0.8.w",
          "nameProductStyleMedium": "0.4.w",
          "priceProductStyle": "0.8.w",
          "priceProductStyleMedium": "0.4.w",
          "discriptionProductStyle": "0.4.w",
          "discriptionProductStyleMedium": "0.3.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0.4.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "2.h",
          "paddingVerticalCategorTilte": "0.4.w",
          "paddingHorizontalCategorTilte": "3.5.w"
        },
        "21/9": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.w",
          "nameProductStyleMedium": "0.5.w",
          "priceProductStyle": "1.w",
          "priceProductStyleMedium": "0.5.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.6.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0.5.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "2.h",
          "paddingVerticalCategorTilte": "0.5.w",
          "paddingHorizontalCategorTilte": "3.5.w"
        },
        "16/9": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.6.w",
          "imageHeight": "50.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "2.h",
          "paddingVerticalCategorTilte": "1.w",
          "paddingHorizontalCategorTilte": "3.5.w"
        },
        "16/10": {
          "categoryNameStyle": "2.5.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.7.w",
          "imageHeight": "50.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "2.h",
          "paddingVerticalCategorTilte": "1.w",
          "paddingHorizontalCategorTilte": "3.5.w"
        },
        "4/3": {
          "categoryNameStyle": "2.5.w",
          "nameProductStyle": "2.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "2.w",
          "priceProductStyleMedium": "1.3.w",
          "discriptionProductStyle": "1.3.w",
          "discriptionProductStyleMedium": "0.7.w",
          "imageHeight": "50.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "2.h",
          "paddingVerticalCategorTilte": "1.w",
          "paddingHorizontalCategorTilte": "3.5.w"
        },
        "default": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.6.w",
          "imageHeight": "50.h",
          "marginBottomImage": "1.w",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "2.h",
          "paddingVerticalCategorTilte": "1.w",
          "paddingHorizontalCategorTilte": "3.5.w"
        }
      }
    },
    "widgetSetting": {
      "id": "bbaefd10-6c1e-4423-8b25-36061d2b61ee",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"image": "imageBackground", "boxFit": "fill"},
        "child": {
          "id": "c50ef29b-6a38-4129-8018-a8d802701371",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                "id": "73155bc8-36af-4506-90a7-4a272ad3a715",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "0a41a4bd-f5b8-4327-b9f1-f2973ebd0a8d",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "3.w"},
                      "child": {
                        "id": "05d30951-9c68-4d86-8feb-ea8d7915b933",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "438bf34b-8b86-4277-9248-854ca6f73427",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "id": "7ecd3420-db5b-4f13-b3ad-b533165e0a3c",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "id":
                                            "2f2d42da-035c-4c2c-a960-1d3d68a13954",
                                        "widgetName": "CategoryTitle",
                                        "widgetSetting": {
                                          "categoryId": "category1",
                                          "useBackground": "true",
                                          "imageId": "imageBackgroundTitle",
                                          "useFullWidth": "false",
                                          "color": "0xFF121212",
                                          "colorBackground": "0x00121212"
                                        }
                                      },
                                      {
                                        "id":
                                            "d7b9087f-20e9-4378-ad09-f062d7fa081d",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "id":
                                                "78756cd1-1132-4def-8c42-9d8f9c1272b2",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "stretch",
                                              "children": [
                                                {
                                                  "id":
                                                      "4bc0aed0-a070-401c-b18c-46371dc15d81",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "flex": "1",
                                                    "child": {
                                                      "id":
                                                          "5af0457d-6b51-4541-acd0-25151c6189a0",
                                                      "widgetName":
                                                          "ProductList",
                                                      "widgetSetting": {
                                                        "dataList": [
                                                          "product1",
                                                          "product2",
                                                          "product3"
                                                        ],
                                                        "mainAxisAlignment":
                                                            "start",
                                                        "crossAxisAlignment":
                                                            "center",
                                                        "colorPrice":
                                                            "0xFFf4b91a",
                                                        "colorProductName":
                                                            "0xFFFFFFFF",
                                                        "toUpperCaseNameProductName":
                                                            "true",
                                                        "useMediumStyle":
                                                            "false",
                                                        "colorDescription":
                                                            "0xFFFFFFFF",
                                                        "useThumnailProduct":
                                                            "false",
                                                        "pathThumnailProduct":
                                                            "",
                                                        "maxLineProductName":
                                                            "1",
                                                        "maxLineProductDescription":
                                                            "2"
                                                      }
                                                    }
                                                  }
                                                },
                                                {
                                                  "id":
                                                      "9d54317e-73d6-4134-892f-c808f045f89b",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "width": "5.w",
                                                    "padding": {
                                                      "vertical": "2.w"
                                                    },
                                                    "decoration": {
                                                      "image":
                                                          "imageSeperatorLine",
                                                      "boxFit": "contain"
                                                    }
                                                  }
                                                },
                                                {
                                                  "id":
                                                      "56067464-95db-43ab-8a04-f3a8f5793b14",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "id":
                                                          "fecef1ef-0df8-448b-8a0d-0973d34b75ca",
                                                      "widgetName": "Padding",
                                                      "widgetSetting": {
                                                        "padding": {
                                                          "all": "1.w"
                                                        },
                                                        "child": {
                                                          "id":
                                                              "31f60776-5ece-4dd9-8fe7-3cdcc6396314",
                                                          "widgetName": "Image",
                                                          "widgetSetting": {
                                                            "imageId": "image1",
                                                            "fixCover": "false"
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                }
                                              ]
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },
                            {
                              "id": "ea934593-3d6c-4915-8aca-830f14bef249",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "id": "5e69ebfe-eddc-45fb-8d4c-482b95d47284",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      {
                                        "id":
                                            "a642353d-e28e-483e-9aae-06521e9b7aca",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "4c592889-8c7f-456b-9d84-8c58ba10fbbf",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category2",
                                                "product": [
                                                  "product4",
                                                  "product5",
                                                  "product6"
                                                ]
                                              },
                                              "indexCategory": "1",
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "imageId": "imageBackgroundTitle",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "caaa2935-5df0-4857-a28b-1ffe85eeb8e4",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"},
                                          "decoration": {
                                            "image": "imageSeperatorLine",
                                            "boxFit": "contain"
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "2538fd0c-0d9f-499e-a35e-2776ab9782c5",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "fbf688ca-c9a5-4ed1-b0f3-d0b0a9a22e1e",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category3",
                                                "product": [
                                                  "product7",
                                                  "product8",
                                                  "product9"
                                                ]
                                              },
                                              "indexCategory": "2",
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "imageId": "imageBackgroundTitle",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              {
                "id": "60fc22bb-b03b-4976-857f-859d1943fab9",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "id": "9249cf83-d70f-4dd0-a82d-786196b22c71",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "3.w"},
                      "child": {
                        "id": "e283586e-cb2c-4dba-90c4-78b2aa11077c",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "id": "9d180893-8025-477d-852b-0b9f6e502e04",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "id": "424022ee-2827-497a-8047-0b1ffeae2fef",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      {
                                        "id":
                                            "8e55c901-cbb5-482f-bfa9-70446d5b139d",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "c9073f4c-6e35-41e7-8083-c78e56da3dd9",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category4",
                                                "product": [
                                                  "product10",
                                                  "product11",
                                                  "product12"
                                                ]
                                              },
                                              "indexCategory": "3",
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "imageId": "imageBackgroundTitle",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "d261492d-8f1b-4092-bfb8-e6e75cfd7b2a",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"},
                                          "decoration": {
                                            "image": "imageSeperatorLine",
                                            "boxFit": "contain"
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "276dd1d7-a291-44ac-b8b9-82d7eafc4fa7",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "5dbfee4f-658c-432a-82cb-114e1bff3ddc",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category5",
                                                "product": [
                                                  "product13",
                                                  "product14",
                                                  "product15"
                                                ]
                                              },
                                              "indexCategory": "4",
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "imageId": "imageBackgroundTitle",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },
                            {
                              "id": "f0396431-5a5f-46ae-977e-8008056f8e61",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "id": "dfe1773f-9ce8-4b37-8b27-8d4ddb63d4e9",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      {
                                        "id":
                                            "feb23f09-5472-41a4-aeac-4e0672967b8f",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "e927c63a-e7bb-445a-b424-b61b7cfc7132",
                                            "widgetName":
                                                "CategoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category6",
                                                "product": [
                                                  "product16",
                                                  "product17",
                                                  "product18"
                                                ]
                                              },
                                              "indexCategory": "5",
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "imageId": "imageBackgroundTitle",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "id":
                                            "c015a061-ac90-46ee-8e29-ed1169389b7f",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "id":
                                                "ad8fb095-3fef-4f5c-986d-652417a7297f",
                                            "widgetName": "Padding",
                                            "widgetSetting": {
                                              "padding": {"all": "1.w"},
                                              "child": {
                                                "id":
                                                    "24a2effc-a27b-453f-92a2-d08d296e530b",
                                                "widgetName": "Image",
                                                "widgetSetting": {
                                                  "imageId": "image2",
                                                  "fixCover": "false"
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              }
            ]
          }
        }
      }
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "/temp_restaurant_2/food(1).png"
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "/temp_restaurant_2/food(2).png"
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "/temp_restaurant_2/food(3).png"
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "/temp_restaurant_2/food(4).png"
      },
      "category5": {
        "key": "category5",
        "type": "category",
        "name": "Category 5",
        "image": "/temp_restaurant_2/food(5).png"
      },
      "category6": {
        "key": "category6",
        "type": "category",
        "name": "Category 6",
        "image": "/temp_restaurant_2/food(6).png"
      },
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$40.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$50.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$60.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$70.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "\$10.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "\$41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "\$42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "\$43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "\$16.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "\$17.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "\$18.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
      },
      "imageBackground": {
        "key": "imageBackground",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-1-1.png"
      },
      "imageBackgroundTitle": {
        "key": "imageBackgroundTitle",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/09/BG-title.png"
      },
      "imageSeperatorLine": {
        "key": "imageSeperatorLine",
        "type": "image",
        "fixed": "true",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/Seperator-Line-1.png"
      },
      "image1": {
        "key": "image1",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/hamburger1.png"
      },
      "image2": {
        "key": "image2",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/09/hamburger2.png"
      }
    },
    "isDeleted": "0",
    "status": "1",
    "created": "2023-10-09 21:51:36"
  };
}
