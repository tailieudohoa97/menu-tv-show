class JsonDataTemplate {
  //[092328HG] Check temprestaurant2: Run OK - Ratio OK
  static final temprestaurant2 = {
    "_id": "64ee744398c3d4605b477abc",
    "templateSlug": "temp_restaurant_2",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'w900',
          'fontFamily': 'Open Sans',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '0.8.w',
          'priceProductStyle': '0.8.w',
          'discriptionProductStyle': '0.7.w',
          'imageHeight': '25.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '21/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '0.9.w',
          'priceProductStyle': '0.9.w',
          'discriptionProductStyle': '0.8.w',
          'imageHeight': '30.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '16/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.w',
          'imageHeight': '30.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '16/10': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.w',
          'imageHeight': '40.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '4/3': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.8.w',
          'priceProductStyle': '1.8.w',
          'discriptionProductStyle': '1.3.w',
          'imageHeight': '40.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.w',
          'imageHeight': '37.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '/temp_restaurant_2/food(1).png',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '/temp_restaurant_2/food(2).png',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '/temp_restaurant_2/food(3).png',
      },
      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '/temp_restaurant_2/food(4).png',
      },

      //Danh mục 1: pro 1, 2 ,3, 4
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$21.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 2: pro 5, 6, 7, 8
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$22.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$24.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$25.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 3: pro 9, 10, 11, 12
      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 9',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 10',
        'price': '\$32.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 11',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 12',
        'price': '\$41.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Hình background
      'imageBackground': {
        'key': 'imageBackground',
        'type': 'image',
        // 'image': '/temp_restaurant_2/BG-2.png',
        'image': 'https://foodiemenu.co/wp-content/uploads/2023/09/BG-2.png',
      },

      //Hình Background Title - No link for this template
      'imageBackgroundTitle': {
        'key': 'imageBackgroundTitle',
        'type': 'image',
        'image': '', //No link for this template
      },

      //Hình danh mục 1
      'image1': {
        'key': 'image1',
        'type': 'image',
        // 'image': '/temp_restaurant_2/food(1).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food1.png',
      },

      //Hình danh mục 2
      'image2': {
        'key': 'image2',
        'type': 'image',
        // 'image': '/temp_restaurant_2/food(2).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food2-1.png',
      },

      //Hình danh mục 3
      'image3': {
        'key': 'image3',
        'type': 'image',
        // 'image': '/temp_restaurant_2/food(3).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food3.png',
      },
    },

    "widgets_setting": {
      "key": "Container-fullScreen",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image": "imageBackground", //key imageBackground
          "boxFit": "cover",
        },
        "child": {
          "widgetName": "Row",
          "widgetSetting": {
            "children": [
              //Cột 1:
              {
                "key": "expanded-A1",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Padding-A1",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "key": "column-A1",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "key": "expanded-A1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "key": "column-A11",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "children": [
                                      //Tiêu đề Danh mục 1
                                      {
                                        "key": "categoryTitle-A111",
                                        "widgetName": "categoryTitle",
                                        "widgetSetting": {
                                          "categoryId":
                                              "category1", //key category1
                                          "useBackground": "true",
                                          "imageId":
                                              "imageBackgroundTitle", //key imageBackgroundTitle
                                          "useFullWidth": "true",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E",
                                        }
                                      },

                                      //Hình đại diện Danh mục 1
                                      {
                                        "key": "expanded-A121",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "container-A1",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "height":
                                                  "30.h", //Dùng biến imageHeight
                                              "margin": {
                                                "bottom": "0.h"
                                              }, //Dùng biến marginBottomImage
                                              "padding": {"vertical": "1.5.w"},
                                              "decoration": {
                                                // "color": "0x18FFFFFF",
                                                "borderRadius": "30",
                                              },
                                              "child": {
                                                "key": "Center-A1",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "widgetName": "image",
                                                    "widgetSetting": {
                                                      "imageId":
                                                          "image1", //key = image1
                                                      "fixCover": "false",
                                                    },
                                                  },
                                                },
                                              },
                                            }
                                          },
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            },

                            // Danh sách sản phẩm của Danh mục 1
                            {
                              "key": "expanded-A1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "key": "productList-A1",
                                  "widgetName": "productList",
                                  "widgetSetting": {
                                    "dataList": [
                                      "product1",
                                      "product2",
                                      "product3",
                                      "product4",
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22",
                                    "colorProductName": "",
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "colorDescription": "",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              //Cột 2:
              {
                "key": "expanded-A3",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Padding-A3",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "key": "column-A3",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "key": "expanded-A31",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "key": "column-A31",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "children": [
                                      //Tiêu đề Danh mục 2
                                      {
                                        "key": "categoryTitle-A3",
                                        "widgetName": "categoryTitle",
                                        "widgetSetting": {
                                          "categoryId":
                                              "category2", //key category1
                                          "useBackground": "true",
                                          "imageId":
                                              "imageBackgroundTitle", //key imageBackgroundTitle
                                          "useFullWidth": "true",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E",
                                        }
                                      },

                                      //Hình đại diện Danh mục 2
                                      {
                                        "key": "expanded-A3",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "container-A3",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "height":
                                                  "30.h", //Dùng biến imageHeight
                                              "margin": {
                                                "bottom": "0.h"
                                              }, //Dùng biến marginBottomImage
                                              "decoration": {
                                                // "color": "0x18FFFFFF",
                                                "borderRadius": "30",
                                              },
                                              "padding": {"vertical": "1.5.w"},
                                              "child": {
                                                "key": "Center-A3",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "widgetName": "image",
                                                    "widgetSetting": {
                                                      "imageId":
                                                          "image2", //key = image2
                                                      "fixCover": "false",
                                                    },
                                                  },
                                                },
                                              },
                                            }
                                          },
                                        }
                                      },
                                    ]
                                  }
                                },
                              }
                            },

                            // Danh sách của Danh mục 2
                            {
                              "key": "expanded-A3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "key": "productList-x",
                                  "widgetName": "productList",
                                  "widgetSetting": {
                                    "dataList": [
                                      "product5",
                                      "product6",
                                      "product7",
                                      "product8",
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22",
                                    "colorProductName": "",
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "colorDescription": "",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              //Cột 3:
              {
                "key": "expanded-A3",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Padding-A3",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "key": "column-A3",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "key": "expanded-A3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "key": "column-A3",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "children": [
                                      //Tiêu đề Danh mục 3
                                      {
                                        "key": "categoryTitle-A3",
                                        "widgetName": "categoryTitle",
                                        "widgetSetting": {
                                          "categoryId":
                                              "category3", //key category3
                                          "useBackground": "true",
                                          "imageId":
                                              "imageBackgroundTitle", //key imageBackgroundTitle
                                          "useFullWidth": "true",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E",
                                        }
                                      },

                                      //Hình đại diện Danh mục 3
                                      {
                                        "key": "expanded-A3",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "container-A3",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "height":
                                                  "30.h", //Dùng biến imageHeight
                                              "margin": {
                                                "bottom": "0.h"
                                              }, //Dùng biến marginBottomImage
                                              "decoration": {
                                                // "color": "0x18FFFFFF",
                                                "borderRadius": "30",
                                              },
                                              "padding": {"vertical": "1.5.w"},
                                              "child": {
                                                "key": "Center-A3",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "widgetName": "image",
                                                    "widgetSetting": {
                                                      "imageId":
                                                          "image3", //key = image3
                                                      "fixCover": "false",
                                                    },
                                                  },
                                                },
                                              },
                                            }
                                          },
                                        }
                                      },
                                    ]
                                  }
                                },
                              }
                            },

                            // Danh sách của Danh mục 3
                            {
                              "key": "expanded-A3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "key": "productList-3",
                                  "widgetName": "productList",
                                  "widgetSetting": {
                                    "dataList": [
                                      "product9",
                                      "product10",
                                      "product11",
                                      "product12",
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22",
                                    "colorProductName": "",
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "colorDescription": "",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
            ]
          }
        }
      }
    }
  };
}
