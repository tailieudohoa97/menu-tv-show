class JsonDataTemplate {
  // [112315TIN] double check: oke
  // [112313TIN] run oke, supported ratios: 4:3, 16:10, 16:9, 21:9
  static final temppho6 = {
    "_id": "id_tem18",
    "templateSlug": "temp_pho_6",
    "ratioSetting": {
      // not use textStyle for this template
      // "textStyle": {},
      "renderScreen": {
        "32/9": {
          "menuVerticalPadding": "5.sp",
          "menuHorizontalPadding": "15.sp",
          "categoryNameFontSize": "5.sp",
          "productFontSize": "4.sp",
          "titleFontSize": "12.sp",
          "subTitleFontSize": "6.sp",
          "normalFontSize": "3.8.sp",
          "productGap": "1.5.sp",
          "categoryProductGap": "3.sp",
          "rowGap": "11.6.sp",
          "columnGap": "18.sp",
          "bottomLeftSectionHeight": "40.sp",
          "bottomLeftSectionWidth": "60.sp",
          "bottomLeftImageSize": "42.sp",
          "decoSize1": "8.sp",
          "decoSize2": "4.sp",
          "topRightSectionHeight": "50.sp",
          "topRightSectionWidth": "60.sp",
          "topRightImageSize": "55.sp",
          "category1TopMargin": "4.sp",
          "dividerTopMargin": "0",
          "dividerBottomMargin": "2.sp",
        },
        "21/9": {
          "menuVerticalPadding": "5.sp",
          "menuHorizontalPadding": "15.sp",
          "categoryNameFontSize": "5.sp",
          "productFontSize": "4.sp",
          "titleFontSize": "12.sp",
          "subTitleFontSize": "6.sp",
          "normalFontSize": "3.8.sp",
          "productGap": "1.5.sp",
          "categoryProductGap": "3.sp",
          "rowGap": "11.6.sp",
          "columnGap": "18.sp",
          "bottomLeftSectionHeight": "40.sp",
          "bottomLeftSectionWidth": "60.sp",
          "bottomLeftImageSize": "42.sp",
          "decoSize1": "8.sp",
          "decoSize2": "4.sp",
          "topRightSectionHeight": "50.sp",
          "topRightSectionWidth": "60.sp",
          "topRightImageSize": "55.sp",
          "category1TopMargin": "4.sp",
          "dividerTopMargin": "0",
          "dividerBottomMargin": "2.sp",
        },
        "16/9": {
          "menuVerticalPadding": "14.sp",
          "menuHorizontalPadding": "15.sp",
          "categoryNameFontSize": "6.sp",
          "productFontSize": "4.4.sp",
          "titleFontSize": "16.sp",
          "subTitleFontSize": "8.sp",
          "normalFontSize": "4.sp",
          "productGap": "2.5.sp",
          "categoryProductGap": "4.5.sp",
          "rowGap": "10.8.sp",
          "columnGap": "22.sp",
          "bottomLeftSectionHeight": "60.sp",
          "bottomLeftSectionWidth": "90.sp",
          "bottomLeftImageSize": "60.sp",
          "decoSize1": "15.sp",
          "decoSize2": "10.sp",
          "topRightSectionHeight": "72.sp",
          "topRightSectionWidth": "80.sp",
          "topRightImageSize": "70.sp",
          "category1TopMargin": "4.sp",
          "dividerTopMargin": "0",
          "dividerBottomMargin": "2.sp",
        },
        "16/10": {
          "menuVerticalPadding": "14.sp",
          "menuHorizontalPadding": "15.sp",
          "categoryNameFontSize": "6.sp",
          "productFontSize": "5.sp",
          "titleFontSize": "16.sp",
          "subTitleFontSize": "8.sp",
          "normalFontSize": "4.sp",
          "productGap": "2.8.sp",
          "categoryProductGap": "4.8.sp",
          "rowGap": "16.4.sp",
          "columnGap": "18.sp",
          "bottomLeftSectionHeight": "60.sp",
          "bottomLeftSectionWidth": "90.sp",
          "bottomLeftImageSize": "55.sp",
          "decoSize1": "15.sp",
          "decoSize2": "10.sp",
          "topRightSectionHeight": "75.sp",
          "topRightSectionWidth": "90.sp",
          "topRightImageSize": "70.sp",
          "category1TopMargin": "5.sp",
          "dividerTopMargin": "1.sp",
          "dividerBottomMargin": "2.sp",
        },
        "4/3": {
          "menuVerticalPadding": "14.sp",
          "menuHorizontalPadding": "15.sp",
          "categoryNameFontSize": "7.sp",
          "productFontSize": "6.3.sp",
          "titleFontSize": "20.sp",
          "subTitleFontSize": "9.sp",
          "normalFontSize": "5.sp",
          "productGap": "3.8.sp",
          "categoryProductGap": "5.2.sp",
          "rowGap": "16.4.sp",
          "columnGap": "18.sp",
          "bottomLeftSectionHeight": "70.sp",
          "bottomLeftSectionWidth": "100.sp",
          "bottomLeftImageSize": "63.sp",
          "decoSize1": "15.sp",
          "decoSize2": "10.sp",
          "topRightSectionHeight": "85.sp",
          "topRightSectionWidth": "105.sp",
          "topRightImageSize": "80.sp",
          "category1TopMargin": "6.sp",
          "dividerTopMargin": "0",
          "dividerBottomMargin": "2.sp",
        },
        "default": {
          "menuVerticalPadding": "14.sp",
          "menuHorizontalPadding": "15.sp",
          "categoryNameFontSize": "6.sp",
          "productFontSize": "4.4.sp",
          "titleFontSize": "16.sp",
          "subTitleFontSize": "8.sp",
          "normalFontSize": "4.sp",
          "productGap": "2.5.sp",
          "categoryProductGap": "4.5.sp",
          "rowGap": "10.8.sp",
          "columnGap": "22.sp",
          "bottomLeftSectionHeight": "60.sp",
          "bottomLeftSectionWidth": "90.sp",
          "bottomLeftImageSize": "60.sp",
          "decoSize1": "15.sp",
          "decoSize2": "10.sp",
          "topRightSectionHeight": "72.sp",
          "topRightSectionWidth": "80.sp",
          "topRightImageSize": "70.sp",
          "category1TopMargin": "4.sp",
          "dividerTopMargin": "0",
          "dividerBottomMargin": "2.sp",
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "CATEGORY 1",
        "image": "",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "CATEGORY 2",
        "image": "",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "CATEGORY 3",
        "image": "",
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "CATEGORY 4",
        "image": "",
      },

      //Danh mục 1: pro 1, 2 ,3, 4, 5
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$40.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Danh mục 2: pro 6, 7, 8, 9, 10, 11, 12
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$50.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$60.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$70.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "\$10.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "\$41.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      //Danh mục 3: pro 13, 14, 15, 16, 17, 18
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "\$42.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "\$43.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "\$16.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "\$42.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "\$43.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Danh mục 4: pro 19, 20, 21, 22, 23, 24, 25, 26, 27
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "\$16.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product21": {
        "key": "product21",
        "type": "product",
        "name": "Product 21",
        "price": "\$50.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product22": {
        "key": "product22",
        "type": "product",
        "name": "Product 22",
        "price": "\$60.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product23": {
        "key": "product23",
        "type": "product",
        "name": "Product 23",
        "price": "\$70.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product24": {
        "key": "product24",
        "type": "product",
        "name": "Product 24",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product25": {
        "key": "product25",
        "type": "product",
        "name": "Product 25",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product26": {
        "key": "product26",
        "type": "product",
        "name": "Product 26",
        "price": "\$10.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product27": {
        "key": "product27",
        "type": "product",
        "name": "Product 27",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Hình background
      "imageBackground": {
        "key": "imageBackground",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/Black-Restaurant-Menu.webp",
      },

      //Hình góc trái bên dưới của menu
      "bottomLeftImage": {
        "key": "bottomLeftImage",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/bottom_left_cat.webp",
      },

      //Hình góc phải bên dưới của menu
      "topRightImage": {
        "key": "topRightImage",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/top_right_cat.webp",
      },

      // Hình deco 1
      "decoImage1": {
        "key": "decoImage1",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/s-3.webp",
      },

      // Hình deco 2
      "decoImage2": {
        "key": "decoImage2",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/t.png",
      },

      // Hình deco 3
      "decoImage3": {
        "key": "decoImage3",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/s-2.webp",
      },

      // Hình deco 4
      "decoImage4": {
        "key": "decoImage4",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/s-1.webp",
      },

      // Hình deco 5
      "decoImage5": {
        "key": "decoImage5",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/s.webp",
      },

      // Hình deco 6
      "decoImage6": {
        "key": "decoImage6",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/s-4.webp",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgetSetting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image":
              "imageBackground", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
          "boxFit": "fill",
        },
        "width": "100.w",
        "child": {
          "key": "",
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              // Bottom left section
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "bottom": "0",
                  "left": "0",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "heightFromFieldKey": "bottomLeftSectionHeight",
                      "widthFromFieldKey": "bottomLeftSectionWidth",
                      "child": {
                        "key": "",
                        "widgetName": "Stack",
                        "widgetSetting": {
                          "children": [
                            // Deco 2
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "1.h",
                                "left": "1.h",
                                "child": {
                                  "key": "",
                                  "widgetName": "SizedBoxByRatio",
                                  "widgetSetting": {
                                    "heightFromFieldKey": "decoSize1",
                                    "widthFromFieldKey": "decoSize1",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SimpleImage",
                                      "widgetSetting": {
                                        "imageDataKey": "decoImage1",
                                        "fit": "contain",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Deco 2
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "0.5.h",
                                "left": "15.h",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "1",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "heightFromFieldKey": "decoSize2",
                                        "widthFromFieldKey": "decoSize2",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage2",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Another deco 2
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "3.h",
                                "right": "20.h",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "1.6",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "heightFromFieldKey": "decoSize2",
                                        "widthFromFieldKey": "decoSize2",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage2",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Deco 3
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "10.h",
                                "right": "15.h",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "-0.5",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "heightFromFieldKey": "decoSize2",
                                        "widthFromFieldKey": "decoSize2",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage3",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Deco 4
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "18.h",
                                "right": "6.h",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "-0.5",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "heightFromFieldKey": "decoSize1",
                                        "widthFromFieldKey": "decoSize1",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage4",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Deco 5
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "bottom": "-1.h",
                                "right": "1.h",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "-0.5",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "heightFromFieldKey": "decoSize1",
                                        "widthFromFieldKey": "decoSize1",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage5",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Bottom left image
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "left": "0",
                                "bottom": "-15.sp",
                                "child": {
                                  "key": "",
                                  "widgetName": "SizedBoxByRatio",
                                  "widgetSetting": {
                                    "heightFromFieldKey": "bottomLeftImageSize",
                                    "widthFromFieldKey": "bottomLeftImageSize",
                                    "child": {
                                      "key": "",
                                      "widgetName": "Container",
                                      "widgetSetting": {
                                        "clipBehavior": "hardEdge",
                                        "decoration": {
                                          "borderRadius": "50.sp",
                                        },
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "bottomLeftImage",
                                            "fit": "cover",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ]
                        }
                      }
                    }
                  }
                }
              },
              // Top right section
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "top": "0",
                  "right": "0",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "color": "0x50ff0000",
                      "heightFromFieldKey": "topRightSectionHeight",
                      "widthFromFieldKey": "topRightSectionWidth",
                      "child": {
                        "key": "",
                        "widgetName": "Stack",
                        "widgetSetting": {
                          "children": [
                            // Deco 2
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "0.5.h",
                                "left": "10.h",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "1",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "heightFromFieldKey": "decoSize2",
                                        "widthFromFieldKey": "decoSize2",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage2",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Another deco 2
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "bottom": "13.h",
                                "left": "15.h",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "1.6",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "heightFromFieldKey": "decoSize2",
                                        "widthFromFieldKey": "decoSize2",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage2",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Another deco 2
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "bottom": "3.h",
                                "right": "10.h",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "1.6",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "heightFromFieldKey": "decoSize2",
                                        "widthFromFieldKey": "decoSize2",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage2",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Top right image
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "-15.sp",
                                "right": "-10.sp",
                                "child": {
                                  "key": "",
                                  "widgetName": "Container",
                                  "widgetSetting": {
                                    "clipBehavior": "hardEdge",
                                    "decoration": {
                                      "borderRadius": "50.sp",
                                    },
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "heightFromFieldKey":
                                            "topRightImageSize",
                                        "widthFromFieldKey":
                                            "topRightImageSize",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "topRightImage",
                                            "fit": "cover",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ]
                        }
                      }
                    }
                  }
                }
              },
              // Menu
              {
                "key": "",
                "widgetName": "PaddingByRatio",
                "widgetSetting": {
                  "verticalPaddingFieldKey": "menuVerticalPadding",
                  "horizontalPaddingFieldKey": "menuHorizontalPadding",
                  "child": {
                    "key": "",
                    "widgetName": "Row",
                    "widgetSetting": {
                      "children": [
                        {
                          // Column 1
                          "key": "",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "key": "",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "crossAxisAlignment": "start",
                                "children": [
                                  {
                                    "key": "",
                                    "widgetName": "Text",
                                    "widgetSetting": {
                                      "data": "PHO",
                                      "style": {
                                        "fontWeight": "w600",
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffffffff",
                                        "height": "1",
                                      },
                                      "fontSizeFromFieldKey": "titleFontSize",
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "Text",
                                    "widgetSetting": {
                                      "data": "BRAND",
                                      "style": {
                                        "fontWeight": "w600",
                                        "fontFamily": "Rajdhani",
                                        "height": "1",
                                        "color": "0xffffffff"
                                      },
                                      "fontSizeFromFieldKey":
                                          "subTitleFontSize",
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      // "height": "1.sp",
                                      "heightFromFieldKey": "dividerTopMargin"
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "width": "10.sp",
                                      "child": {
                                        "key": "",
                                        "widgetName": "Divider",
                                        "widgetSetting": {
                                          "thickness": "0.7.sp",
                                          "color": "0xffffffff"
                                        }
                                      },
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      // "height": "1.sp",
                                      "heightFromFieldKey":
                                          "dividerBottomMargin"
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "Text",
                                    "widgetSetting": {
                                      "data": "📞      +123-456-7890",
                                      "style": {
                                        "fontWeight": "w500",
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffffffff"
                                      },
                                      "fontSizeFromFieldKey": "normalFontSize",
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "height": "2.sp",
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "Text",
                                    "widgetSetting": {
                                      "data": "🌐      www.domain.com",
                                      "style": {
                                        "fontWeight": "w500",
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffffffff"
                                      },
                                      "fontSizeFromFieldKey": "normalFontSize",
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      // "height": "4.sp",
                                      "heightFromFieldKey": "category1TopMargin"
                                    }
                                  },
                                  // Category title 1
                                  {
                                    "key": "",
                                    "widgetName": "Container",
                                    "widgetSetting": {
                                      "padding": {
                                        "vertical": "0.5.h",
                                        "horizontal": "0.5.w",
                                      },
                                      "color": "0xffe6e6e6",
                                      "child": {
                                        "key": "",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "dataFromSetting": "category1.name",
                                          "style": {
                                            "fontWeight": "w700",
                                            "fontFamily": "Rajdhani",
                                            "color": "0xff000000",
                                          },
                                          "fontSizeFromFieldKey":
                                              "categoryNameFontSize",
                                        }
                                      }
                                    },
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey":
                                          "categoryProductGap",
                                    }
                                  },
                                  // Product 1
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product1",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 2
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product2",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 3
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product3",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 4
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product4",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 5
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product5",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        },
                        // Spacing
                        {
                          "key": "",
                          "widgetName": "SizedBoxByRatio",
                          "widgetSetting": {
                            "widthFromFieldKey": "columnGap",
                          }
                        },
                        {
                          // Column 2
                          "key": "",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "key": "",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "crossAxisAlignment": "start",
                                "children": [
                                  // Category title 2
                                  {
                                    "key": "",
                                    "widgetName": "Container",
                                    "widgetSetting": {
                                      "padding": {
                                        "vertical": "0.5.h",
                                        "horizontal": "0.5.w",
                                      },
                                      "color": "0xffe6e6e6",
                                      "child": {
                                        "key": "",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "dataFromSetting": "category2.name",
                                          "style": {
                                            "fontWeight": "w700",
                                            "fontFamily": "Rajdhani",
                                            "color": "0xff000000"
                                          },
                                          "fontSizeFromFieldKey":
                                              "categoryNameFontSize",
                                        }
                                      }
                                    },
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey":
                                          "categoryProductGap",
                                    }
                                  },
                                  // Product 6
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product6",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 7
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product7",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 8
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product8",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 9
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product9",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 10
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product10",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 11
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product11",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 12
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product12",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "rowGap",
                                    }
                                  },
                                  // Category title 3
                                  {
                                    "key": "",
                                    "widgetName": "Container",
                                    "widgetSetting": {
                                      "padding": {
                                        "vertical": "0.5.h",
                                        "horizontal": "0.5.w",
                                      },
                                      "color": "0xffe6e6e6",
                                      "child": {
                                        "key": "",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "dataFromSetting": "category3.name",
                                          "style": {
                                            "fontWeight": "w700",
                                            "fontFamily": "Rajdhani",
                                            "color": "0xff000000"
                                          },
                                          "fontSizeFromFieldKey":
                                              "categoryNameFontSize",
                                        }
                                      }
                                    },
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey":
                                          "categoryProductGap",
                                    }
                                  },
                                  // Product 13
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product13",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 14
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product14",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 15
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product15",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 16
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product16",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 17
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product17",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 18
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product18",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                ],
                              }
                            }
                          }
                        },
                        // Spacing
                        {
                          "key": "",
                          "widgetName": "SizedBoxByRatio",
                          "widgetSetting": {
                            "widthFromFieldKey": "columnGap",
                          }
                        },
                        {
                          // Column 3
                          "key": "",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "key": "",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "mainAxisAlignment": "end",
                                "crossAxisAlignment": "start",
                                "children": [
                                  // Category title 4
                                  {
                                    "key": "",
                                    "widgetName": "Container",
                                    "widgetSetting": {
                                      "padding": {
                                        "vertical": "0.5.h",
                                        "horizontal": "0.5.w",
                                      },
                                      "color": "0xffe6e6e6",
                                      "child": {
                                        "key": "",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "dataFromSetting": "category4.name",
                                          "style": {
                                            "fontWeight": "w700",
                                            "fontFamily": "Rajdhani",
                                            "color": "0xff000000"
                                          },
                                          "fontSizeFromFieldKey":
                                              "categoryNameFontSize",
                                        }
                                      }
                                    },
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey":
                                          "categoryProductGap",
                                    }
                                  },
                                  // Product 19
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product19",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 20
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product20",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 21
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product21",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 22
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product22",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 23
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product23",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 24
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product24",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 25
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product25",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 26
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product26",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap",
                                    }
                                  },
                                  // Product 27
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product27",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Rajdhani",
                                        "color": "0xffe6e6e6"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                ]
                              }
                            }
                          }
                        },
                      ]
                    }
                  }
                }
              }
            ],
          }
        }
      }
    }
  };
}
