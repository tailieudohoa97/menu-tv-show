class JsonDataTemplate {
  //[092328HG] Check temprestaurant1: Run OK - Ratio OK
  static final temprestaurant1 = {
    "_id": "64ee744398c3d4605b477abc",
    "templateSlug": "temp_restaurant_1",
    "RatioSetting": {
      //092314LH Fix dynamic sample data to template
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        '21/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '0.9.w',
          'nameProductStyleMedium': '0.7.w',
          'priceProductStyle': '0.9.w',
          'priceProductStyleMedium': '0.7.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.5.w',
          'imageHeight': '13.h',
          'marginBottomImage': '1.5.w',
          'marginBottomProduct': '1.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        '16/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '1.5.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '16/10': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '1.7.w',
          'nameProductStyleMedium': '1.5.w',
          'priceProductStyle': '1.7.w',
          'priceProductStyleMedium': '1.5.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '18.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.5.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        '4/3': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '1.8.w',
          'nameProductStyleMedium': '1.5.w',
          'priceProductStyle': '1.8.w',
          'priceProductStyleMedium': '1.5.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.7.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      //092314LH Fix dynamic sample data to template
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '/temp_restaurant_2/food(1).png',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '/temp_restaurant_2/food(2).png',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '/temp_restaurant_2/food(3).png',
      },
      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '/temp_restaurant_2/food(4).png',
      },
      'category5': {
        'key': 'category5',
        'type': 'category',
        'name': 'Category 5',
        'image': '/temp_restaurant_2/food(5).png',
      },
      //Danh mục 1: pro 1, 2 ,3
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 2: pro 4, 5, 6, 7, 8
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 2.1',
        'price': '\$21.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 2.2',
        'price': '\$22.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 2.3',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 2.4',
        'price': '\$24.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 2.5',
        'price': '\$25.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 3: pro 9, 10, 11
      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 3.1',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 3.2',
        'price': '\$32.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 3.3',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 4: pro 12, 13, 14
      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 4.1',
        'price': '\$41.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product13': {
        'key': 'product13',
        'type': 'product',
        'name': 'Product 4.2',
        'price': '\$42.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product14': {
        'key': 'product14',
        'type': 'product',
        'name': 'Product 4.3',
        'price': '\$43.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 5: pro 15, 16, 17
      'product15': {
        'key': 'product15',
        'type': 'product',
        'name': 'Product 5.1',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product16': {
        'key': 'product16',
        'type': 'product',
        'name': 'Product 5.2',
        'price': '\$41.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product17': {
        'key': 'product17',
        'type': 'product',
        'name': 'Product 5.3',
        'price': '\$51.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Hình background
      'imageBackground': {
        'key': 'imageBackground',
        'type': 'image',
        // 'image': '/temp_restaurant_1/BG.png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/BG.png',
      },
      //Hình phân tách cột
      'imageSeperatorLine': {
        'key': 'imageSeperatorLine',
        'type': 'image',
        'fixed': 'true',
        // 'image': '/temp_restaurant_1/decorate/Seperator-Line.png',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/Seperator-Line.png',
      },
      //Danh mục 1: 3 hình số 1,2,3
      'image1': {
        'key': 'image1',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food(2).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food2.png',
      },
      'image2': {
        'key': 'image2',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food(4).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food4.png',
      },
      'image3': {
        'key': 'image3',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food(8).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food8.png',
      },

      //Danh mục 2: 3 hình số 4,5,6
      'image4': {
        'key': 'image4',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(1).jpg',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg1.jpg',
      },
      'image5': {
        'key': 'image5',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(2).jpg',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg2.jpg',
      },
      'image6': {
        'key': 'image6',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(3).jpg',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg3.jpg',
      },

      //Danh mục 3-4: 3 hình số 7,8,9
      'image7': {
        'key': 'image7',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(4).jpg',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg4.jpg',
      },
      'image8': {
        'key': 'image8',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(5).jpg',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg5.jpg',
      },
      'image9': {
        'key': 'image9',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(6).jpg',
        'image':
            'https://foodiemenu.co/wp-content/uploads/2023/09/food-bg6.jpg',
      },

      //Danh mục 5: 3 hình số 10
      'image10': {
        'key': 'image10',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food(7).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food7.png',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image":
              "imageBackground", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
          "boxFit": "fill",
        },
        "child": {
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              // Cột A
              {
                "key": "expanded-A",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "padding-A",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            // Cột A - Dòng 1
                            {
                              "key": "expanded-A1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "widgetName": "Container",
                                  "widgetSetting": {
                                    "margin": {
                                      "bottom": "1.w"
                                    }, // Dùng biến marginBottomImage

                                    "child": {
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "children": [
                                          // Cột A - Dòng 1 - Cột 1: Danh mục 1 với 3 sản phẩm 1,2,3
                                          {
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "2",
                                              "child": {
                                                "widgetName": "Container",
                                                "widgetSetting": {
                                                  "decoration": {
                                                    "color": "0x18FFFFFF",
                                                    "borderRadius":
                                                        "30", // Dùng biến boderRadiusStyle
                                                  },
                                                  "padding": {"all": "1.5.w"},
                                                  "child": {
                                                    "key": "CTSC-A11",
                                                    "widgetName":
                                                        "categoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      "dataList": {
                                                        'category': "category1",
                                                        'product': [
                                                          "product1",
                                                          "product2",
                                                          "product3",
                                                        ],
                                                      },
                                                      "indexCategory": "0",
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "colorCategoryTitle":
                                                          "0xFFF47B22",
                                                      "startIndexProduct": "0",
                                                      "endIndexProduct": "3",
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          },
                                          // Cột A - Dòng 1 - Cột 2: Sizedbox khoảng cách
                                          {
                                            "key": "SizedBox-A12",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "1.5.w"}
                                          },
                                          // Cột A - Dòng 1 - Cột 3: 3 hình đại diện
                                          {
                                            "key": "expanded-A13",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "column-A13",
                                                "widgetName": "Column",
                                                "widgetSetting": {
                                                  "mainAxisAlignment": "start",
                                                  "children": [
                                                    {
                                                      "key": "expanded-A13-1",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "widgetName": "image",
                                                          "widgetSetting": {
                                                            "imageId":
                                                                "image1", //key = image1
                                                            "fixCover": "false"
                                                          },
                                                        },
                                                      }
                                                    },
                                                    {
                                                      "key": "expanded-A13-2",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "widgetName": "image",
                                                          "widgetSetting": {
                                                            "imageId":
                                                                "image2", //key = image2
                                                            "fixCover": "false"
                                                          }
                                                        },
                                                      }
                                                    },
                                                    {
                                                      "key": "expanded-A13-3",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "widgetName": "image",
                                                          "widgetSetting": {
                                                            "imageId":
                                                                "image3", //key = image3
                                                            "fixCover": "false"
                                                          }
                                                        },
                                                      }
                                                    },
                                                  ]
                                                }
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              }
                            },

                            // Cột A - Dòng 2 - Khoảng cách
                            {
                              "key": "SizedBox-2",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"height": "2.h"}
                            },

                            // Cột A - Dòng 3 - Danh mục 2 với
                            // 3 hình ảnh 4,5,6
                            // 5 sản phẩm 4,5,6,7,8
                            {
                              "widgetName": "categoryTitleGridLayout1",
                              "widgetSetting": {
                                "dataList": {
                                  'category': "category2",
                                  'product': [
                                    "product4",
                                    "product5",
                                    "product6",
                                    "product7",
                                    "product8",
                                  ],
                                  'image': [
                                    "image4",
                                    "image5",
                                    "image6",
                                  ],
                                },
                                "useBackgroundCategoryTitle": "false",
                                "colorPrice": "0xFFF47B22",
                              }
                            },
                          ]
                        }
                      }
                    }
                  }
                }
              },
              //Cột B
              {
                "key": "expanded-B",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "padding-B",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "key": "column-B",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "mainAxisAlignment": "start",
                          "children": [
                            {
                              //Cột B - Dòng 1: 3 hình ảnh số 7,8,9
                              "key": "row-B1",
                              "widgetName": "Row",
                              "widgetSetting": {
                                "textRightToLeft": "false",
                                "crossAxisAlignment": "start",
                                "mainAxisAlignment": "start",
                                "children": [
                                  //hình số 7
                                  {
                                    "key": "expanded-A13-3",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "widgetName": "image",
                                        "widgetSetting": {
                                          "imageId": "image7", //key = image7
                                          "fixCover": "true"
                                        }
                                      },
                                    }
                                  },

                                  //[092318HG] Add sizedbox
                                  {
                                    "key": "SizedBox-A13-3-1",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {"width": "1.w"}
                                  },

                                  //hình số 8
                                  {
                                    "key": "expanded-A13-3",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "widgetName": "image",
                                        "widgetSetting": {
                                          "imageId": "image8", //key = image8
                                          "fixCover": "true"
                                        }
                                      },
                                    }
                                  },

                                  //[092318HG] Add sizedbox
                                  {
                                    "key": "SizedBox-A13-3-2",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {"width": "1.w"}
                                  },

                                  //hình số 9
                                  {
                                    "key": "expanded-A13-3",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "widgetName": "image",
                                        "widgetSetting": {
                                          "imageId": "image9", //key = image9
                                          "fixCover": "true"
                                        }
                                      },
                                    }
                                  },
                                ]
                              },
                            },
                            //Cột B - Dòng 2: Danh mục 3 và Danh mục 4
                            {
                              "key": "expanded-B2",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  //Cột B - Dòng 2
                                  "key": "row-B2",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      {
                                        "key": "expanded-B2",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            //Cột B - Dòng 2 - Cột 1: Danh mục 3 với
                                            //3 sản phẩm 9,10,11
                                            "key": "CTSC-B21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category3",
                                                'product': [
                                                  "product9",
                                                  "product10",
                                                  "product11",
                                                ],
                                              },
                                              "indexCategory": "2",
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "startIndexProduct": "0",
                                              "endIndexProduct": " 3",
                                              "colorPrice": "0xFFf47b22",
                                            }
                                          },
                                        },
                                      },
                                      //Cột B - Dòng 2 - Cột 2: Hình phân tách 2 danh mục
                                      {
                                        "key": "container-B22",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"},
                                          "child": {
                                            "key": "image-B22",
                                            "widgetName": "image",
                                            "widgetSetting": {
                                              "imageId":
                                                  "imageSeperatorLine", //key = imageSeperatorLine
                                              "fixCover": "false"
                                            }
                                          }
                                        }
                                      },

                                      // Cột B - Dòng 2 - Cột 3: Danh mục 4 với
                                      // 3 sản phẩm 12,13,14,
                                      {
                                        "key": "expanded-B23",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "CTSC-B21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category4",
                                                'product': [
                                                  "product12",
                                                  "product13",
                                                  "product14",
                                                ],
                                              },
                                              "indexCategory": "3",
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "startIndexProduct": "0",
                                              "endIndexProduct": "3",
                                              "colorPrice": "0xFFf47b22",
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  },
                                },
                              },
                            },

                            //Cột B - Dòng 3: Danh mục 5 và hình số 10
                            {
                              "key": "expanded-B3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  //Cột B - Dòng 3
                                  "key": "row-B3",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      //Cột B - Dòng 3 - Cột 1: Danh mục 5 với
                                      //3 sản phẩm 15,16,17
                                      {
                                        "key": "expanded-x",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "CTSC-B31",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category5",
                                                'product': [
                                                  "product15",
                                                  "product16",
                                                  "product17",
                                                ],
                                              },
                                              "indexCategory": "4",
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "startIndexProduct": "0",
                                              "endIndexProduct": "3",
                                              "colorPrice": "0xFFf47b22",
                                            }
                                          },
                                        }
                                      },

                                      //Cột B - Dòng 3 - Cột 2: hình số 10
                                      {
                                        "key": "expanded-B32",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "padding-B32",
                                            "widgetName": "Padding",
                                            "widgetSetting": {
                                              "padding": {"all": "1.w"},
                                              "child": {
                                                "key": "image-B32",
                                                "widgetName": "image",
                                                "widgetSetting": {
                                                  "imageId":
                                                      "image10", //key = image10
                                                  "fixCover": "false"
                                                }
                                              }
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  },
                                },
                              },
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              }
            ]
          }
        }
      }
    }
  };
}
