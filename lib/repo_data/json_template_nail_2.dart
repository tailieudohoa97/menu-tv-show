class JsonDataTemplate {
  //[092328HG] Check Nail2: Run OK - Ratio OK
  //[092304Tuan] adjust according to feedback from Mr Hugo
  //[092305Tuan] adjust according to feedback from Mr Hugo
  static final templateNail2 = {
    "_id": "",
    "templateSlug": "temp_nail_2",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'w700',
          'fontFamily': 'Nunito Sans',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.w',
          'nameProductStyle': '1.w',
          'priceProductStyle': '0.9.w',
          "discriptionProductStyle": '0.55.w',
        },
        '21/9': {
          'categoryNameStyle': '1.6.w',
          'nameProductStyle': '1.4.w',
          'priceProductStyle': '1.4.w',
          "discriptionProductStyle": '0.8.w',
        },
        '16/9': {
          'categoryNameStyle': '1.7.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          "discriptionProductStyle": '1.3.w',
        },
        '16/10': {
          'categoryNameStyle': '1.9.w',
          'nameProductStyle': '1.7.w',
          'priceProductStyle': '1.7.w',
          "discriptionProductStyle": '1.1.w',
        },
        '4/3': {
          'categoryNameStyle': '2.8.w',
          'nameProductStyle': '2.4.w',
          'priceProductStyle': '2.4.w',
          "discriptionProductStyle": '1.2.w',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.25.w',
          'priceProductStyle': '1.25.w',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '',
      },
      //Danh mục 1: pro 1, 2 ,3 ,4 , 5
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$25',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      //Danh mục 2: pro 6,7,8,9,10
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$24.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$25.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },

      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 9',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 10',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      //Danh mục 3: pro 11,12

      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 2.1',
        'price': '\$32.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 2.2',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },

      'image1': {
        'id': 'image1',
        'key': 'image1',
        'type': 'image',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/image1.jpg',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      // begin: container
      "widgetName": "CustomContainer",
      "widgetSetting": {
        "color": "0x4400539C",
        "blendMode": "dstATop",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/background-scaled.jpg",
        // "boxFit": "cover",
        "width": "100.w",
        "height": "100.h",
        "child": {
          "widgetName": "Row",
          "widgetSetting": {
            "children": [
              {
                //column A
                "widgetName": "Container",
                "widgetSetting": {
                  "width": "33.33.w",
                  "padding": {"bottom": "4.5.h", "horizontal": "3.w"},
                  "child": {
                    "widgetName": "Column",
                    "widgetSetting": {
                      "children": [
                        {
                          "widgetName": "SizedBox",
                          "widgetSetting": {
                            "height": "3.h",
                          },
                        },
                        {
                          "widgetName": "Divider",
                          "widgetSetting": {
                            "thickness": "2",
                            "height": "6.h",
                            "color": "0x8F634201",
                          },
                        },
                        {
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "widgetName": "Column",
                              "widgetSetting": {
                                "mainAxisAlignment": "spaceBetween",
                                "children": [
                                  {
                                    "widgetName": "categoryTitle",
                                    "widgetSetting": {
                                      "categoryId": "category1",
                                      "color": "0xFF895B3A"
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product1",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product2",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product3",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product4",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product5",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                ],
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                }
              },
              {
                "widgetName": "VerticalDivider",
                "widgetSetting": {
                  "thickness": "2",
                  "color": "0x8F634201",
                },
              },
              {
                //column B
                "widgetName": "Container",
                "widgetSetting": {
                  "width": "33.33.w",
                  "padding": {"bottom": "4.5.h", "horizontal": "3.w"},
                  "child": {
                    "widgetName": "Column",
                    "widgetSetting": {
                      "children": [
                        {
                          "widgetName": "SizedBox",
                          "widgetSetting": {
                            "height": "3.h",
                          },
                        },
                        {
                          "widgetName": "Row",
                          "widgetSetting": {
                            "children": [
                              {
                                "widgetName": "Expanded",
                                "widgetSetting": {
                                  "child": {
                                    "widgetName": "Divider",
                                    "widgetSetting": {
                                      "thickness": "2",
                                      "height": "6.h",
                                      "color": "0x8F634201",
                                    },
                                  },
                                },
                              },
                              {
                                "widgetName": "Center",
                                "widgetSetting": {
                                  "child": {
                                    "widgetName": "Container",
                                    "widgetSetting": {
                                      "padding": {"horizontal": "3.w"},
                                      "color": "0x00000000",
                                      "height": "6.h",
                                      "child": {
                                        "widgetName": "Center",
                                        "widgetSetting": {
                                          "child": {
                                            "widgetName": "Text",
                                            "widgetSetting": {
                                              "data": "OUR TREATMENTS",
                                              "style": {
                                                "fontSize": "3.h",
                                                "fontWeight": "w700",
                                                "color": "0xFF895B3A",
                                                "fontFamily": "Nunito Sans",
                                              },
                                            },
                                          },
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                "widgetName": "Expanded",
                                "widgetSetting": {
                                  "child": {
                                    "widgetName": "Divider",
                                    "widgetSetting": {
                                      "thickness": "2",
                                      "height": "6.h",
                                      "color": "0x8F634201",
                                    },
                                  },
                                },
                              },
                            ],
                          },
                        },
                        {
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "widgetName": "Column",
                              "widgetSetting": {
                                "mainAxisAlignment": "spaceBetween",
                                "children": [
                                  {
                                    "widgetName": "categoryTitle",
                                    "widgetSetting": {
                                      "categoryId": "category2",
                                      "color": "0xFF895B3A"
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product6",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product7",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product8",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product9",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product10",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                ],
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                }
              },
              {
                "widgetName": "VerticalDivider",
                "widgetSetting": {
                  "thickness": "2",
                  "color": "0x8F634201",
                },
              },
              {
                //column C
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"bottom": "4.5.h", "horizontal": "3.w"},
                      "child": {
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "height": "3.h",
                              },
                            },
                            {
                              "widgetName": "Divider",
                              "widgetSetting": {
                                "thickness": "2",
                                "height": "6.h",
                                "color": "0x8F634201",
                              },
                            },
                            {
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "spaceAround",
                                    "children": [
                                      {
                                        "widgetName": "categoryTitle",
                                        "widgetSetting": {
                                          "categoryId": "category3",
                                          "color": "0xFF895B3A"
                                        },
                                      },
                                      {
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "productId": "product11",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w",
                                          "useMediumStyle": "false",
                                        },
                                      },
                                      {
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "productId": "product12",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w",
                                          "useMediumStyle": "false",
                                        },
                                      },
                                    ],
                                  },
                                },
                              },
                            },
                            {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "color": "0xFF895B3A",
                                "height": "18.h",
                                "width": "20.w",
                                "child": {
                                  "widgetName": "Center",
                                  "widgetSetting": {
                                    "child": {
                                      "widgetName": "Text",
                                      "widgetSetting": {
                                        "data":
                                            "GET 10% OFF \nYOUR FIRST PAYMENT",
                                        "style": {
                                          "color": "0xFFFFFFFF",
                                          "fontSize": "1.5.w",
                                          "fontWeight": "w500",
                                          "fontFamily": "Nunito Sans"
                                        }
                                      },
                                    },
                                  },
                                },
                              },
                            },
                            {
                              "widgetName": "SizedBox",
                              "widgetSetting": {"height": "5.h"}
                            },
                            {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "20.h",
                                "width": "26.w",
                                "decoration": {
                                  "image":
                                      "image1", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
                                  "boxFit": "cover",
                                },
                              },
                            },
                          ],
                        },
                      },
                    },
                  },
                }
              },
            ]
          },
        },
      },
    },
  };
}
