import 'package:tv_menu/repo_data/json_template_restaurant_6.dart'
    as restaurant_6;

import 'package:tv_menu/repo_data/json_template_restaurant_7.dart'
    as restaurant_7;

import 'package:tv_menu/repo_data/json_template_restaurant_12.dart'
    as restaurant_12;

import 'package:tv_menu/repo_data/json_template_restaurant_13.dart'
    as restaurant_13;

import 'package:tv_menu/repo_data/json_temp_pho_1.dart' as pho_1;
import 'package:tv_menu/repo_data/json_temp_pho_2.dart' as pho_2;
import 'package:tv_menu/repo_data/json_temp_pho_3.dart' as pho_3;
import 'package:tv_menu/repo_data/json_temp_pho_4.dart' as pho_4;
import 'package:tv_menu/repo_data/json_temp_pho_5.dart' as pho_5;
import 'package:tv_menu/repo_data/json_template_pho_6.dart' as pho_6;
import 'package:tv_menu/repo_data/json_template_pho_7.dart' as pho_7;
import 'package:tv_menu/repo_data/json_template_pho_10.dart' as pho_10;

class NewTemplates {
  static final restaurant_6_json =
      restaurant_6.JsonDataTemplate.temprestaurant6;

  static final restaurant_7_json =
      restaurant_7.JsonDataTemplate.temprestaurant7;

  static final restaurant_12_json =
      restaurant_12.JsonDataTemplate.temprestaurant12;

  static final restaurant_13_json =
      restaurant_13.JsonDataTemplate.temprestaurant13;

  static final pho_1_json = pho_1.JsonDataTemplate.temppho1;

  static final pho_2_json = pho_2.JsonDataTemplate.temppho2;

  static final pho_3_json = pho_3.JsonDataTemplate.temppho3;

  static final pho_4_json = pho_4.JsonDataTemplate.temppho4;

  static final pho_5_json = pho_5.JsonDataTemplate.temppho5;

  static final pho_6_json = pho_6.JsonDataTemplate.temppho6;

  static final pho_7_json = pho_7.JsonDataTemplate.temppho7;

  static final pho_10_json = pho_10.JsonDataTemplate.temppho10;
}
