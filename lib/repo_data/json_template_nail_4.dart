class JsonDataTemplate {
  //[092328HG] Check Nail4: Run OK - Ratio OK
  //[092304Tuan] adjust according to feedback from Mr Hugo
  static final templateNail4 = {
    "_id": "",
    "templateSlug": "temp_nail_4",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Playfair Display',
        },
        'nameProductStyle': {
          'fontWeight': 'w500',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w500',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.3.w',
          'nameProductStyle': '1.1.w',
          'priceProductStyle': '1.1.w',
        },
        '21/9': {
          'categoryNameStyle': '1.9.w',
          'nameProductStyle': '1.7.w',
          'priceProductStyle': '1.7.w',
        },
        '16/9': {
          'categoryNameStyle': '3.2.h',
          'nameProductStyle': '2.6.h',
          'priceProductStyle': '2.6.h',
        },
        '16/10': {
          'categoryNameStyle': '3.h',
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
        },
        '4/3': {
          'categoryNameStyle': '3.2.w',
          'nameProductStyle': '2.2.w',
          'priceProductStyle': '2.2.w',
        },
        'default': {
          'categoryNameStyle': '3.h',
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '',
      },

      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '',
      },
      //Danh mục 1: pro 1, 2 ,3 ,4 , 5
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      //Danh mục 2: pro 6, 7 ,8 ,9 , 10
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 2.1',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 2.2',
        'price': '\$24.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 2.3',
        'price': '\$25.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },

      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 2.4',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 2.5',
        'price': '\$32.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },

      //Danh mục 3: pro 11,12,13,14,15

      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 3.1',
        'price': '\$32.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 3.2',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product13': {
        'key': 'product13',
        'type': 'product',
        'name': 'Product 3.3',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product14': {
        'key': 'product14',
        'type': 'product',
        'name': 'Product 3.4',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product15': {
        'key': 'product15',
        'type': 'product',
        'name': 'Product 3.5',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      //Danh mục 4: pro 16,17,18,19,20

      'product16': {
        'key': 'product16',
        'type': 'product',
        'name': 'Product 4.1',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product17': {
        'key': 'product17',
        'type': 'product',
        'name': 'Product 4.2',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product18': {
        'key': 'product13',
        'type': 'product',
        'name': 'Product 4.3',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product19': {
        'key': 'product19',
        'type': 'product',
        'name': 'Product 4.4',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product20': {
        'key': 'product19',
        'type': 'product',
        'name': 'Product 4.5',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },

      'image1': {
        'id': 'image1',
        'key': 'image1',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_1-1.jpg',
      },
      'image2': {
        'id': 'image2',
        'key': 'image2',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_2-1.jpg',
      },
      'image3': {
        'id': 'image3',
        'key': 'image3',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_3-1.jpg',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      // begin: container
      "widgetName": "Container",
      "widgetSetting": {
        "width": "100.w",
        "height": "100.h",
        "color": "0xFFF1F1EF",
        "child": {
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              {
                "widgetName": "Row",
                "widgetSetting": {
                  "children": [
                    {
                      //column A
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": '30',
                        "child": {
                          "widgetName": "Padding",
                          "widgetSetting": {
                            "padding": {
                              "horizontal": "2.w",
                              "top": "5.h",
                            },
                            "child": {
                              "widgetName": "Column",
                              "widgetSetting": {
                                'mainAxisAlignment': 'spaceAround',
                                "children": [
                                  {
                                    "key": "CTSC-A1",
                                    "widgetName": "categoryTitleSingleColumn",
                                    "widgetSetting": {
                                      'spaceBetweenCategoryProductList': '3.h',
                                      'colorCategoryTitle': "0xFF333A32",
                                      'colorProductName': '0xFF333A32',
                                      'colorDescription': '0xFF333A32',
                                      "dataList": {
                                        'category': "category1",
                                        'product': [
                                          "product1",
                                          "product2",
                                          "product3",
                                          "product4",
                                          "product5",
                                        ],
                                      },
                                      "indexCategory": "0",
                                      "useBackgroundCategoryTitle": "false",
                                      "startIndexProduct": "0",
                                      "endIndexProduct": "8",
                                      "colorPrice": "0xFF333A32",
                                    }
                                  },
                                  {
                                    "key": "CTSC-A2",
                                    "widgetName": "categoryTitleSingleColumn",
                                    "widgetSetting": {
                                      'spaceBetweenCategoryProductList': '3.h',
                                      'colorCategoryTitle': "0xFF333A32",
                                      'colorProductName': '0xFF333A32',
                                      'colorDescription': '0xFF333A32',
                                      "colorPrice": "0xFF333A32",
                                      "dataList": {
                                        'category': "category2",
                                        'product': [
                                          "product6",
                                          "product7",
                                          "product8",
                                          "product9",
                                          "product10",
                                        ],
                                      },
                                      "indexCategory": "0",
                                      "useBackgroundCategoryTitle": "false",
                                      "startIndexProduct": "0",
                                      "endIndexProduct": "8",
                                    }
                                  }
                                ],
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      "widgetName": "VerticalDivider",
                      "widgetSetting": {
                        "thickness": "1",
                        "color": "0xFFBCBCBA ",
                      },
                    },
                    {
                      //column B
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": '55',
                        "child": {
                          "widgetName": "Stack",
                          "widgetSetting": {
                            "children": [
                              {
                                "widgetName": "Row",
                                "widgetSetting": {
                                  "children": [
                                    {
                                      "widgetName": "Expanded",
                                      "widgetSetting": {
                                        "flex": "6",
                                        "child": {
                                          "widgetName": "Text",
                                          "widgetSetting": {"data": ""},
                                        }
                                      },
                                    },
                                    {
                                      "widgetName": "Expanded",
                                      "widgetSetting": {
                                        "flex": "4",
                                        "child": {
                                          "widgetName": "Container",
                                          "widgetSetting": {
                                            "height": "100.h",
                                            "color":
                                                "0xFFCDC6B6", //secondary color
                                            "child": {
                                              "widgetName": "Text",
                                              "widgetSetting": {"data": ""},
                                            },
                                          },
                                        }
                                      },
                                    },
                                  ],
                                },
                              },
                              {
                                "widgetName": "Padding",
                                "widgetSetting": {
                                  "padding": {
                                    "top": "5.h",
                                    "left": "2.w",
                                  },
                                  "child": {
                                    "widgetName": "Row",
                                    "widgetSetting": {
                                      "children": [
                                        {
                                          "widgetName": "Expanded",
                                          "widgetSetting": {
                                            "flex": "1",
                                            "child": {
                                              "widgetName": "Column",
                                              "widgetSetting": {
                                                'mainAxisAlignment':
                                                    'spaceAround',
                                                "children": [
                                                  {
                                                    "key": "CTSC-B1",
                                                    "widgetName":
                                                        "categoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      'spaceBetweenCategoryProductList':
                                                          '3.h',
                                                      'colorCategoryTitle':
                                                          "0xFF333A32",
                                                      'colorProductName':
                                                          '0xFF333A32',
                                                      'colorDescription':
                                                          '0xFF333A32',
                                                      "dataList": {
                                                        'category': "category3",
                                                        'product': [
                                                          "product11",
                                                          "product12",
                                                          "product13",
                                                          "product14",
                                                          "product15",
                                                        ],
                                                      },
                                                      "indexCategory": "0",
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "startIndexProduct": "0",
                                                      "endIndexProduct": "8",
                                                      "colorPrice":
                                                          "0xFF333A32",
                                                    }
                                                  },
                                                  {
                                                    "key": "CTSC-B2",
                                                    "widgetName":
                                                        "categoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      'spaceBetweenCategoryProductList':
                                                          '3.h',
                                                      'colorCategoryTitle':
                                                          "0xFF333A32",
                                                      'colorProductName':
                                                          '0xFF333A32',
                                                      'colorDescription':
                                                          '0xFF333A32',
                                                      "dataList": {
                                                        'category': "category4",
                                                        'product': [
                                                          "product16",
                                                          "product17",
                                                          "product18",
                                                          "product19",
                                                          "product20",
                                                        ],
                                                      },
                                                      "indexCategory": "0",
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "startIndexProduct": "0",
                                                      "endIndexProduct": "8",
                                                      "colorPrice":
                                                          "0xFF333A32",
                                                    }
                                                  }
                                                ],
                                              },
                                            },
                                          },
                                        },
                                        {
                                          "widgetName": "Expanded",
                                          "widgetSetting": {
                                            "flex": "1",
                                            "child": {
                                              "widgetName": "Column",
                                              "widgetSetting": {
                                                "mainAxisAlignment":
                                                    "spaceAround",
                                                "children": [
                                                  {
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "height": "25.h",
                                                      "width": "23.w",
                                                      "decoration": {
                                                        "image": "image1",
                                                        "boxFit": "cover",
                                                      },
                                                    },
                                                  },
                                                  {
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "height": "25.h",
                                                      "width": "23.w",
                                                      "decoration": {
                                                        "image": "image2",
                                                        "boxFit": "cover",
                                                      },
                                                    },
                                                  },
                                                  {
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "height": "25.h",
                                                      "width": "23.w",
                                                      "decoration": {
                                                        "image": "image3",
                                                        "boxFit": "cover",
                                                      },
                                                    },
                                                  },
                                                ],
                                              },
                                            },
                                          },
                                        }
                                      ],
                                    }
                                  },
                                },
                              },
                            ],
                          },
                        },
                      },
                    },
                    {
                      //column C
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": '15',
                        "child": {
                          "widgetName": "RotatedBox",
                          "widgetSetting": {
                            "quarterTurns": "3",
                            "child": {
                              "widgetName": "Center",
                              "widgetSetting": {
                                "child": {
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "data": "PRO NAIL SALON",
                                    "style": {
                                      "fontFamily": "Playfair Display",
                                      "fontSize": "5.w",
                                      "fontWeight": "bold",
                                    },
                                  },
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  ],
                },
              },
            ]
          },
        },
      },
    },
  };
}
