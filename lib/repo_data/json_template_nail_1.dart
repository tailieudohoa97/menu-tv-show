class JsonDataTemplate {
  //[092328HG] Check Nail1: Run OK - Ratio OK
  //[092304Tuan] adjust according to feedback from Mr Hugo
  static final templateNail1 = {
    "_id": "",
    "templateSlug": "temp_nail_1",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'normal',
          'fontFamily': 'Gravitas One',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Titillium Web',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Titillium Web',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Titillium Web',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'nameProductStyle': '1.w',
          'priceProductStyle': '1.w',
          'categoryNameStyle': '1.4.w',
          'discriptionProductStyle': '0.8.w',
        },
        '21/9': {
          'nameProductStyle': '1.2.w',
          'priceProductStyle': '1.2.w',
          'categoryNameStyle': '1.4.w',
          'discriptionProductStyle': '0.85.w',
        },
        '16/9': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '4.h',
          'discriptionProductStyle': '2.h',
        },
        '16/10': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '3.w',
          'discriptionProductStyle': '2.h',
        },
        '4/3': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '4.h',
          'discriptionProductStyle': '2.h',
        },
        'default': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '4.h',
          'discriptionProductStyle': '2.h',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '',
      },
      //Danh mục 1: pro 1, 2 ,3 ,4 , 5 ,6 ,7 ,8
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$24.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$25.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //column 1: image 1+ image 2
      // image 1: nail salon logo
      // image 2: decoration image
      'image1': {
        'id': 'logo',
        'key': 'logo',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/logo2-scaled.jpg',
      },
      'image2': {
        'key': 'image2',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_img_landscape-scaled.jpg',
      },
      //column 3: image 3 + image 4
      'image3': {
        'key': 'image3',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_illustrate2.png',
      },
      'image4': {
        'key': 'image4',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_illustrate1-scaled.jpg',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      // begin: container
      "widgetName": "Container",
      "widgetSetting": {
        // "padding": {"vertical": "5.w"},
        // "decoration": {
        //   "color": "white",
        //   "boxShadow": {
        //     "color": {
        //       "grey": {"opacity": 0.5}
        //     },
        //     "spreadRadius": "5",
        //     "blurRadius": "7",
        //     "offset": "0 3"
        //   }
        // }
        "child": {
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                //column A
                "key": "expanded-A",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "top": "5.h",
                            "right": "6.3.w",
                            "child": {
                              "key": "container-A1",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "90.h",
                                "width": "23.7.w",
                                // "alignment": "centerLeft",
                                "child": {
                                  "widgetName": "image",
                                  "widgetSetting": {
                                    "imageId": "image2", //key = image2
                                    "fixCover": "true"
                                  },
                                },
                              },
                            },
                          },
                        },
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "top": "0",
                            "right": "3.w",
                            "child": {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "70.h",
                                "width": "23.4.w",
                                "color": "0xEEECEFF1",
                                "child": {
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {
                                          "width": "20.h",
                                          "height": "20.h",
                                          "child": {
                                            "widgetName": "image",
                                            "widgetSetting": {
                                              "imageId": "image1",
                                              "fixCover": "false"
                                            },
                                          }
                                        }
                                      },
                                      {
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "SPA",
                                          "style": {
                                            "letterSpacing": "1.w",
                                            "fontSize": "10.h",
                                            "fontFamily": "Gravitas One",
                                            "fontWeight": "normal",
                                          }
                                        }
                                      },
                                      {
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "CARE YOUR BODY",
                                          "style": {
                                            "fontSize": "2.w",
                                            "fontWeight": "w800",
                                            "fontFamily": "Arvo"
                                          }
                                        }
                                      }
                                    ]
                                  }
                                },
                              },
                            },
                          },
                        },
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "bottom": "0",
                            "right": "3.w",
                            "child": {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "25.h",
                                "width": "23.4.w",
                                "color": "0xCCFCE4EC",
                                "child": {
                                  "widgetName": "Center",
                                  "widgetSetting": {
                                    "child": {
                                      "widgetName": "Text",
                                      "widgetSetting": {
                                        "data": "OPEN FROM 9AM TO 6PM",
                                        "style": {
                                          "fontSize": "3.h",
                                          "fontWeight": "w800",
                                          "fontFamily": "Arvo"
                                        }
                                      },
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                      ],
                    },
                  }
                },
              },
              {
                //column B
                "key": "expanded-B",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "widgetName": "Center",
                    "widgetSetting": {
                      "child": {
                        "key": "CTSC-B1",
                        "widgetName": "categoryTitleSingleColumn",
                        "widgetSetting": {
                          'colorCategoryTitle': "0xFF000000",
                          'colorProductName': '0xFF000000',
                          'colorDescription': '0xFF000000',
                          'runSpacing': '1.h',
                          'mainAxisAlignment': 'center',
                          "dataList": {
                            'category': "category1",
                            'product': [
                              "product1",
                              "product2",
                              "product3",
                              "product4",
                              "product5",
                              "product6",
                              "product7",
                              "product8",
                            ],
                          },
                          "useBackgroundCategoryTitle": "false",
                          "colorPrice": "0xFF000000",
                        },
                      },
                    },
                  },
                },
              },
              //column C
              {
                "key": "expanded-C",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "top": "4.h",
                            "left": "3.w",
                            "child": {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "44.h",
                                "width": "27.3.w",
                                "decoration": {
                                  "image": "image3",
                                  "boxFit": "cover",
                                },
                              },
                            },
                          },
                        },
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "bottom": "4.h",
                            "left": "3.w",
                            "child": {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "44.h",
                                "width": "27.3.w",
                                "decoration": {
                                  "image": "image4",
                                  "boxFit": "cover",
                                },
                              },
                            },
                          },
                        },
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "right": "6.w",
                            "child": {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "5.w",
                                "height": "100.h",
                                "color": "0xEEECEFF1",
                                "child": {
                                  "widgetName": "RotatedBox",
                                  "widgetSetting": {
                                    "quarterTurns": "3",
                                    "child": {
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "mainAxisAlignment": "spaceAround",
                                        "children": [
                                          {
                                            "widgetName": "Text",
                                            "widgetSetting": {
                                              "data": "wwww.salon.wwww",
                                              "style": {
                                                "fontSize": "1.2.w",
                                                "fontFamily": "Arvo"
                                              },
                                            },
                                          },
                                          {
                                            "widgetName": "Text",
                                            "widgetSetting": {
                                              "data": "@salon.com",
                                              "style": {
                                                "fontSize": "1.2.w",
                                                "fontFamily": "Arvo"
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                },
                              },
                            },
                          },
                        },
                      ],
                    },
                  }
                },
              },
            ],
          },
        },
      },
    },
  };
}
