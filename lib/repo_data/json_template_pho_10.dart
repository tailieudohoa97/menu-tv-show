class JsonDataTemplate {
  static final temppho10 = {
    // [112320TIN] run ok, supported ratio: 4:3, 16:10, 16:9, 21:9
    "_id": "id_tem18",
    "templateSlug": "temp_pho_10",
    "ratioSetting": {
      // not use textStyle for this template
      // "textStyle": {},
      "renderScreen": {
        "32/9": {
          "menuVerticalPadding": "4.sp",
          "menuHorizontalPadding": "62.sp",
          "categoryNameFontSize": "4.6.sp",
          "productGap": "2.2.sp",
          "categoryProductGap": "3.2.sp",
          "columnGap": "40.sp",
          "productFontSize": "3.4.sp",
          "titleFontSize": "4.6.sp",
          "brandFontSize": "8.sp",
          "extraInfoFontSize": "4.6.sp",
          "extraInfoGap": "3.2.sp",
          "categoryColumnGap": "6.sp",
          "rowGap": "4.sp",
          "footerColumnGap": "5.sp",
          "categoryImageSize1": "28.sp",
          "categoryImageSize2": "72.sp",
          "categoryImageSize3": "70.sp",
          "extraInfoBannerHeight": "10.sp",
          "brandBannerHeight": "15.sp"
        },
        "21/9": {
          "menuVerticalPadding": "4.sp",
          "menuHorizontalPadding": "62.sp",
          "categoryNameFontSize": "4.6.sp",
          "productGap": "2.2.sp",
          "categoryProductGap": "3.2.sp",
          "columnGap": "40.sp",
          "productFontSize": "3.4.sp",
          "titleFontSize": "4.6.sp",
          "brandFontSize": "8.sp",
          "extraInfoFontSize": "4.6.sp",
          "extraInfoGap": "3.2.sp",
          "categoryColumnGap": "6.sp",
          "rowGap": "4.sp",
          "footerColumnGap": "5.sp",
          "categoryImageSize1": "28.sp",
          "categoryImageSize2": "72.sp",
          "categoryImageSize3": "70.sp",
          "extraInfoBannerHeight": "10.sp",
          "brandBannerHeight": "15.sp"
        },
        "16/9": {
          "menuVerticalPadding": "8.sp",
          "menuHorizontalPadding": "32.sp",
          "categoryNameFontSize": "5.sp",
          "productGap": "2.5.sp",
          "categoryProductGap": "4.2.sp",
          "columnGap": "40.sp",
          "productFontSize": "3.8.sp",
          "titleFontSize": "6.sp",
          "brandFontSize": "12.sp",
          "extraInfoFontSize": "5.sp",
          "extraInfoGap": "4.2.sp",
          "categoryColumnGap": "7.sp",
          "rowGap": "8.sp",
          "footerColumnGap": "5.sp",
          "categoryImageSize1": "40.sp",
          "categoryImageSize2": "65.sp",
          "categoryImageSize3": "80.sp",
          "extraInfoBannerHeight": "15.sp",
          "brandBannerHeight": "25.sp"
        },
        "16/10": {
          "menuVerticalPadding": "8.sp",
          "menuHorizontalPadding": "42.sp",
          "categoryNameFontSize": "6.6.sp",
          "productGap": "2.5.sp",
          "categoryProductGap": "4.2.sp",
          "columnGap": "30.sp",
          "productFontSize": "4.8.sp",
          "titleFontSize": "8.sp",
          "brandFontSize": "14.sp",
          "extraInfoFontSize": "6.2.sp",
          "extraInfoGap": "4.2.sp",
          "categoryColumnGap": "8.sp",
          "rowGap": "8.8.sp",
          "footerColumnGap": "5.sp",
          "categoryImageSize1": "40.sp",
          "categoryImageSize2": "72.sp",
          "categoryImageSize3": "82.sp",
          "extraInfoBannerHeight": "15.sp",
          "brandBannerHeight": "25.sp"
        },
        "4/3": {
          "menuVerticalPadding": "8.sp",
          "menuHorizontalPadding": "42.sp",
          "categoryNameFontSize": "7.8.sp",
          "productGap": "3.6.sp",
          "categoryProductGap": "5.8.sp",
          "columnGap": "30.sp",
          "productFontSize": "5.8.sp",
          "titleFontSize": "10.sp",
          "brandFontSize": "16.sp",
          "extraInfoFontSize": "7.8.sp",
          "extraInfoGap": "5.8.sp",
          "categoryColumnGap": "12.sp",
          "rowGap": "10.sp",
          "footerColumnGap": "5.sp",
          "categoryImageSize1": "40.sp",
          "categoryImageSize2": "72.sp",
          "categoryImageSize3": "82.sp",
          "extraInfoBannerHeight": "15.sp",
          "brandBannerHeight": "25.sp"
        },
        "default": {
          "menuVerticalPadding": "8.sp",
          "menuHorizontalPadding": "32.sp",
          "categoryNameFontSize": "5.sp",
          "productGap": "2.5.sp",
          "categoryProductGap": "4.2.sp",
          "columnGap": "40.sp",
          "productFontSize": "3.8.sp",
          "titleFontSize": "6.sp",
          "brandFontSize": "12.sp",
          "extraInfoFontSize": "5.sp",
          "extraInfoGap": "4.2.sp",
          "categoryColumnGap": "7.sp",
          "rowGap": "8.sp",
          "footerColumnGap": "5.sp",
          "categoryImageSize1": "40.sp",
          "categoryImageSize2": "65.sp",
          "categoryImageSize3": "80.sp",
          "extraInfoBannerHeight": "15.sp",
          "brandBannerHeight": "25.sp"
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "",
      },

      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "",
      },

      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "",
      },

      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "",
      },

      "category5": {
        "key": "category5",
        "type": "category",
        "name": "CATEGORY 5",
        "image": "",
      },

      "category6": {
        "key": "category6",
        "type": "category",
        "name": "CATEGORY 6",
        "image": "",
      },

      //Danh mục 1: pro 1, 2 ,3, 4
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$40",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      //Danh mục 2: pro 5, 6, 7, 8
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$50",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$60",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$70",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$80",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      //Danh mục 3: pro 9, 10, 11, 12
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "\$11",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "\$12",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "\$13",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "\$40",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      //Danh mục 4: pro 13, 14 , 15, 16
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "\$11",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "\$12",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "\$13",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "\$40",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      //Danh mục 5: pro 17, 18, 19, 20
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "\$50",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "\$60",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "\$50",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "\$60",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      //Danh mục 6: pro 21, 22
      "product21": {
        "key": "product21",
        "type": "product",
        "name": "Product 21",
        "price": "\$70",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product22": {
        "key": "product22",
        "type": "product",
        "name": "Product 22",
        "price": "\$80",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      //Hình deco trái
      "leftDecoImage": {
        "key": "leftDecoImage",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/left-deco.webp",
      },

      //Hình deco phải
      "rightDecoImage": {
        "key": "rightDecoImage",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/right-deco.webp",
      },

      // Brand banner bg
      "bannerBg": {
        "key": "bannerBg",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/1caae864-7be3-47c1-8681-9976164d0f88.png"
      },

      "columnDivider": {
        "key": "columnDivider",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/column-divider.png"
      },

      "categoryImageBorder": {
        "key": "categoryImageBorder",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/deco-2-3.png",
      },

      //Hình category 1
      "category1Image": {
        "key": "category1Image",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/cat_1_4.webp",
      },

      //Hình category 2
      "category2Image": {
        "key": "category2Image",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/cat_2_4.webp",
      },

      //Hình category 3
      "category3Image": {
        "key": "category3Image",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/cat_3_4.webp",
      },

      //Hình category 4
      "category4Image": {
        "key": "category4Image",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/cat_3_3-1.webp",
      },

      //Hình category 5
      "category5Image": {
        "key": "category5Image",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/cat_5_4.png",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgetSetting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "color": "0xfffff4dd",
        },
        "width": "100.w",
        "child": {
          "key": "",
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              // Left deco
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  // "top": "1",
                  // "left": "1",
                  "child": {
                    "key": "",
                    // "widgetName": "SizedBoxByRatio",
                    "widgetName": "Container",
                    "widgetSetting": {
                      // "height": "maxFinite",
                      "height": "100.h",
                      // "width": "200",
                      // "color": "0x50ff0000",
                      // "heightFromFieldKey": "decoSize",
                      // "widthFromFieldKey": "decoSize",
                      "child": {
                        "key": "",
                        "widgetName": "SimpleImage",
                        "widgetSetting": {
                          "imageDataKey": "leftDecoImage",
                          "fit": "cover",
                        }
                      }
                    }
                  }
                }
              },
              // Right deco
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "top": "0",
                  "right": "0",
                  "child": {
                    "key": "",
                    // "widgetName": "SizedBoxByRatio",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "height": "100.h",
                      // "heightFromFieldKey": "decoSize",
                      // "widthFromFieldKey": "decoSize",
                      "child": {
                        "key": "",
                        "widgetName": "SimpleImage",
                        "widgetSetting": {
                          "imageDataKey": "rightDecoImage",
                          "fit": "cover",
                        }
                      }
                    }
                  }
                }
              },
              // Category image 2
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "left": "-15.sp",
                  "top": "-15.sp",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "widthFromFieldKey": "categoryImageSize2",
                      "heightFromFieldKey": "categoryImageSize2",
                      "child": {
                        "key": "",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "clipBehavior": "hardEdge",
                          "decoration": {
                            // "color": "0x50ff0000",
                            "borderRadius": "30.sp"
                          },
                          "child": {
                            "key": "",
                            "widgetName": "SimpleImage",
                            "widgetSetting": {
                              "imageDataKey": "category2Image",
                              "fit": "cover"
                            }
                          }
                        }
                      }
                    }
                  }
                }
              },
              // Category image 3
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "right": "-10.sp",
                  "top": "-20.sp",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "widthFromFieldKey": "categoryImageSize2",
                      "heightFromFieldKey": "categoryImageSize2",
                      "child": {
                        "key": "",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "clipBehavior": "hardEdge",
                          "decoration": {
                            // "color": "0x50ff0000",
                            "borderRadius": "30.sp"
                          },
                          "child": {
                            "key": "",
                            "widgetName": "SimpleImage",
                            "widgetSetting": {
                              "imageDataKey": "category3Image",
                              "fit": "cover"
                            }
                          }
                        }
                      }
                    }
                  }
                }
              },
              // Category image 4
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "left": "-18.sp",
                  "bottom": "-18.sp",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "widthFromFieldKey": "categoryImageSize2",
                      "heightFromFieldKey": "categoryImageSize2",
                      "child": {
                        "key": "",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "clipBehavior": "hardEdge",
                          "decoration": {
                            // "color": "0x50ff0000",
                            "borderRadius": "30.sp"
                          },
                          "child": {
                            "key": "",
                            "widgetName": "SimpleImage",
                            "widgetSetting": {
                              "imageDataKey": "category4Image",
                              "fit": "cover"
                            }
                          }
                        }
                      }
                    }
                  }
                }
              },
              // Category image 5
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "right": "20.sp",
                  "bottom": "-6.h",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "widthFromFieldKey": "categoryImageSize3",
                      "heightFromFieldKey": "categoryImageSize3",
                      "child": {
                        "key": "",
                        "widgetName": "FramedImage",
                        "widgetSetting": {
                          "imageDataKey": "category5Image",
                          "imageFrameDataKey": "categoryImageBorder",
                          "borderWidth": "6.sp",
                          "borderRadiusOfImage": "30.sp"
                        }
                      }
                    }
                  }
                }
              },
              {
                "key": "",
                "widgetName": "PaddingByRatio",
                "widgetSetting": {
                  "horizontalPaddingFieldKey": "menuHorizontalPadding",
                  "verticalPaddingFieldKey": "menuVerticalPadding",
                  "child": {
                    "key": "",
                    "widgetName": "Container",
                    "widgetSetting": {
                      // "color": "0x5000ff00",
                      "width": "maxFinite",
                      "child": {
                        "key": "",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            // Title
                            {
                              "key": "",
                              "widgetName": "Text",
                              "widgetSetting": {
                                "data": "Pho Menu",
                                "style": {
                                  "fontFamily": "Tenor Sans",
                                  "color": "0xffe08c48",
                                  "fontWeight": "w600",
                                },
                                "fontSizeFromFieldKey": "titleFontSize",
                              }
                            },

                            // Spacing
                            {
                              "key": "",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "titleBrandGap"
                              }
                            },

                            // Brand
                            {
                              "key": "",
                              "widgetName": "SizedBoxByRatio",
                              // "widgetName": "Container",
                              "widgetSetting": {
                                // "height": "25.sp",
                                // "width": "100.sp",
                                // "width": "maxFinite",
                                "heightFromFieldKey": "brandBannerHeight",
                                // "color": "0x50ff0000",
                                "child": {
                                  "key": "",
                                  "widgetName": "Stack",
                                  "widgetSetting": {
                                    "alignment": "center",
                                    "clipBehavior": "none",
                                    "children": [
                                      // brand banner background
                                      {
                                        "key": "",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "height": "maxFinite",
                                          // "width": "maxFinite",
                                          "child": {
                                            "key": "",
                                            "widgetName": "SimpleImage",
                                            "widgetSetting": {
                                              "imageDataKey": "bannerBg",
                                              "fit": "contain",
                                            }
                                          },
                                        }
                                      },
                                      // Category image 1
                                      {
                                        "key": "",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "top": "-10.sp",
                                          "right": "-20.sp",
                                          "child": {
                                            "key": "",
                                            "widgetName": "SizedBoxByRatio",
                                            "widgetSetting": {
                                              "heightFromFieldKey":
                                                  "categoryImageSize1",
                                              "widthFromFieldKey":
                                                  "categoryImageSize1",
                                              "child": {
                                                "key": "",
                                                "widgetName": "Container",
                                                "widgetSetting": {
                                                  "decoration": {
                                                    "borderRadius": "30.w",
                                                    // "color": "0xffc33123"
                                                  },
                                                  "child": {
                                                    "key": "",
                                                    "widgetName": "SimpleImage",
                                                    "widgetSetting": {
                                                      "imageDataKey":
                                                          "category1Image",
                                                      "fit": "contain",
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      },
                                      // brand text
                                      {
                                        "key": "",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "The Brand",
                                          "style": {
                                            "fontFamily": "Tenor Sans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w600",
                                          },
                                          "fontSizeFromFieldKey":
                                              "brandFontSize",
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            },
                            // Spacing
                            {
                              "key": "",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {"heightFromFieldKey": "rowGap"}
                            },
                            // Midddle menu
                            {
                              "key": "",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "key": "",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "children": [
                                      // Menu column 1
                                      {
                                        "key": "",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "",
                                            "widgetName": "Column",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "end",
                                              "children": [
                                                // Category Title 1
                                                {
                                                  "key": "",
                                                  "widgetName": "Text",
                                                  "widgetSetting": {
                                                    "dataFromSetting":
                                                        "category1.name",
                                                    "style": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "color": "0xffe08c48",
                                                      "fontWeight": "w600",
                                                    },
                                                    "fontSizeFromFieldKey":
                                                        "categoryNameFontSize",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "categoryProductGap",
                                                  }
                                                },
                                                // Product 1
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product1",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 2
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product2",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 3
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product3",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 4
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product4",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "categoryColumnGap",
                                                  }
                                                },
                                                // Category Title 2
                                                {
                                                  "key": "",
                                                  "widgetName": "Text",
                                                  "widgetSetting": {
                                                    "dataFromSetting":
                                                        "category2.name",
                                                    "style": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "color": "0xffe08c48",
                                                      "fontWeight": "w600",
                                                    },
                                                    "fontSizeFromFieldKey":
                                                        "categoryNameFontSize",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "categoryProductGap",
                                                  }
                                                },
                                                // Product 5
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product5",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 6
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product6",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 7
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product7",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 8
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product8",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                              ],
                                            }
                                          },
                                        }
                                      },
                                      // Divider
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        // "widgetName": "Container",
                                        "widgetSetting": {
                                          "widthFromFieldKey": "columnGap",
                                          // "color": "0x50ff0000",
                                          "height": "maxFinite",
                                          "child": {
                                            "key": "",
                                            "widgetName": "SimpleImage",
                                            "widgetSetting": {
                                              "imageDataKey": "columnDivider",
                                              "fit": "fitHeight",
                                            }
                                          }
                                        }
                                      },
                                      // Menu column 2
                                      {
                                        "key": "",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "",
                                            "widgetName": "Column",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "end",
                                              "children": [
                                                // Category Title 3
                                                {
                                                  "key": "",
                                                  "widgetName": "Text",
                                                  "widgetSetting": {
                                                    "dataFromSetting":
                                                        "category3.name",
                                                    "style": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "color": "0xffe08c48",
                                                      "fontWeight": "w600",
                                                    },
                                                    "fontSizeFromFieldKey":
                                                        "categoryNameFontSize",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "categoryProductGap",
                                                  }
                                                },
                                                // Product 9
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product9",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 10
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product10",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 11
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product11",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 12
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product12",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "categoryColumnGap",
                                                  }
                                                },
                                                // Category Title 4
                                                {
                                                  "key": "",
                                                  "widgetName": "Text",
                                                  "widgetSetting": {
                                                    "dataFromSetting":
                                                        "category4.name",
                                                    "style": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "color": "0xffe08c48",
                                                      "fontWeight": "w600",
                                                    },
                                                    "fontSizeFromFieldKey":
                                                        "categoryNameFontSize",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "categoryProductGap",
                                                  }
                                                },
                                                // Product 13
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product13",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 14
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product14",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 15
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product15",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 16
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product16",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                              ],
                                            }
                                          },
                                        }
                                      },
                                      // Divider
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "widthFromFieldKey": "columnGap",
                                          "height": "maxFinite",
                                          "child": {
                                            "key": "",
                                            "widgetName": "SimpleImage",
                                            "widgetSetting": {
                                              "imageDataKey": "columnDivider",
                                              "fit": "fitHeight",
                                            }
                                          }
                                        }
                                      },
                                      // Menu column 3
                                      {
                                        "key": "",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "",
                                            "widgetName": "Column",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "end",
                                              "children": [
                                                // Category Title 5
                                                {
                                                  "key": "",
                                                  "widgetName": "Text",
                                                  "widgetSetting": {
                                                    "dataFromSetting":
                                                        "category5.name",
                                                    "style": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "color": "0xffe08c48",
                                                      "fontWeight": "w600",
                                                    },
                                                    "fontSizeFromFieldKey":
                                                        "categoryNameFontSize",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "categoryProductGap",
                                                  }
                                                },
                                                // Product 17
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product17",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 18
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product18",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 19
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product19",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productGap",
                                                  }
                                                },
                                                // Product 20
                                                {
                                                  "key": "",
                                                  "widgetName": "ProductRow1",
                                                  "widgetSetting": {
                                                    "product": "product20",
                                                    "border": {
                                                      "all": {
                                                        "width": "0",
                                                        "color": "0x00000000"
                                                      }
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "Tenor Sans",
                                                      "fontWeight": "w600",
                                                      "color": "0xffb5704a"
                                                    },
                                                    "prefixProductPrice": "",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productFontSize",
                                                    "productNameLAlignment":
                                                        "end",
                                                    "namePriceGap": "6.sp",
                                                  }
                                                },
                                              ],
                                            }
                                          },
                                        }
                                      },
                                    ]
                                  }
                                },
                              }
                            },
                            // Spacing
                            {
                              "key": "",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {"heightFromFieldKey": "rowGap"}
                            },
                            // Order and footer menu
                            {
                              "key": "",
                              "widgetName": "Row",
                              "widgetSetting": {
                                "children": [
                                  {
                                    "key": "",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "key": "",
                                        "widgetName": "Column",
                                        "widgetSetting": {
                                          "children": [
                                            // Reserve /Order
                                            {
                                              "key": "",
                                              "widgetName": "Text",
                                              "widgetSetting": {
                                                "data": "Reserve /Order",
                                                "style": {
                                                  "fontFamily": "Tenor Sans",
                                                  "color": "0xffe08c48",
                                                  "fontWeight": "w600",
                                                },
                                                "fontSizeFromFieldKey":
                                                    "extraInfoFontSize",
                                              }
                                            },
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "extraInfoGap",
                                              }
                                            },
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              // "widgetName": "Container",
                                              "widgetSetting": {
                                                // "height": "15.sp",
                                                "heightFromFieldKey":
                                                    "extraInfoBannerHeight",
                                                // "width": "30.sp",
                                                // "color": "0x50ff0000",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Stack",
                                                  "widgetSetting": {
                                                    "alignment": "center",
                                                    "clipBehavior": "none",
                                                    "children": [
                                                      // Banner background
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "Container",
                                                        "widgetSetting": {
                                                          // "height": "maxFinite",
                                                          // "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "SimpleImage",
                                                            "widgetSetting": {
                                                              "imageDataKey":
                                                                  "bannerBg",
                                                              "fit": "contain",
                                                            }
                                                          },
                                                        }
                                                      },
                                                      // Phone
                                                      {
                                                        "key": "",
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data":
                                                              "123-456-7890",
                                                          "style": {
                                                            "fontFamily":
                                                                "Tenor Sans",
                                                            "color":
                                                                "0xffffffff",
                                                            "fontWeight":
                                                                "w600",
                                                          },
                                                          "fontSizeFromFieldKey":
                                                              "extraInfoFontSize",
                                                        }
                                                      },
                                                    ]
                                                  }
                                                }
                                              }
                                            }
                                          ]
                                        }
                                      }
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "widthFromFieldKey": "footerColumnGap",
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "key": "",
                                        "widgetName": "Column",
                                        "widgetSetting": {
                                          "crossAxisAlignment": "end",
                                          "children": [
                                            // Category Title 6
                                            {
                                              "key": "",
                                              "widgetName": "Text",
                                              "widgetSetting": {
                                                "dataFromSetting":
                                                    "category6.name",
                                                "style": {
                                                  "fontFamily": "Tenor Sans",
                                                  "color": "0xffe08c48",
                                                  "fontWeight": "w600",
                                                },
                                                "fontSizeFromFieldKey":
                                                    "categoryNameFontSize",
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "categoryProductGap",
                                              }
                                            },
                                            // Product 21
                                            {
                                              "key": "",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product21",
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0x00000000"
                                                  }
                                                },
                                                "productPriceTextStyle": {
                                                  "fontFamily": "Tenor Sans",
                                                  "fontWeight": "w600",
                                                  "color": "0xffb5704a"
                                                },
                                                "productNameTextStyle": {
                                                  "fontFamily": "Tenor Sans",
                                                  "fontWeight": "w600",
                                                  "color": "0xffb5704a"
                                                },
                                                "prefixProductPrice": "",
                                                "productNameFontSizeFromFieldKey":
                                                    "productFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productFontSize",
                                                "productNameLAlignment": "end",
                                                "namePriceGap": "6.sp",
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "productGap",
                                              }
                                            },
                                            // Product 22
                                            {
                                              "key": "",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product22",
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0x00000000"
                                                  }
                                                },
                                                "productPriceTextStyle": {
                                                  "fontFamily": "Tenor Sans",
                                                  "fontWeight": "w600",
                                                  "color": "0xffb5704a"
                                                },
                                                "productNameTextStyle": {
                                                  "fontFamily": "Tenor Sans",
                                                  "fontWeight": "w600",
                                                  "color": "0xffb5704a"
                                                },
                                                "prefixProductPrice": "",
                                                "productNameFontSizeFromFieldKey":
                                                    "productFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productFontSize",
                                                "productNameLAlignment": "end",
                                                "namePriceGap": "6.sp",
                                              }
                                            },
                                          ],
                                        }
                                      },
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "widthFromFieldKey": "footerColumnGap",
                                    }
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "": "",
                                    }
                                  }
                                ]
                              }
                            },
                          ]
                        }
                      }
                    }
                  }
                }
              }
            ],
          }
        }
      }
    }
  };
}
