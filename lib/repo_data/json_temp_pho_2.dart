class JsonDataTemplate {
  //[092328HG] Check temppho2: Run OK - Ratio OK
  static final temppho2 = {
    "_id": "12",
    "templateSlug": "temp_pho_2",
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "3.sp",
          "priceFontSize": "3.5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "100.sp",
          "imageWidth": "100.sp",
        },
        "21/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "7.sp",
          "productFontSize": "3.sp",
          "priceFontSize": "3.5.sp",
          "sloganFontSize": "5.sp",
          "categoryFontSize": "5.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "85.sp",
          "imageWidth": "85.sp",
        },
        "16/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "3.5.sp",
          "priceFontSize": "4.2.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "3.5.sp",
          "imageHeight": "120.sp",
          "imageWidth": "120.sp",
        },
        "16/10": {
          "categoryColumnVerticalPadding": "5.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "4.5.sp",
          "priceFontSize": "5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "4.2.sp",
          "imageHeight": "125.sp",
          "imageWidth": "125.sp",
        },
        "4/3": {
          "categoryColumnVerticalPadding": "12.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "5.sp",
          "priceFontSize": "5.5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "4.5.sp",
          "imageHeight": "130.sp",
          "imageWidth": "130.sp",
        },
        "default": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "7.sp",
          "productFontSize": "2.5.sp",
          "priceFontSize": "3.sp",
          "sloganFontSize": "5.sp",
          "categoryFontSize": "5.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "100.sp",
          "imageWidth": "100.sp",
        },
      },
    },
    "isDeleted": false,
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "",
      },

      //Danh mục 1: pro 1, 2, 3, 4, 5
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      //Danh mục 2: pro 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      //Danh mục 3: pro 16, 17, 18, 19, 20
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "backgroundImage": {
        "key": "backgroundImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/background-1.jpg"
      },
      "leftImage": {
        "key": "leftImage",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_1.png"
      },
      "rightImage": {
        "key": "rightImage",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_4.png"
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "decoration": {
          "image": "backgroundImage",
          "fit": "cover",
        },
        "child": {
          "key": "Row-main",
          "widgetName": "Row",
          "widgetSetting": {
            "mainAxisAlignment": "spaceBetween",
            "crossAxisAlignment": "start",
            "children": [
              // left column
              {
                "key": "Expanded-left-col",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Stack-leftmenu",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        // left top image
                        {
                          "key": "Positioned-image-bottomleft",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "left": "-30.sp",
                            "bottom": "-15.sp",
                            "child": {
                              "key": "SizedBoxByRatio-image-bottomleft",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "widthFromFieldKey": "imageWidth",
                                "heightFromFieldKey": "imageHeight",
                                "child": {
                                  "key": "SimpleImage-image-bottomleft",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "imageDataKey": "leftImage",
                                    "fit": "contain"
                                  }
                                }
                              }
                            }
                          }
                        },
                        // left menu
                        {
                          "key": "PaddingByRatio-leftmenu",
                          "widgetName": "PaddingByRatio",
                          "widgetSetting": {
                            "verticalPaddingFieldKey":
                                "categoryColumnVerticalPadding",
                            "horizontalPaddingFieldKey":
                                "categoryColumnHorizontalPadding",
                            "child": {
                              "key": "CategoryColumn1-leftmenu",
                              "widgetName": "CategoryColumn1",
                              "widgetSetting": {
                                "category": "category1",
                                "categoryFontSizeFromFieldKey":
                                    "categoryFontSize",
                                "categoryTextStyle": {
                                  "fontFamily": "Roboto Slab",
                                  "color": "0xffda2f15",
                                  "fontWeight": "w700",
                                },
                                "crossAxisAlignment": "center",
                                "productRows": [
                                  // product 1
                                  {
                                    "key": "Padding-p-1",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "1",
                                          "product": "product1",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 2
                                  {
                                    "key": "Padding-p-2",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p2",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "2",
                                          "product": "product2",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 3
                                  {
                                    "key": "Padding-p-3",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p3",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "3",
                                          "product": "product3",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 4
                                  {
                                    "key": "Padding-p-4",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p4",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "4",
                                          "product": "product4",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 5
                                  {
                                    "key": "Padding-p-5",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p5",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "5",
                                          "product": "product5",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                ]
                              }
                            }
                          }
                        },
                      ],
                    },
                  }
                }
              },
              // center column
              {
                "key": "Expanded-center-col",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "PaddingByRatio-leftmenu",
                    "widgetName": "PaddingByRatio",
                    "widgetSetting": {
                      "verticalPaddingFieldKey":
                          "categoryColumnVerticalPadding",
                      "horizontalPaddingFieldKey":
                          "categoryColumnHorizontalPadding",
                      "child": {
                        // center menu
                        "key": "MenuColumn1-menu",
                        "widgetName": "MenuColumn1",
                        "widgetSetting": {
                          "brand": "PHO BRAND",
                          "slogan": "Pho, noodle and more",
                          "brandFontSizeFromFieldKey": "brandFontSize",
                          "sloganFontSizeFromFieldKey": "sloganFontSize",
                          "brandTextStyle": {
                            "fontFamily": "Pacifico",
                            "color": "0xffe1b971",
                          },
                          "sloganTextStyle": {
                            "fontFamily": "Roboto Slab",
                            "color": "0xffda2f15",
                            "fontWeight": "w700",
                          },
                          "menuItems": [
                            // product 6
                            {
                              "key": "Padding-p6",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p6",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "6",
                                    "product": "product6",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 7
                            {
                              "key": "Padding-p7",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p7",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "7",
                                    "product": "product7",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 8
                            {
                              "key": "Padding-p8",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p8",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "8",
                                    "product": "product8",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 9
                            {
                              "key": "Padding-p9",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p9",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "9",
                                    "product": "product9",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 10
                            {
                              "key": "Padding-p10",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p10",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "10",
                                    "product": "product10",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 11
                            {
                              "key": "Padding-p11",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p11",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "11",
                                    "product": "product11",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 12
                            {
                              "key": "Padding-p12",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p12",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "12",
                                    "product": "product12",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 13
                            {
                              "key": "Padding-p13",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p13",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "13",
                                    "product": "product13",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 14
                            {
                              "key": "Padding-p14",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p14",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "14",
                                    "product": "product14",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 15
                            {
                              "key": "Padding-p15",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p15",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "15",
                                    "product": "product15",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  },
                }
              },
              // right column
              {
                "key": "Expanded-center-col",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Stack-rightmenu",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        // right top image
                        {
                          "key": "Positioned-image-topright",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "right": "-30.sp",
                            "top": "-15.sp",
                            "child": {
                              "key": "SizedBoxByRatio-image-topright",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "widthFromFieldKey": "imageWidth",
                                "heightFromFieldKey": "imageHeight",
                                "child": {
                                  "key": "SimpleImage-image-topright",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "imageDataKey": "rightImage",
                                    "fit": "contain"
                                  }
                                }
                              }
                            }
                          }
                        },
                        // right menu
                        {
                          "key": "PaddingByRatio-rightmenu",
                          "widgetName": "PaddingByRatio",
                          "widgetSetting": {
                            "verticalPaddingFieldKey":
                                "categoryColumnVerticalPadding",
                            "horizontalPaddingFieldKey":
                                "categoryColumnHorizontalPadding",
                            "child": {
                              "key": "CategoryColumn1-rightmenu",
                              "widgetName": "CategoryColumn1",
                              "widgetSetting": {
                                "category": "category2",
                                "categoryFontSizeFromFieldKey":
                                    "categoryFontSize",
                                "categoryTextStyle": {
                                  "fontFamily": "Roboto Slab",
                                  "color": "0xffda2f15",
                                  "fontWeight": "w700",
                                },
                                "crossAxisAlignment": "center",
                                "mainAxisAlignment": "end",
                                "productRows": [
                                  // product 16
                                  {
                                    "key": "Padding-p16",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p16",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "16",
                                          "product": "product16",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 17
                                  {
                                    "key": "Padding-p17",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p17",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "17",
                                          "product": "product17",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 18
                                  {
                                    "key": "Padding-p18",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p18",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "18",
                                          "product": "product18",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 19
                                  {
                                    "key": "Padding-p19",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p19",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "19",
                                          "product": "product19",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 20
                                  {
                                    "key": "Padding-p20",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p20",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "20",
                                          "product": "product20",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                ]
                              }
                            }
                          }
                        }
                      ],
                    },
                  }
                }
              },
            ]
          },
        },
      }
    }
  };
}
