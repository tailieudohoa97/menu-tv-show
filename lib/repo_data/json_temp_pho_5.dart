class JsonDataTemplate {
  //[092328HG] Check temppho5: Run OK - Ratio OK
  static final temppho5 = {
    "_id": "",
    "templateSlug": "temp_pho_5",
    "isDeleted": false,
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryNameFontSize": "5.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp",
        },
        "21/9": {
          "categoryNameFontSize": "4.sp",
          "productNameFontSize": "2.5.sp",
          "productPriceFontSize": "2.5.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "13.h",
          "columnGap": "2.sp",
        },
        "16/9": {
          "categoryNameFontSize": "6.sp",
          "productNameFontSize": "3.5.sp",
          "productPriceFontSize": "3.5.sp",
          "productDesFontSize": "2.5.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "35.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp",
        },
        "16/10": {
          "categoryNameFontSize": "8.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "2.8.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "35.sp",
          "productRowHeight": "12.5.h",
          "columnGap": "5.sp",
        },
        "4/3": {
          "categoryNameFontSize": "8.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "3.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "40.sp",
          "productRowHeight": "12.h",
          "columnGap": "6.sp",
        },
        "default": {
          "categoryNameFontSize": "5.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp",
        },
      },
    },

    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1-2.jpg",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2-2.jpg",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3-2.jpg",
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_4-1.jpg",
      },

      //Danh mục 1: pro 1, 2, 3, 4, 5
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_1.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 2: pro 6, 7, 8, 9, 10
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_2.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_3.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_4.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_1.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 3: pro 11, 12, 13, 14, 15
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      // Danh mục 4: 16, 17, 18, 19, 20
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": "",
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": "",
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": "",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "padding": {"all": "5.sp"},
        "decoration": {"color": "0xfffdf6e9"},
        "child": {
          "key": "Center-menu",
          "widgetName": "Center",
          "widgetSetting": {
            "child": {
              "key": "Row-menu",
              "widgetName": "Row",
              "widgetSetting": {
                "crossAxisAlignment": "start",
                "mainAxisAlignment": "center",
                "children": [
                  // cat 1
                  {
                    "key": "Container-cat1",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "key": "CategoryColumn2-cat1",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category1",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828",
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp",
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            // pro 1
                            {
                              "key": "SizedBoxByRatio-p1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p1",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p1",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product1",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 2
                            {
                              "key": "SizedBoxByRatio-p2",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p2",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p2",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product2",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 3
                            {
                              "key": "SizedBoxByRatio-p3",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p3",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product3",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 4
                            {
                              "key": "SizedBoxByRatio-p4",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p4",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p4",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product4",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 5
                            {
                              "key": "SizedBoxByRatio-p5",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p5",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p5",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product5",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  },
                  // cat 2
                  {
                    "key": "Container-cat2",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "key": "CategoryColumn2-cat2",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category2",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828",
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp",
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            // pro 6
                            {
                              "key": "SizedBoxByRatio-p6",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p6",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p6",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product6",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 7
                            {
                              "key": "SizedBoxByRatio-p7",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "child": {
                                  "key": "Padding-p7",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p7",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product7",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 8
                            {
                              "key": "SizedBoxByRatio-p8",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p8",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p8",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product8",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 9
                            {
                              "key": "SizedBoxByRatio-p9",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p9",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p9",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product9",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 10
                            {
                              "key": "SizedBoxByRatio-p10",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p10",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p10",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product10",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  },
                  // cat 3
                  {
                    "key": "Container-cat3",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "key": "CategoryColumn2-cat3",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category3",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828",
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp",
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            // pro 11
                            {
                              "key": "SizedBoxByRatio-p11",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p11",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p11",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product11",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 12
                            {
                              "key": "SizedBoxByRatio-p12",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p12",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p12",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product12",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 13
                            {
                              "key": "SizedBoxByRatio-p13",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p13",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p13",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product13",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            }, // pro 14
                            {
                              "key": "SizedBoxByRatio-p14",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p14",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p14",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product14",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            }, // pro 15
                            {
                              "key": "SizedBoxByRatio-p15",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p15",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p15",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product15",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  },
                  // cat 4
                  {
                    "key": "Container-cat4",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "key": "CategoryColumn2-cat4",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category4",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828",
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp",
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            // pro 16
                            {
                              "key": "SizedBoxByRatio-p16",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p16",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p16",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product16",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 17
                            {
                              "key": "SizedBoxByRatio-p17",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p17",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p17",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product17",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            }, // pro 18
                            {
                              "key": "SizedBoxByRatio-p18",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p18",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p18",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product18",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            }, // pro 19
                            {
                              "key": "SizedBoxByRatio-p19",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p19",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p19",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product19",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            }, // pro 20
                            {
                              "key": "SizedBoxByRatio-p20",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p20",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p20",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product20",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      },
    }
  };
}
