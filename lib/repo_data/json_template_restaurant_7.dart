class JsonDataTemplate {
  static final temprestaurant7 = {
    // [112315TIN] double check: oke
    // [112310TIN] run ok, supported ratio: 4:3, 16:10, 16:9, 21:9
    "_id": "id_tem17",
    "templateSlug": "temp_restaurant_7",
    "ratioSetting": {
      // not use textStyle for this template
      // "textStyle": {},
      "renderScreen": {
        "32/9": {
          "menuVerticalPadding": "10.sp",
          "menuHorizontalPadding": "18.sp",
          "menuTitleFontSize": "12.sp",
          "productPriceFontSize": "4.sp",
          "productNameFontSize": "4.sp",
          "productSubNameFontSize": "3.2.sp",
          "productDesFontSize": "2.5.sp",
          "productRowHeight": "20.sp",
          "productRowWidth": "",
          "productDividerWidth": "70.sp",
          "columnGap": "14.sp",
          "productGap": "2.sp",
          "discountTitleFontSize": "4.sp",
          "discountPercentFontSize": "12.sp",
          "discountDateFontSize": "2.5.sp",
          "discountBlockHeight": "35.h",
          "deco1Size": "40.sp",
          "deco2Size": "10.sp",
          "categoryImage1Size": "30.sp",
          "categoryImage2Size": "35.sp",
          "categoryImage3Size": "50.sp",
          "categoryImageBlock1Height": "35.sp",
          "categoryImageBlock1Width": "61.sp",
        },
        "21/9": {
          "menuVerticalPadding": "10.sp",
          "menuHorizontalPadding": "18.sp",
          "menuTitleFontSize": "12.sp",
          "productPriceFontSize": "4.sp",
          "productNameFontSize": "4.sp",
          "productSubNameFontSize": "3.2.sp",
          "productDesFontSize": "2.5.sp",
          "productRowHeight": "20.sp",
          "productRowWidth": "",
          "productDividerWidth": "70.sp",
          "columnGap": "14.sp",
          "productGap": "2.sp",
          "discountTitleFontSize": "4.sp",
          "discountPercentFontSize": "12.sp",
          "discountDateFontSize": "2.5.sp",
          "discountBlockHeight": "35.h",
          "deco1Size": "40.sp",
          "deco2Size": "10.sp",
          "categoryImage1Size": "30.sp",
          "categoryImage2Size": "35.sp",
          "categoryImage3Size": "50.sp",
          "categoryImageBlock1Height": "35.sp",
          "categoryImageBlock1Width": "61.sp",
        },
        "16/9": {
          "menuVerticalPadding": "15.sp",
          "menuHorizontalPadding": "24.sp",
          "menuTitleFontSize": "15.sp",
          "productPriceFontSize": "4.6.sp",
          "productNameFontSize": "4.6.sp",
          "productSubNameFontSize": "3.8.sp",
          "productDesFontSize": "2.8.sp",
          "productRowHeight": "20.sp",
          "productRowWidth": "",
          "productDividerWidth": "70.sp",
          "columnGap": "16.sp",
          "productGap": "2.sp",
          "discountTitleFontSize": "5.sp",
          "discountPercentFontSize": "15.sp",
          "discountDateFontSize": "2.8.sp",
          "discountBlockHeight": "44.h",
          "deco1Size": "80.sp",
          "deco2Size": "20.sp",
          "categoryImage1Size": "55.sp",
          "categoryImage2Size": "55.sp",
          "categoryImage3Size": "80.sp",
          "categoryImageBlock1Height": "59.sp",
          "categoryImageBlock1Width": "102.sp",
        },
        "16/10": {
          "menuVerticalPadding": "16.sp",
          "menuHorizontalPadding": "24.sp",
          "menuTitleFontSize": "18.sp",
          "productPriceFontSize": "5.4.sp",
          "productNameFontSize": "5.4.sp",
          "productSubNameFontSize": "4.4.sp",
          "productDesFontSize": "3.4.sp",
          "productRowHeight": "24.sp",
          "productRowWidth": "",
          "productDividerWidth": "70.sp",
          "columnGap": "16.sp",
          "productGap": "2.sp",
          "discountTitleFontSize": "5.sp",
          "discountPercentFontSize": "15.sp",
          "discountDateFontSize": "2.8.sp",
          "discountBlockHeight": "44.h",
          "deco1Size": "80.sp",
          "deco2Size": "20.sp",
          "categoryImage1Size": "55.sp",
          "categoryImage2Size": "55.sp",
          "categoryImage3Size": "80.sp",
          "categoryImageBlock1Height": "60.sp",
          "categoryImageBlock1Width": "102.sp",
        },
        "4/3": {
          "menuVerticalPadding": "24.sp",
          "menuHorizontalPadding": "24.sp",
          "menuTitleFontSize": "20.sp",
          "productPriceFontSize": "6.2.sp",
          "productNameFontSize": "6.2.sp",
          "productSubNameFontSize": "5.2.sp",
          "productDesFontSize": "4.sp",
          "productRowHeight": "30.sp",
          "productRowWidth": "",
          "productDividerWidth": "70.sp",
          "columnGap": "16.sp",
          "productGap": "2.sp",
          "discountTitleFontSize": "5.sp",
          "discountPercentFontSize": "15.sp",
          "discountDateFontSize": "2.8.sp",
          "discountBlockHeight": "42.h",
          "deco1Size": "80.sp",
          "deco2Size": "20.sp",
          "categoryImage1Size": "65.sp",
          "categoryImage2Size": "60.sp",
          "categoryImage3Size": "80.sp",
          "categoryImageBlock1Height": "70.sp",
          "categoryImageBlock1Width": "118.sp",
        },
        "default": {
          "menuVerticalPadding": "15.sp",
          "menuHorizontalPadding": "24.sp",
          "menuTitleFontSize": "15.sp",
          "productPriceFontSize": "4.6.sp",
          "productNameFontSize": "4.6.sp",
          "productSubNameFontSize": "3.8.sp",
          "productDesFontSize": "2.8.sp",
          "productRowHeight": "20.sp",
          "productRowWidth": "",
          "productDividerWidth": "70.sp",
          "columnGap": "16.sp",
          "productGap": "2.sp",
          "discountTitleFontSize": "5.sp",
          "discountPercentFontSize": "15.sp",
          "discountDateFontSize": "2.8.sp",
          "deco1Size": "80.sp",
          "deco2Size": "20.sp",
          "categoryImage1Size": "45.sp",
          "categoryImage2Size": "50.sp",
          "categoryImage3Size": "80.sp",
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      //Danh mục 1: pro 1, 2 ,3
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "subName": "Lorem, ipsum dolor.",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_1.png",
        "description":
            "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "subName": "Lorem, ipsum dolor.",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_2.png",
        "description":
            "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "subName": "Lorem, ipsum dolor.",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_3.png",
        "description":
            "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
      },

      //Danh mục 2: pro 4, 5, 6
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$40.0",
        "salePrice": "",
        "subName": "Lorem, ipsum dolor.",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_4.png",
        "description":
            "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
      },

      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$50.0",
        "salePrice": "",
        "subName": "Lorem, ipsum dolor.",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_5.webp",
        "description":
            "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
      },

      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$60.0",
        "salePrice": "",
        "subName": "Lorem, ipsum dolor.",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_6.webp",
        "description":
            "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
      },

      //Danh mục 3: pro 7, 8, 0
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$70.0",
        "salePrice": "",
        "subName": "Lorem, ipsum dolor.",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_7.webp",
        "description":
            "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$80.0",
        "salePrice": "",
        "subName": "Lorem, ipsum dolor.",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_8.webp",
        "description":
            "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
      },

      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "\$90.0",
        "salePrice": "",
        "subName": "Lorem, ipsum dolor.",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_9.webp",
        "description":
            "Lorem, ipsum dolor sit amet consectetur adipisicing elit.",
      },

      //Hình background
      "imageBackground": {
        "key": "imageBackground",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/bg.webp",
      },

      // Hình deco 1
      "decoImage1": {
        "key": "decoImage1",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/928f63d4-22b0-4b0b-a094-c5b95396a26f.png",
      },

      // Hình deco 2
      "decoImage2": {
        "key": "decoImage2",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/d8f0e29d-6a05-4e56-a06d-20b3ea0b2894.png",
      },

      // image background title
      "imageBackgroundTitle": {
        "key": "imageBackgroundTitle",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/title_bg.webp",
      },

      // category image 1
      "categoryImage1": {
        "key": "categoryImage1",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/category_1-1.webp",
      },

      // category image 2
      "categoryImage2": {
        "key": "categoryImage2",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/category_2-1.webp",
      },

      // category image 3
      "categoryImage3": {
        "key": "categoryImage3",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/category_3-2.webp",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgetSetting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image":
              "imageBackground", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
          "boxFit": "fill",
        },
        "width": "100.w",
        "child": {
          "key": "",
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              {
                // top center large leaf
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "left": "55.w",
                  "top": "-1.h",
                  "child": {
                    "key": "",
                    "widgetName": "TransformRotate",
                    "widgetSetting": {
                      "angle": "2",
                      "child": {
                        "key": "",
                        "widgetName": "SizedBoxByRatio",
                        "widgetSetting": {
                          // "height": "80.sp",
                          // "width": "80.sp",
                          "heightFromFieldKey": "deco1Size",
                          "widthFromFieldKey": "deco1Size",
                          "child": {
                            "key": "",
                            "widgetName": "SimpleImage",
                            "widgetSetting": {
                              "imageDataKey": "decoImage1",
                            },
                          }
                        }
                      }
                    }
                  }
                }
              },
              {
                // middle left leaf
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "left": "2.sp",
                  "top": "50.h",
                  "child": {
                    "key": "",
                    "widgetName": "TransformRotate",
                    "widgetSetting": {
                      "child": {
                        "key": "",
                        "widgetName": "SizedBoxByRatio",
                        "widgetSetting": {
                          "heightFromFieldKey": "deco2Size",
                          "widthFromFieldKey": "deco2Size",
                          "child": {
                            "key": "",
                            "widgetName": "SimpleImage",
                            "widgetSetting": {
                              "imageDataKey": "decoImage2",
                            },
                          }
                        }
                      }
                    }
                  }
                }
              },
              {
                // bottom left leaf
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "left": "20.sp",
                  "bottom": "4.sp",
                  "child": {
                    "key": "",
                    "widgetName": "TransformRotate",
                    "widgetSetting": {
                      "angle": "1",
                      "child": {
                        "key": "",
                        "widgetName": "SizedBoxByRatio",
                        "widgetSetting": {
                          // "height": "20.sp",
                          // "width": "20.sp",
                          "heightFromFieldKey": "deco2Size",
                          "widthFromFieldKey": "deco2Size",
                          "child": {
                            "key": "",
                            "widgetName": "SimpleImage",
                            "widgetSetting": {
                              "imageDataKey": "decoImage2",
                            },
                          }
                        }
                      }
                    }
                  }
                }
              },
              {
                // middle right leaf
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "right": "2.sp",
                  "top": "50.h",
                  "child": {
                    "key": "",
                    "widgetName": "TransformRotate",
                    "widgetSetting": {
                      "angle": "-1",
                      "child": {
                        "key": "",
                        "widgetName": "SizedBoxByRatio",
                        "widgetSetting": {
                          // "height": "20.sp",
                          // "width": "20.sp",
                          "heightFromFieldKey": "deco2Size",
                          "widthFromFieldKey": "deco2Size",
                          "child": {
                            "key": "",
                            "widgetName": "SimpleImage",
                            "widgetSetting": {
                              "imageDataKey": "decoImage2",
                            },
                          }
                        }
                      }
                    }
                  }
                }
              },
              {
                // bottom right leaf
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "right": "0.sp",
                  "bottom": "4.sp",
                  "child": {
                    "key": "",
                    "widgetName": "TransformRotate",
                    "widgetSetting": {
                      // "angle": "1",
                      "child": {
                        "key": "",
                        "widgetName": "SizedBoxByRatio",
                        "widgetSetting": {
                          // "height": "20.sp",
                          // "width": "20.sp",
                          "heightFromFieldKey": "deco2Size",
                          "widthFromFieldKey": "deco2Size",
                          "child": {
                            "key": "",
                            "widgetName": "SimpleImage",
                            "widgetSetting": {
                              "imageDataKey": "decoImage2",
                            },
                          }
                        }
                      }
                    }
                  }
                }
              },
              // Category image block 1
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "left": "0",
                  "top": "0",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "heightFromFieldKey": "categoryImageBlock1Height",
                      "widthFromFieldKey": "categoryImageBlock1Width",
                      "child": {
                        "widgetName": "Stack",
                        "widgetSetting": {
                          "key": "",
                          "children": [
                            {
                              // Category image 1
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "left": "-5.sp",
                                "top": "5.sp",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "-0.2",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "heightFromFieldKey":
                                            "categoryImage1Size",
                                        "widthFromFieldKey":
                                            "categoryImage1Size",
                                        "child": {
                                          "key": "",
                                          "widgetName": "Container",
                                          "widgetSetting": {
                                            "clipBehavior": "hardEdge",
                                            "decoration": {
                                              "borderRadius": "24",
                                            },
                                            "child": {
                                              "key": "",
                                              "widgetName": "SimpleImage",
                                              "widgetSetting": {
                                                "imageDataKey":
                                                    "categoryImage1",
                                                "fit": "cover"
                                              },
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              // Category image 2
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "right": "0",
                                "top": "-5.sp",
                                "child": {
                                  "key": "",
                                  "widgetName": "SizedBoxByRatio",
                                  "widgetSetting": {
                                    "heightFromFieldKey": "categoryImage2Size",
                                    "widthFromFieldKey": "categoryImage2Size",
                                    "child": {
                                      "key": "",
                                      "widgetName": "Container",
                                      "widgetSetting": {
                                        "clipBehavior": "hardEdge",
                                        "decoration": {
                                          "borderRadius": "24",
                                        },
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "categoryImage2",
                                            "fit": "cover"
                                          }
                                        }
                                      },
                                    }
                                  }
                                }
                              }
                            },
                          ]
                        }
                      }
                    },
                  }
                }
              },
              {
                // top right image - category image 3
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "right": "-10.sp",
                  "top": "-10.sp",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "heightFromFieldKey": "categoryImage3Size",
                      "widthFromFieldKey": "categoryImage3Size",
                      "child": {
                        "key": "",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "clipBehavior": "hardEdge",
                          "decoration": {
                            "borderRadius": "24",
                          },
                          "child": {
                            "key": "",
                            "widgetName": "SimpleImage",
                            "widgetSetting": {
                              "imageDataKey": "categoryImage3",
                              "fit": "cover"
                            },
                          }
                        }
                      }
                    }
                  }
                }
              },
              {
                // menu
                "key": "",
                "widgetName": "PaddingByRatio",
                "widgetSetting": {
                  "verticalPaddingFieldKey": "menuVerticalPadding",
                  "horizontalPaddingFieldKey": "menuHorizontalPadding",
                  "child": {
                    "key": "",
                    "widgetName": "Container",
                    "widgetSetting": {
                      // "color": "0x50ff0000",
                      "child": {
                        "key": "",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "mainAxisAlignment": "end",
                          "children": [
                            // fixed text: "Restaurant Menu"
                            {
                              "key": "",
                              "widgetName": "Row",
                              "widgetSetting": {
                                "children": [
                                  {
                                    "key": "",
                                    "widgetName": "Text",
                                    "widgetSetting": {
                                      "data": "Restaurant Menu",
                                      "style": {
                                        "fontFamily": "DM Serif Display",
                                        "color": "0xffffffff",
                                        "fontWeight": "w500",
                                      },
                                      "fontSizeFromFieldKey":
                                          "menuTitleFontSize",
                                    }
                                  }
                                ]
                              }
                            },
                            {
                              // Menu row: 3 columns
                              "key": "",
                              "widgetName": "Row",
                              "widgetSetting": {
                                "children": [
                                  // Category column 1
                                  {
                                    "key": "",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "key": "",
                                        "widgetName": "Column",
                                        "widgetSetting": {
                                          "children": [
                                            // Product 1
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "height": "20.sp",
                                                "heightFromFieldKey":
                                                    "productRowHeight",
                                                // "width": "70.sp",
                                                "color": "0x5000ff00",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "ProductRow3",
                                                  "widgetSetting": {
                                                    "product": "product1",
                                                    "imageWidth": "20.sp",
                                                    "imageBorderRadius":
                                                        "1.5.sp",
                                                    "elementGap": "4.sp",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productNameFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productPriceFontSize",
                                                    "productDesFontSizeFromFieldKey":
                                                        "productDesFontSize",
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "useSubname": "true",
                                                    "productSubNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xFFED9D54",
                                                    },
                                                    "productDescriptionTextStyle":
                                                        {
                                                      "fontFamily":
                                                          "Source Serif Pro",
                                                      "fontWeight": "w400",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productSubNameFontSizeFromFieldKey":
                                                        "productSubNameFontSize",
                                                    "maxLineOfProductName": "2",
                                                    "prefixProductPrice": "",
                                                    "imageAtEnd": "true"
                                                  }
                                                }
                                              }
                                            },
                                            // Divider
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // // "width": "70.sp",
                                                // "widthFromFieldKey":
                                                //     "productDividerWidth",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Divider",
                                                  "widgetSetting": {
                                                    "color": "0xFFED9D54",
                                                    "thickness": "1"
                                                  }
                                                }
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "productGap"
                                              }
                                            },
                                            // Product 2
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "height": "20.sp",
                                                "heightFromFieldKey":
                                                    "productRowHeight",
                                                // "width": "70.sp",
                                                "color": "0x5000ff00",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "ProductRow3",
                                                  "widgetSetting": {
                                                    "product": "product2",
                                                    "imageWidth": "20.sp",
                                                    "imageBorderRadius":
                                                        "1.5.sp",
                                                    "elementGap": "4.sp",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productNameFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productPriceFontSize",
                                                    "productDesFontSizeFromFieldKey":
                                                        "productDesFontSize",
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "useSubname": "true",
                                                    "productSubNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xFFED9D54",
                                                    },
                                                    "productDescriptionTextStyle":
                                                        {
                                                      "fontFamily":
                                                          "Source Serif Pro",
                                                      "fontWeight": "w400",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productSubNameFontSizeFromFieldKey":
                                                        "productSubNameFontSize",
                                                    "maxLineOfProductName": "2",
                                                    "prefixProductPrice": "",
                                                    "imageAtEnd": "true"
                                                  }
                                                }
                                              }
                                            },
                                            // Divider
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "width": "70.sp",
                                                // "widthFromFieldKey":
                                                //     "productDividerWidth",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Divider",
                                                  "widgetSetting": {
                                                    "color": "0xFFED9D54",
                                                    "thickness": "1"
                                                  }
                                                }
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "productGap"
                                              }
                                            },
                                            // Product 3
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "height": "20.sp",
                                                "heightFromFieldKey":
                                                    "productRowHeight",
                                                // "width": "70.sp",
                                                "color": "0x5000ff00",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "ProductRow3",
                                                  "widgetSetting": {
                                                    "product": "product3",
                                                    "imageWidth": "20.sp",
                                                    "imageBorderRadius":
                                                        "1.5.sp",
                                                    "elementGap": "4.sp",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productNameFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productPriceFontSize",
                                                    "productDesFontSizeFromFieldKey":
                                                        "productDesFontSize",
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "useSubname": "true",
                                                    "productSubNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xFFED9D54",
                                                    },
                                                    "productDescriptionTextStyle":
                                                        {
                                                      "fontFamily":
                                                          "Source Serif Pro",
                                                      "fontWeight": "w400",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productSubNameFontSizeFromFieldKey":
                                                        "productSubNameFontSize",
                                                    "maxLineOfProductName": "2",
                                                    "prefixProductPrice": "",
                                                    "imageAtEnd": "true"
                                                  }
                                                }
                                              }
                                            },
                                            // Divider
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "width": "70.sp",
                                                // "widthFromFieldKey":
                                                //     "productDividerWidth",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Divider",
                                                  "widgetSetting": {
                                                    "color": "0xFFED9D54",
                                                    "thickness": "1"
                                                  }
                                                }
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "productGap"
                                              }
                                            }
                                          ]
                                        }
                                      },
                                    }
                                  },

                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "widthFromFieldKey": "columnGap"
                                    }
                                  },
                                  // Category column 2
                                  {
                                    "key": "",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "key": "",
                                        "widgetName": "Column",
                                        "widgetSetting": {
                                          "children": [
                                            // Product 4
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "height": "20.sp",
                                                "heightFromFieldKey":
                                                    "productRowHeight",
                                                // "width": "70.sp",
                                                "color": "0x5000ff00",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "ProductRow3",
                                                  "widgetSetting": {
                                                    "product": "product4",
                                                    "imageWidth": "20.sp",
                                                    "imageBorderRadius":
                                                        "1.5.sp",
                                                    "elementGap": "4.sp",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productNameFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productPriceFontSize",
                                                    "productDesFontSizeFromFieldKey":
                                                        "productDesFontSize",
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "useSubname": "true",
                                                    "productSubNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xFFED9D54",
                                                    },
                                                    "productDescriptionTextStyle":
                                                        {
                                                      "fontFamily":
                                                          "Source Serif Pro",
                                                      "fontWeight": "w400",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productSubNameFontSizeFromFieldKey":
                                                        "productSubNameFontSize",
                                                    "maxLineOfProductName": "2",
                                                    "prefixProductPrice": "",
                                                    "imageAtEnd": "true"
                                                  }
                                                }
                                              }
                                            },
                                            // Divider
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "width": "70.sp",
                                                // "widthFromFieldKey":
                                                //     "productDividerWidth",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Divider",
                                                  "widgetSetting": {
                                                    "color": "0xFFED9D54",
                                                    "thickness": "1"
                                                  }
                                                }
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "productGap"
                                              }
                                            },
                                            // Product 5
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "height": "20.sp",
                                                "heightFromFieldKey":
                                                    "productRowHeight",
                                                // "width": "70.sp",
                                                "color": "0x5000ff00",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "ProductRow3",
                                                  "widgetSetting": {
                                                    "product": "product5",
                                                    "imageWidth": "20.sp",
                                                    "imageBorderRadius":
                                                        "1.5.sp",
                                                    "elementGap": "4.sp",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productNameFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productPriceFontSize",
                                                    "productDesFontSizeFromFieldKey":
                                                        "productDesFontSize",
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "useSubname": "true",
                                                    "productSubNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xFFED9D54",
                                                    },
                                                    "productDescriptionTextStyle":
                                                        {
                                                      "fontFamily":
                                                          "Source Serif Pro",
                                                      "fontWeight": "w400",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productSubNameFontSizeFromFieldKey":
                                                        "productSubNameFontSize",
                                                    "maxLineOfProductName": "2",
                                                    "prefixProductPrice": "",
                                                    "imageAtEnd": "true"
                                                  }
                                                }
                                              }
                                            },
                                            // Divider
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "width": "70.sp",
                                                // "widthFromFieldKey":
                                                //     "productDividerWidth",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Divider",
                                                  "widgetSetting": {
                                                    "color": "0xFFED9D54",
                                                    "thickness": "1"
                                                  }
                                                }
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "productGap"
                                              }
                                            },
                                            // Product 6
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "height": "20.sp",
                                                "heightFromFieldKey":
                                                    "productRowHeight",
                                                // "width": "70.sp",
                                                "color": "0x5000ff00",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "ProductRow3",
                                                  "widgetSetting": {
                                                    "product": "product6",
                                                    "imageWidth": "20.sp",
                                                    "imageBorderRadius":
                                                        "1.5.sp",
                                                    "elementGap": "4.sp",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productNameFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productPriceFontSize",
                                                    "productDesFontSizeFromFieldKey":
                                                        "productDesFontSize",
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "useSubname": "true",
                                                    "productSubNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xFFED9D54",
                                                    },
                                                    "productDescriptionTextStyle":
                                                        {
                                                      "fontFamily":
                                                          "Source Serif Pro",
                                                      "fontWeight": "w400",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productSubNameFontSizeFromFieldKey":
                                                        "productSubNameFontSize",
                                                    "maxLineOfProductName": "2",
                                                    "prefixProductPrice": "",
                                                    "imageAtEnd": "true"
                                                  }
                                                }
                                              }
                                            },
                                            // Divider
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "width": "70.sp",
                                                // "widthFromFieldKey":
                                                //     "productDividerWidth",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Divider",
                                                  "widgetSetting": {
                                                    "color": "0xFFED9D54",
                                                    "thickness": "1"
                                                  }
                                                }
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "productGap"
                                              }
                                            }
                                          ]
                                        }
                                      },
                                    }
                                  },

                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "widthFromFieldKey": "columnGap"
                                    }
                                  },
                                  // Category column 3
                                  {
                                    "key": "",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "key": "",
                                        "widgetName": "Column",
                                        "widgetSetting": {
                                          "children": [
                                            // Product 7
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "height": "20.sp",
                                                "heightFromFieldKey":
                                                    "productRowHeight",
                                                // "width": "70.sp",
                                                "color": "0x5000ff00",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "ProductRow3",
                                                  "widgetSetting": {
                                                    "product": "product7",
                                                    "imageWidth": "20.sp",
                                                    "imageBorderRadius":
                                                        "1.5.sp",
                                                    "elementGap": "4.sp",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productNameFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productPriceFontSize",
                                                    "productDesFontSizeFromFieldKey":
                                                        "productDesFontSize",
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "useSubname": "true",
                                                    "productSubNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xFFED9D54",
                                                    },
                                                    "productDescriptionTextStyle":
                                                        {
                                                      "fontFamily":
                                                          "Source Serif Pro",
                                                      "fontWeight": "w400",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productSubNameFontSizeFromFieldKey":
                                                        "productSubNameFontSize",
                                                    "maxLineOfProductName": "2",
                                                    "prefixProductPrice": "",
                                                    "imageAtEnd": "true"
                                                  }
                                                }
                                              }
                                            },
                                            // Divider
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "width": "70.sp",
                                                // "widthFromFieldKey":
                                                //     "productDividerWidth",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Divider",
                                                  "widgetSetting": {
                                                    "color": "0xFFED9D54",
                                                    "thickness": "1"
                                                  }
                                                }
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "productGap"
                                              }
                                            },
                                            // Product 8
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "height": "20.sp",
                                                "heightFromFieldKey":
                                                    "productRowHeight",
                                                // "width": "70.sp",
                                                "color": "0x5000ff00",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "ProductRow3",
                                                  "widgetSetting": {
                                                    "product": "product8",
                                                    "imageWidth": "20.sp",
                                                    "imageBorderRadius":
                                                        "1.5.sp",
                                                    "elementGap": "4.sp",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productNameFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productPriceFontSize",
                                                    "productDesFontSizeFromFieldKey":
                                                        "productDesFontSize",
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "useSubname": "true",
                                                    "productSubNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xFFED9D54",
                                                    },
                                                    "productDescriptionTextStyle":
                                                        {
                                                      "fontFamily":
                                                          "Source Serif Pro",
                                                      "fontWeight": "w400",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productSubNameFontSizeFromFieldKey":
                                                        "productSubNameFontSize",
                                                    "maxLineOfProductName": "2",
                                                    "prefixProductPrice": "",
                                                    "imageAtEnd": "true"
                                                  }
                                                }
                                              }
                                            },
                                            // Divider
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "width": "70.sp",
                                                // "widthFromFieldKey":
                                                //     "productDividerWidth",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Divider",
                                                  "widgetSetting": {
                                                    "color": "0xFFED9D54",
                                                    "thickness": "1"
                                                  }
                                                }
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "productGap"
                                              }
                                            },
                                            // Product 9
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "height": "20.sp",
                                                "heightFromFieldKey":
                                                    "productRowHeight",
                                                // "width": "70.sp",
                                                "color": "0x5000ff00",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "ProductRow3",
                                                  "widgetSetting": {
                                                    "product": "product9",
                                                    "imageWidth": "20.sp",
                                                    "imageBorderRadius":
                                                        "1.5.sp",
                                                    "elementGap": "4.sp",
                                                    "productNameFontSizeFromFieldKey":
                                                        "productNameFontSize",
                                                    "productPriceFontSizeFromFieldKey":
                                                        "productPriceFontSize",
                                                    "productDesFontSizeFromFieldKey":
                                                        "productDesFontSize",
                                                    "productNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productPriceTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xffffffff",
                                                    },
                                                    "useSubname": "true",
                                                    "productSubNameTextStyle": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w500",
                                                      "color": "0xFFED9D54",
                                                    },
                                                    "productDescriptionTextStyle":
                                                        {
                                                      "fontFamily":
                                                          "Source Serif Pro",
                                                      "fontWeight": "w400",
                                                      "color": "0xffffffff",
                                                    },
                                                    "productSubNameFontSizeFromFieldKey":
                                                        "productSubNameFontSize",
                                                    "maxLineOfProductName": "2",
                                                    "prefixProductPrice": "",
                                                    "imageAtEnd": "true"
                                                  }
                                                }
                                              }
                                            },
                                            // Divider
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                // "width": "70.sp",
                                                // "widthFromFieldKey":
                                                //     "productDividerWidth",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Divider",
                                                  "widgetSetting": {
                                                    "color": "0xFFED9D54",
                                                    "thickness": "1"
                                                  }
                                                }
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "heightFromFieldKey":
                                                    "productGap"
                                              }
                                            }
                                          ]
                                        }
                                      },
                                    }
                                  },
                                ]
                              }
                            },
                          ]
                        }
                      }
                    }
                  }
                }
              },
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "top": "0",
                  "right": "-10.sp",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      // "height": "30.sp",
                      "width": "120.sp",
                      "heightFromFieldKey": "discountBlockHeight",
                      "child": {
                        "key": "",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "key": "",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "": "",
                              },
                            },
                            {
                              "key": "",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "padding": {
                                  "left": "14.sp",
                                  "right": "32.sp",
                                  "bottom": "5.sp",
                                },
                                "decoration": {
                                  "image": "imageBackgroundTitle",
                                  "boxFit": "contain",
                                },
                                "child": {
                                  "key": "",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "crossAxisAlignment": "start",
                                    "mainAxisAlignment": "start",
                                    "children": [
                                      {
                                        "key": "",
                                        "widgetName": "Row",
                                        "widgetSetting": {
                                          "mainAxisAlignment": "start",
                                          "children": [
                                            {
                                              "key": "",
                                              "widgetName": "Container",
                                              "widgetSetting": {
                                                // "color": "0x5000ff00",
                                                "width": "20.sp",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Text",
                                                  "widgetSetting": {
                                                    "data": "Get Your Discount",
                                                    "style": {
                                                      "fontFamily":
                                                          "DM Serif Display",
                                                      "fontWeight": "w400",
                                                      "color": "0xffffffff",
                                                    },
                                                    "fontSizeFromFieldKey":
                                                        "discountTitleFontSize",
                                                  }
                                                },
                                              }
                                            },
                                            {
                                              "key": "",
                                              "widgetName": "SizedBox",
                                              "widgetSetting": {
                                                "width": "1.sp",
                                              }
                                            },
                                            {
                                              "key": "",
                                              "widgetName": "Text",
                                              "widgetSetting": {
                                                "data": "30%",
                                                "style": {
                                                  "fontFamily":
                                                      "DM Serif Display",
                                                  "fontWeight": "w500",
                                                  "color": "0xffffffff",
                                                  "height": "0",
                                                },
                                                "fontSizeFromFieldKey":
                                                    "discountPercentFontSize",
                                              }
                                            }
                                          ]
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data":
                                              "Only Friday 08:00 a.m until 11:00 a.m.",
                                          "style": {
                                            "fontFamily": "Source Serif Pro",
                                            "fontWeight": "w400",
                                            "color": "0xffffffff",
                                          },
                                          "fontSizeFromFieldKey":
                                              "discountDateFontSize",
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            }
                          ]
                        },
                      }
                    }
                  }
                }
              }
            ]
          }
        }
      }
    }
  };
}
