class JsonDataTemplate {
  //[092328HG] Check temprestaurant5: Run OK - Ratio OK
  static final temprestaurant5 = {
    "_id": "id_tem5",
    "templateSlug": "temp_restaurant_5",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.w',
          'nameProductStyle': '0.8.w',
          'nameProductStyleMedium': '0.4.w',
          'priceProductStyle': '0.8.w',
          'priceProductStyleMedium': '0.4.w',
          'discriptionProductStyle': '0.4.w',
          'discriptionProductStyleMedium': '0.3.w',
          'imageHeight': '50.h',
          'marginBottomImage': '0.4.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.4.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '21/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.w',
          'nameProductStyleMedium': '0.5.w',
          'priceProductStyle': '1.w',
          'priceProductStyleMedium': '0.5.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '50.h',
          'marginBottomImage': '0.5.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.5.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '16/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '16/10': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.7.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '4/3': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '2.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '2.w',
          'priceProductStyleMedium': '1.3.w',
          'discriptionProductStyle': '1.3.w',
          'discriptionProductStyleMedium': '0.7.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      //092314LH Fix dynamic sample data to template
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '/temp_restaurant_2/food(1).png',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '/temp_restaurant_2/food(2).png',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '/temp_restaurant_2/food(3).png',
      },
      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '/temp_restaurant_2/food(4).png',
      },
      'category5': {
        'key': 'category5',
        'type': 'category',
        'name': 'Category 5',
        'image': '/temp_restaurant_2/food(5).png',
      },
      'category6': {
        'key': 'category6',
        'type': 'category',
        'name': 'Category 6',
        'image': '/temp_restaurant_2/food(6).png',
      },

      //Danh mục 1: pro 1, 2 ,3
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 2: pro 4, 5, 6
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$40.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$50.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$60.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 3: pro 7, 8, 9
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$70.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$80.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 9',
        'price': '\$90.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 4: pro  10, 11,12
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 10',
        'price': '\$10.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 11',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 12',
        'price': '\$41.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 5: pro 13, 14, 15
      'product13': {
        'key': 'product13',
        'type': 'product',
        'name': 'Product 13',
        'price': '\$42.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product14': {
        'key': 'product14',
        'type': 'product',
        'name': 'Product 14',
        'price': '\$43.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product15': {
        'key': 'product15',
        'type': 'product',
        'name': 'Product 15',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 6: pro 16, 17, 18
      'product16': {
        'key': 'product16',
        'type': 'product',
        'name': 'Product 16',
        'price': '\$16.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product17': {
        'key': 'product17',
        'type': 'product',
        'name': 'Product 17',
        'price': '\$17.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product18': {
        'key': 'product18',
        'type': 'product',
        'name': 'Product 18',
        'price': '\$18.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Hình background
      'imageBackground': {
        'key': 'imageBackground',
        'type': 'image',
        // 'image': '/temp_restaurant_5/BG-1.png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/BG-1-1.png',
      },

      //Hình Background Title
      'imageBackgroundTitle': {
        'key': 'imageBackgroundTitle',
        'type': 'image',
        // 'image': '/temp_restaurant_5/decorate/BG-title.png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/BG-title.png',
      },

      //Hình phân tách cột
      'imageSeperatorLine': {
        'key': 'imageSeperatorLine',
        'type': 'image',
        'fixed': 'true',
        // 'image': '/temp_restaurant_1/decorate/Seperator-Line.png',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/Seperator-Line-1.png',
      },

      //Hình danh mục 1
      'image1': {
        'key': 'image1',
        'type': 'image',
        // 'image': '/temp_restaurant_5/hamburger(1).png',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/hamburger1.png',
      },

      //Hình danh mục 6
      'image2': {
        'key': 'image2',
        'type': 'image',
        // 'image': '/temp_restaurant_5/hamburger(2).png',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/hamburger2.png',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image":
              "imageBackground", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
          "boxFit": "fill",
        },
        "child": {
          "key": "row",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              //Cột A
              {
                "key": "expanded-A",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Padding-A",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "3.w"},
                      "child": {
                        "key": "column-A",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            // Cột A - Phần 1:
                            {
                              "key": "expanded-A1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "key": "column-A121",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      // Cột A - Phần 1 - Dòng 1: Tiêu đề Danh mục 1
                                      {
                                        "key": "categoryTitle-A11",
                                        "widgetName": "categoryTitle",
                                        "widgetSetting": {
                                          "categoryId":
                                              "category1", //key category1
                                          "useBackground": "true",
                                          "imageId":
                                              "imageBackgroundTitle", //key imageBackgroundTitle
                                          "useFullWidth": "false",
                                          "color": "0xFF121212",
                                          "colorBackground": "0x00121212",
                                        }
                                      },

                                      // Cột A - Phần 1 - Dòng 2: Danh mục 1
                                      {
                                        "key": "expanded-A12",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "row-A12",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "stretch",
                                              "children": [
                                                //Cột A - Phần 1 - Dòng 2 - Cột 1: Danh sách sản phẩm (3 sản phẩm đầu tiên)
                                                {
                                                  "key": "expanded-A121",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "flex": "1",
                                                    "child": {
                                                      "key": "productList-A121",
                                                      "widgetName":
                                                          "productList",
                                                      "widgetSetting": {
                                                        "dataList": [
                                                          "product1",
                                                          "product2",
                                                          "product3",
                                                        ],
                                                        "mainAxisAlignment":
                                                            "start",
                                                        "crossAxisAlignment":
                                                            "center",
                                                        "colorPrice":
                                                            "0xFFf4b91a",
                                                        "colorProductName":
                                                            "0xFFFFFFFF",
                                                        "toUpperCaseNameProductName":
                                                            "true",
                                                        "useMediumStyle":
                                                            "false",
                                                        "colorDescription":
                                                            "0xFFFFFFFF",
                                                        "useThumnailProduct":
                                                            "false",
                                                        "pathThumnailProduct":
                                                            "",
                                                        "maxLineProductName":
                                                            "1",
                                                        "maxLineProductDescription":
                                                            "2"
                                                      }
                                                    }
                                                  }
                                                },

                                                //Cột A - Phần 1 - Dòng 2 - Cột 2: Decoration
                                                {
                                                  "key": "container-A122",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "width": "5.w",
                                                    "padding": {
                                                      "vertical": "2.w"
                                                    },
                                                    "decoration": {
                                                      "image":
                                                          "imageSeperatorLine", //Key imageSeperatorLine, Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
                                                      "boxFit": "contain"
                                                    }
                                                  }
                                                },

                                                //Cột A - Phần 1 - Dòng 2 - Cột 3: hình số 1
                                                {
                                                  "key": "expanded-A123",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "padding-A123",
                                                      "widgetName": "Padding",
                                                      "widgetSetting": {
                                                        "padding": {
                                                          "all": "1.w"
                                                        },
                                                        "child": {
                                                          "key": "image-A123",
                                                          "widgetName": "image",
                                                          "widgetSetting": {
                                                            "imageId":
                                                                "image1", //key = image1
                                                            "fixCover": "false"
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },

                            // Cột A - Phần 2:
                            {
                              "key": "expanded-A",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  //Cột A - Phần 2
                                  "key": "row-A2",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      //Cột A - Phần 2 - Cột 1: Danh mục 2 với
                                      //3 sản phẩm 4,5,6
                                      {
                                        "key": "expanded-A2",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "CTSC-A21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category2",
                                                'product': [
                                                  "product4",
                                                  "product5",
                                                  "product6",
                                                ],
                                              },

                                              "indexCategory": "1",
                                              "useBackgroundCategoryTitle":
                                                  "true",

                                              "imageId":
                                                  "imageBackgroundTitle", //Key imageBackgroundTitle

                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          },
                                        },
                                      },

                                      //Cột A - Phần 2 - cột 2: Decoration
                                      {
                                        "key": "container-A22",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"},
                                          "decoration": {
                                            "image":
                                                "imageSeperatorLine", //Key imageSeperatorLine, Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
                                            "boxFit": "contain"
                                          }
                                        }
                                      },

                                      //Cột A - Phần 2 - Cột 3: Danh mục 3 với
                                      //3 sản phẩm 7,8,9
                                      {
                                        "key": "expanded-A2",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "CTSC-A21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category3",
                                                'product': [
                                                  "product7",
                                                  "product8",
                                                  "product9",
                                                ],
                                              },

                                              "indexCategory": "2",
                                              "useBackgroundCategoryTitle":
                                                  "true",

                                              "imageId":
                                                  "imageBackgroundTitle", //Key imageBackgroundTitle

                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          },
                                        },
                                      },
                                    ]
                                  },
                                },
                              },
                            }
                          ]
                        }
                      },
                    },
                  },
                },
              },

              //Cột B
              {
                "key": "expanded-B",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Padding-B",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "3.w"},
                      "child": {
                        "key": "column-B",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            // Cột B - Phần 1:
                            {
                              "key": "expanded-B1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  //Cột B - Phần 2
                                  "key": "row-B2",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      //Cột B - Phần 2 - Cột 1: Danh mục 4 với
                                      //3 sản phẩm 10,11,12
                                      {
                                        "key": "expanded-B21",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "CTSC-B21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category4",
                                                'product': [
                                                  "product10",
                                                  "product11",
                                                  "product12",
                                                ],
                                              },

                                              "indexCategory": "3",
                                              "useBackgroundCategoryTitle":
                                                  "true",

                                              "imageId":
                                                  "imageBackgroundTitle", //Key imageBackgroundTitle

                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          },
                                        },
                                      },

                                      //Cột B - Phần 2 - cột 2: Decoration
                                      {
                                        "key": "container-B22",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"},
                                          "decoration": {
                                            "image":
                                                "imageSeperatorLine", //Key imageSeperatorLine, Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
                                            "boxFit": "contain"
                                          }
                                        }
                                      },

                                      //Cột A - Phần 2 - Cột 3: Danh mục 5 với
                                      //3 sản phẩm 13,14,15
                                      {
                                        "key": "expanded-B23",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "CTSC-B21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category5",
                                                'product': [
                                                  "product13",
                                                  "product14",
                                                  "product15",
                                                ],
                                              },

                                              "indexCategory": "4",
                                              "useBackgroundCategoryTitle":
                                                  "true",

                                              "imageId":
                                                  "imageBackgroundTitle", //Key imageBackgroundTitle

                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          },
                                        },
                                      },
                                    ]
                                  },
                                },
                              },
                            },

                            // Cột B - Phần 2:
                            {
                              "key": "expanded-B2",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  //Cột B - Phần 2
                                  "key": "row-B2",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      //Cột B - Phần 2 - Cột 1: Danh mục 6 với
                                      //3 sản phẩm 16,17,18
                                      {
                                        "key": "expanded-B2",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "CTSC-B21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category6",
                                                'product': [
                                                  "product16",
                                                  "product17",
                                                  "product18",
                                                ],
                                              },
                                              "indexCategory": "5",
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "imageId":
                                                  "imageBackgroundTitle", //Key imageBackgroundTitle
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          },
                                        },
                                      },

                                      //Cột B - Phần 2 - cột 2: hình số 2
                                      {
                                        "key": "expanded-A123",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "padding-A123",
                                            "widgetName": "Padding",
                                            "widgetSetting": {
                                              "padding": {"all": "1.w"},
                                              "child": {
                                                "key": "image-A123",
                                                "widgetName": "image",
                                                "widgetSetting": {
                                                  "imageId":
                                                      "image2", //key = image2
                                                  "fixCover": "false"
                                                }
                                              }
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  },
                                },
                              },
                            },
                          ]
                        }
                      },
                    },
                  },
                },
              },
            ]
          }
        }
      }
    }
  };
}
