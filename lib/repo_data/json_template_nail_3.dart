class JsonDataTemplate {
  //[092328HG] Check Nail3: Run OK - Ratio OK
  //[092304Tuan] adjust according to feedback from Mr Hugo
  static final templateNail3 = {
    "_id": "",
    "templateSlug": "temp_nail_3",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Cormorant Garamond',
        },
        'nameProductStyle': {
          'fontWeight': 'w600',
          'fontFamily': 'Cormorant Garamond',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Cormorant Garamond',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Lato',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Lato',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Cormorant Garamond',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Cormorant Garamond',
        },
      },
      'renderScreen': {
        '32/9': {
          'nameProductStyle': '1.w',
          'priceProductStyle': '1.w',
          'discriptionProductStyle': '0.9.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '16.h',
        },
        '21/9': {
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.2.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '18.h',
        },
        '16/9': {
          'nameProductStyle': '1.75.w',
          'priceProductStyle': '1.75.w',
          'imageHeight': '25.h',
          'discriptionProductStyle': '1.25.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '20.h',
        },
        '16/10': {
          'nameProductStyle': '1.8.w',
          'priceProductStyle': '1.8.w',
          'imageHeight': '25.h',
          'discriptionProductStyle': '1.25.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '20.h',
        },
        '4/3': {
          'nameProductStyle': '2.w',
          'priceProductStyle': '2.w',
          'discriptionProductStyle': '1.5.w',

          // 'widthThumnailProduct': '28.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '22.h',
        },
        'default': {
          'nameProductStyle': '1.75.w',
          'priceProductStyle': '1.75.w',
          'imageHeight': '25.h',
          'discriptionProductStyle': '1.25.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '20.h',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_1.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_2.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_3.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$23.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_4.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$23.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_5.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$23.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_6.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      // begin: container
      "widgetName": "Container",
      "widgetSetting": {
        "color": "0xFFF2EFEA",
        "width": "100.w",
        "height": "100.h",
        "padding": {"vertical": "3.h"},
        "child": {
          "widgetName": "Column",
          "widgetSetting": {
            "children": [
              {
                "widgetName": "Divider",
                "widgetSetting": {
                  "thickness": "2",
                  "height": "4.h",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "endIndent": "20.w",
                },
              },
              {
                "widgetName": "Text",
                "widgetSetting": {
                  "data": "NAIL SALON",
                  "style": {
                    "fontSize": "4.h",
                    "color": "0xFF616148", //primary color
                    "fontWeight": "w800",
                    "fontFamily": "Cormorant Garamond",
                  }
                },
              },
              {
                "widgetName": "Text",
                "widgetSetting": {
                  "data": "NAIL IT!",
                  "style": {
                    "fontSize": "3.h",
                    "color": "0xFF616148", //primary color
                    "fontWeight": "w800",
                    "fontFamily": "Cormorant Garamond",
                  } //secondary color
                },
              },
              {
                "widgetName": "Divider",
                "widgetSetting": {
                  "thickness": "2",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "height": "4.h",
                  "endIndent": "20.w",
                },
              },
              {
                "widgetName": "Wrap",
                "widgetSetting": {
                  "runSpacing": "2.h",
                  "spacing": "4.w",
                  "children": [
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product1",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product2",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product3",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product4",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product5",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product6",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                  ],
                }
              },
              {
                "widgetName": "Divider",
                "widgetSetting": {
                  "thickness": "2",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "height": "4.h",
                  "endIndent": "20.w",
                },
              },
              {
                "widgetName": "Row",
                "widgetSetting": {
                  "mainAxisAlignment": "spaceAround",
                  "children": [
                    {
                      "widgetName": "Text",
                      "widgetSetting": {
                        "data": "salon@hairdresser",
                        "style": {
                          "color": "0xFF616148",
                          'fontSize': '3.h',
                          'fontFamily': 'Cormorant Garamond',
                        },
                      },
                    },
                    {
                      "widgetName": "Text",
                      "widgetSetting": {
                        "data": "+012345689",
                        "style": {
                          "color": "0xFF616148",
                          'fontSize': '3.h',
                          'fontFamily': 'Neuton',
                        },
                      },
                    },
                    {
                      "widgetName": "Text",
                      "widgetSetting": {
                        "data": "hairdresser.com",
                        "style": {
                          "color": "0xFF616148",
                          'fontSize': '3.h',
                          'fontFamily': 'Cormorant Garamond',
                        },
                      },
                    }
                  ],
                },
              }
            ],
          }
        },
      },
    },
  };
}
