class JsonDataTemplate {
  //[092328HG] Check temppho1: Run OK - Ratio OK
  static final temppho1 = {
    "_id": "",
    "templateSlug": "temp_pho_1",
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "21/9": {
          "categoryImageHeight": "25.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.sp",
          "productPriceFontSize": "2.sp",
          "normalFontSize": "2.sp",
          "brandFontSize": "10.sp",
          "verticalFramePadding": "4.sp",
          "horizontalFramePadding": "4.sp",
        },
        "16/9": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "16/10": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "5.4.sp",
          "productNameFontSize": "3.5.sp",
          "productPriceFontSize": "3.5.sp",
          "normalFontSize": "4.8.sp",
          "brandFontSize": "18.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "4/3": {
          "categoryImageHeight": "45.sp",
          "categoryFontSize": "7.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp",
          "normalFontSize": "5.8.sp",
          "brandFontSize": "22.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "default": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
      },
    },
    "isDeleted": false,
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1.jpg",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2.jpg",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3.jpg",
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_4.jpg",
      },

      //Danh mục 1: pro 1, 2, 3, 4, 5, 6, 7
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 2: pro 8, 9, 10, 11, 12, 13, 14
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 3: pro 15, 16, 17, 18, 19, 20, 21
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product21": {
        "key": "product21",
        "type": "product",
        "name": "Product 21",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 1: pro 22, 23, 24, 25, 26, 27, 28
      "product22": {
        "key": "product22",
        "type": "product",
        "name": "Product 22",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product23": {
        "key": "product23",
        "type": "product",
        "name": "Product 23",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product24": {
        "key": "product24",
        "type": "product",
        "name": "Product 24",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product25": {
        "key": "product25",
        "type": "product",
        "name": "Product 25",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product26": {
        "key": "product26",
        "type": "product",
        "name": "Product 26",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product27": {
        "key": "product27",
        "type": "product",
        "name": "Product 27",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product28": {
        "key": "product28",
        "type": "product",
        "name": "Product 28",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "decoration": {"color": "0xffffffff"},
        "child": {
          "key": "PaddingByRatio-main",
          "widgetName": "PaddingByRatio",
          "widgetSetting": {
            "verticalPaddingFieldKey": "verticalFramePadding",
            "horizontalPaddingFieldKey": "horizontalFramePadding",
            "child": {
              "key": "Center-main",
              "widgetName": "Center",
              "widgetSetting": {
                "child": {
                  "key": "Column-main",
                  "widgetName": "Column",
                  "widgetSetting": {
                    "mainAxisAlignment": "center",
                    "children": [
                      // Row 1: info(local, est)
                      {
                        "key": "Container-header",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "padding": {"vertical": "2.sp"},
                          "child": {
                            "key": "Row-info",
                            "widgetName": "Row",
                            "widgetSetting": {
                              "mainAxisAlignment": "spaceBetween",
                              "children": [
                                // address
                                {
                                  "key": "text-address",
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "data": "4889 Princess St",
                                    "fontSizeFromFieldKey": "normalFontSize",
                                    "style": {
                                      "color": "0xff404042",
                                    }
                                  }
                                },
                                // est
                                {
                                  "key": "text-est",
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "data": "EST 1990",
                                    "fontSizeFromFieldKey": "normalFontSize",
                                    "style": {
                                      "color": "0xff404042",
                                    }
                                  }
                                }
                              ]
                            },
                          }
                        }
                      },
                      {
                        "key": "Container-divider",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "height": "1",
                          "decoration": {
                            "border": {
                              "color": "0xff404042",
                              "width": "0.1.sp",
                            }
                          },
                        }
                      },
                      // Row 2: brand
                      {
                        "key": "UnderlineBrand-brand",
                        "widgetName": "UnderlineBrand",
                        "widgetSetting": {
                          "brand": "PHO BRAND",
                          "fontSizeFromFieldKey": "brandFontSize",
                          "textStyle": {
                            "fontWeight": "w700",
                            "color": "0xff404042",
                          },
                          "bottomBorderSide": {
                            "width": "1.8.sp",
                            "color": "0xff404042 ",
                          }
                        }
                      },
                      // row 3: spacing
                      {
                        "key": "SizedBox-spacing-v-1",
                        "widgetName": "SizedBox",
                        "widgetSetting": {"height": "2.h"}
                      },

                      // row 4: menu
                      {
                        "key": "SizedBox-menu",
                        "widgetName": "SizedBox",
                        "widgetSetting": {
                          "width": "100.w",
                          "child": {
                            "key": "Row-menu",
                            "widgetName": "Row",
                            "widgetSetting": {
                              "direction": "horizontal",
                              "alignment": "spaceBetween",
                              "spacing": "1.sp",
                              "children": [
                                // menu column 1
                                {
                                  "key": "SizedBox-m-c-1",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "key": "CategoryColumn1-m-c-1",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category1",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000",
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          // Menu: Col1 - Pro1
                                          {
                                            "key": "ProductRow1-p1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product1",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro2
                                          {
                                            "key": "ProductRow1-p2",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product2",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro3
                                          {
                                            "key": "ProductRow1-p3",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product3",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro4
                                          {
                                            "key": "ProductRow1-p4",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product4",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro5
                                          {
                                            "key": "ProductRow1-p5",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product5",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro6
                                          {
                                            "key": "ProductRow1-p6",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product6",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro7
                                          {
                                            "key": "ProductRow1-p7",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product7",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                        ]
                                      },
                                    }
                                  }
                                },
                                {
                                  "key": "SizedBox-cat-scpacing",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"width": "2.w"}
                                },
                                // menu column 2
                                {
                                  "key": "SizedBox-m-c-2",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "key": "CategoryColumn1-m-c-2",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category2",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000",
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          // Menu: Col2 - Pro8
                                          {
                                            "key": "ProductRow1-p8",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product8",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pro9
                                          {
                                            "key": "ProductRow1-p9",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product9",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pro10
                                          {
                                            "key": "ProductRow1-p10",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product10",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pr11
                                          {
                                            "key": "ProductRow1-p11",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product11",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pr12
                                          {
                                            "key": "ProductRow1-p12",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product12",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pr13
                                          {
                                            "key": "ProductRow1-p13",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product13",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pr14
                                          {
                                            "key": "ProductRow1-p14",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product14",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                        ]
                                      },
                                    }
                                  }
                                },
                                {
                                  "key": "SizedBox-cat-scpacing",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"width": "2.w"}
                                },
                                // menu column 3
                                {
                                  "key": "SizedBox-m-c-3",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "key": "CategoryColumn1-m-c-3",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category3",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000",
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          // Menu: Col3 - Pro15
                                          {
                                            "key": "ProductRow1-p15",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product15",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col3 - Pro16
                                          {
                                            "key": "ProductRow1-p16",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product16",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },

                                          // Menu: Col3 - Pro17
                                          {
                                            "key": "ProductRow1-p17",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product17",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col3 - Pro18
                                          {
                                            "key": "ProductRow1-p18",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product18",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col3 - Pro19
                                          {
                                            "key": "ProductRow1-p19",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product19",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col3 - Pro20
                                          {
                                            "key": "ProductRow1-p20",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product20",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col3 - Pro21
                                          {
                                            "key": "ProductRow1-p21",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product21",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                        ]
                                      },
                                    }
                                  }
                                },
                                {
                                  "key": "SizedBox-cat-scpacing",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"width": "2.w"}
                                },
                                // menu column 4
                                {
                                  "key": "SizedBox-m-c-4",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "key": "CategoryColumn1-m-c-4",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category4",
                                        "elementGap": "2.2.sp",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000",
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          // Menu: Col4 - Pro22
                                          {
                                            "key": "ProductRow1-p22",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product22",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col4 - Pro23
                                          {
                                            "key": "ProductRow1-p23",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product23",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },

                                          // Menu: Col4 - Pro24
                                          {
                                            "key": "ProductRow1-p24",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product24",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col4 - Pro25
                                          {
                                            "key": "ProductRow1-p25",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product25",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col4 - Pro26
                                          {
                                            "key": "ProductRow1-p26",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product26",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col4 - Pro27
                                          {
                                            "key": "ProductRow1-p27",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product27",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col4 - Pro28
                                          {
                                            "key": "ProductRow1-p28",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product28",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.1.sp",
                                                  "color": "0xff000000",
                                                }
                                              }
                                            }
                                          },
                                        ]
                                      },
                                    }
                                  }
                                },
                              ],
                            }
                          }
                        }
                      }
                    ]
                  },
                },
              }
            }
          }
        }
      }
    }
  };
}
