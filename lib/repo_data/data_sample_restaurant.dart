// import 'package:tv_menu/models/model_datas/category_data.dart';
// import 'package:tv_menu/models/model_datas/product_data.dart';

// class DataSample {
//   //Tạo biến cho List<> có thể sử dụng nhiều nơi trong Class
//   late List<CategoryData> _categoryDataList;
//   late List<ProductData> _productDataList1;
//   late List<ProductData> _productDataList2;
//   late List<ProductData> _productDataList3;
//   late List<ProductData> _productDataList4;
//   late List<ProductData> _productDataList5;
//   late List<ProductData> _productDataList6;
//   // late List<CategoryData> _imageDataList;

//   //Tạo biến có giá trị Mặc định
//   late String logoPath;

//   //Tạo biến get lấy giá trị từ list<> cho các Class/file khác sử dụng
//   List<CategoryData> get categoryDataList => _categoryDataList;
//   List<ProductData> get productDataList1 => _productDataList1;
//   List<ProductData> get productDataList2 => _productDataList2;
//   List<ProductData> get productDataList3 => _productDataList3;
//   List<ProductData> get productDataList4 => _productDataList4;
//   List<ProductData> get productDataList5 => _productDataList5;
//   List<ProductData> get productDataList6 => _productDataList6;
//   // List<ProductData> get imageDataList => _imageDataList;

//   //Contructor
//   // Run các action mặc định khi sử dụng Class
//   DataSample({String logo = ''}) {
//     logoPath = logo != '' ? '5s3s-logoblack.png' : logo;
//     loadDataSample();
//   }

//   // Hàm generateMeaningfulString nhận vào một số nguyên n
//   // Trả về một chuỗi ngẫu nhiên nhưng có ý nghĩa, dựa theo chuỗi Lorem Ipsum, có độ dài nằm trong khoảng từ 20 đến 100 ký tự
//   // String generateMeaningfulString(int n) {
//   //   // Chuỗi Lorem Ipsum để làm nguồn cảm hứng cho chuỗi ngẫu nhiên
//   //   String text =
//   //       "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";
//   //   // Khởi tạo một chuỗi rỗng result để lưu kết quả
//   //   String result = '';
//   //   // Lặp lại cho đến khi result có độ dài nằm trong khoảng từ 50 đến 100 ký tự
//   //   // Điều kiện này có nghĩa là vòng lặp sẽ tiếp tục khi chuỗi có độ dài nhỏ hơn 50 hoặc lớn hơn 100,
//   //   // tức là không thỏa mãn yêu cầu của bạn. Khi chuỗi có độ dài nằm trong khoảng từ 50 đến 100,
//   //   // vòng lặp sẽ dừng lại và trả về kết quả.
//   //   while (result.length < 50 || result.length > 100) {
//   //     // Tách chuỗi Lorem Ipsum thành các câu bằng cách sử dụng dấu chấm (.) làm ký tự phân cách
//   //     List<String> sentences = text.split('.');
//   //     // Chọn ngẫu nhiên một câu từ các câu đã tách được
//   //     String sentence = sentences[Random().nextInt(sentences.length)];
//   //     // Tách câu đã chọn thành các từ bằng cách sử dụng khoảng trắng làm ký tự phân cách
//   //     List<String> words = sentence.split(' ');
//   //     // Gán result bằng chuỗi rỗng để bắt đầu tạo lại chuỗi ngẫu nhiên
//   //     result = '';
//   //     // Lặp lại cho đến khi độ dài của result bằng n hoặc không còn từ nào trong câu đã chọn
//   //     while (result.length < n && words.isNotEmpty) {
//   //       // Chọn ngẫu nhiên một từ từ các từ trong câu đã chọn
//   //       String word = words[Random().nextInt(words.length)];
//   //       // Nếu độ dài của result cộng với độ dài của từ đã chọn nhỏ hơn hoặc bằng n
//   //       if (result.length + word.length <= n) {
//   //         // Thêm từ đã chọn vào result, cùng với một khoảng trắng ở sau nếu cần
//   //         result += word + ' ';
//   //       }
//   //       // Xóa từ đã chọn khỏi câu đã chọn
//   //       words.remove(word);
//   //     }
//   //   }
//   //   // Trả về result làm kết quả của hàm
//   //   return result;
//   // }
//   // //Code trong Function chạy theo thứ tự từ trên xuống
//   // void loadDataSample() {
//   //   // Cách truyền data tĩnh từ model "productData"
//   //   _productDataList = [];
//   //   // Tạo một vòng lặp for từ 1 đến 10
//   //   for (int i = 1; i <= 10; i++) {
//   //     // Thêm một đối tượng productData vào danh sách với các thuộc tính ngẫu nhiên
//   //     _productDataList.add(
//   //       ProductData(
//   //         productName: 'Product $i', // Sử dụng biến i để tăng dần tên sản phẩm
//   //         productDescription: generateMeaningfulString(
//   //             20), // Sử dụng hàm generateMeaningfulString để tạo mô tả ngẫu nhiên nhưng có ý nghĩa
//   //         price:
//   //             '\$${Random().nextInt(100) + 1}.0', // Sử dụng hàm Random().nextInt để tạo giá ngẫu nhiên từ 1 đến 100
//   //       ),
//   //     );
//   //   }
//   //   // Cách truyền data tĩnh từ model "CategoryData"
//   //   _categoryDataList = [];
//   //   // Tạo một vòng lặp for từ 1 đến 10
//   //   for (int i = 1; i <= 10; i++) {
//   //     // Thêm một đối tượng CategoryData vào danh sách với các thuộc tính ngẫu nhiên
//   //     _categoryDataList.add(
//   //       CategoryData(
//   //         categoryName: 'Category $i',
//   //         categoryImage: '/temp_restaurant_2/food($i).png',
//   //         listMenu: _productDataList,
//   //       ),
//   //     );
//   //   }
//   // }

//   //Code trong Function chạy theo thứ tự từ trên xuống
//   void loadDataSample() {
//     // Cách truyền data tĩnh từ model "productData"
//     _productDataList1 = [
//       ProductData(
//         productName: 'Product 1',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$11.0',
//       ),
//       ProductData(
//         productName: 'Product 2',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$12.0',
//       ),
//       ProductData(
//         productName: 'Product 3',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$13.0',
//       ),
//       ProductData(
//         productName: 'Product 4',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$14.0',
//       ),
//       ProductData(
//         productName: 'Product 5',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$15.0',
//       ),
//       ProductData(
//         productName: 'Product 6',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$16.0',
//       ),
//       ProductData(
//         productName: 'Product 7',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$17.0',
//       ),
//       ProductData(
//         productName: 'Product 8',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$18.0',
//       ),
//       ProductData(
//         productName: 'Product 9',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$19.0',
//       ),
//       ProductData(
//         productName: 'Product 10',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$100.0',
//       ),
//     ];

//     _productDataList2 = [
//       ProductData(
//         productName: 'Product 2.1',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$21.0',
//       ),
//       ProductData(
//         productName: 'Product 2.2',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$22.0',
//       ),
//       ProductData(
//         productName: 'Product 2.3',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$23.0',
//       ),
//       ProductData(
//         productName: 'Product 2.4',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$24.0',
//       ),
//       ProductData(
//         productName: 'Product 2.5',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$25.0',
//       ),
//       ProductData(
//         productName: 'Product 2.6',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$26.0',
//       ),
//       ProductData(
//         productName: 'Product 2.7',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$27.0',
//       ),
//       ProductData(
//         productName: 'Product 2.8',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$28.0',
//       ),
//       ProductData(
//         productName: 'Product 2.9',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$29.0',
//       ),
//       ProductData(
//         productName: 'Product 2.10',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$200.0',
//       ),
//     ];

//     _productDataList3 = [
//       ProductData(
//         productName: 'Product 3.1',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$31.0',
//       ),
//       ProductData(
//         productName: 'Product 3.2',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$32.0',
//       ),
//       ProductData(
//         productName: 'Product 3.3',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$33.0',
//       ),
//       ProductData(
//         productName: 'Product 3.4',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$34.0',
//       ),
//       ProductData(
//         productName: 'Product 3.5',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$35.0',
//       ),
//       ProductData(
//         productName: 'Product 3.6',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$36.0',
//       ),
//       ProductData(
//         productName: 'Product 3.7',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$37.0',
//       ),
//       ProductData(
//         productName: 'Product 3.8',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$38.0',
//       ),
//       ProductData(
//         productName: 'Product 3.9',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$39.0',
//       ),
//       ProductData(
//         productName: 'Product 3.10',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$300.0',
//       ),
//     ];

//     _productDataList4 = [
//       ProductData(
//         productName: 'Product 4.1',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$31.0',
//       ),
//       ProductData(
//         productName: 'Product 4.2',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$32.0',
//       ),
//       ProductData(
//         productName: 'Product 4.3',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$33.0',
//       ),
//       ProductData(
//         productName: 'Product 4.4',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$34.0',
//       ),
//       ProductData(
//         productName: 'Product 4.5',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$35.0',
//       ),
//       ProductData(
//         productName: 'Product 4.6',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$36.0',
//       ),
//       ProductData(
//         productName: 'Product 4.7',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$37.0',
//       ),
//       ProductData(
//         productName: 'Product 4.8',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$38.0',
//       ),
//       ProductData(
//         productName: 'Product 4.9',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$39.0',
//       ),
//       ProductData(
//         productName: 'Product 4.10',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$300.0',
//       ),
//     ];

//     _productDataList5 = [
//       ProductData(
//         productName: 'Product 5.1',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$31.0',
//       ),
//       ProductData(
//         productName: 'Product 5.2',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$32.0',
//       ),
//       ProductData(
//         productName: 'Product 5.3',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$33.0',
//       ),
//       ProductData(
//         productName: 'Product 5.5',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$35.0',
//       ),
//       ProductData(
//         productName: 'Product 5.5',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$35.0',
//       ),
//       ProductData(
//         productName: 'Product 5.6',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$36.0',
//       ),
//       ProductData(
//         productName: 'Product 5.7',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$37.0',
//       ),
//       ProductData(
//         productName: 'Product 5.8',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$38.0',
//       ),
//       ProductData(
//         productName: 'Product 5.9',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$39.0',
//       ),
//       ProductData(
//         productName: 'Product 5.10',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$300.0',
//       ),
//     ];

//     _productDataList6 = [
//       ProductData(
//         productName: 'Product 6.1',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$31.0',
//       ),
//       ProductData(
//         productName: 'Product 6.2',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$32.0',
//       ),
//       ProductData(
//         productName: 'Product 6.3',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$33.0',
//       ),
//       ProductData(
//         productName: 'Product 6.4',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$34.0',
//       ),
//       ProductData(
//         productName: 'Product 6.5',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$35.0',
//       ),
//       ProductData(
//         productName: 'Product 6.6',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$36.0',
//       ),
//       ProductData(
//         productName: 'Product 6.7',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$37.0',
//       ),
//       ProductData(
//         productName: 'Product 6.8',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$38.0',
//       ),
//       ProductData(
//         productName: 'Product 6.9',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$39.0',
//       ),
//       ProductData(
//         productName: 'Product 6.10',
//         productDescription:
//             'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
//         price: '\$300.0',
//       ),
//     ];

//     // Cách truyền data tĩnh từ model "CategoryData"
//     _categoryDataList = [
//       CategoryData(
//         categoryName: 'Category 1',
//         categoryImage: '/temp_restaurant_2/food(1).png',
//         listMenu: _productDataList1,
//       ),
//       CategoryData(
//         categoryName: 'Category 2',
//         categoryImage: '/temp_restaurant_2/food(2).png',
//         listMenu: _productDataList2, //Danh sách sản phẩm số 2
//       ),
//       CategoryData(
//         categoryName: 'Category 3',
//         categoryImage: '/temp_restaurant_2/food(3).png',
//         listMenu: _productDataList3, //Danh sách sản phẩm số 3
//       ),
//       CategoryData(
//         categoryName: 'Category 4',
//         categoryImage: '/temp_restaurant_2/food(4).png',
//         listMenu: _productDataList4,
//       ),
//       CategoryData(
//         categoryName: 'Category 5',
//         categoryImage: '/temp_restaurant_2/food(5).png',
//         listMenu: _productDataList5,
//       ),
//       CategoryData(
//         categoryName: 'Category 6',
//         categoryImage: '/temp_restaurant_2/food(6).png',
//         listMenu: _productDataList6,
//       ),
//     ];
//   }
// }
