class JsonDataTemplate {
  // [112315TIN] double check: oke
  // [112308TIN] check ratio: 16:9, 16:10, 4:3, 21:9,
  static final temprestaurant6 = {
    "_id": "id_tem16",
    "templateSlug": "temp_restaurant_6",
    "ratioSetting": {
      // not use textStyle for this template
      // "textStyle": {},
      "renderScreen": {
        "32/9": {
          "categoryColumnVerticalPadding": "6.sp",
          "categoryNameFontSize": "4.sp",
          "productPriceFontSize": "2.8.sp",
          "productNameFontSize": "2.8.sp",
          "imageTitleGap": "2.sp",
          "titleProductGap": "2.sp",
          "productGap": "1.sp",
          "categoryGap": "4.sp",
          "heightBottomLeftImage": "25.w",
          "widthBottomLeftImage": "25.w",
          "leftPanelWidth": "35.w",
          "categoryImageHeight": "20.sp",
          "columnGap": "25.sp",
          "brandTopPadding": "3.sp",
          "brandLeftPadding": "5.sp",
        },
        "21/9": {
          "categoryColumnVerticalPadding": "6.sp",
          "categoryNameFontSize": "4.sp",
          "productPriceFontSize": "2.8.sp",
          "productNameFontSize": "2.8.sp",
          "imageTitleGap": "2.sp",
          "titleProductGap": "2.sp",
          "productGap": "1.sp",
          "categoryGap": "4.sp",
          "heightBottomLeftImage": "25.w",
          "widthBottomLeftImage": "25.w",
          "leftPanelWidth": "35.w",
          "categoryImageHeight": "20.sp",
          "columnGap": "25.sp",
          "brandTopPadding": "18.sp",
          "brandLeftPadding": "5.sp",
          "brandFontSize": "18.sp",
        },
        "16/9": {
          "categoryColumnVerticalPadding": "10.sp",
          "categoryNameFontSize": "4.6.sp",
          "productPriceFontSize": "3.6.sp",
          "productNameFontSize": "3.6.sp",
          "imageTitleGap": "2.sp",
          "titleProductGap": "2.sp",
          "productGap": "1.sp",
          "categoryGap": "7.4.sp",
          "heightBottomLeftImage": "35.w",
          "widthBottomLeftImage": "35.w",
          "leftPanelWidth": "35.w",
          "categoryImageHeight": "30.sp",
          "columnGap": "15.sp",
          "brandTopPadding": "22.sp",
          "brandLeftPadding": "5.sp",
          "brandFontSize": "18.sp",
        },
        "16/10": {
          "categoryColumnVerticalPadding": "12.sp",
          "categoryNameFontSize": "5.8.sp",
          "productPriceFontSize": "4.4.sp",
          "productNameFontSize": "4.4.sp",
          "imageTitleGap": "2.5.sp",
          "titleProductGap": "2.5.sp",
          "productGap": "1.2.sp",
          "categoryGap": "10.sp",
          "heightBottomLeftImage": "35.w",
          "widthBottomLeftImage": "35.w",
          "leftPanelWidth": "35.w",
          "categoryImageHeight": "30.sp",
          "columnGap": "15.sp",
          "brandTopPadding": "32.sp",
          "brandLeftPadding": "5.sp",
          "brandFontSize": "18.sp",
        },
        "4/3": {
          "categoryColumnVerticalPadding": "20.sp",
          "categoryNameFontSize": "5.8.sp",
          "productPriceFontSize": "4.8.sp",
          "productNameFontSize": "4.8.sp",
          "imageTitleGap": "2.5.sp",
          "titleProductGap": "2.5.sp",
          "productGap": "1.6.sp",
          "categoryGap": "20.sp",
          "heightBottomLeftImage": "35.w",
          "widthBottomLeftImage": "35.w",
          "leftPanelWidth": "35.w",
          "categoryImageHeight": "30.sp",
          "columnGap": "15.sp",
          "brandTopPadding": "38.sp",
          "brandLeftPadding": "5.sp",
          "brandFontSize": "18.sp",
        },
        "default": {
          "categoryColumnVerticalPadding": "10.sp",
          "categoryNameFontSize": "4.2.sp",
          "productPriceFontSize": "3.2.sp",
          "productNameFontSize": "3.2.sp",
          "imageTitleGap": "2.sp",
          "titleProductGap": "2.sp",
          "productGap": "1.sp",
          "categoryGap": "10.sp",
          "heightBottomLeftImage": "35.w",
          "widthBottomLeftImage": "35.w",
          "leftPanelWidth": "35.w",
          "categoryImageHeight": "30.sp",
          "columnGap": "15.sp",
          "brandTopPadding": "5.sp",
          "brandLeftPadding": "5.sp",
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "",
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "",
      },

      //Danh mục 1: pro 1, 2 ,3, 4
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$40.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Danh mục 2: pro 5, 6, 7, 8
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$50.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$60.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$70.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Danh mục 3: pro 9, 10, 11, 12
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "\$10.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "\$41.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Danh mục 4: pro  13, 14, 15, 16
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "\$42.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "\$43.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "\$16.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Hình background
      "imageBackground": {
        "key": "imageBackground",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/background.webp",
      },

      //Hình góc trái bên dưới của menu
      "bottomLeftImage": {
        "key": "bottomLeftImage",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/bottomleftimage.webp",
      },

      //Hình viền của bottomLeftImage
      "bottomLeftImageBorder": {
        "key": "bottomLeftImageBorder",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/deco-3-1.png",
      },

      // Hình deco 1
      "decoImage1": {
        "key": "decoImage1",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/s-5.webp",
      },

      // Hình deco 2
      "decoImage2": {
        "key": "decoImage2",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/cbbc3c56-9f80-46a1-a0bd-65d3996b1344.png",
      },

      // Hình deco 3
      "decoImage3": {
        "key": "decoImage3",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/2a16a3c9-3c54-4b33-9a0d-4d4fe3528aa4.png",
      },

      // Hình deco 4
      "decoImage4": {
        "key": "decoImage4",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/t.webp",
      },

      // Hình deco 5
      "decoImage5": {
        "key": "decoImage5",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/deco-5-1.png",
      },

      // Hình deco 6
      "decoImage6": {
        "key": "decoImage6",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/deco-4-1.png",
      },

      // Discount background
      "discountBg": {
        "key": "discountBg",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/deco-1-1.png",
      },

      // image background title
      "imageBackgroundTitle": {
        "key": "imageBackgroundTitle",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/deco-2-1.png",
      },

      //Hình danh mục 1
      "categoryImage1": {
        "key": "categoryImage1",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/category_1.webp",
      },

      //Hình danh mục 2
      "categoryImage2": {
        "key": "categoryImage2",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/category_2.webp",
      },
      //Hình danh mục 3
      "categoryImage3": {
        "key": "categoryImage3",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/category_3.webp",
      },
      //Hình danh mục 4
      "categoryImage4": {
        "key": "categoryImage4",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/category_4.webp",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgetSetting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image":
              "imageBackground", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
          "boxFit": "fill",
        },
        "width": "100.w",
        "child": {
          "key": "row",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "mainAxisAlignment": "center",
            "children": [
              // Left panel
              {
                "key": "SizedBoxByRatio-c1",
                "widgetName": "SizedBoxByRatio",
                "widgetSetting": {
                  "widthFromFieldKey": "leftPanelWidth",
                  "child": {
                    "key": "Stack-bottomleft",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        // Bottom left image and discount
                        {
                          "key": "Positioned-bottomleft",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "left": "-5.w",
                            "bottom": "-5.w",
                            "child": {
                              "key": "SizedBoxByRatio-bottomleft",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "heightBottomLeftImage",
                                "widthFromFieldKey": "widthBottomLeftImage",
                                "child": {
                                  "key": "Stack-bottomleft",
                                  "widgetName": "Stack",
                                  "widgetSetting": {
                                    "children": [
                                      // Bottom left image
                                      {
                                        "key":
                                            "SizedBoxByRatio-inner-bottomleft",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "heightBottomLeftImage",
                                          "widthFromFieldKey":
                                              "widthBottomLeftImage",
                                          "child": {
                                            "key": "FramedImage",
                                            "widgetName": "FramedImage",
                                            "widgetSetting": {
                                              "imageDataKey": "bottomLeftImage",
                                              "imageFrameDataKey":
                                                  "bottomLeftImageBorder",
                                              "borderWidth": "4.sp",
                                              "borderRadiusOfImage": "35.w",
                                            },
                                          }
                                        }
                                      },
                                      // Discount
                                      {
                                        "key": "Positioned-discount",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "top": "0",
                                          "right": "0",
                                          "child": {
                                            "key": "Container-discount",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "height": "35.sp",
                                              "width": "35.sp",
                                              "decoration": {
                                                "image": "discountBg",
                                              },
                                              "child": {
                                                "key": "Column-discount",
                                                "widgetName": "Column",
                                                "widgetSetting": {
                                                  "mainAxisAlignment": "center",
                                                  "children": [
                                                    {
                                                      "key": "Text-discount1",
                                                      "widgetName": "Text",
                                                      "widgetSetting": {
                                                        "data": "All item",
                                                        "style": {
                                                          "fontWeight": "w600",
                                                          "fontFamily":
                                                              "Poppins",
                                                          "color": "0xff000000",
                                                          "fontSize": "4.sp",
                                                        }
                                                      }
                                                    },
                                                    {
                                                      "key": "Text-discount1",
                                                      "widgetName": "Text",
                                                      "widgetSetting": {
                                                        "data": "20%",
                                                        "style": {
                                                          "fontWeight": "w900",
                                                          "fontFamily":
                                                              "Poppins",
                                                          "color": "0xff000000",
                                                          "fontSize": "10.sp",
                                                        }
                                                      }
                                                    },
                                                    {
                                                      "key": "Text-discount1",
                                                      "widgetName": "Text",
                                                      "widgetSetting": {
                                                        "data": "Disc.",
                                                        "style": {
                                                          "fontWeight": "w600",
                                                          "fontFamily":
                                                              "Poppins",
                                                          "color": "0xff000000",
                                                          "fontSize": "4.sp",
                                                        }
                                                      }
                                                    },
                                                  ],
                                                },
                                              }
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  },
                                }
                              },
                            }
                          }
                        },
                        // Brand
                        {
                          "key": "Positioned-brand",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "left": "0",
                            "top": "0",
                            "child": {
                              "key": "",
                              "widgetName": "TransformRotate",
                              "widgetSetting": {
                                "angle": "-0.05",
                                "child": {
                                  "key": "",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "topPaddingFieldKey": "brandTopPadding",
                                    "leftPaddingFieldKey": "brandLeftPadding",
                                    "child": {
                                      "key": "Container-brand",
                                      "widgetName": "Container",
                                      "widgetSetting": {
                                        // "color": "0x50ff0000",
                                        "height": "60.sp",
                                        "child": {
                                          "key": "Column-brand",
                                          "widgetName": "Column",
                                          "widgetSetting": {
                                            "mainAxisAlignment": "start",
                                            "crossAxisAlignment": "start",
                                            "children": [
                                              {
                                                "key": "Text-discount1",
                                                "widgetName": "Text",
                                                "widgetSetting": {
                                                  "data": "Restaurant",
                                                  "style": {
                                                    "fontWeight": "w800",
                                                    "fontFamily": "Arima",
                                                    "color": "0xffffffff",
                                                    "fontStyle": "italic",
                                                    // "fontSize": "20.sp",
                                                    "height": "1",
                                                    "letterSpacing": "2",
                                                    "shadows": [
                                                      {
                                                        "color": "0xFFFE9717",
                                                        "offset": {
                                                          "dx": "-0.4.h",
                                                          "dy": "0.4.h"
                                                        },
                                                      },
                                                    ]
                                                  },
                                                  "fontSizeFromFieldKey":
                                                      "brandFontSize",
                                                }
                                              },
                                              {
                                                "key": "Text-discount1",
                                                "widgetName": "Text",
                                                "widgetSetting": {
                                                  "data": "Brand",
                                                  "style": {
                                                    "fontWeight": "w800",
                                                    "fontFamily": "Arima",
                                                    "height": "1",
                                                    "fontStyle": "italic",
                                                    "color": "0xFFFE9717",
                                                    // "fontSize": "20.sp",
                                                    "shadows": [
                                                      {
                                                        "color": "0xFFFFFFFF",
                                                        "offset": {
                                                          "dx": "-0.4.h",
                                                          "dy": "0.4.h"
                                                        },
                                                      },
                                                    ]
                                                  },
                                                  "fontSizeFromFieldKey":
                                                      "brandFontSize",
                                                }
                                              }
                                            ],
                                          },
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        },
                      ]
                    }
                  }
                },
              },
              // Spacing
              {
                "key": "SizedBoxByRatio-c2",
                "widgetName": "SizedBoxByRatio",
                "widgetSetting": {
                  "widthFromFieldKey": "columnGap",
                },
              },
              // Category 1, 2
              {
                "key": "Expanded-c3",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "PaddingByRatio-c3",
                    "widgetName": "PaddingByRatio",
                    "widgetSetting": {
                      "verticalPaddingFieldKey":
                          "categoryColumnVerticalPadding",
                      "child": {
                        "key": "Column-c3",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "crossAxisAlignment": "start",
                          "children": [
                            // Category Image 1
                            {
                              "key": "SizedBoxByRatio-c1i1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "categoryImageHeight",
                                "width": "maxFinite",
                                "child": {
                                  "key": "Stack-c1i1",
                                  "widgetName": "Stack",
                                  "widgetSetting": {
                                    "children": [
                                      // Deco 1
                                      {
                                        "key": "Positioned-c1d1",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "right": "10.sp",
                                          "child": {
                                            "key": "SizedBox-c1d1",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "25.sp",
                                              "child": {
                                                "key": "SimpleImage-c1d1",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage1",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Deco 2
                                      {
                                        "key": "Positioned-c1d2",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "right": "20.sp",
                                          "bottom": "0",
                                          "child": {
                                            "key": "SizedBox-c1d2",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "5.sp",
                                              "child": {
                                                "key": "SimpleImage-c1d2",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage2",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Deco 3
                                      {
                                        "key": "Positioned-c1d3",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "right": "18.sp",
                                          "top": "0",
                                          "child": {
                                            "key": "SizedBox-c1d3",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "10.sp",
                                              "child": {
                                                "key": "SimpleImage-c1d3",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage3",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Deco 4
                                      {
                                        "key": "Positioned-c1d4",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "right": "15.sp",
                                          "bottom": "10.sp",
                                          "child": {
                                            "key": "SizedBox-c1d4",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "5.sp",
                                              "child": {
                                                "key": "SimpleImage-c1d4",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage4",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Category image 1
                                      {
                                        "key": "SimpleImage-c1",
                                        "widgetName": "SimpleImage",
                                        "widgetSetting": {
                                          "imageDataKey": "categoryImage1",
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "imageTitleGap"
                              }
                            },
                            // Category title 1
                            {
                              "key": "Container-c1c1",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "10.sp",
                                "width": "18.w",
                                "padding": {"bottom": "1.sp"},
                                "decoration": {
                                  "image": "imageBackgroundTitle",
                                  "boxFit": "contain",
                                },
                                "child": {
                                  "key": "Center-c1c1",
                                  "widgetName": "Center",
                                  "widgetSetting": {
                                    "child": {
                                      "key": "Text-c1c1",
                                      "widgetName": "Text",
                                      "widgetSetting": {
                                        "dataFromSetting": "category1.name",
                                        "style": {
                                          "color": "0xffffffff",
                                          "fontFamily": "Knewave",
                                          "letterSpacing": "2",
                                        },
                                        "fontSizeFromFieldKey":
                                            "categoryNameFontSize"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "titleProductGap"
                              }
                            },
                            // product 1
                            {
                              "key": "ProductRow1-1",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product1",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 2
                            {
                              "key": "ProductRow1-2",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product2",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 3
                            {
                              "key": "ProductRow1-3",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product3",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 4
                            {
                              "key": "ProductRow1-4",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product4",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "categoryGap"
                              }
                            },

                            // Category Image 2
                            {
                              "key": "SizedBoxByRatio-c1i2",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "categoryImageHeight",
                                "width": "maxFinite",
                                "child": {
                                  "key": "Stack-c1i2",
                                  "widgetName": "Stack",
                                  "widgetSetting": {
                                    "children": [
                                      // Deco 1

                                      {
                                        "key": "Positioned-c2d1",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "right": "10.sp",
                                          "child": {
                                            "key": "SizedBox-c2d1",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "25.sp",
                                              "child": {
                                                "key": "SimpleImage-c2d1",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage1",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Deco 2
                                      {
                                        "key": "Positioned-c2d2",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "right": "20.sp",
                                          "bottom": "0",
                                          "child": {
                                            "key": "SizedBox-c2d2",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "5.sp",
                                              "child": {
                                                "key": "SimpleImage-c2d2",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage2",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Deco 3
                                      {
                                        "key": "Positioned-c2d3",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "right": "18.sp",
                                          "top": "0",
                                          "child": {
                                            "key": "SizedBox-c2d3",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "10.sp",
                                              "child": {
                                                "key": "SimpleImage-c2d3",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage3",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Deco 4
                                      {
                                        "key": "Positioned-c2d4",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "right": "15.sp",
                                          "bottom": "10.sp",
                                          "child": {
                                            "key": "SizedBox-c2d4",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "5.sp",
                                              "child": {
                                                "key": "SimpleImage-c2d4",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage4",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Category image 2
                                      {
                                        "key": "SimpleImage-c2",
                                        "widgetName": "SimpleImage",
                                        "widgetSetting": {
                                          "imageDataKey": "categoryImage2",
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            },
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "imageTitleGap"
                              }
                            },
                            // Category title 2
                            {
                              "key": "Container-c1c2",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "10.sp",
                                "width": "18.w",
                                "padding": {"bottom": "1.sp"},
                                "decoration": {
                                  "image": "imageBackgroundTitle",
                                  "boxFit": "contain",
                                },
                                "child": {
                                  "key": "Center-c1c1",
                                  "widgetName": "Center",
                                  "widgetSetting": {
                                    "child": {
                                      "key": "Text-c1c2",
                                      "widgetName": "Text",
                                      "widgetSetting": {
                                        "dataFromSetting": "category2.name",
                                        "style": {
                                          "color": "0xffffffff",
                                          "fontFamily": "Knewave",
                                          "letterSpacing": "2",
                                        },
                                        "fontSizeFromFieldKey":
                                            "categoryNameFontSize"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Spacing

                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "titleProductGap"
                              }
                            },
                            // product 5
                            {
                              "key": "ProductRow1-5",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product5",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 6
                            {
                              "key": "ProductRow1-6",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product6",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 7
                            {
                              "key": "ProductRow1-7",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product7",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 8
                            {
                              "key": "ProductRow1-8",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product8",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                          ]
                        },
                      }
                    },
                  },
                }
              },
              // Spacing
              {
                "key": "SizedBoxByRatio-c2",
                "widgetName": "SizedBoxByRatio",
                "widgetSetting": {
                  // "width": "2.w",
                  "widthFromFieldKey": "columnGap",
                },
              },
              // Category 3, 4
              {
                "key": "Expanded-c5",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "PaddingByRatio-c5",
                    "widgetName": "PaddingByRatio",
                    "widgetSetting": {
                      "verticalPaddingFieldKey":
                          "categoryColumnVerticalPadding",
                      "child": {
                        "key": "Column-c1",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "crossAxisAlignment": "start",
                          "children": [
                            // Category Image 1
                            {
                              "key": "SizedBoxByRatio-c2i3",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "categoryImageHeight",
                                "width": "maxFinite",
                                "child": {
                                  "key": "Stack-c2i3",
                                  "widgetName": "Stack",
                                  "widgetSetting": {
                                    "children": [
                                      // Deco 1
                                      {
                                        "key": "Positioned-c2d1",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "right": "10.sp",
                                          "child": {
                                            "key": "SizedBox-c2d1",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "25.sp",
                                              "child": {
                                                "key": "SimpleImage-c2d1",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage1",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Deco 2
                                      {
                                        "key": "Positioned-c2d2",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "left": ".10.sp",
                                          "bottom": "0",
                                          "child": {
                                            "key": "SizedBox-c2d2",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "25.sp",
                                              "child": {
                                                "key": "SimpleImage-c2d2",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage1",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Category image 3
                                      {
                                        "key": "Positioned-c3",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "left": "0",
                                          "right": "0",
                                          "child": {
                                            "key": "SizedBoxByRatio-inner-c3",
                                            "widgetName": "SizedBoxByRatio",
                                            "widgetSetting": {
                                              "heightFromFieldKey":
                                                  "categoryImageHeight",
                                              "child": {
                                                "key": "SimpleImage-c3",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey":
                                                      "categoryImage3",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                    ]
                                  }
                                }
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "imageTitleGap"
                              }
                            },
                            // Category title 3
                            {
                              "key": "Container-c1c1",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "10.sp",
                                "width": "18.w",
                                "padding": {"bottom": "1.sp"},
                                "decoration": {
                                  "image": "imageBackgroundTitle",
                                  "boxFit": "contain",
                                },
                                "child": {
                                  "key": "Center-c1c1",
                                  "widgetName": "Center",
                                  "widgetSetting": {
                                    "child": {
                                      "key": "Text-c1c1",
                                      "widgetName": "Text",
                                      "widgetSetting": {
                                        "dataFromSetting": "category3.name",
                                        "style": {
                                          "color": "0xffffffff",
                                          "fontFamily": "Knewave",
                                          "letterSpacing": "2",
                                        },
                                        "fontSizeFromFieldKey":
                                            "categoryNameFontSize"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "titleProductGap"
                              }
                            },
                            // product 9
                            {
                              "key": "ProductRow1-9",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product9",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 10
                            {
                              "key": "ProductRow1-10",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product10",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 11
                            {
                              "key": "ProductRow1-11",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product11",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 12
                            {
                              "key": "ProductRow1-12",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product12",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "categoryGap"
                              }
                            },

                            // Category Image 4
                            {
                              "key": "SizedBoxByRatio-c1i2",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "categoryImageHeight",
                                "width": "maxFinite",
                                "child": {
                                  "key": "Stack-c1i2",
                                  "widgetName": "Stack",
                                  "widgetSetting": {
                                    "children": [
                                      // Deco 1
                                      {
                                        "key": "Positioned-c2d1",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "right": "10.sp",
                                          "child": {
                                            "key": "SizedBox-c2d1",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "25.sp",
                                              "child": {
                                                "key": "SimpleImage-c2d1",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage1",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Deco 2
                                      {
                                        "key": "Positioned-c2d2",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "left": "10.sp",
                                          "bottom": "0",
                                          "child": {
                                            "key": "SizedBox-c2d2",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {
                                              "height": "25.sp",
                                              "child": {
                                                "key": "SimpleImage-c2d2",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "decoImage1",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                      // Category image 4
                                      {
                                        "key": "Positioned-c2d2",
                                        "widgetName": "Positioned",
                                        "widgetSetting": {
                                          "left": "0",
                                          "right": "0",
                                          "child": {
                                            "key": "SizedBoxByRatio-inner-c4",
                                            "widgetName": "SizedBoxByRatio",
                                            "widgetSetting": {
                                              "heightFromFieldKey":
                                                  "categoryImageHeight",
                                              "child": {
                                                "key": "SimpleImage-c2",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey":
                                                      "categoryImage4",
                                                }
                                              }
                                            }
                                          }
                                        },
                                      },
                                    ]
                                  }
                                }
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "imageTitleGap"
                              }
                            },
                            // Category image 4
                            {
                              "key": "Container-c1c2",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "10.sp",
                                "width": "18.w",
                                "padding": {"bottom": "1.sp"},
                                "decoration": {
                                  "image": "imageBackgroundTitle",
                                  "boxFit": "contain",
                                },
                                "child": {
                                  "key": "Center-c1c1",
                                  "widgetName": "Center",
                                  "widgetSetting": {
                                    "child": {
                                      "key": "Text-c1c2",
                                      "widgetName": "Text",
                                      "widgetSetting": {
                                        "dataFromSetting": "category4.name",
                                        "style": {
                                          "color": "0xffffffff",
                                          "fontFamily": "Knewave",
                                          "letterSpacing": "2",
                                        },
                                        "fontSizeFromFieldKey":
                                            "categoryNameFontSize"
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "titleProductGap"
                              }
                            },
                            // product 13
                            {
                              "key": "ProductRow1-13",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product13",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 14
                            {
                              "key": "ProductRow1-14",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product14",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 15
                            {
                              "key": "ProductRow1-15",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product15",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                            // Spacing
                            {
                              "key": "SizedBoxByRatio-gap1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productGap"
                              }
                            },
                            // product 16
                            {
                              "key": "ProductRow1-16",
                              "widgetName": "ProductRow1",
                              "widgetSetting": {
                                "product": "product16",
                                "border": {
                                  "all": {"color": "0x00000000"}
                                },
                                "productNameTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff"
                                },
                                "productPriceTextStyle": {
                                  "fontWeight": "w400",
                                  "fontFamily": "Poppins",
                                  "color": "0xffffffff",
                                },
                                "prefixProductPrice": "",
                                "productPriceFontSizeFromFieldKey":
                                    "productPriceFontSize",
                                "productNameFontSizeFromFieldKey":
                                    "productNameFontSize",
                              }
                            },
                          ]
                        },
                      }
                    },
                  },
                }
              },
              // Spacing
              {
                "key": "SizedBoxByRatio-c2",
                "widgetName": "SizedBoxByRatio",
                "widgetSetting": {
                  "widthFromFieldKey": "columnGap",
                },
              },
            ]
          }
        }
      }
    }
  };
}
