class JsonDataTemplate {
  //[092328HG] Check temppho3: Run OK - Ratio OK
  static final temppho3 = {
    "_id": "",
    "templateSlug": "temp_pho_3",
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
        "21/9": {
          "categoryFontSize": "3.5.sp",
          "verticalFramePadding": "2.sp",
          "horizontalFramePadding": "2.sp",
          "categoryBlockHeight": "25.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
        "16/9": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
        "16/10": {
          "categoryFontSize": "6.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "23.h",
          "brandFontSize": "12.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
        },
        "4/3": {
          "categoryFontSize": "7.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "23.h",
          "brandFontSize": "15.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp",
        },
        "default": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
      },
    },
    "isDeleted": false,
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "",
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "",
      },
      "category5": {
        "key": "category5",
        "type": "category",
        "name": "Category 5",
        "image": "",
      },
      "category6": {
        "key": "category6",
        "type": "category",
        "name": "Category 6",
        "image": "",
      },
      //Danh mục 1: pro 1, 2, 3, 4, 5,
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 2: pro  6, 7, 8, 9, 10,
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 3: pro 11, 12, 13, 14, 15
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 4: 16, 17, 18, 19, 20
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 5: pro 21, 22, 23, 24, 25,
      "product21": {
        "key": "product21",
        "type": "product",
        "name": "Product 21",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product22": {
        "key": "product22",
        "type": "product",
        "name": "Product 22",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product23": {
        "key": "product23",
        "type": "product",
        "name": "Product 23",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product24": {
        "key": "product24",
        "type": "product",
        "name": "Product 24",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product25": {
        "key": "product25",
        "type": "product",
        "name": "Product 25",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 6: pro 26, 27, 28, 29, 30
      "product26": {
        "key": "product26",
        "type": "product",
        "name": "Product 26",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product27": {
        "key": "product27",
        "type": "product",
        "name": "Product 27",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product28": {
        "key": "product28",
        "type": "product",
        "name": "Product 28",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product29": {
        "key": "product29",
        "type": "product",
        "name": "Product 29",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product30": {
        "key": "product30",
        "type": "product",
        "name": "Product 30",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "leftTopDecoImage": {
        "key": "leftTopDecoImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/left_top_deco.png"
      },
      "rightTopDecoImage": {
        "key": "rightTopDecoImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/right_top_deco.png"
      },
      "bottomDecoImage": {
        "key": "bottomDecoImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/bottom_deco.png"
      },
      "cat1Image": {
        "key": "cat1Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1-1.jpg"
      },
      "cat2Image": {
        "key": "cat2Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2-1.jpg"
      },
      "cat3Image": {
        "key": "cat3Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3-1.jpg"
      },
      "circleBorderImage": {
        "key": "circleBorderImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/circle_border.png"
      }
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Stack",
      "widgetSetting": {
        "children": [
          // menu
          {
            "key": "Container-menu",
            "widgetName": "Container",
            "widgetSetting": {
              "height": "100.h",
              "width": "100.w",
              // bg color - blue
              "decoration": {"color": "0xff0d507e"},

              "child": {
                "key": "PaddingByRatio-menu",
                "widgetName": "PaddingByRatio",
                "widgetSetting": {
                  "verticalPaddingFieldKey": "verticalFramePadding",
                  "horizontalPaddingFieldKey": "horizontalFramePadding",
                  "child": {
                    "key": "Column-menu",
                    "widgetName": "Column",
                    "widgetSetting": {
                      "mainAxisAlignment": "center",
                      "children": [
                        // brand text
                        {
                          "key": "Text-brand",
                          "widgetName": "Text",
                          "widgetSetting": {
                            "data": "PHO BRAND",
                            "fontSizeFromFieldKey": "brandFontSize",
                            "style": {
                              "fontFamily": "Staatliches",
                              "color": "0xffffaf45",
                              // "fontSize": "10.sp",
                            }
                          }
                        },
                        // menu items
                        {
                          "key": "Expanded-menu-items",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "key": "Wrap-menu-items",
                              "widgetName": "Wrap",
                              "widgetSetting": {
                                "direction": "vertical",
                                "spacing": "5.sp",
                                "runSpacing": "10.sp",
                                "clipBehavior": "hardEdge",
                                "children": [
                                  // cat 1
                                  {
                                    "key": "CategoryBlock1-cat-1",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      // image cat 1
                                      "categoryImage": {
                                        "key": "FramedImage-cat-1",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat1Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category1",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        // "fontSize": "5.sp",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 1 - items
                                      "categoryItemRows": [
                                        // pro 1
                                        {
                                          "key": "Padding-p1",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p1",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product1",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 2
                                        {
                                          "key": "Padding-p2",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p2",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product2",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 3
                                        {
                                          "key": "Padding-p3",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p3",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product3",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 4
                                        {
                                          "key": "Padding-p4",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p4",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product4",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 5
                                        {
                                          "key": "Padding-p5",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p5",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product5",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  },

                                  // cat 2
                                  {
                                    "key": "CategoryBlock1-cat-2",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "22.h",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      // image cat 2
                                      "categoryImage": {
                                        "key": "FramedImage-cat-2",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat2Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category2",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        // "fontSize": "5.sp",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 2 - items
                                      "categoryItemRows": [
                                        // pro 6
                                        {
                                          "key": "Padding-p6",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p6",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product6",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 7
                                        {
                                          "key": "Padding-p7",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p7",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product7",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 8
                                        {
                                          "key": "Padding-p8",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p8",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product8",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 9
                                        {
                                          "key": "Padding-p9",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p9",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product9",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 10
                                        {
                                          "key": "Padding-p10",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p10",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product10",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  },
                                  // cat 3
                                  {
                                    "key": "CategoryBlock1-cat-3",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "22.h",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      // image cat 3
                                      "categoryImage": {
                                        "key": "FramedImage-cat-3",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat3Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category3",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 3 - items
                                      "categoryItemRows": [
                                        // pro 11
                                        {
                                          "key": "Padding-p11",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p11",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product11",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 12
                                        {
                                          "key": "Padding-p12",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p12",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product12",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 13
                                        {
                                          "key": "Padding-p13",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p13",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product13",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 14
                                        {
                                          "key": "Padding-p14",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p14",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product14",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 15
                                        {
                                          "key": "Padding-p15",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p15",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product15",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  },

                                  // cat 4
                                  {
                                    "key": "CategoryBlock1-cat-4",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "22.h",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",

                                      // image cat 4
                                      "categoryImage": {
                                        "key": "FramedImage-cat-4",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat3Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category4",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        // "fontSize": "5.sp",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 4 - items
                                      "categoryItemRows": [
                                        // pro 16
                                        {
                                          "key": "Padding-p16",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p16",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product16",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 17
                                        {
                                          "key": "Padding-p17",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p17",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product17",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 18
                                        {
                                          "key": "Padding-p18",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p18",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product18",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 19
                                        {
                                          "key": "Padding-p19",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p19",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product19",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 20
                                        {
                                          "key": "Padding-p20",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p20",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product20",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  },

                                  // cat 5
                                  {
                                    "key": "CategoryBlock1-cat-5",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "22.h",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      // image cat 5
                                      "categoryImage": {
                                        "key": "FramedImage-cat-5",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat1Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category5",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        // "fontSize": "5.sp",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 5 - items
                                      "categoryItemRows": [
                                        // pro 21
                                        {
                                          "key": "Padding-p21",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p21",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product21",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 22
                                        {
                                          "key": "Padding-p22",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p22",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product22",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 23
                                        {
                                          "key": "Padding-p23",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p23",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product23",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 24
                                        {
                                          "key": "Padding-p24",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p24",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product24",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 25
                                        {
                                          "key": "Padding-p25",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p25",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product25",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  },
                                  // cat 6
                                  {
                                    "key": "CategoryBlock1-cat-6",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "22.h",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      // image cat 6
                                      "categoryImage": {
                                        "key": "FramedImage-cat-6",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat2Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category6",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        // "fontSize": "5.sp",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 6 - items
                                      "categoryItemRows": [
                                        // pro 26
                                        {
                                          "key": "Padding-p26",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p26",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product26",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 27
                                        {
                                          "key": "Padding-p27",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p27",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product27",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 28
                                        {
                                          "key": "Padding-p28",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p28",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product28",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 29
                                        {
                                          "key": "Padding-p29",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p29",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product29",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 30
                                        {
                                          "key": "Padding-p30",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p30",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product30",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              }
            },
          },
          // deco bottom
          {
            "key": "Positioned-bottom-deco",
            "widgetName": "Positioned",
            "widgetSetting": {
              "bottom": "-14.sp",
              "child": {
                "key": "Image-bottom-deco",
                "widgetName": "image",
                "widgetSetting": {
                  "imageId": "bottomDecoImage",
                  "fixCover": "true",
                  "width": "100.w",
                  "height": "100.h",
                }
              }
            }
          },
          // deco top left
          {
            "key": "Positioned-top-left-dec",
            "widgetName": "Positioned",
            "widgetSetting": {
              "left": "0",
              "child": {
                "key": "Image-top-left-deco",
                "widgetName": "image",
                "widgetSetting": {
                  "imageId": "leftTopDecoImage",
                  "fixCover": "false",
                  "width": "30.sp",
                  "height": "30.sp",
                }
              }
            }
          },
          // deco top right
          {
            "key": "Positioned-top-right-dec",
            "widgetName": "Positioned",
            "widgetSetting": {
              "right": "0",
              "child": {
                "key": "Image-top-right-deco",
                "widgetName": "image",
                "widgetSetting": {
                  "imageId": "rightTopDecoImage",
                  "fixCover": "false",
                  "width": "30.sp",
                  "height": "30.sp",
                }
              }
            }
          },
        ]
      }
    }
  };
}
