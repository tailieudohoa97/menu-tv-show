// [102325TIN] create template data to test
class JsonDataTemplate {
  //[092328HG] Check temprestaurant1: Run OK - Ratio OK
  static final temprestaurant1 = {
    "_id": "64ee744398c3d4605b477abc",
    "templateSlug": "temp_restaurant_1",
    "RatioSetting": {
      //092314LH Fix dynamic sample data to template
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        '21/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '0.9.w',
          'nameProductStyleMedium': '0.7.w',
          'priceProductStyle': '0.9.w',
          'priceProductStyleMedium': '0.7.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.5.w',
          'imageHeight': '13.h',
          'marginBottomImage': '1.5.w',
          'marginBottomProduct': '1.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        '16/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '1.5.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '16/10': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '1.7.w',
          'nameProductStyleMedium': '1.5.w',
          'priceProductStyle': '1.7.w',
          'priceProductStyleMedium': '1.5.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '18.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.5.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        '4/3': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '1.8.w',
          'nameProductStyleMedium': '1.5.w',
          'priceProductStyle': '1.8.w',
          'priceProductStyleMedium': '1.5.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.7.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '1.h',
          'paddingVerticalCategorTilte': '0',
          'paddingHorizontalCategorTilte': '0',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '14.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      //092314LH Fix dynamic sample data to template
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '/temp_restaurant_2/food(1).png',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '/temp_restaurant_2/food(2).png',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '/temp_restaurant_2/food(3).png',
      },
      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '/temp_restaurant_2/food(4).png',
      },
      'category5': {
        'key': 'category5',
        'type': 'category',
        'name': 'Category 5',
        'image': '/temp_restaurant_2/food(5).png',
      },
      //Danh mục 1: pro 1, 2 ,3
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 2: pro 4, 5, 6, 7, 8
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 2.1',
        'price': '\$21.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 2.2',
        'price': '\$22.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 2.3',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 2.4',
        'price': '\$24.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 2.5',
        'price': '\$25.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 3: pro 9, 10, 11
      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 3.1',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 3.2',
        'price': '\$32.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 3.3',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 4: pro 12, 13, 14
      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 4.1',
        'price': '\$41.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product13': {
        'key': 'product13',
        'type': 'product',
        'name': 'Product 4.2',
        'price': '\$42.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product14': {
        'key': 'product14',
        'type': 'product',
        'name': 'Product 4.3',
        'price': '\$43.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 5: pro 15, 16, 17
      'product15': {
        'key': 'product15',
        'type': 'product',
        'name': 'Product 5.1',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product16': {
        'key': 'product16',
        'type': 'product',
        'name': 'Product 5.2',
        'price': '\$41.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product17': {
        'key': 'product17',
        'type': 'product',
        'name': 'Product 5.3',
        'price': '\$51.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Hình background
      'imageBackground': {
        'key': 'imageBackground',
        'type': 'image',
        // 'image': '/temp_restaurant_1/BG.png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/BG.png',
      },
      //Hình phân tách cột
      'imageSeperatorLine': {
        'key': 'imageSeperatorLine',
        'type': 'image',
        'fixed': 'true',
        // 'image': '/temp_restaurant_1/decorate/Seperator-Line.png',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/Seperator-Line.png',
      },
      //Danh mục 1: 3 hình số 1,2,3
      'image1': {
        'key': 'image1',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food(2).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food2.png',
      },
      'image2': {
        'key': 'image2',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food(4).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food4.png',
      },
      'image3': {
        'key': 'image3',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food(8).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food8.png',
      },

      //Danh mục 2: 3 hình số 4,5,6
      'image4': {
        'key': 'image4',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(1).jpg',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg1.jpg',
      },
      'image5': {
        'key': 'image5',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(2).jpg',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg2.jpg',
      },
      'image6': {
        'key': 'image6',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(3).jpg',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg3.jpg',
      },

      //Danh mục 3-4: 3 hình số 7,8,9
      'image7': {
        'key': 'image7',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(4).jpg',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg4.jpg',
      },
      'image8': {
        'key': 'image8',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(5).jpg',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg5.jpg',
      },
      'image9': {
        'key': 'image9',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food-bg(6).jpg',
        'image':
            'https://foodiemenu.co/wp-content/uploads/2023/09/food-bg6.jpg',
      },

      //Danh mục 5: 3 hình số 10
      'image10': {
        'key': 'image10',
        'type': 'image',
        // 'image': '/temp_restaurant_1/food(7).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food7.png',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image":
              "imageBackground", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
          "boxFit": "fill",
        },
        "child": {
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              // Cột A
              {
                "key": "expanded-A",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "padding-A",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            // Cột A - Dòng 1
                            {
                              "key": "expanded-A1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "widgetName": "Container",
                                  "widgetSetting": {
                                    "margin": {
                                      "bottom": "1.w"
                                    }, // Dùng biến marginBottomImage

                                    "child": {
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "children": [
                                          // Cột A - Dòng 1 - Cột 1: Danh mục 1 với 3 sản phẩm 1,2,3
                                          {
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": "2",
                                              "child": {
                                                "widgetName": "Container",
                                                "widgetSetting": {
                                                  "decoration": {
                                                    "color": "0x18FFFFFF",
                                                    "borderRadius":
                                                        "30", // Dùng biến boderRadiusStyle
                                                  },
                                                  "padding": {"all": "1.5.w"},
                                                  "child": {
                                                    "key": "CTSC-A11",
                                                    "widgetName":
                                                        "categoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      "dataList": {
                                                        'category': "category1",
                                                        'product': [
                                                          "product1",
                                                          "product2",
                                                          "product3",
                                                        ],
                                                      },
                                                      "indexCategory": "0",
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "colorCategoryTitle":
                                                          "0xFFF47B22",
                                                      "startIndexProduct": "0",
                                                      "endIndexProduct": "3",
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          },
                                          // Cột A - Dòng 1 - Cột 2: Sizedbox khoảng cách
                                          {
                                            "key": "SizedBox-A12",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "1.5.w"}
                                          },
                                          // Cột A - Dòng 1 - Cột 3: 3 hình đại diện
                                          {
                                            "key": "expanded-A13",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "column-A13",
                                                "widgetName": "Column",
                                                "widgetSetting": {
                                                  "mainAxisAlignment": "start",
                                                  "children": [
                                                    {
                                                      "key": "expanded-A13-1",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "widgetName": "image",
                                                          "widgetSetting": {
                                                            "imageId":
                                                                "image1", //key = image1
                                                            "fixCover": "false"
                                                          },
                                                        },
                                                      }
                                                    },
                                                    {
                                                      "key": "expanded-A13-2",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "widgetName": "image",
                                                          "widgetSetting": {
                                                            "imageId":
                                                                "image2", //key = image2
                                                            "fixCover": "false"
                                                          }
                                                        },
                                                      }
                                                    },
                                                    {
                                                      "key": "expanded-A13-3",
                                                      "widgetName": "Expanded",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "widgetName": "image",
                                                          "widgetSetting": {
                                                            "imageId":
                                                                "image3", //key = image3
                                                            "fixCover": "false"
                                                          }
                                                        },
                                                      }
                                                    },
                                                  ]
                                                }
                                              }
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              }
                            },

                            // Cột A - Dòng 2 - Khoảng cách
                            {
                              "key": "SizedBox-2",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"height": "2.h"}
                            },

                            // Cột A - Dòng 3 - Danh mục 2 với
                            // 3 hình ảnh 4,5,6
                            // 5 sản phẩm 4,5,6,7,8
                            {
                              "widgetName": "categoryTitleGridLayout1",
                              "widgetSetting": {
                                "dataList": {
                                  'category': "category2",
                                  'product': [
                                    "product4",
                                    "product5",
                                    "product6",
                                    "product7",
                                    "product8",
                                  ],
                                  'image': [
                                    "image4",
                                    "image5",
                                    "image6",
                                  ],
                                },
                                "useBackgroundCategoryTitle": "false",
                                "colorPrice": "0xFFF47B22",
                              }
                            },
                          ]
                        }
                      }
                    }
                  }
                }
              },
              //Cột B
              {
                "key": "expanded-B",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "padding-B",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "key": "column-B",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "mainAxisAlignment": "start",
                          "children": [
                            {
                              //Cột B - Dòng 1: 3 hình ảnh số 7,8,9
                              "key": "row-B1",
                              "widgetName": "Row",
                              "widgetSetting": {
                                "textRightToLeft": "false",
                                "crossAxisAlignment": "start",
                                "mainAxisAlignment": "start",
                                "children": [
                                  //hình số 7
                                  {
                                    "key": "expanded-A13-3",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "widgetName": "image",
                                        "widgetSetting": {
                                          "imageId": "image7", //key = image7
                                          "fixCover": "true"
                                        }
                                      },
                                    }
                                  },

                                  //[092318HG] Add sizedbox
                                  {
                                    "key": "SizedBox-A13-3-1",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {"width": "1.w"}
                                  },

                                  //hình số 8
                                  {
                                    "key": "expanded-A13-3",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "widgetName": "image",
                                        "widgetSetting": {
                                          "imageId": "image8", //key = image8
                                          "fixCover": "true"
                                        }
                                      },
                                    }
                                  },

                                  //[092318HG] Add sizedbox
                                  {
                                    "key": "SizedBox-A13-3-2",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {"width": "1.w"}
                                  },

                                  //hình số 9
                                  {
                                    "key": "expanded-A13-3",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "widgetName": "image",
                                        "widgetSetting": {
                                          "imageId": "image9", //key = image9
                                          "fixCover": "true"
                                        }
                                      },
                                    }
                                  },
                                ]
                              },
                            },
                            //Cột B - Dòng 2: Danh mục 3 và Danh mục 4
                            {
                              "key": "expanded-B2",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  //Cột B - Dòng 2
                                  "key": "row-B2",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      {
                                        "key": "expanded-B2",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            //Cột B - Dòng 2 - Cột 1: Danh mục 3 với
                                            //3 sản phẩm 9,10,11
                                            "key": "CTSC-B21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category3",
                                                'product': [
                                                  "product9",
                                                  "product10",
                                                  "product11",
                                                ],
                                              },
                                              "indexCategory": "2",
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "startIndexProduct": "0",
                                              "endIndexProduct": " 3",
                                              "colorPrice": "0xFFf47b22",
                                            }
                                          },
                                        },
                                      },
                                      //Cột B - Dòng 2 - Cột 2: Hình phân tách 2 danh mục
                                      {
                                        "key": "container-B22",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"},
                                          "child": {
                                            "key": "image-B22",
                                            "widgetName": "image",
                                            "widgetSetting": {
                                              "imageId":
                                                  "imageSeperatorLine", //key = imageSeperatorLine
                                              "fixCover": "false"
                                            }
                                          }
                                        }
                                      },

                                      // Cột B - Dòng 2 - Cột 3: Danh mục 4 với
                                      // 3 sản phẩm 12,13,14,
                                      {
                                        "key": "expanded-B23",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "CTSC-B21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category4",
                                                'product': [
                                                  "product12",
                                                  "product13",
                                                  "product14",
                                                ],
                                              },
                                              "indexCategory": "3",
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "startIndexProduct": "0",
                                              "endIndexProduct": "3",
                                              "colorPrice": "0xFFf47b22",
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  },
                                },
                              },
                            },

                            //Cột B - Dòng 3: Danh mục 5 và hình số 10
                            {
                              "key": "expanded-B3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  //Cột B - Dòng 3
                                  "key": "row-B3",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      //Cột B - Dòng 3 - Cột 1: Danh mục 5 với
                                      //3 sản phẩm 15,16,17
                                      {
                                        "key": "expanded-x",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "CTSC-B31",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category5",
                                                'product': [
                                                  "product15",
                                                  "product16",
                                                  "product17",
                                                ],
                                              },
                                              "indexCategory": "4",
                                              "useBackgroundCategoryTitle":
                                                  "false",
                                              "startIndexProduct": "0",
                                              "endIndexProduct": "3",
                                              "colorPrice": "0xFFf47b22",
                                            }
                                          },
                                        }
                                      },

                                      //Cột B - Dòng 3 - Cột 2: hình số 10
                                      {
                                        "key": "expanded-B32",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "padding-B32",
                                            "widgetName": "Padding",
                                            "widgetSetting": {
                                              "padding": {"all": "1.w"},
                                              "child": {
                                                "key": "image-B32",
                                                "widgetName": "image",
                                                "widgetSetting": {
                                                  "imageId":
                                                      "image10", //key = image10
                                                  "fixCover": "false"
                                                }
                                              }
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  },
                                },
                              },
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              }
            ]
          }
        }
      }
    }
  };

  static final temprestaurant2 = {
    "_id": "64ee744398c3d4605b477abc",
    "templateSlug": "temp_restaurant_2",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'w900',
          'fontFamily': 'Open Sans',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '0.8.w',
          'priceProductStyle': '0.8.w',
          'discriptionProductStyle': '0.7.w',
          'imageHeight': '25.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '21/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '0.9.w',
          'priceProductStyle': '0.9.w',
          'discriptionProductStyle': '0.8.w',
          'imageHeight': '30.h',
          'marginBottomImage': '0.h',
          // [112320TIN] fix oveflow product list
          'marginBottomProduct': '2.8.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '16/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.w',
          'imageHeight': '30.h',
          'marginBottomImage': '0.h',
          // [112320TIN] fix oveflow product list
          'marginBottomProduct': '2.9.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '16/10': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.w',
          'imageHeight': '40.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        '4/3': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.8.w',
          'priceProductStyle': '1.8.w',
          'discriptionProductStyle': '1.3.w',
          'imageHeight': '40.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.w',
          'imageHeight': '37.h',
          'marginBottomImage': '0.h',
          'marginBottomProduct': '3.h',
          'boderRadiusStyle': '100',
          'marginBottomCategoryTitle': '0.h',
          'paddingVerticalCategorTilte': '2.h',
          'paddingHorizontalCategorTilte': '1.h',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '/temp_restaurant_2/food(1).png',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '/temp_restaurant_2/food(2).png',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '/temp_restaurant_2/food(3).png',
      },
      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '/temp_restaurant_2/food(4).png',
      },

      //Danh mục 1: pro 1, 2 ,3, 4
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$21.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 2: pro 5, 6, 7, 8
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$22.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$24.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$25.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 3: pro 9, 10, 11, 12
      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 9',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 10',
        'price': '\$32.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 11',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 12',
        'price': '\$41.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Hình background
      'imageBackground': {
        'key': 'imageBackground',
        'type': 'image',
        // 'image': '/temp_restaurant_2/BG-2.png',
        'image': 'https://foodiemenu.co/wp-content/uploads/2023/09/BG-2.png',
      },

      //Hình Background Title - No link for this template
      'imageBackgroundTitle': {
        'key': 'imageBackgroundTitle',
        'type': 'image',
        'image': '', //No link for this template
      },

      //Hình danh mục 1
      'image1': {
        'key': 'image1',
        'type': 'image',
        // 'image': '/temp_restaurant_2/food(1).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food1.png',
      },

      //Hình danh mục 2
      'image2': {
        'key': 'image2',
        'type': 'image',
        // 'image': '/temp_restaurant_2/food(2).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food2-1.png',
      },

      //Hình danh mục 3
      'image3': {
        'key': 'image3',
        'type': 'image',
        // 'image': '/temp_restaurant_2/food(3).png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/food3.png',
      },
    },

    "widgets_setting": {
      "key": "Container-fullScreen",
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image": "imageBackground", //key imageBackground
          "boxFit": "cover",
        },
        "child": {
          "widgetName": "Row",
          "widgetSetting": {
            "children": [
              //Cột 1:
              {
                "key": "expanded-A1",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Padding-A1",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "key": "column-A1",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "key": "expanded-A1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "key": "column-A11",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "children": [
                                      //Tiêu đề Danh mục 1
                                      {
                                        "key": "categoryTitle-A111",
                                        "widgetName": "categoryTitle",
                                        "widgetSetting": {
                                          "categoryId":
                                              "category1", //key category1
                                          "useBackground": "true",
                                          "imageId":
                                              "imageBackgroundTitle", //key imageBackgroundTitle
                                          "useFullWidth": "true",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E",
                                        }
                                      },

                                      //Hình đại diện Danh mục 1
                                      {
                                        "key": "expanded-A121",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "container-A1",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "height":
                                                  "30.h", //Dùng biến imageHeight
                                              "margin": {
                                                "bottom": "0.h"
                                              }, //Dùng biến marginBottomImage
                                              "padding": {"vertical": "1.5.w"},
                                              "decoration": {
                                                // "color": "0x18FFFFFF",
                                                "borderRadius": "30",
                                              },
                                              "child": {
                                                "key": "Center-A1",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "widgetName": "image",
                                                    "widgetSetting": {
                                                      "imageId":
                                                          "image1", //key = image1
                                                      "fixCover": "false",
                                                    },
                                                  },
                                                },
                                              },
                                            }
                                          },
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            },

                            // Danh sách sản phẩm của Danh mục 1
                            {
                              "key": "expanded-A1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "key": "productList-A1",
                                  "widgetName": "productList",
                                  "widgetSetting": {
                                    "dataList": [
                                      "product1",
                                      "product2",
                                      "product3",
                                      "product4",
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22",
                                    "colorProductName": "",
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "colorDescription": "",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              //Cột 2:
              {
                "key": "expanded-A3",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Padding-A3",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "key": "column-A3",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "key": "expanded-A31",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "key": "column-A31",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "children": [
                                      //Tiêu đề Danh mục 2
                                      {
                                        "key": "categoryTitle-A3",
                                        "widgetName": "categoryTitle",
                                        "widgetSetting": {
                                          "categoryId":
                                              "category2", //key category1
                                          "useBackground": "true",
                                          "imageId":
                                              "imageBackgroundTitle", //key imageBackgroundTitle
                                          "useFullWidth": "true",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E",
                                        }
                                      },

                                      //Hình đại diện Danh mục 2
                                      {
                                        "key": "expanded-A3",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "container-A3",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "height":
                                                  "30.h", //Dùng biến imageHeight
                                              "margin": {
                                                "bottom": "0.h"
                                              }, //Dùng biến marginBottomImage
                                              "decoration": {
                                                // "color": "0x18FFFFFF",
                                                "borderRadius": "30",
                                              },
                                              "padding": {"vertical": "1.5.w"},
                                              "child": {
                                                "key": "Center-A3",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "widgetName": "image",
                                                    "widgetSetting": {
                                                      "imageId":
                                                          "image2", //key = image2
                                                      "fixCover": "false",
                                                    },
                                                  },
                                                },
                                              },
                                            }
                                          },
                                        }
                                      },
                                    ]
                                  }
                                },
                              }
                            },

                            // Danh sách của Danh mục 2
                            {
                              "key": "expanded-A3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "key": "productList-x",
                                  "widgetName": "productList",
                                  "widgetSetting": {
                                    "dataList": [
                                      "product5",
                                      "product6",
                                      "product7",
                                      "product8",
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22",
                                    "colorProductName": "",
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "colorDescription": "",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              //Cột 3:
              {
                "key": "expanded-A3",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Padding-A3",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "key": "column-A3",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "key": "expanded-A3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "5",
                                "child": {
                                  "key": "column-A3",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "children": [
                                      //Tiêu đề Danh mục 3
                                      {
                                        "key": "categoryTitle-A3",
                                        "widgetName": "categoryTitle",
                                        "widgetSetting": {
                                          "categoryId":
                                              "category3", //key category3
                                          "useBackground": "true",
                                          "imageId":
                                              "imageBackgroundTitle", //key imageBackgroundTitle
                                          "useFullWidth": "true",
                                          "color": "0xFFFFFFFF",
                                          "colorBackground": "0xB161644E",
                                        }
                                      },

                                      //Hình đại diện Danh mục 3
                                      {
                                        "key": "expanded-A3",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "container-A3",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "height":
                                                  "30.h", //Dùng biến imageHeight
                                              "margin": {
                                                "bottom": "0.h"
                                              }, //Dùng biến marginBottomImage
                                              "decoration": {
                                                // "color": "0x18FFFFFF",
                                                "borderRadius": "30",
                                              },
                                              "padding": {"vertical": "1.5.w"},
                                              "child": {
                                                "key": "Center-A3",
                                                "widgetName": "Center",
                                                "widgetSetting": {
                                                  "child": {
                                                    "widgetName": "image",
                                                    "widgetSetting": {
                                                      "imageId":
                                                          "image3", //key = image3
                                                      "fixCover": "false",
                                                    },
                                                  },
                                                },
                                              },
                                            }
                                          },
                                        }
                                      },
                                    ]
                                  }
                                },
                              }
                            },

                            // Danh sách của Danh mục 3
                            {
                              "key": "expanded-A3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "6",
                                "child": {
                                  "key": "productList-3",
                                  "widgetName": "productList",
                                  "widgetSetting": {
                                    "dataList": [
                                      "product9",
                                      "product10",
                                      "product11",
                                      "product12",
                                    ],
                                    "mainAxisAlignment": "start",
                                    "crossAxisAlignment": "center",
                                    "colorPrice": "0xFFf47b22",
                                    "colorProductName": "",
                                    "toUpperCaseNameProductName": "true",
                                    "useMediumStyle": "false",
                                    "colorDescription": "",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "maxLineProductName": "1",
                                    "maxLineProductDescription": "2"
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
            ]
          }
        }
      }
    }
  };

  static final temprestaurant3 = {
    "_id": "64ee744398c3d4605b477abc",
    "templateSlug": "temp_restaurant_3",
    "RatioSetting": {
      "textStyle": {
        "categoryNameStyle": {
          "fontWeight": "w300",
          "fontFamily": "Alfa Slab One",
          "letterSpacing": "5",
        },
        "nameProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "Poppins",
        },
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Poppins",
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Poppins",
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "Poppins",
        },
        "priceProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "Poppins",
        },
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Open Sans",
        },
      },
      "renderScreen": {
        "32/9": {
          "categoryNameStyle": "1.w",
          "nameProductStyle": "0.7.w",
          "nameProductStyleMedium": "0.5.w",
          "priceProductStyle": "0.7.w",
          "priceProductStyleMedium": "0.5.w",
          "discriptionProductStyle": "0.5.w",
          "discriptionProductStyleMedium": "0.3.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
        "21/9": {
          "categoryNameStyle": "1.5.w",
          "nameProductStyle": "1.w",
          "nameProductStyleMedium": "0.7.w",
          "priceProductStyle": "1.w",
          "priceProductStyleMedium": "0.7.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
        "16/9": {
          "categoryNameStyle": "2.5.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "20.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.5.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
        "16/10": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "1.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
        "4/3": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "2.w",
          "nameProductStyleMedium": "1.5.w",
          "priceProductStyle": "2.w",
          "priceProductStyleMedium": "1.5.w",
          "discriptionProductStyle": "1.5.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
        "default": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "20.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "",
      },
      //Danh mục 1: pro 1, 2 ,3, 4, 5
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      //Danh mục 2: pro 6, 7, 8, 8, 9, 10
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 2.1",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 2.2",
        "price": "\$24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 2.3",
        "price": "\$25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 2.4",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 2.5",
        "price": "\$32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      //Danh mục 3: pro 11, 12, 13, 14, 15
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 3.1",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 3.2",
        "price": "\$41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 3.3",
        "price": "\$42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 3.4",
        "price": "\$43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 3.5",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Hình background
      'imageBackground': {
        'key': 'imageBackground',
        'type': 'image',
        // 'image': '/temp_restaurant_3/food-bg(4).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg4-1.jpg',
      },

      //Danh mục 1: 3 hình số 1,2,3
      'image1': {
        'key': 'image1',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(1).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp31.png",
      },
      'image2': {
        'key': 'image2',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(2).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp32.png",
      },
      'image3': {
        'key': 'image3',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(3).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp33.png",
      },

      //Danh mục 2: 3 hình số 4,5,6
      'image4': {
        'key': 'image4',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(4).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp34.png",
      },
      'image5': {
        'key': 'image5',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(5).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp35.png",
      },
      'image6': {
        'key': 'image6',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(6).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp36.png",
      },

      //Danh mục 3-4: 3 hình số 7,8,9
      'image7': {
        'key': 'image7',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(7).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp37.png",
      },
      'image8': {
        'key': 'image8',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(9).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp39.png",
      },
      'image9': {
        'key': 'image9',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(8).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp38.png",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image":
              "imageBackground", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
          "boxFit": "cover",
        },
        "child": {
          "key": "Container-overlay",
          "widgetName": "Container",
          "widgetSetting": {
            "color": "0xe5000000",
            "child": {
              "key": "Row-menu",
              "widgetName": "Row",
              "widgetSetting": {
                "textRightToLeft": "false",
                "children": [
                  // Cột 1
                  {
                    "key": "expanded-c1",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "key": "padding-c1",
                        "widgetName": "Padding",
                        "widgetSetting": {
                          "padding": {"all": "2.w"},
                          "child": {
                            "key": "Column-c1",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "mainAxisAlignment": "start",
                              "children": [
                                // Cột 1 - Dòng 1 categoryTitleSingleColumn
                                {
                                  "key": "categoryTitleSingleColumn-c1-r1",
                                  "widgetName": "categoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "dataList": {
                                      "category": "category1",
                                      "product": [
                                        "product1",
                                        "product2",
                                        "product3",
                                        "product4",
                                        "product5",
                                      ],
                                    },
                                    "toUpperCaseNameProductName": "false",
                                    "useBackgroundCategoryTitle": "true",
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "useFullWidthCategoryTitle": "true",
                                  }
                                },

                                // Cột 1 - Dòng 2 - Spacing marginBottomCategoryTitle
                                {
                                  "key": "PaddingByRatio-c1-r2",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                // Cột 1 - Dòng 3 - 3 hình
                                {
                                  "key": "Expanded-c1-r3",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "child": {
                                      "key": "Row-c1-r3",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "crossAxisAlignment": "start",
                                        "children": [
                                          // Cột 1 - Dòng 3 - hình 1
                                          {
                                            "key": "Expanded-image1",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-1",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image1",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          // Khoảng trống 2.w
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 1 - Dòng 3 - hình 2
                                          {
                                            "key": "Expanded-image2",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-2",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image2",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          // Khoảng trống 2.w
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 1 - Dòng 3 - hình 3
                                          {
                                            "key": "Expanded-image3",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-3",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image3",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  // Cột 2
                  {
                    "key": "expanded-c1",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "key": "Container-c1",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "padding": {"all": "2.w"},
                          "color": "0x09ffffff",
                          "child": {
                            "key": "Column-c1",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "mainAxisAlignment": "start",
                              "children": [
                                // Cột 2 - Dòng 1 categoryTitleSingleColumn
                                {
                                  "key": "categoryTitleSingleColumn-c1-r1",
                                  "widgetName": "categoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "dataList": {
                                      "category": "category2",
                                      "product": [
                                        "product6",
                                        "product7",
                                        "product8",
                                        "product9",
                                        "product10",
                                      ],
                                    },
                                    "toUpperCaseNameProductName": "false",
                                    "useBackgroundCategoryTitle": "true",
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "useFullWidthCategoryTitle": "true",
                                  }
                                },

                                // Cột 2 - Dòng 2 - Spacing marginBottomCategoryTitle
                                {
                                  "key": "PaddingByRatio-c1-r2",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                // Cột 2 - Dòng 3 - 3 hình
                                {
                                  "key": "Expanded-c1-r3",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "child": {
                                      "key": "Row-c1-r3",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "crossAxisAlignment": "start",
                                        "children": [
                                          // Cột 2 - Dòng 3 - hình 4
                                          {
                                            "key": "Expanded-image4",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-4",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image4",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 2 - Dòng 3 - hình 5
                                          {
                                            "key": "Expanded-image5",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-5",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image5",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          // Khoảng trống 2.w
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 1 - Dòng 3 - hình 6
                                          {
                                            "key": "Expanded-image6",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-6",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image6",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  // Cột 3
                  {
                    "key": "expanded-c1",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "key": "padding-c1",
                        "widgetName": "Padding",
                        "widgetSetting": {
                          "padding": {"all": "2.w"},
                          "child": {
                            "key": "Column-c1",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "mainAxisAlignment": "start",
                              "children": [
                                // Cột 3 - Dòng 1 categoryTitleSingleColumn
                                {
                                  "key": "categoryTitleSingleColumn-c1-r1",
                                  "widgetName": "categoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "dataList": {
                                      "category": "category3",
                                      "product": [
                                        "product11",
                                        "product12",
                                        "product13",
                                        "product14",
                                        "product15",
                                      ],
                                    },
                                    "toUpperCaseNameProductName": "false",
                                    "useBackgroundCategoryTitle": "true",
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "useFullWidthCategoryTitle": "true",
                                  }
                                },
                                // Cột 3 - Dòng 2 - Spacing marginBottomCategoryTitle
                                {
                                  "key": "PaddingByRatio-c1-r2",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                // Cột 3 - Dòng 3 - 3 hình
                                {
                                  "key": "Expanded-c1-r3",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "child": {
                                      "key": "Row-c1-r3",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "crossAxisAlignment": "start",
                                        "children": [
                                          // Cột 3 - Dòng 3 - hình 7
                                          {
                                            "key": "Expanded-image7",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-7",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image7",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          // Khoảng trống 2.w
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 3 - Dòng 3 - hình 8
                                          {
                                            "key": "Expanded-image8",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-8",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image8",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          // Khoảng trống 2.w
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 3 - Dòng 3 - hình 9
                                          {
                                            "key": "Expanded-image9",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-9",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image9",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                ]
              }
            }
          }
        }
      }
    }
  };

  static final temprestaurant4 = {
    "_id": "idTemplateRestaurant4",
    "templateSlug": "temp_restaurant_4",
    "isDeleted": false,
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '1.w',
          'nameProductStyleMedium': '0.6.w',
          'priceProductStyle': '1.w',
          'priceProductStyleMedium': '0.6.w',
          'discriptionProductStyle': '0.6.w',
          'discriptionProductStyleMedium': '0.5.w',
          'imageHeight': '40.h',
          'marginBottomImage': '0.7.w',
          'marginBottomProduct': '1.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '21/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.3.w',
          'nameProductStyleMedium': '0.7.w',
          'priceProductStyle': '1.3.w',
          'priceProductStyleMedium': '0.7.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '1.5.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '16/9': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '16/10': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '4/3': {
          'categoryNameStyle': '3.5.w',
          'nameProductStyle': '2.w',
          'nameProductStyleMedium': '1.3.w',
          'priceProductStyle': '2.w',
          'priceProductStyleMedium': '1.3.w',
          'discriptionProductStyle': '1.3.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.3.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        'default': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
      },
    },
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '',
      },
      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '',
      },
      'category5': {
        'key': 'category5',
        'type': 'category',
        'name': 'Category 5',
        'image': '',
      },
      //Danh mục 1: pro 1, 2, 3, 4, 5, 6
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$40.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$50.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$60.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 2: pro 7, 8, 9
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$70.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$80.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 9',
        'price': '\$90.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 3: pro  10, 11,12
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 10',
        'price': '\$10.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 11',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 12',
        'price': '\$41.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 4: pro 13, 14, 15
      'product13': {
        'key': 'product13',
        'type': 'product',
        'name': 'Product 13',
        'price': '\$42.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product14': {
        'key': 'product14',
        'type': 'product',
        'name': 'Product 14',
        'price': '\$43.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product15': {
        'key': 'product15',
        'type': 'product',
        'name': 'Product 15',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 5: pro 16, 17, 18
      'product16': {
        'key': 'product16',
        'type': 'product',
        'name': 'Product 16',
        'price': '\$16.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product17': {
        'key': 'product17',
        'type': 'product',
        'name': 'Product 17',
        'price': '\$17.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product18': {
        'key': 'product18',
        'type': 'product',
        'name': 'Product 18',
        'price': '\$18.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      //Hình background
      'imageBackground': {
        'key': 'imageBackground',
        'type': 'image',
        // 'image': '/temp_restaurant_4/BG-2.png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/BG-2-1.png',
      },
      // Hình background category title
      "imageBgCategoryTitle": {
        "key": "imageBgCategoryTitle",
        "type": "image",
        // "image": "/temp_restaurant_4/decorate/BG-title-2.png",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/BG-title-2.png",
      },
      'image1': {
        'key': 'image1',
        'type': 'image',
        // 'image': '/temp_restaurant_4/food-bg(1).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg1-1.jpg',
      },
      'image2': {
        'key': 'image2',
        'type': 'image',
        // 'image': '/temp_restaurant_4/food-bg(2).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg2-1.jpg',
      },
      'image3': {
        'key': 'image3',
        'type': 'image',
        // 'image': '/temp_restaurant_4/food-bg(3).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg3-1.jpg',
      },
      'image4': {
        'key': 'image4',
        'type': 'image',
        // 'image': '/temp_restaurant_4/food-bg(4).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg4-2-scaled.jpg',
      },
      'image5': {
        'key': 'image5',
        'type': 'image',
        // 'image': '/temp_restaurant_4/food-bg(5).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg5-1-scaled.jpg',
      },
    },
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "widht": "100.w",
        "height": "100.h",
        "decoration": {
          "image": "imageBackground",
          "boxFit": "fill",
        },
        "child": {
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              // Cột A
              {
                "key": "expanded-A",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "key": "padding-A",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "widgetName": "Column",
                        "widgetSetting": {
                          "mainAxisAlignment": "start",
                          "children": [
                            // Cột A - Dòng 1 - Danh mục 1
                            {
                              "key": "CTGL-A1",
                              "widgetName": "CategoryTitleGridLayout3",
                              "widgetSetting": {
                                "dataList": {
                                  "category": "category1",
                                  "product": [
                                    "product1",
                                    "product2",
                                    "product3",
                                    "product4",
                                    "product5",
                                    "product6",
                                  ]
                                },
                                "useBackgroundCategoryTitle": "true",
                                // "pathImageBgCategoryTitle":
                                //     "/temp_restaurant_4/decorate/BG-title-2.png",
                                "imageId": "imageBgCategoryTitle",
                                "colorBackgroundCategoryTitle": "0x00121212",
                                "colorCategoryTitle": "0xFF121212",
                                "colorProductName": "0xFF121212",
                                "colorPrice": "0xFF8d1111",
                                "colorDescription": "0xFF121212",
                                "useFullWidthCategoryTitle": "false",
                              }
                            },
                            // Cột A - Dòng 2 - khoảng cách
                            {
                              "key": "PaddingByRatio-A2",
                              "widgetName": "PaddingByRatio",
                              "widgetSetting": {
                                "bottomPaddingDataKey":
                                    "marginBottomCategoryTitle"
                              }
                            },

                            // Cột A - Dòng 3 - Danh mục 2 với
                            // 3 sản phẩm: 7, 8, 9
                            // 3 hình ảnh 1,2,3
                            {
                              "widgetName": "categoryTitleGridLayout4",
                              "widgetSetting": {
                                "dataList": {
                                  'category': "category2",
                                  'product': [
                                    "product7",
                                    "product8",
                                    "product9",
                                  ],
                                  'image': [
                                    "image1",
                                    "image2",
                                    "image3",
                                  ],
                                },
                                "useBackgroundCategoryTitle": "true",
                                "imageId":
                                    "imageBgCategoryTitle", //key imageBgCategoryTitle
                                "colorCategoryTitle": "0xFF121212",
                                "colorProductName": "0xFF121212",
                                "colorPrice": "0xFF8d1111",
                                "colorDescription": "0xFF121212",
                              }
                            },
                          ]
                        }
                      }
                    }
                  }
                }
              },

              // Cột B
              {
                "key": "expanded-B",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "key": "padding-B",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "widgetName": "Row",
                        "widgetSetting": {
                          "children": [
                            // Cột B - cột 1
                            {
                              "key": "expanded-B1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "children": [
                                      //Cột B - Dòng 1 - 1 hình ảnh
                                      {
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "Row-br2",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "start",
                                              "children": [
                                                {
                                                  "key": "Expanded-image4",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "image-B11",
                                                      "widgetName": "image",
                                                      "widgetSetting": {
                                                        "imageId": "image4"
                                                      },
                                                    },
                                                  },
                                                }
                                              ]
                                            }
                                          }
                                        }
                                      },
                                      //Cột B - Dòng 2 - khoảng cách
                                      {
                                        "key": "PaddingByRatio-A2",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "bottomPaddingDataKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },
                                      //Cột B - Dòng 3 - Danh mục 3
                                      {
                                        "key": "CTSC-B13",
                                        "widgetName":
                                            "categoryTitleSingleColumn",
                                        "widgetSetting": {
                                          "dataList": {
                                            "category": "category3",
                                            "product": [
                                              "product10",
                                              "product11",
                                              "product12",
                                            ],
                                          },
                                          "useBackgroundCategoryTitle": "true",
                                          // "pathImageBgCategoryTitle":
                                          //     "/temp_restaurant_4/decorate/BG-title-2.png",
                                          "imageId": "imageBgCategoryTitle",
                                          "colorCategoryTitle": "0xFF121212",
                                          "colorProductName": "0xFF121212",
                                          "colorPrice": "0xFF8d1111",
                                          "colorDescription": "0xFF121212",
                                        }
                                      },
                                      //Cột B - Dòng 4 - khoảng cách
                                      {
                                        "key": "PaddingByRatio-A2",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "bottomPaddingDataKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },

                                      //Cột B - Dòng 5 - 1 hình ảnh
                                      {
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "Row-br2",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "start",
                                              "children": [
                                                {
                                                  "key": "Expanded-image4",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "image-B15",
                                                      "widgetName": "image",
                                                      "widgetSetting": {
                                                        "imageId": "image5"
                                                      },
                                                    },
                                                  },
                                                }
                                              ]
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            },
                            // //Cột B - Cột 2 -khoảng cách
                            {
                              "key": "SizedBox-B2",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"width": "3.w"}
                            },
                            {
                              "key": "expanded-B3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "children": [
                                      //Cột B - Cột 3 - Dòng 1
                                      {
                                        "key": "expanded-B31",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "CTSC-B311",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category4",
                                                "product": [
                                                  "product13",
                                                  "product14",
                                                  "product15",
                                                ],
                                              },
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              // "pathImageBgCategoryTitle":
                                              //     "/temp_restaurant_4/decorate/BG-title-2.png",
                                              "imageId": "imageBgCategoryTitle",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorProductName": "0xFF121212",
                                              "colorPrice": "0xFF8d1111",
                                              "colorDescription": "0xFF121212",
                                            }
                                          }
                                        }
                                      },
                                      //Cột B - Cột 3 - Dòng 2
                                      {
                                        "key": "PaddingByRatio-A2",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "bottomPaddingDataKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },
                                      //Cột B - Cột 3 - Dòng 3
                                      {
                                        "key": "expanded-B33",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "CTSC-B331",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category5",
                                                "product": [
                                                  "product16",
                                                  "product17",
                                                  "product18",
                                                ],
                                              },
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              // "pathImageBgCategoryTitle":
                                              //     "/temp_restaurant_4/decorate/BG-title-2.png",
                                              "imageId": "imageBgCategoryTitle",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorProductName": "0xFF121212",
                                              "colorPrice": "0xFF8d1111",
                                              "colorDescription": "0xFF121212",
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              }
            ]
          }
        }
      }
    }
  };
  static final temprestaurant5 = {
    "_id": "id_tem5",
    "templateSlug": "temp_restaurant_5",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.w',
          'nameProductStyle': '0.8.w',
          'nameProductStyleMedium': '0.4.w',
          'priceProductStyle': '0.8.w',
          'priceProductStyleMedium': '0.4.w',
          'discriptionProductStyle': '0.4.w',
          'discriptionProductStyleMedium': '0.3.w',
          'imageHeight': '50.h',
          'marginBottomImage': '0.4.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.4.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '21/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.w',
          'nameProductStyleMedium': '0.5.w',
          'priceProductStyle': '1.w',
          'priceProductStyleMedium': '0.5.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '50.h',
          'marginBottomImage': '0.5.w',
          'marginBottomProduct': '1.9.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.5.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '16/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '16/10': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.7.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        '4/3': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyle': '2.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '2.w',
          'priceProductStyleMedium': '1.3.w',
          'discriptionProductStyle': '1.3.w',
          'discriptionProductStyleMedium': '0.7.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.6.w',
          'imageHeight': '50.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '1.w',
          'paddingHorizontalCategorTilte': '3.5.w',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      //092314LH Fix dynamic sample data to template
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '/temp_restaurant_2/food(1).png',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '/temp_restaurant_2/food(2).png',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '/temp_restaurant_2/food(3).png',
      },
      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '/temp_restaurant_2/food(4).png',
      },
      'category5': {
        'key': 'category5',
        'type': 'category',
        'name': 'Category 5',
        'image': '/temp_restaurant_2/food(5).png',
      },
      'category6': {
        'key': 'category6',
        'type': 'category',
        'name': 'Category 6',
        'image': '/temp_restaurant_2/food(6).png',
      },

      //Danh mục 1: pro 1, 2 ,3
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 2: pro 4, 5, 6
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$40.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$50.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$60.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 3: pro 7, 8, 9
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$70.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$80.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 9',
        'price': '\$90.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 4: pro  10, 11,12
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 10',
        'price': '\$10.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 11',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 12',
        'price': '\$41.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 5: pro 13, 14, 15
      'product13': {
        'key': 'product13',
        'type': 'product',
        'name': 'Product 13',
        'price': '\$42.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product14': {
        'key': 'product14',
        'type': 'product',
        'name': 'Product 14',
        'price': '\$43.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product15': {
        'key': 'product15',
        'type': 'product',
        'name': 'Product 15',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 6: pro 16, 17, 18
      'product16': {
        'key': 'product16',
        'type': 'product',
        'name': 'Product 16',
        'price': '\$16.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product17': {
        'key': 'product17',
        'type': 'product',
        'name': 'Product 17',
        'price': '\$17.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product18': {
        'key': 'product18',
        'type': 'product',
        'name': 'Product 18',
        'price': '\$18.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Hình background
      'imageBackground': {
        'key': 'imageBackground',
        'type': 'image',
        // 'image': '/temp_restaurant_5/BG-1.png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/BG-1-1.png',
      },

      //Hình Background Title
      'imageBackgroundTitle': {
        'key': 'imageBackgroundTitle',
        'type': 'image',
        // 'image': '/temp_restaurant_5/decorate/BG-title.png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/BG-title.png',
      },

      //Hình phân tách cột
      'imageSeperatorLine': {
        'key': 'imageSeperatorLine',
        'type': 'image',
        'fixed': 'true',
        // 'image': '/temp_restaurant_1/decorate/Seperator-Line.png',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/Seperator-Line-1.png',
      },

      //Hình danh mục 1
      'image1': {
        'key': 'image1',
        'type': 'image',
        // 'image': '/temp_restaurant_5/hamburger(1).png',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/hamburger1.png',
      },

      //Hình danh mục 6
      'image2': {
        'key': 'image2',
        'type': 'image',
        // 'image': '/temp_restaurant_5/hamburger(2).png',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/hamburger2.png',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image":
              "imageBackground", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
          "boxFit": "fill",
        },
        "child": {
          "key": "row",
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              //Cột A
              {
                "key": "expanded-A",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Padding-A",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "3.w"},
                      "child": {
                        "key": "column-A",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            // Cột A - Phần 1:
                            {
                              "key": "expanded-A1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "key": "column-A121",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      // Cột A - Phần 1 - Dòng 1: Tiêu đề Danh mục 1
                                      {
                                        "key": "categoryTitle-A11",
                                        "widgetName": "categoryTitle",
                                        "widgetSetting": {
                                          "categoryId":
                                              "category1", //key category1
                                          "useBackground": "true",
                                          "imageId":
                                              "imageBackgroundTitle", //key imageBackgroundTitle
                                          "useFullWidth": "false",
                                          "color": "0xFF121212",
                                          "colorBackground": "0x00121212",
                                        }
                                      },

                                      // Cột A - Phần 1 - Dòng 2: Danh mục 1
                                      {
                                        "key": "expanded-A12",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "row-A12",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "stretch",
                                              "children": [
                                                //Cột A - Phần 1 - Dòng 2 - Cột 1: Danh sách sản phẩm (3 sản phẩm đầu tiên)
                                                {
                                                  "key": "expanded-A121",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "flex": "1",
                                                    "child": {
                                                      "key": "productList-A121",
                                                      "widgetName":
                                                          "productList",
                                                      "widgetSetting": {
                                                        "dataList": [
                                                          "product1",
                                                          "product2",
                                                          "product3",
                                                        ],
                                                        "mainAxisAlignment":
                                                            "start",
                                                        "crossAxisAlignment":
                                                            "center",
                                                        "colorPrice":
                                                            "0xFFf4b91a",
                                                        "colorProductName":
                                                            "0xFFFFFFFF",
                                                        "toUpperCaseNameProductName":
                                                            "true",
                                                        "useMediumStyle":
                                                            "false",
                                                        "colorDescription":
                                                            "0xFFFFFFFF",
                                                        "useThumnailProduct":
                                                            "false",
                                                        "pathThumnailProduct":
                                                            "",
                                                        "maxLineProductName":
                                                            "1",
                                                        "maxLineProductDescription":
                                                            "2"
                                                      }
                                                    }
                                                  }
                                                },

                                                //Cột A - Phần 1 - Dòng 2 - Cột 2: Decoration
                                                {
                                                  "key": "container-A122",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "width": "5.w",
                                                    "padding": {
                                                      "vertical": "2.w"
                                                    },
                                                    "decoration": {
                                                      "image":
                                                          "imageSeperatorLine", //Key imageSeperatorLine, Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
                                                      "boxFit": "contain"
                                                    }
                                                  }
                                                },

                                                //Cột A - Phần 1 - Dòng 2 - Cột 3: hình số 1
                                                {
                                                  "key": "expanded-A123",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "padding-A123",
                                                      "widgetName": "Padding",
                                                      "widgetSetting": {
                                                        "padding": {
                                                          "all": "1.w"
                                                        },
                                                        "child": {
                                                          "key": "image-A123",
                                                          "widgetName": "image",
                                                          "widgetSetting": {
                                                            "imageId":
                                                                "image1", //key = image1
                                                            "fixCover": "false"
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          }
                                        }
                                      }
                                    ]
                                  }
                                }
                              }
                            },

                            // Cột A - Phần 2:
                            {
                              "key": "expanded-A",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  //Cột A - Phần 2
                                  "key": "row-A2",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      //Cột A - Phần 2 - Cột 1: Danh mục 2 với
                                      //3 sản phẩm 4,5,6
                                      {
                                        "key": "expanded-A2",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "CTSC-A21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category2",
                                                'product': [
                                                  "product4",
                                                  "product5",
                                                  "product6",
                                                ],
                                              },

                                              "indexCategory": "1",
                                              "useBackgroundCategoryTitle":
                                                  "true",

                                              "imageId":
                                                  "imageBackgroundTitle", //Key imageBackgroundTitle

                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          },
                                        },
                                      },

                                      //Cột A - Phần 2 - cột 2: Decoration
                                      {
                                        "key": "container-A22",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"},
                                          "decoration": {
                                            "image":
                                                "imageSeperatorLine", //Key imageSeperatorLine, Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
                                            "boxFit": "contain"
                                          }
                                        }
                                      },

                                      //Cột A - Phần 2 - Cột 3: Danh mục 3 với
                                      //3 sản phẩm 7,8,9
                                      {
                                        "key": "expanded-A2",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "CTSC-A21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category3",
                                                'product': [
                                                  "product7",
                                                  "product8",
                                                  "product9",
                                                ],
                                              },

                                              "indexCategory": "2",
                                              "useBackgroundCategoryTitle":
                                                  "true",

                                              "imageId":
                                                  "imageBackgroundTitle", //Key imageBackgroundTitle

                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          },
                                        },
                                      },
                                    ]
                                  },
                                },
                              },
                            }
                          ]
                        }
                      },
                    },
                  },
                },
              },

              //Cột B
              {
                "key": "expanded-B",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Padding-B",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "3.w"},
                      "child": {
                        "key": "column-B",
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            // Cột B - Phần 1:
                            {
                              "key": "expanded-B1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  //Cột B - Phần 2
                                  "key": "row-B2",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      //Cột B - Phần 2 - Cột 1: Danh mục 4 với
                                      //3 sản phẩm 10,11,12
                                      {
                                        "key": "expanded-B21",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "CTSC-B21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category4",
                                                'product': [
                                                  "product10",
                                                  "product11",
                                                  "product12",
                                                ],
                                              },

                                              "indexCategory": "3",
                                              "useBackgroundCategoryTitle":
                                                  "true",

                                              "imageId":
                                                  "imageBackgroundTitle", //Key imageBackgroundTitle

                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          },
                                        },
                                      },

                                      //Cột B - Phần 2 - cột 2: Decoration
                                      {
                                        "key": "container-B22",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "width": "5.w",
                                          "padding": {"vertical": "2.w"},
                                          "decoration": {
                                            "image":
                                                "imageSeperatorLine", //Key imageSeperatorLine, Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
                                            "boxFit": "contain"
                                          }
                                        }
                                      },

                                      //Cột A - Phần 2 - Cột 3: Danh mục 5 với
                                      //3 sản phẩm 13,14,15
                                      {
                                        "key": "expanded-B23",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "CTSC-B21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category5",
                                                'product': [
                                                  "product13",
                                                  "product14",
                                                  "product15",
                                                ],
                                              },

                                              "indexCategory": "4",
                                              "useBackgroundCategoryTitle":
                                                  "true",

                                              "imageId":
                                                  "imageBackgroundTitle", //Key imageBackgroundTitle

                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          },
                                        },
                                      },
                                    ]
                                  },
                                },
                              },
                            },

                            // Cột B - Phần 2:
                            {
                              "key": "expanded-B2",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  //Cột B - Phần 2
                                  "key": "row-B2",
                                  "widgetName": "Row",
                                  "widgetSetting": {
                                    "textRightToLeft": "false",
                                    "crossAxisAlignment": "stretch",
                                    "children": [
                                      //Cột B - Phần 2 - Cột 1: Danh mục 6 với
                                      //3 sản phẩm 16,17,18
                                      {
                                        "key": "expanded-B2",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "CTSC-B21",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                'category': "category6",
                                                'product': [
                                                  "product16",
                                                  "product17",
                                                  "product18",
                                                ],
                                              },
                                              "indexCategory": "5",
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              "imageId":
                                                  "imageBackgroundTitle", //Key imageBackgroundTitle
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorBackgroundCategoryTitle":
                                                  "0x00121212",
                                              "colorPrice": "0xFFf4b91a",
                                              "colorProductName": "0xFFFFFFFF",
                                              "colorDescription": "0xFFFFFFFF",
                                              "useThumnailProduct": "false",
                                              "pathThumnailProduct": "",
                                              "toUpperCaseNameProductName":
                                                  "true",
                                              "useMediumStyle": "false",
                                              "useFullWidthCategoryTitle":
                                                  "false"
                                            }
                                          },
                                        },
                                      },

                                      //Cột B - Phần 2 - cột 2: hình số 2
                                      {
                                        "key": "expanded-A123",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "padding-A123",
                                            "widgetName": "Padding",
                                            "widgetSetting": {
                                              "padding": {"all": "1.w"},
                                              "child": {
                                                "key": "image-A123",
                                                "widgetName": "image",
                                                "widgetSetting": {
                                                  "imageId":
                                                      "image2", //key = image2
                                                  "fixCover": "false"
                                                }
                                              }
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  },
                                },
                              },
                            },
                          ]
                        }
                      },
                    },
                  },
                },
              },
            ]
          }
        }
      }
    }
  };
  static final templateNail1 = {
    "_id": "",
    "templateSlug": "temp_nail_1",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'normal',
          'fontFamily': 'Gravitas One',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Titillium Web',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Titillium Web',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Titillium Web',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'nameProductStyle': '1.w',
          'priceProductStyle': '1.w',
          'categoryNameStyle': '1.4.w',
          'discriptionProductStyle': '0.8.w',
        },
        '21/9': {
          'nameProductStyle': '1.2.w',
          'priceProductStyle': '1.2.w',
          'categoryNameStyle': '1.4.w',
          'discriptionProductStyle': '0.85.w',
        },
        '16/9': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '4.h',
          'discriptionProductStyle': '2.h',
        },
        '16/10': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '3.w',
          'discriptionProductStyle': '2.h',
        },
        '4/3': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '4.h',
          'discriptionProductStyle': '2.h',
        },
        'default': {
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
          'categoryNameStyle': '4.h',
          'discriptionProductStyle': '2.h',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '',
      },
      //Danh mục 1: pro 1, 2 ,3 ,4 , 5 ,6 ,7 ,8
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$24.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$25.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //column 1: image 1+ image 2
      // image 1: nail salon logo
      // image 2: decoration image
      'image1': {
        'id': 'logo',
        'key': 'logo',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/logo2-scaled.jpg',
      },
      'image2': {
        'key': 'image2',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_img_landscape-scaled.jpg',
      },
      //column 3: image 3 + image 4
      'image3': {
        'key': 'image3',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_illustrate2.png',
      },
      'image4': {
        'key': 'image4',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/nail_temp1_illustrate1-scaled.jpg',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      // begin: container
      "widgetName": "Container",
      "widgetSetting": {
        // "padding": {"vertical": "5.w"},
        // "decoration": {
        //   "color": "white",
        //   "boxShadow": {
        //     "color": {
        //       "grey": {"opacity": 0.5}
        //     },
        //     "spreadRadius": "5",
        //     "blurRadius": "7",
        //     "offset": "0 3"
        //   }
        // }
        "child": {
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                //column A
                "key": "expanded-A",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "top": "5.h",
                            "right": "6.3.w",
                            "child": {
                              "key": "container-A1",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "90.h",
                                "width": "23.7.w",
                                // "alignment": "centerLeft",
                                "child": {
                                  "widgetName": "image",
                                  "widgetSetting": {
                                    "imageId": "image2", //key = image2
                                    "fixCover": "true"
                                  },
                                },
                              },
                            },
                          },
                        },
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "top": "0",
                            "right": "3.w",
                            "child": {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "70.h",
                                "width": "23.4.w",
                                "color": "0xEEECEFF1",
                                "child": {
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {
                                          "width": "20.h",
                                          "height": "20.h",
                                          "child": {
                                            "widgetName": "image",
                                            "widgetSetting": {
                                              "imageId": "image1",
                                              "fixCover": "false"
                                            },
                                          }
                                        }
                                      },
                                      {
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "SPA",
                                          "style": {
                                            "letterSpacing": "1.w",
                                            "fontSize": "10.h",
                                            "fontFamily": "Gravitas One",
                                            "fontWeight": "normal",
                                          }
                                        }
                                      },
                                      {
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "CARE YOUR BODY",
                                          "style": {
                                            "fontSize": "2.w",
                                            "fontWeight": "w800",
                                            "fontFamily": "Arvo"
                                          }
                                        }
                                      }
                                    ]
                                  }
                                },
                              },
                            },
                          },
                        },
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "bottom": "0",
                            "right": "3.w",
                            "child": {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "25.h",
                                "width": "23.4.w",
                                "color": "0xCCFCE4EC",
                                "child": {
                                  "widgetName": "Center",
                                  "widgetSetting": {
                                    "child": {
                                      "widgetName": "Text",
                                      "widgetSetting": {
                                        "data": "OPEN FROM 9AM TO 6PM",
                                        "style": {
                                          "fontSize": "3.h",
                                          "fontWeight": "w800",
                                          "fontFamily": "Arvo"
                                        }
                                      },
                                    },
                                  },
                                },
                              },
                            },
                          },
                        }
                      ],
                    },
                  }
                },
              },
              {
                //column B
                "key": "expanded-B",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "widgetName": "Center",
                    "widgetSetting": {
                      "child": {
                        "key": "CTSC-B1",
                        "widgetName": "categoryTitleSingleColumn",
                        "widgetSetting": {
                          'colorCategoryTitle': "0xFF000000",
                          'colorProductName': '0xFF000000',
                          'colorDescription': '0xFF000000',
                          'runSpacing': '1.h',
                          'mainAxisAlignment': 'center',
                          "dataList": {
                            'category': "category1",
                            'product': [
                              "product1",
                              "product2",
                              "product3",
                              "product4",
                              "product5",
                              "product6",
                              "product7",
                              "product8",
                            ],
                          },
                          "useBackgroundCategoryTitle": "false",
                          "colorPrice": "0xFF000000",
                        },
                      },
                    },
                  },
                },
              },
              //column C
              {
                "key": "expanded-C",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "top": "4.h",
                            "left": "3.w",
                            "child": {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "44.h",
                                "width": "27.3.w",
                                "decoration": {
                                  "image": "image3",
                                  "boxFit": "cover",
                                },
                              },
                            },
                          },
                        },
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "bottom": "4.h",
                            "left": "3.w",
                            "child": {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "44.h",
                                "width": "27.3.w",
                                "decoration": {
                                  "image": "image4",
                                  "boxFit": "cover",
                                },
                              },
                            },
                          },
                        },
                        {
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "right": "6.w",
                            "child": {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "5.w",
                                "height": "100.h",
                                "color": "0xEEECEFF1",
                                "child": {
                                  "widgetName": "RotatedBox",
                                  "widgetSetting": {
                                    "quarterTurns": "3",
                                    "child": {
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "mainAxisAlignment": "spaceAround",
                                        "children": [
                                          {
                                            "widgetName": "Text",
                                            "widgetSetting": {
                                              "data": "wwww.salon.wwww",
                                              "style": {
                                                "fontSize": "1.2.w",
                                                "fontFamily": "Arvo"
                                              },
                                            },
                                          },
                                          {
                                            "widgetName": "Text",
                                            "widgetSetting": {
                                              "data": "@salon.com",
                                              "style": {
                                                "fontSize": "1.2.w",
                                                "fontFamily": "Arvo"
                                              },
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  },
                                },
                              },
                            },
                          },
                        },
                      ],
                    },
                  }
                },
              },
            ],
          },
        },
      },
    },
  };
  static final templateNail2 = {
    "_id": "",
    "templateSlug": "temp_nail_2",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'w700',
          'fontFamily': 'Nunito Sans',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Nunito Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Nunito Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.w',
          'nameProductStyle': '1.w',
          'priceProductStyle': '0.9.w',
          "discriptionProductStyle": '0.55.w',
        },
        '21/9': {
          'categoryNameStyle': '1.6.w',
          'nameProductStyle': '1.4.w',
          'priceProductStyle': '1.4.w',
          "discriptionProductStyle": '0.8.w',
        },
        '16/9': {
          'categoryNameStyle': '1.7.w',
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          "discriptionProductStyle": '1.3.w',
        },
        '16/10': {
          'categoryNameStyle': '1.9.w',
          'nameProductStyle': '1.7.w',
          'priceProductStyle': '1.7.w',
          "discriptionProductStyle": '1.1.w',
        },
        '4/3': {
          'categoryNameStyle': '2.8.w',
          'nameProductStyle': '2.4.w',
          'priceProductStyle': '2.4.w',
          "discriptionProductStyle": '1.2.w',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.25.w',
          'priceProductStyle': '1.25.w',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '',
      },
      //Danh mục 1: pro 1, 2 ,3 ,4 , 5
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$25',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      //Danh mục 2: pro 6,7,8,9,10
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$24.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$25.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },

      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 9',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 10',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      //Danh mục 3: pro 11,12

      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 2.1',
        'price': '\$32.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 2.2',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },

      'image1': {
        'id': 'image1',
        'key': 'image1',
        'type': 'image',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/image1.jpg',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      // begin: container
      "widgetName": "CustomContainer",
      "widgetSetting": {
        "color": "0x4400539C",
        "blendMode": "dstATop",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/background-scaled.jpg",
        // "boxFit": "cover",
        "width": "100.w",
        "height": "100.h",
        "child": {
          "widgetName": "Row",
          "widgetSetting": {
            "children": [
              {
                //column A
                "widgetName": "Container",
                "widgetSetting": {
                  "width": "33.33.w",
                  "padding": {"bottom": "4.5.h", "horizontal": "3.w"},
                  "child": {
                    "widgetName": "Column",
                    "widgetSetting": {
                      "children": [
                        {
                          "widgetName": "SizedBox",
                          "widgetSetting": {
                            "height": "3.h",
                          },
                        },
                        {
                          "widgetName": "Divider",
                          "widgetSetting": {
                            "thickness": "2",
                            "height": "6.h",
                            "color": "0x8F634201",
                          },
                        },
                        {
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "widgetName": "Column",
                              "widgetSetting": {
                                "mainAxisAlignment": "spaceBetween",
                                "children": [
                                  {
                                    "widgetName": "categoryTitle",
                                    "widgetSetting": {
                                      "categoryId": "category1",
                                      "color": "0xFF895B3A"
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product1",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product2",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product3",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product4",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product5",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                ],
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                }
              },
              {
                "widgetName": "VerticalDivider",
                "widgetSetting": {
                  "thickness": "2",
                  "color": "0x8F634201",
                },
              },
              {
                //column B
                "widgetName": "Container",
                "widgetSetting": {
                  "width": "33.33.w",
                  "padding": {"bottom": "4.5.h", "horizontal": "3.w"},
                  "child": {
                    "widgetName": "Column",
                    "widgetSetting": {
                      "children": [
                        {
                          "widgetName": "SizedBox",
                          "widgetSetting": {
                            "height": "3.h",
                          },
                        },
                        {
                          "widgetName": "Row",
                          "widgetSetting": {
                            "children": [
                              {
                                "widgetName": "Expanded",
                                "widgetSetting": {
                                  "child": {
                                    "widgetName": "Divider",
                                    "widgetSetting": {
                                      "thickness": "2",
                                      "height": "6.h",
                                      "color": "0x8F634201",
                                    },
                                  },
                                },
                              },
                              {
                                "widgetName": "Center",
                                "widgetSetting": {
                                  "child": {
                                    "widgetName": "Container",
                                    "widgetSetting": {
                                      "padding": {"horizontal": "3.w"},
                                      "color": "0x00000000",
                                      "height": "6.h",
                                      "child": {
                                        "widgetName": "Center",
                                        "widgetSetting": {
                                          "child": {
                                            "widgetName": "Text",
                                            "widgetSetting": {
                                              "data": "OUR TREATMENTS",
                                              "style": {
                                                "fontSize": "3.h",
                                                "fontWeight": "w700",
                                                "color": "0xFF895B3A",
                                                "fontFamily": "Nunito Sans",
                                              },
                                            },
                                          },
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                "widgetName": "Expanded",
                                "widgetSetting": {
                                  "child": {
                                    "widgetName": "Divider",
                                    "widgetSetting": {
                                      "thickness": "2",
                                      "height": "6.h",
                                      "color": "0x8F634201",
                                    },
                                  },
                                },
                              },
                            ],
                          },
                        },
                        {
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "widgetName": "Column",
                              "widgetSetting": {
                                "mainAxisAlignment": "spaceBetween",
                                "children": [
                                  {
                                    "widgetName": "categoryTitle",
                                    "widgetSetting": {
                                      "categoryId": "category2",
                                      "color": "0xFF895B3A"
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product6",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product7",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product8",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product9",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                  {
                                    "widgetName": "ServiceItemColumn",
                                    "widgetSetting": {
                                      "productId": "product10",
                                      "colorPrice": "0xFFA76744",
                                      "colorProductName": "0xFFA76744",
                                      "colorDescription": "0xFF000000",
                                      "width": "23.w",
                                      "useMediumStyle": "false",
                                    },
                                  },
                                ],
                              },
                            },
                          },
                        },
                      ],
                    },
                  },
                }
              },
              {
                "widgetName": "VerticalDivider",
                "widgetSetting": {
                  "thickness": "2",
                  "color": "0x8F634201",
                },
              },
              {
                //column C
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"bottom": "4.5.h", "horizontal": "3.w"},
                      "child": {
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "height": "3.h",
                              },
                            },
                            {
                              "widgetName": "Divider",
                              "widgetSetting": {
                                "thickness": "2",
                                "height": "6.h",
                                "color": "0x8F634201",
                              },
                            },
                            {
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "spaceAround",
                                    "children": [
                                      {
                                        "widgetName": "categoryTitle",
                                        "widgetSetting": {
                                          "categoryId": "category3",
                                          "color": "0xFF895B3A"
                                        },
                                      },
                                      {
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "productId": "product11",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w",
                                          "useMediumStyle": "false",
                                        },
                                      },
                                      {
                                        "widgetName": "ServiceItemColumn",
                                        "widgetSetting": {
                                          "productId": "product12",
                                          "colorPrice": "0xFFA76744",
                                          "colorProductName": "0xFFA76744",
                                          "colorDescription": "0xFF000000",
                                          "width": "23.w",
                                          "useMediumStyle": "false",
                                        },
                                      },
                                    ],
                                  },
                                },
                              },
                            },
                            {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "color": "0xFF895B3A",
                                "height": "18.h",
                                "width": "20.w",
                                "child": {
                                  "widgetName": "Center",
                                  "widgetSetting": {
                                    "child": {
                                      "widgetName": "Text",
                                      "widgetSetting": {
                                        "data":
                                            "GET 10% OFF \nYOUR FIRST PAYMENT",
                                        "style": {
                                          "color": "0xFFFFFFFF",
                                          "fontSize": "1.5.w",
                                          "fontWeight": "w500",
                                          "fontFamily": "Nunito Sans"
                                        }
                                      },
                                    },
                                  },
                                },
                              },
                            },
                            {
                              "widgetName": "SizedBox",
                              "widgetSetting": {"height": "5.h"}
                            },
                            {
                              "widgetName": "Container",
                              "widgetSetting": {
                                "height": "20.h",
                                "width": "26.w",
                                "decoration": {
                                  "image":
                                      "image1", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
                                  "boxFit": "cover",
                                },
                              },
                            },
                          ],
                        },
                      },
                    },
                  },
                }
              },
            ]
          },
        },
      },
    },
  };
  static final templateNail3 = {
    "_id": "",
    "templateSlug": "temp_nail_3",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Cormorant Garamond',
        },
        'nameProductStyle': {
          'fontWeight': 'w600',
          'fontFamily': 'Cormorant Garamond',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Cormorant Garamond',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Lato',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Lato',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Cormorant Garamond',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Cormorant Garamond',
        },
      },
      'renderScreen': {
        '32/9': {
          'nameProductStyle': '1.w',
          'priceProductStyle': '1.w',
          'discriptionProductStyle': '0.9.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '16.h',
        },
        '21/9': {
          'nameProductStyle': '1.5.w',
          'priceProductStyle': '1.5.w',
          'discriptionProductStyle': '1.2.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '18.h',
        },
        '16/9': {
          'nameProductStyle': '1.75.w',
          'priceProductStyle': '1.75.w',
          'imageHeight': '25.h',
          'discriptionProductStyle': '1.25.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '20.h',
        },
        '16/10': {
          'nameProductStyle': '1.8.w',
          'priceProductStyle': '1.8.w',
          'imageHeight': '25.h',
          'discriptionProductStyle': '1.25.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '20.h',
        },
        '4/3': {
          'nameProductStyle': '2.w',
          'priceProductStyle': '2.w',
          'discriptionProductStyle': '1.5.w',

          // 'widthThumnailProduct': '28.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '22.h',
        },
        'default': {
          'nameProductStyle': '1.75.w',
          'priceProductStyle': '1.75.w',
          'imageHeight': '25.h',
          'discriptionProductStyle': '1.25.w',
          'widthThumnailProduct': '28.w',
          'heightThumnailProduct': '20.h',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_1.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_2.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_3.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$23.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_4.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$23.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_5.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$23.0',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_6.jpg',
        'description':
            'Lorem ipsum dolor sit am adasda sda ting dolore magna al',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      // begin: container
      "widgetName": "Container",
      "widgetSetting": {
        "color": "0xFFF2EFEA",
        "width": "100.w",
        "height": "100.h",
        "padding": {"vertical": "3.h"},
        "child": {
          "widgetName": "Column",
          "widgetSetting": {
            "children": [
              {
                "widgetName": "Divider",
                "widgetSetting": {
                  "thickness": "2",
                  "height": "4.h",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "endIndent": "20.w",
                },
              },
              {
                "widgetName": "Text",
                "widgetSetting": {
                  "data": "NAIL SALON",
                  "style": {
                    "fontSize": "4.h",
                    "color": "0xFF616148", //primary color
                    "fontWeight": "w800",
                    "fontFamily": "Cormorant Garamond",
                  }
                },
              },
              {
                "widgetName": "Text",
                "widgetSetting": {
                  "data": "NAIL IT!",
                  "style": {
                    "fontSize": "3.h",
                    "color": "0xFF616148", //primary color
                    "fontWeight": "w800",
                    "fontFamily": "Cormorant Garamond",
                  } //secondary color
                },
              },
              {
                "widgetName": "Divider",
                "widgetSetting": {
                  "thickness": "2",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "height": "4.h",
                  "endIndent": "20.w",
                },
              },
              {
                "widgetName": "Wrap",
                "widgetSetting": {
                  "runSpacing": "2.h",
                  "spacing": "4.w",
                  "children": [
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product1",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product2",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product3",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product4",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product5",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                    {
                      "widgetName": "ServiceItemWithThumbnail",
                      "widgetSetting": {
                        "productId": "product6",
                        "toUpperCaseNameProductName": "true",
                        "colorPrice": "0xFF5C594F",
                        "colorProductName": "0xFF5C594F",
                        "colorDescription": "0xFF5C594F",
                      }
                    },
                  ],
                }
              },
              {
                "widgetName": "Divider",
                "widgetSetting": {
                  "thickness": "2",
                  "color": "0xFF616148",
                  "indent": "20.w",
                  "height": "4.h",
                  "endIndent": "20.w",
                },
              },
              {
                "widgetName": "Row",
                "widgetSetting": {
                  "mainAxisAlignment": "spaceAround",
                  "children": [
                    {
                      "widgetName": "Text",
                      "widgetSetting": {
                        "data": "salon@hairdresser",
                        "style": {
                          "color": "0xFF616148",
                          'fontSize': '3.h',
                          'fontFamily': 'Cormorant Garamond',
                        },
                      },
                    },
                    {
                      "widgetName": "Text",
                      "widgetSetting": {
                        "data": "+012345689",
                        "style": {
                          "color": "0xFF616148",
                          'fontSize': '3.h',
                          'fontFamily': 'Neuton',
                        },
                      },
                    },
                    {
                      "widgetName": "Text",
                      "widgetSetting": {
                        "data": "hairdresser.com",
                        "style": {
                          "color": "0xFF616148",
                          'fontSize': '3.h',
                          'fontFamily': 'Cormorant Garamond',
                        },
                      },
                    }
                  ],
                },
              }
            ],
          }
        },
      },
    },
  };
  static final templateNail4 = {
    "_id": "",
    "templateSlug": "temp_nail_4",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Playfair Display',
        },
        'nameProductStyle': {
          'fontWeight': 'w500',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'priceProductStyle': {
          'fontWeight': 'w500',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.3.w',
          // [112320TIN] fix oveflow product list
          'nameProductStyle': '0.9.w',
          'priceProductStyle': '0.9.w',
        },
        '21/9': {
          'categoryNameStyle': '1.9.w',
          // [112320TIN] fix oveflow product list
          'nameProductStyle': '1.4.w',
          'priceProductStyle': '1.4.w',
        },
        '16/9': {
          'categoryNameStyle': '3.2.h',
          'nameProductStyle': '2.6.h',
          'priceProductStyle': '2.6.h',
        },
        '16/10': {
          'categoryNameStyle': '3.h',
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
        },
        '4/3': {
          'categoryNameStyle': '3.2.w',
          'nameProductStyle': '2.2.w',
          'priceProductStyle': '2.2.w',
        },
        'default': {
          'categoryNameStyle': '3.h',
          'nameProductStyle': '2.5.h',
          'priceProductStyle': '2.5.h',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '',
      },

      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '',
      },
      //Danh mục 1: pro 1, 2 ,3 ,4 , 5
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      //Danh mục 2: pro 6, 7 ,8 ,9 , 10
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 2.1',
        'price': '\$23.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 2.2',
        'price': '\$24.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 2.3',
        'price': '\$25.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },

      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 2.4',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 2.5',
        'price': '\$32.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },

      //Danh mục 3: pro 11,12,13,14,15

      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 3.1',
        'price': '\$32.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 3.2',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product13': {
        'key': 'product13',
        'type': 'product',
        'name': 'Product 3.3',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product14': {
        'key': 'product14',
        'type': 'product',
        'name': 'Product 3.4',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product15': {
        'key': 'product15',
        'type': 'product',
        'name': 'Product 3.5',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      //Danh mục 4: pro 16,17,18,19,20

      'product16': {
        'key': 'product16',
        'type': 'product',
        'name': 'Product 4.1',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product17': {
        'key': 'product17',
        'type': 'product',
        'name': 'Product 4.2',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product18': {
        'key': 'product13',
        'type': 'product',
        'name': 'Product 4.3',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product19': {
        'key': 'product19',
        'type': 'product',
        'name': 'Product 4.4',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },
      'product20': {
        'key': 'product19',
        'type': 'product',
        'name': 'Product 4.5',
        'price': '\$33.0',
        'salePrice': '',
        'image': '',
        'description': '',
      },

      'image1': {
        'id': 'image1',
        'key': 'image1',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_1-1.jpg',
      },
      'image2': {
        'id': 'image2',
        'key': 'image2',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_2-1.jpg',
      },
      'image3': {
        'id': 'image3',
        'key': 'image3',
        'type': 'image',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/treatment_3-1.jpg',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      // begin: container
      "widgetName": "Container",
      "widgetSetting": {
        "width": "100.w",
        "height": "100.h",
        "color": "0xFFF1F1EF",
        "child": {
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              {
                "widgetName": "Row",
                "widgetSetting": {
                  "children": [
                    {
                      //column A
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": '30',
                        "child": {
                          "widgetName": "Padding",
                          "widgetSetting": {
                            "padding": {
                              "horizontal": "2.w",
                              "top": "5.h",
                            },
                            "child": {
                              "widgetName": "Column",
                              "widgetSetting": {
                                'mainAxisAlignment': 'spaceAround',
                                "children": [
                                  {
                                    "key": "CTSC-A1",
                                    "widgetName": "categoryTitleSingleColumn",
                                    "widgetSetting": {
                                      'spaceBetweenCategoryProductList': '3.h',
                                      'colorCategoryTitle': "0xFF333A32",
                                      'colorProductName': '0xFF333A32',
                                      'colorDescription': '0xFF333A32',
                                      "dataList": {
                                        'category': "category1",
                                        'product': [
                                          "product1",
                                          "product2",
                                          "product3",
                                          "product4",
                                          "product5",
                                        ],
                                      },
                                      "indexCategory": "0",
                                      "useBackgroundCategoryTitle": "false",
                                      "startIndexProduct": "0",
                                      "endIndexProduct": "8",
                                      "colorPrice": "0xFF333A32",
                                    }
                                  },
                                  {
                                    "key": "CTSC-A2",
                                    "widgetName": "categoryTitleSingleColumn",
                                    "widgetSetting": {
                                      'spaceBetweenCategoryProductList': '3.h',
                                      'colorCategoryTitle': "0xFF333A32",
                                      'colorProductName': '0xFF333A32',
                                      'colorDescription': '0xFF333A32',
                                      "colorPrice": "0xFF333A32",
                                      "dataList": {
                                        'category': "category2",
                                        'product': [
                                          "product6",
                                          "product7",
                                          "product8",
                                          "product9",
                                          "product10",
                                        ],
                                      },
                                      "indexCategory": "0",
                                      "useBackgroundCategoryTitle": "false",
                                      "startIndexProduct": "0",
                                      "endIndexProduct": "8",
                                    }
                                  }
                                ],
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      "widgetName": "VerticalDivider",
                      "widgetSetting": {
                        "thickness": "1",
                        "color": "0xFFBCBCBA ",
                      },
                    },
                    {
                      //column B
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": '55',
                        "child": {
                          "widgetName": "Stack",
                          "widgetSetting": {
                            "children": [
                              {
                                "widgetName": "Row",
                                "widgetSetting": {
                                  "children": [
                                    {
                                      "widgetName": "Expanded",
                                      "widgetSetting": {
                                        "flex": "6",
                                        "child": {
                                          "widgetName": "Text",
                                          "widgetSetting": {"data": ""},
                                        }
                                      },
                                    },
                                    {
                                      "widgetName": "Expanded",
                                      "widgetSetting": {
                                        "flex": "4",
                                        "child": {
                                          "widgetName": "Container",
                                          "widgetSetting": {
                                            "height": "100.h",
                                            "color":
                                                "0xFFCDC6B6", //secondary color
                                            "child": {
                                              "widgetName": "Text",
                                              "widgetSetting": {"data": ""},
                                            },
                                          },
                                        }
                                      },
                                    },
                                  ],
                                },
                              },
                              {
                                "widgetName": "Padding",
                                "widgetSetting": {
                                  "padding": {
                                    "top": "5.h",
                                    "left": "2.w",
                                  },
                                  "child": {
                                    "widgetName": "Row",
                                    "widgetSetting": {
                                      "children": [
                                        {
                                          "widgetName": "Expanded",
                                          "widgetSetting": {
                                            "flex": "1",
                                            "child": {
                                              "widgetName": "Column",
                                              "widgetSetting": {
                                                'mainAxisAlignment':
                                                    'spaceAround',
                                                "children": [
                                                  {
                                                    "key": "CTSC-B1",
                                                    "widgetName":
                                                        "categoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      'spaceBetweenCategoryProductList':
                                                          '3.h',
                                                      'colorCategoryTitle':
                                                          "0xFF333A32",
                                                      'colorProductName':
                                                          '0xFF333A32',
                                                      'colorDescription':
                                                          '0xFF333A32',
                                                      "dataList": {
                                                        'category': "category3",
                                                        'product': [
                                                          "product11",
                                                          "product12",
                                                          "product13",
                                                          "product14",
                                                          "product15",
                                                        ],
                                                      },
                                                      "indexCategory": "0",
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "startIndexProduct": "0",
                                                      "endIndexProduct": "8",
                                                      "colorPrice":
                                                          "0xFF333A32",
                                                    }
                                                  },
                                                  {
                                                    "key": "CTSC-B2",
                                                    "widgetName":
                                                        "categoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      'spaceBetweenCategoryProductList':
                                                          '3.h',
                                                      'colorCategoryTitle':
                                                          "0xFF333A32",
                                                      'colorProductName':
                                                          '0xFF333A32',
                                                      'colorDescription':
                                                          '0xFF333A32',
                                                      "dataList": {
                                                        'category': "category4",
                                                        'product': [
                                                          "product16",
                                                          "product17",
                                                          "product18",
                                                          "product19",
                                                          "product20",
                                                        ],
                                                      },
                                                      "indexCategory": "0",
                                                      "useBackgroundCategoryTitle":
                                                          "false",
                                                      "startIndexProduct": "0",
                                                      "endIndexProduct": "8",
                                                      "colorPrice":
                                                          "0xFF333A32",
                                                    }
                                                  }
                                                ],
                                              },
                                            },
                                          },
                                        },
                                        {
                                          "widgetName": "Expanded",
                                          "widgetSetting": {
                                            "flex": "1",
                                            "child": {
                                              "widgetName": "Column",
                                              "widgetSetting": {
                                                "mainAxisAlignment":
                                                    "spaceAround",
                                                "children": [
                                                  {
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "height": "25.h",
                                                      "width": "23.w",
                                                      "decoration": {
                                                        "image": "image1",
                                                        "boxFit": "cover",
                                                      },
                                                    },
                                                  },
                                                  {
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "height": "25.h",
                                                      "width": "23.w",
                                                      "decoration": {
                                                        "image": "image2",
                                                        "boxFit": "cover",
                                                      },
                                                    },
                                                  },
                                                  {
                                                    "widgetName": "Container",
                                                    "widgetSetting": {
                                                      "height": "25.h",
                                                      "width": "23.w",
                                                      "decoration": {
                                                        "image": "image3",
                                                        "boxFit": "cover",
                                                      },
                                                    },
                                                  },
                                                ],
                                              },
                                            },
                                          },
                                        }
                                      ],
                                    }
                                  },
                                },
                              },
                            ],
                          },
                        },
                      },
                    },
                    {
                      //column C
                      "widgetName": "Expanded",
                      "widgetSetting": {
                        "flex": '15',
                        "child": {
                          "widgetName": "RotatedBox",
                          "widgetSetting": {
                            "quarterTurns": "3",
                            "child": {
                              "widgetName": "Center",
                              "widgetSetting": {
                                "child": {
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "data": "PRO NAIL SALON",
                                    "style": {
                                      "fontFamily": "Playfair Display",
                                      "fontSize": "5.w",
                                      "fontWeight": "bold",
                                    },
                                  },
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  ],
                },
              },
            ]
          },
        },
      },
    },
  };
  static final templateNail5 = {
    "_id": "",
    "templateSlug": "temp_nail_5",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'DM Serif Display',
        },
        'nameProductStyle': {
          'fontWeight': 'w500',
          'fontFamily': 'DM Serif Display',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'bold',
          'fontFamily': 'DM Serif Display',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'DM Serif Text',
          'height': '1.5',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w600',
          'fontFamily': 'DM Serif Display',
        },
        'priceProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyleMedium': '0.8.w',
          'priceProductStyleMedium': '1.2.w',
          'discriptionProductStyleMedium': '0.6.w',
        },
        '21/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyleMedium': '1.2.w',
          'priceProductStyleMedium': '1.5.w',
          'discriptionProductStyleMedium': '0.9.w',
        },
        '16/9': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyleMedium': '1.5.w',
          'priceProductStyleMedium': '2.4.w',
          'discriptionProductStyleMedium': '1.w',
        },
        '16/10': {
          'categoryNameStyleMedium': '3.w',
          'nameProductStyleMedium': '1.4.w',
          'priceProductStyleMedium': '2.3.w',
          'discriptionProductStyleMedium': '1.w',
        },
        '4/3': {
          'categoryNameStyle': '1.8.w',
          'nameProductStyleMedium': '2.w',
          'priceProductStyleMedium': '3.w',
          'discriptionProductStyleMedium': '1.5.w',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyleMedium': '2.w',
          'priceProductStyleMedium': '2.w',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '',
      },

      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '',
      },
      //Danh mục 1: pro 1, 2 ,3 ,4 , 5
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$23.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$23.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      //Danh mục 2: pro 6, 7 ,8 ,9 , 10
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$23.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$24.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$25.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },

      'image1': {
        'id': 'image1',
        'key': 'image1',
        'type': 'image',
        'image':
            'https://foodiemenu.co/wp-content/uploads/2023/10/close-up-woman-getting-manicure-done_23-2149171309.jpg',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      // begin: container
      "widgetName": "Container",
      "widgetSetting": {
        "width": "100.w",
        "height": "100.h",
        "color": "0xFFFFFFFF",
        "child": {
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              //background
              {
                "widgetName": "Column",
                "widgetSetting": {
                  "children": [
                    {
                      "widgetName": "Container",
                      "widgetSetting": {
                        "height": "5.h",
                        "color": "0xFFA76744", //primary color
                      },
                    },
                    {
                      "widgetName": "Expanded",
                      "widgetSetting": {"": ""},
                    },
                    {
                      "widgetName": "Container",
                      "widgetSetting": {
                        "height": "20.h",
                        "color": "0xFF2E292D",
                        "child": {
                          "widgetName": "Padding",
                          "widgetSetting": {
                            "padding": {
                              "left": "5.w",
                              "top": "2.h",
                              "bottom": "2.h",
                            },
                            "child": {
                              "widgetName": "Column",
                              "widgetSetting": {
                                "crossAxisAlignment": "start",
                                "mainAxisAlignment": "spaceAround",
                                "children": [
                                  {
                                    "widgetName": "Row",
                                    "widgetSetting": {
                                      "children": [
                                        {
                                          "widgetName": "CustomContainer",
                                          "widgetSetting": {
                                            "color": "0xFFA76744",
                                            "blendMode": "modulate",
                                            "height": "8.h",
                                            "width": "8.h",
                                            "image":
                                                "http://foodiemenu.co/wp-content/uploads/2023/09/white-globe-icon-24-2.png",
                                          },
                                        },
                                        {
                                          "widgetName": "Text",
                                          "widgetSetting": {
                                            "data": "WWW.SALON.WWW",
                                            "style": {
                                              "fontFamily": "DM Serif Display",
                                              "color": "0xFFFFFFFF",
                                              "fontSize": "2.5.h",
                                            },
                                          },
                                        },
                                        {
                                          "widgetName": "SizedBox",
                                          "widgetSetting": {"width": "5.w"},
                                        },
                                        {
                                          "widgetName": "CustomContainer",
                                          "widgetSetting": {
                                            "color": "0xFFA76744",
                                            "blendMode": "modulate",
                                            "height": "6.h",
                                            "width": "6.h",
                                            "image":
                                                "http://foodiemenu.co/wp-content/uploads/2023/09/pinpng.com-contact-icon-png-666849.png",
                                          },
                                        },
                                        {
                                          "widgetName": "Text",
                                          "widgetSetting": {
                                            "data": "+1223 334 444",
                                            "style": {
                                              "fontFamily": "DM Serif Display",
                                              "color": "0xFFFFFFFF",
                                              "fontSize": "2.5.h",
                                            },
                                          },
                                        },
                                      ],
                                    },
                                  }
                                ],
                              },
                            },
                          },
                        },
                      },
                    },
                  ],
                },
              },
              //content
              {
                "widgetName": "Padding",
                "widgetSetting": {
                  "padding": {"top": "5.h"},
                  "child": {
                    "widgetName": "Row",
                    "widgetSetting": {
                      "children": [
                        {
                          //left column
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "widgetName": "Center",
                              "widgetSetting": {
                                "child": {
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "crossAxisAlignment": "start",
                                    "children": [
                                      {
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {"height": "3.h"},
                                      },
                                      {
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "BEAUTY SERVICES",
                                          "style": {
                                            "fontFamily": "DM Serif Display",
                                            "color": "0xFF000000",
                                            "fontSize": "5.h",
                                            "fontWeight": "bold",
                                          },
                                        },
                                      },
                                      {
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {"height": "2.h"},
                                      },
                                      {
                                        "widgetName": "Wrap",
                                        "widgetSetting": {
                                          "spacing": "2.w",
                                          "runSpacing": "4.h",
                                          "children": [
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "productId": "product1",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "colorDescription":
                                                    "0xFF000000",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product2",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product3",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product4",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product5",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product6",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product7",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product8",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                          ],
                                        },
                                      },
                                    ],
                                  },
                                },
                              },
                            },
                          },
                        },
                        {
                          //right column
                          "widgetName": "SizedBox",
                          "widgetSetting": {
                            "width": "45.w",
                            "child": {
                              "widgetName": "Column",
                              "widgetSetting": {
                                "children": [
                                  {
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "height": "5.h",
                                    },
                                  },
                                  {
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "height": "20.h",
                                      "child": {
                                        "widgetName": "Row",
                                        "widgetSetting": {
                                          "mainAxisAlignment": "spaceBetween",
                                          "children": [
                                            {
                                              "widgetName": "Expanded",
                                              "widgetSetting": {
                                                "child": {
                                                  "widgetName": "Column",
                                                  "widgetSetting": {
                                                    "crossAxisAlignment":
                                                        "start",
                                                    "children": [
                                                      {
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data":
                                                              "Our Missions",
                                                          "style": {
                                                            "color":
                                                                "0xFFA76744",
                                                            "fontWeight":
                                                                "bold",
                                                            "fontSize": "3.5.h",
                                                            "fontFamily":
                                                                "DM Serif Display"
                                                          },
                                                          "textAlign": "left",
                                                        }
                                                      },
                                                      {
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data": "Lorem ipsum dolor sit amet consectetur,"
                                                              "Lorem ipsum dolor sit amet, consectetur"
                                                              "Lorem ipsum dolor sit amet, consectetur",
                                                          "style": {
                                                            "fontFamily":
                                                                "DM Serif Text",
                                                            "fontSize": "2.h",
                                                            "color":
                                                                "0xFF000000",
                                                          },
                                                          "textAlign": "left",
                                                        }
                                                      }
                                                    ],
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              "widgetName": "SizedBox",
                                              "widgetSetting": {"width": "4.w"},
                                            },
                                            {
                                              "widgetName": "Expanded",
                                              "widgetSetting": {
                                                "child": {
                                                  "widgetName": "Column",
                                                  "widgetSetting": {
                                                    "crossAxisAlignment":
                                                        "start",
                                                    "children": [
                                                      {
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data":
                                                              "Our Missions",
                                                          "style": {
                                                            "color":
                                                                "0xFFA76744",
                                                            "fontWeight":
                                                                "bold",
                                                            "fontSize": "3.5.h",
                                                            "fontFamily":
                                                                "DM Serif Display"
                                                          },
                                                          "textAlign": "left",
                                                        }
                                                      },
                                                      {
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data": "Lorem ipsum dolor sit amet con sectetur, "
                                                              "Lorem ipsum dolor sit amet, consectetur"
                                                              "Lorem ipsum dolor sit amet, consectetur",
                                                          "style": {
                                                            "fontFamily":
                                                                "DM Serif Text",
                                                            "fontSize": "2.h",
                                                            "color":
                                                                "0xFF000000",
                                                          },
                                                          "textAlign": "left",
                                                        }
                                                      }
                                                    ],
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              "widgetName": "SizedBox",
                                              "widgetSetting": {"width": "4.w"},
                                            },
                                          ],
                                        },
                                      },
                                    },
                                  },
                                  {
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "height": "55.h",
                                      "child": {
                                        "widgetName": "Stack",
                                        "widgetSetting": {
                                          "children": [
                                            {
                                              "widgetName": "CustomContainer",
                                              "widgetSetting": {
                                                "height": "50.h",
                                                'opacity': '1',
                                                "blendMode": "color",
                                                "fit": "contain",
                                                "alignment": "topLeft",
                                                "color": "0xFFA76744",
                                                "image":
                                                    "https://foodiemenu.co/wp-content/uploads/2023/10/outline-vector-flower-illustration-corner-border-frame-floral-elements-coloring-page-outline-vector-flower-illustration-189504420-e1696476093637.webp",
                                              },
                                            },
                                            {
                                              "widgetName": "Container",
                                              "widgetSetting": {
                                                "height": "50.h",
                                                "decoration": {
                                                  "color": "0xAAFFFFFF",
                                                  "opacity": "1.0",
                                                }
                                              },
                                            },
                                            {
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "right": "0",
                                                "bottom": "0",
                                                "child": {
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "height": "50.h",
                                                    "width": "30.w",
                                                    "color": "0xFFA76744",
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "right": "21.5.w",
                                                "top": "5.5.h",
                                                "child": {
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "height": "9.h",
                                                    "width": "10.w",
                                                    "child": {
                                                      "widgetName": "Center",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "widgetName": "Text",
                                                          "widgetSetting": {
                                                            "data":
                                                                "DISCOUNT OF\nTHE MONTH",
                                                            "style": {
                                                              "fontFamily":
                                                                  "DM Serif Text",
                                                              "color":
                                                                  "0xFFFFFFFF",
                                                              "fontSize":
                                                                  "2.5.sp",
                                                            },
                                                          }
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "right": "0",
                                                "bottom": "2.5.h",
                                                "child": {
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "height": "37.5.h",
                                                    "width": "40.w",
                                                    "decoration": {
                                                      "image": "image1",
                                                      "boxFit": "cover",
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "top": "5.5.h",
                                                "right": "0",
                                                "child": {
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "color": "0xFFFFFFFF",
                                                    "height": "9.h",
                                                    "width": "23.w",
                                                    "child": {
                                                      "widgetName": "Center",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "widgetName": "Text",
                                                          "widgetSetting": {
                                                            "data": "50% OFF",
                                                            "style": {
                                                              "color":
                                                                  "0xFFA76744",
                                                              "fontWeight":
                                                                  "bold",
                                                              "fontSize": "6.h",
                                                              "fontFamily":
                                                                  "DM Serif Display"
                                                            },
                                                          },
                                                        }
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            }
                                          ],
                                        },
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                          },
                        }
                      ],
                    },
                  },
                },
              },
            ],
          },
        },
      },
    },
  };
  static final temppho1 = {
    "_id": "11",
    "templateSlug": "temp_pho_1",
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "21/9": {
          "categoryImageHeight": "25.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.sp",
          "productPriceFontSize": "2.sp",
          "normalFontSize": "2.sp",
          "brandFontSize": "10.sp",
          "verticalFramePadding": "4.sp",
          "horizontalFramePadding": "4.sp",
        },
        "16/9": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "16/10": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "5.4.sp",
          "productNameFontSize": "3.5.sp",
          "productPriceFontSize": "3.5.sp",
          "normalFontSize": "4.8.sp",
          "brandFontSize": "18.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "4/3": {
          "categoryImageHeight": "45.sp",
          "categoryFontSize": "7.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp",
          "normalFontSize": "5.8.sp",
          "brandFontSize": "22.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
        "default": {
          "categoryImageHeight": "35.sp",
          "categoryFontSize": "4.4.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "normalFontSize": "2.8.sp",
          "brandFontSize": "16.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
        },
      },
    },
    "isDeleted": false,
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1.jpg",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2.jpg",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3.jpg",
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_4.jpg",
      },

      //Danh mục 1: pro 1, 2, 3, 4, 5, 6, 7
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 2: pro 8, 9, 10, 11, 12, 13, 14
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 3: pro 15, 16, 17, 18, 19, 20, 21
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product21": {
        "key": "product21",
        "type": "product",
        "name": "Product 21",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 1: pro 22, 23, 24, 25, 26, 27, 28
      "product22": {
        "key": "product22",
        "type": "product",
        "name": "Product 22",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product23": {
        "key": "product23",
        "type": "product",
        "name": "Product 23",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product24": {
        "key": "product24",
        "type": "product",
        "name": "Product 24",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product25": {
        "key": "product25",
        "type": "product",
        "name": "Product 25",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product26": {
        "key": "product26",
        "type": "product",
        "name": "Product 26",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product27": {
        "key": "product27",
        "type": "product",
        "name": "Product 27",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product28": {
        "key": "product28",
        "type": "product",
        "name": "Product 28",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "decoration": {"color": "0xffffffff"},
        "child": {
          "key": "PaddingByRatio-main",
          "widgetName": "PaddingByRatio",
          "widgetSetting": {
            "verticalPaddingFieldKey": "verticalFramePadding",
            "horizontalPaddingFieldKey": "horizontalFramePadding",
            "child": {
              "key": "Center-main",
              "widgetName": "Center",
              "widgetSetting": {
                "child": {
                  "key": "Column-main",
                  "widgetName": "Column",
                  "widgetSetting": {
                    "mainAxisAlignment": "center",
                    "children": [
                      // Row 1: info(local, est)
                      {
                        "key": "Container-header",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "padding": {"vertical": "2.sp"},
                          "child": {
                            "key": "Row-info",
                            "widgetName": "Row",
                            "widgetSetting": {
                              "mainAxisAlignment": "spaceBetween",
                              "children": [
                                // address
                                {
                                  "key": "text-address",
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "data": "4889 Princess St",
                                    "fontSizeFromFieldKey": "normalFontSize",
                                    "style": {
                                      "color": "0xff404042",
                                    }
                                  }
                                },
                                // est
                                {
                                  "key": "text-est",
                                  "widgetName": "Text",
                                  "widgetSetting": {
                                    "data": "EST 1990",
                                    "fontSizeFromFieldKey": "normalFontSize",
                                    "style": {
                                      "color": "0xff404042",
                                    }
                                  }
                                }
                              ]
                            },
                          }
                        }
                      },
                      {
                        "key": "Container-divider",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "height": "1",
                          "decoration": {
                            "border": {
                              "color": "0xff404042",
                              "width": "0.1.sp",
                            }
                          },
                        }
                      },
                      // Row 2: brand
                      {
                        "key": "UnderlineBrand-brand",
                        "widgetName": "UnderlineBrand",
                        "widgetSetting": {
                          "brand": "PHO BRAND",
                          "fontSizeFromFieldKey": "brandFontSize",
                          "textStyle": {
                            "fontWeight": "w700",
                            "color": "0xff404042",
                          },
                          "bottomBorderSide": {
                            "width": "1.8.sp",
                            "color": "0xff404042 ",
                          }
                        }
                      },
                      // row 3: spacing
                      {
                        "key": "SizedBox-spacing-v-1",
                        "widgetName": "SizedBox",
                        "widgetSetting": {"height": "2.h"}
                      },

                      // row 4: menu
                      {
                        "key": "SizedBox-menu",
                        "widgetName": "SizedBox",
                        "widgetSetting": {
                          "width": "100.w",
                          "child": {
                            "key": "Row-menu",
                            "widgetName": "Row",
                            "widgetSetting": {
                              "direction": "horizontal",
                              "alignment": "spaceBetween",
                              "spacing": "1.sp",
                              "children": [
                                // menu column 1
                                {
                                  "key": "SizedBox-m-c-1",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "key": "CategoryColumn1-m-c-1",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category1",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000",
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          // Menu: Col1 - Pro1
                                          {
                                            "key": "ProductRow1-p1",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product1",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro2
                                          {
                                            "key": "ProductRow1-p2",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product2",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro3
                                          {
                                            "key": "ProductRow1-p3",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product3",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro4
                                          {
                                            "key": "ProductRow1-p4",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product4",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro5
                                          {
                                            "key": "ProductRow1-p5",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product5",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro6
                                          {
                                            "key": "ProductRow1-p6",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product6",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col1 - Pro7
                                          {
                                            "key": "ProductRow1-p7",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product7",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                        ]
                                      },
                                    }
                                  }
                                },
                                {
                                  "key": "SizedBox-cat-scpacing",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"width": "2.w"}
                                },
                                // menu column 2
                                {
                                  "key": "SizedBox-m-c-2",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "key": "CategoryColumn1-m-c-2",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category2",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000",
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          // Menu: Col2 - Pro8
                                          {
                                            "key": "ProductRow1-p8",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product8",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pro9
                                          {
                                            "key": "ProductRow1-p9",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product9",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pro10
                                          {
                                            "key": "ProductRow1-p10",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product10",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pr11
                                          {
                                            "key": "ProductRow1-p11",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product11",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pr12
                                          {
                                            "key": "ProductRow1-p12",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product12",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pr13
                                          {
                                            "key": "ProductRow1-p13",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product13",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col2 - Pr14
                                          {
                                            "key": "ProductRow1-p14",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product14",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                        ]
                                      },
                                    }
                                  }
                                },
                                {
                                  "key": "SizedBox-cat-scpacing",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"width": "2.w"}
                                },
                                // menu column 3
                                {
                                  "key": "SizedBox-m-c-3",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "key": "CategoryColumn1-m-c-3",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category3",
                                        "elementGap": "2.2.sp",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000",
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          // Menu: Col3 - Pro15
                                          {
                                            "key": "ProductRow1-p15",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product15",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col3 - Pro16
                                          {
                                            "key": "ProductRow1-p16",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product16",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },

                                          // Menu: Col3 - Pro17
                                          {
                                            "key": "ProductRow1-p17",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product17",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col3 - Pro18
                                          {
                                            "key": "ProductRow1-p18",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product18",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col3 - Pro19
                                          {
                                            "key": "ProductRow1-p19",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product19",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col3 - Pro20
                                          {
                                            "key": "ProductRow1-p20",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product20",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col3 - Pro21
                                          {
                                            "key": "ProductRow1-p21",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product21",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                        ]
                                      },
                                    }
                                  }
                                },
                                {
                                  "key": "SizedBox-cat-scpacing",
                                  "widgetName": "SizedBox",
                                  "widgetSetting": {"width": "2.w"}
                                },
                                // menu column 4
                                {
                                  "key": "SizedBox-m-c-4",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "width": "20.w",
                                    "child": {
                                      "key": "CategoryColumn1-m-c-4",
                                      "widgetName": "CategoryColumn1",
                                      "widgetSetting": {
                                        "category": "category4",
                                        "elementGap": "2.2.sp",
                                        "categoryFontSizeFromFieldKey":
                                            "categoryFontSize",
                                        "categoryImageHeightFromFieldKey":
                                            "categoryImageHeight",
                                        "categoryTextStyle": {
                                          "fontWeight": "w700",
                                          "color": "0xff000000",
                                        },
                                        "upperCategoryName": "true",
                                        "productRows": [
                                          // Menu: Col4 - Pro22
                                          {
                                            "key": "ProductRow1-p22",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product22",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col4 - Pro23
                                          {
                                            "key": "ProductRow1-p23",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product23",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },

                                          // Menu: Col4 - Pro24
                                          {
                                            "key": "ProductRow1-p24",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product24",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col4 - Pro25
                                          {
                                            "key": "ProductRow1-p25",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product25",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col4 - Pro26
                                          {
                                            "key": "ProductRow1-p26",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product26",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col4 - Pro27
                                          {
                                            "key": "ProductRow1-p27",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product27",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.2.sp",
                                                  "color": "0x60000000",
                                                }
                                              }
                                            }
                                          },
                                          // Menu: Col4 - Pro28
                                          {
                                            "key": "ProductRow1-p28",
                                            "widgetName": "ProductRow1",
                                            "widgetSetting": {
                                              "product": "product28",
                                              "upperCaseProductName": "true",
                                              "productNameFontSizeFromFieldKey":
                                                  "productNameFontSize",
                                              "productPriceFontSizeFromFieldKey":
                                                  "productPriceFontSize",
                                              "productNameTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "productPriceTextStyle": {
                                                "color": "0xff000000",
                                              },
                                              "padding": {"vertical": "2.2.sp"},
                                              "border": {
                                                "bottom": {
                                                  "width": "0.1.sp",
                                                  "color": "0xff000000",
                                                }
                                              }
                                            }
                                          },
                                        ]
                                      },
                                    }
                                  }
                                },
                              ],
                            }
                          }
                        }
                      }
                    ]
                  },
                },
              }
            }
          }
        }
      }
    }
  };
  static final temppho2 = {
    "_id": "12",
    "templateSlug": "temp_pho_2",
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "3.sp",
          "priceFontSize": "3.5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "100.sp",
          "imageWidth": "100.sp",
        },
        "21/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "7.sp",
          "productFontSize": "3.sp",
          "priceFontSize": "3.5.sp",
          "sloganFontSize": "5.sp",
          "categoryFontSize": "5.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "85.sp",
          "imageWidth": "85.sp",
        },
        "16/9": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "3.5.sp",
          "priceFontSize": "4.2.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "3.5.sp",
          "imageHeight": "120.sp",
          "imageWidth": "120.sp",
        },
        "16/10": {
          "categoryColumnVerticalPadding": "5.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "4.5.sp",
          "priceFontSize": "5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "4.2.sp",
          "imageHeight": "125.sp",
          "imageWidth": "125.sp",
        },
        "4/3": {
          "categoryColumnVerticalPadding": "12.sp",
          "categoryColumnHorizontalPadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productFontSize": "5.sp",
          "priceFontSize": "5.5.sp",
          "sloganFontSize": "7.sp",
          "categoryFontSize": "7.sp",
          "indexChipRadius": "4.5.sp",
          "imageHeight": "130.sp",
          "imageWidth": "130.sp",
        },
        "default": {
          "categoryColumnVerticalPadding": "4.sp",
          "categoryColumnHorizontalPadding": "4.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "7.sp",
          "productFontSize": "2.5.sp",
          "priceFontSize": "3.sp",
          "sloganFontSize": "5.sp",
          "categoryFontSize": "5.sp",
          "indexChipRadius": "3.sp",
          "imageHeight": "100.sp",
          "imageWidth": "100.sp",
        },
      },
    },
    "isDeleted": false,
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "",
      },

      //Danh mục 1: pro 1, 2, 3, 4, 5
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      //Danh mục 2: pro 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      //Danh mục 3: pro 16, 17, 18, 19, 20
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "backgroundImage": {
        "key": "backgroundImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/background-1.jpg"
      },
      "leftImage": {
        "key": "leftImage",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_1.png"
      },
      "rightImage": {
        "key": "rightImage",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_4.png"
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "decoration": {
          "image": "backgroundImage",
          "fit": "cover",
        },
        "child": {
          "key": "Row-main",
          "widgetName": "Row",
          "widgetSetting": {
            "mainAxisAlignment": "spaceBetween",
            "crossAxisAlignment": "start",
            "children": [
              // left column
              {
                "key": "Expanded-left-col",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Stack-leftmenu",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        // left top image
                        {
                          "key": "Positioned-image-bottomleft",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "left": "-30.sp",
                            "bottom": "-15.sp",
                            "child": {
                              "key": "SizedBoxByRatio-image-bottomleft",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "widthFromFieldKey": "imageWidth",
                                "heightFromFieldKey": "imageHeight",
                                "child": {
                                  "key": "SimpleImage-image-bottomleft",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "imageDataKey": "leftImage",
                                    "fit": "contain"
                                  }
                                }
                              }
                            }
                          }
                        },
                        // left menu
                        {
                          "key": "PaddingByRatio-leftmenu",
                          "widgetName": "PaddingByRatio",
                          "widgetSetting": {
                            "verticalPaddingFieldKey":
                                "categoryColumnVerticalPadding",
                            "horizontalPaddingFieldKey":
                                "categoryColumnHorizontalPadding",
                            "child": {
                              "key": "CategoryColumn1-leftmenu",
                              "widgetName": "CategoryColumn1",
                              "widgetSetting": {
                                "category": "category1",
                                "categoryFontSizeFromFieldKey":
                                    "categoryFontSize",
                                "categoryTextStyle": {
                                  "fontFamily": "Roboto Slab",
                                  "color": "0xffda2f15",
                                  "fontWeight": "w700",
                                },
                                "crossAxisAlignment": "center",
                                "productRows": [
                                  // product 1
                                  {
                                    "key": "Padding-p-1",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p1",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "1",
                                          "product": "product1",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 2
                                  {
                                    "key": "Padding-p-2",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p2",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "2",
                                          "product": "product2",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 3
                                  {
                                    "key": "Padding-p-3",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p3",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "3",
                                          "product": "product3",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 4
                                  {
                                    "key": "Padding-p-4",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p4",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "4",
                                          "product": "product4",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 5
                                  {
                                    "key": "Padding-p-5",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p5",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "5",
                                          "product": "product5",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                ]
                              }
                            }
                          }
                        },
                      ],
                    },
                  }
                }
              },
              // center column
              {
                "key": "Expanded-center-col",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "PaddingByRatio-leftmenu",
                    "widgetName": "PaddingByRatio",
                    "widgetSetting": {
                      "verticalPaddingFieldKey":
                          "categoryColumnVerticalPadding",
                      "horizontalPaddingFieldKey":
                          "categoryColumnHorizontalPadding",
                      "child": {
                        // center menu
                        "key": "MenuColumn1-menu",
                        "widgetName": "MenuColumn1",
                        "widgetSetting": {
                          "brand": "PHO BRAND",
                          "slogan": "Pho, noodle and more",
                          "brandFontSizeFromFieldKey": "brandFontSize",
                          "sloganFontSizeFromFieldKey": "sloganFontSize",
                          "brandTextStyle": {
                            "fontFamily": "Pacifico",
                            "color": "0xffe1b971",
                          },
                          "sloganTextStyle": {
                            "fontFamily": "Roboto Slab",
                            "color": "0xffda2f15",
                            "fontWeight": "w700",
                          },
                          "menuItems": [
                            // product 6
                            {
                              "key": "Padding-p6",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p6",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "6",
                                    "product": "product6",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 7
                            {
                              "key": "Padding-p7",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p7",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "7",
                                    "product": "product7",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 8
                            {
                              "key": "Padding-p8",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p8",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "8",
                                    "product": "product8",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 9
                            {
                              "key": "Padding-p9",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p9",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "9",
                                    "product": "product9",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 10
                            {
                              "key": "Padding-p10",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p10",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "10",
                                    "product": "product10",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 11
                            {
                              "key": "Padding-p11",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p11",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "11",
                                    "product": "product11",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 12
                            {
                              "key": "Padding-p12",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p12",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "12",
                                    "product": "product12",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 13
                            {
                              "key": "Padding-p13",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p13",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "13",
                                    "product": "product13",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 14
                            {
                              "key": "Padding-p14",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p14",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "14",
                                    "product": "product14",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                            // product 15
                            {
                              "key": "Padding-p15",
                              "widgetName": "Padding",
                              "widgetSetting": {
                                "padding": {"vertical": "1.sp"},
                                "child": {
                                  "key": "IndexedMenuItem1-p15",
                                  "widgetName": "IndexedMenuItem1",
                                  "widgetSetting": {
                                    "index": "15",
                                    "product": "product15",
                                    "fontSizeFromFieldKey": "productFontSize",
                                    "priceFontSizeFromFieldKey":
                                        "priceFontSize",
                                    "textStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xffffffff",
                                      "fontWeight": "w700",
                                      "letterSpacing": "1",
                                    },
                                    "priceTextStyle": {
                                      "fontfamily": "openSans",
                                      "color": "0xfffc9100",
                                      "fontWeight": "w700",
                                    },
                                    "indexChipBgColor": "0xffda2f15",
                                    "indexChipFgColor": "0xffffffff",
                                    "indexChipRadiusFromFieldKey":
                                        "indexChipRadius",
                                    "elementGap": "2.2.sp",
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  },
                }
              },
              // right column
              {
                "key": "Expanded-center-col",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "Stack-rightmenu",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        // right top image
                        {
                          "key": "Positioned-image-topright",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "right": "-30.sp",
                            "top": "-15.sp",
                            "child": {
                              "key": "SizedBoxByRatio-image-topright",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "widthFromFieldKey": "imageWidth",
                                "heightFromFieldKey": "imageHeight",
                                "child": {
                                  "key": "SimpleImage-image-topright",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "imageDataKey": "rightImage",
                                    "fit": "contain"
                                  }
                                }
                              }
                            }
                          }
                        },
                        // right menu
                        {
                          "key": "PaddingByRatio-rightmenu",
                          "widgetName": "PaddingByRatio",
                          "widgetSetting": {
                            "verticalPaddingFieldKey":
                                "categoryColumnVerticalPadding",
                            "horizontalPaddingFieldKey":
                                "categoryColumnHorizontalPadding",
                            "child": {
                              "key": "CategoryColumn1-rightmenu",
                              "widgetName": "CategoryColumn1",
                              "widgetSetting": {
                                "category": "category2",
                                "categoryFontSizeFromFieldKey":
                                    "categoryFontSize",
                                "categoryTextStyle": {
                                  "fontFamily": "Roboto Slab",
                                  "color": "0xffda2f15",
                                  "fontWeight": "w700",
                                },
                                "crossAxisAlignment": "center",
                                "mainAxisAlignment": "end",
                                "productRows": [
                                  // product 16
                                  {
                                    "key": "Padding-p16",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p16",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "16",
                                          "product": "product16",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 17
                                  {
                                    "key": "Padding-p17",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p17",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "17",
                                          "product": "product17",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 18
                                  {
                                    "key": "Padding-p18",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p18",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "18",
                                          "product": "product18",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 19
                                  {
                                    "key": "Padding-p19",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p19",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "19",
                                          "product": "product19",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                  // product 20
                                  {
                                    "key": "Padding-p20",
                                    "widgetName": "Padding",
                                    "widgetSetting": {
                                      "padding": {"vertical": "1.sp"},
                                      "child": {
                                        "key": "IndexedMenuItem1-p20",
                                        "widgetName": "IndexedMenuItem1",
                                        "widgetSetting": {
                                          "index": "20",
                                          "product": "product20",
                                          "fontSizeFromFieldKey":
                                              "productFontSize",
                                          "priceFontSizeFromFieldKey":
                                              "priceFontSize",
                                          "textStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xffffffff",
                                            "fontWeight": "w700",
                                            "letterSpacing": "1",
                                          },
                                          "priceTextStyle": {
                                            "fontfamily": "openSans",
                                            "color": "0xfffc9100",
                                            "fontWeight": "w700",
                                          },
                                          "indexChipBgColor": "0xffda2f15",
                                          "indexChipFgColor": "0xffffffff",
                                          "indexChipRadiusFromFieldKey":
                                              "indexChipRadius",
                                          "elementGap": "2.2.sp",
                                        }
                                      }
                                    }
                                  },
                                ]
                              }
                            }
                          }
                        }
                      ],
                    },
                  }
                }
              },
            ]
          },
        },
      }
    }
  };
  static final temppho3 = {
    "_id": "",
    "templateSlug": "temp_pho_3",
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
        "21/9": {
          "categoryFontSize": "3.5.sp",
          "verticalFramePadding": "2.sp",
          "horizontalFramePadding": "2.sp",
          "categoryBlockHeight": "25.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
        "16/9": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
        "16/10": {
          "categoryFontSize": "6.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "23.h",
          "brandFontSize": "12.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
        },
        "4/3": {
          "categoryFontSize": "7.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "23.h",
          "brandFontSize": "15.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp",
        },
        "default": {
          "categoryFontSize": "5.sp",
          "verticalFramePadding": "8.sp",
          "horizontalFramePadding": "8.sp",
          "categoryBlockHeight": "22.h",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
        },
      },
    },
    "isDeleted": false,
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "",
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "",
      },
      "category5": {
        "key": "category5",
        "type": "category",
        "name": "Category 5",
        "image": "",
      },
      "category6": {
        "key": "category6",
        "type": "category",
        "name": "Category 6",
        "image": "",
      },
      //Danh mục 1: pro 1, 2, 3, 4, 5,
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 2: pro  6, 7, 8, 9, 10,
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 3: pro 11, 12, 13, 14, 15
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 4: 16, 17, 18, 19, 20
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 5: pro 21, 22, 23, 24, 25,
      "product21": {
        "key": "product21",
        "type": "product",
        "name": "Product 21",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product22": {
        "key": "product22",
        "type": "product",
        "name": "Product 22",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product23": {
        "key": "product23",
        "type": "product",
        "name": "Product 23",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product24": {
        "key": "product24",
        "type": "product",
        "name": "Product 24",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product25": {
        "key": "product25",
        "type": "product",
        "name": "Product 25",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 6: pro 26, 27, 28, 29, 30
      "product26": {
        "key": "product26",
        "type": "product",
        "name": "Product 26",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product27": {
        "key": "product27",
        "type": "product",
        "name": "Product 27",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product28": {
        "key": "product28",
        "type": "product",
        "name": "Product 28",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product29": {
        "key": "product29",
        "type": "product",
        "name": "Product 29",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product30": {
        "key": "product30",
        "type": "product",
        "name": "Product 30",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "leftTopDecoImage": {
        "key": "leftTopDecoImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/left_top_deco.png"
      },
      "rightTopDecoImage": {
        "key": "rightTopDecoImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/right_top_deco.png"
      },
      "bottomDecoImage": {
        "key": "bottomDecoImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/bottom_deco.png"
      },
      "cat1Image": {
        "key": "cat1Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1-1.jpg"
      },
      "cat2Image": {
        "key": "cat2Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2-1.jpg"
      },
      "cat3Image": {
        "key": "cat3Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3-1.jpg"
      },
      "circleBorderImage": {
        "key": "circleBorderImage",
        "type": "image",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/circle_border.png"
      }
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Stack",
      "widgetSetting": {
        "children": [
          // menu
          {
            "key": "Container-menu",
            "widgetName": "Container",
            "widgetSetting": {
              "height": "100.h",
              "width": "100.w",
              // bg color - blue
              "decoration": {"color": "0xff0d507e"},

              "child": {
                "key": "PaddingByRatio-menu",
                "widgetName": "PaddingByRatio",
                "widgetSetting": {
                  "verticalPaddingFieldKey": "verticalFramePadding",
                  "horizontalPaddingFieldKey": "horizontalFramePadding",
                  "child": {
                    "key": "Column-menu",
                    "widgetName": "Column",
                    "widgetSetting": {
                      "mainAxisAlignment": "center",
                      "children": [
                        // brand text
                        {
                          "key": "Text-brand",
                          "widgetName": "Text",
                          "widgetSetting": {
                            "data": "PHO BRAND",
                            "fontSizeFromFieldKey": "brandFontSize",
                            "style": {
                              "fontFamily": "Staatliches",
                              "color": "0xffffaf45",
                              // "fontSize": "10.sp",
                            }
                          }
                        },
                        // menu items
                        {
                          "key": "Expanded-menu-items",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "key": "Wrap-menu-items",
                              "widgetName": "Wrap",
                              "widgetSetting": {
                                "direction": "vertical",
                                "spacing": "5.sp",
                                "runSpacing": "10.sp",
                                "clipBehavior": "hardEdge",
                                "children": [
                                  // cat 1
                                  {
                                    "key": "CategoryBlock1-cat-1",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      // image cat 1
                                      "categoryImage": {
                                        "key": "FramedImage-cat-1",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat1Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category1",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        // "fontSize": "5.sp",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 1 - items
                                      "categoryItemRows": [
                                        // pro 1
                                        {
                                          "key": "Padding-p1",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p1",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product1",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 2
                                        {
                                          "key": "Padding-p2",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p2",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product2",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 3
                                        {
                                          "key": "Padding-p3",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p3",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product3",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 4
                                        {
                                          "key": "Padding-p4",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p4",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product4",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 5
                                        {
                                          "key": "Padding-p5",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p5",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product5",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  },

                                  // cat 2
                                  {
                                    "key": "CategoryBlock1-cat-2",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "22.h",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      // image cat 2
                                      "categoryImage": {
                                        "key": "FramedImage-cat-2",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat2Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category2",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        // "fontSize": "5.sp",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 2 - items
                                      "categoryItemRows": [
                                        // pro 6
                                        {
                                          "key": "Padding-p6",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p6",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product6",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 7
                                        {
                                          "key": "Padding-p7",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p7",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product7",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 8
                                        {
                                          "key": "Padding-p8",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p8",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product8",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 9
                                        {
                                          "key": "Padding-p9",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p9",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product9",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 10
                                        {
                                          "key": "Padding-p10",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p10",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product10",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  },
                                  // cat 3
                                  {
                                    "key": "CategoryBlock1-cat-3",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "22.h",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      // image cat 3
                                      "categoryImage": {
                                        "key": "FramedImage-cat-3",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat3Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category3",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 3 - items
                                      "categoryItemRows": [
                                        // pro 11
                                        {
                                          "key": "Padding-p11",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p11",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product11",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 12
                                        {
                                          "key": "Padding-p12",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p12",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product12",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 13
                                        {
                                          "key": "Padding-p13",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p13",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product13",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 14
                                        {
                                          "key": "Padding-p14",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p14",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product14",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 15
                                        {
                                          "key": "Padding-p15",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p15",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product15",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  },

                                  // cat 4
                                  {
                                    "key": "CategoryBlock1-cat-4",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "22.h",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",

                                      // image cat 4
                                      "categoryImage": {
                                        "key": "FramedImage-cat-4",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat3Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category4",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        // "fontSize": "5.sp",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 4 - items
                                      "categoryItemRows": [
                                        // pro 16
                                        {
                                          "key": "Padding-p16",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p16",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product16",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 17
                                        {
                                          "key": "Padding-p17",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p17",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product17",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 18
                                        {
                                          "key": "Padding-p18",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p18",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product18",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 19
                                        {
                                          "key": "Padding-p19",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p19",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product19",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 20
                                        {
                                          "key": "Padding-p20",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p20",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product20",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  },

                                  // cat 5
                                  {
                                    "key": "CategoryBlock1-cat-5",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "22.h",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      // image cat 5
                                      "categoryImage": {
                                        "key": "FramedImage-cat-5",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat1Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category5",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        // "fontSize": "5.sp",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 5 - items
                                      "categoryItemRows": [
                                        // pro 21
                                        {
                                          "key": "Padding-p21",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p21",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product21",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 22
                                        {
                                          "key": "Padding-p22",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p22",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product22",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 23
                                        {
                                          "key": "Padding-p23",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p23",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product23",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 24
                                        {
                                          "key": "Padding-p24",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p24",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product24",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 25
                                        {
                                          "key": "Padding-p25",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p25",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product25",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  },
                                  // cat 6
                                  {
                                    "key": "CategoryBlock1-cat-6",
                                    "widgetName": "CategoryBlock1",
                                    "widgetSetting": {
                                      "width": "40.w",
                                      // "height": "22.h",
                                      // "height": "26.h",
                                      "heightFromFieldKey":
                                          "categoryBlockHeight",
                                      // image cat 6
                                      "categoryImage": {
                                        "key": "FramedImage-cat-6",
                                        "widgetName": "FramedImage",
                                        "widgetSetting": {
                                          "imageDataKey": "cat2Image",
                                          "imageFrameDataKey":
                                              "circleBorderImage",
                                          "width": "32.sp",
                                          "height": "32.sp",
                                          "borderWidth": "2.sp",
                                          "borderRadiusOfImage": "32.sp",
                                        }
                                      },
                                      "category": "category6",
                                      "categoryFontSizeFromFieldKey":
                                          "categoryFontSize",
                                      "categoryTextStyle": {
                                        "fontFamily": "Staatliches",
                                        // "fontSize": "5.sp",
                                        "color": "0xffffffff"
                                      },
                                      "elementGap": "5.sp",
                                      // cat 6 - items
                                      "categoryItemRows": [
                                        // pro 26
                                        {
                                          "key": "Padding-p26",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p26",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product26",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                        // pro 29
                                        {
                                          "key": "Padding-p29",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p29",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product29",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        }, // pro 30
                                        {
                                          "key": "Padding-p30",
                                          "widgetName": "Padding",
                                          "widgetSetting": {
                                            "padding": {"vertical": "0.5.sp"},
                                            "child": {
                                              "key": "ProductRow1-p30",
                                              "widgetName": "ProductRow1",
                                              "widgetSetting": {
                                                "product": "product30",
                                                "productNameFontSizeFromFieldKey":
                                                    "productNameFontSize",
                                                "productPriceFontSizeFromFieldKey":
                                                    "productPriceFontSize",
                                                "productNameTextStyle": {
                                                  "color": "0xffffffff",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "productPriceTextStyle": {
                                                  "color": "0xffffaf45",
                                                  "fontFamily": "Poppins",
                                                  // "fontSize": "3.sp",
                                                },
                                                "border": {
                                                  "all": {
                                                    "width": "0",
                                                    "color": "0"
                                                  }
                                                },
                                                "prefixFoodPrice": "\$",
                                                "fractionDigitsForPrice": "1",
                                              }
                                            }
                                          }
                                        },
                                      ],
                                    }
                                  }
                                ]
                              }
                            }
                          }
                        }
                      ]
                    }
                  }
                }
              }
            },
          },
          // deco bottom
          {
            "key": "Positioned-bottom-deco",
            "widgetName": "Positioned",
            "widgetSetting": {
              "bottom": "-14.sp",
              "child": {
                "key": "Image-bottom-deco",
                "widgetName": "image",
                "widgetSetting": {
                  "imageId": "bottomDecoImage",
                  "fixCover": "true",
                  "width": "100.w",
                  "height": "100.h",
                }
              }
            }
          },
          // deco top left
          {
            "key": "Positioned-top-left-dec",
            "widgetName": "Positioned",
            "widgetSetting": {
              "left": "0",
              "child": {
                "key": "Image-top-left-deco",
                "widgetName": "image",
                "widgetSetting": {
                  "imageId": "leftTopDecoImage",
                  "fixCover": "false",
                  "width": "30.sp",
                  "height": "30.sp",
                }
              }
            }
          },
          // deco top right
          {
            "key": "Positioned-top-right-dec",
            "widgetName": "Positioned",
            "widgetSetting": {
              "right": "0",
              "child": {
                "key": "Image-top-right-deco",
                "widgetName": "image",
                "widgetSetting": {
                  "imageId": "rightTopDecoImage",
                  "fixCover": "false",
                  "width": "30.sp",
                  "height": "30.sp",
                }
              }
            }
          },
        ]
      }
    }
  };
  static final temppho4 = {
    "_id": "",
    "templateSlug": "temp_pho_4",
    "isDeleted": false,
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "21/9": {
          "categoryNameFontSize": "5.sp",
          "categoryDesFontSize": "3.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "16/9": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
          "productDesFontSize": "2.5.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "16/10": {
          "categoryNameFontSize": "10.sp",
          "categoryDesFontSize": "4.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "2.8.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "4/3": {
          "categoryNameFontSize": "12.sp",
          "categoryDesFontSize": "5.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp",
          "productDesFontSize": "3.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "default": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
      },
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      //Danh mục 1: pro 1, 2, 3
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 2: pro  4, 5, 6
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 3: pro  7, 8, 9
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      // Danh mục 4: 10, 11, 12
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "cat1Image": {
        "key": "cat1Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1.png"
      },
      "cat2Image": {
        "key": "cat2Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2.png"
      },
      "cat3Image": {
        "key": "cat3Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3.png"
      },
      "cat4Image": {
        "key": "cat4Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_4.png"
      }
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "decoration": {"color": "0xffece3d8"},
        "child": {
          "key": "Padding-menu",
          "widgetName": "PaddingByRatio",
          "widgetSetting": {
            "verticalPaddingFieldKey": "verticalFramePadding",
            "horizontalPaddingFieldKey": "horizontalFramePadding",
            "child": {
              // menu
              "key": "Wrap-menu",
              "widgetName": "Wrap",
              "widgetSetting": {
                "direction": "vertical",
                "alignment": "center",
                "runAlignment": "center",
                "children": [
                  // cat 1
                  {
                    "key": "SizedBox-cat1",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "key": "CategoryBlock2-cat1",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category1",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            // "fontSize": "8.sp",
                            "color": "0xff583618",
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            // "fontSize": "2.8.sp",
                            "fontStyle": "italic",
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "key": "divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "categoryItems": [
                            // pro 1
                            {
                              "key": "SizedBox-p1",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p1",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product1",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 2
                            {
                              "key": "SizedBox-p2",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product2",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 3
                            {
                              "key": "SizedBox-p3",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p3",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product3",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // cat 1 - image
                            {
                              "key": "Container-image-cat4",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "key": "SimpleImage-cat1",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat1Image",
                                    "fit": "contain",
                                  }
                                }
                              }
                            }
                          ],
                        }
                      }
                    }
                  },
                  // cat 2
                  {
                    "key": "SizedBox-cat2",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "key": "CategoryBlock2-cat2",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category2",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            // "fontSize": "8.sp",
                            "color": "0xff583618",
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            // "fontSize": "2.8.sp",
                            "fontStyle": "italic",
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "key": "divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "categoryItems": [
                            // cat 2 - image
                            {
                              "key": "Container-image-cat4",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "key": "SimpleImage-cat1",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat2Image",
                                    "fit": "contain"
                                  }
                                }
                              }
                            },
                            // pro 4
                            {
                              "key": "SizedBox-p4",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p4",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product4",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 5
                            {
                              "key": "SizedBox-p5",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p5",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product5",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 6
                            {
                              "key": "SizedBox-p6",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p6",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product6",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  },
                  // cat 3
                  {
                    "key": "SizedBox-cat3",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "key": "CategoryBlock2-cat3",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category3",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            // "fontSize": "8.sp",
                            "color": "0xff583618",
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            // "fontSize": "2.8.sp",
                            "fontStyle": "italic",
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "key": "divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "categoryItems": [
                            // pro 7
                            {
                              "key": "SizedBox-p7",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p7",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product7",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 8
                            {
                              "key": "SizedBox-p8",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p8",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product8",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 9
                            {
                              "key": "SizedBox-p9",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p9",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product9",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },

                            // cat 3 - image
                            {
                              "key": "Container-image-cat4",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "key": "SimpleImage-cat3",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat3Image",
                                    "fit": "contain"
                                  }
                                }
                              }
                            }
                          ],
                        }
                      }
                    }
                  },
                  // cat 4
                  {
                    "key": "SizedBox-cat4",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "key": "CategoryBlock2-cat4",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category4",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            // "fontSize": "8.sp",
                            "color": "0xff583618",
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            // "fontSize": "2.8.sp",
                            "fontStyle": "italic",
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "key": "divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "categoryItems": [
                            // cat 4 - image
                            {
                              "key": "Container-image-cat4",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "key": "SimpleImage-cat4",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat4Image",
                                    "fit": "contain"
                                  }
                                }
                              }
                            },
                            // pro 10
                            {
                              "key": "SizedBox-p10",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p10",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product10",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 11
                            {
                              "key": "SizedBox-p11",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p11",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product11",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 12
                            {
                              "key": "SizedBox-p12",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p12",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product12",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  }
                ]
              }
            }
          },
        }
      },
    },
  };
  static final temppho5 = {
    "_id": "",
    "templateSlug": "temp_pho_5",
    "isDeleted": false,
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryNameFontSize": "5.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp",
        },
        "21/9": {
          "categoryNameFontSize": "4.sp",
          "productNameFontSize": "2.5.sp",
          "productPriceFontSize": "2.5.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "13.h",
          "columnGap": "2.sp",
        },
        "16/9": {
          "categoryNameFontSize": "6.sp",
          "productNameFontSize": "3.5.sp",
          "productPriceFontSize": "3.5.sp",
          "productDesFontSize": "2.5.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "35.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp",
        },
        "16/10": {
          "categoryNameFontSize": "8.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "2.8.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "35.sp",
          "productRowHeight": "12.5.h",
          "columnGap": "5.sp",
        },
        "4/3": {
          "categoryNameFontSize": "8.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "3.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "40.sp",
          "productRowHeight": "12.h",
          "columnGap": "6.sp",
        },
        "default": {
          "categoryNameFontSize": "5.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
          "categoryImageHeight": "22.sp",
          "productRowHeight": "12.h",
          "columnGap": "4.sp",
        },
      },
    },

    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1-2.jpg",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2-2.jpg",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3-2.jpg",
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_4-1.jpg",
      },

      //Danh mục 1: pro 1, 2, 3, 4, 5
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_1.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 2: pro 6, 7, 8, 9, 10
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_2.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_3.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_4.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/pho_1.jpg",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 3: pro 11, 12, 13, 14, 15
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      // Danh mục 4: 16, 17, 18, 19, 20
      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "51.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "25.0",
        "salePrice": "",
        "image": "",
      },
      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "31.0",
        "salePrice": "",
        "image": "",
      },
      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "32.0",
        "salePrice": "",
        "image": "",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "padding": {"all": "5.sp"},
        "decoration": {"color": "0xfffdf6e9"},
        "child": {
          "key": "Center-menu",
          "widgetName": "Center",
          "widgetSetting": {
            "child": {
              "key": "Row-menu",
              "widgetName": "Row",
              "widgetSetting": {
                "crossAxisAlignment": "start",
                "mainAxisAlignment": "center",
                "children": [
                  // cat 1
                  {
                    "key": "Container-cat1",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "key": "CategoryColumn2-cat1",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category1",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828",
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp",
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            // pro 1
                            {
                              "key": "SizedBoxByRatio-p1",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p1",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p1",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product1",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 2
                            {
                              "key": "SizedBoxByRatio-p2",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p2",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p2",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product2",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 3
                            {
                              "key": "SizedBoxByRatio-p3",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p3",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p3",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product3",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 4
                            {
                              "key": "SizedBoxByRatio-p4",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p4",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p4",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product4",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 5
                            {
                              "key": "SizedBoxByRatio-p5",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p5",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p5",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product5",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  },
                  // cat 2
                  {
                    "key": "Container-cat2",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "key": "CategoryColumn2-cat2",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category2",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828",
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp",
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            // pro 6
                            {
                              "key": "SizedBoxByRatio-p6",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p6",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p6",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product6",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 7
                            {
                              "key": "SizedBoxByRatio-p7",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "child": {
                                  "key": "Padding-p7",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p7",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product7",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 8
                            {
                              "key": "SizedBoxByRatio-p8",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p8",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p8",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product8",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 9
                            {
                              "key": "SizedBoxByRatio-p9",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p9",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p9",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product9",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 10
                            {
                              "key": "SizedBoxByRatio-p10",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p10",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p10",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product10",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  },
                  // cat 3
                  {
                    "key": "Container-cat3",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "key": "CategoryColumn2-cat3",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category3",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828",
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp",
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            // pro 11
                            {
                              "key": "SizedBoxByRatio-p11",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p11",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p11",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product11",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 12
                            {
                              "key": "SizedBoxByRatio-p12",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p12",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p12",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product12",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 13
                            {
                              "key": "SizedBoxByRatio-p13",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p13",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p13",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product13",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            }, // pro 14
                            {
                              "key": "SizedBoxByRatio-p14",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p14",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p14",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product14",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            }, // pro 15
                            {
                              "key": "SizedBoxByRatio-p15",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p15",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p15",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product15",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  },
                  // cat 4
                  {
                    "key": "Container-cat4",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "width": "24.w",
                      "padding": {"horizontal": "2.5.sp"},
                      "child": {
                        "key": "CategoryColumn2-cat4",
                        "widgetName": "CategoryColumn2",
                        "widgetSetting": {
                          "category": "category4",
                          "categoryImageHeightFromFieldKey":
                              "categoryImageHeight",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryNameContainerDecoration": {
                            "borderRadius": "1.5.sp",
                            "color": "0xff561828",
                          },
                          "upperCategoryName": "true",
                          "categoryNameContainnerPadding": {"vertical": "1.sp"},
                          "categoryNameTextStyle": {
                            "fontFamily": "Staatliches",
                            "color": "0xffffffff",
                            "letterSpacing": "1.sp",
                          },
                          "elementGapFromFieldKey": "columnGap",
                          "categoryImageWidth": "maxFinite",
                          "categoryImageBorderRadius": "1.5.sp",
                          "categoryItems": [
                            // pro 16
                            {
                              "key": "SizedBoxByRatio-p16",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p16",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p16",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product16",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            // pro 17
                            {
                              "key": "SizedBoxByRatio-p17",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p17",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p17",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product17",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            }, // pro 18
                            {
                              "key": "SizedBoxByRatio-p18",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p18",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p18",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product18",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            }, // pro 19
                            {
                              "key": "SizedBoxByRatio-p19",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p19",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p19",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product19",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            }, // pro 20
                            {
                              "key": "SizedBoxByRatio-p20",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey": "productRowHeight",
                                "color": "0x50ffff00",
                                "child": {
                                  "key": "Padding-p20",
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"vertical": "2.sp"},
                                    "child": {
                                      "key": "ProductRow3-p20",
                                      "widgetName": "ProductRow3",
                                      "widgetSetting": {
                                        "product": "product20",
                                        "imageWidth": "20.sp",
                                        "imageHeight": "15.sp",
                                        "imageBorderRadius": "1.5.sp",
                                        "elementGap": "2.sp",
                                        "productNameFontSizeFromFieldKey":
                                            "productNameFontSize",
                                        "productPriceFontSizeFromFieldKey":
                                            "productPriceFontSize",
                                        "productDesFontSizeFromFieldKey":
                                            "productDesFontSize",
                                        "productNameTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "productPriceTextStyle": {
                                          "fontFamily": "Staatliches",
                                          "fontWeight": "bold",
                                          "letterSpacing": ".2.sp",
                                        },
                                        "horizontalDivider": {
                                          "key": "Divider-p1",
                                          "widgetName": "Divider",
                                          "widgetSetting": {"height": "2.8.sp"}
                                        },
                                        "productDescriptionTextStyle": {
                                          "fontFamily": "Open Sans",
                                          "fontStyle": "italic",
                                          "color": "0xff66635a",
                                        },
                                        "fractionDigitsForPrice": "2",
                                        "maxLineOfProductName": "2",
                                        "prefixProductPrice": "",
                                        "subfixProductPrice": "",
                                        "upperProductName": "true",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      },
    }
  };
}
