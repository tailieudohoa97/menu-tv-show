class JsonDataTemplate {
  //[092328HG] Check temprestaurant3: Run OK - Ratio OK
  static final temprestaurant3 = {
    "_id": "64ee744398c3d4605b477abc",
    "templateSlug": "temp_restaurant_3",
    "RatioSetting": {
      "textStyle": {
        "categoryNameStyle": {
          "fontWeight": "w300",
          "fontFamily": "Alfa Slab One",
          "letterSpacing": "5",
        },
        "nameProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "Poppins",
        },
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Poppins",
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "Poppins",
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "Poppins",
        },
        "priceProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "Poppins",
        },
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "Open Sans",
        },
      },
      "renderScreen": {
        "32/9": {
          "categoryNameStyle": "1.w",
          "nameProductStyle": "0.7.w",
          "nameProductStyleMedium": "0.5.w",
          "priceProductStyle": "0.7.w",
          "priceProductStyleMedium": "0.5.w",
          "discriptionProductStyle": "0.5.w",
          "discriptionProductStyleMedium": "0.3.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
        "21/9": {
          "categoryNameStyle": "1.5.w",
          "nameProductStyle": "1.w",
          "nameProductStyleMedium": "0.7.w",
          "priceProductStyle": "1.w",
          "priceProductStyleMedium": "0.7.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
        "16/9": {
          "categoryNameStyle": "2.5.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "20.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.5.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
        "16/10": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "1.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
        "4/3": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "2.w",
          "nameProductStyleMedium": "1.5.w",
          "priceProductStyle": "2.w",
          "priceProductStyleMedium": "1.5.w",
          "discriptionProductStyle": "1.5.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "50.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
        "default": {
          "categoryNameStyle": "3.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "1.w",
          "discriptionProductStyleMedium": "0.8.w",
          "imageHeight": "20.h",
          "marginBottomImage": "0",
          "marginBottomProduct": "2.h",
          "boderRadiusStyle": "30",
          "marginBottomCategoryTitle": "5.h",
          "paddingVerticalCategorTilte": "0.w",
          "paddingHorizontalCategorTilte": "2.w",
          "widthThumnailProduct": "3.w",
          "heightThumnailProduct": "3.w",
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "",
      },
      //Danh mục 1: pro 1, 2 ,3, 4, 5
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      //Danh mục 2: pro 6, 7, 8, 8, 9, 10
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 2.1",
        "price": "\$23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 2.2",
        "price": "\$24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 2.3",
        "price": "\$25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 2.4",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 2.5",
        "price": "\$32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      //Danh mục 3: pro 11, 12, 13, 14, 15
      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 3.1",
        "price": "\$33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 3.2",
        "price": "\$41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 3.3",
        "price": "\$42.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 3.4",
        "price": "\$43.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 3.5",
        "price": "\$31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Hình background
      'imageBackground': {
        'key': 'imageBackground',
        'type': 'image',
        // 'image': '/temp_restaurant_3/food-bg(4).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg4-1.jpg',
      },

      //Danh mục 1: 3 hình số 1,2,3
      'image1': {
        'key': 'image1',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(1).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp31.png",
      },
      'image2': {
        'key': 'image2',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(2).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp32.png",
      },
      'image3': {
        'key': 'image3',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(3).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp33.png",
      },

      //Danh mục 2: 3 hình số 4,5,6
      'image4': {
        'key': 'image4',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(4).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp34.png",
      },
      'image5': {
        'key': 'image5',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(5).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp35.png",
      },
      'image6': {
        'key': 'image6',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(6).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp36.png",
      },

      //Danh mục 3-4: 3 hình số 7,8,9
      'image7': {
        'key': 'image7',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(7).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp37.png",
      },
      'image8': {
        'key': 'image8',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(9).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp39.png",
      },
      'image9': {
        'key': 'image9',
        'type': 'image',
        // 'image': "/temp_restaurant_3/food-temp3(8).png",
        'image':
            "http://foodiemenu.co/wp-content/uploads/2023/09/food-temp38.png",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image":
              "imageBackground", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
          "boxFit": "cover",
        },
        "child": {
          "key": "Container-overlay",
          "widgetName": "Container",
          "widgetSetting": {
            "color": "0xe5000000",
            "child": {
              "key": "Row-menu",
              "widgetName": "Row",
              "widgetSetting": {
                "textRightToLeft": "false",
                "children": [
                  // Cột 1
                  {
                    "key": "expanded-c1",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "key": "padding-c1",
                        "widgetName": "Padding",
                        "widgetSetting": {
                          "padding": {"all": "2.w"},
                          "child": {
                            "key": "Column-c1",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "mainAxisAlignment": "start",
                              "children": [
                                // Cột 1 - Dòng 1 categoryTitleSingleColumn
                                {
                                  "key": "categoryTitleSingleColumn-c1-r1",
                                  "widgetName": "categoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "dataList": {
                                      "category": "category1",
                                      "product": [
                                        "product1",
                                        "product2",
                                        "product3",
                                        "product4",
                                        "product5",
                                      ],
                                    },
                                    "toUpperCaseNameProductName": "false",
                                    "useBackgroundCategoryTitle": "true",
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "useFullWidthCategoryTitle": "true",
                                  }
                                },

                                // Cột 1 - Dòng 2 - Spacing marginBottomCategoryTitle
                                {
                                  "key": "PaddingByRatio-c1-r2",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                // Cột 1 - Dòng 3 - 3 hình
                                {
                                  "key": "Expanded-c1-r3",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "child": {
                                      "key": "Row-c1-r3",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "crossAxisAlignment": "start",
                                        "children": [
                                          // Cột 1 - Dòng 3 - hình 1
                                          {
                                            "key": "Expanded-image1",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-1",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image1",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          // Khoảng trống 2.w
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 1 - Dòng 3 - hình 2
                                          {
                                            "key": "Expanded-image2",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-2",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image2",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          // Khoảng trống 2.w
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 1 - Dòng 3 - hình 3
                                          {
                                            "key": "Expanded-image3",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-3",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image3",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  // Cột 2
                  {
                    "key": "expanded-c1",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "key": "Container-c1",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "padding": {"all": "2.w"},
                          "color": "0x09ffffff",
                          "child": {
                            "key": "Column-c1",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "mainAxisAlignment": "start",
                              "children": [
                                // Cột 2 - Dòng 1 categoryTitleSingleColumn
                                {
                                  "key": "categoryTitleSingleColumn-c1-r1",
                                  "widgetName": "categoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "dataList": {
                                      "category": "category2",
                                      "product": [
                                        "product6",
                                        "product7",
                                        "product8",
                                        "product9",
                                        "product10",
                                      ],
                                    },
                                    "toUpperCaseNameProductName": "false",
                                    "useBackgroundCategoryTitle": "true",
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "useFullWidthCategoryTitle": "true",
                                  }
                                },

                                // Cột 2 - Dòng 2 - Spacing marginBottomCategoryTitle
                                {
                                  "key": "PaddingByRatio-c1-r2",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                // Cột 2 - Dòng 3 - 3 hình
                                {
                                  "key": "Expanded-c1-r3",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "child": {
                                      "key": "Row-c1-r3",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "crossAxisAlignment": "start",
                                        "children": [
                                          // Cột 2 - Dòng 3 - hình 4
                                          {
                                            "key": "Expanded-image4",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-4",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image4",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 2 - Dòng 3 - hình 5
                                          {
                                            "key": "Expanded-image5",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-5",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image5",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          // Khoảng trống 2.w
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 1 - Dòng 3 - hình 6
                                          {
                                            "key": "Expanded-image6",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-6",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image6",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                  // Cột 3
                  {
                    "key": "expanded-c1",
                    "widgetName": "Expanded",
                    "widgetSetting": {
                      "flex": "1",
                      "child": {
                        "key": "padding-c1",
                        "widgetName": "Padding",
                        "widgetSetting": {
                          "padding": {"all": "2.w"},
                          "child": {
                            "key": "Column-c1",
                            "widgetName": "Column",
                            "widgetSetting": {
                              "mainAxisAlignment": "start",
                              "children": [
                                // Cột 3 - Dòng 1 categoryTitleSingleColumn
                                {
                                  "key": "categoryTitleSingleColumn-c1-r1",
                                  "widgetName": "categoryTitleSingleColumn",
                                  "widgetSetting": {
                                    "dataList": {
                                      "category": "category3",
                                      "product": [
                                        "product11",
                                        "product12",
                                        "product13",
                                        "product14",
                                        "product15",
                                      ],
                                    },
                                    "toUpperCaseNameProductName": "false",
                                    "useBackgroundCategoryTitle": "true",
                                    "colorCategoryTitle": "0xFF121212",
                                    "colorBackgroundCategoryTitle":
                                        "0xfffa8700",
                                    "colorPrice": "0xfffa8700",
                                    "colorProductName": "0xFFffffff",
                                    "colorDescription": "0xFFffffff",
                                    "useThumnailProduct": "false",
                                    "pathThumnailProduct": "",
                                    "useFullWidthCategoryTitle": "true",
                                  }
                                },
                                // Cột 3 - Dòng 2 - Spacing marginBottomCategoryTitle
                                {
                                  "key": "PaddingByRatio-c1-r2",
                                  "widgetName": "PaddingByRatio",
                                  "widgetSetting": {
                                    "bottomPaddingFieldKey":
                                        "marginBottomCategoryTitle"
                                  }
                                },
                                // Cột 3 - Dòng 3 - 3 hình
                                {
                                  "key": "Expanded-c1-r3",
                                  "widgetName": "Expanded",
                                  "widgetSetting": {
                                    "child": {
                                      "key": "Row-c1-r3",
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "crossAxisAlignment": "start",
                                        "children": [
                                          // Cột 3 - Dòng 3 - hình 7
                                          {
                                            "key": "Expanded-image7",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-7",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image7",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          // Khoảng trống 2.w
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 3 - Dòng 3 - hình 8
                                          {
                                            "key": "Expanded-image8",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-8",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image8",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          },
                                          // Khoảng trống 2.w
                                          {
                                            "key": "SizedBox-image-spacing",
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "2.w"}
                                          },
                                          // Cột 3 - Dòng 3 - hình 9
                                          {
                                            "key": "Expanded-image9",
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "child": {
                                                "key": "image-9",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey": "image9",
                                                  "fit": "contain",
                                                }
                                              },
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              ]
                            }
                          }
                        }
                      }
                    }
                  },
                ]
              }
            }
          }
        }
      }
    }
  };
}
