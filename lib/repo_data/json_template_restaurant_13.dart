class JsonDataTemplate {
  // [112315TIN] run: ok, supported ratio: 4:3, 16:10, 16:9, 21:9, double check: oke
  static final temprestaurant13 = {
    "_id": "",
    "templateSlug": "temp_restaurant_13",
    "ratioSetting": {
      // not use textStyle for this template
      // "textStyle": {},
      "renderScreen": {
        "32/9": {
          "categoryNameFontSize": "5.sp",
          "menuTitleFontSize": "8.sp",
          "menuSubTitleFontSize": "4.2.sp",
          "normalFontSize": "3.sp",
          "productFontSize": "3.6.sp",
          "categoryDividerGap": "0.5.sp",
          "dividerProductGap": "1.5.sp",
          "productGap": "2.2.sp",
          "columnGap": "1.sp",
          "columnHorizontalPadding": "18.sp",
          "columnTopPadding": "8.sp",
          "columnBottomPadding": "6.sp",
          "categoryImageVerticalPadding": "3.sp",
          "categoryBackgroundHeight": "77.h"
        },
        "21/9": {
          "categoryNameFontSize": "5.sp",
          "menuTitleFontSize": "8.sp",
          "menuSubTitleFontSize": "4.2.sp",
          "normalFontSize": "3.2.sp",
          "productFontSize": "3.6.sp",
          "categoryDividerGap": "0.5.sp",
          "dividerProductGap": "1.5.sp",
          "productGap": "2.2.sp",
          "columnGap": "1.sp",
          "columnHorizontalPadding": "18.sp",
          "columnTopPadding": "8.sp",
          "columnBottomPadding": "6.sp",
          "categoryImageVerticalPadding": "3.sp",
          "categoryBackgroundHeight": "77.h"
        },
        "16/9": {
          "categoryNameFontSize": "5.sp",
          "menuTitleFontSize": "10.sp",
          "menuSubTitleFontSize": "6.sp",
          "normalFontSize": "3.6.sp",
          "productFontSize": "3.8.sp",
          "categoryDividerGap": "1.sp",
          "dividerProductGap": "3.5.sp",
          "productGap": "4.2.sp",
          "columnGap": "1.sp",
          "columnHorizontalPadding": "14.sp",
          "columnTopPadding": "14.sp",
          "columnBottomPadding": "10.sp",
          "categoryImageVerticalPadding": "6.sp",
          "categoryBackgroundHeight": "77.h"
        },
        "16/10": {
          "categoryNameFontSize": "6.sp",
          "menuTitleFontSize": "11.sp",
          "menuSubTitleFontSize": "6.5.sp",
          "normalFontSize": "3.8.sp",
          "productFontSize": "4.2.sp",
          "categoryDividerGap": "1.sp",
          "dividerProductGap": "3.5.sp",
          "productGap": "4.2.sp",
          "columnGap": "1.sp",
          "columnHorizontalPadding": "14.sp",
          "columnTopPadding": "14.sp",
          "columnBottomPadding": "10.sp",
          "categoryImageVerticalPadding": "6.sp",
          "categoryBackgroundHeight": "75.h"
        },
        "4/3": {
          "categoryNameFontSize": "8.sp",
          "menuTitleFontSize": "11.sp",
          "menuSubTitleFontSize": "8.sp",
          "normalFontSize": "4.5.sp",
          "productFontSize": "4.8.sp",
          "categoryDividerGap": "1.sp",
          "dividerProductGap": "4.5.sp",
          "productGap": "5.6.sp",
          "columnGap": "1.sp",
          "columnHorizontalPadding": "12.sp",
          "columnTopPadding": "14.sp",
          "columnBottomPadding": "10.sp",
          "categoryImageVerticalPadding": "6.sp",
          "categoryBackgroundHeight": "75.h"
        },
        "default": {
          "categoryNameFontSize": "5.sp",
          "menuTitleFontSize": "10.sp",
          "menuSubTitleFontSize": "6.sp",
          "normalFontSize": "3.6.sp",
          "productFontSize": "3.8.sp",
          "categoryDividerGap": "1.sp",
          "dividerProductGap": "3.5.sp",
          "productGap": "4.2.sp",
          "columnGap": "1.sp",
          "columnHorizontalPadding": "14.sp",
          "columnTopPadding": "14.sp",
          "columnBottomPadding": "10.sp",
          "categoryImageVerticalPadding": "6.sp",
          "categoryBackgroundHeight": "77.h"
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "CATEGORY 1",
        "image": "",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "CATEGORY 2",
        "image": "",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "CATEGORY 3",
        "image": "",
      },
      //Danh mục 1: pro 1, 2, 3, 4, 5, 6, 7, 8
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$40.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$50.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$60.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$70.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Danh mục 2: pro 9, 10, 11, 12
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Danh mục 3: pro 13, 14, 15, 16, 17, 18, 19, 20
      "product13": {
        "key": "product13",
        "type": "product",
        "name": "Product 13",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product14": {
        "key": "product14",
        "type": "product",
        "name": "Product 14",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product15": {
        "key": "product15",
        "type": "product",
        "name": "Product 15",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product16": {
        "key": "product16",
        "type": "product",
        "name": "Product 16",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product17": {
        "key": "product17",
        "type": "product",
        "name": "Product 17",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product18": {
        "key": "product18",
        "type": "product",
        "name": "Product 18",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product19": {
        "key": "product19",
        "type": "product",
        "name": "Product 19",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product20": {
        "key": "product20",
        "type": "product",
        "name": "Product 20",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Hình column background
      "columnBackground": {
        "key": "columnBackground",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/s3-1.webp",
      },

      // category image 1
      "categoryImage1": {
        "key": "categoryImage1",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/cat_1-1.webp",
      },

      // category image 2
      "categoryImage2": {
        "key": "categoryImage2",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/cat_2_1.webp",
      },

      // category image 3
      "categoryImage3": {
        "key": "categoryImage3",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/cat_3-1.webp",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgetSetting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "color": "0xffffc823",
        },
        "width": "100.w",
        "child": {
          "key": "",
          "widgetName": "Row",
          "widgetSetting": {
            "children": [
              // Column 1
              {
                "key": "",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        // Background
                        {
                          "key": "",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "top": "0",
                            "left": "0",
                            "child": {
                              "key": "",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey":
                                    "categoryBackgroundHeight",
                                "child": {
                                  "key": "",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "imageDataKey": "columnBackground",
                                    "fit": "cover"
                                  }
                                }
                              }
                            }
                          }
                        },
                        // Category column 1
                        {
                          "key": "",
                          "widgetName": "PaddingByRatio",
                          "widgetSetting": {
                            "horizontalPaddingFieldKey":
                                "columnHorizontalPadding",
                            "topPaddingFieldKey": "columnTopPadding",
                            "bottomPaddingFieldKey": "columnBottomPadding",
                            "child": {
                              "key": "",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "crossAxisAlignment": "start",
                                "children": [
                                  {
                                    // Category title 1
                                    "key": "",
                                    "widgetName": "Text",
                                    "widgetSetting": {
                                      "dataFromSetting": "category1.name",
                                      "style": {
                                        "color": "0xffffcb30",
                                        "fontWeight": "w700",
                                        "fontFamily": "Montserrat"
                                      },
                                      "fontSizeFromFieldKey":
                                          "categoryNameFontSize"
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey":
                                          "categoryDividerGap",
                                    }
                                  },
                                  // Divider
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "width": "10.sp",
                                      "child": {
                                        "key": "",
                                        "widgetName": "Divider",
                                        "widgetSetting": {
                                          "color": "0xffffcb30",
                                          "thickness": ".5.sp",
                                        }
                                      }
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "dividerProductGap",
                                    }
                                  },
                                  // Product 1
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product1",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 2
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product2",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 3
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product3",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 4
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product4",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 5
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product5",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 6
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product6",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 7
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product7",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 8
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product8",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Category image 1
                                  {
                                    "key": "",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "key": "",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "topPaddingFieldKey":
                                              "categoryImageVerticalPadding",
                                          "child": {
                                            "key": "",
                                            "widgetName": "SizedBoxByRatio",
                                            "widgetSetting": {
                                              "width": "maxFinite",
                                              "height": "maxFinite",
                                              "child": {
                                                "key": "",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey":
                                                      "categoryImage1",
                                                  "fit": "contain",
                                                }
                                              }
                                            }
                                          },
                                        }
                                      },
                                    }
                                  },
                                ]
                              }
                            }
                          }
                        }
                      ]
                    }
                  },
                }
              },
              // Spacing
              {
                "key": "",
                "widgetName": "SizedBoxByRatio",
                "widgetSetting": {
                  "widthFromFieldKey": "columnGap",
                }
              },
              // Column 2
              {
                "key": "",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        // Background
                        {
                          "key": "",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "bottom": "0",
                            "left": "0",
                            "child": {
                              "key": "",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey":
                                    "categoryBackgroundHeight",
                                "child": {
                                  "key": "",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "imageDataKey": "columnBackground",
                                    "fit": "cover"
                                  }
                                }
                              }
                            }
                          }
                        },
                        // Category column 2
                        {
                          "key": "",
                          "widgetName": "PaddingByRatio",
                          "widgetSetting": {
                            "horizontalPaddingFieldKey":
                                "columnHorizontalPadding",
                            "topPaddingFieldKey": "columnTopPadding",
                            "bottomPaddingFieldKey": "columnBottomPadding",
                            "child": {
                              "key": "",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "crossAxisAlignment": "start",
                                "children": [
                                  // Category image 2
                                  {
                                    "key": "",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "key": "",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "bottomPaddingFieldKey":
                                              "categoryImageVerticalPadding",
                                          "child": {
                                            "key": "",
                                            "widgetName": "SizedBoxByRatio",
                                            "widgetSetting": {
                                              "width": "maxFinite",
                                              "height": "maxFinite",
                                              "child": {
                                                "key": "",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey":
                                                      "categoryImage2",
                                                  "fit": "contain",
                                                }
                                              }
                                            }
                                          },
                                        }
                                      },
                                    }
                                  },

                                  {
                                    // Category title 2
                                    "key": "",
                                    "widgetName": "Text",
                                    "widgetSetting": {
                                      "dataFromSetting": "category2.name",
                                      "style": {
                                        "color": "0xffffcb30",
                                        "fontWeight": "w700",
                                        "fontFamily": "Montserrat"
                                      },
                                      "fontSizeFromFieldKey":
                                          "categoryNameFontSize"
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey":
                                          "categoryDividerGap",
                                    }
                                  },
                                  // Divider
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "width": "10.sp",
                                      "child": {
                                        "key": "",
                                        "widgetName": "Divider",
                                        "widgetSetting": {
                                          "color": "0xffffcb30",
                                          "thickness": ".5.sp",
                                        }
                                      }
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "dividerProductGap",
                                    }
                                  },
                                  // Product 9
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product9",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 10
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product10",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 11
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product11",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 12
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product12",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {"height": "3.h"}
                                  },
                                  {
                                    "key": "",
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "width": "maxFinite",
                                      "child": {
                                        "key": "",
                                        "widgetName": "Column",
                                        "widgetSetting": {
                                          "crossAxisAlignment": "center",
                                          "children": [
                                            {
                                              "key": "",
                                              "widgetName": "Text",
                                              "widgetSetting": {
                                                "data": "THE BRAND",
                                                "style": {
                                                  "color": "0xffffcb30",
                                                  "fontWeight": "w800",
                                                  "fontFamily": "Montserrat"
                                                },
                                                "fontSizeFromFieldKey":
                                                    "menuTitleFontSize"
                                              }
                                            },
                                            {
                                              "key": "",
                                              "widgetName": "Text",
                                              "widgetSetting": {
                                                "data": "RESTAURANT",
                                                "style": {
                                                  "color": "0xffffcb30",
                                                  "fontWeight": "w700",
                                                  "fontFamily": "Montserrat"
                                                },
                                                "fontSizeFromFieldKey":
                                                    "menuSubTitleFontSize"
                                              }
                                            },
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "height": "1.sp",
                                              }
                                            },
                                            {
                                              "key": "",
                                              "widgetName": "SizedBox",
                                              "widgetSetting": {
                                                "width": "12.sp",
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Divider",
                                                  "widgetSetting": {
                                                    "color": "0xffffcb30",
                                                    "thickness": "1.sp"
                                                  }
                                                },
                                              }
                                            },
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "height": "2.sp",
                                              }
                                            },
                                            {
                                              "key": "",
                                              "widgetName": "Text",
                                              "widgetSetting": {
                                                "data": "📞 +123-456-7890",
                                                "style": {
                                                  "fontWeight": "w500",
                                                  "fontFamily": "Montserrat",
                                                  "color": "0xffffffff"
                                                },
                                                "fontSizeFromFieldKey":
                                                    "normalFontSize",
                                              }
                                            },
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "height": "1.sp",
                                              }
                                            },
                                            {
                                              "key": "",
                                              "widgetName": "Text",
                                              "widgetSetting": {
                                                "data": "🌐 www.domain.com",
                                                "style": {
                                                  "fontWeight": "w500",
                                                  "fontFamily": "Montserrat",
                                                  "color": "0xffffffff"
                                                },
                                                "fontSizeFromFieldKey":
                                                    "normalFontSize",
                                              }
                                            },
                                          ]
                                        }
                                      }
                                    }
                                  },
                                ]
                              }
                            }
                          }
                        }
                      ]
                    }
                  },
                }
              },
              // Spacing
              {
                "key": "",
                "widgetName": "SizedBoxByRatio",
                "widgetSetting": {
                  "widthFromFieldKey": "columnGap",
                }
              },
              // Column 3
              {
                "key": "",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "key": "",
                    "widgetName": "Stack",
                    "widgetSetting": {
                      "children": [
                        // Background
                        {
                          "key": "",
                          "widgetName": "Positioned",
                          "widgetSetting": {
                            "top": "0",
                            "left": "0",
                            "child": {
                              "key": "",
                              "widgetName": "SizedBoxByRatio",
                              "widgetSetting": {
                                "heightFromFieldKey":
                                    "categoryBackgroundHeight",
                                "child": {
                                  "key": "",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "imageDataKey": "columnBackground",
                                    "fit": "cover"
                                  }
                                }
                              }
                            }
                          }
                        },
                        // Category column 3
                        {
                          "key": "",
                          "widgetName": "PaddingByRatio",
                          "widgetSetting": {
                            "horizontalPaddingFieldKey":
                                "columnHorizontalPadding",
                            "topPaddingFieldKey": "columnTopPadding",
                            "bottomPaddingFieldKey": "columnBottomPadding",
                            "child": {
                              "key": "",
                              "widgetName": "Column",
                              "widgetSetting": {
                                "crossAxisAlignment": "start",
                                "children": [
                                  {
                                    // Category title 3
                                    "key": "",
                                    "widgetName": "Text",
                                    "widgetSetting": {
                                      "dataFromSetting": "category3.name",
                                      "style": {
                                        "color": "0xffffcb30",
                                        "fontWeight": "w700",
                                        "fontFamily": "Montserrat"
                                      },
                                      "fontSizeFromFieldKey":
                                          "categoryNameFontSize"
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey":
                                          "categoryDividerGap",
                                    }
                                  },
                                  // Divider
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "width": "10.sp",
                                      "child": {
                                        "key": "",
                                        "widgetName": "Divider",
                                        "widgetSetting": {
                                          "color": "0xffffcb30",
                                          "thickness": ".5.sp",
                                        }
                                      }
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "dividerProductGap",
                                    }
                                  },
                                  // Product 13
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product13",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 14
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product14",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 15
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product15",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 16
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product16",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 17
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product17",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 18
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product18",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 19
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product19",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Spacing
                                  {
                                    "key": "",
                                    "widgetName": "SizedBoxByRatio",
                                    "widgetSetting": {
                                      "heightFromFieldKey": "productGap"
                                    }
                                  },
                                  // Product 20
                                  {
                                    "key": "",
                                    "widgetName": "ProductRow1",
                                    "widgetSetting": {
                                      "product": "product20",
                                      "border": {
                                        "all": {
                                          "width": "0",
                                          "color": "0x00000000"
                                        }
                                      },
                                      "productPriceTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "productNameTextStyle": {
                                        "fontFamily": "Montserrat",
                                        "fontWeight": "w500",
                                        "color": "0xffffffff"
                                      },
                                      "prefixProductPrice": "",
                                      "productNameFontSizeFromFieldKey":
                                          "productFontSize",
                                      "productPriceFontSizeFromFieldKey":
                                          "productFontSize",
                                    }
                                  },
                                  // Category image 3
                                  {
                                    "key": "",
                                    "widgetName": "Expanded",
                                    "widgetSetting": {
                                      "child": {
                                        "key": "",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "topPaddingFieldKey":
                                              "categoryImageVerticalPadding",
                                          "child": {
                                            "key": "",
                                            "widgetName": "SizedBoxByRatio",
                                            "widgetSetting": {
                                              "width": "maxFinite",
                                              "height": "maxFinite",
                                              "child": {
                                                "key": "",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey":
                                                      "categoryImage3",
                                                  "fit": "contain",
                                                }
                                              }
                                            }
                                          },
                                        }
                                      },
                                    }
                                  },
                                ]
                              }
                            }
                          }
                        }
                      ]
                    }
                  },
                }
              },
            ]
          }
        }
      }
    }
  };
}
