class JsonDataTemplate {
  //[092328HG] Check Nail5: Run OK - Ratio OK
  //[092304Tuan] adjust according to feedback from Mr Hugo
  //[092305Tuan] adjust according to feedback from Mr Hugo
  static final templateNail5 = {
    "_id": "",
    "templateSlug": "temp_nail_5",
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'DM Serif Display',
        },
        'nameProductStyle': {
          'fontWeight': 'w500',
          'fontFamily': 'DM Serif Display',
          'height': '1.5',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'bold',
          'fontFamily': 'DM Serif Display',
          'height': '1.5',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
          'height': '1.5',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'DM Serif Text',
          'height': '1.5',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w600',
          'fontFamily': 'DM Serif Display',
        },
        'priceProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyleMedium': '0.8.w',
          'priceProductStyleMedium': '1.2.w',
          'discriptionProductStyleMedium': '0.6.w',
        },
        '21/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyleMedium': '1.2.w',
          'priceProductStyleMedium': '1.5.w',
          'discriptionProductStyleMedium': '0.9.w',
        },
        '16/9': {
          'categoryNameStyle': '2.5.w',
          'nameProductStyleMedium': '1.5.w',
          'priceProductStyleMedium': '2.4.w',
          'discriptionProductStyleMedium': '1.w',
        },
        '16/10': {
          'categoryNameStyleMedium': '3.w',
          'nameProductStyleMedium': '1.4.w',
          'priceProductStyleMedium': '2.3.w',
          'discriptionProductStyleMedium': '1.w',
        },
        '4/3': {
          'categoryNameStyle': '1.8.w',
          'nameProductStyleMedium': '2.w',
          'priceProductStyleMedium': '3.w',
          'discriptionProductStyleMedium': '1.5.w',
        },
        'default': {
          'categoryNameStyle': '2.w',
          'nameProductStyleMedium': '2.w',
          'priceProductStyleMedium': '2.w',
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '',
      },

      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '',
      },
      //Danh mục 1: pro 1, 2 ,3 ,4 , 5
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$23.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$23.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      //Danh mục 2: pro 6, 7 ,8 ,9 , 10
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$23.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$24.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$25.0',
        'image': '',
        'description':
            'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor',
      },

      'image1': {
        'id': 'image1',
        'key': 'image1',
        'type': 'image',
        'image':
            'https://foodiemenu.co/wp-content/uploads/2023/10/close-up-woman-getting-manicure-done_23-2149171309.jpg',
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      // begin: container
      "widgetName": "Container",
      "widgetSetting": {
        "width": "100.w",
        "height": "100.h",
        "color": "0xFFFFFFFF",
        "child": {
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              //background
              {
                "widgetName": "Column",
                "widgetSetting": {
                  "children": [
                    {
                      "widgetName": "Container",
                      "widgetSetting": {
                        "height": "5.h",
                        "color": "0xFFA76744", //primary color
                      },
                    },
                    {
                      "widgetName": "Expanded",
                      "widgetSetting": {"": ""},
                    },
                    {
                      "widgetName": "Container",
                      "widgetSetting": {
                        "height": "20.h",
                        "color": "0xFF2E292D",
                        "child": {
                          "widgetName": "Padding",
                          "widgetSetting": {
                            "padding": {
                              "left": "5.w",
                              "top": "2.h",
                              "bottom": "2.h",
                            },
                            "child": {
                              "widgetName": "Column",
                              "widgetSetting": {
                                "crossAxisAlignment": "start",
                                "mainAxisAlignment": "spaceAround",
                                "children": [
                                  {
                                    "widgetName": "Row",
                                    "widgetSetting": {
                                      "children": [
                                        {
                                          "widgetName": "CustomContainer",
                                          "widgetSetting": {
                                            "color": "0xFFA76744",
                                            "blendMode": "modulate",
                                            "height": "8.h",
                                            "width": "8.h",
                                            "image":
                                                "http://foodiemenu.co/wp-content/uploads/2023/09/white-globe-icon-24-2.png",
                                          },
                                        },
                                        {
                                          "widgetName": "Text",
                                          "widgetSetting": {
                                            "data": "WWW.SALON.WWW",
                                            "style": {
                                              "fontFamily": "DM Serif Display",
                                              "color": "0xFFFFFFFF",
                                              "fontSize": "2.5.h",
                                            },
                                          },
                                        },
                                        {
                                          "widgetName": "SizedBox",
                                          "widgetSetting": {"width": "5.w"},
                                        },
                                        {
                                          "widgetName": "CustomContainer",
                                          "widgetSetting": {
                                            "color": "0xFFA76744",
                                            "blendMode": "modulate",
                                            "height": "6.h",
                                            "width": "6.h",
                                            "image":
                                                "http://foodiemenu.co/wp-content/uploads/2023/09/pinpng.com-contact-icon-png-666849.png",
                                          },
                                        },
                                        {
                                          "widgetName": "Text",
                                          "widgetSetting": {
                                            "data": "+1223 334 444",
                                            "style": {
                                              "fontFamily": "DM Serif Display",
                                              "color": "0xFFFFFFFF",
                                              "fontSize": "2.5.h",
                                            },
                                          },
                                        },
                                      ],
                                    },
                                  }
                                ],
                              },
                            },
                          },
                        },
                      },
                    },
                  ],
                },
              },
              //content
              {
                "widgetName": "Padding",
                "widgetSetting": {
                  "padding": {"top": "5.h"},
                  "child": {
                    "widgetName": "Row",
                    "widgetSetting": {
                      "children": [
                        {
                          //left column
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "widgetName": "Center",
                              "widgetSetting": {
                                "child": {
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "crossAxisAlignment": "start",
                                    "children": [
                                      {
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {"height": "3.h"},
                                      },
                                      {
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "BEAUTY SERVICES",
                                          "style": {
                                            "fontFamily": "DM Serif Display",
                                            "color": "0xFF000000",
                                            "fontSize": "5.h",
                                            "fontWeight": "bold",
                                          },
                                        },
                                      },
                                      {
                                        "widgetName": "SizedBox",
                                        "widgetSetting": {"height": "2.h"},
                                      },
                                      {
                                        "widgetName": "Wrap",
                                        "widgetSetting": {
                                          "spacing": "2.w",
                                          "runSpacing": "4.h",
                                          "children": [
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "productId": "product1",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "colorDescription":
                                                    "0xFF000000",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product2",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product3",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product4",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product5",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product6",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product7",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                            {
                                              "widgetName":
                                                  "ServiceItemCustom1",
                                              "widgetSetting": {
                                                "colorDescription":
                                                    "0xFF000000",
                                                "productId": "product8",
                                                "colorPrice": "0xFFA76744",
                                                "colorProductName":
                                                    "0xFFA76744",
                                                "width": "23.w",
                                                "useMediumStyle": "true",
                                              },
                                            },
                                          ],
                                        },
                                      },
                                    ],
                                  },
                                },
                              },
                            },
                          },
                        },
                        {
                          //right column
                          "widgetName": "SizedBox",
                          "widgetSetting": {
                            "width": "45.w",
                            "child": {
                              "widgetName": "Column",
                              "widgetSetting": {
                                "children": [
                                  {
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "height": "5.h",
                                    },
                                  },
                                  {
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "height": "20.h",
                                      "child": {
                                        "widgetName": "Row",
                                        "widgetSetting": {
                                          "mainAxisAlignment": "spaceBetween",
                                          "children": [
                                            {
                                              "widgetName": "Expanded",
                                              "widgetSetting": {
                                                "child": {
                                                  "widgetName": "Column",
                                                  "widgetSetting": {
                                                    "crossAxisAlignment":
                                                        "start",
                                                    "children": [
                                                      {
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data":
                                                              "Our Missions",
                                                          "style": {
                                                            "color":
                                                                "0xFFA76744",
                                                            "fontWeight":
                                                                "bold",
                                                            "fontSize": "3.5.h",
                                                            "fontFamily":
                                                                "DM Serif Display"
                                                          },
                                                          "textAlign": "left",
                                                        }
                                                      },
                                                      {
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data": "Lorem ipsum dolor sit amet consectetur,"
                                                              "Lorem ipsum dolor sit amet, consectetur"
                                                              "Lorem ipsum dolor sit amet, consectetur",
                                                          "style": {
                                                            "fontFamily":
                                                                "DM Serif Text",
                                                            "fontSize": "2.h",
                                                            "color":
                                                                "0xFF000000",
                                                          },
                                                          "textAlign": "left",
                                                        }
                                                      }
                                                    ],
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              "widgetName": "SizedBox",
                                              "widgetSetting": {"width": "4.w"},
                                            },
                                            {
                                              "widgetName": "Expanded",
                                              "widgetSetting": {
                                                "child": {
                                                  "widgetName": "Column",
                                                  "widgetSetting": {
                                                    "crossAxisAlignment":
                                                        "start",
                                                    "children": [
                                                      {
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data":
                                                              "Our Missions",
                                                          "style": {
                                                            "color":
                                                                "0xFFA76744",
                                                            "fontWeight":
                                                                "bold",
                                                            "fontSize": "3.5.h",
                                                            "fontFamily":
                                                                "DM Serif Display"
                                                          },
                                                          "textAlign": "left",
                                                        }
                                                      },
                                                      {
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data": "Lorem ipsum dolor sit amet con sectetur, "
                                                              "Lorem ipsum dolor sit amet, consectetur"
                                                              "Lorem ipsum dolor sit amet, consectetur",
                                                          "style": {
                                                            "fontFamily":
                                                                "DM Serif Text",
                                                            "fontSize": "2.h",
                                                            "color":
                                                                "0xFF000000",
                                                          },
                                                          "textAlign": "left",
                                                        }
                                                      }
                                                    ],
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              "widgetName": "SizedBox",
                                              "widgetSetting": {"width": "4.w"},
                                            },
                                          ],
                                        },
                                      },
                                    },
                                  },
                                  {
                                    "widgetName": "SizedBox",
                                    "widgetSetting": {
                                      "height": "55.h",
                                      "child": {
                                        "widgetName": "Stack",
                                        "widgetSetting": {
                                          "children": [
                                            {
                                              "widgetName": "CustomContainer",
                                              "widgetSetting": {
                                                "height": "50.h",
                                                'opacity': '1',
                                                "blendMode": "dstOver",
                                                "fit": "contain",
                                                "alignment": "topLeft",
                                                "color": "0xFFA76744",
                                                "image":
                                                    "https://foodiemenu.co/wp-content/uploads/2023/10/outline-vector-flower-illustration-corner-border-frame-floral-elements-coloring-page-outline-vector-flower-illustration-189504420-e1696476093637.webp",
                                              },
                                            },
                                            {
                                              "widgetName": "Container",
                                              "widgetSetting": {
                                                "height": "50.h",
                                                "decoration": {
                                                  "color": "0xAAFFFFFF",
                                                  "opacity": "1.0",
                                                }
                                              },
                                            },
                                            {
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "right": "0",
                                                "bottom": "0",
                                                "child": {
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "height": "50.h",
                                                    "width": "30.w",
                                                    "color": "0xFFA76744",
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "right": "21.5.w",
                                                "top": "5.5.h",
                                                "child": {
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "height": "9.h",
                                                    "width": "10.w",
                                                    "child": {
                                                      "widgetName": "Center",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "widgetName": "Text",
                                                          "widgetSetting": {
                                                            "data":
                                                                "DISCOUNT OF\nTHE MONTH",
                                                            "style": {
                                                              "fontFamily":
                                                                  "DM Serif Text",
                                                              "color":
                                                                  "0xFFFFFFFF",
                                                              "fontSize":
                                                                  "2.5.sp",
                                                            },
                                                          }
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "right": "0",
                                                "bottom": "2.5.h",
                                                "child": {
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "height": "37.5.h",
                                                    "width": "40.w",
                                                    "decoration": {
                                                      "image": "image1",
                                                      "boxFit": "cover",
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              "widgetName": "Positioned",
                                              "widgetSetting": {
                                                "top": "5.5.h",
                                                "right": "0",
                                                "child": {
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "color": "0xFFFFFFFF",
                                                    "height": "9.h",
                                                    "width": "23.w",
                                                    "child": {
                                                      "widgetName": "Center",
                                                      "widgetSetting": {
                                                        "child": {
                                                          "widgetName": "Text",
                                                          "widgetSetting": {
                                                            "data": "50% OFF",
                                                            "style": {
                                                              "color":
                                                                  "0xFFA76744",
                                                              "fontWeight":
                                                                  "bold",
                                                              "fontSize": "6.h",
                                                              "fontFamily":
                                                                  "DM Serif Display"
                                                            },
                                                          },
                                                        }
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            }
                                          ],
                                        },
                                      },
                                    },
                                  },
                                ],
                              },
                            },
                          },
                        }
                      ],
                    },
                  },
                },
              },
            ],
          },
        },
      },
    },
  };
}
