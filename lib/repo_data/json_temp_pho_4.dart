class JsonDataTemplate {
  //[092328HG] Check temppho4: Run OK - Ratio OK
  static final temppho4 = {
    "_id": "",
    "templateSlug": "temp_pho_4",
    "isDeleted": false,
    "RatioSetting": {
      "renderScreen": {
        "32/9": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "21/9": {
          "categoryNameFontSize": "5.sp",
          "categoryDesFontSize": "3.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "16/9": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "3.sp",
          "productDesFontSize": "2.5.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "16/10": {
          "categoryNameFontSize": "10.sp",
          "categoryDesFontSize": "4.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "4.sp",
          "productPriceFontSize": "4.sp",
          "productDesFontSize": "2.8.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "4/3": {
          "categoryNameFontSize": "12.sp",
          "categoryDesFontSize": "5.2.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "4.5.sp",
          "productPriceFontSize": "4.5.sp",
          "productDesFontSize": "3.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
        "default": {
          "categoryNameFontSize": "8.sp",
          "categoryDesFontSize": "3.8.sp",
          "brandFontSize": "10.sp",
          "productNameFontSize": "2.8.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "2.2.sp",
          "verticalFramePadding": "5.sp",
          "horizontalFramePadding": "5.sp",
        },
      },
    },
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "Category 1",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "Category 2",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "Category 3",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "category4": {
        "key": "category4",
        "type": "category",
        "name": "Category 4",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      //Danh mục 1: pro 1, 2, 3
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "11.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "12.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "13.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      //Danh mục 2: pro  4, 5, 6
      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "21.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "22.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "23.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      // Danh mục 3: pro  7, 8, 9
      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "24.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "25.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "31.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      // Danh mục 4: 10, 11, 12
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "32.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "33.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },
      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "41.0",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
      },

      "cat1Image": {
        "key": "cat1Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_1.png"
      },
      "cat2Image": {
        "key": "cat2Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_2.png"
      },
      "cat3Image": {
        "key": "cat3Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_3.png"
      },
      "cat4Image": {
        "key": "cat4Image",
        "type": "image",
        "image": "http://foodiemenu.co/wp-content/uploads/2023/09/cat_4.png"
      }
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "height": "100.h",
        "width": "100.w",
        "decoration": {"color": "0xffece3d8"},
        "child": {
          "key": "Padding-menu",
          "widgetName": "PaddingByRatio",
          "widgetSetting": {
            "verticalPaddingFieldKey": "verticalFramePadding",
            "horizontalPaddingFieldKey": "horizontalFramePadding",
            "child": {
              // menu
              "key": "Wrap-menu",
              "widgetName": "Wrap",
              "widgetSetting": {
                "direction": "vertical",
                "alignment": "center",
                "runAlignment": "center",
                "children": [
                  // cat 1
                  {
                    "key": "SizedBox-cat1",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "key": "CategoryBlock2-cat1",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category1",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            // "fontSize": "8.sp",
                            "color": "0xff583618",
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            // "fontSize": "2.8.sp",
                            "fontStyle": "italic",
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "key": "divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "categoryItems": [
                            // pro 1
                            {
                              "key": "SizedBox-p1",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p1",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product1",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 2
                            {
                              "key": "SizedBox-p2",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p2",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product2",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 3
                            {
                              "key": "SizedBox-p3",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p3",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product3",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // cat 1 - image
                            {
                              "key": "Container-image-cat4",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "key": "SimpleImage-cat1",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat1Image",
                                    "fit": "contain",
                                  }
                                }
                              }
                            }
                          ],
                        }
                      }
                    }
                  },
                  // cat 2
                  {
                    "key": "SizedBox-cat2",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "key": "CategoryBlock2-cat2",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category2",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            // "fontSize": "8.sp",
                            "color": "0xff583618",
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            // "fontSize": "2.8.sp",
                            "fontStyle": "italic",
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "key": "divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "categoryItems": [
                            // cat 2 - image
                            {
                              "key": "Container-image-cat4",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "key": "SimpleImage-cat1",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat2Image",
                                    "fit": "contain"
                                  }
                                }
                              }
                            },
                            // pro 4
                            {
                              "key": "SizedBox-p4",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p4",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product4",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 5
                            {
                              "key": "SizedBox-p5",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p5",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product5",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 6
                            {
                              "key": "SizedBox-p6",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p6",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product6",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  },
                  // cat 3
                  {
                    "key": "SizedBox-cat3",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "key": "CategoryBlock2-cat3",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category3",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            // "fontSize": "8.sp",
                            "color": "0xff583618",
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            // "fontSize": "2.8.sp",
                            "fontStyle": "italic",
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "key": "divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "categoryItems": [
                            // pro 7
                            {
                              "key": "SizedBox-p7",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p7",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product7",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 8
                            {
                              "key": "SizedBox-p8",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p8",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product8",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 9
                            {
                              "key": "SizedBox-p9",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p9",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product9",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },

                            // cat 3 - image
                            {
                              "key": "Container-image-cat4",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "key": "SimpleImage-cat3",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat3Image",
                                    "fit": "contain"
                                  }
                                }
                              }
                            }
                          ],
                        }
                      }
                    }
                  },
                  // cat 4
                  {
                    "key": "SizedBox-cat4",
                    "widgetName": "SizedBox",
                    "widgetSetting": {
                      "height": "45.h",
                      "width": "45.w",
                      "child": {
                        "key": "CategoryBlock2-cat4",
                        "widgetName": "CategoryBlock2",
                        "widgetSetting": {
                          "category": "category4",
                          "categoryNameFontSizeFromFieldKey":
                              "categoryNameFontSize",
                          "categoryDesFontSizeFromFieldKey":
                              "categoryDesFontSize",
                          "categoryNameTextStyle": {
                            "fontFamily": "Roboto Slab",
                            // "fontSize": "8.sp",
                            "color": "0xff583618",
                          },
                          "categoryDescriptionTextStyle": {
                            "fontFamily": "Dancing Script",
                            // "fontSize": "2.8.sp",
                            "fontStyle": "italic",
                          },
                          "indentCategoryDescription": "10.sp",
                          "categoryItemSectionPadding": "5.sp",
                          "divider": {
                            "key": "divider",
                            "widgetName": "Divider",
                            "widgetSetting": {
                              "color": "0xffc6ac82",
                              "indent": "2.w",
                            }
                          },
                          "categoryItems": [
                            // cat 4 - image
                            {
                              "key": "Container-image-cat4",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "width": "20.w",
                                "height": "25.h",
                                "padding": {"all": "2.sp"},
                                "child": {
                                  "key": "SimpleImage-cat4",
                                  "widgetName": "SimpleImage",
                                  "widgetSetting": {
                                    "Width": "20.w",
                                    "imageDataKey": "cat4Image",
                                    "fit": "contain"
                                  }
                                }
                              }
                            },
                            // pro 10
                            {
                              "key": "SizedBox-p10",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p10",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product10",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 11
                            {
                              "key": "SizedBox-p11",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p11",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product11",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                            // pro 12
                            {
                              "key": "SizedBox-p12",
                              "widgetName": "SizedBox",
                              "widgetSetting": {
                                "width": "20.w",
                                "child": {
                                  "key": "ProductRow2-p12",
                                  "widgetName": "ProductRow2",
                                  "widgetSetting": {
                                    "product": "product12",
                                    "productPriceFontSizeFromFieldKey":
                                        "productPriceFontSize",
                                    "productNameFontSizeFromFieldKey":
                                        "productNameFontSize",
                                    "productDesFontSizeFromFieldKey":
                                        "productDesFontSize",
                                    "productNameTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productPriceTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.8.sp",
                                      "fontWeight": "w700",
                                    },
                                    "productDescriptionTextStyle": {
                                      "fontFamily": "Open Sans",
                                      // "fontSize": "2.2.sp",
                                    },
                                    "fractionDigitsForPrice": "2",
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  }
                ]
              }
            }
          },
        }
      },
    },
  };
}
