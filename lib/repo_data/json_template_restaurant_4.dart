class JsonDataTemplate {
  //[092328HG] Check temprestaurant4: Run OK - Ratio OK
  static final temprestaurant4 = {
    "_id": "idTemplateRestaurant4",
    "templateSlug": "temp_restaurant_4",
    "isDeleted": false,
    "RatioSetting": {
      'textStyle': {
        'categoryNameStyle': {
          'fontWeight': 'bold',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'nameProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'discriptionProductStyle': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'discriptionProductStyleMedium': {
          'fontWeight': 'w300',
          'fontFamily': 'Open Sans',
        },
        'priceProductStyle': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
        'priceProductStyleMedium': {
          'fontWeight': 'w800',
          'fontFamily': 'Bebas Neue',
        },
      },
      'renderScreen': {
        '32/9': {
          'categoryNameStyle': '1.5.w',
          'nameProductStyle': '1.w',
          'nameProductStyleMedium': '0.6.w',
          'priceProductStyle': '1.w',
          'priceProductStyleMedium': '0.6.w',
          'discriptionProductStyle': '0.6.w',
          'discriptionProductStyleMedium': '0.5.w',
          'imageHeight': '40.h',
          'marginBottomImage': '0.7.w',
          'marginBottomProduct': '1.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '21/9': {
          'categoryNameStyle': '2.w',
          'nameProductStyle': '1.3.w',
          'nameProductStyleMedium': '0.7.w',
          'priceProductStyle': '1.3.w',
          'priceProductStyleMedium': '0.7.w',
          'discriptionProductStyle': '0.7.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '1.5.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '2.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '16/9': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '16/10': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        '4/3': {
          'categoryNameStyle': '3.5.w',
          'nameProductStyle': '2.w',
          'nameProductStyleMedium': '1.3.w',
          'priceProductStyle': '2.w',
          'priceProductStyleMedium': '1.3.w',
          'discriptionProductStyle': '1.3.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.3.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
        'default': {
          'categoryNameStyle': '3.w',
          'nameProductStyle': '1.5.w',
          'nameProductStyleMedium': '1.w',
          'priceProductStyle': '1.5.w',
          'priceProductStyleMedium': '1.w',
          'discriptionProductStyle': '1.w',
          'discriptionProductStyleMedium': '0.8.w',
          'imageHeight': '40.h',
          'marginBottomImage': '1.w',
          'marginBottomProduct': '2.h',
          'boderRadiusStyle': '30',
          'marginBottomCategoryTitle': '3.h',
          'paddingVerticalCategorTilte': '0.w',
          'paddingHorizontalCategorTilte': '2.w',
          'widthThumnailProduct': '3.w',
          'heightThumnailProduct': '3.w',
        },
      },
    },
    "dataSetting": {
      'category1': {
        'key': 'category1',
        'type': 'category',
        'name': 'Category 1',
        'image': '',
      },
      'category2': {
        'key': 'category2',
        'type': 'category',
        'name': 'Category 2',
        'image': '',
      },
      'category3': {
        'key': 'category3',
        'type': 'category',
        'name': 'Category 3',
        'image': '',
      },
      'category4': {
        'key': 'category4',
        'type': 'category',
        'name': 'Category 4',
        'image': '',
      },
      'category5': {
        'key': 'category5',
        'type': 'category',
        'name': 'Category 5',
        'image': '',
      },
      //Danh mục 1: pro 1, 2, 3, 4, 5, 6
      'product1': {
        'key': 'product1',
        'type': 'product',
        'name': 'Product 1',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product2': {
        'key': 'product2',
        'type': 'product',
        'name': 'Product 2',
        'price': '\$12.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product3': {
        'key': 'product3',
        'type': 'product',
        'name': 'Product 3',
        'price': '\$13.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product4': {
        'key': 'product4',
        'type': 'product',
        'name': 'Product 4',
        'price': '\$40.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product5': {
        'key': 'product5',
        'type': 'product',
        'name': 'Product 5',
        'price': '\$50.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product6': {
        'key': 'product6',
        'type': 'product',
        'name': 'Product 6',
        'price': '\$60.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 2: pro 7, 8, 9
      'product7': {
        'key': 'product7',
        'type': 'product',
        'name': 'Product 7',
        'price': '\$70.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product8': {
        'key': 'product8',
        'type': 'product',
        'name': 'Product 8',
        'price': '\$80.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product9': {
        'key': 'product9',
        'type': 'product',
        'name': 'Product 9',
        'price': '\$90.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 3: pro  10, 11,12
      'product10': {
        'key': 'product10',
        'type': 'product',
        'name': 'Product 10',
        'price': '\$10.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product11': {
        'key': 'product11',
        'type': 'product',
        'name': 'Product 11',
        'price': '\$11.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product12': {
        'key': 'product12',
        'type': 'product',
        'name': 'Product 12',
        'price': '\$41.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 4: pro 13, 14, 15
      'product13': {
        'key': 'product13',
        'type': 'product',
        'name': 'Product 13',
        'price': '\$42.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product14': {
        'key': 'product14',
        'type': 'product',
        'name': 'Product 14',
        'price': '\$43.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product15': {
        'key': 'product15',
        'type': 'product',
        'name': 'Product 15',
        'price': '\$31.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      //Danh mục 5: pro 16, 17, 18
      'product16': {
        'key': 'product16',
        'type': 'product',
        'name': 'Product 16',
        'price': '\$16.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      'product17': {
        'key': 'product17',
        'type': 'product',
        'name': 'Product 17',
        'price': '\$17.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },

      'product18': {
        'key': 'product18',
        'type': 'product',
        'name': 'Product 18',
        'price': '\$18.0',
        'salePrice': '',
        'image': '',
        'description':
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry',
      },
      //Hình background
      'imageBackground': {
        'key': 'imageBackground',
        'type': 'image',
        // 'image': '/temp_restaurant_4/BG-2.png',
        'image': 'http://foodiemenu.co/wp-content/uploads/2023/09/BG-2-1.png',
      },
      // Hình background category title
      "imageBgCategoryTitle": {
        "key": "imageBgCategoryTitle",
        "type": "image",
        // "image": "/temp_restaurant_4/decorate/BG-title-2.png",
        "image":
            "http://foodiemenu.co/wp-content/uploads/2023/09/BG-title-2.png",
      },
      'image1': {
        'key': 'image1',
        'type': 'image',
        // 'image': '/temp_restaurant_4/food-bg(1).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg1-1.jpg',
      },
      'image2': {
        'key': 'image2',
        'type': 'image',
        // 'image': '/temp_restaurant_4/food-bg(2).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg2-1.jpg',
      },
      'image3': {
        'key': 'image3',
        'type': 'image',
        // 'image': '/temp_restaurant_4/food-bg(3).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg3-1.jpg',
      },
      'image4': {
        'key': 'image4',
        'type': 'image',
        // 'image': '/temp_restaurant_4/food-bg(4).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg4-2-scaled.jpg',
      },
      'image5': {
        'key': 'image5',
        'type': 'image',
        // 'image': '/temp_restaurant_4/food-bg(5).jpg',
        'image':
            'http://foodiemenu.co/wp-content/uploads/2023/09/food-bg5-1-scaled.jpg',
      },
    },
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "widht": "100.w",
        "height": "100.h",
        "decoration": {
          "image": "imageBackground",
          "boxFit": "fill",
        },
        "child": {
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              // Cột A
              {
                "key": "expanded-A",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "key": "padding-A",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "widgetName": "Column",
                        "widgetSetting": {
                          "mainAxisAlignment": "start",
                          "children": [
                            // Cột A - Dòng 1 - Danh mục 1
                            {
                              "key": "CTGL-A1",
                              "widgetName": "CategoryTitleGridLayout3",
                              "widgetSetting": {
                                "dataList": {
                                  "category": "category1",
                                  "product": [
                                    "product1",
                                    "product2",
                                    "product3",
                                    "product4",
                                    "product5",
                                    "product6",
                                  ]
                                },
                                "useBackgroundCategoryTitle": "true",
                                // "pathImageBgCategoryTitle":
                                //     "/temp_restaurant_4/decorate/BG-title-2.png",
                                "imageId": "imageBgCategoryTitle",
                                "colorBackgroundCategoryTitle": "0x00121212",
                                "colorCategoryTitle": "0xFF121212",
                                "colorProductName": "0xFF121212",
                                "colorPrice": "0xFF8d1111",
                                "colorDescription": "0xFF121212",
                                "useFullWidthCategoryTitle": "false",
                              }
                            },
                            // Cột A - Dòng 2 - khoảng cách
                            {
                              "key": "PaddingByRatio-A2",
                              "widgetName": "PaddingByRatio",
                              "widgetSetting": {
                                "bottomPaddingDataKey":
                                    "marginBottomCategoryTitle"
                              }
                            },

                            // Cột A - Dòng 3 - Danh mục 2 với
                            // 3 sản phẩm: 7, 8, 9
                            // 3 hình ảnh 1,2,3
                            {
                              "widgetName": "categoryTitleGridLayout4",
                              "widgetSetting": {
                                "dataList": {
                                  'category': "category2",
                                  'product': [
                                    "product7",
                                    "product8",
                                    "product9",
                                  ],
                                  'image': [
                                    "image1",
                                    "image2",
                                    "image3",
                                  ],
                                },
                                "useBackgroundCategoryTitle": "true",
                                "imageId":
                                    "imageBgCategoryTitle", //key imageBgCategoryTitle
                                "colorCategoryTitle": "0xFF121212",
                                "colorProductName": "0xFF121212",
                                "colorPrice": "0xFF8d1111",
                                "colorDescription": "0xFF121212",
                              }
                            },
                          ]
                        }
                      }
                    }
                  }
                }
              },

              // Cột B
              {
                "key": "expanded-B",
                "widgetName": "Expanded",
                "widgetSetting": {
                  "flex": "1",
                  "child": {
                    "key": "padding-B",
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "widgetName": "Row",
                        "widgetSetting": {
                          "children": [
                            // Cột B - cột 1
                            {
                              "key": "expanded-B1",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "children": [
                                      //Cột B - Dòng 1 - 1 hình ảnh
                                      {
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "Row-br2",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "start",
                                              "children": [
                                                {
                                                  "key": "Expanded-image4",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "image-B11",
                                                      "widgetName": "image",
                                                      "widgetSetting": {
                                                        "imageId": "image4"
                                                      },
                                                    },
                                                  },
                                                }
                                              ]
                                            }
                                          }
                                        }
                                      },
                                      //Cột B - Dòng 2 - khoảng cách
                                      {
                                        "key": "PaddingByRatio-A2",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "bottomPaddingDataKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },
                                      //Cột B - Dòng 3 - Danh mục 3
                                      {
                                        "key": "CTSC-B13",
                                        "widgetName":
                                            "categoryTitleSingleColumn",
                                        "widgetSetting": {
                                          "dataList": {
                                            "category": "category3",
                                            "product": [
                                              "product10",
                                              "product11",
                                              "product12",
                                            ],
                                          },
                                          "useBackgroundCategoryTitle": "true",
                                          // "pathImageBgCategoryTitle":
                                          //     "/temp_restaurant_4/decorate/BG-title-2.png",
                                          "imageId": "imageBgCategoryTitle",
                                          "colorCategoryTitle": "0xFF121212",
                                          "colorProductName": "0xFF121212",
                                          "colorPrice": "0xFF8d1111",
                                          "colorDescription": "0xFF121212",
                                        }
                                      },
                                      //Cột B - Dòng 4 - khoảng cách
                                      {
                                        "key": "PaddingByRatio-A2",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "bottomPaddingDataKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },

                                      //Cột B - Dòng 5 - 1 hình ảnh
                                      {
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "Row-br2",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "crossAxisAlignment": "start",
                                              "children": [
                                                {
                                                  "key": "Expanded-image4",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "image-B15",
                                                      "widgetName": "image",
                                                      "widgetSetting": {
                                                        "imageId": "image5"
                                                      },
                                                    },
                                                  },
                                                }
                                              ]
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            },
                            // //Cột B - Cột 2 -khoảng cách
                            {
                              "key": "SizedBox-B2",
                              "widgetName": "SizedBox",
                              "widgetSetting": {"width": "3.w"}
                            },
                            {
                              "key": "expanded-B3",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "flex": "1",
                                "child": {
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "start",
                                    "children": [
                                      //Cột B - Cột 3 - Dòng 1
                                      {
                                        "key": "expanded-B31",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "CTSC-B311",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category4",
                                                "product": [
                                                  "product13",
                                                  "product14",
                                                  "product15",
                                                ],
                                              },
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              // "pathImageBgCategoryTitle":
                                              //     "/temp_restaurant_4/decorate/BG-title-2.png",
                                              "imageId": "imageBgCategoryTitle",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorProductName": "0xFF121212",
                                              "colorPrice": "0xFF8d1111",
                                              "colorDescription": "0xFF121212",
                                            }
                                          }
                                        }
                                      },
                                      //Cột B - Cột 3 - Dòng 2
                                      {
                                        "key": "PaddingByRatio-A2",
                                        "widgetName": "PaddingByRatio",
                                        "widgetSetting": {
                                          "bottomPaddingDataKey":
                                              "marginBottomCategoryTitle"
                                        }
                                      },
                                      //Cột B - Cột 3 - Dòng 3
                                      {
                                        "key": "expanded-B33",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "flex": "1",
                                          "child": {
                                            "key": "CTSC-B331",
                                            "widgetName":
                                                "categoryTitleSingleColumn",
                                            "widgetSetting": {
                                              "dataList": {
                                                "category": "category5",
                                                "product": [
                                                  "product16",
                                                  "product17",
                                                  "product18",
                                                ],
                                              },
                                              "useBackgroundCategoryTitle":
                                                  "true",
                                              // "pathImageBgCategoryTitle":
                                              //     "/temp_restaurant_4/decorate/BG-title-2.png",
                                              "imageId": "imageBgCategoryTitle",
                                              "colorCategoryTitle":
                                                  "0xFF121212",
                                              "colorProductName": "0xFF121212",
                                              "colorPrice": "0xFF8d1111",
                                              "colorDescription": "0xFF121212",
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              }
            ]
          }
        }
      }
    }
  };
}
