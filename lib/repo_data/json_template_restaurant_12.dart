class JsonDataTemplate {
  // [112315TIN] double check: oke
  // [112314TIN] run ok, supported ratio: 4:3, 16:10, 16:9, 21:9
  static final temprestaurant12 = {
    "_id": "",
    "templateSlug": "temp_restaurant_12",
    "ratioSetting": {
      // not use textStyle for this template
      // "textStyle": {},
      "renderScreen": {
        "32/9": {
          "menuVerticalPadding": "12.sp",
          "menuHorizontalPadding": "24.sp",
          "categoryNameFontSize": "5.sp",
          "menuTitleFontSize": "14.sp",
          "menuSubTitleFontSize": "5.5.sp",
          "brandFontSize": "6.sp",
          "productFontSize": "3.2.sp",
          "categoryColumnHorizontalPadding": "12.sp",
          "imageProductGap": "4.sp",
          "categoryTopMargin": "10.sp",
          "categoryProductGap": "3.sp",
          "productGap": "2.sp",
        },
        "21/9": {
          "menuVerticalPadding": "12.sp",
          "menuHorizontalPadding": "24.sp",
          "categoryNameFontSize": "5.sp",
          "menuTitleFontSize": "14.sp",
          "menuSubTitleFontSize": "5.5.sp",
          "brandFontSize": "6.sp",
          "productFontSize": "3.2.sp",
          "categoryColumnHorizontalPadding": "12.sp",
          "imageProductGap": "4.sp",
          "categoryTopMargin": "10.sp",
          "categoryProductGap": "3.sp",
          "productGap": "2.sp",
        },
        "16/9": {
          "menuVerticalPadding": "15.sp",
          "menuHorizontalPadding": "24.sp",
          "categoryNameFontSize": "6.sp",
          "menuTitleFontSize": "18.sp",
          "menuSubTitleFontSize": "8.sp",
          "brandFontSize": "8.sp",
          "productFontSize": "4.6.sp",
          "categoryColumnHorizontalPadding": "12.sp",
          "imageProductGap": "6.sp",
          "categoryTopMargin": "10.sp",
          "categoryProductGap": "5.sp",
          "productGap": "3.sp",
        },
        "16/10": {
          "menuVerticalPadding": "15.sp",
          "menuHorizontalPadding": "24.sp",
          "categoryNameFontSize": "6.sp",
          "menuTitleFontSize": "18.sp",
          "menuSubTitleFontSize": "8.sp",
          "brandFontSize": "8.sp",
          "productFontSize": "4.6.sp",
          "categoryColumnHorizontalPadding": "12.sp",
          "imageProductGap": "6.sp",
          "categoryTopMargin": "10.sp",
          "categoryProductGap": "5.sp",
          "productGap": "3.2.sp",
        },
        "4/3": {
          "menuVerticalPadding": "20.sp",
          "menuHorizontalPadding": "18.sp",
          "categoryNameFontSize": "8.sp",
          "menuTitleFontSize": "22.sp",
          "menuSubTitleFontSize": "8.sp",
          "brandFontSize": "10.sp",
          "productFontSize": "5.sp",
          "categoryColumnHorizontalPadding": "12.sp",
          "imageProductGap": "6.sp",
          "categoryTopMargin": "14.sp",
          "categoryProductGap": "5.sp",
          "productGap": "5.sp",
        },
        "default": {
          "menuVerticalPadding": "15.sp",
          "menuHorizontalPadding": "24.sp",
          "categoryNameFontSize": "6.sp",
          "menuTitleFontSize": "18.sp",
          "menuSubTitleFontSize": "8.sp",
          "brandFontSize": "8.sp",
          "productFontSize": "4.6.sp",
          "categoryColumnHorizontalPadding": "12.sp",
          "imageProductGap": "6.sp",
          "categoryTopMargin": "10.sp",
          "categoryProductGap": "5.sp",
          "productGap": "3.sp",
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "CATEGORY 1",
        "image": "",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "CATEGORY 2",
        "image": "",
      },
      "category3": {
        "key": "category3",
        "type": "category",
        "name": "CATEGORY 3",
        "image": "",
      },
      //Danh mục 1: pro 1, 2, 3, 4, 5
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },
      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$40.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$50.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Danh mục 2: pro 6, 7
      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$60.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$70.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Danh mục 3: pro 8, 9, 10, 11, 12
      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product11": {
        "key": "product11",
        "type": "product",
        "name": "Product 11",
        "price": "\$90.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      "product12": {
        "key": "product12",
        "type": "product",
        "name": "Product 12",
        "price": "\$80.0",
        "salePrice": "",
        "image": "",
        "description": "",
      },

      //Hình background
      "imageBackground": {
        "key": "imageBackground",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/screen_2x-1.webp",
      },

      // Hình deco 1
      "decoImage1": {
        "key": "decoImage1",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/deco-7.png",
      },

      // Hình deco 2
      "decoImage2": {
        "key": "decoImage2",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/fb0c8438-817c-4516-b388-7970eae3d395.png",
      },

      // Hình deco 3
      "decoImage3": {
        "key": "decoImage3",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/aee54b79-253e-4775-9972-14ed1d651513.png",
      },

      // Hình deco 4
      "decoImage4": {
        "key": "decoImage4",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/6ef33fb1-a5d8-41f4-abbb-98614e406e83.png",
      },

      // Hình deco 5
      "decoImage5": {
        "key": "decoImage5",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/deco-3.png",
      },

      // Hình deco 6
      "decoImage6": {
        "key": "decoImage6",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/c766c918-7a99-4951-a34b-836c62db3c69.png",
      },

      // Hình deco 7
      "decoImage7": {
        "key": "decoImage7",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/74e3f3e5-9c2f-4c0f-90bd-d5ce7f1ee864.png",
      },

      // Hình deco 8
      "decoImage8": {
        "key": "decoImage8",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/deco-1.png",
      },

      // Hình deco 9
      "decoImage9": {
        "key": "decoImage9",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/353a5356-218d-4d38-8a02-6f25233c8846.png",
      },

      // Hình deco 10
      "decoImage10": {
        "key": "decoImage10",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/deco-2.png",
      },

      // Hình deco 11
      "decoImage11": {
        "key": "decoImage11",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/ab6da27a-9c81-4172-a0d2-40cda9700d26-1.png",
      },

      // Hình deco 12
      "decoImage12": {
        "key": "decoImage12",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/deco-6.png",
      },

      // Hình deco 13
      "decoImage13": {
        "key": "decoImage13",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/bd3ba66e-2e97-46fe-8774-c3fb6f7b88f9.png",
      },

      // Hình deco 14
      "decoImage14": {
        "key": "decoImage14",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/deco-5.png",
      },

      // Hình deco 15
      "decoImage15": {
        "key": "decoImage15",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/deco-4.png",
      },

      // Hình deco 16
      "decoImage16": {
        "key": "decoImage16",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/5c76f393-8dab-4e96-8f3a-31604131e91b.png",
      },

      // Hình deco 17
      "decoImage17": {
        "key": "decoImage17",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/c5984f26-bfc1-41eb-85c3-004069cf7f10.png",
      },

      // Hình deco 18
      "decoImage18": {
        "key": "decoImage18",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/ab6da27a-9c81-4172-a0d2-40cda9700d26-1.png",
      },

      // category image 1
      "categoryImage1": {
        "key": "categoryImage1",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_7-1.webp",
      },

      // category image 2
      "categoryImage2": {
        "key": "categoryImage2",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_5-1.webp",
      },

      // category image 3
      "categoryImage3": {
        "key": "categoryImage3",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/product_4-1.png",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgetSetting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image":
              "imageBackground", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
          "boxFit": "fill",
        },
        "width": "100.w",
        "child": {
          "key": "",
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              // Bottom right deco
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "right": "-10.sp",
                  "bottom": "-8.sp",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "height": "25.sp",
                      "width": "25.sp",
                      "child": {
                        "key": "",
                        "widgetName": "SimpleImage",
                        "widgetSetting": {
                          "imageDataKey": "decoImage17",
                          "fit": "contain",
                        }
                      }
                    }
                  }
                }
              },
              // Middle left deco
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "left": "0",
                  "top": "30.h",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "height": "32.sp",
                      "width": "14.sp",
                      "child": {
                        "key": "",
                        "widgetName": "Stack",
                        "widgetSetting": {
                          "children": [
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "0",
                                "left": "0",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "1.2",
                                    "origin": {
                                      "dx": "4.sp",
                                      "dy": "4.sp",
                                    },
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "height": "20.sp",
                                        "width": "30.sp",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage1",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  },
                                }
                              }
                            },
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "bottom": "0",
                                "right": "0",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "1.2",
                                    "origin": {
                                      "dx": "11.sp",
                                      "dy": "4.sp",
                                    },
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "height": "8.sp",
                                        "width": "15.sp",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage10",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  }
                }
              },
              // Top center deco
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "right": "30.w",
                  "top": "0",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "height": "14.sp",
                      "width": "45.sp",
                      "child": {
                        "key": "",
                        "widgetName": "Stack",
                        "widgetSetting": {
                          "children": [
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "-6.sp",
                                "right": "0",
                                "child": {
                                  "key": "",
                                  "widgetName": "SizedBoxByRatio",
                                  "widgetSetting": {
                                    "height": "20.sp",
                                    "width": "32.sp",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SimpleImage",
                                      "widgetSetting": {
                                        "imageDataKey": "decoImage6",
                                        "fit": "contain",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "-4.sp",
                                "left": "0",
                                "child": {
                                  "key": "",
                                  "widgetName": "SizedBoxByRatio",
                                  "widgetSetting": {
                                    "height": "14.sp",
                                    "width": "36.sp",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SimpleImage",
                                      "widgetSetting": {
                                        "imageDataKey": "decoImage5",
                                        "fit": "contain",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  }
                }
              },
              // Top left deco
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "left": "0",
                  "top": "0",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "height": "28.sp",
                      "width": "35.w",
                      "child": {
                        "key": "",
                        "widgetName": "Stack",
                        "widgetSetting": {
                          "children": [
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "2.sp",
                                "left": "-10.sp",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "-0.5",
                                    "origin": {
                                      "dx": "11.sp",
                                      "dy": "4.sp",
                                    },
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "height": "20.sp",
                                        "width": "36.sp",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage2",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "-10.sp",
                                "left": "6.sp",
                                "child": {
                                  "key": "",
                                  "widgetName": "SizedBoxByRatio",
                                  "widgetSetting": {
                                    "height": "20.sp",
                                    "width": "20.sp",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SimpleImage",
                                      "widgetSetting": {
                                        "imageDataKey": "decoImage3",
                                        "fit": "contain",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "-2.sp",
                                "right": "0",
                                "child": {
                                  "key": "",
                                  "widgetName": "SizedBoxByRatio",
                                  "widgetSetting": {
                                    "height": "20.sp",
                                    "width": "20.sp",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SimpleImage",
                                      "widgetSetting": {
                                        "imageDataKey": "decoImage4",
                                        "fit": "contain",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "5.sp",
                                "left": "0",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "1.2",
                                    "origin": {
                                      "dx": "2.sp",
                                      "dy": "2.sp",
                                    },
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "height": "12.sp",
                                        "width": "18.sp",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage1",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  },
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  }
                }
              },

              // Top right deco
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "right": "0",
                  "top": "0",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "height": "25.h",
                      "width": "20.sp",
                      "child": {
                        "key": "",
                        "widgetName": "Stack",
                        "widgetSetting": {
                          "children": [
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "-10.sp",
                                "left": "0",
                                "child": {
                                  "key": "",
                                  "widgetName": "SizedBoxByRatio",
                                  "widgetSetting": {
                                    "height": "30.sp",
                                    "width": "30.sp",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SimpleImage",
                                      "widgetSetting": {
                                        "imageDataKey": "decoImage7",
                                        "fit": "contain",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "top": "-12.sp",
                                "left": "20.sp",
                                "child": {
                                  "key": "",
                                  "widgetName": "TransformRotate",
                                  "widgetSetting": {
                                    "angle": "1.5",
                                    "origin": {
                                      "dx": "2.sp",
                                      "dy": "2.sp",
                                    },
                                    "child": {
                                      "key": "",
                                      "widgetName": "SizedBoxByRatio",
                                      "widgetSetting": {
                                        "height": "16.sp",
                                        "width": "28.sp",
                                        "child": {
                                          "key": "",
                                          "widgetName": "SimpleImage",
                                          "widgetSetting": {
                                            "imageDataKey": "decoImage8",
                                            "fit": "contain",
                                          }
                                        }
                                      }
                                    }
                                  },
                                }
                              }
                            },
                            {
                              "key": "",
                              "widgetName": "Positioned",
                              "widgetSetting": {
                                "bottom": "0",
                                "left": "2.sp",
                                "child": {
                                  "key": "",
                                  "widgetName": "SizedBoxByRatio",
                                  "widgetSetting": {
                                    "height": "36.sp",
                                    "width": "10.sp",
                                    "child": {
                                      "key": "",
                                      "widgetName": "SimpleImage",
                                      "widgetSetting": {
                                        "imageDataKey": "decoImage9",
                                        "fit": "contain",
                                      }
                                    }
                                  }
                                }
                              }
                            },
                          ],
                        }
                      }
                    }
                  }
                }
              },
              {
                // Menu
                "key": "",
                "widgetName": "PaddingByRatio",
                "widgetSetting": {
                  "topPaddingFieldKey": "menuVerticalPadding",
                  "horizontalPaddingFieldKey": "menuHorizontalPadding",
                  "child": {
                    "key": "",
                    "widgetName": "Container",
                    "widgetSetting": {
                      "child": {
                        "key": "",
                        "widgetName": "Row",
                        "widgetSetting": {
                          "children": [
                            // Column 1
                            {
                              "key": "",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "key": "",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "key": "",
                                        "widgetName": "Row",
                                        "widgetSetting": {
                                          "children": [
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "widthFromFieldKey":
                                                    "categoryColumnHorizontalPadding"
                                              }
                                            },
                                            // Category column 1
                                            {
                                              "key": "",
                                              "widgetName": "Expanded",
                                              "widgetSetting": {
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Column",
                                                  "widgetSetting": {
                                                    "children": [
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "categoryTopMargin",
                                                        }
                                                      },
                                                      // Category title 1
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "Container",
                                                        "widgetSetting": {
                                                          "padding": {
                                                            "top": "1.h",
                                                            "bottom": "0.4.h",
                                                          },
                                                          "width": "maxFinite",
                                                          "color": "0xffffffff",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "Text",
                                                            "widgetSetting": {
                                                              "dataFromSetting":
                                                                  "category1.name",
                                                              "style": {
                                                                "fontWeight":
                                                                    "w800",
                                                                "fontFamily":
                                                                    "League Spartan",
                                                                "color":
                                                                    "0xffff6d00",
                                                              },
                                                              "fontSizeFromFieldKey":
                                                                  "categoryNameFontSize",
                                                            }
                                                          }
                                                        },
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "categoryProductGap",
                                                        }
                                                      },
                                                      // Product 1
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product": "product1",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // More spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "height": "1.5.sp"
                                                        }
                                                      },
                                                      // Product 2
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product": "product2",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // More spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "height": "1.5.sp"
                                                        }
                                                      },
                                                      // Product 3
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product": "product3",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // More spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "height": "1.5.sp"
                                                        }
                                                      },
                                                      // Product 4
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product": "product4",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // More spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "height": "1.5.sp"
                                                        }
                                                      },
                                                      // Product 5
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product": "product5",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                    ]
                                                  }
                                                }
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "widthFromFieldKey":
                                                    "categoryColumnHorizontalPadding"
                                              }
                                            }
                                          ]
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "imageProductGap"
                                        }
                                      },

                                      // Category image 1 section
                                      {
                                        "key": "",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "",
                                            "widgetName": "Stack",
                                            "widgetSetting": {
                                              "alignment": "center",
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "height": "70.sp",
                                                    "width": "70.sp",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Stack",
                                                      "widgetSetting": {
                                                        "alignment": "center",
                                                        "children": [
                                                          {
                                                            "key": "",
                                                            "widgetName":
                                                                "Positioned",
                                                            "widgetSetting": {
                                                              "top": "0",
                                                              "left": "10.sp",
                                                              "child": {
                                                                "key": "",
                                                                "widgetName":
                                                                    "SizedBoxByRatio",
                                                                "widgetSetting":
                                                                    {
                                                                  "height":
                                                                      "15.sp",
                                                                  "width":
                                                                      "15.sp",
                                                                  "child": {
                                                                    "key": "",
                                                                    "widgetName":
                                                                        "SimpleImage",
                                                                    "widgetSetting":
                                                                        {
                                                                      "imageDataKey":
                                                                          "decoImage12",
                                                                      "fit":
                                                                          "contain",
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          },
                                                          {
                                                            "key": "",
                                                            "widgetName":
                                                                "SimpleImage",
                                                            "widgetSetting": {
                                                              "imageDataKey":
                                                                  "decoImage13",
                                                              "fit": "contain",
                                                            }
                                                          }
                                                        ]
                                                      }
                                                    }
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "height": "45.sp",
                                                    "width": "45.sp",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "SimpleImage",
                                                      "widgetSetting": {
                                                        "imageDataKey":
                                                            "categoryImage1",
                                                        "fit": "contain",
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "imageProductGap"
                                        }
                                      },
                                    ],
                                  }
                                }
                              }
                            },
                            // Column 2
                            {
                              "key": "",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "key": "",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "mainAxisAlignment": "spaceBetween",
                                    "children": [
                                      // Custome text section
                                      {
                                        "key": "",
                                        "widgetName": "Container",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "",
                                            "widgetName": "Column",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Row",
                                                  "widgetSetting": {
                                                    "children": [
                                                      // Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "Expanded",
                                                        "widgetSetting": {
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "SizedBox",
                                                            "widgetSetting": {
                                                              "width":
                                                                  "maxFinite",
                                                              "child": {
                                                                "key": "",
                                                                "widgetName":
                                                                    "Divider",
                                                                "widgetSetting":
                                                                    {
                                                                  "color":
                                                                      "0xffff6d00",
                                                                  "thickness":
                                                                      "1.sp",
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "4.sp",
                                                        }
                                                      },
                                                      {
                                                        "key": "",
                                                        "widgetName": "Text",
                                                        "widgetSetting": {
                                                          "data": "BRAND",
                                                          "style": {
                                                            "color":
                                                                "0xffffffff",
                                                            "fontWeight":
                                                                "w700",
                                                            "fontFamily":
                                                                "League Spartan",
                                                          },
                                                          "fontSizeFromFieldKey":
                                                              "brandFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "4.sp",
                                                        }
                                                      },
                                                      // Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "Expanded",
                                                        "widgetSetting": {
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "SizedBox",
                                                            "widgetSetting": {
                                                              "width":
                                                                  "maxFinite",
                                                              "child": {
                                                                "key": "",
                                                                "widgetName":
                                                                    "Divider",
                                                                "widgetSetting":
                                                                    {
                                                                  "color":
                                                                      "0xffff6d00",
                                                                  "thickness":
                                                                      "1.sp",
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      },
                                                    ]
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName": "Text",
                                                  "widgetSetting": {
                                                    "data": "SPECIAL",
                                                    "style": {
                                                      "color": "0xffffffff",
                                                      "fontWeight": "w800",
                                                      "fontFamily": "Barlow",
                                                    },
                                                    "fontSizeFromFieldKey":
                                                        "menuTitleFontSize",
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName": "Text",
                                                  "widgetSetting": {
                                                    "data": "RESTAURANT MENU",
                                                    "style": {
                                                      "color": "0xffffffff",
                                                      "fontWeight": "w800",
                                                      "fontFamily": "Barlow",
                                                    },
                                                    "fontSizeFromFieldKey":
                                                        "menuSubTitleFontSize",
                                                  }
                                                },
                                                // Divider
                                                {
                                                  "key": "",
                                                  "widgetName": "SizedBox",
                                                  "widgetSetting": {
                                                    "height": "4.sp",
                                                  }
                                                },
                                                // Divider
                                                {
                                                  "key": "",
                                                  "widgetName": "SizedBox",
                                                  "widgetSetting": {
                                                    "width": "maxFinite",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Divider",
                                                      "widgetSetting": {
                                                        "color": "0xffff6d00",
                                                        "thickness": "1.sp",
                                                      }
                                                    }
                                                  },
                                                },
                                              ]
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "imageProductGap"
                                        }
                                      },
                                      // Category image 2 section
                                      {
                                        "key": "",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "",
                                            "widgetName": "Stack",
                                            "widgetSetting": {
                                              "alignment": "center",
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "height": "70.sp",
                                                    "width": "70.sp",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Stack",
                                                      "widgetSetting": {
                                                        "alignment": "center",
                                                        "children": [
                                                          {
                                                            "key": "",
                                                            "widgetName":
                                                                "Positioned",
                                                            "widgetSetting": {
                                                              "bottom": "0",
                                                              "left": "10.sp",
                                                              "child": {
                                                                "key": "",
                                                                "widgetName":
                                                                    "SizedBoxByRatio",
                                                                "widgetSetting":
                                                                    {
                                                                  "height":
                                                                      "20.sp",
                                                                  "wid30":
                                                                      "25.sp",
                                                                  "child": {
                                                                    "key": "",
                                                                    "widgetName":
                                                                        "SimpleImage",
                                                                    "widgetSetting":
                                                                        {
                                                                      "imageDataKey":
                                                                          "decoImage7",
                                                                      "fit":
                                                                          "contain",
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          },
                                                          {
                                                            "key": "",
                                                            "widgetName":
                                                                "SimpleImage",
                                                            "widgetSetting": {
                                                              "imageDataKey":
                                                                  "decoImage18",
                                                              "fit": "contain",
                                                            }
                                                          }
                                                        ]
                                                      }
                                                    }
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "height": "45.sp",
                                                    "width": "45.sp",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "SimpleImage",
                                                      "widgetSetting": {
                                                        "imageDataKey":
                                                            "categoryImage2",
                                                        "fit": "contain",
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "imageProductGap"
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "Row",
                                        "widgetSetting": {
                                          "children": [
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "widthFromFieldKey":
                                                    "categoryColumnHorizontalPadding"
                                              }
                                            },
                                            // Category column 2
                                            {
                                              "key": "",
                                              "widgetName": "Expanded",
                                              "widgetSetting": {
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Column",
                                                  "widgetSetting": {
                                                    "children": [
                                                      // Category title 2
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "Container",
                                                        "widgetSetting": {
                                                          "padding": {
                                                            "top": "1.h",
                                                            "bottom": "0.4.h",
                                                          },
                                                          "width": "maxFinite",
                                                          "color": "0xffff6d00",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "Text",
                                                            "widgetSetting": {
                                                              "dataFromSetting":
                                                                  "category2.name",
                                                              "style": {
                                                                "fontWeight":
                                                                    "w800",
                                                                "fontFamily":
                                                                    "League Spartan",
                                                                "color":
                                                                    "0xffffffff",
                                                              },
                                                              "fontSizeFromFieldKey":
                                                                  "categoryNameFontSize",
                                                            }
                                                          }
                                                        },
                                                      },

                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "categoryProductGap",
                                                        }
                                                      },
                                                      // Product 6
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product": "product6",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // More spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "height": "1.5.sp"
                                                        }
                                                      },
                                                      // Product 7
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product": "product7",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // More spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "height": "1.5.sp"
                                                        }
                                                      },
                                                    ]
                                                  }
                                                },
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "widthFromFieldKey":
                                                    "categoryColumnHorizontalPadding"
                                              }
                                            },
                                          ]
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "height": "12.sp",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Stack",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Positioned",
                                                  "widgetSetting": {
                                                    "top": "-1.sp",
                                                    "left": "-25.sp",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "TransformRotate",
                                                      "widgetSetting": {
                                                        "angle": "-1.44",
                                                        "origin": {
                                                          "dx": "20.sp",
                                                          "dy": "0",
                                                        },
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "SizedBoxByRatio",
                                                          "widgetSetting": {
                                                            "height": "70.sp",
                                                            "width": "20.sp",
                                                            "child": {
                                                              "key": "",
                                                              "widgetName":
                                                                  "SimpleImage",
                                                              "widgetSetting": {
                                                                "imageDataKey":
                                                                    "decoImage15",
                                                                "fit":
                                                                    "contain",
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  },
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName": "Positioned",
                                                  "widgetSetting": {
                                                    "top": "0",
                                                    "left": "0",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "SizedBoxByRatio",
                                                      "widgetSetting": {
                                                        "height": "8.sp",
                                                        "width": "15.sp",
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "SimpleImage",
                                                          "widgetSetting": {
                                                            "imageDataKey":
                                                                "decoImage14",
                                                            "fit": "contain",
                                                          }
                                                        }
                                                      }
                                                    }
                                                  },
                                                },
                                              ]
                                            }
                                          }
                                        }
                                      }
                                    ],
                                  }
                                }
                              }
                            },
                            // Column 3
                            {
                              "key": "",
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "key": "",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "key": "",
                                        "widgetName": "Row",
                                        "widgetSetting": {
                                          "children": [
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "widthFromFieldKey":
                                                    "categoryColumnHorizontalPadding"
                                              }
                                            },
                                            // Category column 3
                                            {
                                              "key": "",
                                              "widgetName": "Expanded",
                                              "widgetSetting": {
                                                "child": {
                                                  "key": "",
                                                  "widgetName": "Column",
                                                  "widgetSetting": {
                                                    "children": [
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "categoryTopMargin",
                                                        }
                                                      },
                                                      // Category title 3
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "Container",
                                                        "widgetSetting": {
                                                          "padding": {
                                                            "top": "1.h",
                                                            "bottom": "0.4.h",
                                                          },
                                                          "width": "maxFinite",
                                                          "color": "0xffffffff",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "Text",
                                                            "widgetSetting": {
                                                              "dataFromSetting":
                                                                  "category3.name",
                                                              "style": {
                                                                "fontWeight":
                                                                    "w800",
                                                                "fontFamily":
                                                                    "League Spartan",
                                                                "color":
                                                                    "0xffff6d00",
                                                              },
                                                              "fontSizeFromFieldKey":
                                                                  "categoryNameFontSize",
                                                            }
                                                          }
                                                        },
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "categoryProductGap",
                                                        }
                                                      },
                                                      // Product 8
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product": "product8",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // More spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "height": "1.5.sp"
                                                        }
                                                      },
                                                      // Product 9
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product": "product9",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // More spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "height": "1.5.sp"
                                                        }
                                                      },
                                                      // Product 10
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product":
                                                              "product10",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // More spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "height": "1.5.sp"
                                                        }
                                                      },
                                                      // Product 11
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product":
                                                              "product11",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // More spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "height": "1.5.sp"
                                                        }
                                                      },
                                                      // Product 12
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "ProductRow1",
                                                        "widgetSetting": {
                                                          "product":
                                                              "product12",
                                                          "border": {
                                                            "all": {
                                                              "width": "0",
                                                              "color":
                                                                  "0x00000000"
                                                            }
                                                          },
                                                          "productPriceTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffff6d00"
                                                          },
                                                          "productNameTextStyle":
                                                              {
                                                            "fontFamily":
                                                                "League Spartan",
                                                            "fontWeight":
                                                                "w700",
                                                            "color":
                                                                "0xffffffff"
                                                          },
                                                          "prefixProductPrice":
                                                              "",
                                                          "upperCaseProductName":
                                                              "true",
                                                          "productNameFontSizeFromFieldKey":
                                                              "productFontSize",
                                                          "productPriceFontSizeFromFieldKey":
                                                              "productFontSize",
                                                        }
                                                      },
                                                      // Spacing
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "heightFromFieldKey":
                                                              "productGap"
                                                        }
                                                      },
                                                      // Dashed Divider
                                                      {
                                                        "key": "",
                                                        "widgetName":
                                                            "SizedBoxByRatio",
                                                        "widgetSetting": {
                                                          "width": "maxFinite",
                                                          "child": {
                                                            "key": "",
                                                            "widgetName":
                                                                "DashedDivider",
                                                            "widgetSetting": {
                                                              "color":
                                                                  "0xffffffff",
                                                              "thickness":
                                                                  "0.3.h",
                                                              "dashWidth":
                                                                  "0.5.h",
                                                              "dashSpace":
                                                                  "0.4.h",
                                                            }
                                                          }
                                                        }
                                                      },
                                                    ]
                                                  }
                                                }
                                              }
                                            },
                                            // Spacing
                                            {
                                              "key": "",
                                              "widgetName": "SizedBoxByRatio",
                                              "widgetSetting": {
                                                "widthFromFieldKey":
                                                    "categoryColumnHorizontalPadding"
                                              }
                                            }
                                          ]
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "imageProductGap"
                                        }
                                      },

                                      // Category image 3 section
                                      {
                                        "key": "",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {
                                          "child": {
                                            "key": "",
                                            "widgetName": "Stack",
                                            "widgetSetting": {
                                              "alignment": "center",
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "height": "70.sp",
                                                    "width": "70.sp",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Stack",
                                                      "widgetSetting": {
                                                        "alignment": "center",
                                                        "children": [
                                                          {
                                                            "key": "",
                                                            "widgetName":
                                                                "Positioned",
                                                            "widgetSetting": {
                                                              "top": "10.sp",
                                                              "left": "0",
                                                              "child": {
                                                                "key": "",
                                                                "widgetName":
                                                                    "SizedBoxByRatio",
                                                                "widgetSetting":
                                                                    {
                                                                  "height":
                                                                      "15.sp",
                                                                  "width":
                                                                      "15.sp",
                                                                  "child": {
                                                                    "key": "",
                                                                    "widgetName":
                                                                        "SimpleImage",
                                                                    "widgetSetting":
                                                                        {
                                                                      "imageDataKey":
                                                                          "decoImage1",
                                                                      "fit":
                                                                          "contain",
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          },
                                                          {
                                                            "key": "",
                                                            "widgetName":
                                                                "SimpleImage",
                                                            "widgetSetting": {
                                                              "imageDataKey":
                                                                  "decoImage16",
                                                              "fit": "contain",
                                                            }
                                                          },
                                                          {
                                                            "key": "",
                                                            "widgetName":
                                                                "Positioned",
                                                            "widgetSetting": {
                                                              "right": "4.sp",
                                                              "bottom": "10.sp",
                                                              "child": {
                                                                "key": "",
                                                                "widgetName":
                                                                    "TransformRotate",
                                                                "widgetSetting":
                                                                    {
                                                                  "angle":
                                                                      "1.7",
                                                                  "origin": {
                                                                    "dx":
                                                                        "10.sp",
                                                                    "dy":
                                                                        "2.sp",
                                                                  },
                                                                  "child": {
                                                                    "key": "",
                                                                    "widgetName":
                                                                        "SizedBoxByRatio",
                                                                    "widgetSetting":
                                                                        {
                                                                      "height":
                                                                          "15.sp",
                                                                      "width":
                                                                          "15.sp",
                                                                      "child": {
                                                                        "key":
                                                                            "",
                                                                        "widgetName":
                                                                            "SimpleImage",
                                                                        "widgetSetting":
                                                                            {
                                                                          "imageDataKey":
                                                                              "decoImage1",
                                                                          "fit":
                                                                              "contain",
                                                                        }
                                                                      }
                                                                    }
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          },
                                                        ]
                                                      }
                                                    }
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "height": "45.sp",
                                                    "width": "45.sp",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "SimpleImage",
                                                      "widgetSetting": {
                                                        "imageDataKey":
                                                            "categoryImage3",
                                                        "fit": "contain",
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          }
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "imageProductGap"
                                        }
                                      },
                                    ],
                                  }
                                }
                              }
                            },
                          ]
                        }
                      }
                    }
                  }
                }
              },
            ]
          }
        }
      }
    }
  };
}
