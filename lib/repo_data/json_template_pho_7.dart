class JsonDataTemplate {
  // [112317TIN] Run OK, supported ratio: 4:3, 16:10, 16:9, 21:9
  static final temppho7 = {
    "_id": "id_tem18",
    "templateSlug": "temp_pho_7",
    "ratioSetting": {
      // not use textStyle for this template
      // "textStyle": {},
      "renderScreen": {
        "32/9": {
          "menuVerticalPadding": "8.sp",
          "menuHorizontalPadding": "18.sp",
          "categoryNameFontSize": "4.sp",
          "productGap": "1.sp",
          "categoryProductGap": "3.5.sp",
          "columnGap": "22.sp",
          "categoryImageHorizontalMargin": "2.sp",
          "categoryTitleHorizontalPadding": "0.5.sp",
          "categoryTitleWidth": "60.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "1.8.sp",
          "productPriceChipHorizontalPadding": "0",
          "productPriceChipVerticalPadding": "1.sp",
          "productRowHeight": "11.sp",
          "productPriceChipSize": "6.sp",
          "title1FontSize": "5.sp",
          "title2FontSize": "11.sp",
          "title3FontSize": "8.sp",
          "titleGap": "2.sp",
          "extraInfo1FontSize": "4.sp",
          "extraInfo2FontSize": "4.6.sp",
          "extraInfo3FontSize": "3.sp",
          "extraInfoGap": "1.sp",
          "categoryImageSize": "50.sp",
          "decoSize": "28.w",
          "title1Width": "70.sp",
        },
        "21/9": {
          "menuVerticalPadding": "8.sp",
          "menuHorizontalPadding": "18.sp",
          "categoryNameFontSize": "4.sp",
          "productGap": "1.sp",
          "categoryProductGap": "3.5.sp",
          "columnGap": "22.sp",
          "categoryImageHorizontalMargin": "2.sp",
          "categoryTitleHorizontalPadding": "0.5.sp",
          "categoryTitleWidth": "60.sp",
          "productNameFontSize": "3.sp",
          "productPriceFontSize": "2.8.sp",
          "productDesFontSize": "1.8.sp",
          "productPriceChipHorizontalPadding": "0",
          "productPriceChipVerticalPadding": "1.sp",
          "productPriceChip2VerticalPadding": "2.sp",
          "productRowHeight": "11.sp",
          "productPriceChipSize": "6.sp",
          "productPriceChip2Size": "8.sp",
          "title1FontSize": "5.sp",
          "title2FontSize": "11.sp",
          "title3FontSize": "8.sp",
          "titleGap": "2.sp",
          "extraInfo1FontSize": "4.sp",
          "extraInfo2FontSize": "4.6.sp",
          "extraInfo3FontSize": "3.sp",
          "extraInfoGap": "1.sp",
          "categoryImageSize": "50.sp",
          "decoSize": "28.w",
          "title1Width": "70.sp",
        },
        "16/9": {
          "menuVerticalPadding": "11.sp",
          "menuHorizontalPadding": "15.sp",
          "categoryNameFontSize": "5.sp",
          "productGap": "2.5.sp",
          "categoryProductGap": "4.5.sp",
          "columnGap": "22.sp",
          "categoryImageHorizontalMargin": "5.sp",
          "categoryTitleHorizontalPadding": "0.5.sp",
          "categoryTitleWidth": "60.sp",
          "productNameFontSize": "3.6.sp",
          "productPriceFontSize": "3.2.sp",
          "productDesFontSize": "2.sp",
          "productPriceChipHorizontalPadding": "0",
          "productPriceChipVerticalPadding": "1.sp",
          "productPriceChip2VerticalPadding": "2.5.sp",
          "productRowHeight": "12.sp",
          "productPriceChipSize": "7.sp",
          "productPriceChip2Size": "10.sp",
          "title1FontSize": "6.sp",
          "title2FontSize": "12.sp",
          "title3FontSize": "10.sp",
          "titleGap": "2.sp",
          "extraInfo1FontSize": "5.sp",
          "extraInfo2FontSize": "5.6.sp",
          "extraInfo3FontSize": "4.sp",
          "extraInfoGap": "2.sp",
          "categoryImageSize": "70.sp",
          "decoSize": "33.w",
          "title1Width": "70.sp",
        },
        "16/10": {
          "menuVerticalPadding": "16.sp",
          "menuHorizontalPadding": "15.sp",
          "categoryNameFontSize": "5.4.sp",
          "productGap": "2.2.sp",
          "categoryProductGap": "4.5.sp",
          "columnGap": "18.sp",
          "categoryImageHorizontalMargin": "2.6.sp",
          "categoryTitleHorizontalPadding": "0.5.sp",
          "categoryTitleWidth": "65.sp",
          "productNameFontSize": "3.8.sp",
          "productPriceFontSize": "3.5.sp",
          "productDesFontSize": "2.4.sp",
          "productPriceChipHorizontalPadding": "0",
          "productPriceChipVerticalPadding": "1.2.sp",
          "productPriceChip2VerticalPadding": "3.sp",
          "productRowHeight": "14.sp",
          "productPriceChipSize": "8.sp",
          "productPriceChip2Size": "12.sp",
          "title1FontSize": "7.sp",
          "title2FontSize": "12.sp",
          "title3FontSize": "10.sp",
          "titleGap": "2.sp",
          "extraInfo1FontSize": "5.sp",
          "extraInfo2FontSize": "5.6.sp",
          "extraInfo3FontSize": "4.sp",
          "extraInfoGap": "2.sp",
          "categoryImageSize": "76.sp",
          "decoSize": "33.w",
          "title1Width": "80.sp",
        },
        "4/3": {
          "menuVerticalPadding": "22.sp",
          "menuHorizontalPadding": "15.sp",
          "categoryNameFontSize": "5.4.sp",
          "productGap": "2.sp",
          "categoryProductGap": "4.5.sp",
          "columnGap": "8.sp",
          "categoryImageHorizontalMargin": "2.6.sp",
          "categoryTitleHorizontalPadding": "0.5.sp",
          "categoryTitleWidth": "65.sp",
          "productNameFontSize": "5.4.sp",
          "productPriceFontSize": "4.8.sp",
          "productDesFontSize": "3.2.sp",
          "productPriceChipHorizontalPadding": "0",
          "productPriceChipVerticalPadding": "2.4.sp",
          "productPriceChip2VerticalPadding": "4.2.sp",
          "productRowHeight": "20.sp",
          "productPriceChipSize": "12.sp",
          "productPriceChip2Size": "16.sp",
          "title1FontSize": "9.sp",
          "title2FontSize": "15.sp",
          "title3FontSize": "12.8.sp",
          "titleGap": "4.sp",
          "extraInfo1FontSize": "7.sp",
          "extraInfo2FontSize": "7.6.sp",
          "extraInfo3FontSize": "6.sp",
          "extraInfoGap": "2.sp",
          "categoryImageSize": "78.sp",
          "decoSize": "33.w",
          "title1Width": "80.sp",
        },
        "default": {
          "menuVerticalPadding": "14.sp",
          "menuHorizontalPadding": "15.sp",
          "categoryNameFontSize": "6.sp",
          "productFontSize": "4.4.sp",
          "titleFontSize": "16.sp",
          "subTitleFontSize": "8.sp",
          "normalFontSize": "4.sp",
          "productGap": "2.5.sp",
          "categoryProductGap": "4.5.sp",
          "rowGap": "10.8.sp",
          "columnGap": "22.sp",
          "bottomLeftSectionHeight": "60.sp",
          "bottomLeftSectionWidth": "90.sp",
          "bottomLeftImageSize": "60.sp",
          "decoSize1": "15.sp",
          "decoSize2": "10.sp",
          "topRightSectionHeight": "72.sp",
          "topRightSectionWidth": "80.sp",
          "topRightImageSize": "70.sp",
          "category1TopMargin": "4.sp",
          "dividerTopMargin": "0",
          "dividerBottomMargin": "2.sp",
        },
      },
    },

    //isDeleted: Check có bị xóa hay chưa
    //Nếu status bị delete thì ko show ra nữa
    //Chuyển vào thùng rác, không phải xóa hoàn toàn
    "isDeleted": false,

    //"dataSetting" là thông số tùy chỉnh
    "dataSetting": {
      "category1": {
        "key": "category1",
        "type": "category",
        "name": "CATEGORY 1",
        "image": "",
      },
      "category2": {
        "key": "category2",
        "type": "category",
        "name": "CATEGORY 2",
        "image": "",
      },

      //Danh mục 1: pro 1, 2 ,3, 4
      "product1": {
        "key": "product1",
        "type": "product",
        "name": "Product 1",
        "price": "\$11",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product2": {
        "key": "product2",
        "type": "product",
        "name": "Product 2",
        "price": "\$12",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product3": {
        "key": "product3",
        "type": "product",
        "name": "Product 3",
        "price": "\$13",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product4": {
        "key": "product4",
        "type": "product",
        "name": "Product 4",
        "price": "\$40",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      //Danh mục 2: pro 5, 6, 7, 8
      "product5": {
        "key": "product5",
        "type": "product",
        "name": "Product 5",
        "price": "\$50",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product6": {
        "key": "product6",
        "type": "product",
        "name": "Product 6",
        "price": "\$60",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product7": {
        "key": "product7",
        "type": "product",
        "name": "Product 7",
        "price": "\$70",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      "product8": {
        "key": "product8",
        "type": "product",
        "name": "Product 8",
        "price": "\$80",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      // Product on top left image
      "product9": {
        "key": "product9",
        "type": "product",
        "name": "Product 9",
        "price": "\$90",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      // Product on bottom right image
      "product10": {
        "key": "product10",
        "type": "product",
        "name": "Product 10",
        "price": "\$10",
        "salePrice": "",
        "image": "",
        "description":
            "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolore, eligendi!",
      },

      //Hình background
      "imageBackground": {
        "key": "imageBackground",
        "type": "image",
        "image": "https://foodiemenu.co/wp-content/uploads/2023/11/bg-1-1.webp",
      },

      //Hình category 1
      "category1Image": {
        "key": "category1Image",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/cat_1_3-1.webp",
      },

      //Hình category 2
      "category2Image": {
        "key": "category2Image",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/cat_2_3.webp",
      },

      //Hình category 3
      "category3Image": {
        "key": "category3Image",
        "type": "image",
        "image":
            "https://foodiemenu.co/wp-content/uploads/2023/11/cat_3_3.webp",
      },
    },

    //"widgets_setting" lấy từ class TemplateControllerState()
    //Chứa thông số mặc định
    "widgetSetting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {
          "image":
              "imageBackground", //Nếu lấy data từ key thì ghi "image", còn url thì "imageUrl"
          "boxFit": "fill",
        },
        "width": "100.w",
        "child": {
          "key": "",
          "widgetName": "Stack",
          "widgetSetting": {
            "children": [
              // Deco top left
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "top": "-10.w",
                  "left": "-10.w",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "heightFromFieldKey": "decoSize",
                      "widthFromFieldKey": "decoSize",
                      "child": {
                        "key": "",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "decoration": {
                            "borderRadius": "30.w",
                            "color": "0xffc33123"
                          }
                        }
                      }
                    }
                  }
                }
              },
              // Deco buttom right
              {
                "key": "",
                "widgetName": "Positioned",
                "widgetSetting": {
                  "bottom": "-10.w",
                  "right": "-10.w",
                  "child": {
                    "key": "",
                    "widgetName": "SizedBoxByRatio",
                    "widgetSetting": {
                      "heightFromFieldKey": "decoSize",
                      "widthFromFieldKey": "decoSize",
                      "child": {
                        "key": "",
                        "widgetName": "Container",
                        "widgetSetting": {
                          "decoration": {
                            "borderRadius": "30.w",
                            "color": "0xffc33123"
                          }
                        }
                      }
                    }
                  }
                }
              },
              {
                "key": "",
                "widgetName": "PaddingByRatio",
                "widgetSetting": {
                  "horizontalPaddingFieldKey": "menuHorizontalPadding",
                  "verticalPaddingFieldKey": "menuVerticalPadding",
                  "child": {
                    "key": "",
                    "widgetName": "Row",
                    "widgetSetting": {
                      "children": [
                        // Column 1
                        {
                          "key": "",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "key": "",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "child": {
                                  "key": "",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "categoryImageSize",
                                          "widthFromFieldKey":
                                              "categoryImageSize",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Stack",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "clipBehavior": "hardEdge",
                                                    "width": "maxFinite",
                                                    "height": "maxFinite",
                                                    "decoration": {
                                                      "borderRadius": "4.sp",
                                                    },
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "SimpleImage",
                                                      "widgetSetting": {
                                                        "imageDataKey":
                                                            "category1Image",
                                                        "fit": "cover",
                                                      }
                                                    }
                                                  }
                                                },
                                                // Product price of product9
                                                {
                                                  "key": "",
                                                  "widgetName": "Positioned",
                                                  "widgetSetting": {
                                                    "bottom": "5.h",
                                                    "right": "5.h",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "SizedBoxByRatio",
                                                      "widgetSetting": {
                                                        "heightFromFieldKey":
                                                            "productPriceChip2Size",
                                                        "widthFromFieldKey":
                                                            "productPriceChip2Size",
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "Container",
                                                          "widgetSetting": {
                                                            "clipBehavior":
                                                                "hardEdge",
                                                            "decoration": {
                                                              "color":
                                                                  "0xff191919",
                                                              "borderRadius":
                                                                  "10.sp",
                                                            },
                                                            "child": {
                                                              "key": "",
                                                              "widgetName":
                                                                  "PaddingByRatio",
                                                              "widgetSetting": {
                                                                "horizontalPaddingFieldKey":
                                                                    "productPriceChipHorizontalPadding",
                                                                "verticalPaddingFieldKey":
                                                                    "productPriceChip2VerticalPadding",
                                                                "child": {
                                                                  "key": "",
                                                                  "widgetName":
                                                                      "Text",
                                                                  "widgetSetting":
                                                                      {
                                                                    "dataFromSetting":
                                                                        "product9.price",
                                                                    "style": {
                                                                      "fontFamily":
                                                                          "Poppins",
                                                                      "fontWeight":
                                                                          "w500",
                                                                      "color":
                                                                          "0xffffffff"
                                                                    },
                                                                    "fontSizeFromFieldKey":
                                                                        "productPriceFontSize"
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    },
                                                  }
                                                }
                                              ]
                                            }
                                          }
                                        }
                                      },
                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "categoryImageHorizontalMargin",
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "widthFromFieldKey":
                                              "categoryTitleWidth",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "clipBehavior": "hardEdge",
                                              "decoration": {
                                                "color": "0xffc33123",
                                                "borderRadius": "10.sp"
                                              },
                                              "child": {
                                                "key": "",
                                                "widgetName": "PaddingByRatio",
                                                "widgetSetting": {
                                                  "topPaddingFieldKey":
                                                      "categoryTitleHorizontalPadding",
                                                  "bottomPaddingFieldKey":
                                                      "categoryTitleHorizontalPadding",
                                                  "child": {
                                                    "key": "",
                                                    "widgetName": "Text",
                                                    "widgetSetting": {
                                                      "dataFromSetting":
                                                          "category1.name",
                                                      "style": {
                                                        "fontFamily": "Poppins",
                                                        "color": "0xffffffff",
                                                        "fontWeight": "w500",
                                                        "letterSpacing":
                                                            "0.8.sp"
                                                      },
                                                      "fontSizeFromFieldKey":
                                                          "categoryNameFontSize",
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      },
                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "categoryProductGap",
                                        }
                                      },

                                      // Product 1
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "productRowHeight",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "ProductRow2",
                                                      "widgetSetting": {
                                                        "product": "product1",
                                                        "productNameTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w600",
                                                          "color": "0xffc33123",
                                                        },
                                                        "productDescriptionTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w400",
                                                          "color": "0xff202020",
                                                        },
                                                        // hide product price at here
                                                        "productPriceTextStyle":
                                                            {
                                                          "color": "0x00000000",
                                                          "fontSize": "0",
                                                        },
                                                        "productNameFontSizeFromFieldKey":
                                                            "productNameFontSize",
                                                        "productDesFontSizeFromFieldKey":
                                                            "productDesFontSize",
                                                      },
                                                    },
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "widthFromFieldKey":
                                                        "productNamePriceGap",
                                                    "width": "5.sp"
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productPriceChipSize",
                                                    "widthFromFieldKey":
                                                        "productPriceChipSize",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Container",
                                                      "widgetSetting": {
                                                        "clipBehavior":
                                                            "hardEdge",
                                                        "decoration": {
                                                          "color": "0xffc33123",
                                                          "borderRadius":
                                                              "10.sp",
                                                        },
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "PaddingByRatio",
                                                          "widgetSetting": {
                                                            "horizontalPaddingFieldKey":
                                                                "productPriceChipHorizontalPadding",
                                                            "verticalPaddingFieldKey":
                                                                "productPriceChipVerticalPadding",
                                                            "child": {
                                                              "key": "",
                                                              "widgetName":
                                                                  "Text",
                                                              "widgetSetting": {
                                                                "dataFromSetting":
                                                                    "product1.price",
                                                                "style": {
                                                                  "fontFamily":
                                                                      "Poppins",
                                                                  "fontWeight":
                                                                      "w500",
                                                                  "color":
                                                                      "0xffffffff"
                                                                },
                                                                "fontSizeFromFieldKey":
                                                                    "productPriceFontSize"
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          },
                                        }
                                      },

                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey": "productGap",
                                        }
                                      },

                                      // Product 2
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "productRowHeight",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "ProductRow2",
                                                      "widgetSetting": {
                                                        "product": "product2",
                                                        "productNameTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w600",
                                                          "color": "0xffc33123",
                                                        },
                                                        "productDescriptionTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w400",
                                                          "color": "0xff202020",
                                                        },
                                                        // hide product price at here
                                                        "productPriceTextStyle":
                                                            {
                                                          "color": "0x00000000",
                                                          "fontSize": "0",
                                                        },
                                                        "productNameFontSizeFromFieldKey":
                                                            "productNameFontSize",
                                                        "productDesFontSizeFromFieldKey":
                                                            "productDesFontSize",
                                                      },
                                                    },
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "widthFromFieldKey":
                                                        "productNamePriceGap",
                                                    "width": "5.sp"
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productPriceChipSize",
                                                    "widthFromFieldKey":
                                                        "productPriceChipSize",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Container",
                                                      "widgetSetting": {
                                                        "clipBehavior":
                                                            "hardEdge",
                                                        "decoration": {
                                                          "color": "0xff191919",
                                                          "borderRadius":
                                                              "10.sp",
                                                        },
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "PaddingByRatio",
                                                          "widgetSetting": {
                                                            "horizontalPaddingFieldKey":
                                                                "productPriceChipHorizontalPadding",
                                                            "verticalPaddingFieldKey":
                                                                "productPriceChipVerticalPadding",
                                                            "child": {
                                                              "key": "",
                                                              "widgetName":
                                                                  "Text",
                                                              "widgetSetting": {
                                                                "dataFromSetting":
                                                                    "product2.price",
                                                                "style": {
                                                                  "fontFamily":
                                                                      "Poppins",
                                                                  "fontWeight":
                                                                      "w500",
                                                                  "color":
                                                                      "0xffffffff"
                                                                },
                                                                "fontSizeFromFieldKey":
                                                                    "productPriceFontSize"
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          },
                                        }
                                      },

                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey": "productGap",
                                        }
                                      },

                                      // Product 3
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "productRowHeight",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "ProductRow2",
                                                      "widgetSetting": {
                                                        "product": "product3",
                                                        "productNameTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w600",
                                                          "color": "0xffc33123",
                                                        },
                                                        "productDescriptionTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w400",
                                                          "color": "0xff202020",
                                                        },
                                                        // hide product price at here
                                                        "productPriceTextStyle":
                                                            {
                                                          "color": "0x00000000",
                                                          "fontSize": "0",
                                                        },
                                                        "productNameFontSizeFromFieldKey":
                                                            "productNameFontSize",
                                                        "productDesFontSizeFromFieldKey":
                                                            "productDesFontSize",
                                                      },
                                                    },
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "widthFromFieldKey":
                                                        "productNamePriceGap",
                                                    "width": "5.sp"
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productPriceChipSize",
                                                    "widthFromFieldKey":
                                                        "productPriceChipSize",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Container",
                                                      "widgetSetting": {
                                                        "clipBehavior":
                                                            "hardEdge",
                                                        "decoration": {
                                                          "color": "0xffc33123",
                                                          "borderRadius":
                                                              "10.sp",
                                                        },
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "PaddingByRatio",
                                                          "widgetSetting": {
                                                            "horizontalPaddingFieldKey":
                                                                "productPriceChipHorizontalPadding",
                                                            "verticalPaddingFieldKey":
                                                                "productPriceChipVerticalPadding",
                                                            "child": {
                                                              "key": "",
                                                              "widgetName":
                                                                  "Text",
                                                              "widgetSetting": {
                                                                "dataFromSetting":
                                                                    "product3.price",
                                                                "style": {
                                                                  "fontFamily":
                                                                      "Poppins",
                                                                  "fontWeight":
                                                                      "w500",
                                                                  "color":
                                                                      "0xffffffff"
                                                                },
                                                                "fontSizeFromFieldKey":
                                                                    "productPriceFontSize"
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          },
                                        }
                                      },

                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey": "productGap",
                                        }
                                      },

                                      // Product 4
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "productRowHeight",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "ProductRow2",
                                                      "widgetSetting": {
                                                        "product": "product4",
                                                        "productNameTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w600",
                                                          "color": "0xffc33123",
                                                        },
                                                        "productDescriptionTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w400",
                                                          "color": "0xff202020",
                                                        },
                                                        // hide product price at here
                                                        "productPriceTextStyle":
                                                            {
                                                          "color": "0x00000000",
                                                          "fontSize": "0",
                                                        },
                                                        "productNameFontSizeFromFieldKey":
                                                            "productNameFontSize",
                                                        "productDesFontSizeFromFieldKey":
                                                            "productDesFontSize",
                                                      },
                                                    },
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "widthFromFieldKey":
                                                        "productNamePriceGap",
                                                    "width": "5.sp"
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productPriceChipSize",
                                                    "widthFromFieldKey":
                                                        "productPriceChipSize",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Container",
                                                      "widgetSetting": {
                                                        "clipBehavior":
                                                            "hardEdge",
                                                        "decoration": {
                                                          "color": "0xff191919",
                                                          "borderRadius":
                                                              "10.sp",
                                                        },
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "PaddingByRatio",
                                                          "widgetSetting": {
                                                            "horizontalPaddingFieldKey":
                                                                "productPriceChipHorizontalPadding",
                                                            "verticalPaddingFieldKey":
                                                                "productPriceChipVerticalPadding",
                                                            "child": {
                                                              "key": "",
                                                              "widgetName":
                                                                  "Text",
                                                              "widgetSetting": {
                                                                "dataFromSetting":
                                                                    "product4.price",
                                                                "style": {
                                                                  "fontFamily":
                                                                      "Poppins",
                                                                  "fontWeight":
                                                                      "w500",
                                                                  "color":
                                                                      "0xffffffff"
                                                                },
                                                                "fontSizeFromFieldKey":
                                                                    "productPriceFontSize"
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          },
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            }
                          }
                        },
                        // Spacing
                        {
                          "key": "",
                          "widgetName": "SizedBoxByRatio",
                          "widgetSetting": {
                            "widthFromFieldKey": "columnGap",
                          }
                        },
                        // Column 2
                        {
                          "key": "",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "key": "",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "child": {
                                  "key": "",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      // Title 1
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "widthFromFieldKey": "title1Width",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "clipBehavior": "hardEdge",
                                              "decoration": {
                                                "color": "0xff191919",
                                                "borderRadius": "10.sp"
                                              },
                                              "child": {
                                                "key": "",
                                                "widgetName": "PaddingByRatio",
                                                "widgetSetting": {
                                                  "topPaddingFieldKey":
                                                      "categoryTitleHorizontalPadding",
                                                  "bottomPaddingFieldKey":
                                                      "categoryTitleHorizontalPadding",
                                                  "child": {
                                                    "key": "",
                                                    "widgetName": "Text",
                                                    "widgetSetting": {
                                                      "data": "TRADITIONAL",
                                                      "style": {
                                                        "fontFamily": "Poppins",
                                                        "color": "0xffffffff",
                                                        "fontWeight": "w600",
                                                        "letterSpacing":
                                                            "0.8.sp"
                                                      },
                                                      "fontSizeFromFieldKey":
                                                          "title1FontSize",
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      },

                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey": "titleGap",
                                        }
                                      },

                                      // More spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "height": "4.sp",
                                        }
                                      },

                                      // Title 2
                                      {
                                        "key": "",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "THE BRAND",
                                          "style": {
                                            "fontFamily": "Poppins",
                                            "color": "0xffc33123",
                                            "fontWeight": "w700",
                                            "height": "1",
                                          },
                                          "fontSizeFromFieldKey":
                                              "title2FontSize",
                                        }
                                      },

                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey": "titleGap",
                                        }
                                      },

                                      // Title 3
                                      {
                                        "key": "",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "FOOD MENU",
                                          "style": {
                                            "fontFamily": "Poppins",
                                            "color": "0xff202020",
                                            "fontWeight": "w600",
                                            "height": "1",
                                          },
                                          "fontSizeFromFieldKey":
                                              "title3FontSize",
                                        }
                                      },
                                      // Fill spacing
                                      {
                                        "key": "",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {"": ""}
                                      },
                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "categoryImageHorizontalMargin",
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "categoryImageSize",
                                          "widthFromFieldKey":
                                              "categoryImageSize",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "clipBehavior": "hardEdge",
                                              "decoration": {
                                                "borderRadius": "4.sp"
                                              },
                                              "child": {
                                                "key": "",
                                                "widgetName": "SimpleImage",
                                                "widgetSetting": {
                                                  "imageDataKey":
                                                      "category2Image",
                                                  "fit": "cover"
                                                }
                                              }
                                            }
                                          }
                                        }
                                      },
                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "categoryImageHorizontalMargin",
                                        }
                                      },
                                      // Fill spacing
                                      {
                                        "key": "",
                                        "widgetName": "Expanded",
                                        "widgetSetting": {"": ""}
                                      },
                                      // Delivery
                                      {
                                        "key": "",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "Delivery Order :",
                                          "style": {
                                            "fontFamily": "Poppins",
                                            "color": "0xff191919",
                                            "fontWeight": "w600",
                                          },
                                          "fontSizeFromFieldKey":
                                              "extraInfo1FontSize",
                                        }
                                      },

                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey": "extraInfoGap",
                                        }
                                      },

                                      // Phone
                                      {
                                        "key": "",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "+ 123-456-7890",
                                          "style": {
                                            "fontFamily": "Poppins",
                                            "color": "0xffc33123",
                                            "fontWeight": "w600",
                                          },
                                          "fontSizeFromFieldKey":
                                              "extraInfo2FontSize",
                                        }
                                      },

                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey": "extraInfoGap",
                                        }
                                      },

                                      // Website
                                      {
                                        "key": "",
                                        "widgetName": "Text",
                                        "widgetSetting": {
                                          "data": "www.domain.com",
                                          "style": {
                                            "fontFamily": "Poppins",
                                            "color": "0xff000000",
                                            "fontWeight": "w600",
                                          },
                                          "fontSizeFromFieldKey":
                                              "extraInfo3FontSize",
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            }
                          }
                        },
                        // Spacing
                        {
                          "key": "",
                          "widgetName": "SizedBoxByRatio",
                          "widgetSetting": {
                            "widthFromFieldKey": "columnGap",
                          }
                        },
                        // Column 3
                        {
                          "key": "",
                          "widgetName": "Expanded",
                          "widgetSetting": {
                            "child": {
                              "key": "",
                              "widgetName": "Container",
                              "widgetSetting": {
                                "child": {
                                  "key": "",
                                  "widgetName": "Column",
                                  "widgetSetting": {
                                    "children": [
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "widthFromFieldKey":
                                              "categoryTitleWidth",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Container",
                                            "widgetSetting": {
                                              "clipBehavior": "hardEdge",
                                              "decoration": {
                                                "color": "0xffc33123",
                                                "borderRadius": "10.sp"
                                              },
                                              "child": {
                                                "key": "",
                                                "widgetName": "PaddingByRatio",
                                                "widgetSetting": {
                                                  "topPaddingFieldKey":
                                                      "categoryTitleHorizontalPadding",
                                                  "bottomPaddingFieldKey":
                                                      "categoryTitleHorizontalPadding",
                                                  "child": {
                                                    "key": "",
                                                    "widgetName": "Text",
                                                    "widgetSetting": {
                                                      "dataFromSetting":
                                                          "category2.name",
                                                      "style": {
                                                        "fontFamily": "Poppins",
                                                        "color": "0xffffffff",
                                                        "fontWeight": "w500",
                                                        "letterSpacing":
                                                            "0.8.sp"
                                                      },
                                                      "fontSizeFromFieldKey":
                                                          "categoryNameFontSize",
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      },
                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "categoryProductGap",
                                        }
                                      },
                                      // Product 5
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "productRowHeight",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "ProductRow2",
                                                      "widgetSetting": {
                                                        "product": "product5",
                                                        "productNameTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w600",
                                                          "color": "0xffc33123",
                                                        },
                                                        "productDescriptionTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w400",
                                                          "color": "0xff202020",
                                                        },
                                                        // hide product price at here
                                                        "productPriceTextStyle":
                                                            {
                                                          "color": "0x00000000",
                                                          "fontSize": "0",
                                                        },
                                                        "productNameFontSizeFromFieldKey":
                                                            "productNameFontSize",
                                                        "productDesFontSizeFromFieldKey":
                                                            "productDesFontSize",
                                                      },
                                                    },
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "widthFromFieldKey":
                                                        "productNamePriceGap",
                                                    "width": "5.sp"
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productPriceChipSize",
                                                    "widthFromFieldKey":
                                                        "productPriceChipSize",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Container",
                                                      "widgetSetting": {
                                                        "clipBehavior":
                                                            "hardEdge",
                                                        "decoration": {
                                                          "color": "0xff191919",
                                                          "borderRadius":
                                                              "10.sp",
                                                        },
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "PaddingByRatio",
                                                          "widgetSetting": {
                                                            "horizontalPaddingFieldKey":
                                                                "productPriceChipHorizontalPadding",
                                                            "verticalPaddingFieldKey":
                                                                "productPriceChipVerticalPadding",
                                                            "child": {
                                                              "key": "",
                                                              "widgetName":
                                                                  "Text",
                                                              "widgetSetting": {
                                                                "dataFromSetting":
                                                                    "product5.price",
                                                                "style": {
                                                                  "fontFamily":
                                                                      "Poppins",
                                                                  "fontWeight":
                                                                      "w500",
                                                                  "color":
                                                                      "0xffffffff"
                                                                },
                                                                "fontSizeFromFieldKey":
                                                                    "productPriceFontSize"
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          },
                                        }
                                      },
                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey": "productGap",
                                        }
                                      },

                                      // Product 6
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "productRowHeight",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "ProductRow2",
                                                      "widgetSetting": {
                                                        "product": "product6",
                                                        "productNameTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w600",
                                                          "color": "0xffc33123",
                                                        },
                                                        "productDescriptionTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w400",
                                                          "color": "0xff202020",
                                                        },
                                                        // hide product price at here
                                                        "productPriceTextStyle":
                                                            {
                                                          "color": "0x00000000",
                                                          "fontSize": "0",
                                                        },
                                                        "productNameFontSizeFromFieldKey":
                                                            "productNameFontSize",
                                                        "productDesFontSizeFromFieldKey":
                                                            "productDesFontSize",
                                                      },
                                                    },
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "widthFromFieldKey":
                                                        "productNamePriceGap",
                                                    "width": "5.sp"
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productPriceChipSize",
                                                    "widthFromFieldKey":
                                                        "productPriceChipSize",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Container",
                                                      "widgetSetting": {
                                                        "clipBehavior":
                                                            "hardEdge",
                                                        "decoration": {
                                                          "color": "0xffc33123",
                                                          "borderRadius":
                                                              "10.sp",
                                                        },
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "PaddingByRatio",
                                                          "widgetSetting": {
                                                            "horizontalPaddingFieldKey":
                                                                "productPriceChipHorizontalPadding",
                                                            "verticalPaddingFieldKey":
                                                                "productPriceChipVerticalPadding",
                                                            "child": {
                                                              "key": "",
                                                              "widgetName":
                                                                  "Text",
                                                              "widgetSetting": {
                                                                "dataFromSetting":
                                                                    "product6.price",
                                                                "style": {
                                                                  "fontFamily":
                                                                      "Poppins",
                                                                  "fontWeight":
                                                                      "w500",
                                                                  "color":
                                                                      "0xffffffff"
                                                                },
                                                                "fontSizeFromFieldKey":
                                                                    "productPriceFontSize"
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          },
                                        }
                                      },

                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey": "productGap",
                                        }
                                      },

                                      // Product 7
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "productRowHeight",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "ProductRow2",
                                                      "widgetSetting": {
                                                        "product": "product7",
                                                        "productNameTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w600",
                                                          "color": "0xffc33123",
                                                        },
                                                        "productDescriptionTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w400",
                                                          "color": "0xff202020",
                                                        },
                                                        // hide product price at here
                                                        "productPriceTextStyle":
                                                            {
                                                          "color": "0x00000000",
                                                          "fontSize": "0",
                                                        },
                                                        "productNameFontSizeFromFieldKey":
                                                            "productNameFontSize",
                                                        "productDesFontSizeFromFieldKey":
                                                            "productDesFontSize",
                                                      },
                                                    },
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "widthFromFieldKey":
                                                        "productNamePriceGap",
                                                    "width": "5.sp"
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productPriceChipSize",
                                                    "widthFromFieldKey":
                                                        "productPriceChipSize",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Container",
                                                      "widgetSetting": {
                                                        "clipBehavior":
                                                            "hardEdge",
                                                        "decoration": {
                                                          "color": "0xff191919",
                                                          "borderRadius":
                                                              "10.sp",
                                                        },
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "PaddingByRatio",
                                                          "widgetSetting": {
                                                            "horizontalPaddingFieldKey":
                                                                "productPriceChipHorizontalPadding",
                                                            "verticalPaddingFieldKey":
                                                                "productPriceChipVerticalPadding",
                                                            "child": {
                                                              "key": "",
                                                              "widgetName":
                                                                  "Text",
                                                              "widgetSetting": {
                                                                "dataFromSetting":
                                                                    "product7.price",
                                                                "style": {
                                                                  "fontFamily":
                                                                      "Poppins",
                                                                  "fontWeight":
                                                                      "w500",
                                                                  "color":
                                                                      "0xffffffff"
                                                                },
                                                                "fontSizeFromFieldKey":
                                                                    "productPriceFontSize"
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          },
                                        }
                                      },
                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey": "productGap",
                                        }
                                      },
                                      // Product 8
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "productRowHeight",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Row",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Expanded",
                                                  "widgetSetting": {
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "ProductRow2",
                                                      "widgetSetting": {
                                                        "product": "product8",
                                                        "productNameTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w600",
                                                          "color": "0xffc33123",
                                                        },
                                                        "productDescriptionTextStyle":
                                                            {
                                                          "fontFamily":
                                                              "Poppins",
                                                          "fontWeight": "w400",
                                                          "color": "0xff202020",
                                                        },
                                                        // hide product price at here
                                                        "productPriceTextStyle":
                                                            {
                                                          "color": "0x00000000",
                                                          "fontSize": "0",
                                                        },
                                                        "productNameFontSizeFromFieldKey":
                                                            "productNameFontSize",
                                                        "productDesFontSizeFromFieldKey":
                                                            "productDesFontSize",
                                                      },
                                                    },
                                                  }
                                                },
                                                // Spacing
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "widthFromFieldKey":
                                                        "productNamePriceGap",
                                                    "width": "5.sp"
                                                  }
                                                },
                                                {
                                                  "key": "",
                                                  "widgetName":
                                                      "SizedBoxByRatio",
                                                  "widgetSetting": {
                                                    "heightFromFieldKey":
                                                        "productPriceChipSize",
                                                    "widthFromFieldKey":
                                                        "productPriceChipSize",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName": "Container",
                                                      "widgetSetting": {
                                                        "clipBehavior":
                                                            "hardEdge",
                                                        "decoration": {
                                                          "color": "0xffc33123",
                                                          "borderRadius":
                                                              "10.sp",
                                                        },
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "PaddingByRatio",
                                                          "widgetSetting": {
                                                            "horizontalPaddingFieldKey":
                                                                "productPriceChipHorizontalPadding",
                                                            "verticalPaddingFieldKey":
                                                                "productPriceChipVerticalPadding",
                                                            "child": {
                                                              "key": "",
                                                              "widgetName":
                                                                  "Text",
                                                              "widgetSetting": {
                                                                "dataFromSetting":
                                                                    "product8.price",
                                                                "style": {
                                                                  "fontFamily":
                                                                      "Poppins",
                                                                  "fontWeight":
                                                                      "w500",
                                                                  "color":
                                                                      "0xffffffff"
                                                                },
                                                                "fontSizeFromFieldKey":
                                                                    "productPriceFontSize"
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    }
                                                  }
                                                },
                                              ]
                                            }
                                          },
                                        }
                                      },

                                      // Spacing
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "categoryImageHorizontalMargin",
                                        }
                                      },
                                      {
                                        "key": "",
                                        "widgetName": "SizedBoxByRatio",
                                        "widgetSetting": {
                                          "heightFromFieldKey":
                                              "categoryImageSize",
                                          "widthFromFieldKey":
                                              "categoryImageSize",
                                          "child": {
                                            "key": "",
                                            "widgetName": "Stack",
                                            "widgetSetting": {
                                              "children": [
                                                {
                                                  "key": "",
                                                  "widgetName": "Container",
                                                  "widgetSetting": {
                                                    "width": "maxFinite",
                                                    "height": "maxFinite",
                                                    "clipBehavior": "hardEdge",
                                                    "decoration": {
                                                      "borderRadius": "4.sp"
                                                    },
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "SimpleImage",
                                                      "widgetSetting": {
                                                        "imageDataKey":
                                                            "category3Image",
                                                        "fit": "cover"
                                                      }
                                                    }
                                                  }
                                                },
                                                // Product price of product10
                                                {
                                                  "key": "",
                                                  "widgetName": "Positioned",
                                                  "widgetSetting": {
                                                    "bottom": "5.h",
                                                    "left": "5.h",
                                                    "child": {
                                                      "key": "",
                                                      "widgetName":
                                                          "SizedBoxByRatio",
                                                      "widgetSetting": {
                                                        "heightFromFieldKey":
                                                            "productPriceChip2Size",
                                                        "widthFromFieldKey":
                                                            "productPriceChip2Size",
                                                        "child": {
                                                          "key": "",
                                                          "widgetName":
                                                              "Container",
                                                          "widgetSetting": {
                                                            "clipBehavior":
                                                                "hardEdge",
                                                            "decoration": {
                                                              "color":
                                                                  "0xff191919",
                                                              "borderRadius":
                                                                  "10.sp",
                                                            },
                                                            "child": {
                                                              "key": "",
                                                              "widgetName":
                                                                  "PaddingByRatio",
                                                              "widgetSetting": {
                                                                "horizontalPaddingFieldKey":
                                                                    "productPriceChipHorizontalPadding",
                                                                "verticalPaddingFieldKey":
                                                                    "productPriceChip2VerticalPadding",
                                                                "child": {
                                                                  "key": "",
                                                                  "widgetName":
                                                                      "Text",
                                                                  "widgetSetting":
                                                                      {
                                                                    "dataFromSetting":
                                                                        "product10.price",
                                                                    "style": {
                                                                      "fontFamily":
                                                                          "Poppins",
                                                                      "fontWeight":
                                                                          "w500",
                                                                      "color":
                                                                          "0xffffffff"
                                                                    },
                                                                    "fontSizeFromFieldKey":
                                                                        "productPriceFontSize"
                                                                  }
                                                                }
                                                              }
                                                            }
                                                          }
                                                        }
                                                      }
                                                    },
                                                  }
                                                }
                                              ]
                                            }
                                          }
                                        }
                                      },
                                    ]
                                  }
                                }
                              }
                            }
                          }
                        },
                      ]
                    }
                  }
                }
              }
            ],
          }
        }
      }
    }
  };
}
