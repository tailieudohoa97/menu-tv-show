import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/views/main_screen.dart';

import '../cubit/screen/screen_cubit.dart';
import '../cubit/screen/screen_state.dart';

class BuildScreenLayout extends StatelessWidget {
  final String templateId;
  final String userId;
  final String listProductId;
  const BuildScreenLayout(
      {super.key,
      required this.templateId,
      required this.userId,
      required this.listProductId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ScreenCubit>(
        create: (context) => ScreenCubit(
            listProductId: listProductId,
            templateId: templateId,
            userId: userId),
        child: _cubitProdList());
  }

  Widget _cubitProdList() {
    //define variable list view to store product

    return BlocBuilder<ScreenCubit, ScreenState>(
      builder: (context, state) {
        if (state is LoadingState) {
          // check state if state is LoadingState return loading progress
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is ErrorState) {
          // check state if state is ErrorState return error text
          return const Text("Error occurred while load product");
        } else if (state is LoadedState) {
          // check state if state is LoadedState get data from state
          final listProds = state.screen;
          // map list product to list widget to send to layout

          // return layout with modified data
          return screenLayout(templateId);
        } else {
          return Container();
        }
      },
    );
  }
}
