import 'package:flutter/material.dart'; //File nào cũng phải có

import 'package:flutter_bloc/flutter_bloc.dart';

// List<Template> prods = RepoRequest().fetchProds({}) as List<Template>;
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/bloc/templates/templates_bloc.dart';
import 'package:tv_menu/bloc/templates/templates_event.dart';
import 'package:tv_menu/bloc/templates/templates_state.dart';
import 'package:tv_menu/common/common.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/repo/repository/template_repository.dart';
import '../../../../data/enum.dart';
import '../../../../helper/textstyle_global.dart';

//[102312HG] Makeup - Add New Template Popup
class BuildAddNewTemplateLayout extends StatefulWidget {
  final Function onCreateTemplate;
  const BuildAddNewTemplateLayout({super.key, required this.onCreateTemplate});

  @override
  State<BuildAddNewTemplateLayout> createState() =>
      _BuildAddNewTemplateLayoutState();
}

class _BuildAddNewTemplateLayoutState extends State<BuildAddNewTemplateLayout> {
  TextEditingController nameTemplate = TextEditingController();
  TextEditingController slugTemplate = TextEditingController();
  TextEditingController imageTemplate = TextEditingController();
  TextEditingController categoryId = TextEditingController();
  TextEditingController ratioSetting = TextEditingController();
  TextEditingController subscriptionLevel = TextEditingController();
  TextEditingController ratios = TextEditingController();
  TextEditingController widgetSetting = TextEditingController();
  TextEditingController dataSetting = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return buildAddTemplateLayout();
  }

  Widget buildAddTemplateLayout() {
    return BlocBuilder<TemplateBloc, TemplateState>(
      builder: (context, state) {
        return CreateTemplateDialog(
          nameTemplate: nameTemplate,
          slugTemplate: slugTemplate,
          categoryId: categoryId,
          subscriptionLevel: subscriptionLevel,
          dataSetting: dataSetting,
          ratioSetting: ratioSetting,
          ratios: ratios,
          widgetSetting: widgetSetting,
          onCreateTemplate: () {
            widget.onCreateTemplate(Template(
                id: "new id",
                templateName: nameTemplate.text,
                templateSlug: slugTemplate.text,
                categoryId: categoryId.text,
                dataSetting: dataSetting.text,
                ratioSetting: ratioSetting.text,
                widgetSetting: widgetSetting.text,
                isDeleted: false,
                subscriptionLevel: subscriptionLevel.text,
                ratios: ratios.text));
          },
          title: StringManager.addNewTemplate,
        );
      },
    );
  }
}
