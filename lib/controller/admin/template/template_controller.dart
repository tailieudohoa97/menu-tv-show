import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/bloc/templates/templates_bloc.dart';
import 'package:tv_menu/bloc/templates/templates_event.dart';
import 'package:tv_menu/bloc/templates/templates_state.dart';

import 'package:tv_menu/repo/repository/template_repository.dart';
import 'package:tv_menu/views/admin/template_management/template_management_page.dart';

class TemplatesController extends StatefulWidget {
  const TemplatesController({Key? key}) : super(key: key);

  @override
  State<TemplatesController> createState() => _TemplatesControllerState();
}

class _TemplatesControllerState extends State<TemplatesController> {
  // [092301TREE]
  @override
  Widget build(BuildContext context) {
    return _buildListTemplates();
  }

  Widget _buildListTemplates() {
    return BlocBuilder<TemplateBloc, TemplateState>(
      builder: (context, state) {
        switch (state.status) {
          // if status is initial then show loading
          case TemplateStatus.initial:
            return const Center(child: CircularProgressIndicator());
          // if status is failure then show error text
          case TemplateStatus.failure:
            return Center(
              child: Text(state.error),
            );
          // if status is success then get templates
          case TemplateStatus.success:
            // this variable is used to store bloc templates state
            dynamic listTemplate = state.templates;

            // props through Templates widget
            if (state.isLoading) {
              return const Center(child: CircularProgressIndicator());
            }
            return TemplatePage(
              onCreateTemplate: (template) {
                BlocProvider.of<TemplateBloc>(context)
                    .add(AddTemplateEvent(template: template));
              },
              templates: listTemplate,
              onUpdateTemplate: (template) {
                BlocProvider.of<TemplateBloc>(context)
                    .add(UpdateTemplateEvent(template: template));
              },
              onDeleteTemplate: (id) {
                BlocProvider.of<TemplateBloc>(context)
                    .add(DeleteTemplateEvent(templateId: id));
              },
              onChangeStatusTemplate: (id, value) {
                BlocProvider.of<TemplateBloc>(context).add(
                    ChangeStatusTemplateEvent(templateId: id, status: value));
              },
            );
          default:
            return Container();
        }
      },
    );
  }
}
