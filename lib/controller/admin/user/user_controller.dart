import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:tv_menu/bloc/users/users_bloc.dart';
import 'package:tv_menu/bloc/users/users_event.dart';
import 'package:tv_menu/bloc/users/users_state.dart';

import 'package:tv_menu/repo/repository/user_repository.dart';
import 'package:tv_menu/views/admin/user_management/user_management_page.dart';

class UsersController extends StatefulWidget {
  const UsersController({Key? key}) : super(key: key);

  @override
  State<UsersController> createState() => _UsersControllerState();
}

class _UsersControllerState extends State<UsersController> {
  // [092301TREE]
  @override
  Widget build(BuildContext context) {
    return _buildListUsers();
  }

  Widget _buildListUsers() {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        switch (state.status) {
          // if status is initial then show loading
          case UserStatus.initial:
            return const Center(child: CircularProgressIndicator());
          // if status is failure then show error text
          case UserStatus.failure:
            return Center(
              child: Text(state.error),
            );
          // if status is success then get users
          case UserStatus.success:
            // this variable is used to store bloc users state
            dynamic listUser = state.users;

            // props through Users widget
            if (state.isLoading) {
              return const Center(child: CircularProgressIndicator());
            }
            return UserPage(
              onCreateUser: (user) {
                BlocProvider.of<UserBloc>(context)
                    .add(AddUserEvent(user: user));
              },
              users: listUser,
              onEditUser: (user) {
                BlocProvider.of<UserBloc>(context)
                    .add(UpdateUserEvent(user: user));
              },
              onDeleteUser: (id) {
                BlocProvider.of<UserBloc>(context)
                    .add(DeleteUserEvent(userId: id));
              },
            );
          default:
            return Container();
        }
      },
    );
  }
}
