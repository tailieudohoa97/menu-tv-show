import 'package:flutter/material.dart'; //File nào cũng phải có

import 'package:flutter_bloc/flutter_bloc.dart';

// List<User> prods = RepoRequest().fetchProds({}) as List<User>;
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/bloc/templates/templates_bloc.dart';
import 'package:tv_menu/bloc/templates/templates_event.dart';
import 'package:tv_menu/bloc/templates/templates_state.dart';
import 'package:tv_menu/bloc/users/users_bloc.dart';
import 'package:tv_menu/bloc/users/users_state.dart';
import 'package:tv_menu/common/common.dart';

import 'package:tv_menu/repo/repository/template_repository.dart';
import '../../../../data/enum.dart';
import '../../../../helper/textstyle_global.dart';

class BuildAddNewUserLayout extends StatelessWidget {
  final Function onCreateUser;
  BuildAddNewUserLayout({super.key, required this.onCreateUser});

  TextEditingController usernameController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  TextEditingController subscriptionLevelController = TextEditingController();

  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return buildAddUserLayout();
  }

  Widget buildAddUserLayout() {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        return CreateUserDialog(
            userName: usernameController,
            password: passwordController,
            email: emailController,
            subscriptionLevel: subscriptionLevelController,
            onCreateUser: (user) => onCreateUser(user),
            title: StringManager.addNewUser);
      },
    );
  }
}
