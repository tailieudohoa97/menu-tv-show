import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:tv_menu/bloc/user_template/user_template_bloc.dart';
import 'package:tv_menu/bloc/user_template/user_template_event.dart';
import 'package:tv_menu/bloc/user_template/user_template_state.dart';

import 'package:tv_menu/views/admin/template_management/template_management_page.dart';
import 'package:tv_menu/views/admin/user_template_management/user_template_management_page.dart';

class UserTemplatesController extends StatefulWidget {
  const UserTemplatesController({Key? key}) : super(key: key);

  @override
  State<UserTemplatesController> createState() =>
      _UserTemplatesControllerState();
}

class _UserTemplatesControllerState extends State<UserTemplatesController> {
  // [092301TREE]
  @override
  Widget build(BuildContext context) {
    return _buildListUserTemplates();
  }

  Widget _buildListUserTemplates() {
    return BlocBuilder<UserTemplateBloc, UserTemplateState>(
      builder: (context, state) {
        switch (state.status) {
          // if status is initial then show loading
          case UserTemplateStatus.initial:
            return const Center(child: CircularProgressIndicator());
          // if status is failure then show error text
          case UserTemplateStatus.failure:
            return Center(
              child: Text(state.error),
            );
          // if status is success then get UserTemplates
          case UserTemplateStatus.success:
            // this variable is used to store bloc UserTemplates state
            dynamic listUserTemplate = state.UserTemplates;

            // props through UserTemplates widget
            if (state.isLoading) {
              return const Center(child: CircularProgressIndicator());
            }
            return UserTemplatePage(
              onCreateUserTemplate: (userTemplate) {
                BlocProvider.of<UserTemplateBloc>(context)
                    .add(AddUserTemplateEvent(UserTemplate: userTemplate));
              },
              templates: listUserTemplate,
              onUpdateUserTemplate: (userTemplate) {
                BlocProvider.of<UserTemplateBloc>(context)
                    .add(UpdateUserTemplateEvent(UserTemplate: userTemplate));
              },
              onDeleteUserTemplate: (id) {
                BlocProvider.of<UserTemplateBloc>(context)
                    .add(DeleteUserTemplateEvent(UserTemplateId: id));
              },
            );
          default:
            return Container();
        }
      },
    );
  }
}
