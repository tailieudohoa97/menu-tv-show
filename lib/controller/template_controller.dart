// ignore_for_file: public_member_api_docs, sort_constructors_first
// import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
// import 'package:tv_menu/repo_data/data_sample_from_old_branch.dart';
import 'package:tv_menu/helper/calulate_ratio_temp.dart';
import 'package:tv_menu/helper/extension.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
// import 'package:tv_menu/repo_data/json_template_restaurant_6.dart';
import 'package:tv_menu/repo_data/export_new_template.dart';
import 'package:tv_menu/repo_data/new/export_temp.dart';
import 'package:tv_menu/services/template_service.dart';
// import 'package:collection/collection.dart';

// class TemplateController extends StatefulWidget {
//   const TemplateController({super.key});

//   @override
//   _TemplateControllerState createState() => _TemplateControllerState();
// }

class TemplateControllerState extends StatefulWidget {
  final String idTemplate;
  final String userId;
  TemplateControllerState({
    Key? key,
    required this.idTemplate,
    required this.userId,
  }) : super(key: key);

  @override
  State<TemplateControllerState> createState() =>
      _TemplateControllerStateState();
}

class _TemplateControllerStateState extends State<TemplateControllerState> {
  // Danh sách dữ liệu từ tệp JSON
  TemplateService templateService = TemplateService();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // GlobalSetting.dataSetting = jsonData.containsKey('dataSetting')
    //     ? Map.fromIterable(jsonData['dataSetting'],
    //         key: (item) => item["key"].toString(), value: (item) => item)
    //     : {};
    if (widget.idTemplate.isInt) {
      return FutureBuilder<Map<String, dynamic>>(
        future: getDataTemplate(
            int.parse(
              widget.idTemplate,
            ),
            // TODO: Replace with Token Object
            int.parse(widget.userId)),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data!["widgetSetting"] != null) {
              //092314LH Fix dynamic sample data to template
              Map<String, dynamic> radioSetting =
                  snapshot.data!.containsKey('ratioSetting')
                      ? snapshot.data!['ratioSetting']
                      : {};
              // initState();
              GlobalSetting.ratioDataSetting =
                  radioSetting.containsKey('renderScreen')
                      ? radioSetting['renderScreen']
                      : {};
              GlobalSetting.ratioStyle = CalculateRatioTemplate(
                  context, radioSetting['textStyle'] ?? {});
              //092314LH Set dataSetting from json data
              GlobalSetting.dataSetting =
                  snapshot.data!.containsKey('dataSetting')
                      ? snapshot.data!['dataSetting']
                      : {};
              GlobalSetting.dynamicDataSetting = {};
              return Sizer(builder: (context, orientation, deviceType) {
                return ModuleHelper.renderWidget(
                    snapshot.data!.containsKey("widgetSetting")
                        ? snapshot.data!["widgetSetting"]
                        : null);
              });
            } else {
              return Center(
                child: Text('ID Template is not avaliable!!!'),
              );
            }
          } else if (snapshot.hasError) {
            return Center(child: Text('${snapshot.error}'));
          }

          // By default, show a loading spinner.
          return Center(child: const CircularProgressIndicator());
        },
      );
    } else {
      return Center(
        child: Text('you are wrong ID Template!!!'),
      );
    }
  }

  Future<Map<String, dynamic>> getDataTemplate(
      int idTemplate, int userId) async {
    // [112321TIN] test template
    // return JsonDataTemplate.pho_5_json;

    // [112308TIN] test new template

    // return NewTemplates.pho_10_json;
    // return NewTemplates.restaurant_13_json;
    // final result = switch (idTemplate) {
    //   1 => JsonDataTemplate.temprestaurant1,
    //   2 => JsonDataTemplate.temprestaurant2,
    //   3 => JsonDataTemplate.temprestaurant3,
    //   4 => JsonDataTemplate.temprestaurant4,
    //   5 => JsonDataTemplate.temprestaurant5,
    //   6 => JsonDataTemplate.templateNail1,
    //   7 => JsonDataTemplate.templateNail2,
    //   8 => JsonDataTemplate.templateNail3,
    //   9 => JsonDataTemplate.templateNail4,
    //   10 => JsonDataTemplate.templateNail5,
    //   11 => JsonDataTemplate.temppho1,
    //   12 => JsonDataTemplate.temppho2,
    //   13 => JsonDataTemplate.temppho3,
    //   14 => JsonDataTemplate.temppho4,
    //   15 => JsonDataTemplate.temppho5,
    //   _ => JsonDataTemplate.temprestaurant1,
    // };
    // final result = JsonDataTemplate.templateNail4;
    // print('field, ${result.keys}');
    // result['ratioSetting'] = (result['ratioSetting'] ??
    //     result['RatioSetting'] ??
    //     Map<String, dynamic>());

    // result['widgetSetting'] = (result['widgetSetting'] ??
    //     result['widgets_setting'] ??
    //     Map<String, dynamic>());

    // return result;

    final result = await templateService.getTemplateData(idTemplate);
    if (result.isNotEmpty) {
      if (result.containsKey('dataSetting')) {
        final getUserSetting = await templateService.getUserSettingTemplate(
          idTemplate.toString(),
        );
        if (getUserSetting.isNotEmpty) {
          result['dataSetting'] = getUserSetting['userData'];
        }
      }
      return result;
    }
    return result;
  }
}
