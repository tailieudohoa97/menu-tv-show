// import 'package:flutter/material.dart';
// import 'package:sizer/sizer.dart';
// import 'package:tv_menu/helper/calulate_ratio_temp.dart';
// import 'package:tv_menu/helper/global_setting.dart';
// import 'package:tv_menu/helper/module_helper.dart';

// // import 'package:tv_menu/repo_data/json_temp_pho_5.dart';
// // import 'package:tv_menu/repo_data/json_template_restaurant_4.dart';
// import 'package:tv_menu/repo_data/sample_json_template.dart';

// class TemplateControllerState extends StatelessWidget {
//   final String idTemplate;
//   TemplateControllerState({
//     Key? key,
//     required this.idTemplate,
//   }) : super(key: key);
//   Map<String, dynamic> jsonData = {};

//   // [092326HG]
//   // Định nghĩa ánh xạ giữa idTemplate và jsonData
//   static final Map<String, Map<String, dynamic>> templateMap = {
//     '1': JsonDataTemplate.temprestaurant1,
//     '2': JsonDataTemplate.temprestaurant2,
//     '3': JsonDataTemplate.temprestaurant3,
//     '4': JsonDataTemplate.temprestaurant4,
//     '5': JsonDataTemplate.temprestaurant5,
//     '6': JsonDataTemplate.temppho1,
//     '7': JsonDataTemplate.temppho2,
//     '8': JsonDataTemplate.temppho3,
//     '9': JsonDataTemplate.temppho4,
//     '10': JsonDataTemplate.temppho5,
//     '11': JsonDataTemplate.templateNail1,
//     '12': JsonDataTemplate.templateNail2,
//     '13': JsonDataTemplate.templateNail3,
//     '14': JsonDataTemplate.templateNail4,
//     '15': JsonDataTemplate.templateNail5,
//   };

//   @override
//   Widget build(BuildContext context) {
//     //[092326HG] block
//     // if (idTemplate == '1') {
//     //   jsonData = JsonDataTemplate.temppho5;
//     // }
//     // else if (idTemplate == '2') {
//     //   jsonData = JsonDataTemplate.temprestaurant5;
//     // }

//     //[092326HG]
//     // Kiểm tra xem idTemplate có tồn tại trong templateMap không
//     if (!templateMap.containsKey(idTemplate)) {
//       throw ArgumentError('Invalid idTemplate: $idTemplate');
//     }

//     // Lấy dữ liệu JSON tương ứng
//     Map<String, dynamic> jsonData = templateMap[idTemplate]!;

//     //092314LH Fix dynamic sample data to template
//     Map<String, dynamic> radioSetting =
//         jsonData.containsKey('RatioSetting') ? jsonData['RatioSetting'] : {};

//     GlobalSetting.ratioDataSetting = radioSetting.containsKey('renderScreen')
//         ? radioSetting['renderScreen']
//         : {};
//     GlobalSetting.ratioStyle =
//         CalculateRatioTemplate(context, radioSetting['textStyle'] ?? {});

//     //092314LH Set dataSetting from json data
//     GlobalSetting.dataSetting =
//         jsonData.containsKey('dataSetting') ? jsonData['dataSetting'] : {};
//     GlobalSetting.dynamicDataSetting = {};

//     return Sizer(builder: (context, orientation, deviceType) {
//       return ModuleHelper.renderWidget(jsonData.containsKey("widgets_setting")
//           ? jsonData["widgets_setting"]
//           : null);
//     });
//   }
// }
