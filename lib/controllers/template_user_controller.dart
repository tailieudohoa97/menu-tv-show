// ignore_for_file: public_member_api_docs, sort_constructors_first
// import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:tv_menu/helper/calulate_ratio_temp.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';

import 'package:tv_menu/services/template_service.dart';
// import 'package:collection/collection.dart';

// class TemplateController extends StatefulWidget {
//   const TemplateController({super.key});

//   @override
//   _TemplateUserControllerState createState() => _TemplateUserControllerState();
// }

class TemplateUserControllerState extends StatefulWidget {
  final String tokenUserTemplate;
  TemplateUserControllerState({
    Key? key,
    required this.tokenUserTemplate,
  }) : super(key: key);

  @override
  State<TemplateUserControllerState> createState() =>
      _TemplateUserControllerStateState();
}

class _TemplateUserControllerStateState
    extends State<TemplateUserControllerState> {
  // Danh sách dữ liệu từ tệp JSON
  TemplateService templateService = TemplateService();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // GlobalSetting.dataSetting = jsonData.containsKey('dataSetting')
    //     ? Map.fromIterable(jsonData['dataSetting'],
    //         key: (item) => item["key"].toString(), value: (item) => item)
    //     : {};
    if (widget.tokenUserTemplate.isNotEmpty) {
      return FutureBuilder<Map<String, dynamic>>(
        future: getDataTemplate(widget.tokenUserTemplate),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data!["widgetSetting"] != null) {
              //092314LH Fix dynamic sample data to template
              Map<String, dynamic> radioSetting =
                  snapshot.data!.containsKey('ratioSetting')
                      ? snapshot.data!['ratioSetting']
                      : {};
              // initState();
              GlobalSetting.ratioDataSetting =
                  radioSetting.containsKey('renderScreen')
                      ? radioSetting['renderScreen']
                      : {};
              GlobalSetting.ratioStyle = CalculateRatioTemplate(
                  context, radioSetting['textStyle'] ?? {});
              //092314LH Set dataSetting from json data
              GlobalSetting.dataSetting =
                  snapshot.data!.containsKey('dataSetting')
                      ? snapshot.data!['dataSetting']
                      : {};
              GlobalSetting.dynamicDataSetting = {};
              return Sizer(builder: (context, orientation, deviceType) {
                return ModuleHelper.renderWidget(
                    snapshot.data!.containsKey("widgetSetting")
                        ? snapshot.data!["widgetSetting"]
                        : null);
              });
            } else {
              return Center(
                child: Text('Token is not avaliable!!!'),
              );
            }
          } else if (snapshot.hasError) {
            return Center(child: Text('${snapshot.error}'));
          }

          // By default, show a loading spinner.
          return Center(child: const CircularProgressIndicator());
        },
      );
    } else {
      return Center(
        child: Text('you need to input your token!!!'),
      );
    }
  }

  Future<Map<String, dynamic>> getDataTemplate(
    String tokenUserTemplate,
  ) async {
    Map<String, dynamic> template = {};
    final getUserSetting =
        await templateService.getUserSettingTemplate(tokenUserTemplate);
    if (getUserSetting.isNotEmpty) {
      int idTemplate = int.parse(getUserSetting['templateId']);
      final template = await templateService.getTemplateData(idTemplate);
      if (template.containsKey('dataSetting')) {
        if (getUserSetting.containsKey('userData')) {
          template['dataSetting'] = getUserSetting['userData'];
          return template;
        }
      }
    }
    return template;
  }
}
