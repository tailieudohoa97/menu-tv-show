class StringManager {
  /* app */
  static const package = 'tv_menu';
  static const appName = 'TV Menu';

  /* common */
  static const textNext = 'Next';
  static const textVerified = 'Verified';
  static const textDone = 'Done';
  static const success = 'Success';
  static const ok = 'OK';
  static const passwordHiddenChar = '*';
  static const skip = 'Skip';
  static const dashboard = 'Dashboard';
  static const logout = 'Logout';
  static const cancel = 'Cancel';
  static const confirm = 'Confirm';
  static const setting = 'Setting';
  static const congratulations = 'Congratulations!';

  /* login */
  static const loginTitle = 'Have an account?';
  static const signin = 'SIGN IN';
  static const userNameLogin = 'Username';
  static const password = 'Password';
  static const rememberMe = 'Remember Me';
  static const user = 'User';
  static const admin = 'Admin';
  static const supervisor = 'Supervisor';
  static const dontHaveAccount = "Don't have an account? ";
  static const signUp = 'Sign Up';
  static const forGotPassword = 'Forgot Password';

  /* Fotgot Password */
  static const forgotPasswordCaption =
      "Enter the email address associated with your account and we'll send you a link to reset your password.";
  static const fotgotCongratulation =
      "We've sent a new password to your email - please check it!";

  /* User Management */
  static const titleUserManagement = 'User Management';
  static const addNewUser = 'Add New User';
  static const searchHint = 'Search...';
  // [102331TIN] add search hint for user management
  static const searchHintForUserManagement = 'Search to find account...';
  static const filterButton = 'Filter';
  static const roleType = 'Role Type';
  static const userName = 'User Name';
  static const firstName = 'First Name';
  static const lastName = 'Last Name';
  static const email = 'Email';
  static const address = 'Address';
  static const phoneNumber = 'Phone Number';
  static const action = 'Action';
  static const editUserTitle = 'Edit User';
  static const deleteUser = 'Delete User';
  // [112315TIN] add Preivew user text
  static const previewUser = 'Preview User';
  static const confirmDeleteUserDesc = 'Do you want to delete this user?';

  /* Template Management */
  static const titleTemplateManagement = 'Template Management';
  static const addNewTemplate = 'Add New Template';
  static const editTemplateTitle = 'Edit Template';
  static const deleteTemplate = 'Delete Template';
  static const confirmDeleteTemplateDesc =
      'Do you want to delete this template?';

  static const titleUserTemplateManagement = 'User Template Management';
  static const addNewUserTemplate = 'Add New User Template';
  static const editUserTemplateTitle = 'Edit User Template';
  static const deleteUserTemplate = 'Delete User Template';
  static const confirmDeleteUserTemplateDesc =
      'Do you want to delete this user template?';
  static const name = 'Template Name';
  static const subscriptionLevel = 'Subcription Level';
  static const templateSlug = 'Template Slug';
  // [112307TIN] edit text
  // static const category = 'Category Id';
  static const category = 'Category';
  static const image = 'Image';
  static const ratioSetting = 'Ratio Setting';
  static const created = 'Date Created';
  static const ratios = 'Ratio ';
  static const dataSetting = 'Data Setting';
  static const widgetSetting = 'Widget Setting';
  static const status = 'Status';
  static const jsonData = 'Json Data';
  // [102331TIN] add search hint for template management
  static const searchHintForTemplateManagement =
      'Find the template theme you want...';

  /* Analysis */
  static const titleAnalysis = 'Analysis';
}
