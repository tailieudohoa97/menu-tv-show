import 'package:flutter/material.dart';
import 'package:tv_menu/common/common.dart';

class CustomCheckbox extends StatefulWidget {
  static final TextStyle DEFAULT_LABEL_STYLE =
      AppTextStyles.body14Regular.copyWith(color: Colors.white);
  final bool initValue;
  final String label;
  final TextStyle? labelStyle;
  final void Function(bool?)? _handleChange;
  final double? gap;
  const CustomCheckbox({
    super.key,
    this.initValue = false,
    this.label = '',
    Function(bool?)? onChanged,
    this.labelStyle,
    this.gap,
  }) : this._handleChange = onChanged;

  @override
  State<StatefulWidget> createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<CustomCheckbox> {
  bool _isChecked = false;

  @override
  void initState() {
    super.initState();
    _isChecked = widget.initValue;
  }

  void _customHandleChange(bool? isChecked) {
    if (isChecked == null) return;
    _isChecked = isChecked;
    if (widget._handleChange != null) {
      (widget._handleChange as Function(bool?))(isChecked);
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(
          value: _isChecked,
          activeColor: AppColors.confirmButton,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          side: MaterialStateBorderSide.resolveWith(
            (states) => const BorderSide(width: 1, color: AppColors.white),
          ),
          onChanged: _customHandleChange,
        ),
        SizedBox(width: widget.gap ?? 0),
        InkWell(
          onTap: () => _customHandleChange(!_isChecked),
          child: Text(widget.label,
              style: widget.labelStyle ?? CustomCheckbox.DEFAULT_LABEL_STYLE),
        )
      ],
    );
  }
}
