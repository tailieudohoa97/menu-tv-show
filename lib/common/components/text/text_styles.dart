import 'package:flutter/painting.dart';

import '../../colors/app_color.dart';

class AppTextStyles {
  static const fontMerriweather = 'Merriweather';
  static const fontCatamaran = 'Catamaran';
/* title */
  static const TextStyle title32Bold = TextStyle(
    fontFamily: fontCatamaran,
    fontSize: 32,
    color: AppColors.neutral,
    height: (52 / 32),
    fontWeight: FontWeight.w700,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle title24Bold = TextStyle(
    fontFamily: fontCatamaran,
    fontSize: 24,
    color: AppColors.neutral,
    height: (36 / 24),
    fontWeight: FontWeight.w700,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle title24Medium = TextStyle(
    fontFamily: fontCatamaran,
    fontSize: 24,
    color: AppColors.neutral,
    height: (36 / 24),
    fontWeight: FontWeight.w500,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle title24Regular = TextStyle(
    fontFamily: fontCatamaran,
    fontSize: 24,
    color: AppColors.neutral,
    height: (36 / 24),
    fontWeight: FontWeight.w400,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle title20Bold = TextStyle(
    fontFamily: fontCatamaran,
    fontSize: 20,
    color: AppColors.neutral,
    height: (32 / 20),
    fontWeight: FontWeight.w700,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle title20Medium = TextStyle(
    fontFamily: fontCatamaran,
    fontSize: 20,
    color: AppColors.neutral,
    height: (32 / 20),
    fontWeight: FontWeight.w500,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle title20Regular = TextStyle(
    fontFamily: fontCatamaran,
    fontSize: 20,
    color: AppColors.neutral,
    height: (32 / 20),
    fontWeight: FontWeight.w400,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle title18Bold = TextStyle(
    fontFamily: fontCatamaran,
    fontSize: 18,
    color: AppColors.neutral,
    height: (24 / 18),
    fontWeight: FontWeight.w700,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle title18Medium = TextStyle(
    fontFamily: fontCatamaran,
    fontSize: 18,
    color: AppColors.neutral,
    height: (24 / 18),
    fontWeight: FontWeight.w500,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle title18Regular = TextStyle(
    fontFamily: fontCatamaran,
    fontSize: 18,
    color: AppColors.neutral,
    height: (24 / 18),
    fontWeight: FontWeight.w400,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

/* body */
  static const TextStyle body16Bold = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 16,
    color: AppColors.neutral,
    height: (24 / 16),
    fontWeight: FontWeight.w700,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle body16Regular = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 16,
    color: AppColors.neutral,
    height: (24 / 16),
    fontWeight: FontWeight.w300,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal, decoration: TextDecoration.none,
  );

  static const TextStyle body14Medium = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 14,
    color: AppColors.lightGray,
    height: (20 / 14),
    fontWeight: FontWeight.w400,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle body14Regular = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 14,
    color: AppColors.neutral,
    height: (20 / 14),
    fontWeight: FontWeight.w300,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  // [102320TIN] add body14Bold
  static const TextStyle body14Bold = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 14,
    color: AppColors.neutral,
    height: (20 / 14),
    fontWeight: FontWeight.w700,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

/* button */
  static const TextStyle button18Medium = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 18,
    color: AppColors.neutral,
    height: (24 / 18),
    fontWeight: FontWeight.w400,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle button16Medium = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 16,
    color: AppColors.neutral,
    height: (24 / 16),
    fontWeight: FontWeight.w400,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle button18Bold = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 18,
    color: AppColors.neutral,
    height: (24 / 16),
    fontWeight: FontWeight.w800,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

/* other */

  static const TextStyle heading32Bold = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 32,
    color: AppColors.neutral,
    height: (52 / 32),
    fontWeight: FontWeight.w700,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle caption12Medium = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 12,
    color: AppColors.neutral,
    height: (16 / 12),
    fontWeight: FontWeight.w400,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle caption12Regular = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 12,
    color: AppColors.neutral,
    height: (16 / 12),
    fontWeight: FontWeight.w300,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle caption10Regular = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 10,
    color: AppColors.neutral,
    height: (16 / 10),
    fontWeight: FontWeight.w300,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle label14Medium = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 14,
    color: AppColors.neutral,
    height: (20 / 14),
    fontWeight: FontWeight.w400,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle label12Medium = TextStyle(
    fontFamily: fontMerriweather,
    fontSize: 12,
    color: AppColors.neutral,
    height: (16 / 12),
    fontWeight: FontWeight.w400,
    letterSpacing: 0,
    // package: DomainService.isDev ? null : StringManager.package,
    leadingDistribution: TextLeadingDistribution.even,
    fontStyle: FontStyle.normal,
  );
}
