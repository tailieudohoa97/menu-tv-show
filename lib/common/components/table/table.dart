import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:tv_menu/common/components/table/table_footer.dart';

import '../../common.dart';

class TableManagement extends StatelessWidget {
  // [102320TIN] add default rows per page items
  static const List<int> _defaultRowsPerPageItems = [2, 5, 10, 30];
  int rowsPerPage = PaginatedDataTable.defaultRowsPerPage;
  int? sortColumnIndex;
  bool sortAscending;
  final DataTableSource dataSource;
  final List<DataColumn> columns;
  // [102319TIN] Add custom paginator controller
  final PaginatorController paginatorController = PaginatorController();
  TableManagement(
      {Key? key,
      required this.dataSource,
      required this.columns,
      required this.rowsPerPage,
      this.sortColumnIndex,
      this.sortAscending = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // [102325TIN] fix custom tbale footer overlay table content
    return Column(
        // alignment: Alignment.bottomCenter,
        // clipBehavior: Clip.hardEdge,
        children: [
          Expanded(
            child: Theme(
              data: ThemeData(
                  textTheme: TextTheme(
                    bodySmall: TextStyle(
                      // [102319TIN] Change footer color to white
                      // color: AppColors.lightGray
                      // Change text color
                      color: Colors.white,
                    ),
                  ),
                  // [102320TIN] hide divider between rows
                  dividerColor: Colors.transparent,
                  // set default color row at here
                  // dataTableTheme: DataTableThemeData(
                  //   dataRowColor:
                  // )
                  // [102320TIN] set selected color for rows and enable hover effect
                  colorScheme: Theme.of(context).colorScheme.copyWith(
                        primary: Colors.blue,
                      )),
              child: Container(
                decoration: BoxDecoration(
                  //[102312HG] Makeup
                  // color: Theme.of(context).primaryColor,
                  color: Colors.white.withOpacity(0.05),
                  border: Border.all(
                    width: 0.5,
                    color: Colors.white.withOpacity(0.05),
                  ),
                  borderRadius: BorderRadius.circular(30),
                ),
                child: PaginatedDataTable2(
                  // [102320TIN] replace fixed rows per page items to variable
                  // 100 Won't be shown since it is smaller than total records
                  // availableRowsPerPage: const [2, 5, 10, 30, 100],
                  availableRowsPerPage: _defaultRowsPerPageItems,
                  horizontalMargin: 20,
                  checkboxHorizontalMargin: 12,
                  dataRowHeight: 60,
                  columnSpacing: 0,
                  wrapInCard: false,
                  renderEmptyRowsInTheEnd: false,
                  showCheckboxColumn: false,
                  headingRowColor: MaterialStateColor.resolveWith(
                      (states) => AppColors.transparent),
                  headingTextStyle: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold, //[102312HG] Makeup
                  ),
                  rowsPerPage: rowsPerPage,
                  dataTextStyle: const TextStyle(color: AppColors.lightGray),

                  // minWidth: 800,
                  fit: FlexFit.tight,

                  onRowsPerPageChanged: (value) {
                    // No need to wrap into setState, it will be called inside the widget
                    // and trigger rebuild
                    //setState(() {
                    rowsPerPage = value!;
                    //});
                  },
                  initialFirstRowIndex: 0,
                  onPageChanged: (rowIndex) {},
                  sortColumnIndex: sortColumnIndex,
                  sortAscending: sortAscending,
                  sortArrowIcon: Icons.keyboard_arrow_up, // custom arrow
                  sortArrowAnimationDuration: const Duration(
                      milliseconds: 0), // custom animation duration
                  // onSelectAll: dataSource.selectAll,
                  columns: columns,
                  empty: Center(
                      child: Container(
                          padding: const EdgeInsets.all(20),
                          color: Colors.grey[200],
                          child: const Text('No data'))),
                  source: dataSource,

                  // [102319TIN] Hide default paginator
                  hidePaginator: true,
                  controller: paginatorController,
                ),
              ),
            ),
          ),
          // [102320TIN] replace fixed code to widget
          TableFooter(
            paginatorController: paginatorController,
            color: Colors.white,
            initRowsPerPage: rowsPerPage,
            rowsPerPageItems: _defaultRowsPerPageItems,
          )
          // [102319TIN] Add custom paginator
          // Container(
          //   // color: Colors.amber,
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.end,
          //     children: [
          //       const Text(
          //         'Rows per page',
          //         style: TextStyle(color: Colors.white),
          //       ),
          //       SizedBox(
          //         width: 16,
          //       ),
          //       DropdownMenu<int>(
          //         initialSelection: rowsPerPage,
          //         // [102320TIN] style for dropdown
          //         trailingIcon:
          //             Icon(Icons.arrow_drop_down_sharp, color: Colors.white),
          //         inputDecorationTheme: InputDecorationTheme(
          //           border: OutlineInputBorder(
          //             borderSide: BorderSide(color: Colors.white),
          //           ),
          //           focusedBorder: OutlineInputBorder(
          //             borderSide: BorderSide(color: Colors.white),
          //           ),
          //         ),
          //         // items: const [
          //         //   DropdownMenuItem<int>(value: 2, child: Text('2')),
          //         //   DropdownMenuItem<int>(value: 5, child: Text('5')),
          //         //   DropdownMenuItem<int>(value: 10, child: Text('10')),
          //         //   DropdownMenuItem<int>(value: 30, child: Text('30'))
          //         // ],
          //         // onChanged: (selectedValue) {
          //         //   print('selected value $selectedValue');
          //         //   if (selectedValue != null) {
          //         //     paginatorController.setRowsPerPage(selectedValue);
          //         //   }
          //         // },

          //         dropdownMenuEntries: const [
          //           DropdownMenuEntry(value: 2, label: '2'),
          //           DropdownMenuEntry(value: 5, label: '5'),
          //           DropdownMenuEntry(value: 10, label: '10'),
          //           DropdownMenuEntry(value: 30, label: '30'),
          //         ],
          //         onSelected: (selectedValue) {
          //           print('selected value $selectedValue');
          //           if (selectedValue != null) {
          //             paginatorController.setRowsPerPage(selectedValue);
          //           }
          //         },
          //       ),
          //       SizedBox(
          //         width: 16,
          //       ),
          //       // [102320TIN] replace text button to icon button
          //       // Tooltip(
          //       // message: 'Previous page',
          //       // child:
          //       IconButton(
          //         tooltip: 'Previous page',
          //         onPressed: () {
          //           paginatorController.goToPreviousPage();
          //         },
          //         // style: IconButton.styleFrom(foregroundColor: Colors.white),
          //         color: Colors.white,
          //         icon: Icon(Icons.chevron_left),
          //       ),
          //       // ),
          //       SizedBox(
          //         width: 16,
          //       ),
          //       // Tooltip(
          //       //   message: 'Next page',
          //       //   child:
          //       IconButton(
          //         tooltip: 'Next page',
          //         onPressed: () {
          //           paginatorController.goToNextPage();
          //         },
          //         // style: IconButton.styleFrom(foregroundColor: Colors.white),
          //         color: Colors.white,
          //         icon: Icon(Icons.chevron_right),
          //       ),
          //       // )
          //     ],
          //   ),
          // )
        ]);
  }
}
