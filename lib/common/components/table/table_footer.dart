import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:tv_menu/common/common.dart';

// [102325TIN] disable prev/next page button on first/last page
// [102320TIN] create table footer widget
class TableFooter extends StatefulWidget {
  static const List<int> _defaultRowsPerPageItems = [2, 5, 10, 30];
  final int? initRowsPerPage;
  final PaginatorController paginatorController;
  final Color color;
  final List<int> rowsPerPageItems;

  const TableFooter({
    super.key,
    this.initRowsPerPage,
    required this.paginatorController,
    this.color = Colors.white,
    this.rowsPerPageItems = _defaultRowsPerPageItems,
  });

  @override
  State<StatefulWidget> createState() => _TableFooterState();
}

class _TableFooterState extends State<TableFooter> {
  bool _isFirstPage = true;
  bool _isLastPage = true;
  int _pageCount = 1;

  @override
  void initState() {
    super.initState();
    if (!widget.paginatorController.isAttached) return;
    _updatePageStatus();
  }

  int _calculatePageCount() {
    return (widget.paginatorController.rowCount /
            widget.paginatorController.rowsPerPage)
        .ceil();
  }

  int _calculateCurrentPage() {
    return ((widget.paginatorController.currentRowIndex + 1) /
            widget.paginatorController.rowsPerPage)
        .ceil();
  }

  void _handleClickNextPage() {
    widget.paginatorController.goToNextPage();
    _updatePageStatus();
    setState(() {});
  }

  void _hanleClickPrevPage() {
    widget.paginatorController.goToPreviousPage();
    _updatePageStatus();
    setState(() {});
  }

  void _handleChangeRowsPerPage(int? rowsPerPage) {
    if (rowsPerPage == null) return;
    widget.paginatorController.setRowsPerPage(rowsPerPage);
    widget.paginatorController.goToFirstPage();
    _updatePageStatus();
    setState(() {});
  }

  void _updatePageStatus() {
    _pageCount = _calculatePageCount();
    final currentPage = _calculateCurrentPage();
    _isFirstPage = currentPage == 1;
    _isLastPage = currentPage == _pageCount;
  }

  @override
  Widget build(BuildContext context) {
    print('_isFirtPage, ${_isFirstPage}');
    print('_isLastPage, ${_isLastPage}');
    return Padding(
      padding: EdgeInsets.all(16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            'Rows per page',
            style: AppTextStyles.body14Regular.copyWith(color: widget.color),
          ),
          SizedBox(width: 16),
          DropdownMenu<int>(
            // [102320TIN] style for dropdown
            width: 80,
            initialSelection: widget.initRowsPerPage,
            trailingIcon:
                Icon(Icons.arrow_drop_down_sharp, color: widget.color),
            inputDecorationTheme: InputDecorationTheme(
              border: OutlineInputBorder(
                borderSide: BorderSide(color: widget.color),
                borderRadius: BorderRadius.circular(16),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: widget.color),
                borderRadius: BorderRadius.circular(16),
              ),
              labelStyle:
                  AppTextStyles.body14Regular.copyWith(color: widget.color),
            ),
            menuStyle: MenuStyle(
              // elevation: MaterialStatePropertyAll(5)
              // [102324TIN] Use bg transparent and padding bottom
              // to add spacing for menu and label of dropdown
              backgroundColor: MaterialStatePropertyAll(Colors.transparent),
              shadowColor: MaterialStatePropertyAll(Colors.transparent),
              padding: MaterialStatePropertyAll(
                EdgeInsets.only(bottom: 16),
              ),
            ),
            // items: const [
            //   DropdownMenuItem<int>(value: 2, child: Text('2')),
            //   DropdownMenuItem<int>(value: 5, child: Text('5')),
            //   DropdownMenuItem<int>(value: 10, child: Text('10')),
            //   DropdownMenuItem<int>(value: 30, child: Text('30'))
            // ],
            // onChanged: (selectedValue) {
            //   print('selected value $selectedValue');
            //   if (selectedValue != null) {
            //     paginatorController.setRowsPerPage(selectedValue);
            //   }
            // },

            dropdownMenuEntries: widget.rowsPerPageItems
                .asMap()
                .entries
                .map(
                  (item) => DropdownMenuEntry<int>(
                      value: item.value,
                      label: item.value.toString(),
                      style: TextButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: item.key == 0
                              ? BorderRadius.only(
                                  topLeft: Radius.circular(16),
                                  topRight: Radius.circular(16),
                                )
                              : (item.key == widget.rowsPerPageItems.length - 1)
                                  ? BorderRadius.only(
                                      bottomLeft: Radius.circular(16),
                                      bottomRight: Radius.circular(16),
                                    )
                                  : BorderRadius.zero,
                        ),
                        fixedSize: Size(80, 32),
                        padding:
                            EdgeInsets.symmetric(vertical: 6, horizontal: 16),
                        backgroundColor: Colors.grey.shade800.withOpacity(.8),
                        textStyle: AppTextStyles.body14Regular.copyWith(
                          color: Colors.white,
                        ),
                      )),
                )
                .toList(),
            onSelected: _handleChangeRowsPerPage,
          ),
          SizedBox(width: 16),
          // [102320TIN] replace text button to icon button
          // Tooltip(
          // message: 'Previous page',
          // child:
          IconButton(
            tooltip: 'Previous page',
            // [102325TIN] First page, disable button
            onPressed: _isFirstPage ? null : _hanleClickPrevPage,
            // style: IconButton.styleFrom(foregroundColor: Colors.white),
            color: widget.color,
            icon: Icon(Icons.chevron_left),
          ),
          // ),
          SizedBox(width: 16),
          // Tooltip(
          //   message: 'Next page',
          //   child:
          IconButton(
            tooltip: 'Next page',
            // [102325TIN] Last page, disable button
            onPressed: _isLastPage ? null : _handleClickNextPage,
            // style: IconButton.styleFrom(foregroundColor: Colors.white),
            color: widget.color,
            icon: Icon(Icons.chevron_right),
          ),
          // )
        ],
      ),
    );
  }
}
