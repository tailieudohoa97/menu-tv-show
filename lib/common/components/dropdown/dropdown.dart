import 'package:flutter/material.dart';

typedef ChangeDropdownHandler<Data extends Object> = void Function(Data?);

// [112312TIN] rename widget from Dropdown to AppDropDown
// controller property to textController

// [102331TIN] create this widget
class AppDropdown<Data extends Object> extends StatefulWidget {
  final List<Data> dataList;
  final Data? initValue;
  final TextEditingController? textController;
  final ChangeDropdownHandler<Data>? _handleChangeItem;
  final double? width;

  const AppDropdown({
    super.key,
    required this.dataList,
    this.initValue,
    this.textController,
    this.width,
    ChangeDropdownHandler<Data>? onChangeItem,
  }) : _handleChangeItem = onChangeItem;

  @override
  State<StatefulWidget> createState() => _AppDropdownState<Data>();
}

class _AppDropdownState<Data extends Object> extends State<AppDropdown<Data>> {
  Data? _selectedItem;

  @override
  initState() {
    super.initState();
    _selectedItem = widget.initValue ?? widget.dataList.first;
  }

  _handleSelectItem(Data? seletedItem) {
    _selectedItem = seletedItem;
    if (widget.textController != null && seletedItem != null) {
      widget.textController?.text = seletedItem.toString();
    }
    if (widget._handleChangeItem != null) {
      (widget._handleChangeItem as ChangeDropdownHandler<Data>)(seletedItem);
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MenuAnchor(
      alignmentOffset: Offset(0, 16),
      style: MenuStyle(
        padding: MaterialStatePropertyAll(
          EdgeInsets.symmetric(
            vertical: 0,
            horizontal: 0,
          ),
        ),
        shape: MaterialStatePropertyAll(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
        ),
      ),
      menuChildren: widget.dataList.map((data) {
        return MenuItemButton(
          onPressed: () => _handleSelectItem(data),
          child: Text(data.toString()),
        );
      }).toList(),
      builder: (context, controller, child) {
        return Container(
          // color: Colors.amber,
          width: widget.width,
          child: TextButton(
              onPressed: () {
                if (controller.isOpen) {
                  controller.close();
                } else {
                  controller.open();
                }
              },
              style: TextButton.styleFrom(
                padding: EdgeInsets.symmetric(
                  vertical: 20,
                  horizontal: 16,
                ),
                backgroundColor: Colors.white.withOpacity(.09),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                foregroundColor: Colors.white,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      (_selectedItem ?? '').toString(),
                    ),
                  ),
                  SizedBox(width: 16),
                  Icon(
                    controller.isOpen
                        ? Icons.keyboard_arrow_up
                        : Icons.keyboard_arrow_down,
                  )
                ],
              )),
        );
      },
    );
  }
}
