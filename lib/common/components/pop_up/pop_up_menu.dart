import 'package:flutter/material.dart';
import 'package:tv_menu/common/colors/app_color.dart';

class PopUPMenu extends StatelessWidget {
  final List<PopupMenuEntry<String>> menuList;
  final Widget? icon;
  final Function(String)? onSelected;
  const PopUPMenu({
    Key? key,
    required this.menuList,
    this.icon,
    this.onSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(
      color: AppColors.black,
      itemBuilder: (context) => menuList,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      icon: icon,
      onSelected: onSelected,
    );
  }
}
