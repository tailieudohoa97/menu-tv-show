import 'package:flutter/material.dart';

import '../../common.dart';

class ButtonApp extends StatefulWidget {
  final VoidCallback? onTap;
  final Widget? title;
  final IconData? icon;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final double? width;
  final double? height;
  final double? circularIndex;
  final Color? splashColor;
  final Color? borderColor;
  final Color? hoverColor;
  final Color? focusColor;
  final Color? textColor;
  final Color? iconColor;
  final Color? buttonColor;

  final bool borderEnable;

  const ButtonApp({
    Key? key,
    this.title,
    this.onTap,
    this.icon,
    this.margin,
    this.width,
    this.height,
    this.padding,
    this.circularIndex,
    this.splashColor,
    this.borderColor,
    this.focusColor,
    this.hoverColor,
    this.iconColor,
    this.textColor,
    this.buttonColor,
    this.borderEnable = true,
  }) : super(key: key);

  @override
  State<ButtonApp> createState() => _ButtonAppState();
}

class _ButtonAppState extends State<ButtonApp> {
  bool _isHover = false;
  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onHover: (event) {
        setState(() {
          _isHover = true;
        });
      },
      onExit: (event) {
        setState(() {
          _isHover = false;
        });
      },
      child: InkWell(
          borderRadius: BorderRadius.circular(widget.circularIndex ?? 30),
          splashColor: widget.splashColor ?? Colors.blue.withOpacity(0.1),
          hoverColor: widget.hoverColor ?? Colors.blueAccent.withOpacity(0.1),
          focusColor: widget.focusColor ?? Colors.green,
          onTap: widget.onTap ?? () {},
          child: Container(
            width: widget.width,
            height: widget.height,
            margin: widget.margin,
            padding: widget.padding ??
                const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            decoration: widget.borderEnable
                ? BoxDecoration(
                    color: _isHover
                        ? widget.buttonColor != null
                            ? widget.buttonColor!
                                .withOpacity(0.8) //[102311HG] Makeup
                            : AppColors.transparent
                        : widget.buttonColor ??
                            Theme.of(context).primaryColor.withOpacity(0.5),
                    //[102312HG] Makeup
                    border: Border.all(
                      //color: Colors.grey.withOpacity(0),
                      color: widget.borderColor ?? Colors.white,
                      width: 1.5,
                    ),
                    borderRadius:
                        BorderRadius.circular(widget.circularIndex ?? 30))
                : null,
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (widget.title != null) widget.title!,
                  if (widget.title != null && widget.icon != null)
                    const SizedBox(
                      width: 3,
                    ),
                  if (widget.icon != null)
                    Icon(
                      widget.icon,
                      color: widget.iconColor ?? AppColors.blueNeon,
                    )
                ],
              ),
            ),
          )),
    );
  }
}
