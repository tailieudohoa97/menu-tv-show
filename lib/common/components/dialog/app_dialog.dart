import 'package:flutter/material.dart';

import '../../common.dart';

class AppDialog extends StatelessWidget {
  final String? title;
  final String? description;
  final String? buttonTitle;
  final Widget? descriptionWidget;
  final Widget? iconWidget;
  final bool shortButton;

  const AppDialog({
    Key? key,
    this.title,
    this.description,
    this.buttonTitle,
    this.descriptionWidget,
    this.iconWidget,
    this.shortButton = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.symmetric(horizontal: 24),
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 44,
          vertical: 30,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: AppColors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            if (iconWidget != null) ...[iconWidget!, const SizedBox(height: 5)],
            if (title != null)
              Padding(
                padding: const EdgeInsets.only(bottom: 4),
                child: AppText(
                  title,
                  AppTextStyles.title24Bold,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            if (description != null)
              AppText(
                description,
                AppTextStyles.body14Regular,
                color: AppColors.gray,
                textAlign: TextAlign.center,
              )
            else
              descriptionWidget != null
                  ? descriptionWidget!
                  : const SizedBox.shrink(),
          ],
        ),
      ),
    );
  }
}
