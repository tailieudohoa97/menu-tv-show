import 'package:flutter/material.dart';
import 'package:tv_menu/common/components/checkbox/checkbox.dart';
import 'package:tv_menu/common/components/dropdown/dropdown.dart';
import 'package:tv_menu/models/category.dart';
import 'package:tv_menu/services/template_service.dart';
import 'package:tv_menu/widgets/popup_layout.dart';
import '../../../../../common/common.dart';
import '../text_field/text_field_input.dart';

class CreateTemplateDialog extends StatelessWidget {
  final Function onCreateTemplate;
  final TextEditingController nameTemplate;
  final TextEditingController slugTemplate;
  final TextEditingController subscriptionLevel;
  final TextEditingController ratios;

  final TextEditingController categoryId;
  final TextEditingController ratioSetting;
  final TextEditingController widgetSetting;
  final TextEditingController dataSetting;
  final String title;
  const CreateTemplateDialog({
    required this.title,
    Key? key,
    required this.nameTemplate,
    required this.onCreateTemplate,
    required this.slugTemplate,
    required this.subscriptionLevel,
    required this.ratios,
    required this.categoryId,
    required this.ratioSetting,
    required this.widgetSetting,
    required this.dataSetting,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // [112301TIN] set max widht of popup is 90% screen width
    final screenSize = MediaQuery.of(context).size;
    //[102316HG] Makeup
    return popupLayout(
      maxWidth: screenSize.width * 0.9,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            AppText(
              title,
              AppTextStyles.title24Bold.copyWith(color: AppColors.white),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 30),
            // [112006TIN] relayout dialog
            // [112301TIN] split form inputs to 2 coloumns

            Column(
              children: [
                Row(
                  children: [
                    TextFieldInput(
                      radius: 20,
                      controller: nameTemplate,
                      title: StringManager.name,
                      width: 500,
                    ),
                    const SizedBox(width: 20),
                    TextFieldInput(
                      radius: 20,
                      controller: slugTemplate,
                      title: StringManager.templateSlug,
                      // descriptionForm: true,
                      width: 500,
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    AppDropdown(
                      dataList: const ['Modal', 'Gold', 'Diamond'],
                      initValue: 'Modal',
                      textController: subscriptionLevel,
                      width: 500,
                    ),
                    const SizedBox(width: 20),
                    // [112312TIN] replace text input by dropdown
                    FutureBuilder(
                      future: TemplateService.getCategories(),
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          return Text(
                            'Error ${snapshot.error.toString()}',
                            style: TextStyle(color: AppColors.red),
                          );
                        }
                        if (snapshot.hasData) {
                          final categories =
                              (snapshot.data ?? []).map((jsonMap) {
                            final map = Map<String, dynamic>.from(jsonMap);
                            return Category.fromMap(map);
                          }).toList();

                          final Category? initCategory = categories.firstOrNull;

                          return AppDropdown<Category>(
                            initValue: initCategory,
                            dataList: categories,
                            width: 500,
                            onChangeItem: (category) {
                              if (category == null) return;
                              print('category selected ${category.name}');
                              categoryId.text = category.id;
                            },
                          );
                        }

                        return SizedBox(
                          child: Center(
                            child: SizedBox(
                              height: 20,
                              width: 20,
                              child: CircularProgressIndicator(
                                strokeWidth: 2,
                                strokeCap: StrokeCap.round,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        );
                      },
                    ),

                    // TextFieldInput(
                    //   radius: 20,
                    //   controller: categoryId,
                    //   title: StringManager.category,
                    //   // descriptionForm: true,
                    //   width: 500,
                    // ),
                  ],
                ),
                // [112306TIN] change ratio text input to checkbox
                // TextFieldInput(
                //   radius: 20,
                //   controller: ratios,
                //   title: StringManager.ratios,
                //   // descriptionForm: true,
                //   width: 500,
                // ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    Text(
                      'Supported Ratio:',
                      style: AppTextStyles.body16Regular.copyWith(
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(width: 16),
                    CustomCheckbox(
                      label: '4:3',
                      onChanged: (_) {},
                    ),
                    SizedBox(width: 16),
                    CustomCheckbox(
                      label: '16:10',
                      onChanged: (_) {},
                    ),
                    SizedBox(width: 16),
                    CustomCheckbox(
                      label: '16:9',
                      onChanged: (_) {},
                    ),
                    SizedBox(width: 16),
                    CustomCheckbox(
                      label: '21:9',
                      onChanged: (_) {},
                    ),
                    SizedBox(width: 16),
                    CustomCheckbox(
                      label: '32:9',
                      onChanged: (_) {},
                    ),
                  ],
                ),
                // [102331TIN] replace text field by dropdown
                // const SizedBox(height: 20),
                // TextFieldInput(
                //   radius: 20,
                //   controller: subscriptionLevel,
                //   title: StringManager.subscriptionLevel,
                //   descriptionForm: true,
                //   width: 500,
                // ),
                const SizedBox(height: 20),
                //[112301TIN] add max lines
                //[102312HG] Makeup
                TextFieldInput(
                  radius: 20,
                  controller: ratioSetting,
                  title: StringManager.ratioSetting,
                  descriptionForm: true,
                  maxLines: 4,
                  width: 1020,
                ),
                const SizedBox(height: 20),
                TextFieldInput(
                  radius: 20,
                  controller: widgetSetting,
                  title: StringManager.widgetSetting,
                  descriptionForm: true,
                  maxLines: 4,
                  width: 1020,
                ),
                const SizedBox(height: 20),
                TextFieldInput(
                  radius: 20,
                  controller: dataSetting,
                  title: StringManager.dataSetting,
                  descriptionForm: true,
                  maxLines: 4,
                  width: 1020,
                ),
              ],
            ),
            const SizedBox(height: 30),
            //[102316HG] Makeup
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                ButtonApp(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                  title: AppText(
                      StringManager.cancel,
                      AppTextStyles.body14Medium
                          .copyWith(color: AppColors.white)),
                  //[102312HG] Makeup
                  // buttonColor: AppColors.cancelButton,
                  buttonColor: AppColors.transparent,
                  borderColor: AppColors.white,
                  circularIndex: 20,
                  onTap: () {
                    //TODO: Fix Return CallBack

                    Navigator.of(context).pop();
                  },
                ),
                // [102323TIN] Increase spacing between buttons
                const SizedBox(
                  width: 16,
                ),
                ButtonApp(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  title: AppText(
                      StringManager.confirm,
                      AppTextStyles.body14Medium
                          .copyWith(color: AppColors.white)),
                  //[102312HG] Makeup
                  // buttonColor: AppColors.confirmButton,
                  buttonColor: Color(0xFF7978fb),
                  borderColor: Color(0xFF7978fb),
                  circularIndex: 20,
                  onTap: () {
                    //TODO: Fix Return CallBack
                    onCreateTemplate();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
