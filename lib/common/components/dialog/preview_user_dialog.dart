import 'package:flutter/material.dart';
import 'package:tv_menu/common/components/dropdown/dropdown.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/widgets/popup_layout.dart';
import '../../../../../common/common.dart';
import '../text_field/text_field_input.dart';

// [112315TIN] create this widget
class PreviewUserDialog extends StatelessWidget {
  static const _avatarImageLink =
      'https://secure.gravatar.com/avatar/a0dcb662da76deb8c9072e6376cc9098?s=96&d=mm&r=g';
  final User user;
  final String title;
  const PreviewUserDialog({
    super.key,
    required this.title,
    required this.user,
  });

  @override
  Widget build(BuildContext context) {
    //[102316HG] Makeup
    return popupLayout(
      child: SingleChildScrollView(
        child:
            // set fixed width is 300
            SizedBox(
          width: 300,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              AppText(
                title,
                AppTextStyles.title24Bold.copyWith(color: AppColors.white),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 30),
              CircleAvatar(
                radius: 50,
                backgroundColor: AppColors.lightGray,
                backgroundImage: NetworkImage(_avatarImageLink),
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: CircleAvatar(
                        radius: 25,
                        backgroundColor: AppColors.transparent,
                        child: InkWell(
                          onTap: () async {},
                          child: Container(
                              constraints: BoxConstraints.tight(
                                Size(50, 45),
                              ),
                              color: AppColors.transparent,
                              child: const Icon(Icons.camera_alt)),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 20),
              TextFieldInput(
                enabled: false,
                initialValue: user.userName,
                radius: 20,
                title: StringManager.userName,
                width: 400,
              ),
              const SizedBox(height: 20),
              TextFieldInput(
                enabled: false,
                initialValue:
                    user.email != null && (user.email as String).isNotEmpty
                        ? user.email
                        : '_',
                radius: 20,
                title: StringManager.email,
                width: 400,
              ),
              const SizedBox(height: 20),
              TextFieldInput(
                enabled: false,
                initialValue: user.passWord != null &&
                        (user.passWord as String).isNotEmpty
                    ? user.passWord
                    : '_',
                radius: 20,
                title: StringManager.password,
                width: 400,
              ),
              const SizedBox(height: 20),
              TextFieldInput(
                enabled: false,
                initialValue: user.subscriptionLevel != null &&
                        (user.subscriptionLevel as String).isNotEmpty
                    ? user.subscriptionLevel
                    : '_',
                radius: 20,
                title: StringManager.subscriptionLevel,
                width: 400,
              ),
              const SizedBox(height: 30),

              //[102316HG] Makeup
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ButtonApp(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    title: AppText(
                        StringManager.ok,
                        AppTextStyles.body14Medium
                            .copyWith(color: AppColors.white)),
                    //[102312HG] Makeup
                    // buttonColor: AppColors.confirmButton,
                    buttonColor: Color(0xFF7978fb),
                    borderColor: Color(0xFF7978fb),
                    circularIndex: 20,
                    onTap: () {
                      // //TODO: Fix Return CallBack
                      // widget.onCreateUser(User(
                      //     id: "next id",
                      //     userName: widget.userName.text,
                      //     passWord: widget.password.text,
                      //     email: widget.email.text,
                      //     subscriptionLevel: widget.subscriptionLevel.text));
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
