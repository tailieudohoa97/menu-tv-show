import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tv_menu/common/components/dropdown/dropdown.dart';
import 'package:tv_menu/models/user.dart';
import 'package:tv_menu/widgets/popup_layout.dart';

import '../../../../../common/common.dart';
import '../../../views/admin/user_management/widgets/user_sources.dart';
import '../text_field/text_field_input.dart';

class EditUserDialog extends StatefulWidget {
  final String title;
  final User user;
  final Function onEditUser;
  const EditUserDialog({
    required this.title,
    required this.user,
    Key? key,
    required this.onEditUser,
  }) : super(key: key);

  @override
  State<EditUserDialog> createState() => _EditUserDialogState();
}

class _EditUserDialogState extends State<EditUserDialog> {
  TextEditingController userNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordNumberController = TextEditingController();
  TextEditingController subscriptionLevelController = TextEditingController();
  @override
  void initState() {
    super.initState();

    userNameController.text = widget.user.userName;
    emailController.text = widget.user.email!;
    passwordNumberController.text = widget.user.passWord!;
    subscriptionLevelController.text = widget.user.subscriptionLevel!;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //[102316HG] Makeup
    return popupLayout(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            AppText(
              widget.title,
              AppTextStyles.title24Bold.copyWith(color: AppColors.white),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 30),
            TextFieldInput(
              radius: 20,
              controller: userNameController,
              title: StringManager.userName,
              width: 400,
            ),
            const SizedBox(height: 20),
            TextFieldInput(
              radius: 20,
              controller: emailController,
              title: StringManager.email,
              width: 400,
            ),
            const SizedBox(height: 20),
            TextFieldInput(
              radius: 20,
              controller: passwordNumberController,
              title: StringManager.phoneNumber,
              width: 400,
            ),
            const SizedBox(height: 20),
            AppDropdown(
              dataList: const ['Modal', 'Gold', 'Diamond'],
              initValue: subscriptionLevelController.text,
              textController: subscriptionLevelController,
            ),
            const SizedBox(height: 30),

            //[102316HG] Makeup
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                ButtonApp(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                  title: AppText(
                      StringManager.cancel,
                      AppTextStyles.body14Medium
                          .copyWith(color: AppColors.white)),
                  // buttonColor: AppColors.cancelButton,
                  //[102312HG] Makeup
                  buttonColor: AppColors.transparent,
                  borderColor: AppColors.white,
                  circularIndex: 20,
                  onTap: () {
                    //TODO: Fix Return CallBack

                    Navigator.of(context).pop();
                  },
                ),
                const SizedBox(
                  width: 5,
                ),
                ButtonApp(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  title: AppText(
                      StringManager.confirm,
                      AppTextStyles.body14Medium
                          .copyWith(color: AppColors.white)),
                  //[102312HG] Makeup
                  // buttonColor: AppColors.confirmButton,
                  buttonColor: Color(0xFF7978fb),
                  borderColor: Color(0xFF7978fb),
                  circularIndex: 20,
                  onTap: () {
                    //TODO: Fix Return CallBack
                    widget.onEditUser(User(
                        id: widget.user.id,
                        userName: userNameController.text,
                        passWord: passwordNumberController.text,
                        subscriptionLevel: subscriptionLevelController.text,
                        email: emailController.text));
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
