import 'package:flutter/material.dart';
import 'package:tv_menu/common/components/checkbox/checkbox.dart';
import 'package:tv_menu/common/components/dropdown/dropdown.dart';
import 'package:tv_menu/models/category.dart';
import 'package:tv_menu/models/template.dart';
import 'package:tv_menu/services/template_service.dart';
import 'package:tv_menu/widgets/popup_layout.dart';
import 'package:collection/collection.dart';
import '../../../../../common/common.dart';
import '../text_field/text_field_input.dart';

class EditTemplateDialog extends StatefulWidget {
  final String title;
  final Template template;
  final Function onEditTemplate;

  const EditTemplateDialog({
    required this.title,
    required this.template,
    Key? key,
    required this.onEditTemplate,
  }) : super(key: key);

  @override
  State<EditTemplateDialog> createState() => _EditTemplateDialogState();
}

class _EditTemplateDialogState extends State<EditTemplateDialog> {
  TextEditingController nameTemplate = TextEditingController();
  TextEditingController slugTemplate = TextEditingController();
  TextEditingController imageTemplate = TextEditingController();
  TextEditingController categoryId = TextEditingController();
  TextEditingController ratios = TextEditingController();
  TextEditingController created = TextEditingController();
  TextEditingController subscriptionLevel = TextEditingController();
  TextEditingController ratioSetting = TextEditingController();
  TextEditingController widgetSetting = TextEditingController();
  TextEditingController dataSetting = TextEditingController();

  @override
  void initState() {
    super.initState();
    nameTemplate.text = widget.template.templateName;
    slugTemplate.text = widget.template.templateSlug!;
    imageTemplate.text = widget.template.image!;
    ratios.text = widget.template.ratios!;
    created.text = widget.template.created!;
    subscriptionLevel.text = widget.template.subscriptionLevel!;
    categoryId.text = widget.template.categoryId!;
    ratioSetting.text = widget.template.ratioSetting!;
    widgetSetting.text = widget.template.widgetSetting!;
    dataSetting.text = widget.template.dataSetting!;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // [112306TIN] init supported ratios
    final String? templateRatios = widget.template.ratios;
    final List<String> supportedRatios =
        templateRatios != null && templateRatios.isNotEmpty
            ? templateRatios.split(',')
            : [];

    //[102316HG] Makeup
    return popupLayout(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            AppText(
              widget.title,
              AppTextStyles.title24Bold.copyWith(color: AppColors.white),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 30),
            // [112306TIN] relayout dialog
            // [112302TIN] split form inputs to 2 coloumns
            Column(
              children: [
                Row(
                  children: [
                    TextFieldInput(
                      controller: nameTemplate,
                      title: StringManager.name,
                      width: 500,
                    ),
                    const SizedBox(width: 20),
                    TextFieldInput(
                      controller: slugTemplate,
                      title: StringManager.templateSlug,
                      // descriptionForm: true,
                      width: 500,
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    // TextFieldInput(
                    //   controller: created,
                    //   title: StringManager.created,
                    //   // descriptionForm: true,
                    //   width: 500,
                    // ),
                    // const SizedBox(width: 20),
                    AppDropdown(
                      dataList: const ['Modal', 'Gold', 'Diamond'],
                      initValue: 'Modal',
                      textController: subscriptionLevel,
                      width: 500,
                    ),
                    const SizedBox(width: 20),
                    // [112312TIN] replace text input by dropdown
                    FutureBuilder(
                      future: TemplateService.getCategories(),
                      builder: (context, snapshot) {
                        if (snapshot.hasError) {
                          return Text(
                            'Error ${snapshot.error.toString()}',
                            style: TextStyle(color: AppColors.red),
                          );
                        }
                        if (snapshot.hasData) {
                          final categories =
                              (snapshot.data ?? []).map((jsonMap) {
                            final map = Map<String, dynamic>.from(jsonMap);
                            return Category.fromMap(map);
                          }).toList();

                          final Category? initCategory =
                              categories.firstWhereOrNull((category) =>
                                  category.id == widget.template.categoryId);

                          return AppDropdown<Category>(
                            initValue: initCategory,
                            dataList: categories,
                            width: 500,
                            onChangeItem: (category) {
                              if (category == null) return;
                              print('category selected ${category.name}');
                              categoryId.text = category.id;
                            },
                          );
                        }

                        return SizedBox(
                          child: Center(
                            child: SizedBox(
                              height: 20,
                              width: 20,
                              child: CircularProgressIndicator(
                                strokeWidth: 2,
                                strokeCap: StrokeCap.round,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                    // TextFieldInput(
                    //   controller: categoryId,
                    //   title: StringManager.category,
                    //   // descriptionForm: true,
                    //   width: 500,
                    // ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    Text(
                      'Supported Ratio:',
                      style: AppTextStyles.body16Regular.copyWith(
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(width: 16),
                    CustomCheckbox(
                      label: '4:3',
                      initValue: supportedRatios.contains('4:3'),
                      onChanged: (_) {},
                    ),
                    SizedBox(width: 16),
                    CustomCheckbox(
                      label: '16:10',
                      initValue: supportedRatios.contains('16:10'),
                      onChanged: (_) {},
                    ),
                    SizedBox(width: 16),
                    CustomCheckbox(
                      label: '16:9',
                      initValue: supportedRatios.contains('16:9'),
                      onChanged: (_) {},
                    ),
                    SizedBox(width: 16),
                    CustomCheckbox(
                      label: '21:9',
                      initValue: supportedRatios.contains('21:9'),
                      onChanged: (_) {},
                    ),
                    SizedBox(width: 16),
                    CustomCheckbox(
                      label: '32:9',
                      initValue: supportedRatios.contains('32:9'),
                      onChanged: (_) {},
                    ),
                  ],
                ),
                const SizedBox(width: 20),
                // [112302TIN] replace text field by dropdown
                // TextFieldInput(
                //   controller: subscriptionLevel,
                //   title: StringManager.subscriptionLevel,
                //   descriptionForm: true,
                //   width: 500,
                // ),
                //[102316HG] Makeup
                TextFieldInput(
                  controller: ratioSetting,
                  title: StringManager.ratioSetting,
                  descriptionForm: true,
                  maxLines: 4,
                  width: 1020,
                ),
                const SizedBox(height: 20),
                TextFieldInput(
                  controller: widgetSetting,
                  title: StringManager.widgetSetting,
                  descriptionForm: true,
                  maxLines: 4,
                  width: 1020,
                ),
                const SizedBox(height: 20),
                TextFieldInput(
                  controller: dataSetting,
                  title: StringManager.dataSetting,
                  descriptionForm: true,
                  maxLines: 4,
                  width: 1020,
                ),
              ],
            ),

            const SizedBox(height: 30),

            //[102316HG] Makeup
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                ButtonApp(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                  title: AppText(
                      StringManager.cancel,
                      AppTextStyles.body14Medium
                          .copyWith(color: AppColors.white)),
                  // buttonColor: AppColors.cancelButton,
                  //[102312HG] Makeup
                  buttonColor: AppColors.transparent,
                  borderColor: AppColors.white,
                  circularIndex: 20,
                  onTap: () {
                    //TODO: Fix Return CallBack

                    Navigator.of(context).pop();
                  },
                ),
                const SizedBox(
                  width: 5,
                ),
                ButtonApp(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  title: AppText(
                      StringManager.confirm,
                      AppTextStyles.body14Medium
                          .copyWith(color: AppColors.white)),
                  //[102312HG] Makeup
                  // buttonColor: AppColors.confirmButton,
                  buttonColor: Color(0xFF7978fb),
                  borderColor: Color(0xFF7978fb),
                  circularIndex: 20,
                  onTap: () {
                    //TODO: Fix Return CallBack
                    widget.onEditTemplate(Template(
                      id: widget.template.id,
                      templateName: nameTemplate.text,
                      templateSlug: slugTemplate.text,
                      categoryId: categoryId.text,
                      dataSetting: dataSetting.text,
                      ratioSetting: ratioSetting.text,
                      widgetSetting: widgetSetting.text,
                      isDeleted: false,
                    ));
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
