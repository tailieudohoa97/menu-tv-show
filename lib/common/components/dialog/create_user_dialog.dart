import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tv_menu/common/components/dropdown/dropdown.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/widgets/popup_layout.dart';
import '../../../../../common/common.dart';
import '../text_field/text_field_input.dart';

class CreateUserDialog extends StatefulWidget {
  final Function onCreateUser;
  final TextEditingController userName;
  final TextEditingController subscriptionLevel;
  final TextEditingController password;
  final TextEditingController email;
  final String title;
  const CreateUserDialog({
    required this.title,
    Key? key,
    required this.onCreateUser,
    required this.userName,
    required this.subscriptionLevel,
    required this.password,
    required this.email,
  }) : super(key: key);

  @override
  State<CreateUserDialog> createState() => _CreateUserDialogState();
}

class _CreateUserDialogState extends State<CreateUserDialog> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //[102316HG] Makeup
    return popupLayout(
      child: SingleChildScrollView(
        child:
            // set fixed width is 300
            SizedBox(
          width: 300,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              AppText(
                widget.title,
                AppTextStyles.title24Bold.copyWith(color: AppColors.white),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 30),
              CircleAvatar(
                radius: 50,
                backgroundColor: AppColors.lightGray,
                backgroundImage: NetworkImage(
                  'https://secure.gravatar.com/avatar/a0dcb662da76deb8c9072e6376cc9098?s=96&d=mm&r=g',
                ) as ImageProvider,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: CircleAvatar(
                        radius: 25,
                        backgroundColor: AppColors.transparent,
                        child: InkWell(
                          onTap: () async {},
                          child: Container(
                              constraints: BoxConstraints.tight(
                                Size(50, 45),
                              ),
                              color: AppColors.transparent,
                              child: const Icon(Icons.camera_alt)),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 20),
              TextFieldInput(
                radius: 20,
                controller: widget.userName,
                title: StringManager.userName,
                width: 400,
              ),
              const SizedBox(height: 20),
              TextFieldInput(
                radius: 20,
                controller: widget.email,
                title: StringManager.email,
                width: 400,
              ),
              const SizedBox(height: 20),
              TextFieldInput(
                radius: 20,
                controller: widget.password,
                title: StringManager.password,
                width: 400,
              ),
              const SizedBox(height: 20),
              // [102331TIN] replace text field by dropdown
              // TextFieldInput(
              //   radius: 20,
              //   controller: widget.subscriptionLevel,
              //   title: StringManager.subscriptionLevel,
              //   descriptionForm: true,
              //   width: 400,
              // ),
              // const SizedBox(height: 30),
              AppDropdown(
                dataList: const ['Modal', 'Gold', 'Diamond'],
                initValue: 'Modal',
                textController: widget.subscriptionLevel,
              ),
              const SizedBox(height: 30),

              //[102316HG] Makeup
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  ButtonApp(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 25, vertical: 10),
                    title: AppText(
                        StringManager.cancel,
                        AppTextStyles.body14Medium
                            .copyWith(color: AppColors.white)),
                    // buttonColor: AppColors.cancelButton,
                    //[102312HG] Makeup
                    buttonColor: AppColors.transparent,
                    borderColor: AppColors.white,
                    circularIndex: 20,
                    onTap: () {
                      //TODO: Fix Return CallBack

                      Navigator.of(context).pop();
                    },
                  ),
                  // [102323TIN] Increase spacing between buttons
                  const SizedBox(
                    width: 16,
                  ),
                  ButtonApp(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    title: AppText(
                        StringManager.confirm,
                        AppTextStyles.body14Medium
                            .copyWith(color: AppColors.white)),
                    //[102312HG] Makeup
                    // buttonColor: AppColors.confirmButton,
                    buttonColor: Color(0xFF7978fb),
                    borderColor: Color(0xFF7978fb),
                    circularIndex: 20,
                    onTap: () {
                      //TODO: Fix Return CallBack
                      widget.onCreateUser(User(
                          id: "next id",
                          userName: widget.userName.text,
                          passWord: widget.password.text,
                          email: widget.email.text,
                          subscriptionLevel: widget.subscriptionLevel.text));
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
