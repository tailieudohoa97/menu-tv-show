import 'package:flutter/material.dart';
import '../../common.dart';

//[102311HG] Block this code, don't use
// class TextFieldInput extends StatelessWidget {
//   const TextFieldInput(
//       {required this.title,
//       this.controller,
//       this.width,
//       this.descriptionForm = false,
//       this.autofocus = false,
//       this.obscureText = false,
//       this.onChanged,
//       this.suffixIcon,
//       this.textInputAction,
//       this.hintStyle,
//       this.radius,
//       super.key});
//   final String title;
//   final double? width;
//   final TextEditingController? controller;
//   final Widget? suffixIcon;
//   final bool descriptionForm;
//   final bool autofocus;
//   final double? radius;
//   final bool obscureText;
//   final TextStyle? hintStyle;
//   final Function(String)? onChanged;
//   final TextInputAction? textInputAction;
//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.center,
//       crossAxisAlignment: CrossAxisAlignment.center,
//       children: [
//         Container(
//           decoration: radius != null
//               ? BoxDecoration(
//                   borderRadius: BorderRadius.circular(radius!),
//                   color: AppColors.white.withOpacity(0.08))
//               : null,
//           width: width ?? 200,
//           child: TextFormField(
//             controller: controller,
//             onChanged: onChanged,
//             obscureText: obscureText,
//             keyboardType: descriptionForm ? TextInputType.multiline : null,
//             maxLines: descriptionForm ? 3 : 1,
//             autofocus: autofocus,
//             decoration: InputDecoration(
//               hintText: title,
//               hintStyle: hintStyle ??
//                   AppTextStyles.body16Regular.copyWith(color: AppColors.white),
//               suffixIcon: suffixIcon,
//               border: OutlineInputBorder(
//                 borderSide: BorderSide(
//                     color: AppColors.white.withOpacity(1),
//                     style: BorderStyle.none),
//                 borderRadius: radius != null
//                     ? BorderRadius.circular(radius!)
//                     : const BorderRadius.all(Radius.circular(4.0)),
//               ),
//               enabledBorder: OutlineInputBorder(
//                 borderSide: BorderSide(
//                     color: AppColors.white.withOpacity(1),
//                     style: BorderStyle.none),
//                 borderRadius: radius != null
//                     ? BorderRadius.circular(radius!)
//                     : const BorderRadius.all(Radius.circular(4.0)),
//               ),
//               focusedBorder: OutlineInputBorder(
//                 borderSide: BorderSide(
//                     color: AppColors.white.withOpacity(0),
//                     style: BorderStyle.none),
//                 borderRadius: radius != null
//                     ? BorderRadius.circular(radius!)
//                     : const BorderRadius.all(Radius.circular(4.0)),
//               ),
//               errorBorder: const OutlineInputBorder(
//                 borderSide: BorderSide(color: Colors.red, width: 5),
//               ),
//             ),
//             textInputAction: textInputAction,
//           ),
//         ),
//       ],
//     );
//   }
// }
//[112315TIN] add enabled, initialValue properties
//[112301TIN] add max lines property for text field
//[102311HG] Makeup
class TextFieldInput extends StatefulWidget {
  const TextFieldInput({
    required this.title,
    this.controller,
    this.width,
    this.descriptionForm = false,
    this.autofocus = false,
    this.obscureText = false,
    this.onChanged,
    this.suffixIcon,
    this.textInputAction,
    this.hintStyle,
    this.radius = 20, //[102316HG] Makeup
    this.maxLines,
    this.enabled,
    this.initialValue,
    super.key,
  });

  final String title;
  final double? width;
  final TextEditingController? controller;
  final Widget? suffixIcon;
  final bool descriptionForm;
  final bool autofocus;
  final double? radius;
  final bool obscureText;
  final TextStyle? hintStyle;
  final Function(String)? onChanged;
  final TextInputAction? textInputAction;
  final int? maxLines;
  final bool? enabled;
  final String? initialValue;

  @override
  _TextFieldInputState createState() => _TextFieldInputState();
}

class _TextFieldInputState extends State<TextFieldInput> {
  final FocusNode _focusNode = FocusNode();
  bool _isFocused = false;

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      setState(() {
        _isFocused = _focusNode.hasFocus;
      });
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(
          milliseconds: 300), // Điều chỉnh thời gian của animation
      curve: Curves.linear, // Điều chỉnh kiểu của animation

      decoration: BoxDecoration(
        borderRadius: widget.radius != null
            ? BorderRadius.circular(widget.radius!)
            : null,
        color: _isFocused
            ? AppColors.white.withOpacity(0)
            : AppColors.white.withOpacity(0.08),
      ),
      width: widget.width ?? 200,
      child: TextFormField(
        initialValue: widget.initialValue,
        enabled: widget.enabled,
        focusNode: _focusNode,
        controller: widget.controller,
        onChanged: widget.onChanged,
        obscureText: widget.obscureText,
        keyboardType: widget.descriptionForm ? TextInputType.multiline : null,
        maxLines: widget.maxLines ?? (widget.descriptionForm ? 3 : 1),
        autofocus: widget.autofocus,
        decoration: InputDecoration(
          // hintText: widget.title,
          // [112301TIN] replace title by label
          label: Text(
            widget.title,
            style: AppTextStyles.body16Regular.copyWith(
              color: Colors.white,
            ),
          ),
          hintStyle: widget.hintStyle ??
              AppTextStyles.body16Regular.copyWith(
                // [112301TIN] makeup
                // color: AppColors.white,
                color: AppColors.white.withOpacity(.5),
              ),
          suffixIcon: widget.suffixIcon,
          enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: AppColors.white.withOpacity(0), width: 1),
            borderRadius: widget.radius != null
                ? BorderRadius.circular(widget.radius!)
                : const BorderRadius.all(Radius.circular(4.0)),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: AppColors.white, width: 1),
            borderRadius: widget.radius != null
                ? BorderRadius.circular(widget.radius!)
                : const BorderRadius.all(Radius.circular(4.0)),
          ),
          // [112315TIN] add disabled border style
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: widget.radius != null
                ? BorderRadius.circular(widget.radius!)
                : const BorderRadius.all(Radius.circular(4.0)),
          ),
          errorBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 5),
          ),
          // [112312TIN] align hint text to top
          alignLabelWithHint: true,
        ),
        textInputAction: widget.textInputAction,
      ),
    );
  }
}
