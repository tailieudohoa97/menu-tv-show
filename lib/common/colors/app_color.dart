import 'package:flutter/material.dart';

class AppColors {
  static const white = Color(0xffffffff);
  static const blueLight = Color(0xff4EA0F6);
  static const blue = Color(0xff407ACE);
  static const black = Color(0xff000000);
  static const gray = Color(0xff828282);
  static const lightGray = Color(0xffBCBCBC);
  static const green = Color(0xff6CFF3F);
  static const red = Color(0xffCD2027);
  static const lightPurple = Color(0xFFF1E6FF);
  static const purple = Color(0xFF6F35A5);
  static const darkPurple = Color(0xFF572DFF);
  static const greyDark = Color(0xFF29313A);

  static const warning = Color(0xffed6c02);
  static const info = Color(0xff0288d1);
  static const neutral = Color(0xff232735);
  static const transparent = Color(0x00000000);
  static const Color cardBackgroundColor = Color(0xFF21222D);
  static const hintTextField = Color(0xffBCBCBC);
  static const confirmButton = Color(0xFF09ADAA);
  static const cancelButton = Color(0xfffb5b5a);

  static const blueCommon =
      _CustomSwatch(_primarydarkPrimaryValue, <int, Color>{
    50: Color(0xFFE1E2E3),
    100: Color(0xFFB3B6B9),
    200: Color(0xFF81868B),
    300: Color(0xFF4F565D),
    400: Color(0xFF29313A),
    500: Color(_primarydarkPrimaryValue),
    600: Color(0xFF030B14),
    700: Color(0xFF020911),
    800: Color(0xFF02070D),
    900: Color(0xFF010307),
  });

  static MaterialColor blueNeon = MaterialColor(
    0xFF0096FF,
    <int, Color>{
      50: const Color(0xFFA9DFD8).withOpacity(0.1),
      100: const Color(0xFFA9DFD8).withOpacity(0.2),
      200: const Color(0xFFA9DFD8).withOpacity(0.3),
      300: const Color(0xFFA9DFD8).withOpacity(0.4),
      400: const Color(0xFFA9DFD8).withOpacity(0.5),
      500: const Color(0xFFA9DFD8).withOpacity(0.6),
      600: const Color(0xFFA9DFD8).withOpacity(0.7),
      700: const Color(0xFFA9DFD8).withOpacity(0.8),
      800: const Color(0xFFA9DFD8).withOpacity(0.9),
      900: const Color(0xFFA9DFD8).withOpacity(1.0),
    },
  );

  static const MaterialColor primarydark =
      MaterialColor(_primarydarkPrimaryValue, <int, Color>{
    50: Color(0xFFE1E2E3),
    100: Color(0xFFB3B6B9),
    200: Color(0xFF81868B),
    300: Color(0xFF4F565D),
    400: Color(0xFF29313A),
    500: Color(_primarydarkPrimaryValue),
    600: Color(0xFF030B14),
    700: Color(0xFF020911),
    800: Color(0xFF02070D),
    900: Color(0xFF010307),
  });
  static const int _primarydarkPrimaryValue = 0xFF030D17;
  // static const int _primarydarkPrimaryValue = 0xFF0c1420; //[102312HG] Makeup

  static const MaterialColor primarydarkAccent =
      MaterialColor(_primarydarkAccentValue, <int, Color>{
    100: Color(0xFF4E4EFF),
    200: Color(_primarydarkAccentValue),
    400: Color(0xFF0000E7),
    700: Color(0xFF0000CE),
  });
  static const int _primarydarkAccentValue = 0xFF1B1BFF;
}

class _CustomSwatch extends ColorSwatch<int> {
  const _CustomSwatch(int primary, Map<int, Color> sw) : super(primary, sw);

  Color get opacity2 => this[50]!;
  Color get opacity3 => this[100]!;
  Color get opacity4 => this[200]!;
  Color get opacity5 => this[300]!;
  Color get opacity6 => this[400]!;
  Color get opacity7 => this[500]!;
  Color get opacity8 => this[600]!;
  Color get opacity9 => this[700]!;
  Color get opacity10 => this[800]!;
  Color get opacity11 => this[900]!;
}
