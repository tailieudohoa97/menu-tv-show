part 'image_path.dart';

class AppImageAsset {
  static const assetImagePath = 'assets/images/';
  static const typePng = '.png';
  static const typeSvg = '.svg';
  static const typeJson = '.json';
}
