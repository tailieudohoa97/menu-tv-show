import 'dart:convert';

import 'package:equatable/equatable.dart';

class User extends Equatable {
  String id;
  String userName;
  String? passWord;
  String? email;
  String? subscriptionLevel;

  User(
      {required this.id,
      required this.userName,
      this.passWord,
      this.email,
      this.subscriptionLevel});
  bool selected = false;
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'username': userName,
      'password': passWord,
      'email': email,
      'subscriptionLevel': subscriptionLevel,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
        id: map['id'].toString(),
        passWord: map['password'] ?? "",
        userName: map['username'] ?? "",
        email: map['email'] ?? "",
        subscriptionLevel: map['subscriptionLevel'] ?? "");
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) => User.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [id, userName];
}
