import 'dart:convert';

import 'package:equatable/equatable.dart';

class MenuItem extends Equatable {
  String id;
  String name;
  String price;
  String? imageUrl;
  String? description;
  String? userId;

  MenuItem(
      {required this.id,
      required this.name,
      required this.price,
      this.imageUrl,
      this.description,
      this.userId});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'imageUrl': imageUrl,
      'description': description,
      'userId': userId
    };
  }

  factory MenuItem.fromMap(Map<String, dynamic> map) {
    return MenuItem(
        id: map['id'] ?? "",
        price: map['price'] ?? "",
        name: map['name'] ?? "",
        imageUrl: map['imageUrl'] ?? "",
        description: map['description'] ?? "",
        userId: map['userId'] ?? "");
  }

  String toJson() => json.encode(toMap());

  factory MenuItem.fromJson(String source) =>
      MenuItem.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [id, name, price];
}
