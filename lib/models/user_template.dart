import 'dart:convert';

import 'package:equatable/equatable.dart';

class UserTemplate extends Equatable {
  String? id;
  String userId;
  String templateId;
  String? title;
  String? userData;
  String? tokenUserTemplate;
  String? created;
  String? expiryTime;
  bool? isDeleted;
  bool? status;
  bool selected = false;
  UserTemplate(
      {required this.userId,
      required this.templateId,
      this.id,
      this.title,
      this.userData,
      this.tokenUserTemplate,
      this.created,
      this.expiryTime,
      this.isDeleted,
      this.status});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'userId': userId,
      'templateId': templateId,
      'title': title,
      'userData': userData,
      'tokenUserTemplate': tokenUserTemplate,
      'created': created,
      'expiryTime': expiryTime,
      'isDeleted': isDeleted,
      'status': status
    };
  }

  factory UserTemplate.fromMap(Map<String, dynamic> map) {
    return UserTemplate(
        userId: map['userId'] ?? "",
        templateId: map['templateId'] ?? "",
        id: map['id'] ?? "",
        title: map['title'] ?? "",
        userData: map['userData'] ?? "",
        tokenUserTemplate: map['tokenUserTemplate'] ?? "",
        created: map['created'] ?? "",
        expiryTime: map['expiryTime'] ?? "",
        isDeleted: map['isDeleted'] == "0" ? false : true,
        status: map['status'] == "0" ? false : true);
  }

  String toJson() => json.encode(toMap());

  factory UserTemplate.fromJson(String source) =>
      UserTemplate.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [userId, templateId];
}
