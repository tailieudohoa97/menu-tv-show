import 'dart:convert';

import 'package:equatable/equatable.dart';

class Template extends Equatable {
  final String id;
  final String templateName;
  final String? templateSlug;
  final String? categoryId;
  final String? image;
  final String? subscriptionLevel;
  final String? ratios;
  final String? created;
  final String? ratioSetting;
  final String? widgetSetting;
  final String? dataSetting;
  final bool? isDeleted;
  bool selected = false;
  Template(
      {required this.id,
      required this.templateName,
      this.templateSlug,
      this.categoryId,
      this.image,
      this.subscriptionLevel,
      this.ratios,
      this.created,
      this.ratioSetting,
      this.widgetSetting,
      this.dataSetting,
      this.isDeleted});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'templateName': templateName,
      'templateSlug': templateSlug,
      'categoryId': categoryId,
      'image': image,
      'subscriptionLevel': subscriptionLevel,
      'ratios': ratios,
      'created': created,
      'ratioSetting': ratioSetting,
      'widgetSetting': widgetSetting,
      'dataSetting': dataSetting,
      'isDeleted': isDeleted! ? "1" : "0"
    };
  }

  factory Template.fromMap(Map<String, dynamic> map) {
    return Template(
        id: map['id'].toString(),
        templateName: map['templateName'] ?? "",
        templateSlug: map['templateSlug'] ?? "",
        categoryId: map['categoryId'] ?? "",
        image: map['image'] ?? "",
        subscriptionLevel: map['subscriptionLevel'] ?? "",
        ratios: map['ratios'] ?? "",
        created: map['created'] ?? "",
        ratioSetting: map['ratioSetting'] ?? "",
        widgetSetting: map['widgetSetting'] ?? "",
        dataSetting: map['dataSetting'] ?? "",
        isDeleted: map['isDeleted'].toString() == "0" ? true : false);
  }

  String toJson() => json.encode(toMap());

  factory Template.fromJson(String source) =>
      Template.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [id, templateName];
}
