import 'package:tv_menu/models/model_datas/product_data.dart';

// [092318Tin] add description field
class CategoryData {
  final String categoryName;
  final String categoryImage;
  // final List<ProductData> listMenu;
  final String? categoryDescription;

  CategoryData({
    required this.categoryName,
    required this.categoryImage,
    // required this.listMenu,
    this.categoryDescription,
  });

  CategoryData.fromMap(Map map)
      : this(
          categoryName: map['name'],
          categoryImage: map['image'],
          // listMenu: map['listMenu'] ?? [],
          categoryDescription: map['description'],
        );
}
