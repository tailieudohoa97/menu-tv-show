// [112309TIN] add subName field
// [092318Tin] Add image field
class ProductData {
  final String productName;
  final String productDescription;
  final String price;
  final String? image;
  final String? subName;

  ProductData({
    required this.productName,
    required this.productDescription,
    required this.price,
    this.image,
    this.subName,
  });

  ProductData.fromMap(Map map)
      : this(
          productName: map['name'] ?? '',
          productDescription: map['description'] ?? '',
          price: map['price'] ?? '',
          image: map['image'] ?? '',
          subName: map['subName'] ?? '',
        );
}
