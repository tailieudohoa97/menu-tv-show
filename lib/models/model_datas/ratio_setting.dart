class RatioSetting {
  final double? imageHeight;
  final double? marginBottomImage;
  final double? marginBottomProduct;
  final double? marginBottomCategoryTitle;
  final double? paddingHorizontalCategorTilte;
  final double? paddingVerticalCategorTilte;
  final double? boderRadiusStyle;

  RatioSetting(
      this.imageHeight,
      this.marginBottomImage,
      this.marginBottomProduct,
      this.marginBottomCategoryTitle,
      this.paddingHorizontalCategorTilte,
      this.paddingVerticalCategorTilte,
      this.boderRadiusStyle);

  // RatioSetting.fromMap(Map map) : this(

  // );
}
