// [112307TIN] create this model
import 'dart:convert';

class Category {
  String id;
  String name;
  String image;

  Category({required this.id, this.name = '', this.image = ''});

  factory Category.fromMap(Map<String, dynamic> map) {
    return Category(
      id: map['id'].toString(),
      name: map['name'] ?? "",
      image: map['image'] ?? "",
    );
  }

  @override
  String toString() {
    return name;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'image': image,
    };
  }

  String toJson() => json.encode(toMap());
}
