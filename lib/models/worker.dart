import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import 'models.dart';

@immutable
class Worker extends Equatable {
  String username;

  String? email;
  String role; //kitchen, cashier, supervisor

  String? nonce;
  // String? ID;
  String? avatar;

  String? lastLogin;

  Worker({
    required this.username,

    // required this.password,
    required this.email,
    required this.role,
    required this.nonce,
    // this.ID,
    this.avatar,
    this.lastLogin,
  });

  Map<String, dynamic> toMap() {
    return {
      'username': username,

      // 'password': password,
      'email': email ?? "",
      'role': role,
      'avatar': avatar,

      'nonce': nonce,
      // // 'ID': ID,

      'lastLogin': lastLogin,
    };
  }

  factory Worker.fromMap(Map<String, dynamic> map) {
    return Worker(
      username: map['username'],

      // password: map['password'],
      email: map['email'] ?? "",
      role: map['role'],

      nonce: map['nonce'].toString(),
      // // ID: map['ID'] ?? "",
      // avatar: map['ID'] ?? "",

      lastLogin: map["lastLogin"],
    );
  }

  String toJson() => json.encode(toMap());

  factory Worker.fromJson(String source) => Worker.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      username,

      // password,
      email ?? "",
      role,

      nonce ?? "",
      // ID ?? "",
      avatar ?? "",

      lastLogin ?? ""
    ];
  }
}
