import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';
import 'package:tv_menu/routes/router.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return MaterialApp.router(
            theme: ThemeData(
              fontFamily: GoogleFonts.openSans().fontFamily,
            ),
            routerConfig: RouterConfigApp.router);
      },
    );
  }
}
