import 'package:flutter/material.dart';
import 'package:tv_menu/controller/admin/template/add_new_template_controller.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/views/admin/template_management/widgets/table_template_background.dart';
import 'package:tv_menu/views/admin/user_template_management/widgets/table_user_template_background.dart';

import '../../../common/common.dart';
import '../../../helper/responsive.dart';

class UserTemplatePage extends StatelessWidget {
  final List<UserTemplate> templates;
  final Function onCreateUserTemplate;
  final Function onUpdateUserTemplate;
  final Function onDeleteUserTemplate;
  const UserTemplatePage({
    super.key,
    required this.templates,
    required this.onCreateUserTemplate,
    required this.onUpdateUserTemplate,
    required this.onDeleteUserTemplate,
  });

  @override
  Widget build(BuildContext context) {
    SizedBox _height(BuildContext context) => SizedBox(
          height: Responsive.isDesktop(context) ? 30 : 20,
        );

    return SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: Responsive.isMobile(context) ? 15 : 18),
          child: Column(
            children: [
              SizedBox(
                height: Responsive.isMobile(context) ? 5 : 18,
              ),
              SizedBox(
                  height: 50,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Expanded(
                          child: Text(
                            StringManager.titleUserTemplateManagement,
                            style: TextStyle(color: Colors.white, fontSize: 25),
                          ),
                        ),
                        if (!Responsive.isMobile(context)) ...[
                          SizedBox(
                            width: Responsive.isDesktop(context) ? 20 : 10,
                          ),
                        ]
                      ])),
              SizedBox(
                height: Responsive.isMobile(context) ? 5 : 20,
              ),
              TableUserTemplateBackgroundWidget(
                templates: templates,
                onEditUserTemplate: onUpdateUserTemplate,
                onDeleteUserTemplate: onDeleteUserTemplate,
              ),
              _height(context),
            ],
          ),
        )));
  }
}
