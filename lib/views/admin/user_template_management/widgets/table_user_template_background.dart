import 'package:flutter/material.dart';
import 'package:tv_menu/helper/responsive.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/views/admin/template_management/widgets/table_template_data.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_filter.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_reload.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_role.dart';
import 'package:tv_menu/views/admin/user_template_management/widgets/table_user_template_data.dart';
import 'package:tv_menu/widgets/custom_card.dart';

import '../../../../common/common.dart';

class TableUserTemplateBackgroundWidget extends StatefulWidget {
  final Function onEditUserTemplate;
  final Function onDeleteUserTemplate;
  final List<UserTemplate> templates;
  const TableUserTemplateBackgroundWidget(
      {super.key,
      required this.templates,
      required this.onEditUserTemplate,
      required this.onDeleteUserTemplate});

  @override
  State<TableUserTemplateBackgroundWidget> createState() =>
      _TableUserTemplateBackgroundWidgetState();
}

class _TableUserTemplateBackgroundWidgetState
    extends State<TableUserTemplateBackgroundWidget> {
  SizedBox _height(BuildContext context) => SizedBox(
        height: Responsive.isDesktop(context) ? 30 : 20,
      );
  SizedBox _width(BuildContext context) => SizedBox(
        width: Responsive.isDesktop(context) ? 20 : 10,
      );
  final TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //[102312HG] Makeup
    return CustomCard(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Container(
              width: 300,
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(30)),
              child: Center(
                child: TextFormField(
                  onChanged: (v) {
                    setState(() {});
                  },
                  style: const TextStyle(color: AppColors.lightGray),
                  controller: _searchController,
                  decoration: InputDecoration(
                      prefixIcon: const Icon(
                        Icons.search,
                        color: Colors.grey,
                      ),
                      // iconColor: Colors.grey,
                      suffixIcon: _searchController.text.isNotEmpty
                          ? IconButton(
                              icon: const Icon(
                                Icons.clear,
                                color: Colors.redAccent,
                              ),
                              onPressed: () {
                                setState(() {
                                  _searchController.text = '';
                                });
                              },
                            )
                          : null,
                      hintText: StringManager.searchHint,
                      hintStyle: const TextStyle(color: Colors.grey),
                      border: InputBorder.none),
                ),
              ),
            ),
            if (!Responsive.isMobile(context)) ...[
              _width(context),
              // [102312HG] Makeup
              // ButtonApp(
              //   icon: Icons.filter_alt_outlined,
              //   buttonColor: Color(0xFF7978fb),
              //   borderColor: Color(0xFF7978fb),
              //   iconColor: Colors.white,
              //   title: AppText(
              //     StringManager.filterButton,
              //     AppTextStyles.body14Medium.copyWith(color: Colors.white),
              //   ),
              // ),
              buttonAppFilter(),

              _width(context),

              // [102312HG] Makeup
              // ButtonApp(
              //   icon: Icons.supervised_user_circle_outlined,
              //   buttonColor: Color(0xFFfb5b5a),
              //   borderColor: Color(0xFFfb5b5a),
              //   iconColor: Colors.white,
              //   title: AppText(
              //     StringManager.roleType,
              //     AppTextStyles.body14Medium.copyWith(color: Colors.white),
              //   ),
              // ),
              buttonAppRole(),
              _width(context),

              // [102312HG] Makeup
              // const ButtonApp(
              //   circularIndex: 120,
              //   borderColor: Colors.white12,
              //   iconColor: Colors.white,
              //   icon: Icons.replay_rounded,
              //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              // )
              buttonAppReload(
                circularIndex: 120,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              ),
            ]
          ],
        ),
        _height(context),
        SizedBox(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: TableUserTemplateData(
              templates: widget.templates,
              onDeleteUserTemplate: widget.onDeleteUserTemplate,
              onEditUserTemplate: widget.onEditUserTemplate,
            )),
      ],
    ));
  }
}
