import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tv_menu/common/components/table/table.dart';
import 'package:tv_menu/models/template.dart';
import 'package:tv_menu/models/user_template.dart';

import '../../../../common/constants/constants.dart';
import 'user_template_sources.dart';

class TableUserTemplateData extends StatefulWidget {
  final Function onEditUserTemplate;
  final Function onDeleteUserTemplate;
  final List<UserTemplate> templates;
  const TableUserTemplateData({
    super.key,
    required this.templates,
    required this.onEditUserTemplate,
    required this.onDeleteUserTemplate,
  });

  @override
  TableUserTemplateDataState createState() => TableUserTemplateDataState();
}

class TableUserTemplateDataState extends State<TableUserTemplateData> {
  final int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;
  bool _sortAscending = true;
  int? _sortColumnIndex;
  bool _initialized = false;
  late UserTemplateDataSource _templatesDataSource;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  void sort<T>(
    Comparable<T> Function(UserTemplate d) getField,
    int columnIndex,
    bool ascending,
  ) {
    _templatesDataSource.sort<T>(getField, ascending);
    setState(() {
      _sortColumnIndex = columnIndex;
      _sortAscending = ascending;
    });
  }

  @override
  void dispose() {
    _templatesDataSource.dispose();
    super.dispose();
  }

  List<DataColumn> get _columns {
    return [
      DataColumn(
        label: const Text("ID"),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.id!, columnIndex, ascending),
      ),
      DataColumn(
        label: const Text(StringManager.name),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.title!, columnIndex, ascending),
      ),
      DataColumn(
        label: const Text("User Data"),
        onSort: (columnIndex, ascending) => sort<String>(
            (d) => json.encode(d.userData), columnIndex, ascending),
      ),
      DataColumn(
        label: const Text("Token User Template"),
        onSort: (columnIndex, ascending) => sort<String>(
            (d) => json.encode(d.tokenUserTemplate), columnIndex, ascending),
      ),
      DataColumn(
        label: const Text("Date Created"),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => json.encode(d.created), columnIndex, ascending),
      ),
      DataColumn(
        label: const Text("Expiry Time"),
        onSort: (columnIndex, ascending) => sort<String>(
            (d) => json.encode(d.expiryTime), columnIndex, ascending),
      ),
      DataColumn(
        label: const Text(StringManager.status),
        onSort: (columnIndex, ascending) => sort<String>(
            (d) => json.encode(d.isDeleted), columnIndex, ascending),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    _templatesDataSource = UserTemplateDataSource(context, widget.templates,
        widget.onEditUserTemplate, widget.onDeleteUserTemplate);
    return TableManagement(
      dataSource: _templatesDataSource,
      columns: _columns,
      rowsPerPage: _rowsPerPage,
      sortColumnIndex: _sortColumnIndex,
      sortAscending: _sortAscending,
    );
  }
}
