import 'package:flutter/material.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:tv_menu/common/components/text/app_text.dart';
import 'package:tv_menu/common/components/text/text_styles.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/models/template.dart';
import 'package:tv_menu/models/user_template.dart';
import 'dart:convert';

import '../../../../common/common.dart';

class RestorableUserTemplateSelections extends RestorableProperty<Set<int>> {
  Set<int> _templateSelections = {};

  /// Returns whether or not a template row is selected by index.
  bool isSelected(int index) => _templateSelections.contains(index);

  /// Takes a list of [UserTemplate]s and saves the row indices of selected rows
  /// into a [Set].
  void setUserTemplateSelections(List<UserTemplate> templates) {
    final updatedSet = <int>{};
    for (var i = 0; i < templates.length; i += 1) {
      var template = templates[i];
      if (template.selected) {
        updatedSet.add(i);
      }
    }
    _templateSelections = updatedSet;
    notifyListeners();
  }

  @override
  Set<int> createDefaultValue() => _templateSelections;

  @override
  Set<int> fromPrimitives(Object? data) {
    final selectedItemIndices = data as List<dynamic>;
    _templateSelections = {
      ...selectedItemIndices.map<int>((dynamic id) => id as int),
    };
    return _templateSelections;
  }

  @override
  void initWithValue(Set<int> value) {
    _templateSelections = value;
  }

  @override
  Object toPrimitives() => _templateSelections.toList();
}

// /// Domain model entity
// class UserTemplate {
//   UserTemplate(
//       {required this.id,
//       required this.templateSlug,
//       required this.ratioSetting,
//       required this.isDeleted,
//       required this.widgetSetting});

//   final String id;

//   final String templateSlug;
//   final Map<String, dynamic> ratioSetting;
//   final bool isDeleted;
//   final Map<String, dynamic> widgetSetting;
//   bool selected = false;

//   factory UserTemplate.fromMap(Map<String, dynamic> map) {
//     return UserTemplate(
//         id: map['_id'] ?? "",
//         templateSlug: map['templateSlug'] ?? "",
//         ratioSetting: map['ratioSetting'] ?? "",
//         widgetSetting: map['widgets_setting'] ?? "",
//         isDeleted: map['isDeleted']);
//   }
// }

/// Data source implementing standard Flutter's DataTableSource abstract class
/// which is part of DataTable and PaginatedDataTable synchronous data fecthin API.
/// This class uses static collection of deserts as a data store, projects it into
/// DataRows, keeps track of selected items, provides sprting capability
class UserTemplateDataSource extends DataTableSource {
  UserTemplateDataSource(this.context, this.templates, this.onEditUserTemplate,
      this.onDeleteUserTemplate);
  int _selectedCount = 0;
  final Function onEditUserTemplate;
  final Function onDeleteUserTemplate;
  final BuildContext context;
  List<UserTemplate> templates;
  // Add row tap handlers and show snackbar
  // Override height values for certain rows
  // Color each Row by index's parity
  bool hasZebraStripes = true;

  void sort<T>(
      Comparable<T> Function(UserTemplate d) getField, bool ascending) {
    templates.sort((a, b) {
      final aValue = getField(a);
      final bValue = getField(b);
      return ascending
          ? Comparable.compare(aValue, bValue)
          : Comparable.compare(bValue, aValue);
    });
    notifyListeners();
  }

  void updateSelectedUserTemplates(
      RestorableUserTemplateSelections selectedRows) {
    _selectedCount = 0;
    for (var i = 0; i < templates.length; i += 1) {
      var template = templates[i];
      if (selectedRows.isSelected(i)) {
        template.selected = true;
        _selectedCount += 1;
      } else {
        template.selected = false;
      }
    }
    notifyListeners();
  }

  @override
  DataRow getRow(int index, [Color? color]) {
    assert(index >= 0);
    if (index >= templates.length) throw 'index > _templates.length';
    final template = templates[index];
    return DataRow2.byIndex(
      index: index,
      selected: template.selected,
      color: color != null
          ? MaterialStateProperty.all(color)
          : (hasZebraStripes && index.isEven
              ? MaterialStateProperty.all(AppColors.blueCommon.opacity6)
              : null),
      onSelectChanged: (value) {
        if (template.selected != value) {
          _selectedCount += value! ? 1 : -1;
          assert(_selectedCount >= 0);
          template.selected = value;
          notifyListeners();
        }
      },
      cells: [
        DataCell(SizedBox(
            width: 40,
            child: AppText(
              template.id,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        DataCell(SizedBox(
            width: 80,
            child: AppText(
              template.title,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        DataCell(SizedBox(
            width: 180,
            child: AppText(
              template.userData,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        DataCell(SizedBox(
            width: 180,
            child: AppText(
              template.tokenUserTemplate,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        DataCell(SizedBox(
            width: 100,
            child: AppText(
              template.created,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        DataCell(SizedBox(
            width: 120,
            child: AppText(
              template.expiryTime,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        DataCell(SizedBox(
            width: 60,
            child: AppText(
              !template.isDeleted! ? 'Deleted' : 'Active',
              AppTextStyles.body14Medium.copyWith(
                color: !template.isDeleted! ? AppColors.red : AppColors.green,
              ),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
      ],
    );
  }

  @override
  int get rowCount => templates.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;

  void selectAll(bool? checked) {
    for (final template in templates) {
      template.selected = checked ?? false;
    }
    _selectedCount = (checked ?? false) ? templates.length : 0;
    notifyListeners();
  }
}
