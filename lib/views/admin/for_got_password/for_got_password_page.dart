import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../../common/common.dart';
import '../../../common/components/text_field/text_field_input.dart';
import '../../../routes/route_names.dart';

class ForGotPasswordPage extends StatelessWidget {
  ForGotPasswordPage({super.key});
  final TextEditingController emailController = TextEditingController()
    ..text = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('../../../assets/BG-main.png'),
            fit: BoxFit.cover,
          ),
        ),
        alignment: Alignment.topCenter,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 150,
              ),
              SizedBox(
                width: 300,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AppText(
                        StringManager.forGotPassword,
                        AppTextStyles.title18Bold
                            .copyWith(color: AppColors.white)),
                  ],
                ),
              ),
              _height(50),
              SizedBox(
                width: 300,
                child: AppText(
                  StringManager.forgotPasswordCaption,
                  AppTextStyles.body14Regular
                      .copyWith(color: AppColors.lightGray),
                  textAlign: TextAlign.start,
                ),
              ),
              _height(20),
              TextFieldInput(
                title: StringManager.email,
                autofocus: true,
                onChanged: (value) {},
                controller: emailController, //Tên hàm
                textInputAction: TextInputAction.next,
                width: 300,
                radius: 30,
              ),
              _height(20),
              ButtonApp(
                width: 300,
                height: 50,
                title: AppText(
                  StringManager.confirm,
                  AppTextStyles.button18Bold.copyWith(color: AppColors.white),
                  textAlign: TextAlign.center,
                ),
                buttonColor: AppColors.confirmButton,
                onTap: () {
                  _confirmFunction(context);
                },
              ),
              _height(30),
              SizedBox(
                width: 300,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      width: 5,
                    ),
                    RichText(
                        text: TextSpan(
                            text: StringManager.dontHaveAccount,
                            children: [
                              TextSpan(
                                  text: StringManager.signUp,
                                  style: AppTextStyles.body14Medium
                                      .copyWith(color: AppColors.white),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {}),
                            ],
                            style: AppTextStyles.body14Medium
                                .copyWith(color: AppColors.lightGray))),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _height(double height) {
    return SizedBox(
      height: height,
    );
  }

  void _confirmFunction(BuildContext context) async {
    var result = await showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AppDialog(
            iconWidget: const Icon(
              Icons.task_alt,
              color: AppColors.green,
              size: 80,
            ),
            descriptionWidget: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const AppText(
                    StringManager.congratulations, AppTextStyles.body16Bold),
                _height(10),
                SizedBox(
                  width: 250,
                  child: AppText(
                    StringManager.fotgotCongratulation,
                    AppTextStyles.body14Regular.copyWith(color: AppColors.gray),
                    textAlign: TextAlign.center,
                  ),
                ),
                _height(20),
                SizedBox(
                  width: 250,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ButtonApp(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 10),
                        title: AppText(
                            StringManager.ok,
                            AppTextStyles.body14Medium
                                .copyWith(color: AppColors.white)),
                        buttonColor: AppColors.confirmButton,
                        circularIndex: 20,
                        onTap: () {
                          //TODO: Fix Return CallBack
                          Navigator.pop(context);
                          Navigator.pushNamed(context, Routes.AdminLoginRoute);
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ));
      },
    );
    if (result == null) return;
    if (result) {
      //TODO: Add Create User Event
    }
  }
}
