import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/admin_builders/controllers/builders_controller.dart';
import 'package:tv_menu/bloc/templates/templates_bloc.dart';
import 'package:tv_menu/bloc/templates/templates_event.dart';
import 'package:tv_menu/bloc/user_template/user_template_bloc.dart';
import 'package:tv_menu/bloc/user_template/user_template_event.dart';
import 'package:tv_menu/bloc/users/users_bloc.dart';
import 'package:tv_menu/bloc/users/users_event.dart';
import 'package:tv_menu/controller/admin/template/template_controller.dart';
import 'package:tv_menu/controller/admin/user/user_controller.dart';
import 'package:tv_menu/controller/admin/user_template/user_template_controller.dart';
import 'package:tv_menu/data/enum.dart';
import 'package:tv_menu/admin_builders/views/builder_page.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/views/admin/user_management/user_management_page.dart';
import 'package:tv_menu/widgets/custom_card.dart';

import '../../../common/common.dart';
import '../../../common/constants/constants.dart';
import '../chart/chart_page.dart';
import '../template_management/template_management_page.dart';

class MenuTabNavigation extends StatefulWidget {
  const MenuTabNavigation({super.key});

  @override
  State<MenuTabNavigation> createState() => _MenuTabNavigationState();
}

class _MenuTabNavigationState extends State<MenuTabNavigation> {
  late List<Widget> _destinations;
  // [102320TIN] add tab text style
  static const _tabTextStyle = TextStyle(fontSize: 14);
  int _selectedIndex = 0;
  bool _isExpanded = false;
  Widget _buildNavigationRail() {
    BlocProvider.of<UserBloc>(context).add(UserFetchEvent());
    // [102331TIN] moving logout button to trailing of sidebar
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: NavigationRail(
            //[102312HG] Makeup
            // backgroundColor: Theme.of(context).primaryColor,
            backgroundColor: Color(0xff0c1420), //Background sidebar
            extended: _isExpanded,
            labelType: NavigationRailLabelType.none,
            selectedIndex: _selectedIndex,
            onDestinationSelected: (int index) {
              if (index == 2) {
                BlocProvider.of<UserTemplateBloc>(context)
                    .add(UserTemplateFetchEvent());
              }
              if (index == 1) {
                BlocProvider.of<TemplateBloc>(context)
                    .add(TemplateFetchEvent());
              }
              if (index == 0) {
                BlocProvider.of<UserBloc>(context).add(UserFetchEvent());
              }
              setState(() {
                _selectedIndex = index;
              });
            },
            // [102320TIN] set font size is 14
            destinations: const <NavigationRailDestination>[
              NavigationRailDestination(
                icon: Icon(Icons.supervised_user_circle_outlined),
                selectedIcon: Icon(Icons.supervised_user_circle),
                label: Text(StringManager.titleUserManagement,
                    style: _tabTextStyle),
              ),
              NavigationRailDestination(
                icon: Icon(Icons.padding_outlined),
                selectedIcon: Icon(Icons.padding_rounded),
                label: Text(StringManager.titleTemplateManagement,
                    style: _tabTextStyle),
              ),
              NavigationRailDestination(
                icon: Icon(Icons.verified_user_outlined),
                selectedIcon: Icon(Icons.verified_user),
                label: Text(StringManager.titleUserTemplateManagement),
              ),
              // NavigationRailDestination(
              //   icon: Icon(Icons.build_circle_outlined),
              //   selectedIcon: Icon(Icons.build_circle),
              //   label: Text("Builder Template"),
              // ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(bottom: 24),
          child: TextButton(
            onPressed: () {
              Helpers.clearLocalDb(BOX.app);
              Navigator.pop(context);
            },
            style: TextButton.styleFrom(
              padding: EdgeInsets.symmetric(
                vertical: 24,
                horizontal: 24,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(Icons.logout, color: AppColors.lightGray),
                if (_isExpanded) ...[
                  SizedBox(
                    width: 16,
                  ),
                  AppText(
                    StringManager.logout,
                    AppTextStyles.body14Regular
                        .copyWith(color: AppColors.lightGray),
                  ),
                ]
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _destinations = [
      const UsersController(),
      const TemplatesController(),
      const UserTemplatesController(),
      // [112304TIN] enable builder tab
      // BuilderPage(
      //   template: Template(id: 'id', templateName: 'templateName'),
      // ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //[102312HG] Makeup
      // backgroundColor: Theme.of(context).primaryColor,
      backgroundColor: Color(0xff0c1420),
      appBar: AppBar(
        //[102312HG] Makeup
        // backgroundColor: Theme.of(context).primaryColor,
        backgroundColor: Color(0xff0c1420),

        // [102320TIN] set font size to 24
        //ADDED APP BAR
        title: Text(
          StringManager.dashboard,
          style: AppTextStyles.title24Medium.copyWith(
            color: Colors.white,
          ),
        ),
        elevation: 0.0,
        bottomOpacity: 0.0,
        leading: IconButton(
          icon: const Icon(Icons.menu),
          onPressed: () {
            setState(() {
              _isExpanded = !_isExpanded;
            });
          },
        ),
        // [102331TIN] hide user dropdown
        // actions: [
        //   Row(
        //     children: [
        //       const SizedBox(
        //         width: 20,
        //       ),
        //       const Icon(
        //         Icons.supervised_user_circle,
        //         size: 30,
        //         // [102312HG] Makeup
        //         // color: Colors.blueAccent,
        //         color: Color(0xFF7978fb),
        //       ),
        //       PopUPMenu(
        //         icon: const Icon(
        //           Icons.arrow_drop_down_rounded,
        //           size: 30,
        //           // [102312HG] Makeup
        //           // color: Colors.blueAccent,
        //           color: Color(0xFF7978fb),
        //         ),
        //         // Callback that sets the selected popup menu item.
        //         onSelected: (String i) {
        //           if (i == StringManager.logout) {
        //             Helpers.clearLocalDb(BOX.app);
        //             Navigator.pop(context);
        //           }
        //         },
        //         menuList: <PopupMenuEntry<String>>[
        //           PopupMenuItem(
        //             value: StringManager.setting,
        //             child: ListTile(
        //               leading: const Icon(Icons.settings),
        //               title: AppText(
        //                 StringManager.setting,
        //                 AppTextStyles.body14Regular
        //                     .copyWith(color: AppColors.white),
        //               ),
        //             ),
        //           ),
        //           const PopupMenuDivider(
        //             height: 2,
        //           ),
        //           PopupMenuItem(
        //             value: StringManager.logout,
        //             child: ListTile(
        //               leading: const Icon(Icons.logout),
        //               title: AppText(
        //                 StringManager.logout,
        //                 AppTextStyles.body14Regular
        //                     .copyWith(color: AppColors.white),
        //               ),
        //             ),
        //           ),
        //         ],
        //       ),
        //       const SizedBox(
        //         width: 20,
        //       ),
        //     ],
        //   )
        // ],
      ),
      body: Row(
        children: <Widget>[
          _buildNavigationRail(),
          Expanded(
            child: CustomCard(
              color: Theme.of(context).primaryColor,
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
              ),
              child: _destinations[_selectedIndex],
            ),
          )
        ],
      ),
    );
  }
}
