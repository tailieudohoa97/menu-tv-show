import 'package:flutter/material.dart';

import '../../../helper/textstyle_global.dart';

//Title for:
//add_new_product.dart
//suspend_and_save_cart.dart
//manage_cash.dart
//today_profit.dart
//close_register.dart
//daily_report.dart
//popup_google_map.dart

// Use this
// Title
// const PopupTitleOnly(
//   title: 'Add a new product',
// ),
class PopupTitleOnly extends StatelessWidget {
  final String title;

  const PopupTitleOnly({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        //Title
        Container(
          margin: const EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                width: 1,
                color: Colors.black.withOpacity(0.2),
              ),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: TextStyleGlobal.mediumNameProduct,
              ),

              //Close button
              CloseButton(
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}
