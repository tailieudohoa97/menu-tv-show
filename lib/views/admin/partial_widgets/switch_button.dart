import 'package:flutter/material.dart';

class SwitchButton extends StatefulWidget {
  final Function onChanged;
  const SwitchButton({required this.onChanged});
  @override
  _SwitchButtonState createState() => _SwitchButtonState();
}

class _SwitchButtonState extends State<SwitchButton> {
  bool _value = false;

  @override
  Widget build(BuildContext context) {
    return Switch.adaptive(
      // key: UniqueKey(), //thuộc tính key khiến nút trượt không mượt
      activeColor: Color(0xFF09ADAA),
      value: _value,
      onChanged: (newValue) {
        widget.onChanged(newValue);
        setState(() {
          _value = newValue;
        });
      },
    );
  }
}
