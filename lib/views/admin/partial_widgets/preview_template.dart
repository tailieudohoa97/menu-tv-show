import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';

import 'package:tv_menu/controller/template_controller.dart';

class PreviewTemplate extends StatelessWidget {
  final String idTemplate;
  const PreviewTemplate({super.key, required this.idTemplate});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        // [062301HG] makeup
        // padding: const EdgeInsets.all(20),
        // width: 1420,
        // height: 630,
        // [112301TIN] add close button and use AspectRatio for preview template
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TextButton(
              style: TextButton.styleFrom(
                foregroundColor: Colors.white,
              ),
              onPressed: () => Navigator.pop(context),
              child: Icon(Icons.close),
            ),
            Container(
              height: 630,
              color: Colors.white,
              child: AspectRatio(
                aspectRatio: 16 / 10,
                child: TemplateControllerState(
                  idTemplate: idTemplate,
                  userId: "0",
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
