import 'package:decimal/decimal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

// [052305HG] Add file for capitalizeZ
import 'package:basic_utils/basic_utils.dart';
import 'package:tv_menu/helper/app_setting.dart';
import 'package:tv_menu/repo/repository/template_repository.dart';
import 'package:tv_menu/repo/repository/user_repository.dart';
import 'package:tv_menu/repo/repository/user_template_repository.dart';
import 'package:tv_menu/views/admin/partial_widgets/preview_template.dart';

//[072318HG] Lỗi trùng tên "SearchBar"
//03232YW - Separate search bar to new file
class SearchBarLayout extends StatefulWidget {
  final String searchType;
  final Function? onSelectItem;
  final String? hintText;
  // [102331TIN] add widthOfResultBox
  final double? widthOfResultBox;
  const SearchBarLayout(
      {super.key,
      required this.searchType,
      this.onSelectItem,
      this.hintText,
      this.widthOfResultBox});

  @override
  State<SearchBarLayout> createState() => _SearchBarLayoutState();
}

class _SearchBarLayoutState extends State<SearchBarLayout> {
  TextEditingController _searchText = TextEditingController();
  String errorMessage = "";
  List<dynamic>? _allResults;

  bool checkTextFieldFocus = false;
  int checkSelectItem = 0;
  List<dynamic>? result;
  Future<List?> getFilteredResults() async {
    dynamic query = _searchText.text;
    switch (widget.searchType) {
      case "template":
        _allResults = (await TemplateRepository().searchTemplates(query));
        break;
      case "user":
        _allResults = (await UserRepository().searchUsers(query));
        break;
      case "user_template":
        _allResults = (await TemplateRepository().searchTemplates(query));
        break;
      default:
    }

    return _allResults!;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget _layoutResult(String searchType, int index) {
      if (widget.searchType == "template") {
        return _searchResultTable(result![index]['templateName'],
            result![index]['categoryName'], result![index]['created'], () {
          setState(() {
            _searchText.text = "";
          });
        });
      }
      if (widget.searchType == "user") {
        return _searchUserResultTable(result![index]['username'],
            result![index]['email'], result![index]['subscriptionLevel'], () {
          setState(() {
            _searchText.text = "";
          });
        });
      } else {
        return Text(
          result![index],
          style: const TextStyle(color: Colors.black),
        );
      }
    }

    // [052303HG] get height, working...
    // double _getHeight() {
    //   final resultLength = result!.length;
    //   if (resultLength == 0) {
    //     return 0;
    //   }
    //   return 50 * resultLength.toDouble();
    // }

    // return Column(
    //   children: [
    //     Container(
    //       height: 35,
    //       color: Colors.white,
    //       child: TextField(
    //         //focusNode: _focusNode1,
    //         onTap: () {
    //           setState(() {
    //             checkTextFieldFocus = true;
    //           });
    //         },
    //         controller: _searchText,
    //         onChanged: (value) async {
    //           result = await getFilteredResults();
    //           if (_searchText.text.length < 3) {
    //             setState(() {
    //               errorMessage = "Please enter 3 or more characters";
    //             });
    //           } else {
    //             if (result!.isEmpty || result == null) {
    //               setState(() {
    //                 errorMessage = "No results found";
    //               });
    //             } else {
    //               setState(() {
    //                 errorMessage = "";
    //               });
    //             }
    //           }
    //         },
    //         decoration: InputDecoration(
    //           // hintText: 'Search ItemCart...',
    //           suffixIcon: IconButton(
    //             icon: Icon(
    //               Icons.close,
    //               color: Color(0xFF09ADAA),
    //               size: 16,
    //             ),
    //             onPressed: () {
    //               setState(() {
    //                 _searchText.text = "";
    //               });
    //             },
    //           ),

    //           hintStyle: TextStyle(
    //             color: Colors.grey,
    //             fontWeight: FontWeight.w100,
    //           ),
    //           contentPadding: EdgeInsets.symmetric(vertical: 6, horizontal: 10),
    //           hintText: widget.hintText ?? "",
    //           enabledBorder: OutlineInputBorder(
    //             borderSide: BorderSide(
    //               color: Color.fromRGBO(9, 173, 170, 1),
    //             ),
    //             borderRadius: BorderRadius.all(Radius.circular(5)),
    //           ),
    //           focusedBorder: OutlineInputBorder(
    //             borderSide: BorderSide(
    //               color: Color.fromRGBO(9, 173, 170, 1),
    //             ),
    //             borderRadius: BorderRadius.all(Radius.circular(5)),
    //           ),
    //           border: OutlineInputBorder(),
    //         ),
    //         // clipBehavior: Clip.hardEdge,
    //         cursorColor: Colors.black,
    //         // cursorHeight: 20,
    //         style: const TextStyle(
    //           color: Color.fromARGB(255, 0, 0, 0),
    //           fontSize: 12,
    //           fontWeight: FontWeight.w600, //[052309HG] Makeup
    //         ),
    //       ),
    //     ),
    //     _searchText.text.isNotEmpty && checkTextFieldFocus
    //         ? Container(
    //             padding: const EdgeInsets.symmetric(vertical: 4),
    //             decoration: BoxDecoration(
    //               color: Colors.white,
    //               border: Border.all(
    //                 color: const Color.fromRGBO(9, 173, 170, 1),
    //                 width: 1,
    //               ),
    //             ),
    //             constraints: BoxConstraints(
    //               minHeight: 50,
    //               maxHeight: 900,
    //             ),
    //             height: errorMessage == ""
    //                 // [052304HG] get height, working...
    //                 ? 70 * double.parse(result!.length.toString())
    //                 // ? _getHeight()
    //                 : 40,
    //             child: errorMessage == ""
    //                 ? ListView.builder(
    //                     itemCount: result!.length,
    //                     itemBuilder: (BuildContext context, int index) {
    //                       return ListTile(
    //                           title: _layoutResult(widget.searchType, index));
    //                     },
    //                   )
    //                 : Row(
    //                     children: [
    //                       Padding(
    //                         padding: const EdgeInsets.fromLTRB(10, 6, 10, 6),
    //                         child: Text(
    //                           errorMessage,
    //                           style: const TextStyle(color: Colors.black),
    //                         ),
    //                       ),
    //                     ],
    //                   ),
    //           )
    //         : SizedBox(),
    //   ],
    // );

    // [102331TIN] use MenuAnchor to result
    final List<Widget> resultWidgets = [];
    if (errorMessage == '') {
      for (int i = 0; i < (result?.length ?? 0); i++) {
        resultWidgets.add(MenuItemButton(
            child: SizedBox(
          width: widget.widthOfResultBox,
          child: _layoutResult(widget.searchType, i),
        )));
      }
    } else {
      resultWidgets.add(
        MenuItemButton(
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 6, 10, 6),
                child: Text(
                  errorMessage,
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        ),
      );
    }

    return MenuAnchor(
      alignmentOffset: Offset(0, 16),
      // [112302TIN] makeup
      style: MenuStyle(
        backgroundColor: MaterialStatePropertyAll(Colors.grey.shade800),
        maximumSize: MaterialStatePropertyAll(Size(double.maxFinite, 300)),
        shape: MaterialStatePropertyAll(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
          ),
        ),
        padding: MaterialStatePropertyAll(EdgeInsets.symmetric(vertical: 16)),
      ),
      builder: (context, controller, child) {
        return Container(
          // height: 35,
          // [112302TIN] makeup
          // color: Colors.white,
          // color: Colors.transparent,
          child: TextField(
            //focusNode: _focusNode1,
            onTap: () {
              setState(() {
                checkTextFieldFocus = true;
              });
              if (errorMessage.isNotEmpty || result != null) {
                controller.open();
              }
            },
            controller: _searchText,
            onChanged: (value) async {
              result = await getFilteredResults();
              if (_searchText.text.length < 3) {
                setState(() {
                  errorMessage = "Please enter 3 or more characters";
                });
              } else {
                if (result!.isEmpty || result == null) {
                  setState(() {
                    errorMessage = "No results found";
                  });
                } else {
                  setState(() {
                    errorMessage = "";
                  });
                }
              }
              controller.open();
            },
            decoration: InputDecoration(
                prefixIcon: const Icon(
                  Icons.search,
                  color: Colors.grey,
                ),
                // hintText: 'Search ItemCart...',
                suffixIcon: IconButton(
                  icon: Icon(
                    Icons.close,
                    // [112302TIN] makeup
                    // color: Color(0xFF09ADAA),
                    // size: 16,
                    color: Colors.grey,
                  ),
                  onPressed: () {
                    setState(() {
                      _searchText.text = "";
                    });
                  },
                ),
                hintStyle: TextStyle(
                  color: Colors.grey,
                  // fontWeight: FontWeight.w100,
                ),

                // contentPadding: EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                hintText: widget.hintText ?? "",
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  // [112302TIN] makeup
                  // borderSide: BorderSide(
                  //   color: Color.fromRGBO(9, 173, 170, 1),
                  // ),
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  // [112302TIN] makeup
                  // borderSide: BorderSide(
                  //   color: Color.fromRGBO(9, 173, 170, 1),
                  // ),
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                // border: OutlineInputBorder(),
                // [112302TIN] makeup
                border: InputBorder.none),
            // clipBehavior: Clip.hardEdge,
            // cursorColor: Colors.black,
            cursorColor: Colors.grey,
            // cursorHeight: 20,
            style: const TextStyle(
              // color: Color.fromARGB(255, 0, 0, 0),
              // [112302TIN] makeup
              color: Colors.grey,
              // [112302TIN] makeup
              // fontSize: 12,
              fontSize: 14,
              fontWeight: FontWeight.w600, //[052309HG] Makeup
            ),
          ),
        );
      },
      menuChildren: resultWidgets,
      // children: [
      //   _searchText.text.isNotEmpty && checkTextFieldFocus
      //       ? Container(
      //           padding: const EdgeInsets.symmetric(vertical: 4),
      //           decoration: BoxDecoration(
      //             color: Colors.white,
      //             border: Border.all(
      //               color: const Color.fromRGBO(9, 173, 170, 1),
      //               width: 1,
      //             ),
      //           ),
      //           constraints: BoxConstraints(
      //             minHeight: 50,
      //             maxHeight: 900,
      //           ),
      //           height: errorMessage == ""
      //               // [052304HG] get height, working...
      //               ? 70 * double.parse(result!.length.toString())
      //               // ? _getHeight()
      //               : 40,
      //           child: errorMessage == ""
      //               ? ListView.builder(
      //                   itemCount: result!.length,
      //                   itemBuilder: (BuildContext context, int index) {
      //                     return ListTile(
      //                         title: _layoutResult(widget.searchType, index));
      //                   },
      //                 )
      //               : Row(
      //                   children: [
      //                     Padding(
      //                       padding: const EdgeInsets.fromLTRB(10, 6, 10, 6),
      //                       child: Text(
      //                         errorMessage,
      //                         style: const TextStyle(color: Colors.black),
      //                       ),
      //                     ),
      //                   ],
      //                 ),
      //         )
      //       : SizedBox(),
      // ],
    );
  }

  Widget _searchResultTable(String templateName, String templateLevel,
      String created, Function onPress) {
    return ElevatedButton(
      onPressed: () {},
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(Colors.white),
        overlayColor:
            MaterialStateProperty.all(Color(0xFFfafafa)), // [062329HG] remake
        elevation: MaterialStateProperty.all(0),
        enableFeedback: true,
        padding: MaterialStateProperty.all(EdgeInsets.zero),
        foregroundColor: MaterialStateProperty.all(Colors.black),
      ),
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Colors.grey,
              width: 1.0,
            ),
          ),
          // [112302TIN] makeup
          color: Colors.grey.shade800,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                //Thumbnail

                // [052305HG] Update code
                //Product's name and ID
                Text.rich(
                  TextSpan(
                    text: StringUtils.capitalize(
                        allWords: true, '$templateName ———'),
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      // [112302TIN] makeup

                      color: Colors.white,
                    ),
                    children: [
                      TextSpan(
                        text: " #$templateLevel",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF09ADAA),
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(width: 10),
              ],
            ),
            Row(
              children: [
                //Price
                Text(
                  created,
                  style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.w600,
                    // [112302TIN] makeup
                    color: Colors.white,
                  ),
                ),
                SizedBox(width: 10),

                //'Use this customer profile' button
                ElevatedButton(
                  onPressed: () {
                    onPress();
                  },

                  // [062329HG] remake
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all(Color(0xFF09adaa)),
                    backgroundColor:
                        MaterialStateProperty.all(Color(0x4B09ADAA)),
                    overlayColor: MaterialStateProperty.all(Color(0x219E9E9E)),
                    elevation: MaterialStateProperty.all(0),
                    enableFeedback: true,
                  ),
                  child: Text(
                    'View demo template'.toUpperCase(),
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 13,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _searchUserResultTable(String templateName, String templateLevel,
      String created, Function onPress) {
    return ElevatedButton(
      onPressed: () {},
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all(Colors.white),
        overlayColor:
            MaterialStateProperty.all(Color(0xFFfafafa)), // [062329HG] remake
        elevation: MaterialStateProperty.all(0),
        enableFeedback: true,
        padding: MaterialStateProperty.all(EdgeInsets.zero),
        foregroundColor: MaterialStateProperty.all(Colors.black),
      ),
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Colors.grey,
              width: 1.0,
            ),
          ),
          // [112302TIN] makeup
          color: Colors.grey.shade800,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                //Thumbnail

                // [052305HG] Update code
                //Product's name and ID
                Text.rich(
                  TextSpan(
                    text: StringUtils.capitalize(
                        allWords: true, '$templateName ———'),
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      // [112302TIN] makeup

                      color: Colors.white,
                    ),
                    children: [
                      TextSpan(
                        text: " #$templateLevel",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF09ADAA),
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(width: 10),
              ],
            ),
            Row(
              children: [
                //Price
                Text(
                  created,
                  style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.w600,
                    // [112302TIN] makeup
                    color: Colors.white,
                  ),
                ),
                SizedBox(width: 10),

                //'Use this customer profile' button
                ElevatedButton(
                  onPressed: () {
                    onPress();
                  },

                  // [062329HG] remake
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all(Color(0xFF09adaa)),
                    backgroundColor:
                        MaterialStateProperty.all(Color(0x4B09ADAA)),
                    overlayColor: MaterialStateProperty.all(Color(0x219E9E9E)),
                    elevation: MaterialStateProperty.all(0),
                    enableFeedback: true,
                  ),
                  child: Text(
                    'View detail user'.toUpperCase(),
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 13,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
