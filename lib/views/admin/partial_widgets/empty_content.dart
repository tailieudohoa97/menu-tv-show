import 'package:flutter/material.dart';

Widget emptyContent([onBack]) {
  return Column(
    children: [
      IconButton(
        onPressed: () {
          onBack("Back");
        },
        icon: const Icon(
          Icons.arrow_back_ios_new_outlined,
          color: Colors.white,
        ),
        tooltip: "Back",
        mouseCursor: SystemMouseCursors.click,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Text(
            'The Category is empty',
            style: TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.bold,
            ),
          )
        ],
      ),
    ],
  );
}
