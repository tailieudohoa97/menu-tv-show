import 'package:flutter/material.dart';

//Remove button
class RemoveButton extends StatefulWidget {
  final Function handleOnSelect;
  const RemoveButton({super.key, required this.handleOnSelect});

  @override
  State<RemoveButton> createState() => _RemoveButtonState();
}

class _RemoveButtonState extends State<RemoveButton> {
  bool hover = true;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 50,
      height: 50,
      child: MouseRegion(
        cursor: SystemMouseCursors.click,
        onHover: (event) {
          setState(() {
            hover = true;
          });
        },
        onExit: ((event) {
          setState(() {
            hover = false;
          });
        }),
        child: ElevatedButton(
          onPressed: () => widget.handleOnSelect(),
          style: TextButton.styleFrom(
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0),
            ),
            padding: const EdgeInsets.all(0),
            foregroundColor: (hover == false)
                ? const Color(0x59000000)
                : const Color(0xFF000000),
            backgroundColor: const Color(0x0009ADAA),
          ),
          child: const Icon(
            Icons.close,
            size: 15,
          ),
        ),
      ),
    );
  }
}
