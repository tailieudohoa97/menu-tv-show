import 'package:flutter/material.dart';
// [052305HG] Add file for capitalize
import 'package:basic_utils/basic_utils.dart';

//Button For:
//add_new_product.dart
//apply_coupon.dart
//suspend_and_save_cart.dart
//add_shipping_address.dart
//print_receipt.dart
//close_register
//screen_login.dart
//customer_tab_content.dart
//confirm_pin.dart
//end_of_day.dart

//Use this
// SaveButton(
//   title: 'Create Product',
//   colorBg: Color(0xFF09ADAA),
//   colorText: Colors.white,
//   onPressed: (){},
//   //margin: true,
// )

class SaveButton extends StatelessWidget {
  final String title;
  final Color colorBg;
  final Color colorText;
  final VoidCallback onPressed;
  final bool margin;

  const SaveButton({
    Key? key,
    required this.title,
    required this.colorBg,
    required this.colorText,
    required this.onPressed,
    this.margin = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin ? EdgeInsets.only(top: 0) : EdgeInsets.only(top: 30),
      alignment: Alignment.center,
      child: Row(
        children: [
          Expanded(
            child: SizedBox(
              height: 40,
              width: double.infinity,
              child: ElevatedButton(
                onPressed: onPressed,
                style: ElevatedButton.styleFrom(
                  elevation: 0,
                  backgroundColor: colorBg,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5),
                  ),
                  foregroundColor: colorText,
                ),
                child: Text(
                  StringUtils.capitalize(allWords: true, title),
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 14,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
