import 'package:flutter/material.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

// Add file for capitalize
import 'package:basic_utils/basic_utils.dart';
import 'package:tv_menu/helper/textstyle_global.dart';

//Document:
//https://pub.dev/packages/multi_select_flutter

class MultiSelect extends StatefulWidget {
  @override
  _MultiSelectState createState() => _MultiSelectState();
}

class _MultiSelectState extends State<MultiSelect> {
  var _selectedshops = [];
  final _shopList = [
    'Apple',
    'Banana',
    'Cherry',
    'Durian',
    'Elderberry',
    'Fig',
    'Grapes',
    'Honeydew Melon',
    'Imbe',
    'Jackshop',
    'Kiwi',
    'Lemon',
    'Mango',
    'Nectarine',
    'Orange',
    'Pineapple',
    'Quince',
    'Raspberry',
    'Strawberry',
    'Tangerine',
    'Ugli shop',
    'Vanilla Bean',
    'Watermelon',
    'Xigua Melon',
    'Yellow Passion shop',
    'Zucchini',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20, left: 10, right: 10, top: 10),
      decoration: BoxDecoration(
        color: Colors.grey[100],
        borderRadius: BorderRadius.circular(10),
      ),
      clipBehavior: Clip.hardEdge,
      child: MultiSelectDialogField(
        autovalidateMode: AutovalidateMode.always,

        //NÚT MỞ POPUP
        buttonIcon: Icon(
          Icons.storefront_outlined,
          color: Colors.black,
        ), //Icon bên phải
        buttonText: Text(
          StringUtils.capitalize(allWords: true, "Select shops: "),
          style: TextStyleGlobal.mediumNameProduct,
        ), //Tiêu đề nút
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.grey[100],
        ),

        //LAYOUT CỦA POPUP
        title: Text(StringUtils.capitalize(allWords: true, "Select shops")),
        barrierColor: Colors.black45, //Màu BG overlay, mặc định là trong suốt

        //style của font item chưa được chọn trong popup
        // itemsTextStyle: TextStyle(
        //   color: Colors.white,
        // ),
        //BG của item chưa được chọn trong popup
        // unselectedColor: Color.fromARGB(103, 0, 0, 0),

        //Màu của item đã được chọn trong và ngoài popup
        selectedColor: Color(0xFF09ADAA),
        //Style của item đã được chọn trong popup
        selectedItemsTextStyle: TextStyle(
          color: Colors.white,
        ),

        //Sắp xếp các mục được chọn thành 1 nhóm, mặc định là false
        separateSelectedItems: true,

        //THANH SEARCH TRONG POPUP
        searchable: true, //bật chức năng search

        //
        items: _shopList
            .map((shop) => MultiSelectItem<String>(shop, shop))
            .toList(),
        initialValue: _selectedshops.toList(),
        onConfirm: (values) {
          setState(() {
            _selectedshops = values.cast<String>().toList();
          });
        },

        listType: MultiSelectListType.CHIP, //Show dạng row
        chipDisplay: MultiSelectChipDisplay(
          //scroll: true, //Xuất hiện thanh cuộn khi tràn dòng, False thì sẽ xuống dòng
          //scrollBar: HorizontalScrollBar(isAlwaysShown: true), //Xuất hiện thanh cuộn mới cuộn được

          //Màu nền
          chipColor: Color(0xFF09ADAA),

          //Style của item đã được chọn trong popup
          textStyle: TextStyle(
            color: Colors.white,
          ),

          items: _selectedshops.map((e) => MultiSelectItem(e, e)).toList(),
          icon: Icon(
            Icons.close,
            size: 10,
            color: Colors.white,
          ),
          onTap: (item) {
            setState(() {
              _selectedshops.remove(item);
            });
          },
        ),
      ),
    );
  }
}
