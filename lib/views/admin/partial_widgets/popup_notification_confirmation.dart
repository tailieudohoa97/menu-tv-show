import 'package:flutter/material.dart';
import 'package:tv_menu/views/admin/partial_widgets/save_button.dart';

// Use this code
// onPressed: () => {
//   showDialog(
//     context: context,
//     builder: (BuildContext context) {
//       return PopupNotificationConfirmation(
//         content: "Are you sure you want to delete?",
//         yes: "Yes",
//         no: "No",
//       );
//     },
//   ).then((value) {
//     if (value == true) {
//       // Người dùng đã nhấp vào "Có"
//       // User clicked "Yes"
//     } else {
//       // Người dùng đã nhấp vào "Không" hoặc nhấp bên ngoài hộp thoại
//       // User clicked "No" or clicked outside the dialog
//     }
//   }),
// },

//[032324HG] Create popup notification confirmation
class PopupNotificationConfirmation extends StatelessWidget {
  final String content;
  final String yes;
  final String no;

  const PopupNotificationConfirmation({
    Key? key,
    required this.content,
    required this.yes,
    required this.no,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
        side: BorderSide(
          width: 2,
          color: Color(0xFFfaebcc),
        ),
      ),
      contentPadding: EdgeInsets.all(20),
      content: Text(content),
      contentTextStyle: TextStyle(color: Color(0xFF8a6d3b)),
      backgroundColor: Color(0xFFfcf8e3),
      actions: <Widget>[
        Container(
          padding: EdgeInsets.all(15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              //Button Yes
              IntrinsicWidth(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: SaveButton(
                    title: yes,
                    colorBg: Color(0xFFD8D8D8),
                    colorText: Colors.black45,
                    onPressed: () {
                      Navigator.of(context).pop(true);
                    },
                    margin: true,
                  ),
                ),
              ),

              //Button No
              IntrinsicWidth(
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: SaveButton(
                    title: no,
                    colorBg: Color(0xFF8a6d3b),
                    colorText: Color(0xFFfcf8e3),
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                    margin: true,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
