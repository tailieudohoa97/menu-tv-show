// import 'package:data_table_2/data_table_2.dart';
// import 'package:equatable/equatable.dart';
// import 'package:flutter/material.dart';
// import 'package:tv_menu/common/common.dart';

// class CellMetadata {
//   String cellName;
//   CellType cellType;

//   CellMetadata({this.cellName = '', this.cellType = CellType.text});
// }

// enum CellType { text }

// // [102323TIN] create this class
// class DataSources<Data extends Equatable> extends DataTableSource {
//   final List<Data> datas;
//   final List<CellMetadata> fieldsMetadata;
//   bool hasZebraStripes = true;

//   DataSources({required this.datas, required this.fieldsMetadata});

//   DataCell _buildTextCell(content) {
//     return DataCell(
//       SizedBox(
//         width: 120,
//         child: AppText(
//           content,
//           AppTextStyles.body14Medium,
//           maxLines: 1,
//           overflow: TextOverflow.ellipsis,
//           color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
//         ),
//       ),
//     );
//   }

//   @override
//   DataRow getRow(int index, [Color? color]) {
//     assert(index >= 0);
//     if (index >= datas.length) throw 'index > _datas.length';
//     final data = datas[index];
//     final List<DataCell> dataCells = [];

//     for (CellMetadata cellMetadata in fieldsMetadata) {
//       if (cellMetadata.cellName.isNotEmpty) {
//       switch (cellMetadata.cellType) {
//         case CellType.text:
//           dataCells.add(
//             _buildTextCell()
//           )
//       }
//       }
//     }

//     return DataRow2.byIndex(
//       index: index,
//       // selected: user.selected,
//       color: color != null
//           ? MaterialStateProperty.all(color)
//           : (hasZebraStripes && index.isEven
//               // [102320TIN] change color of even rows
//               ? MaterialStatePropertyAll(Colors.white.withOpacity(.01))
//               // ? MaterialStateProperty.all(AppColors.blueCommon.opacity6)
//               : null),
//       cells: [
//         DataCell(SizedBox(
//             width: 120,
//             child: AppText(
//               user.userName,
//               AppTextStyles.body14Medium,
//               maxLines: 1,
//               overflow: TextOverflow.ellipsis,
//               color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
//             ))),
//         DataCell(SizedBox(
//             width: 120,
//             child: AppText(
//               user.passWord,
//               AppTextStyles.body14Medium,
//               maxLines: 1,
//               overflow: TextOverflow.ellipsis,
//               color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
//             ))),
//         DataCell(SizedBox(
//             width: 120,
//             child: AppText(
//               user.subscriptionLevel,
//               AppTextStyles.body14Medium,
//               maxLines: 1,
//               overflow: TextOverflow.ellipsis,
//               color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
//             ))),
//         DataCell(SizedBox(
//             width: 200,
//             child: AppText(
//               user.email,
//               AppTextStyles.body14Medium,
//               maxLines: 2,
//               overflow: TextOverflow.ellipsis,
//               color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
//             ))),
//         DataCell(Row(
//           children: [
//             IconButton(
//                 tooltip: "Detail", //[102312HG] Makeup
//                 // [102320TIN] change color
//                 color: Colors.blue,
//                 // color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
//                 onPressed: () {
//                   Helpers.debugLog('See this user', user.id);
//                 },
//                 icon: const Icon(Icons.remove_red_eye_outlined)),
//             IconButton(
//                 tooltip: "Edit", //[102312HG] Makeup
//                 color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
//                 onPressed: () async {
//                   var result = await showDialog<bool>(
//                     context: context,
//                     barrierDismissible: false,
//                     builder: (BuildContext context) {
//                       return EditUserDialog(
//                         title: StringManager.editUserTitle,
//                         user: user,
//                         onEditUser: (user) => onEditUser(user),
//                       );
//                     },
//                   );
//                   if (result == null) return;
//                   if (result) {
//                     //TODO: Add Create User Event
//                   }
//                 },
//                 icon: const Icon(Icons.edit)),
//             IconButton(
//                 tooltip: "Delete", //[102312HG] Makeup
//                 color: Colors.redAccent,
//                 onPressed: () async {
//                   var result = await showDialog<bool>(
//                     context: context,
//                     barrierDismissible: true,
//                     builder: (BuildContext context) {
//                       return AppDialog(
//                           iconWidget: const Icon(
//                             Icons.delete_forever,
//                             color: AppColors.red,
//                             size: 60,
//                           ),
//                           descriptionWidget: Column(
//                             children: [
//                               const AppText(StringManager.confirmDeleteUserDesc,
//                                   AppTextStyles.body16Regular),
//                               AppText(
//                                   user.userName,
//                                   AppTextStyles.button18Bold
//                                       .copyWith(color: AppColors.red)),
//                               const SizedBox(
//                                 height: 20,
//                               ),
//                               SizedBox(
//                                 width: 250,
//                                 child: Row(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   children: <Widget>[
//                                     ButtonApp(
//                                       padding: const EdgeInsets.symmetric(
//                                           horizontal: 25, vertical: 10),
//                                       title: AppText(
//                                           StringManager.cancel,
//                                           AppTextStyles.body14Medium.copyWith(
//                                               color: AppColors.white)),
//                                       buttonColor: AppColors.cancelButton,
//                                       circularIndex: 20,
//                                       onTap: () {
//                                         //TODO: Fix Return CallBack

//                                         Navigator.pop(context);
//                                       },
//                                     ),
//                                     const SizedBox(
//                                       width: 15,
//                                     ),
//                                     ButtonApp(
//                                       padding: const EdgeInsets.symmetric(
//                                           horizontal: 20, vertical: 10),
//                                       title: AppText(
//                                           StringManager.confirm,
//                                           AppTextStyles.body14Medium.copyWith(
//                                               color: AppColors.white)),
//                                       buttonColor: AppColors.confirmButton,
//                                       circularIndex: 20,
//                                       onTap: () {
//                                         //TODO: Fix Return CallBack
//                                         onDeleteUser(user.id);
//                                         Navigator.pop(context);
//                                       },
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             ],
//                           ));
//                     },
//                   );
//                   if (result == null) return;
//                   if (result) {
//                     //TODO: Add Create User Event
//                   }
//                 },
//                 icon: const Icon(Icons.delete)),
//           ],
//         ))
//       ],
//     );
//   }

//   void sort<T>(Comparable<T> Function(Data d) getField, bool ascending) {
//     datas.sort((a, b) {
//       final aValue = getField(a);
//       final bValue = getField(b);
//       return ascending
//           ? Comparable.compare(aValue, bValue)
//           : Comparable.compare(bValue, aValue);
//     });
//     notifyListeners();
//   }

//   @override
//   int get rowCount => datas.length;

//   @override
//   bool get isRowCountApproximate => false;

//   @override
//   int get selectedRowCount => 0;
// }
