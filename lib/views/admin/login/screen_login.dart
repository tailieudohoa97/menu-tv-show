// ignore_for_file: file_names, avoid_print, duplicate_ignore

import 'dart:async';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
// [032321HG] Add file

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tv_menu/services/template_service.dart';

import '../../../common/common.dart';
import '../../../common/components/text_field/text_field_input.dart';
import '../../../cubit/worker/worker_cubit.dart';
import '../../../cubit/worker/worker_state.dart';
import '../../../routes/route_names.dart';

//Screen Start
class ScreenLogin extends StatefulWidget {
  final String message;
  final dynamic hintLogin;
  const ScreenLogin({required this.message, super.key, this.hintLogin});

  @override
  State<ScreenLogin> createState() => _ScreenLoginState();
}

class _ScreenLoginState extends State<ScreenLogin> {
  String errorMessage = '';
  @override
  Widget build(BuildContext context) {
    if (widget.message != '') {
      setState(() {
        errorMessage = widget.message;
      });
    }
    //Helpers.disableBrowserBackButton();
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return Material(
      child: Container(
        width: screenWidth,
        height: screenHeight,
        decoration: const BoxDecoration(
          image: DecorationImage(
            //[102311HG] Makeup
            // image: AssetImage('../../../assets/Background-login.png'),
            // image: NetworkImage(
            //     'https://foodiemenu.co/wp-content/uploads/2023/10/Background-login.png'),
            image: NetworkImage(
                'https://foodiemenu.co/wp-content/uploads/2023/11/Background-login.webp'),
            fit: BoxFit.cover,
          ),
        ),
        alignment: Alignment.center,
        child: Container(
          padding: const EdgeInsets.all(50),
          color: Colors.black.withOpacity(0.4),
          alignment: Alignment.center,
          // constraints: const BoxConstraints(
          //   minWidth: 400,
          //   maxWidth: 450,
          // ),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  //Title Page
                  AppText(
                    StringManager.loginTitle,
                    // style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    AppTextStyles.title24Regular
                        .copyWith(color: AppColors.white),
                  ),
                  if (errorMessage != '')
                    _showErrorMessage(errorMessage)
                  else
                    const SizedBox(
                      height: 10,
                    ),
                  UserEmail(
                    hintLogin: widget.hintLogin,
                    handleOnChange: (value) {
                      if (value == 'RequiredUsernameEmailPassword') {
                        setState(() {
                          errorMessage = 'Username or password is required';
                        });
                      } else if (value == 'RequiredPassword') {
                        setState(() {
                          errorMessage = 'Password is required';
                        });
                      } else if (value.toString().length < 6 &&
                              value.toString().isNotEmpty ||
                          value.toString().length > 25) {
                        setState(() {
                          errorMessage = 'Username or is out of range';
                        });
                      } else if (!isValidUsernameOrEmail(value) &&
                          value.toString().isNotEmpty) {
                        setState(() {
                          errorMessage = 'Username has invalid character';
                        });
                      } else if (value == 'WrongLogin') {
                        setState(() {
                          errorMessage = 'Username or password is invalid';
                        });
                      } else {
                        setState(() {
                          errorMessage = '';
                        });
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _showErrorMessage(String message) {
    //[102311HG] Makeup
    return Container(
      width: 300,
      decoration: BoxDecoration(
        color: Color(0xfff2dede),
        border: Border.all(
          width: 1,
          color: Color(0xffebccd1),
        ),
        borderRadius: BorderRadius.circular(50),
      ),
      padding: const EdgeInsets.all(10.0),
      margin: const EdgeInsets.only(bottom: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.warning_rounded,
            size: 18,
            color: Color(0xFFa94442),
          ),
          SizedBox(width: 10),
          AppText(
            message,
            AppTextStyles.body16Regular.copyWith(
              color: Color(0xFFa94442),
              fontSize: 14,
            ),
          )
        ],
      ),
    );
  }
}

bool isValidUsernameOrEmail(String input) {
  // Regular expression for username validation
  final usernameRegex = RegExp(
    r'^[a-zA-Z0-9](_(?!(\.|_))|\.(?!(_|\.))|[a-zA-Z0-9]){4,}[a-zA-Z0-9]$',
  );

  // Regular expression for email validation
  final emailRegex = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');

  // Combine the two regex patterns into a single pattern
  final combinedRegex =
      RegExp('${usernameRegex.pattern}|${emailRegex.pattern}');

  return combinedRegex.hasMatch(input);
}

class UserEmail extends StatefulWidget {
  final dynamic hintLogin;
  final Function handleOnChange;
  const UserEmail({
    required this.handleOnChange,
    super.key,
    this.hintLogin,
  });

  @override
  State<UserEmail> createState() => _UserEmailState();
}

class _UserEmailState extends State<UserEmail> {
  bool _rememberMe = false;
  //[072310HG]
  //Cho phép button nhận phím Enter
  //Enable button on Enter key press
  void _handleSubmitted(BuildContext context) {
    final username = nameController.text;
    final password = passwordController.text;
    if (username.isEmpty && password.isEmpty) {
      widget.handleOnChange('RequiredUsernameEmailPassword');
    } else if (password.isEmpty) {
      widget.handleOnChange('RequiredPassword');
    } else {
      print(username);
      context.read<WorkerCubit>().login(username, password);
    }
  }
  //[072310HG] End update

  //230316LH Add default login account
  TextEditingController nameController = TextEditingController()..text = '';

  TextEditingController passwordController = TextEditingController()..text = '';

  final bool _isChecked = false;

  // Tạo các biến để kiểm soát việc hiển thị mật khẩu
  bool showPassword = false;
  String drowDownMenuValue = StringManager.user;
  @override
  void initState() {
    super.initState();
    // [102331TIN] auto fill account for test
    nameController.text = 'ywelmai';
    passwordController.text = '123456';
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: SingleChildScrollView(
        child: Column(
          children: [
            //USER
            // [09202310SON] Update UI Username Field
            TextFieldInput(
              title: StringManager.userNameLogin,
              autofocus: true,
              onChanged: (value) => widget.handleOnChange(value),
              controller: nameController, //Tên hàm
              textInputAction: TextInputAction.next,
              width: 300,
              radius: 30,
            ),
            const SizedBox(
              height: 20,
            ),

            //PASSWORD
            // [09202310SON] Update UI Password Field
            TextFieldInput(
              title: StringManager.password,
              controller: passwordController, //Tên hàm
              textInputAction: TextInputAction.next,
              obscureText: !showPassword,

              suffixIcon: IconButton(
                icon: Icon(
                  showPassword ? Icons.visibility : Icons.visibility_off,
                  color: Colors.white, //[102311HG] Makeup
                  size: 18,
                ),
                onPressed: () {
                  setState(() {
                    showPassword = !showPassword;
                  });
                },
              ),

              width: 300,
              radius: 30,
            ),
            const SizedBox(
              height: 5,
            ),
            SizedBox(
              width: 300,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Checkbox(
                        value: _rememberMe,
                        onChanged: (value) {
                          setState(() {
                            _rememberMe = value!;
                          });
                        },
                        activeColor: AppColors.confirmButton,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        side: MaterialStateBorderSide.resolveWith(
                          (states) => const BorderSide(
                              width: 1, color: AppColors.white),
                        ),
                      ),
                      AppText(
                        StringManager.rememberMe,
                        AppTextStyles.body14Regular
                            .copyWith(color: Color(0xFFFFFFFF)),
                      ),
                    ],
                  ),
                ],
              ),
            ),

            //LOGIN BUTTON
            const SizedBox(
              height: 20,
            ),
            BlocProvider<WorkerCubit>(
              create: (context) => WorkerCubit(),
              child: _cubitLogin(),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 300,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const SizedBox(
                        width: 5,
                      ),
                      RichText(
                        text: TextSpan(
                            text: StringManager.forGotPassword,
                            style: AppTextStyles.body14Medium
                                .copyWith(color: AppColors.white),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () => Navigator.pushNamed(
                                  context, Routes.forGotPasswordRouter)),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _loginCTA(Function handleSubmitted, String message) {
    return Column(
      children: [
        if (message != '')
          //[102311HG] Makeup
          Container(
            width: 300,
            decoration: BoxDecoration(
              color: Color(0xfff2dede),
              border: Border.all(
                width: 1,
                color: Color(0xffebccd1),
              ),
              borderRadius: BorderRadius.circular(50),
            ),
            padding: const EdgeInsets.all(10.0),
            margin: const EdgeInsets.only(bottom: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Icons.warning_rounded,
                  size: 18,
                  color: Color(0xFFa94442),
                ),
                SizedBox(width: 10),
                Text(
                  message,
                  style: const TextStyle(
                    color: Color(0xFFa94442),
                    fontSize: 14,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          )
        else
          const SizedBox(),
        ButtonApp(
          width: 300,
          height: 50,
          title: AppText(
            StringManager.signin,
            AppTextStyles.button18Medium
                .copyWith(color: Color(0xFF000000)), //[102311HG] Makeup
            textAlign: TextAlign.center,
          ),
          buttonColor: Color(0xFFfbceb5), //[102311HG] Makeup

          //[072310HG]
          //Cho phép button nhận phím Enter
          //Enable button on Enter key press
          // onPressed: () async {
          //   var username = nameController.text,
          //       password = passwordController.text;
          //   if (username.isEmpty && password.isEmpty) {
          //     widget.handleOnChange("RequiredUsernameEmailPassword");
          //   } else if (password.isEmpty) {
          //     widget.handleOnChange("RequiredPassword");
          //   } else {
          //     // Worker? worker = await RepoRequest()
          //     //     .fetchWorker(username, password) as Worker;

          //     // if (worker.nonce != null) {
          //     //   print(worker);
          //     //   print(worker.nonce);
          //     //   Navigator.pushNamed(context, Routes.HomeScreenRoute);
          //     // } else {
          //     //   widget.handleOnChange("WrongLogin");
          //     // }
          //     _reqWorker();
          //   }
          // },
          onTap: () {
            handleSubmitted();
          },
        ),
      ],
    );
  }

  Widget _cubitLogin() {
    return BlocConsumer<WorkerCubit, WorkerState>(
      listener: buildCubitListener,
      builder: (context, state) {
        if (state is InitialState) {
          return _loginCTA(() => _handleSubmitted(context), '');
        }
        if (state is LoadingState) {
          return const Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Center(
                child: CircularProgressIndicator(),
              ),
            ],
          );
        } else if (state is ErrorState) {
          return _loginCTA(() => _handleSubmitted(context), state.message);
        } else if (state is LoadedState) {
          return _loginCTA(() => _handleSubmitted(context), '');
        } else {
          return Container();
        }
      },
    );
  }

  void buildCubitListener(context, state) {
    if (state is LoadedState) {
      Navigator.pushNamed(context, Routes.AdminDashboardRoute);
    }
  }
}
