import 'dart:convert';

import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:tv_menu/common/common.dart';
import 'package:tv_menu/common/components/table/table.dart';
import 'package:tv_menu/models/template.dart';

import '../../../../common/constants/constants.dart';
import 'template_sources.dart';

class TableTemplateData extends StatefulWidget {
  final Function onEditTemplate;
  final Function onDeleteTemplate;
  final Function onChangeStatusTemplate;
  final List<Template> templates;
  const TableTemplateData({
    super.key,
    required this.templates,
    required this.onEditTemplate,
    required this.onDeleteTemplate,
    required this.onChangeStatusTemplate,
  });

  @override
  TableTemplateDataState createState() => TableTemplateDataState();
}

class TableTemplateDataState extends State<TableTemplateData> {
  final int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;
  bool _sortAscending = true;
  int? _sortColumnIndex;
  bool _initialized = false;
  late TemplateDataSource _templatesDataSource;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  void sort<T>(
    Comparable<T> Function(Template d) getField,
    int columnIndex,
    bool ascending,
  ) {
    _templatesDataSource.sort<T>(getField, ascending);
    setState(() {
      _sortColumnIndex = columnIndex;
      _sortAscending = ascending;
    });
  }

  @override
  void dispose() {
    _templatesDataSource.dispose();
    super.dispose();
  }

  // [102324TIN] set size for columns to avoid overflowing table width
  // [102320TIN] set style for header table is 14bold
  List<DataColumn2> get _columns {
    return [
      DataColumn2(
        // [112302TIN] set fixed with for column
        fixedWidth: 32,
        label: Text(
          "ID",
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.id!, columnIndex, ascending),
        size: ColumnSize.S,
      ),
      DataColumn2(
        label: Text(
          StringManager.name,
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.templateName, columnIndex, ascending),
        size: ColumnSize.L,
      ),
      DataColumn2(
        label: Text(
          StringManager.subscriptionLevel,
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.subscriptionLevel!, columnIndex, ascending),
        size: ColumnSize.M,
      ),
      DataColumn2(
        label: Text(
          StringManager.templateSlug,
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
        onSort: (columnIndex, ascending) => sort<String>(
            (d) => json.encode(d.templateSlug), columnIndex, ascending),
        size: ColumnSize.M,
      ),
      DataColumn2(
        label: Text(
          StringManager.category,
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
          overflow: TextOverflow.ellipsis,
          maxLines: 1,
        ),
        onSort: (columnIndex, ascending) => sort<String>(
            (d) => json.encode(d.categoryId), columnIndex, ascending),
        size: ColumnSize.M,
      ),
      DataColumn2(
        label: Text(
          StringManager.ratios,
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
        onSort: (columnIndex, ascending) => sort<String>(
            (d) => json.encode(d.ratioSetting), columnIndex, ascending),
        size: ColumnSize.M,
      ),
      // [112307TIN] hide json columns
      // DataColumn2(
      //   label: Text(
      //     StringManager.ratioSetting,
      //     style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
      //   ),
      //   onSort: (columnIndex, ascending) => sort<String>(
      //       (d) => json.encode(d.ratioSetting), columnIndex, ascending),
      //   size: ColumnSize.M,
      // ),
      // DataColumn2(
      //   label: Text(
      //     StringManager.widgetSetting,
      //     style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
      //   ),
      //   onSort: (columnIndex, ascending) => sort<String>(
      //       (d) => json.encode(d.widgetSetting), columnIndex, ascending),
      //   size: ColumnSize.M,
      // ),
      // DataColumn2(
      //   label: Text(
      //     StringManager.dataSetting,
      //     style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
      //   ),
      //   onSort: (columnIndex, ascending) => sort<String>(
      //       (d) => json.encode(d.dataSetting), columnIndex, ascending),
      //   // [102331TIN] edit size of column

      //   size: ColumnSize.M,
      // ),
      DataColumn2(
        label: Text(
          StringManager.created,
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
        onSort: (columnIndex, ascending) => sort<String>(
            (d) => json.encode(d.dataSetting), columnIndex, ascending),
        // [102331TIN] edit size of column

        size: ColumnSize.M,
      ),
      DataColumn2(
        // [112307TIN] fix table with is overflow
        // [112302TIN] set fixed with for column
        // fixedWidth: 48,
        label: Text(
          StringManager.status,
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        onSort: (columnIndex, ascending) => sort<String>(
            (d) => json.encode(d.isDeleted), columnIndex, ascending),
        // [102331TIN] edit size of column
        size: ColumnSize.M,
      ),
      DataColumn2(
        // [112302TIN] edit size of column
        fixedWidth: 160,
        label: Text(
          'Action',
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
        // [102331TIN] edit size of column
        size: ColumnSize.L,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    _templatesDataSource = TemplateDataSource(
        context,
        widget.templates,
        widget.onEditTemplate,
        widget.onDeleteTemplate,
        widget.onChangeStatusTemplate);
    return TableManagement(
      dataSource: _templatesDataSource,
      columns: _columns,
      rowsPerPage: _rowsPerPage,
      sortColumnIndex: _sortColumnIndex,
      sortAscending: _sortAscending,
    );
  }
}
