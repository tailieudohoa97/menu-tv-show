import 'package:flutter/material.dart';
import 'package:tv_menu/helper/responsive.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/views/admin/partial_widgets/search_bar.dart';
import 'package:tv_menu/views/admin/template_management/widgets/table_template_data.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_filter.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_reload.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_role.dart';
import 'package:tv_menu/widgets/custom_card.dart';

import '../../../../common/common.dart';

class TableTemplateBackgroundWidget extends StatefulWidget {
  final Function onEditTemplate;
  final Function onDeleteTemplate;
  final Function onChangeStatusTemplate;
  final List<Template> templates;
  const TableTemplateBackgroundWidget(
      {super.key,
      required this.templates,
      required this.onEditTemplate,
      required this.onDeleteTemplate,
      required this.onChangeStatusTemplate});

  @override
  State<TableTemplateBackgroundWidget> createState() =>
      _TableTemplateBackgroundWidgetState();
}

class _TableTemplateBackgroundWidgetState
    extends State<TableTemplateBackgroundWidget> {
  SizedBox _height(BuildContext context) => SizedBox(
        height: Responsive.isDesktop(context) ? 30 : 20,
      );
  SizedBox _width(BuildContext context) => SizedBox(
        width: Responsive.isDesktop(context) ? 20 : 10,
      );
  final TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //[102312HG] Makeup
    return CustomCard(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Container(
              width: 700,
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(30)),
              child: Center(
                  child: SearchBarLayout(
                searchType: "template",
                // [102331TIN] set width for result box
                widthOfResultBox: 680,
                // [102331TIN] edit search hint
                // hintText: "Enter keyword",
                hintText: StringManager.searchHintForTemplateManagement,
              )),
            ),
            if (!Responsive.isMobile(context)) ...[
              _width(context),
              // [102312HG] Makeup
              // ButtonApp(
              //   icon: Icons.filter_alt_outlined,
              //   buttonColor: Color(0xFF7978fb),
              //   borderColor: Color(0xFF7978fb),
              //   iconColor: Colors.white,
              //   title: AppText(
              //     StringManager.filterButton,
              //     AppTextStyles.body14Medium.copyWith(color: Colors.white),
              //   ),
              // ),
              buttonAppFilter(),

              _width(context),

              // [102312HG] Makeup
              // ButtonApp(
              //   icon: Icons.supervised_user_circle_outlined,
              //   buttonColor: Color(0xFFfb5b5a),
              //   borderColor: Color(0xFFfb5b5a),
              //   iconColor: Colors.white,
              //   title: AppText(
              //     StringManager.roleType,
              //     AppTextStyles.body14Medium.copyWith(color: Colors.white),
              //   ),
              // ),
              buttonAppRole(),
              _width(context),

              // [102312HG] Makeup
              // const ButtonApp(
              //   circularIndex: 120,
              //   borderColor: Colors.white12,
              //   iconColor: Colors.white,
              //   icon: Icons.replay_rounded,
              //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              // )
              buttonAppReload(
                circularIndex: 120,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              ),
            ]
          ],
        ),
        _height(context),
        SizedBox(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: TableTemplateData(
              templates: widget.templates,
              onDeleteTemplate: widget.onDeleteTemplate,
              onEditTemplate: widget.onEditTemplate,
              onChangeStatusTemplate: widget.onChangeStatusTemplate,
            )),
      ],
    ));
  }
}
