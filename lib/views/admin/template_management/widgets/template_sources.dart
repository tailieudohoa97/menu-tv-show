import 'package:flutter/cupertino.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:tv_menu/admin_builders/views/builder_page.dart';
import 'package:tv_menu/admin_builders/controllers/builders_controller.dart';
import 'package:tv_menu/common/components/dropdown/dropdown.dart';
import 'package:tv_menu/common/components/text/app_text.dart';
import 'package:tv_menu/common/components/text/text_styles.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/models/category.dart';
import 'package:tv_menu/models/template.dart';
import 'package:tv_menu/routes/route_names.dart';
import 'package:tv_menu/services/template_service.dart';
import 'package:tv_menu/views/admin/partial_widgets/preview_template.dart';
import 'dart:convert';

import '../../../../common/common.dart';

class RestorableTemplateSelections extends RestorableProperty<Set<int>> {
  Set<int> _templateSelections = {};

  /// Returns whether or not a template row is selected by index.
  bool isSelected(int index) => _templateSelections.contains(index);

  /// Takes a list of [Template]s and saves the row indices of selected rows
  /// into a [Set].
  void setTemplateSelections(List<Template> templates) {
    final updatedSet = <int>{};
    for (var i = 0; i < templates.length; i += 1) {
      var template = templates[i];
      if (template.selected) {
        updatedSet.add(i);
      }
    }
    _templateSelections = updatedSet;
    notifyListeners();
  }

  @override
  Set<int> createDefaultValue() => _templateSelections;

  @override
  Set<int> fromPrimitives(Object? data) {
    final selectedItemIndices = data as List<dynamic>;
    _templateSelections = {
      ...selectedItemIndices.map<int>((dynamic id) => id as int),
    };
    return _templateSelections;
  }

  @override
  void initWithValue(Set<int> value) {
    _templateSelections = value;
  }

  @override
  Object toPrimitives() => _templateSelections.toList();
}

// /// Domain model entity
// class Template {
//   Template(
//       {required this.id,
//       required this.templateSlug,
//       required this.ratioSetting,
//       required this.isDeleted,
//       required this.widgetSetting});

//   final String id;

//   final String templateSlug;
//   final Map<String, dynamic> ratioSetting;
//   final bool isDeleted;
//   final Map<String, dynamic> widgetSetting;
//   bool selected = false;

//   factory Template.fromMap(Map<String, dynamic> map) {
//     return Template(
//         id: map['_id'] ?? "",
//         templateSlug: map['templateSlug'] ?? "",
//         ratioSetting: map['ratioSetting'] ?? "",
//         widgetSetting: map['widgets_setting'] ?? "",
//         isDeleted: map['isDeleted']);
//   }
// }

/// Data source implementing standard Flutter's DataTableSource abstract class
/// which is part of DataTable and PaginatedDataTable synchronous data fecthin API.
/// This class uses static collection of deserts as a data store, projects it into
/// DataRows, keeps track of selected items, provides sprting capability
class TemplateDataSource extends DataTableSource {
  TemplateDataSource(this.context, this.templates, this.onEditTemplate,
      this.onDeleteTemplate, this.onChangeStatusTemplate);
  int _selectedCount = 0;
  final Function onEditTemplate;
  final Function onDeleteTemplate;
  final Function onChangeStatusTemplate;
  final BuildContext context;
  List<Template> templates;
  // Add row tap handlers and show snackbar
  // Override height values for certain rows
  // Color each Row by index's parity
  bool hasZebraStripes = true;

  void sort<T>(Comparable<T> Function(Template d) getField, bool ascending) {
    templates.sort((a, b) {
      final aValue = getField(a);
      final bValue = getField(b);
      return ascending
          ? Comparable.compare(aValue, bValue)
          : Comparable.compare(bValue, aValue);
    });
    notifyListeners();
  }

  void updateSelectedTemplates(RestorableTemplateSelections selectedRows) {
    _selectedCount = 0;
    for (var i = 0; i < templates.length; i += 1) {
      var template = templates[i];
      if (selectedRows.isSelected(i)) {
        template.selected = true;
        _selectedCount += 1;
      } else {
        template.selected = false;
      }
    }
    notifyListeners();
  }

  @override
  DataRow getRow(int index, [Color? color]) {
    assert(index >= 0);
    if (index >= templates.length) throw 'index > _templates.length';
    final template = templates[index];
    return DataRow2.byIndex(
      index: index,
      // [102320TIN] disable select row
      // selected: template.selected,
      selected: false,
      color: color != null
          ? MaterialStateProperty.all(color)
          : (hasZebraStripes && index.isEven
              // [102320TIN] change color of even rows
              ? MaterialStatePropertyAll(Colors.white.withOpacity(.01))
              // ? MaterialStateProperty.all(AppColors.blueCommon.opacity6)
              : null),
      onSelectChanged: (value) {
        if (template.selected != value) {
          _selectedCount += value! ? 1 : -1;
          assert(_selectedCount >= 0);
          template.selected = value;
          notifyListeners();
        }
      },
      cells: [
        DataCell(SizedBox(
            width: 40,
            child: AppText(
              template.id,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        DataCell(SizedBox(
            width: 180,
            child: AppText(
              template.templateName,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        DataCell(SizedBox(
            width: 120,
            child: AppText(
              template.subscriptionLevel,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        DataCell(SizedBox(
            width: 120,
            child: AppText(
              template.templateSlug,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        DataCell(
          Container(
            height: 60,
            constraints: BoxConstraints(minWidth: 60),
            child:
                // AppText(
                //   template.categoryId,
                //   AppTextStyles.body14Medium,
                //   maxLines: 1,
                //   overflow: TextOverflow.ellipsis,
                // )
                // [112307TIN] use dropdown for categoy (can quick edit category)
                // * Note: not effect, just ui
                FutureBuilder(
              future: TemplateService.getCategories(),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Text(
                    'Error ${snapshot.error.toString()}',
                    style: TextStyle(color: AppColors.red),
                  );
                }
                if (snapshot.hasData) {
                  final categories = (snapshot.data ?? []).map((jsonMap) {
                    final map = Map<String, dynamic>.from(jsonMap);
                    return Category.fromMap(map);
                  }).toList();

                  final Category? initCategory = categories.firstWhereOrNull(
                      (category) => category.id == template.categoryId);

                  return Padding(
                    padding: EdgeInsets.all(6),
                    child: AppDropdown<Category>(
                      initValue: initCategory,
                      dataList: categories,
                      onChangeItem: (category) {
                        if (category == null) return;
                        print('category selected ${category.name}');
                      },
                    ),
                  );
                }

                return SizedBox(
                  child: Center(
                    child: SizedBox(
                      height: 20,
                      width: 20,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        strokeCap: StrokeCap.round,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
        DataCell(SizedBox(
            width: 120,
            child: AppText(
              template.ratios,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        // [112307TIN] hide json columns
        // DataCell(SizedBox(
        //     width: 120,
        //     child: AppText(
        //       json.encode(template.ratioSetting),
        //       AppTextStyles.body14Medium,
        //       maxLines: 1,
        //       overflow: TextOverflow.ellipsis,
        //     ))),
        // DataCell(SizedBox(
        //     width: 120,
        //     child: AppText(
        //       json.encode(
        //         template.widgetSetting,
        //       ),
        //       AppTextStyles.body14Medium,
        //       maxLines: 1,
        //       overflow: TextOverflow.ellipsis,
        //     ))),
        // DataCell(SizedBox(
        //     width: 120,
        //     child: AppText(
        //       json.encode(
        //         template.dataSetting,
        //       ),
        //       AppTextStyles.body14Medium,
        //       maxLines: 1,
        //       overflow: TextOverflow.ellipsis,
        //     ))),
        DataCell(SizedBox(
            width: 120,
            child: AppText(
              template.created,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ))),
        // [112307TIN] fix table with is overflow
        DataCell(
          SizedBox(
            child: Row(
              children: [
                SizedBox(
                  height: 10,
                  // width: 30,
                  // child: CupertinoSwitch(
                  //   value: !template.isDeleted!,
                  //   onChanged: (value) {
                  //     onChangeStatusTemplate(template.id, !value);
                  //   },
                  // )
                  // [112307TIN] makeup
                  child: Theme(
                    data: ThemeData(useMaterial3: true),
                    child: Switch(
                      value: !template.isDeleted!,
                      activeTrackColor: Colors.blue,
                      thumbColor: MaterialStatePropertyAll(Colors.white),
                      thumbIcon: MaterialStatePropertyAll(
                        !template.isDeleted!
                            ? Icon(Icons.done, color: Colors.blue)
                            : Icon(Icons.close, color: Colors.grey),
                      ),
                      trackOutlineColor:
                          MaterialStatePropertyAll(Colors.transparent),
                      onChanged: (value) {
                        onChangeStatusTemplate(template.id, !value);
                      },
                    ),
                  ),
                ),
                SizedBox(width: 8),
                AppText(
                  template.isDeleted! ? 'Deleted' : 'Active',
                  AppTextStyles.body14Medium.copyWith(
                    // color:
                    //     template.isDeleted! ? AppColors.red : AppColors.green,
                    color: template.isDeleted!
                        ? AppColors.cancelButton
                        : AppColors.confirmButton,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
        ),
        DataCell(Row(
          children: [
            IconButton(
                // 102320TIN change color and add tooltip message
                tooltip: 'Detail',
                color: Colors.blue,
                // color: Colors.grey,
                onPressed: () {
                  if (!template.isDeleted!) {
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            content: PreviewTemplate(
                              idTemplate: template.id,
                            ),
                            contentPadding: EdgeInsets.zero,
                          );
                        });
                  }
                },
                icon: const Icon(Icons.remove_red_eye_outlined)),
            IconButton(
                // 102320TIN add tooltip message
                tooltip: 'Edit',
                color: Colors.grey,
                onPressed: () async {
                  if (!template.isDeleted!) {
                    var result = await showDialog<bool>(
                      context: context,
                      barrierDismissible: false,
                      builder: (BuildContext context) {
                        return EditTemplateDialog(
                          title: StringManager.editTemplateTitle,
                          template: template,
                          onEditTemplate: onEditTemplate,
                        );
                      },
                    );
                    if (result == null) return;
                    if (result) {
                      //TODO: Add Create Template Event
                    }
                  }
                },
                icon: const Icon(Icons.edit)),
            // [112302TIN] add this button
            IconButton(
              tooltip: 'Edit by builder',
              color: Colors.grey,
              onPressed: () async {
                final screenSize = MediaQuery.of(context).size;
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (BuildContext context) {
                    return SizedBox(
                      width: screenSize.width * 0.9,
                      height: screenSize.height * 0.9,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          TextButton(
                            style: TextButton.styleFrom(
                              foregroundColor: Colors.white,
                            ),
                            onPressed: () => Navigator.pop(context),
                            child: Icon(Icons.close),
                          ),
                          Expanded(
                            // put Builder here
                            child: BuilderPage(
                              template: template,
                            ),
                          )
                        ],
                      ),
                    );
                  },
                );
              },
              icon: const Icon(Icons.build_circle_outlined),
            ),
            // IconButton(
            //     // 102320TIN add tooltip message
            //     tooltip: 'Edit with builder',
            //     color: Colors.grey,
            //     onPressed: () {
            //       // Navigator.pushNamed(context, Routes.AdminBuilderRoute,
            //       //     arguments: {"template": template.toJson()});
            //       if (template.isDeleted!) {
            //         showDialog(
            //             context: context,
            //             builder: (context) {
            //               return AlertDialog(
            //                 content: BuilderPage(
            //                   template: template,
            //                 ),
            //                 contentPadding: EdgeInsets.zero,
            //               );
            //             });
            //       }
            //     },
            //     icon: const Icon(Icons.build_circle_rounded)),
            IconButton(
                // 102320TIN add tooltip message
                tooltip: 'Delete',
                color: Colors.redAccent,
                onPressed: template.isDeleted!
                    ? null
                    : () async {
                        var result = await showDialog<bool>(
                          context: context,
                          barrierDismissible: true,
                          builder: (BuildContext context) {
                            return AppDialog(
                                iconWidget: const Icon(
                                  Icons.delete_forever,
                                  color: AppColors.red,
                                  size: 60,
                                ),
                                descriptionWidget: Column(
                                  children: [
                                    const AppText(
                                        StringManager.confirmDeleteTemplateDesc,
                                        AppTextStyles.body16Regular),
                                    AppText(
                                        template.id,
                                        AppTextStyles.button18Bold
                                            .copyWith(color: AppColors.red)),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    SizedBox(
                                      width: 250,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          ButtonApp(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 25, vertical: 10),
                                            title: AppText(
                                                StringManager.cancel,
                                                AppTextStyles.body14Medium
                                                    .copyWith(
                                                        color:
                                                            AppColors.white)),
                                            buttonColor: AppColors.cancelButton,
                                            circularIndex: 20,
                                            onTap: () {
                                              //TODO: Fix Return CallBack

                                              Navigator.pop(context);
                                            },
                                          ),
                                          const SizedBox(
                                            width: 15,
                                          ),
                                          ButtonApp(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 20, vertical: 10),
                                            title: AppText(
                                                StringManager.confirm,
                                                AppTextStyles.body14Medium
                                                    .copyWith(
                                                        color:
                                                            AppColors.white)),
                                            buttonColor:
                                                AppColors.confirmButton,
                                            circularIndex: 20,
                                            onTap: () {
                                              //TODO: Fix Return CallBack
                                              onDeleteTemplate(template.id);
                                              Navigator.pop(context);
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ));
                          },
                        );
                        if (result == null) return;
                        if (result) {
                          //TODO: Add Create Template Event
                        }
                      },
                icon: const Icon(Icons.delete)),
          ],
        ))
      ],
    );
  }

  @override
  int get rowCount => templates.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;

  void selectAll(bool? checked) {
    for (final template in templates) {
      template.selected = checked ?? false;
    }
    _selectedCount = (checked ?? false) ? templates.length : 0;
    notifyListeners();
  }
}
