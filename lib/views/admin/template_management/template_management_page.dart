import 'package:flutter/material.dart';
import 'package:tv_menu/controller/admin/template/add_new_template_controller.dart';
import 'package:tv_menu/models/models.dart';
import 'package:tv_menu/views/admin/template_management/widgets/table_template_background.dart';

import '../../../common/common.dart';
import '../../../helper/responsive.dart';

class TemplatePage extends StatelessWidget {
  final List<Template> templates;
  final Function onCreateTemplate;
  final Function onUpdateTemplate;
  final Function onDeleteTemplate;
  final Function onChangeStatusTemplate;
  const TemplatePage({
    super.key,
    required this.templates,
    required this.onCreateTemplate,
    required this.onUpdateTemplate,
    required this.onDeleteTemplate,
    required this.onChangeStatusTemplate,
  });

  @override
  Widget build(BuildContext context) {
    SizedBox _height(BuildContext context) => SizedBox(
          height: Responsive.isDesktop(context) ? 30 : 20,
        );

    return SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: Responsive.isMobile(context) ? 15 : 18),
          child: Column(
            children: [
              SizedBox(
                height: Responsive.isMobile(context) ? 5 : 18,
              ),
              SizedBox(
                  height: 50,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Expanded(
                          child: Text(
                            StringManager.titleTemplateManagement,
                            style: TextStyle(color: Colors.white, fontSize: 25),
                          ),
                        ),
                        if (!Responsive.isMobile(context)) ...[
                          ButtonApp(
                            title: AppText(
                                StringManager.addNewTemplate,
                                // [102320TIN] change body16bold to body14bold
                                AppTextStyles.body14Bold
                                    .copyWith(color: AppColors.white)),
                            iconColor: AppColors.white,
                            icon: Icons.add,
                            splashColor: AppColors.green.withOpacity(0.5),
                            hoverColor: Colors.blueAccent,
                            focusColor: Colors.green,
                            onTap: () async {
                              var result = await showDialog<bool>(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return BuildAddNewTemplateLayout(
                                    onCreateTemplate: onCreateTemplate,
                                  );
                                },
                              );
                              if (result == null) return;
                              if (result) {
                                onCreateTemplate();
                                //TODO: Add Create Template Event
                              }
                            },
                          ),
                          SizedBox(
                            width: Responsive.isDesktop(context) ? 20 : 10,
                          ),
                        ]
                      ])),
              SizedBox(
                height: Responsive.isMobile(context) ? 5 : 20,
              ),
              TableTemplateBackgroundWidget(
                templates: templates,
                onEditTemplate: onUpdateTemplate,
                onDeleteTemplate: onDeleteTemplate,
                onChangeStatusTemplate: onChangeStatusTemplate,
              ),
              _height(context),
            ],
          ),
        )));
  }
}
