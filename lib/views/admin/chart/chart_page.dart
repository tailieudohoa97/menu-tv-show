import 'package:flutter/material.dart';
import 'package:tv_menu/views/admin/chart/widgets/activity_details_card.dart';
import 'package:tv_menu/views/admin/chart/widgets/bar_graph_card.dart';
import 'package:tv_menu/views/admin/chart/widgets/liner_chart_sample_1.dart';
import 'package:tv_menu/views/admin/chart/widgets/pie_chart.dart';

import '../../../common/constants/constants.dart';
import '../../../helper/responsive.dart';
import 'widgets/line_chart_card.dart';

class ChartPage extends StatelessWidget {
  const ChartPage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    SizedBox _height(BuildContext context) => SizedBox(
          height: Responsive.isDesktop(context) ? 30 : 20,
        );

    return Container(
        // color: Colors.amber,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: Responsive.isMobile(context) ? 15 : 18),
          child: Column(
            children: [
              SizedBox(
                height: Responsive.isMobile(context) ? 5 : 18,
              ),
              const SizedBox(
                height: 50,
                child:
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                  Expanded(
                    child: Text(
                      StringManager.titleAnalysis,
                      // [102320TIN] change font size from 25 to 24
                      style: TextStyle(color: Colors.white, fontSize: 24),
                    ),
                  ),
                ]),
              ),
              _height(context),
              const ActivityDetailsCard(),
              _height(context),
              LineChartCard(),
              _height(context),
              BarGraphCard(),
              _height(context),
              const LineChartSample1(),
              _height(context),
              const PieChartSample1(),
              _height(context),
            ],
          ),
        )));
  }
}
