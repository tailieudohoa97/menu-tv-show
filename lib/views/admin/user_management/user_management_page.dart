import 'package:flutter/material.dart';
import 'package:tv_menu/controller/admin/user/add_new_user_controller.dart';
import 'package:tv_menu/models/user.dart';

import 'package:tv_menu/views/admin/user_management/widgets/table_user_background.dart';

import '../../../common/common.dart';
import '../../../helper/responsive.dart';

class UserPage extends StatelessWidget {
  final List<User> users;
  final Function onEditUser;
  final Function onDeleteUser;
  final Function onCreateUser;
  const UserPage({
    super.key,
    required this.users,
    required this.onEditUser,
    required this.onDeleteUser,
    required this.onCreateUser,
  });

  @override
  Widget build(BuildContext context) {
    SizedBox _height(BuildContext context) => SizedBox(
          height: Responsive.isDesktop(context) ? 30 : 20,
        );

    return SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: Responsive.isMobile(context) ? 15 : 18),
          child: Column(
            children: [
              SizedBox(
                height: Responsive.isMobile(context) ? 5 : 18,
              ),
              SizedBox(
                  height: 50,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Expanded(
                          child: Text(
                            StringManager.titleUserManagement,
                            // [102320TIN] change font size to 24
                            // style: TextStyle(color: Colors.white, fontSize: 25),
                            style: TextStyle(color: Colors.white, fontSize: 24),
                          ),
                        ),
                        if (!Responsive.isMobile(context)) ...[
                          Material(
                            color: AppColors.transparent,
                            child: ButtonApp(
                              title: AppText(
                                  StringManager.addNewUser,
                                  // [102320TIN] change body16bold to body14bold
                                  AppTextStyles.body14Bold
                                      .copyWith(color: AppColors.white)),
                              iconColor: AppColors.white,

                              //[102312HG] Makeup
                              // splashColor: AppColors.green.withOpacity(0.5),
                              // hoverColor: Colors.black,
                              // focusColor: Colors.green,
                              icon: Icons.add,
                              onTap: () async {
                                var result = await showDialog<bool>(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    return BuildAddNewUserLayout(
                                        onCreateUser: (user) =>
                                            onCreateUser(user));
                                  },
                                );
                                if (result == null) return;
                                if (result) {
                                  //TODO: Add Create User Event
                                }
                              },
                            ),
                          ),
                          SizedBox(
                            width: Responsive.isDesktop(context) ? 20 : 10,
                          ),
                        ]
                      ])),
              SizedBox(
                height: Responsive.isMobile(context) ? 5 : 20,
              ),
              TableUserBackgroundWidget(
                users: users,
                onDeleteUser: onDeleteUser,
                onEditUser: onEditUser,
              ),
              _height(context),
            ],
          ),
        )));
  }
}
