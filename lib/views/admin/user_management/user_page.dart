import 'package:flutter/material.dart';
import 'package:tv_menu/common/colors/app_color.dart';
import 'package:tv_menu/common/components/button/app_button.dart';

import 'package:tv_menu/views/admin/user_management/widgets/table_data.dart';

import '../../../common/constants/constants.dart';
import '../../../helper/responsive.dart';

class UserPage extends StatelessWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;

  const UserPage({super.key, required this.scaffoldKey});

  @override
  Widget build(BuildContext context) {
    SizedBox _height(BuildContext context) => SizedBox(
          height: Responsive.isDesktop(context) ? 30 : 20,
        );

    return SizedBox(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
            child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: Responsive.isMobile(context) ? 15 : 18),
          child: Column(
            children: [
              SizedBox(
                height: Responsive.isMobile(context) ? 5 : 18,
              ),
              SizedBox(
                  height: 50,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Expanded(
                          child: Text(
                            // [102320TIN] change font size to 24
                            StringManager.titleUserManagement,
                            style: TextStyle(color: Colors.white, fontSize: 24),
                          ),
                        ),
                        if (!Responsive.isMobile(context)) ...[
                          ButtonApp(
                            title: Text(StringManager.addNewUser),
                            icon: Icons.add,
                            splashColor: AppColors.green.withOpacity(0.5),
                            hoverColor: Colors.blueAccent,
                            focusColor: Colors.green,
                            onTap: () {
                              print("hiii");
                            },
                          ),
                          SizedBox(
                            width: Responsive.isDesktop(context) ? 20 : 10,
                          ),
                        ]
                      ])),
              SizedBox(
                height: Responsive.isMobile(context) ? 5 : 20,
              ),
              //const TableWidget(),
              _height(context),
            ],
          ),
        )));
  }
}
