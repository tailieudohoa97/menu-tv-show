import 'package:flutter/material.dart';
import 'package:tv_menu/helper/responsive.dart';
import 'package:tv_menu/models/user.dart';
import 'package:tv_menu/views/admin/partial_widgets/search_bar.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_filter.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_reload.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_role.dart';
import 'package:tv_menu/widgets/custom_card.dart';
import 'package:tv_menu/views/admin/user_management/widgets/table_user_data.dart';

import '../../../../common/common.dart';
import 'user_sources.dart';

class TableUserBackgroundWidget extends StatefulWidget {
  final List<User> users;
  final Function onEditUser;
  final Function onDeleteUser;
  const TableUserBackgroundWidget(
      {super.key,
      required this.onEditUser,
      required this.onDeleteUser,
      required this.users});

  @override
  State<TableUserBackgroundWidget> createState() =>
      _TableUserBackgroundWidgetState();
}

class _TableUserBackgroundWidgetState extends State<TableUserBackgroundWidget> {
  SizedBox _height(BuildContext context) => SizedBox(
        height: Responsive.isDesktop(context) ? 30 : 20,
      );
  SizedBox _width(BuildContext context) => SizedBox(
        width: Responsive.isDesktop(context) ? 20 : 10,
      );
  final TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //[102312HG] Makeup
    return CustomCard(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        //header
        Row(
          children: [
            //Search Bar
            Container(
              width: 300,
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.05),
                  borderRadius: BorderRadius.circular(30)),
              child: Center(
                child: SearchBarLayout(
                  searchType: "user",
                  hintText: "Search to find account",
                ),
              ),
            ),

            if (!Responsive.isMobile(context)) ...[
              _width(context),
              // [102312HG] Makeup
              // ButtonApp(
              //   icon: Icons.filter_alt_outlined,
              //   buttonColor: Color(0xFF7978fb),
              //   borderColor: Color(0xFF7978fb),
              //   iconColor: Colors.white,
              //   title: AppText(
              //     StringManager.filterButton,
              //     AppTextStyles.body14Medium.copyWith(color: Colors.white),
              //   ),
              // ),
              buttonAppFilter(),

              _width(context),
              // [102312HG] Makeup
              // ButtonApp(
              //   icon: Icons.supervised_user_circle_outlined,
              //   buttonColor: Color(0xFFfb5b5a),
              //   borderColor: Color(0xFFfb5b5a),
              //   iconColor: Colors.white,
              //   title: AppText(
              //     StringManager.roleType,
              //     AppTextStyles.body14Medium.copyWith(color: Colors.white),
              //   ),
              // ),

              buttonAppRole(),

              _width(context),
              // [102312HG] Makeup
              // const ButtonApp(
              //   circularIndex: 120,
              //   borderColor: Colors.white12,
              //   iconColor: Colors.white,
              //   icon: Icons.replay_rounded,
              //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              // )

              buttonAppReload(
                circularIndex: 120,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              ),
            ]
          ],
        ),
        _height(context),

        //User Management Table
        SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: TableUserData(
            users: widget.users,
            onEditUser: widget.onEditUser,
            onDeleteUser: widget.onDeleteUser,
          ),
        ),
      ],
    ));
  }
}
