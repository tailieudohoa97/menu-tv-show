import 'package:flutter/material.dart';
import 'package:tv_menu/common/components/button/app_button.dart';
import 'package:tv_menu/common/components/text/app_text.dart';
import 'package:tv_menu/common/components/text/text_styles.dart';
import 'package:tv_menu/common/constants/string_manager.dart';

//[102316HG] Create this widget
Widget buttonAppReload({
  double? circularIndex,
  IconData icon = Icons.replay_rounded,
  Color buttonColor = Colors.white10,
  Color borderColor = Colors.white10,
  Color iconColor = Colors.white,
  VoidCallback? onTap,
  String? title,
  TextStyle? textStyle,
  EdgeInsets padding = const EdgeInsets.all(0),
}) {
  return ButtonApp(
    circularIndex: circularIndex,
    icon: icon,
    buttonColor: buttonColor,
    borderColor: borderColor,
    iconColor: iconColor,
    onTap: onTap ?? () {},
    padding: padding,
    title: title != null
        ? AppText(
            title,
            textStyle ??
                AppTextStyles.body14Medium.copyWith(
                  color: Colors.white,
                ),
          )
        : null,
  );
}
