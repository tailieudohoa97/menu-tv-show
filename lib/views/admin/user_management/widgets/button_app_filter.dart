import 'package:flutter/material.dart';
import 'package:tv_menu/common/components/button/app_button.dart';
import 'package:tv_menu/common/components/text/app_text.dart';
import 'package:tv_menu/common/components/text/text_styles.dart';
import 'package:tv_menu/common/constants/string_manager.dart';

//[102316HG] Create this widget
Widget buttonAppFilter({
  IconData icon = Icons.filter_alt_outlined,
  Color buttonColor = const Color(0xFF7978fb),
  Color borderColor = const Color(0xFF7978fb),
  Color iconColor = Colors.white,
  VoidCallback? onTap,
  String text = StringManager.filterButton,
  TextStyle? textStyle,
}) {
  return ButtonApp(
    icon: icon,
    buttonColor: buttonColor,
    borderColor: borderColor,
    iconColor: iconColor,
    onTap: onTap ?? () {},
    title: AppText(
      text,
      textStyle ??
          AppTextStyles.body14Medium.copyWith(
            color: Colors.white,
          ),
    ),
  );
}
