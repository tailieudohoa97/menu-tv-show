import 'package:flutter/material.dart';
import 'package:tv_menu/helper/responsive.dart';
import 'package:tv_menu/models/user.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_filter.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_reload.dart';
import 'package:tv_menu/views/admin/user_management/widgets/button_app_role.dart';
import 'package:tv_menu/widgets/custom_card.dart';
import 'package:tv_menu/views/admin/user_management/widgets/table.dart';

import '../../../../common/components/button/app_button.dart';
import '../../../../common/constants/constants.dart';

class TableWidget extends StatefulWidget {
  final List<User> users;
  final Function onEditUser;
  final Function onDeleteUser;
  const TableWidget(
      {super.key,
      required this.users,
      required this.onEditUser,
      required this.onDeleteUser});

  @override
  State<TableWidget> createState() => _TableWidgetState();
}

class _TableWidgetState extends State<TableWidget> {
  SizedBox _height(BuildContext context) => SizedBox(
        height: Responsive.isDesktop(context) ? 30 : 20,
      );
  SizedBox _width(BuildContext context) => SizedBox(
        width: Responsive.isDesktop(context) ? 20 : 10,
      );
  final TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //[102312HG] Makeup
    return CustomCard(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                width: 300,
                decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(30)),
                child: Center(
                  child: TextField(
                    onChanged: (v) {
                      setState(() {});
                    },
                    style: const TextStyle(color: Colors.black),
                    controller: _searchController,
                    decoration: InputDecoration(
                        prefixIcon: const Icon(
                          Icons.search,
                          color: Colors.grey,
                        ),
                        // iconColor: Colors.grey,
                        suffixIcon: _searchController.text.isNotEmpty
                            ? IconButton(
                                icon: const Icon(
                                  Icons.clear,
                                  color: Colors.redAccent,
                                ),
                                onPressed: () {
                                  setState(() {
                                    _searchController.text = '';
                                  });
                                },
                              )
                            : null,
                        hintText: StringManager.searchHint,
                        hintStyle: const TextStyle(color: Colors.grey),
                        border: InputBorder.none),
                  ),
                ),
              ),
              if (!Responsive.isMobile(context)) ...[
                _width(context),
                // [102312HG] Makeup
                // const ButtonApp(
                //   icon: Icons.filter_alt_outlined,
                //   title: Text(StringManager.filterButton),
                // ),
                buttonAppFilter(),
                _width(context),

                // [102312HG] Makeup
                // const ButtonApp(
                //   icon: Icons.supervised_user_circle_outlined,
                //   title: Text(StringManager.roleType),
                // ),
                buttonAppRole(),
                _width(context),

                // [102312HG] Makeup
                // const ButtonApp(
                //   circularIndex: 120,
                //   icon: Icons.replay_rounded,
                //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                // )
                buttonAppReload(
                  circularIndex: 120,
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                ),
              ]
            ],
          ),
          _height(context),
          if (Responsive.isMobile(context)) ...[
            Row(
              children: [
                _width(context),
                const ButtonApp(
                  icon: Icons.filter_alt_outlined,
                  title: Text(StringManager.filterButton),
                ),
                _width(context),
                const ButtonApp(
                  icon: Icons.supervised_user_circle_outlined,
                  title: Text(StringManager.roleType),
                ),
                _width(context),
                const ButtonApp(
                  circularIndex: 120,
                  icon: Icons.replay_rounded,
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                )
              ],
            ),
            _height(context)
          ],
          SizedBox(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: PaginatedDataTable2Demo(
                users: widget.users,
                onEditUser: widget.onEditUser,
                onDeleteUser: widget.onDeleteUser,
              )),
        ],
      ),
    );
  }
}
