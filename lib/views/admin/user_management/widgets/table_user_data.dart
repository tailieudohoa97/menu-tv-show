// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:tv_menu/common/common.dart';
import 'package:tv_menu/common/components/table/table.dart';
import 'package:tv_menu/models/user.dart';
import 'package:tv_menu/views/admin/user_management/widgets/user_sources.dart';

import '../../../../common/colors/app_color.dart';
import '../../../../common/constants/constants.dart';

class TableUserData extends StatefulWidget {
  final Function onEditUser;
  final Function onDeleteUser;
  final List<User> users;
  const TableUserData({
    super.key,
    required this.onEditUser,
    required this.onDeleteUser,
    required this.users,
  });

  @override
  TableUserDataState createState() => TableUserDataState();
}

class TableUserDataState extends State<TableUserData> {
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;
  bool _sortAscending = true;
  int? _sortColumnIndex;
  bool _initialized = false;
  dynamic usersDataSource;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  void sort<T>(
    Comparable<T> Function(User d) getField,
    int columnIndex,
    bool ascending,
  ) {
    usersDataSource.sort<T>(getField, ascending);
    setState(() {
      _sortColumnIndex = columnIndex;
      _sortAscending = ascending;
    });
  }

  @override
  void dispose() {
    usersDataSource.dispose();
    super.dispose();
  }

  // [102320TIN] set style for header table is 14bold
  List<DataColumn> get _columns {
    return [
      DataColumn(
        label: Text(
          StringManager.userName,
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.userName, columnIndex, ascending),
      ),
      DataColumn(
        label: Text(
          StringManager.password,
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.passWord!, columnIndex, ascending),
      ),
      DataColumn(
        label: Text(
          StringManager.subscriptionLevel,
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.subscriptionLevel!, columnIndex, ascending),
      ),
      DataColumn(
        label: Text(
          'Email',
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.email!, columnIndex, ascending),
      ),
      DataColumn(
        label: Text(
          'Action',
          style: AppTextStyles.body14Bold.copyWith(color: Colors.white),
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    usersDataSource = UserDataSource(
        context, widget.users, widget.onEditUser, widget.onDeleteUser);
    return TableManagement(
      dataSource: usersDataSource,
      columns: _columns,
      rowsPerPage: _rowsPerPage,
      sortColumnIndex: _sortColumnIndex,
      sortAscending: _sortAscending,
    );
  }
}
