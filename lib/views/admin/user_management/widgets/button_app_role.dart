import 'package:flutter/material.dart';
import 'package:tv_menu/common/components/button/app_button.dart';
import 'package:tv_menu/common/components/text/app_text.dart';
import 'package:tv_menu/common/components/text/text_styles.dart';
import 'package:tv_menu/common/constants/string_manager.dart';

//[102316HG] Create this widget
Widget buttonAppRole({
  IconData icon = Icons.supervised_user_circle_outlined,
  Color buttonColor = const Color(0xFFfb5b5a),
  Color borderColor = const Color(0xFFfb5b5a),
  Color iconColor = Colors.white,
  VoidCallback? onTap,
  String text = StringManager.roleType,
  TextStyle? textStyle,
}) {
  return ButtonApp(
    icon: icon,
    buttonColor: buttonColor,
    borderColor: borderColor,
    iconColor: iconColor,
    onTap: onTap ?? () {},
    title: AppText(
      text,
      textStyle ??
          AppTextStyles.body14Medium.copyWith(
            color: Colors.white,
          ),
    ),
  );
}
