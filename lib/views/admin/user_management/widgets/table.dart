// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:tv_menu/models/user.dart';
import 'package:tv_menu/views/admin/user_management/widgets/user_sources.dart';

import '../../../../common/colors/app_color.dart';
import '../../../../common/constants/constants.dart';

class PaginatedDataTable2Demo extends StatefulWidget {
  final Function onEditUser;
  final Function onDeleteUser;
  final List<User> users;
  const PaginatedDataTable2Demo(
      {super.key,
      required this.users,
      required this.onEditUser,
      required this.onDeleteUser});

  @override
  PaginatedDataTable2DemoState createState() => PaginatedDataTable2DemoState();
}

class PaginatedDataTable2DemoState extends State<PaginatedDataTable2Demo> {
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;
  bool _sortAscending = true;
  int? _sortColumnIndex;
  late UserDataSource usersDataSource;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  void sort<T>(
    Comparable<T> Function(User d) getField,
    int columnIndex,
    bool ascending,
  ) {
    usersDataSource.sort<T>(getField, ascending);
    setState(() {
      _sortColumnIndex = columnIndex;
      _sortAscending = ascending;
    });
  }

  @override
  void dispose() {
    usersDataSource.dispose();
    super.dispose();
  }

  List<DataColumn> get _columns {
    return [
      DataColumn(
        label: const Text(StringManager.userName),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.userName, columnIndex, ascending),
      ),
      DataColumn(
        label: const Text(StringManager.password),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.passWord!, columnIndex, ascending),
      ),
      DataColumn(
        label: const Text(StringManager.subscriptionLevel),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.subscriptionLevel!, columnIndex, ascending),
      ),
      DataColumn(
        label: const Text('Email'),
        onSort: (columnIndex, ascending) =>
            sort<String>((d) => d.email!, columnIndex, ascending),
      ),
      const DataColumn(
        label: Text('Action'),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    usersDataSource = UserDataSource(
        context, widget.users, widget.onEditUser, widget.onDeleteUser);
    return Stack(alignment: Alignment.bottomCenter, children: [
      Theme(
        data: ThemeData(
          textTheme: const TextTheme(
            bodySmall:
                TextStyle(color: AppColors.lightGray), // Change text color
          ),
        ),
        child: PaginatedDataTable2(
          // 100 Won't be shown since it is smaller than total records
          availableRowsPerPage: const [2, 5, 10, 30, 100],
          horizontalMargin: 20,
          checkboxHorizontalMargin: 12,
          columnSpacing: 12,
          wrapInCard: false,
          renderEmptyRowsInTheEnd: false,
          showCheckboxColumn: false,
          headingRowColor: MaterialStateColor.resolveWith(
              (states) => AppColors.blueCommon.opacity5),
          headingTextStyle: const TextStyle(color: Colors.white),
          rowsPerPage: _rowsPerPage,
          dataTextStyle: const TextStyle(color: AppColors.red),

          minWidth: 800,
          fit: FlexFit.tight,
          border: const TableBorder(
              borderRadius: BorderRadius.all(Radius.circular(30)),
              top: const BorderSide(color: Colors.grey),
              bottom: BorderSide(color: Colors.grey),
              left: BorderSide(color: Colors.grey),
              right: BorderSide(color: Colors.grey),
              // verticalInside: BorderSide(color: Colors.grey[300]!),
              horizontalInside: BorderSide(color: Colors.grey)),
          onRowsPerPageChanged: (value) {
            // No need to wrap into setState, it will be called inside the widget
            // and trigger rebuild
            //setState(() {
            _rowsPerPage = value!;
            print(_rowsPerPage);
            //});
          },
          initialFirstRowIndex: 0,
          onPageChanged: (rowIndex) {
            print(rowIndex / _rowsPerPage);
          },
          sortColumnIndex: _sortColumnIndex,
          sortAscending: _sortAscending,
          sortArrowIcon: Icons.keyboard_arrow_up, // custom arrow
          sortArrowAnimationDuration:
              const Duration(milliseconds: 0), // custom animation duration
          onSelectAll: usersDataSource.selectAll,
          columns: _columns,
          empty: Center(
              child: Container(
                  padding: const EdgeInsets.all(20),
                  color: Colors.grey[200],
                  child: const Text('No data'))),
          source: usersDataSource,
        ),
      ),
    ]);
  }
}
