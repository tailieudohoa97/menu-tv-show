import 'package:flutter/material.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:tv_menu/common/components/dialog/preview_user_dialog.dart';
import 'package:tv_menu/common/components/text/app_text.dart';
import 'package:tv_menu/common/components/text/text_styles.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/models/user.dart';

import '../../../../common/common.dart';
import '../../../../common/components/dialog/edit_user_dialog.dart';

class RestorableUserSelections extends RestorableProperty<Set<int>> {
  Set<int> _userSelections = {};

  /// Returns whether or not a user row is selected by index.
  bool isSelected(int index) => _userSelections.contains(index);

  /// Takes a list of [User]s and saves the row indices of selected rows
  /// into a [Set].
  void setUserSelections(List<User> users) {
    final updatedSet = <int>{};
    for (var i = 0; i < users.length; i += 1) {
      var user = users[i];
      if (user.selected) {
        updatedSet.add(i);
      }
    }
    _userSelections = updatedSet;
    notifyListeners();
  }

  @override
  Set<int> createDefaultValue() => _userSelections;

  @override
  Set<int> fromPrimitives(Object? data) {
    final selectedItemIndices = data as List<dynamic>;
    _userSelections = {
      ...selectedItemIndices.map<int>((dynamic id) => id as int),
    };
    return _userSelections;
  }

  @override
  void initWithValue(Set<int> value) {
    _userSelections = value;
  }

  @override
  Object toPrimitives() => _userSelections.toList();
}

/// Data source implementing standard Flutter's DataTableSource abstract class
/// which is part of DataTable and PaginatedDataTable synchronous data fecthin API.
/// This class uses static collection of deserts as a data store, projects it into
/// DataRows, keeps track of selected items, provides sprting capability
class UserDataSource extends DataTableSource {
  final Function onEditUser;
  final Function onDeleteUser;
  UserDataSource(this.context, this.users, this.onEditUser, this.onDeleteUser);
  int _selectedCount = 0;

  final BuildContext context;
  List<User> users;
  // Add row tap handlers and show snackbar
  // Override height values for certain rows
  // Color each Row by index's parity
  // bool hasZebraStripes = true;

  bool hasZebraStripes = true;

  void sort<T>(Comparable<T> Function(User d) getField, bool ascending) {
    users.sort((a, b) {
      final aValue = getField(a);
      final bValue = getField(b);
      return ascending
          ? Comparable.compare(aValue, bValue)
          : Comparable.compare(bValue, aValue);
    });
    notifyListeners();
  }

  void updateSelectedUsers(RestorableUserSelections selectedRows) {
    _selectedCount = 0;
    for (var i = 0; i < users.length; i += 1) {
      var user = users[i];
      if (selectedRows.isSelected(i)) {
        user.selected = true;
        _selectedCount += 1;
      } else {
        user.selected = false;
      }
    }
    notifyListeners();
  }

  @override
  DataRow getRow(int index, [Color? color]) {
    assert(index >= 0);
    if (index >= users.length) throw 'index > _users.length';
    final user = users[index];
    return DataRow2.byIndex(
      index: index,

      // selected: user.selected,
      color: color != null
          ? MaterialStateProperty.all(color)
          : (hasZebraStripes && index.isEven
              // [102320TIN] change color of even rows
              ? MaterialStatePropertyAll(Colors.white.withOpacity(.01))
              // ? MaterialStateProperty.all(AppColors.blueCommon.opacity6)
              : null),
      onSelectChanged: (value) {
        if (user.selected != value) {
          _selectedCount += value! ? 1 : -1;
          assert(_selectedCount >= 0);
          user.selected = value;
          notifyListeners();
        }
      },
      cells: [
        DataCell(SizedBox(
            width: 120,
            child: AppText(
              user.userName,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
            ))),
        DataCell(SizedBox(
            width: 120,
            child: AppText(
              user.passWord,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
            ))),
        DataCell(SizedBox(
            width: 120,
            child: AppText(
              user.subscriptionLevel,
              AppTextStyles.body14Medium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
            ))),
        DataCell(SizedBox(
            width: 200,
            child: AppText(
              user.email,
              AppTextStyles.body14Medium,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
            ))),
        DataCell(Row(
          children: [
            IconButton(
                tooltip: "Detail", //[102312HG] Makeup
                // [102320TIN] change color
                color: Colors.blue,
                // color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
                onPressed: () {
                  Helpers.debugLog('See this user', user.id);
                  // [112315TIN] add user preview dialog
                  showDialog<bool>(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return PreviewUserDialog(
                        title: StringManager.previewUser,
                        user: user,
                      );
                    },
                  );
                },
                icon: const Icon(Icons.remove_red_eye_outlined)),
            IconButton(
                tooltip: "Edit", //[102312HG] Makeup
                color: Colors.white.withOpacity(0.5), //[102312HG] Makeup
                onPressed: () async {
                  var result = await showDialog<bool>(
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) {
                      return EditUserDialog(
                        title: StringManager.editUserTitle,
                        user: user,
                        onEditUser: (user) => onEditUser(user),
                      );
                    },
                  );
                  if (result == null) return;
                  if (result) {
                    //TODO: Add Create User Event
                  }
                },
                icon: const Icon(Icons.edit)),
            IconButton(
                tooltip: "Delete", //[102312HG] Makeup
                color: Colors.redAccent,
                onPressed: () async {
                  var result = await showDialog<bool>(
                    context: context,
                    barrierDismissible: true,
                    builder: (BuildContext context) {
                      return AppDialog(
                          iconWidget: const Icon(
                            Icons.delete_forever,
                            color: AppColors.red,
                            size: 60,
                          ),
                          descriptionWidget: Column(
                            children: [
                              const AppText(StringManager.confirmDeleteUserDesc,
                                  AppTextStyles.body16Regular),
                              AppText(
                                  user.userName,
                                  AppTextStyles.button18Bold
                                      .copyWith(color: AppColors.red)),
                              const SizedBox(
                                height: 20,
                              ),
                              SizedBox(
                                width: 250,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    ButtonApp(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 25, vertical: 10),
                                      title: AppText(
                                          StringManager.cancel,
                                          AppTextStyles.body14Medium.copyWith(
                                              color: AppColors.white)),
                                      buttonColor: AppColors.cancelButton,
                                      circularIndex: 20,
                                      onTap: () {
                                        //TODO: Fix Return CallBack

                                        Navigator.pop(context);
                                      },
                                    ),
                                    const SizedBox(
                                      width: 15,
                                    ),
                                    ButtonApp(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20, vertical: 10),
                                      title: AppText(
                                          StringManager.confirm,
                                          AppTextStyles.body14Medium.copyWith(
                                              color: AppColors.white)),
                                      buttonColor: AppColors.confirmButton,
                                      circularIndex: 20,
                                      onTap: () {
                                        //TODO: Fix Return CallBack
                                        onDeleteUser(user.id);
                                        Navigator.pop(context);
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ));
                    },
                  );
                  if (result == null) return;
                  if (result) {
                    //TODO: Add Create User Event
                  }
                },
                icon: const Icon(Icons.delete)),
          ],
        ))
      ],
    );
  }

  @override
  int get rowCount => users.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;

  void selectAll(bool? checked) {
    for (final user in users) {
      user.selected = checked ?? false;
    }
    _selectedCount = (checked ?? false) ? users.length : 0;
    notifyListeners();
  }
}
