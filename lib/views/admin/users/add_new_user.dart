import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tv_menu/views/admin/partial_widgets/popup_title_only.dart';

import '../../../../helper/textstyle_global.dart';

Widget addNewUserLayout(
  Widget nameTextField,
  Widget emailTextField,
  Widget addressTextField,
  Widget saveButton,
) {
  return SingleChildScrollView(
    child: Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
      width: 740,
      padding: const EdgeInsets.all(20),
      child: IntrinsicHeight(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Title
            const PopupTitleOnly(
              title: 'Add a new user',
            ),

            //Name of new product
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: nameTextField),
                ),

                //Price of new product
                Expanded(
                  child: Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: emailTextField),
                ),
                //072315YW- add field stock
                Expanded(
                  child: Container(
                      margin: const EdgeInsets.only(right: 10),
                      child: addressTextField),
                ),
              ],
            ),

            //[062316HG] STAY HIDE
            //Tax class
            // const TaxClass(),

            //Create Product Button
            saveButton
          ],
        ),
      ),
    ),
  );
}

Widget nameTextField(Function handleOnChange, String errorMessage,
    TextEditingController controller) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      const Padding(
        padding: EdgeInsets.only(top: 20, bottom: 10),
        child: Text(
          "Product Name: *",
          style: TextStyleGlobal.mediumNameProduct,
        ),
      ),
      SizedBox(
        height: 40,
        child: TextField(
          controller: controller,
          onChanged: (value) => handleOnChange(value),
          decoration: InputDecoration(
            // errorText: 'This field is required',
            contentPadding:
                const EdgeInsets.symmetric(vertical: 6, horizontal: 15),
            enabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(
                width: 1,
                color: Color(0xFFdbdbdb),
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                width: 1,
                color:
                    errorMessage == "" ? const Color(0xFF09ADAA) : Colors.red,
              ),
            ),
          ),
          cursorColor: Colors.black,
        ),
      ),
      if (errorMessage != "")
        Text(
          errorMessage,
          style: const TextStyle(color: Colors.red, fontSize: 12),
        )
    ],
  );
}

// price text field
Widget emailTextField(Function handleOnChange, String errorMessage,
    TextEditingController controller) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      const Padding(
        padding: EdgeInsets.only(top: 20, bottom: 10),
        child: Text(
          "Email: *",
          style: TextStyleGlobal.mediumNameProduct,
        ),
      ),
      SizedBox(
        height: 40,
        child: TextField(
          controller: controller,
          onChanged: (value) => handleOnChange(value),
          decoration: InputDecoration(
              // errorText: 'This field is required',
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 6, horizontal: 15),
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  width: 1,
                  color: Color(0xFFdbdbdb),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 1,
                      color: errorMessage == ""
                          ? const Color(0xFF09ADAA)
                          : Colors.red))),
          cursorColor: Colors.black,
        ),
      ),
      if (errorMessage != "")
        Text(
          errorMessage,
          style: const TextStyle(color: Colors.red, fontSize: 12),
        )
    ],
  );
}

//[062329HG] Temporarily Hide . Stock input
Widget addressTextField(Function handleOnChange, String errorMessage,
    TextEditingController controller) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      const Padding(
        padding: EdgeInsets.only(top: 20, bottom: 10),
        child: Text(
          "Address: *",
          style: TextStyleGlobal.mediumNameProduct,
        ),
      ),
      SizedBox(
        height: 40,
        child: TextField(
          controller: controller,
          onChanged: (value) => handleOnChange(value),
          decoration: InputDecoration(
              // errorText: 'This field is required',
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 6, horizontal: 15),
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  width: 1,
                  color: Color(0xFFdbdbdb),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 1,
                      color: errorMessage == ""
                          ? const Color(0xFF09ADAA)
                          : Colors.red))),
          cursorColor: Colors.black,
        ),
      ),
      if (errorMessage != "")
        Text(
          errorMessage,
          style: const TextStyle(color: Colors.red, fontSize: 12),
        )
    ],
  );
}

// class Categories extends StatefulWidget {
//   const Categories({super.key});

//   @override
//   State<Categories> createState() => _Categories();
// }

// class _Categories extends State<Categories> {
//   String shippingMethod = 'Uncategorized';

//   // Hoặc Dùng kiểu Non-nullable, có dấu ?
//   // String? chooseStore;
//   dynamic cats = jsonDecode(Helpers.getLocalDb(BOX.app)!.get('category'));

//   List listItem = [];

//   @override
//   Widget build(BuildContext context) {
//     listItem = List<Category>.from(cats.map((x) => Category.fromMap(x)))
//         .map((e) => e.name)
//         .toList();
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         const Padding(
//           padding: EdgeInsets.only(top: 20, bottom: 10),
//           child: Text(
//             "Category",
//             style: TextStyleGlobal.mediumNameProduct,
//           ),
//         ),
//         Container(
//           height: 40,
//           decoration: BoxDecoration(
//             border: Border.all(
//               width: 1,
//               color: const Color(0xFFdbdbdb),
//             ),
//             borderRadius: BorderRadius.circular(5),
//           ),
//           child: Row(
//             children: [
//               Expanded(
//                 child: Stack(
//                   children: [
//                     Row(
//                       children: [
//                         Expanded(
//                           child: ButtonTheme(
//                             alignedDropdown: true,
//                             child: DropdownButton(
//                               //Ban đầu rỗng, sẽ hiển thị cái hint
//                               hint: const Text('Choose...'),
//                               value: shippingMethod,
//                               isExpanded: true,
//                               underline: const SizedBox(),
//                               style: const TextStyle(
//                                 fontSize: 13,
//                                 color: Colors.black,
//                               ),
//                               icon: const Icon(Icons.keyboard_arrow_down),
//                               // Mỗi lần click chọn 1 item
//                               // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
//                               // dẫn tới "value: chooseStore," ở phía trên thay đổi
//                               // Và cái hint sẽ bị thay = cái string item đã chọn
//                               onChanged: (newValue) {
//                                 setState(() {
//                                   shippingMethod = newValue
//                                       .toString(); //Phải cùng kiểu String >>> .toString()
//                                 });
//                               },

//                               //Tạo list item, layout cho 1 item
//                               items: listItem.map((newValue) {
//                                 return DropdownMenuItem(
//                                   // alignment: Alignment.centerLeft,
//                                   value: newValue,
//                                   child: Text(newValue),
//                                 );
//                               }).toList(),
//                             ),
//                           ),
//                         ),
//                       ],
//                     )
//                   ],
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ],
//     );
//   }
// }

class TaxStatus extends StatefulWidget {
  const TaxStatus({super.key});

  @override
  State<TaxStatus> createState() => _TaxStatus();
}

class _TaxStatus extends State<TaxStatus> {
  String shippingMethod = 'Taxable';

  // Hoặc Dùng kiểu Non-nullable, có dấu ?
  // String? chooseStore;

  List listItem = ['Taxable', 'Shipping only', 'None'];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Tax status:",
            style: TextStyleGlobal.mediumNameProduct,
          ),
        ),
        Container(
          height: 40,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: const Color(0xFFdbdbdb),
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Row(
            children: [
              Expanded(
                child: Stack(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton(
                              //Ban đầu rỗng, sẽ hiển thị cái hint
                              hint: const Text('Choose...'),
                              value: shippingMethod,
                              isExpanded: true,
                              underline: const SizedBox(),
                              style: const TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                              ),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              // Mỗi lần click chọn 1 item
                              // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
                              // dẫn tới "value: chooseStore," ở phía trên thay đổi
                              // Và cái hint sẽ bị thay = cái string item đã chọn
                              onChanged: (newValue) {
                                setState(() {
                                  shippingMethod = newValue
                                      .toString(); //Phải cùng kiểu String >>> .toString()
                                });
                              },

                              //Tạo list item, layout cho 1 item
                              items: listItem.map((newValue) {
                                return DropdownMenuItem(
                                  // alignment: Alignment.centerLeft,
                                  value: newValue,
                                  child: Text(newValue),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class TaxClass extends StatefulWidget {
  const TaxClass({super.key});

  @override
  State<TaxClass> createState() => _TaxClass();
}

class _TaxClass extends State<TaxClass> {
  String? shippingMethod;

  // Hoặc Dùng kiểu Non-nullable, có dấu ?
  // String? chooseStore;

  List listItem = ['Hà Nội', 'Hồ Chí Minh'];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 20, bottom: 10),
          child: Text(
            "Tax class:",
            style: TextStyleGlobal.mediumNameProduct,
          ),
        ),
        Container(
          height: 40,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: const Color(0xFFdbdbdb),
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Row(
            children: [
              Expanded(
                child: Stack(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton(
                              //Ban đầu rỗng, sẽ hiển thị cái hint
                              hint: const Text('Choose...'),
                              value: shippingMethod,
                              isExpanded: true,
                              underline: const SizedBox(),
                              style: const TextStyle(
                                fontSize: 13,
                                color: Colors.black,
                              ),
                              icon: const Icon(Icons.keyboard_arrow_down),
                              // Mỗi lần click chọn 1 item
                              // thì hàm Onchange sẽ set lại value mới = value mới vừa chọn
                              // dẫn tới "value: chooseStore," ở phía trên thay đổi
                              // Và cái hint sẽ bị thay = cái string item đã chọn
                              onChanged: (newValue) {
                                setState(() {
                                  shippingMethod = newValue
                                      .toString(); //Phải cùng kiểu String >>> .toString()
                                });
                              },

                              //Tạo list item, layout cho 1 item
                              items: listItem.map((newValue) {
                                return DropdownMenuItem(
                                  // alignment: Alignment.centerLeft,
                                  value: newValue,
                                  child: Text(newValue),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
