import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';
import 'package:tv_menu/controller/template_controller.dart';
import 'package:tv_menu/helper/textstyle_global_2.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';

Widget screenLayout(String templateId) {
  Widget tempLayout;

  tempLayout = TemplateControllerState(
    idTemplate: templateId,
    userId: '1',
  );

  return Sizer(
    builder: (context, orientation, deviceType) {
      return MaterialApp(
        theme: ThemeData(
          fontFamily: GoogleFonts.openSans().fontFamily,
        ),
        home: Scaffold(
          body: tempLayout,
        ),
      );
    },
  );
}
