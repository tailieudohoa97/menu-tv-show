// import 'package:tv_menu/views/widgets/build_menu_column_2.dart';
// import 'package:flutter/material.dart';
// import 'package:sizer/sizer.dart';
// import 'package:tv_menu/models/model_datas/category_data.dart';
// import 'package:tv_menu/helper/calculate_ratio_temp_restaurant_3.dart';
// import 'package:tv_menu/helper/global_setting.dart';

// /*
// // Used to:
//     Build the interface of an application displaying restaurant information based on categories and products.

// // Required inputs:
//   - ratioStyle: Dynamic parameter to determine interface ratios and sizes based on the device.
//   - categoryDataList: List of category and product data (type List<CategoryData>).
//   - useBackgroundCategoryTitle: Choose whether to use a background for the category title (default is true).
//   - pathImageBgCategoryTitle: Path to the background image for the category title (default is a specific image path).
//   - colorCategoryTitle: Color for the category title (default is white).
//   - colorBackgroundCategoryTitle: Background color for the category title (default is transparent white).
//   - colorProductName: Color for the product name (default is white).
//   - colorPrice: Color for the product price (default is orange).
//   - colorDescription: Color for the product description (default is white).
//   - toUpperCaseNameProductName: Use uppercase for the product name or not (default is true).
//   - useThumnailProduct: Use a thumbnail image for the product or not (default is false).
//   - pathThumnailProduct: Path to the thumbnail image for the product (default is a specific image path).
//   - imageList: List of paths to product images.
//   - startIndexWidgetList: Starting index of the list of products and images to display in the column (default is 0).
//   - endIndexWidgetList: Ending index of the list of products and images to display in the column (default is 3).

// // Note:
//   - This code segment constructs an interface to display restaurant categories and their respective products in three columns.
//   - Each column uses the buildMenuColumn2 function to build the category and corresponding product interface.
//   - Parameters for each column are passed through categoryDataList and imageList.
//   - Other parameters such as colors and the use of a background are configured using the corresponding parameters.
// */

// class TempRestaurant3State extends StatelessWidget {
//   late CalculateRatioRestaurant3 ratioStyle;
//   Color colorCategoryTitle = Color(0xFF121212);
//   bool useBackgroundCategoryTitle = true;
//   bool useFullWidthCategoryTitle = true;
//   String pathImageBgCategoryTitle = '';
//   Color colorBackgroundCategoryTitle = const Color(0xfffa8700);
//   Color colorProductName = Color(0xFFffffff);
//   Color colorPrice = Color(0xfffa8700);
//   Color colorDescription = Color(0xFFffffff);
//   bool toUpperCaseNameProductName = false;
//   bool useThumnailProduct = false;
//   String pathThumnailProduct = GlobalSetting.noImage;
//   bool fixCover = false;
//   List<CategoryData> categoryDataList;
//   List<String> imageList = [
//     "/temp_restaurant_3/food-temp3(1).png",
//     "/temp_restaurant_3/food-temp3(2).png",
//     "/temp_restaurant_3/food-temp3(3).png",
//     "/temp_restaurant_3/food-temp3(4).png",
//     "/temp_restaurant_3/food-temp3(5).png",
//     "/temp_restaurant_3/food-temp3(6).png",
//     "/temp_restaurant_3/food-temp3(7).png",
//     "/temp_restaurant_3/food-temp3(8).png",
//     "/temp_restaurant_3/food-temp3(9).png",
//   ];
//   int startIndexWidgetList = 0;
//   int endIndexWidgetList = 3;

//   TempRestaurant3State(this.categoryDataList, {super.key});

//   @override
//   Widget build(BuildContext context) {
//     ratioStyle = CalculateRatioRestaurant3(context);
//     GlobalSetting.ratioStyle = ratioStyle;
//     return Sizer(
//       builder: (context, orientation, deviceType) {
//         return Container(
//           decoration: BoxDecoration(
//             image: DecorationImage(
//               //Fixed
//               image: AssetImage('/temp_restaurant_3/food-bg(4).jpg'),
//               fit: BoxFit.cover,
//             ),
//           ),
//           child: Container(
//             color: Colors.black.withOpacity(0.9), // Màu đen với opacity 0.8
//             child: Row(
//               children: [
//                 // Cột chính 1 - Danh mục 1 và sản phẩm, 3 hình ảnh
//                 Expanded(
//                   child: Padding(
//                     padding: EdgeInsets.all(2.w),
//                     child: buildMenuColumn2(
//                       indexCategory: 0,
//                       useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//                       pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//                       colorCategoryTitle: colorCategoryTitle,
//                       colorBackgroundCategoryTitle:
//                           colorBackgroundCategoryTitle,
//                       colorPrice: colorPrice,
//                       colorProductName: colorProductName,
//                       colorDescription: colorDescription,
//                       useThumnailProduct: useThumnailProduct,
//                       pathThumnailProduct: pathThumnailProduct,
//                       toUpperCaseNameProductName: toUpperCaseNameProductName,
//                       useMediumStyle: false,
//                       categoryDataList: categoryDataList,
//                       widgetList: imageList,
//                       startIndexProduct: 0,
//                       endIndexProduct: 5,
//                       startIndexWidgetList: 0,
//                       endIndexWidgetList: 3,
//                       fixCover: fixCover,
//                       useFullWidthCategoryTitle: useFullWidthCategoryTitle,
//                     ),
//                   ),
//                 ),

//                 // Cột chính 2 - Danh mục 2 và sản phẩm, 3 hình ảnh
//                 Expanded(
//                   child: Container(
//                     color: Color.fromARGB(9, 255, 255, 255),
//                     padding: EdgeInsets.all(2.w),
//                     child: buildMenuColumn2(
//                       indexCategory: 1,
//                       useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//                       pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//                       colorCategoryTitle: colorCategoryTitle,
//                       colorBackgroundCategoryTitle:
//                           colorBackgroundCategoryTitle,
//                       colorPrice: colorPrice,
//                       colorProductName: colorProductName,
//                       colorDescription: colorDescription,
//                       useThumnailProduct: useThumnailProduct,
//                       pathThumnailProduct: pathThumnailProduct,
//                       toUpperCaseNameProductName: toUpperCaseNameProductName,
//                       useMediumStyle: false,
//                       categoryDataList: categoryDataList,
//                       widgetList: imageList,
//                       startIndexProduct: 0,
//                       endIndexProduct: 5,
//                       startIndexWidgetList: 3,
//                       endIndexWidgetList: 6,
//                       fixCover: fixCover,
//                       useFullWidthCategoryTitle: useFullWidthCategoryTitle,
//                     ),
//                   ),
//                 ),

//                 // Cột chính 3 - Danh mục 3 và sản phẩm, 3 hình ảnh
//                 Expanded(
//                   child: Padding(
//                     padding: EdgeInsets.all(2.w),
//                     child: buildMenuColumn2(
//                       indexCategory: 2,
//                       useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//                       pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//                       colorCategoryTitle: colorCategoryTitle,
//                       colorBackgroundCategoryTitle:
//                           colorBackgroundCategoryTitle,
//                       colorPrice: colorPrice,
//                       colorProductName: colorProductName,
//                       colorDescription: colorDescription,
//                       useThumnailProduct: useThumnailProduct,
//                       pathThumnailProduct: pathThumnailProduct,
//                       toUpperCaseNameProductName: toUpperCaseNameProductName,
//                       useMediumStyle: false,
//                       categoryDataList: categoryDataList,
//                       widgetList: imageList,
//                       startIndexProduct: 0,
//                       endIndexProduct: 5,
//                       startIndexWidgetList: 6,
//                       endIndexWidgetList: 9,
//                       fixCover: fixCover,
//                       useFullWidthCategoryTitle: useFullWidthCategoryTitle,
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         );
//       },
//     );
//   }
// }


// // -------------------------------------- 
// // {
// // 	"templateSlug": "temp_restaurant_3",
// // 	"RatioSetting": {
// // 		'textStyle': {
// // 			'categoryNameStyle': {
// // 				'fontWeight': 'w300',
// // 				'fontFamily': 'alfaSlabOne',
// // 				'letterSpacing': '5',
// // 			},
// // 			'nameProductStyle': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'poppins',
// // 			},
// // 			'nameProductStyleMedium': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'poppins',
// // 			},
// // 			'discriptionProductStyle': {
// // 				'fontWeight': 'w300',
// // 				'fontFamily': 'poppins',
// // 			},
// // 			'discriptionProductStyleMedium': {
// // 				'fontWeight': 'w300',
// // 				'fontFamily': 'poppins',
// // 			},
// // 			'priceProductStyle': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'poppins',
// // 			},
// // 			'priceProductStyleMedium': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'openSans',
// // 			},
// // 		},
// // 		'renderScreen': {
// // 			'32/9': {
// //         'categoryNameStyle':'1.w',
// //         'nameProductStyle':'0.7.w',
// //         'nameProductStyleMedium':'0.5.w',
// //         'priceProductStyle':'0.7.w',
// //         'priceProductStyleMedium':'0.5.w',
// //         'discriptionProductStyle':'0.5.w',
// //         'discriptionProductStyleMedium':'0.3.w',
// //         'imageHeight':'50.h',
// //         'marginBottomImage':'0',
// //         'marginBottomProduct':'2.h',
// //         'boderRadiusStyle':'30',
// //         'marginBottomCategoryTitle':'5.h',
// //         'paddingVerticalCategorTilte':'0.w',
// //         'paddingHorizontalCategorTilte':'2.w',
// //         'widthThumnailProduct':'3.w',
// //         'heightThumnailProduct':'3.w',
// // 			},
// // 			'21/9': {
// //         'categoryNameStyle':'1.5.w',
// //         'nameProductStyle':'1.w',
// //         'nameProductStyleMedium':'0.7.w',
// //         'priceProductStyle':'1.w',
// //         'priceProductStyleMedium':'0.7.w',
// //         'discriptionProductStyle':'0.7.w',
// //         'discriptionProductStyleMedium':'0.8.w',
// //         'imageHeight':'50.h',
// //         'marginBottomImage':'0',
// //         'marginBottomProduct':'2.h',
// //         'boderRadiusStyle':'30',
// //         'marginBottomCategoryTitle':'5.h',
// //         'paddingVerticalCategorTilte':'0.w',
// //         'paddingHorizontalCategorTilte':'2.w',
// //         'widthThumnailProduct':'3.w',
// //         'heightThumnailProduct':'3.w',
// // 			},

// // 			'16/9': {
// //         'categoryNameStyle':'2.5.w',
// //         'nameProductStyle':'1.5.w',
// //         'nameProductStyleMedium':'1.w',
// //         'priceProductStyle':'1.5.w',
// //         'priceProductStyleMedium':'1.w',
// //         'discriptionProductStyle':'1.w',
// //         'discriptionProductStyleMedium':'0.8.w',
// //         'imageHeight':'20.h',
// //         'marginBottomImage':'0',
// //         'marginBottomProduct':'2.h',
// //         'boderRadiusStyle':'30',
// //         'marginBottomCategoryTitle':'5.h',
// //         'paddingVerticalCategorTilte':'0.5.w',
// //         'paddingHorizontalCategorTilte':'2.w',
// //         'widthThumnailProduct':'3.w',
// //         'heightThumnailProduct':'3.w',
// // 			},

// // 			'16/10': {
// //         'categoryNameStyle':'3.w',
// //         'nameProductStyle':'1.5.w',
// //         'nameProductStyleMedium':'1.w',
// //         'priceProductStyle':'1.5.w',
// //         'priceProductStyleMedium':'1.w',
// //         'discriptionProductStyle':'1.w',
// //         'discriptionProductStyleMedium':'0.8.w',
// //         'imageHeight':'50.h',
// //         'marginBottomImage':'0',
// //         'marginBottomProduct':'2.h',
// //         'boderRadiusStyle':'30',
// //         'marginBottomCategoryTitle':'5.h',
// //         'paddingVerticalCategorTilte':'1.w',
// //         'paddingHorizontalCategorTilte':'2.w',
// //         'widthThumnailProduct':'3.w',
// //         'heightThumnailProduct':'3.w',
// // 			},

// // 			'4/3': {
// //         'categoryNameStyle':'3.w',
// //         'nameProductStyle':'2.w',
// //         'nameProductStyleMedium':'1.5.w',
// //         'priceProductStyle':'2.w',
// //         'priceProductStyleMedium':'1.5.w',
// //         'discriptionProductStyle':'1.5.w',
// //         'discriptionProductStyleMedium':'0.8.w',
// //         'imageHeight':'50.h',
// //         'marginBottomImage':'0',
// //         'marginBottomProduct':'2.h',
// //         'boderRadiusStyle':'30',
// //         'marginBottomCategoryTitle':'5.h',
// //         'paddingVerticalCategorTilte':'0.w',
// //         'paddingHorizontalCategorTilte':'2.w',
// //         'widthThumnailProduct':'3.w',
// //         'heightThumnailProduct':'3.w',
// // 			},

// // 			'default': {
// //         'categoryNameStyle':'3.w',
// //         'nameProductStyle':'1.5.w',
// //         'nameProductStyleMedium':'1.w',
// //         'priceProductStyle':'1.5.w',
// //         'priceProductStyleMedium':'1.w',
// //         'discriptionProductStyle':'1.w',
// //         'discriptionProductStyleMedium':'0.8.w',
// //         'imageHeight':'20.h',
// //         'marginBottomImage':'0',
// //         'marginBottomProduct':'2.h',
// //         'boderRadiusStyle':'30',
// //         'marginBottomCategoryTitle':'5.h',
// //         'paddingVerticalCategorTilte':'0.w',
// //         'paddingHorizontalCategorTilte':'2.w',
// //         'widthThumnailProduct':'3.w',
// //         'heightThumnailProduct':'3.w',
// // 			},
// // 		},
// // 	}
// // }