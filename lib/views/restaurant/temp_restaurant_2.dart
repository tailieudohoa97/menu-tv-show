// import 'package:flutter/material.dart';
// import 'package:tv_menu/views/widgets/build_menu_column.dart';
// import 'package:tv_menu/models/model_datas/category_data.dart';
// import 'package:tv_menu/helper/calculate_ratio_temp_restaurant_2.dart';

// import 'package:tv_menu/helper/global_setting.dart';

// /*
// // Used to:
//     Build the interface of an application displaying restaurant information based on categories and products.

// // Required inputs:
//   - ratioStyle: Dynamic parameter to determine interface ratios and sizes based on the device.
//   - categoryDataList: List of category and product data (type List<CategoryData>).

// // Note:
//   - This code segment constructs an interface to display restaurant categories and their respective products.
//   - The interface is divided into columns, with each column displaying a category and its corresponding products.
//   - The number of columns is determined by the number of elements in categoryDataList (maximum of 3 columns).
//   - Each column uses the buildMenuColumn function to build the category and product interface.
//   - Parameters for each column are passed through categoryDataList.
// */

// class TempRestaurant2State extends StatelessWidget {
//   // late CalculateRatioRestaurant2 ratioStyle;

//   //startIndexCategory: Vị trí Danh mục bắt đầu
//   //endIndexCategory: Vị trí danh mục trước khi kết thúc
//   //0,3 là lấy 3 danh mục ở vị trí: 0, 1, 2
//   int startIndexCategory = 0;
//   int endIndexCategory = 3;
//   bool useFullWidthCategoryTitle = true;

//   List<CategoryData> categoryDataList;
//   TempRestaurant2State(this.categoryDataList, {super.key});

//   @override
//   Widget build(BuildContext context) {
//     // ratioStyle = CalculateRatioRestaurant2(context); //Cho BE can tuy bien
//     GlobalSetting.ratioStyle = ratioStyle;
//     return Container(
//       decoration: const BoxDecoration(
//         image: DecorationImage(
//           image: AssetImage('/temp_restaurant_2/BG-2.png'),
//           fit: BoxFit.cover,
//         ),
//       ),
//       child: Row(
//         children:
//             // Vòng lặp gọi Layout và thông số cho 1 Cột Danh mục
//             // từ List<CategoryData> categoryDataList;
//             categoryDataList
//                 .sublist(startIndexCategory, endIndexCategory)
//                 .map(
//                   (cateItem) => Expanded(
//                     child: buildMenuColumn(
//                       categoryName: cateItem.categoryName,
//                       imagePath: cateItem.categoryImage,
//                       fixCover: false,
//                       toUpperCaseNameProductName: true,
//                       useMediumStyle: false,
//                       colorPrice: Color(0xFFf47b22),
//                       useThumnailProduct: false,
//                       useBackgroundCategoryTitle: true,
//                       colorCategoryTitle: const Color(0xFFFFFFFF),
//                       pathImageBgCategoryTitle: '',
//                       colorBackgroundCategoryTitle: Color(0xB161644E),
//                       product: cateItem.listMenu,
//                       startIndexProduct: 0,
//                       endIndexProduct: 4,
//                       maxLineProductName: 1,
//                       maxLineProductDescription: 2,
//                       useFullWidthCategoryTitle: useFullWidthCategoryTitle,
//                     ),
//                   ),
//                 )
//                 .toList(),
//       ),
//     );
//   }
// }

// // // -------------------------------------- 
// // // {
// // // 	"templateSlug": "temp_restaurant_2",
// // // 	"RatioSetting": {
// // // 		'textStyle': {
// // // 			'categoryNameStyle': {
// // // 				'fontWeight': 'bold',
// // // 				'fontFamily': 'openSans',
// // // 			},
// // // 			'nameProductStyle': {
// // // 				'fontWeight': 'w800',
// // // 				'fontFamily': 'openSans',
// // // 			},
// // // 			'nameProductStyleMedium': {
// // // 				'fontWeight': 'w800',
// // // 				'fontFamily': 'openSans',
// // // 			},
// // // 			'discriptionProductStyle': {
// // // 				'fontWeight': 'w300',
// // // 				'fontFamily': 'openSans',
// // // 			},
// // // 			'discriptionProductStyleMedium': {
// // // 				'fontWeight': 'w300',
// // // 				'fontFamily': 'openSans',
// // // 			},
// // // 			'priceProductStyle': {
// // // 				'fontWeight': 'w800',
// // // 				'fontFamily': 'openSans',

// // // 			},
// // // 			'priceProductStyleMedium': {
// // // 				'fontWeight': 'w800',
// // // 				'fontFamily': 'openSans',

// // // 			},
// // // 		},
// // // 		'renderScreen': {
// // // 			'32/9': {
// // //         'categoryNameStyle':'1.5.w',
// // //         'nameProductStyle':'1.w',
// // //         'priceProductStyle':'1.w',
// // //         'discriptionProductStyle':'0.7.w',
// // //         'imageHeight':'25.h',
// // //         'marginBottomImage':'0.h',
// // //         'marginBottomProduct':'3.h',
// // //         'boderRadiusStyle':'100',
// // //         'marginBottomCategoryTitle':'0.h',
// // //         'paddingVerticalCategorTilte':'2.h',
// // //         'paddingHorizontalCategorTilte':'1.h',
// // // 			},
// // // 			'21/9': {
// // //         'categoryNameStyle':'1.5.w',
// // //         'nameProductStyle':'1.3.w',
// // //         'priceProductStyle':'1.3.w',
// // //         'discriptionProductStyle':'0.8.w',
// // //         'imageHeight':'30.h',
// // //         'marginBottomImage':'0.h',
// // //         'marginBottomProduct':'3.h',
// // //         'boderRadiusStyle':'100',
// // //         'marginBottomCategoryTitle':'0.h',
// // //         'paddingVerticalCategorTilte':'2.h',
// // //         'paddingHorizontalCategorTilte':'1.h',
// // // 			},

// // // 			'16/9': {
// // //         'categoryNameStyle':'2.w',
// // //         'nameProductStyle':'1.5.w',
// // //         'priceProductStyle':'1.5.w',
// // //         'discriptionProductStyle':'1.w',
// // //         'imageHeight':'30.h',
// // //         'marginBottomImage':'0.h',
// // //         'marginBottomProduct':'3.h',
// // //         'boderRadiusStyle':'100',
// // //         'marginBottomCategoryTitle':'0.h',
// // //         'paddingVerticalCategorTilte':'2.h',
// // //         'paddingHorizontalCategorTilte':'1.h',
// // // 			},

// // // 			'16/10': {
// // //         'categoryNameStyle':'2.w',
// // //         'nameProductStyle':'1.5.w',
// // //         'priceProductStyle':'1.5.w',
// // //         'discriptionProductStyle':'1.w',
// // //         'imageHeight':'40.h',
// // //         'marginBottomImage':'0.h',
// // //         'marginBottomProduct':'3.h',
// // //         'boderRadiusStyle':'100',
// // //         'marginBottomCategoryTitle':'0.h',
// // //         'paddingVerticalCategorTilte':'2.h',
// // //         'paddingHorizontalCategorTilte':'1.h',
// // // 			},

// // // 			'4/3': {
// // //         'categoryNameStyle':'2.w',
// // //         'nameProductStyle':'1.8.w',
// // //         'priceProductStyle':'1.8.w',
// // //         'discriptionProductStyle':'1.3.w',
// // //         'imageHeight':'40.h',
// // //         'marginBottomImage':'0.h',
// // //         'marginBottomProduct':'3.h',
// // //         'boderRadiusStyle':'100',
// // //         'marginBottomCategoryTitle':'0.h',
// // //         'paddingVerticalCategorTilte':'2.h',
// // //         'paddingHorizontalCategorTilte':'1.h',
// // // 			},

// // // 			'default': {
// // //         'categoryNameStyle':'2.w',
// // //         'nameProductStyle':'1.5.w',
// // //         'priceProductStyle':'1.5.w',
// // //         'discriptionProductStyle':'1.w',
// // //         'imageHeight':'37.h',
// // //         'marginBottomImage':'0.h',
// // //         'marginBottomProduct':'3.h',
// // //         'boderRadiusStyle':'100',
// // //         'marginBottomCategoryTitle':'0.h',
// // //         'paddingVerticalCategorTilte':'2.h',
// // //         'paddingHorizontalCategorTilte':'1.h',
// // // 			},
// // // 		},
// // // 	}
// // // }