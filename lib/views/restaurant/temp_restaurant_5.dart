// import 'package:tv_menu/views/widgets/category_title_grid_layout_2.dart';
// import 'package:tv_menu/views/widgets/category_with_image_in_row.dart';
// import 'package:flutter/material.dart';
// import 'package:sizer/sizer.dart';
// import 'package:tv_menu/views/widgets/two_category_in_row.dart';
// import 'package:tv_menu/models/model_datas/category_data.dart';
// import 'package:tv_menu/helper/calculate_ratio_temp_restaurant_5.dart';

// import 'package:tv_menu/helper/global_setting.dart';

// /*
// // Used to:
//     Build a widget displaying the interface of a specific restaurant with multiple categories and various products.

// // Required inputs:
//   - ratioStyle: Dynamic parameter to determine interface ratios and sizes based on the device.
//   - colorCategoryTitle: Color for the category title (default is black).
//   - colorPrice: Color for the product price (default is yellow).
//   - useBackgroundCategoryTitle: Choose whether to use a background for the category title (default is true).
//   - pathImageBgCategoryTitle: Path to the background image for the category title (default is a specific image path).
//   - categoryDataList: List of category and product data (type List<CategoryData>).
//   - imageList: List of paths to product images.

// // Layout will display:
//   - This widget constructs the interface of a restaurant with two columns, each containing multiple categories and various products.
//   - Each category will have a title, a list of products, and corresponding product information.
//   - This interface comprises multiple columns and rows, based on the input data, to display the restaurant's categories and products.
// */

// class TempRestaurant5State extends StatelessWidget {
//   late CalculateRatioRestaurant5 ratioStyle;
//   Color colorCategoryTitle = Color(0xFF121212);
//   Color colorPrice = Color(0xFFf4b91a);
//   bool useBackgroundCategoryTitle = true;
//   String pathImageBgCategoryTitle = 'temp_restaurant_5/decorate/BG-title.png';

//   List<CategoryData> categoryDataList;
//   List<String> imageList = [
//     "/temp_restaurant_5/hamburger(1).png",
//     "/temp_restaurant_5/hamburger(2).png",
//   ];

//   TempRestaurant5State(this.categoryDataList, {super.key});

//   @override
//   Widget build(BuildContext context) {
//     ratioStyle = CalculateRatioRestaurant5(context);
//     GlobalSetting.ratioStyle = ratioStyle;
//     return Sizer(
//       builder: (context, orientation, deviceType) {
//         return Container(
//           decoration: BoxDecoration(
//             image: DecorationImage(
//               //Fixed
//               image: AssetImage('/temp_restaurant_5/BG-1.png'),
//               fit: BoxFit.fill,
//             ),
//           ),
//           child: Row(
//             children: [
//               // Cột 1
//               Expanded(
//                 child: Padding(
//                   padding: EdgeInsets.all(3.w),
//                   child: Column(
//                     children: [
//                       // Dòng 1: Danh mục 1
//                       categoryTitleGridLayout2(
//                         categoryData: categoryDataList.elementAt(0),
//                         imagePath: './temp_restaurant_5/hamburger(1).png',
//                         fixCover: false,
//                         useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//                         pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//                         colorCategoryTitle: colorCategoryTitle,
//                         colorPrice: colorPrice,
//                       ),

//                       // Dòng 2: Danh mục 2 và 3
//                       twoCategoryInRow(
//                         categoryDataList: categoryDataList,
//                         indexCategory1: 1,
//                         indexCategory2: 2,
//                         useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//                         pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//                         colorCategoryTitle: colorCategoryTitle,
//                         colorPrice: colorPrice,
//                         startIndexProduct: 0,
//                         endIndexProduct: 3,
//                       ),
//                     ],
//                   ),
//                 ),
//               ),

//               // Cột 2
//               Expanded(
//                 child: Padding(
//                   padding: EdgeInsets.all(3.w),
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     children: [
//                       // Dòng 1: Danh mục 4 và 5
//                       twoCategoryInRow(
//                         categoryDataList: categoryDataList,
//                         indexCategory1: 3,
//                         indexCategory2: 4,
//                         useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//                         pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//                         colorCategoryTitle: colorCategoryTitle,
//                         colorPrice: colorPrice,
//                         startIndexProduct: 0,
//                         endIndexProduct: 3,
//                       ),

//                       // Dòng 2: Danh mục 6 và 1 hình đại diện không nền
//                       categoryWithImageInRow(
//                         categoryDataList: categoryDataList,
//                         indexCategory: 5,
//                         useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//                         pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//                         colorCategoryTitle: colorCategoryTitle,
//                         colorPrice: colorPrice,
//                         imagePath: './temp_restaurant_5/hamburger(2).png',
//                         fixCover: false,
//                         startIndexProduct: 0,
//                         endIndexProduct: 3,
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         );
//       },
//     );
//   }
// }


// // -------------------------------------- 
// // {
// // 	"templateSlug": "temp_restaurant_4",
// // 	"RatioSetting": {
// // 		'textStyle': {
// // 			'categoryNameStyle': {
// // 				'fontWeight': 'bold',
// // 				'fontFamily': 'bebasNeue',
// // 			},
// // 			'nameProductStyle': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',
// // 			},
// // 			'nameProductStyleMedium': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',
// // 			},
// // 			'discriptionProductStyle': {
// // 				'fontWeight': 'w300',
// // 				'fontFamily': 'openSans',
// // 			},
// // 			'discriptionProductStyleMedium': {
// // 				'fontWeight': 'w300',
// // 				'fontFamily': 'openSans',
// // 			},
// // 			'priceProductStyle': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',
// // 			},
// // 			'priceProductStyleMedium': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',
// // 			},
// // 		},
// // 		'renderScreen': {
// // 			'32/9': {
//         // 'categoryNameStyle':'1.5.w',
//         // 'nameProductStyle':'0.7.w',
//         // 'nameProductStyleMedium':'0.5.w',
//         // 'priceProductStyle':'0.7.w',
//         // 'priceProductStyleMedium':'0.5.w',
//         // 'discriptionProductStyle':'0.5.w',
//         // 'discriptionProductStyleMedium':'0.3.w',
//         // 'imageHeight':'50.h',
//         // 'marginBottomImage':'0.5.w',
//         // 'marginBottomProduct':'2.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'2.h',
//         // 'paddingVerticalCategorTilte':'0.5.w',
//         // 'paddingHorizontalCategorTilte':'3.5.w',
// // 			},
// // 			'21/9': {
//         // 'categoryNameStyle':'2.w',
//         // 'nameProductStyle':'1.w',
//         // 'nameProductStyleMedium':'0.5.w',
//         // 'priceProductStyle':'1.w',
//         // 'priceProductStyleMedium':'0.5.w',
//         // 'discriptionProductStyle':'0.7.w',
//         // 'discriptionProductStyleMedium':'0.6.w',
//         // 'imageHeight':'50.h',
//         // 'marginBottomImage':'0.5.w',
//         // 'marginBottomProduct':'2.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'2.h',
//         // 'paddingVerticalCategorTilte':'0.5.w',
//         // 'paddingHorizontalCategorTilte':'3.5.w',
// // 			},

// // 			'16/9': {
//         // 'categoryNameStyle':'2.w',
//         // 'nameProductStyle':'1.5.w',
//         // 'nameProductStyleMedium':'1.w',
//         // 'priceProductStyle':'1.5.w',
//         // 'priceProductStyleMedium':'1.w',
//         // 'discriptionProductStyle':'0.7.w',
//         // 'discriptionProductStyleMedium':'0.6.w',
//         // 'imageHeight':'50.h',
//         // 'marginBottomImage':'1.w',
//         // 'marginBottomProduct':'2.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'2.h',
//         // 'paddingVerticalCategorTilte':'1.w',
//         // 'paddingHorizontalCategorTilte':'3.5.w',
// // 			},

// // 			'16/10': {
//         // 'categoryNameStyle':'2.5.w',
//         // 'nameProductStyle':'1.5.w',
//         // 'nameProductStyleMedium':'1.w',
//         // 'priceProductStyle':'1.5.w',
//         // 'priceProductStyleMedium':'1.w',
//         // 'discriptionProductStyle':'1.w',
//         // 'discriptionProductStyleMedium':'0.7.w',
//         // 'imageHeight':'50.h',
//         // 'marginBottomImage':'1.w',
//         // 'marginBottomProduct':'2.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'2.h',
//         // 'paddingVerticalCategorTilte':'1.w',
//         // 'paddingHorizontalCategorTilte':'3.5.w',
// // 			},

// // 			'4/3': {
//         // 'categoryNameStyle':'2.5.w',
//         // 'nameProductStyle':'2.w',
//         // 'nameProductStyleMedium':'1.w',
//         // 'priceProductStyle':'2.w',
//         // 'priceProductStyleMedium':'1.3.w',
//         // 'discriptionProductStyle':'1.3.w',
//         // 'discriptionProductStyleMedium':'0.7.w',
//         // 'imageHeight':'50.h',
//         // 'marginBottomImage':'1.w',
//         // 'marginBottomProduct':'2.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'2.h',
//         // 'paddingVerticalCategorTilte':'1.w',
//         // 'paddingHorizontalCategorTilte':'3.5.w',
// // 			},

// // 			'default': {
//         // 'categoryNameStyle':'2.w',
//         // 'nameProductStyle':'1.5.w',
//         // 'nameProductStyleMedium':'1.w',
//         // 'priceProductStyle':'1.5.w',
//         // 'priceProductStyleMedium':'1.w',
//         // 'discriptionProductStyle':'0.7.w',
//         // 'discriptionProductStyleMedium':'0.6.w',
//         // 'imageHeight':'50.h',
//         // 'marginBottomImage':'1.w',
//         // 'marginBottomProduct':'2.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'2.h',
//         // 'paddingVerticalCategorTilte':'1.w',
//         // 'paddingHorizontalCategorTilte':'3.5.w',
// // 			},
// // 		},
// // 	}
// // }