// // import 'package:tv_menu/helper/calulate_ratio_temp.dart';
// import 'package:tv_menu/helper/calculate_ratio_temp_restaurant_1.dart';
// import 'package:flutter/material.dart';
// import 'package:sizer/sizer.dart';
// import 'package:tv_menu/views/widgets/category_title_single_column.dart';
// import 'package:tv_menu/views/widgets/category_with_image_in_row.dart';
// import 'package:tv_menu/views/widgets/image.dart';
// import 'package:tv_menu/views/widgets/category_title_grid_layout_1.dart';
// import 'package:tv_menu/views/widgets/two_category_in_row.dart';
// import 'package:tv_menu/models/model_datas/category_data.dart';

// import 'package:tv_menu/views/widgets/qty_widget_in_row.dart';
// import 'package:tv_menu/views/widgets/qty_widget_in_column.dart';
// import 'package:tv_menu/helper/global_setting.dart';

// /*
// // Used to:
//     Build the interface of an application displaying restaurant information based on categories and products.

// // Required inputs:
//   - ratioStyle: Dynamic parameter to determine interface ratios and sizes based on the device.
//   - useBackgroundCategoryTitle: Specifies whether to use a background for category titles (true) or not (false).
//   - colorCategoryTitle: Color for category titles (type Color).
//   - categoryDataList: List of product category data (type List<CategoryData>).
//   - imageList: List of image paths (type List<String>) for products or categories.
//   - startIndexProduct and endIndexProduct: Index of the first and last product to be displayed within a category (default is 0).
//   - imagePath: Image path for a category or product thumbnail.
// */

// class TempRestaurant1State extends StatelessWidget {
//   late CalculateRatioRestaurant1 ratioStyle;

//   bool useBackgroundCategoryTitle = false;
//   Color colorCategoryTitle = Color(0xFFf47b22);

//   // Tạo 1 biến danh sách danh mục với menu
//   List<CategoryData> categoryDataList;
//   List<String> imageList = [
//     "/temp_restaurant_1/food-bg(1).jpg",
//     "/temp_restaurant_1/food-bg(2).jpg",
//     "/temp_restaurant_1/food-bg(3).jpg",
//     "/temp_restaurant_1/food(2).png",
//     "/temp_restaurant_1/food(4).png",
//     "/temp_restaurant_1/food(8).png",
//     "/temp_restaurant_1/food-bg(1).jpg",
//     "/temp_restaurant_1/food-bg(2).jpg",
//     "/temp_restaurant_1/food-bg(3).jpg",
//   ];

//   TempRestaurant1State(this.categoryDataList, {super.key});

//   @override
//   Widget build(BuildContext context) {
//     // GlobalSetting.ratioStyle = CalculateRatioTemplate(context, 'template1');
//     // ratioStyle = CalculateRatioRestaurant1(context);
//     GlobalSetting.ratioStyle = ratioStyle;
//     return Sizer(
//       builder: (context, orientation, deviceType) {
//         return Container(
//           decoration: BoxDecoration(
//             image: DecorationImage(
//               //Fixed
//               image: AssetImage('/temp_restaurant_1/BG.png'),
//               fit: BoxFit.fill,
//             ),
//           ),
//           child: Row(
//             children: [
//               // Cột 1
//               Expanded(
//                 child: Padding(
//                   padding: EdgeInsets.all(2.w),
//                   child: Column(
//                     children: [
//                       // Dòng 1
//                       Expanded(
//                         flex: 1,
//                         child: Container(
//                           margin: EdgeInsets.only(
//                               bottom: ratioStyle.marginBottomImage),
//                           child: Row(
//                             children: [
//                               // Cột 1: Danh mục 1 với sản phẩm
//                               // Expanded(
//                               //   flex: 2,
//                               //   child: Container(
//                               //     decoration: BoxDecoration(
//                               //       borderRadius: BorderRadius.circular(
//                               //           GlobalSetting
//                               //               .ratioStyle.boderRadiusStyle),
//                               //       color: Color(0x18FFFFFF),
//                               //     ),
//                               //     padding: EdgeInsets.all(1.5.w),
//                               //     child: categoryTitleSingleColumn(
//                               //       indexCategory: 0,
//                               //       useBackgroundCategoryTitle: false,
//                               //       categoryDataList: categoryDataList,
//                               //       colorCategoryTitle: colorCategoryTitle,
//                               //       startIndexProduct: 0,
//                               //       endIndexProduct: 3,
//                               //     ),
//                               //   ),
//                               // ),

//                               // Cột 2: khoảng cách
//                               SizedBox(width: 1.5.w),

//                               // Cột 3: 3 hình đại diện
//                               qtyWidgetInColumn(
//                                 imageList
//                                     .sublist(3, 6)
//                                     .map(
//                                       (imagePath) => image(
//                                         imagePath: imagePath,
//                                         fixCover: false,
//                                       ),
//                                     )
//                                     .toList(),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),

//                       SizedBox(height: 2.h),

//                       // Dòng 2: Danh mục 2: 3 hình ảnh, 5 sản phẩm
//                       // Expanded(
//                       //   flex: 1,
//                       //   child: categoryTitleGridLayout1(
//                       //     imageList: imageList,
//                       //     categoryData: categoryDataList.elementAt(1),
//                       //     useBackgroundCategoryTitle: false,
//                       //     colorPrice: Color(0xFFf47b22),
//                       //   ),
//                       // ),
//                     ],
//                   ),
//                 ),
//               ),

//               // Cột 2
//               Expanded(
//                 child: Padding(
//                   padding: EdgeInsets.all(2.w),
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     children: [
//                       // Dòng 1: 3 hình ảnh
//                       IntrinsicHeight(
//                         child: qtyWidgetInRow(
//                           imageList
//                               .sublist(0, 3)
//                               .map(
//                                 (imagePath) => image(
//                                   imagePath: imagePath,
//                                 ),
//                               )
//                               .toList(),
//                         ),
//                       ),

//                       // Dòng 2: Danh mục 3 và Danh mục 4
//                       twoCategoryInRow(
//                         categoryDataList: categoryDataList,
//                         indexCategory1: 2,
//                         indexCategory2: 3,
//                         useBackgroundCategoryTitle: false,
//                         startIndexProduct: 0,
//                         endIndexProduct: 3,
//                       ),

//                       // Dòng 3: Danh mục 5 và 1 hình đại diện không nền
//                       categoryWithImageInRow(
//                         categoryDataList: categoryDataList,
//                         indexCategory: 4,
//                         useBackgroundCategoryTitle: false,
//                         imagePath: '/temp_restaurant_1/food(7).png',
//                         fixCover: false,
//                         startIndexProduct: 0,
//                         endIndexProduct: 3,
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         );
//       },
//     );
//   }
// }


// // -------------------------------------- 
// // {
// // 	"templateSlug": "temp_restaurant_1",
// // 	"RatioSetting": {
// // 		'textStyle': {
// // 			'categoryNameStyle': {
// // 				'fontWeight': 'bold',
// // 				'fontFamily': 'bebasNeue',
// // 			},
// // 			'nameProductStyle': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',
// // 				'height': '1.5',
// // 			},
// // 			'nameProductStyleMedium': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',
// // 				'height': '1.5',
// // 			},
// // 			'discriptionProductStyle': {
// // 				'fontWeight': 'w300',
// // 				'fontFamily': 'openSans',
// // 				'height': '1.5',
// // 			},
// // 			'discriptionProductStyleMedium': {
// // 				'fontWeight': 'w300',
// // 				'fontFamily': 'openSans',
// // 				'height': '1.5',
// // 			},
// // 			'priceProductStyle': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',

// // 			},
// // 			'priceProductStyleMedium': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',

// // 			},
// // 		},
// // 		'renderScreen': {
// // 			'32/9': {
// // 				'categoryNameStyle': ' 2.w',
// // 				'nameProductStyle': ' 1.5.w',
// // 				'nameProductStyleMedium': ' 1.w',
// // 				'priceProductStyle': ' 1.5.w',
// // 				'priceProductStyleMedium': ' 1.w',
// // 				'discriptionProductStyle': ' 0.7.w',
// // 				'discriptionProductStyleMedium': ' 0.6.w',
// // 				'imageHeight': ' 14.h',
// // 				'marginBottomImage': ' 1.w',
// // 				'marginBottomProduct': ' 2.h',
// // 				'boderRadiusStyle': ' 100',
// // 				'marginBottomCategoryTitle': ' 1.h',
// // 				'paddingVerticalCategorTilte': ' 0',
// // 				'paddingHorizontalCategorTilte': ' 0',
// // 			},
// // 			'21/9': {
// // 				'categoryNameStyle': '1.7.w',
// // 				'nameProductStyle': '1.w',
// // 				'nameProductStyleMedium': '0.8.w',
// // 				'priceProductStyle': '1.w',
// // 				'priceProductStyleMedium': '0.8.w',
// // 				'discriptionProductStyle': '0.7.w',
// // 				'discriptionProductStyleMedium': '0.5.w',
// // 				'imageHeight': '13.h',
// // 				'marginBottomImage': '0.5.w',
// // 				'marginBottomProduct': '1.5.h',
// // 				'boderRadiusStyle': '30',
// // 				'marginBottomCategoryTitle': '1.5.h',
// // 				'paddingVerticalCategorTilte': '0',
// // 				'paddingHorizontalCategorTilte': '0',
// // 			},

// // 			'16/9': {
// // 				'categoryNameStyle': '2.w',
// // 				'nameProductStyle': '1.5.w',
// // 				'nameProductStyleMedium': '1.w',
// // 				'priceProductStyle': '1.5.w',
// // 				'priceProductStyleMedium': '1.w',
// // 				'discriptionProductStyle': '0.7.w',
// // 				'discriptionProductStyleMedium': '0.6.w',
// // 				'imageHeight': '14.h',
// // 				'marginBottomImage': '1.w',
// // 				'marginBottomProduct': '1.5.h',
// // 				'boderRadiusStyle': '30',
// // 				'marginBottomCategoryTitle': '2.h',
// // 				'paddingVerticalCategorTilte': '1.h',
// // 				'paddingHorizontalCategorTilte': '1.h',
// // 			},

// // 			'16/10': {
// // 				'categoryNameStyle': '2.5.w',
// // 				'nameProductStyle': '1.7.w',
// // 				'nameProductStyleMedium': '1.5.w',
// // 				'priceProductStyle': '1.7.w',
// // 				'priceProductStyleMedium': '1.5.w',
// // 				'discriptionProductStyle': '0.7.w',
// // 				'discriptionProductStyleMedium': '0.6.w',
// // 				'imageHeight': '18.h',
// // 				'marginBottomImage': '1.w',
// // 				'marginBottomProduct': '2.5.h',
// // 				'boderRadiusStyle': '30',
// // 				'marginBottomCategoryTitle': '1.h',
// // 				'paddingVerticalCategorTilte': '0',
// // 				'paddingHorizontalCategorTilte': '0',
// // 			},

// // 			'4/3': {
// // 				'categoryNameStyle': '2.5.w',
// // 				'nameProductStyle': '1.8.w',
// // 				'nameProductStyleMedium': '1.5.w',
// // 				'priceProductStyle': '1.8.w',
// // 				'priceProductStyleMedium': '1.5.w',
// // 				'discriptionProductStyle': '1.w',
// // 				'discriptionProductStyleMedium': '0.7.w',
// // 				'imageHeight': '14.h',
// // 				'marginBottomImage': '1.w',
// // 				'marginBottomProduct': '2.h',
// // 				'boderRadiusStyle': '30',
// // 				'marginBottomCategoryTitle': '1.h',
// // 				'paddingVerticalCategorTilte': '0',
// // 				'paddingHorizontalCategorTilte': '0',
// // 			},

// // 			'default': {
// // 				'categoryNameStyle': '2.w',
// // 				'nameProductStyle': '1.5.w',
// // 				'nameProductStyleMedium': '1.w',
// // 				'priceProductStyle': '1.5.w',
// // 				'priceProductStyleMedium': '1.w',
// // 				'discriptionProductStyle': '0.7.w',
// // 				'discriptionProductStyleMedium': '0.6.w',
// // 				'imageHeight': '14.h',
// // 				'marginBottomImage': '1.w',
// // 				'marginBottomProduct': '2.h',
// // 				'boderRadiusStyle': '30',
// // 				'marginBottomCategoryTitle': '2.h',
// // 			},
// // 		},
// // 	}
// // }