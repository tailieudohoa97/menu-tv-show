// import 'package:tv_menu/views/widgets/category_title.dart';
// import 'package:tv_menu/views/widgets/category_title_grid_layout_3.dart';
// import 'package:tv_menu/views/widgets/category_title_single_column.dart';

// import 'package:tv_menu/views/widgets/image.dart';
// import 'package:tv_menu/views/widgets/product_item.dart';
// import 'package:tv_menu/views/widgets/qty_widget_in_row.dart';
// import 'package:flutter/material.dart';
// import 'package:sizer/sizer.dart';
// import 'package:tv_menu/models/model_datas/category_data.dart';
// import 'package:tv_menu/helper/calculate_ratio_temp_restaurant_4.dart';

// import 'package:tv_menu/helper/global_setting.dart';

// /*
// // Used to:
//     Build the interface of an application displaying restaurant information based on categories and products.

// // Required inputs:
//   - ratioStyle: Dynamic parameter to determine interface ratios and sizes based on the device.
//   - categoryDataList: List of category and product data (type List<CategoryData>).
//   - useFullWidthCategoryTitle: Choose whether to use full width for the category title (default is false).
//   - useBackgroundCategoryTitle: Choose whether to use a background for the category title (default is true).
//   - pathImageBgCategoryTitle: Path to the background image for the category title (default is a specific image path).
//   - colorCategoryTitle: Color for the category title (default is black).
//   - colorProductName: Color for the product name (default is black).
//   - colorPrice: Color for the product price (default is red).
//   - colorDescription: Color for the product description (default is black).
//   - imageList: List of paths to product images.
//   - startIndexWidgetList: Starting index of the list of products and images to display in the column (default is 0).
//   - endIndexWidgetList: Ending index of the list of products and images to display in the column (default is 3).

// // Note:
//   - This code segment constructs an interface to display restaurant categories and their respective products in two columns.
//   - Each column uses the functions categoryTitleGridLayout3 and categoryTitleSingleColumn to display category and product information.
//   - Parameters for each column are passed through categoryDataList and imageList.
//   - Other parameters such as colors and the use of a background are configured using the corresponding parameters.
//   - Utilizes Sizer to automatically adjust the interface size based on the device.
// */

// class TempRestaurant4State extends StatelessWidget {
//   late CalculateRatioRestaurant4 ratioStyle;
//   bool useFullWidthCategoryTitle = false;
//   bool useBackgroundCategoryTitle = true;
//   String pathImageBgCategoryTitle =
//       '/temp_restaurant_4/decorate/BG-title-2.png';
//   Color colorCategoryTitle = Color(0xFF121212);
//   Color colorProductName = Color(0xFF121212);
//   Color colorPrice = Color(0xFF8d1111);
//   Color colorDescription = Color(0xFF121212);
//   // Tạo 1 biến danh sách danh mục với menu
//   List<CategoryData> categoryDataList;
//   List<String> imageList = [
//     "/temp_restaurant_4/food-bg(1).jpg",
//     "/temp_restaurant_4/food-bg(2).jpg",
//     "/temp_restaurant_4/food-bg(3).jpg",
//     "/temp_restaurant_4/food-bg(4).jpg",
//     "/temp_restaurant_4/food-bg(5).jpg",
//     "/temp_restaurant_4/food-bg(6).jpg",
//   ];

//   TempRestaurant4State(this.categoryDataList, {super.key});

//   @override
//   Widget build(BuildContext context) {
//     ratioStyle = CalculateRatioRestaurant4(context);
//     GlobalSetting.ratioStyle = ratioStyle;
//     return Sizer(
//       builder: (context, orientation, deviceType) {
//         return Container(
//           decoration: BoxDecoration(
//             image: DecorationImage(
//               //Fixed
//               image: AssetImage('/temp_restaurant_4/BG-2.png'),
//               fit: BoxFit.fill,
//             ),
//           ),
//           child: Row(
//             children: [
//               // Cột 1
//               Expanded(
//                 child: Padding(
//                   padding: EdgeInsets.all(2.w),
//                   child: Column(
//                     children: [
//                       // Dòng 1: Danh mục 1 và sản phẩm
//                       categoryTitleGridLayout3(
//                         categoryData: categoryDataList.elementAt(0),
//                         useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//                         pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//                         colorCategoryTitle: colorCategoryTitle,
//                         colorProductName: colorProductName,
//                         colorPrice: colorPrice,
//                         colorDescription: colorDescription,
//                         useFullWidthCategoryTitle: useFullWidthCategoryTitle,
//                       ),

//                       SizedBox(height: ratioStyle.marginBottomCategoryTitle),

//                       // Dòng 2: Danh mục 2 và sản phẩm
//                       Expanded(
//                         child: Column(
//                           children: [
//                             //Tiêu đề danh mục 2
//                             categoryTitle(
//                               catName:
//                                   categoryDataList.elementAt(1).categoryName,
//                               colorCategoryTitle: colorCategoryTitle,
//                               useBackgroundCategoryTitle:
//                                   useBackgroundCategoryTitle,
//                               pathImageBgCategoryTitle:
//                                   pathImageBgCategoryTitle,
//                               useFullWidthCategoryTitle:
//                                   useFullWidthCategoryTitle,
//                             ),

//                             //3 hình ảnh
//                             qtyWidgetInRow(
//                               imageList
//                                   .sublist(0, 3)
//                                   .map(
//                                     (imagePath) => image(
//                                       imagePath: imagePath,
//                                     ),
//                                   )
//                                   .toList(),
//                             ),

//                             IntrinsicHeight(
//                               child: qtyWidgetInRow(
//                                 categoryDataList
//                                     .elementAt(1)
//                                     .listMenu
//                                     .sublist(0, 3)
//                                     .map(
//                                       (item) => productItem(
//                                         name: item.productName,
//                                         price: item.price,
//                                         description: item.productDescription,
//                                         useMediumStyle: false,
//                                         colorPrice: colorPrice,
//                                         colorProductName: colorProductName,
//                                         colorDescription: colorDescription,
//                                       ),
//                                     )
//                                     .toList(),
//                               ),
//                             ),
//                           ],
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),

//               // Cột 2
//               Expanded(
//                 child: Padding(
//                   padding: EdgeInsets.all(2.w),
//                   child: Row(
//                     children: [
//                       //Cột 1
//                       Expanded(
//                         child: Column(
//                           mainAxisAlignment: MainAxisAlignment.start,
//                           children: [
//                             // Dòng 1: 1 Hình ảnh lấy từ "List<String> imageList"
//                             qtyWidgetInRow(
//                               imageList
//                                   .sublist(3, 4)
//                                   .map(
//                                     (imagePath) => image(
//                                       imagePath: imagePath,
//                                     ),
//                                   )
//                                   .toList(),
//                             ),

//                             SizedBox(
//                                 height: ratioStyle.marginBottomCategoryTitle),

//                             // Dòng 2: Danh mục 3 và sản phẩm
//                             categoryTitleSingleColumn(
//                               indexCategory: 2,
//                               useBackgroundCategoryTitle:
//                                   useBackgroundCategoryTitle,
//                               pathImageBgCategoryTitle:
//                                   pathImageBgCategoryTitle,
//                               categoryDataList: categoryDataList,
//                               colorCategoryTitle: colorCategoryTitle,
//                               colorProductName: colorProductName,
//                               colorPrice: colorPrice,
//                               colorDescription: colorDescription,
//                               startIndexProduct: 0,
//                               endIndexProduct: 3,
//                             ),

//                             SizedBox(
//                                 height: ratioStyle.marginBottomCategoryTitle),

//                             // Dòng 3: 1 Hình ảnh lấy từ "List<String> imageList"
//                             qtyWidgetInRow(
//                               imageList
//                                   .sublist(4, 5)
//                                   .map(
//                                     (imagePath) => image(
//                                       imagePath: imagePath,
//                                     ),
//                                   )
//                                   .toList(),
//                             ),
//                           ],
//                         ),
//                       ),

//                       SizedBox(width: 3.w),

//                       //Cột 2
//                       Expanded(
//                         child: Column(
//                           mainAxisAlignment: MainAxisAlignment.start,
//                           children: [
//                             // Dòng 1: Danh mục 4 và sản phẩm
//                             Expanded(
//                               child: categoryTitleSingleColumn(
//                                 indexCategory: 3,
//                                 useBackgroundCategoryTitle:
//                                     useBackgroundCategoryTitle,
//                                 pathImageBgCategoryTitle:
//                                     pathImageBgCategoryTitle,
//                                 categoryDataList: categoryDataList,
//                                 colorCategoryTitle: colorCategoryTitle,
//                                 colorProductName: colorProductName,
//                                 colorPrice: colorPrice,
//                                 colorDescription: colorDescription,
//                                 startIndexProduct: 0,
//                                 endIndexProduct: 3,
//                               ),
//                             ),

//                             SizedBox(
//                                 height: ratioStyle.marginBottomCategoryTitle),

//                             // Dòng 2: Danh mục 5 và sản phẩm
//                             Expanded(
//                               child: categoryTitleSingleColumn(
//                                 indexCategory: 4,
//                                 useBackgroundCategoryTitle:
//                                     useBackgroundCategoryTitle,
//                                 pathImageBgCategoryTitle:
//                                     pathImageBgCategoryTitle,
//                                 categoryDataList: categoryDataList,
//                                 colorCategoryTitle: colorCategoryTitle,
//                                 colorProductName: colorProductName,
//                                 colorPrice: colorPrice,
//                                 colorDescription: colorDescription,
//                                 startIndexProduct: 0,
//                                 endIndexProduct: 3,
//                               ),
//                             ),
//                           ],
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         );
//       },
//     );
//   }
// }


// // -------------------------------------- 
// // {
// // 	"templateSlug": "temp_restaurant_4",
// // 	"RatioSetting": {
// // 		'textStyle': {
// // 			'categoryNameStyle': {
// // 				'fontWeight': 'bold',
// // 				'fontFamily': 'bebasNeue',
// // 			},
// // 			'nameProductStyle': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',
// // 			},
// // 			'nameProductStyleMedium': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',
// // 			},
// // 			'discriptionProductStyle': {
// // 				'fontWeight': 'w300',
// // 				'fontFamily': 'openSans',
// // 			},
// // 			'discriptionProductStyleMedium': {
// // 				'fontWeight': 'w300',
// // 				'fontFamily': 'openSans',
// // 			},
// // 			'priceProductStyle': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',
// // 			},
// // 			'priceProductStyleMedium': {
// // 				'fontWeight': 'w800',
// // 				'fontFamily': 'bebasNeue',
// // 			},
// // 		},
// // 		'renderScreen': {
// // 			'32/9': {
//         // 'categoryNameStyle':'1.5.w',
//         // 'nameProductStyle':'1.w',
//         // 'nameProductStyleMedium':'0.6.w',
//         // 'priceProductStyle':'1.w',
//         // 'priceProductStyleMedium':'0.6.w',
//         // 'discriptionProductStyle':'0.6.w',
//         // 'discriptionProductStyleMedium':'0.5.w',
//         // 'imageHeight':'40.h',
//         // 'marginBottomImage':'0.7.w',
//         // 'marginBottomProduct':'1.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'2.h',
//         // 'paddingVerticalCategorTilte':'0.w',
//         // 'paddingHorizontalCategorTilte':'2.w',
//         // 'widthThumnailProduct':'3.w',
//         // 'heightThumnailProduct':'3.w',
// // 			},
// // 			'21/9': {
//         // 'categoryNameStyle':'2.w',
//         // 'nameProductStyle':'1.3.w',
//         // 'nameProductStyleMedium':'0.7.w',
//         // 'priceProductStyle':'1.3.w',
//         // 'priceProductStyleMedium':'0.7.w',
//         // 'discriptionProductStyle':'0.7.w',
//         // 'discriptionProductStyleMedium':'0.8.w',
//         // 'imageHeight':'40.h',
//         // 'marginBottomImage':'1.w',
//         // 'marginBottomProduct':'1.5.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'2.h',
//         // 'paddingVerticalCategorTilte':'0.w',
//         // 'paddingHorizontalCategorTilte':'2.w',
//         // 'widthThumnailProduct':'3.w',
//         // 'heightThumnailProduct':'3.w',
// // 			},

// // 			'16/9': {
//         // 'categoryNameStyle':'3.w',
//         // 'nameProductStyle':'1.5.w',
//         // 'nameProductStyleMedium':'1.w',
//         // 'priceProductStyle':'1.5.w',
//         // 'priceProductStyleMedium':'1.w',
//         // 'discriptionProductStyle':'1.w',
//         // 'discriptionProductStyleMedium':'0.8.w',
//         // 'imageHeight':'40.h',
//         // 'marginBottomImage':'1.w',
//         // 'marginBottomProduct':'2.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'3.h',
//         // 'paddingVerticalCategorTilte':'0.w',
//         // 'paddingHorizontalCategorTilte':'2.w',
//         // 'widthThumnailProduct':'3.w',
//         // 'heightThumnailProduct':'3.w',
// // 			},

// // 			'16/10': {
//         // 'categoryNameStyle':'3.w',
//         // 'nameProductStyle':'1.5.w',
//         // 'nameProductStyleMedium':'1.w',
//         // 'priceProductStyle':'1.5.w',
//         // 'priceProductStyleMedium':'1.w',
//         // 'discriptionProductStyle':'1.w',
//         // 'discriptionProductStyleMedium':'0.8.w',
//         // 'imageHeight':'40.h',
//         // 'marginBottomImage':'1.w',
//         // 'marginBottomProduct':'2.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'3.h',
//         // 'paddingVerticalCategorTilte':'0.w',
//         // 'paddingHorizontalCategorTilte':'2.w',
//         // 'widthThumnailProduct':'3.w',
//         // 'heightThumnailProduct':'3.w',
// // 			},

// // 			'4/3': {
//         // 'categoryNameStyle':'3.5.w',
//         // 'nameProductStyle':'2.w',
//         // 'nameProductStyleMedium':'1.3.w',
//         // 'priceProductStyle':'2.w',
//         // 'priceProductStyleMedium':'1.3.w',
//         // 'discriptionProductStyle':'1.3.w',
//         // 'discriptionProductStyleMedium':'0.8.w',
//         // 'imageHeight':'40.h',
//         // 'marginBottomImage':'1.3.w',
//         // 'marginBottomProduct':'2.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'3.h',
//         // 'paddingVerticalCategorTilte':'0.w',
//         // 'paddingHorizontalCategorTilte':'2.w',
//         // 'widthThumnailProduct':'3.w',
//         // 'heightThumnailProduct':'3.w',
// // 			},

// // 			'default': {
//         // 'categoryNameStyle':'3.w',
//         // 'nameProductStyle':'1.5.w',
//         // 'nameProductStyleMedium':'1.w',
//         // 'priceProductStyle':'1.5.w',
//         // 'priceProductStyleMedium':'1.w',
//         // 'discriptionProductStyle':'1.w',
//         // 'discriptionProductStyleMedium':'0.8.w',
//         // 'imageHeight':'40.h',
//         // 'marginBottomImage':'1.w',
//         // 'marginBottomProduct':'2.h',
//         // 'boderRadiusStyle':'30',
//         // 'marginBottomCategoryTitle':'3.h',
//         // 'paddingVerticalCategorTilte':'0.w',
//         // 'paddingHorizontalCategorTilte':'2.w',
//         // 'widthThumnailProduct':'3.w',
//         // 'heightThumnailProduct':'3.w',
// // 			},
// // 		},
// // 	}
// // }