import 'package:flutter/material.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';
import 'package:tv_menu/helper/module_helper.dart';

/// Build a column to display category info: name, image, item list,
/// in order from top to bottom
///
/// Layout: A column has a image, a category name, list of food row widget
/// arranged in order from top to bottom
///
/// Fields:
///   - category: Category object
///   - productRows: Product row widgets,
///   - categoryImageWidth: width of category image,
///   - categoryImageHeight: height of category image,
///   - elementGap: Spacing between image, category name and list of food row widget,
///   - categoryTextStyle: Style of category name,
///   - upperCaseCategoryName: to upper case of not for category name,
///   - mainAxisAlignment: Main axis alignment of column
class CategoryColumn1 extends StatelessWidget {
  // default values
  static const double _defaultElementGap = 16;
  static const TextStyle _defaultCategoryTextStyle = TextStyle(
    fontSize: 22,
    fontWeight: FontWeight.bold,
  );

  final CategoryData category;
  final double? categoryImageWidth;
  final double? categoryImageHeight;
  final double elementGap;
  final TextStyle categoryTextStyle;
  final List<Widget> productRows;
  final bool upperCaseCategoryName;
  // [102302Tin] Add mainAxisAlignment, crossAxisAlignment properties
  final MainAxisAlignment mainAxisAlignment;
  final CrossAxisAlignment crossAxisAlignment;

  const CategoryColumn1({
    super.key,
    required this.category,
    this.productRows = const [],
    this.categoryImageWidth,
    this.categoryImageHeight,
    double? elementGap,
    this.categoryTextStyle = _defaultCategoryTextStyle,
    this.upperCaseCategoryName = false,
    this.mainAxisAlignment = MainAxisAlignment.start,
    this.crossAxisAlignment = CrossAxisAlignment.start,
  }) : elementGap = elementGap ?? _defaultElementGap;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: crossAxisAlignment,
      mainAxisAlignment: mainAxisAlignment,
      children: [
        if (category.categoryImage != null &&
            (category.categoryImage as String).isNotEmpty)
          Container(
            width: categoryImageWidth ?? double.maxFinite,
            height: categoryImageHeight,
            decoration: const BoxDecoration(),
            clipBehavior: Clip.hardEdge,
            child: Image(
              image: ModuleHelper.getImageProvider(category.categoryImage),
              fit: BoxFit.cover,
            ),
          ),
        SizedBox(height: elementGap),
        Text(
          upperCaseCategoryName
              ? category.categoryName.toUpperCase()
              : category.categoryName,
          style: categoryTextStyle,
        ),
        if (productRows.isNotEmpty) ...[
          SizedBox(height: elementGap),
          ...productRows,
        ]
      ],
    );
  }
}
