import 'package:flutter/material.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/menu_item_price_1.dart';

/// An indexed and horizontal menu item
///
/// Layout: A row includes index, name and price of menu item,
/// arranged horizontally
///
/// Fields:
///   + index: Index of menu item (start at 1)
///   + product: Product data
///   + textStyle: Text style for all text in menu item, except menu item price
///   + priceTextStyle: Text style for menu item price
///   + elementGap: Spacing between index, name and price
///   + indexChipBgColor: Background color of chip that wraps index of menu item
///   + indexChipFgColor: Foreground color of chip that wraps index of menu item
///   + indexChipRadius: Radius of chip that wraps index of menu item
class IndexedMenuItem1 extends StatelessWidget {
  // default values
  static const double _defaultElementGap = 16;
  static const Color _defaultIndexChipBgColor = Colors.black;
  static const Color _defaultIndexChipFgColor = Colors.white;
  static const double _defaultIndexChipRadius = 16;

  final int index;
  final ProductData product;
  final TextStyle? textStyle;
  final double elementGap;
  final TextStyle? priceTextStyle;
  final Color indexChipBgColor;
  final Color indexChipFgColor;
  final double indexChipRadius;

  const IndexedMenuItem1({
    super.key,
    this.index = 1,
    required this.product,
    this.textStyle,
    this.priceTextStyle,
    double? elementGap,
    Color? indexChipBgColor = _defaultIndexChipBgColor,
    Color? indexChipFgColor = _defaultIndexChipFgColor,
    double? indexChipRadius = _defaultIndexChipRadius,
  })  : elementGap = elementGap ?? _defaultElementGap,
        indexChipBgColor = indexChipBgColor ?? _defaultIndexChipBgColor,
        indexChipFgColor = indexChipFgColor ?? _defaultIndexChipFgColor,
        indexChipRadius = indexChipRadius ?? _defaultIndexChipRadius;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        CircleAvatar(
          backgroundColor: indexChipBgColor,
          foregroundColor: indexChipFgColor,
          radius: indexChipRadius,
          child: Text(index.toString(), style: textStyle),
        ),
        SizedBox(width: elementGap),
        Expanded(child: Text(product.productName, style: textStyle)),
        SizedBox(width: elementGap),
        MenuItemPrice1(
          price: num.parse(product.price),
          textStyle: priceTextStyle,
          prefix: '\$',
        ),
      ],
    );
  }
}
