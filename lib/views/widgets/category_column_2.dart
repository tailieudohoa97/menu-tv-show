import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';

/// Used to: Build a category column with name, image and item list of category
///
/// Layout: A column with 3 rows
///   - Row 1: Category name be wrapped a container
///   - Row 2: A image of category
///   - Row 3: Item list of category, arranged vertically
///
/// Fields:
///   - category: Category data
///   - categoryNameTextStyle: Text style of category name
///   - categoryNameContainerDecoration: Decoration of box that contains category name
///   - categoryNameContainnerPadding: Padding of box that contains category name
///   - categoryImageHeight: Height of category image
///   - categoryImageWidth: Width of  of category image
///   - categoryImageBorderRadius: Border radius of category image
///   - categoryItems: Category item list
///   - elementGap: Spacing between name, image, item list of category
///   - upperCaseCategoryName: Uppercase for category name or not
///   - hideScrollbarWhenOverflow: Hide scrollbar when overflow or not

class CategoryColumn2 extends StatelessWidget {
  // default values
  static const double _defaultElementGap = 16;

  final CategoryData category;
  final TextStyle categoryNameTextStyle;
  final Decoration categoryNameContainerDecoration;
  final EdgeInsetsGeometry categoryNameContainnerPadding;
  final bool upperCaseCategoryName;
  final double? categoryImageHeight;
  final double? categoryImageWidth;
  final BorderRadiusGeometry? categoryImageBorderRadius;
  final List<Widget> categoryItems;
  final double elementGap;
  final bool hideScrollbarWhenOverflow;

  const CategoryColumn2({
    super.key,
    required this.category,
    this.categoryNameTextStyle = const TextStyle(),
    this.categoryNameContainerDecoration = const BoxDecoration(),
    this.categoryNameContainnerPadding = const EdgeInsets.all(8),
    this.categoryImageHeight,
    this.categoryImageWidth,
    this.categoryImageBorderRadius,
    this.categoryItems = const [],
    double? elementGap,
    this.upperCaseCategoryName = false,
    this.hideScrollbarWhenOverflow = false,
  }) : elementGap = elementGap ?? _defaultElementGap;

  @override
  Widget build(BuildContext context) {
    final scrollbarBehavior = ScrollConfiguration.of(context).copyWith(
      scrollbars: !hideScrollbarWhenOverflow,
    );

    return ScrollConfiguration(
      behavior: scrollbarBehavior,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: categoryNameContainnerPadding,
              decoration: categoryNameContainerDecoration,
              child: Center(
                child: Text(
                  upperCaseCategoryName
                      ? category.categoryName.toUpperCase()
                      : category.categoryName,
                  style: categoryNameTextStyle,
                ),
              ),
            ),
            SizedBox(height: elementGap),
            if (category.categoryImage != null) ...[
              Container(
                height: categoryImageHeight,
                width: categoryImageWidth,
                clipBehavior: Clip.hardEdge,
                decoration:
                    BoxDecoration(borderRadius: categoryImageBorderRadius),
                child: Image(
                  image: ModuleHelper.getImageProvider(category.categoryImage),
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(height: elementGap),
            ],
            ...categoryItems,
          ],
        ),
      ),
    );
  }
}
