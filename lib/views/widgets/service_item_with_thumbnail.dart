import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';

/// Used to build a column display name, price,thumbnail and desciption of product
///
/// Layout: Has 3 rows
///   _ Row 1: Contains name and price
///   - Row 2: Contains thumbnail
///   - Row 3: Contains desciption of product
///
/// Fields:
///   - name: product name
///   - price: product price
///   - description: product description
///   - pathThumnailProduct: thummail product path
///   - colorProductName
///   - colorPrice
///   - colorProductDescription
///   - maxLineOfProductName: Maximun line allowed of row
///   - maxLineOfProductDescription: Maximun line allowed of row
///   - width: width of column
///   - useMediumStyle: use medium style or not
///   - upperCaseProductName: to upper case or not for product name
Widget serviceItemWithThumbnail({
  required String name,
  bool toUpperCaseNameProductName = true,
  required String price,
  required String description,
  bool useMediumStyle = false, // Tham số quyết định style sử dụng
  Color colorPrice = const Color(0xFFFFFFFF),
  Color colorProductName = const Color(0xFFFFFFFF),
  Color colorDescription = const Color(0xFFFFFFFF),
  String pathThumnailProduct = GlobalSetting.noImage,
  int maxLineProductName = 1,
  int maxLineProductDescription = 2,
}) {
  return SizedBox(
    width: GlobalSetting.ratioStyle.getField('widthThumnailProduct'),
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            //service name
            Text(
              toUpperCaseNameProductName ? name.toUpperCase() : name,
              style: useMediumStyle
                  ? GlobalSetting.ratioStyle
                      .getTextStyle('nameProductStyleMedium')
                      .copyWith(height: 1.5, color: colorProductName)
                  : GlobalSetting.ratioStyle
                      .getTextStyle('nameProductStyle')
                      .copyWith(height: 1.5, color: colorProductName),
              maxLines: maxLineProductName,
            ),
            //service price
            Text(
              price.toUpperCase(),
              style: useMediumStyle
                  ? GlobalSetting.ratioStyle
                      .getTextStyle('priceProductStyleMedium')
                      .copyWith(color: colorPrice)
                  : GlobalSetting.ratioStyle
                      .getTextStyle('priceProductStyle')
                      .copyWith(color: colorPrice),
              maxLines: maxLineProductName,
            ),
          ],
        ),
        SizedBox(height: 1.h),
        //service thumbnail
        ModuleHelper.getImage(
          pathThumnailProduct,
          height: GlobalSetting.ratioStyle.getField('heightThumnailProduct'),
          width: GlobalSetting.ratioStyle.getField('widthThumnailProduct'),
          fit: BoxFit.cover,
        ),
        //service description
        Text(
          description,
          textAlign: TextAlign.center,
          style: useMediumStyle
              ? GlobalSetting.ratioStyle
                  .getTextStyle('discriptionProductStyleMedium')
                  .copyWith(height: 1.5, color: colorDescription)
              : GlobalSetting.ratioStyle
                  .getTextStyle('discriptionProductStyle')
                  .copyWith(height: 1.5, color: colorDescription),
          maxLines: maxLineProductDescription,
        ),
      ],
    ),
  );
}
