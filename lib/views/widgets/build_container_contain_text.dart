import 'package:flutter/material.dart';

//092303 Tuan create container_contain_text
/*
//usage:
    * a simple container containing text

//layout will dislay:
  a text inside a custom container


//input: all parameters are required
  * height, width, color of the container is required
  * text, and style of the text alse is required
  * textAlign and padding is optional, 
  the default value of textAlign is center and padding's is EdgeInsets.all(0)
*/

Widget buildContainerContainText({
  required double height,
  required double width,
  required TextStyle style,
  required Color backgroundColor,
  required String text,
  TextAlign? textAlign,
  EdgeInsetsGeometry? padding,
}) {
  return Container(
    color: backgroundColor,
    width: width,
    height: height,
    padding: padding ?? const EdgeInsets.all(0),
    child: Center(
        child: Text(
      text.toUpperCase(),
      style: style,
      textAlign: textAlign ?? TextAlign.center,
    )),
  );
}
