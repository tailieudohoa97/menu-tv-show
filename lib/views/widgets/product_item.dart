import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';

/*
// Used to:
    Create an interface element displaying product information, including product name, price, and description.

// Layout will display:
    - A row containing product information including:
        + Product name
        + Product description
        + Product price
        + (Optional) Product thumbnail image

// Required inputs:
  - ratioStyle: Scale and style for interface elements.
  - name: Product name (string) to be displayed.
  - toUpperCaseNameProductName: Determine whether the product name should be displayed in uppercase (default is true).
  - price: Product price (string) to be displayed.
  - description: Product description (string) to be displayed.
  - useMediumStyle: Parameter determining the style for the product (default is false).
  - colorPrice: Color for the product price (default is white).
  - colorProductName: Color for the product name (default is white).
  - colorDescription: Color for the product description (default is white).
  - useThumnailProduct: Determine whether to use a product thumbnail image (default is false).
  - pathThumnailProduct: Path to the product thumbnail image (string).
*/

Widget productItem({
  required String name,
  bool toUpperCaseNameProductName = true,
  required String price,
  required String description,
  bool useMediumStyle = false, // Tham số quyết định style sử dụng
  Color colorPrice = const Color(0xFFFFFFFF),
  Color colorProductName = const Color(0xFFFFFFFF),
  Color colorDescription = const Color(0xFFFFFFFF),
  bool useThumnailProduct = false,
  String pathThumnailProduct = GlobalSetting.noImage,
  int maxLineProductName = 1,
  int maxLineProductDescription = 2,
}) {
  //092314LH Fix dynamic sample data to template
  return Container(
    margin: EdgeInsets.only(
        bottom: GlobalSetting.ratioStyle.getField('marginBottomProduct')),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        //Hình thumbnail của sản phẩm
        if (useThumnailProduct)
          Container(
            margin: EdgeInsets.only(right: 1.w, top: 1.h),
            decoration: BoxDecoration(
              color: Colors.amber,
              borderRadius: BorderRadius.circular(
                  GlobalSetting.ratioStyle.getField('boderRadiusStyle') / 3),
            ),

            //CẦN 1 list LỌC ĐƯỢC THUMNAIL CỦA TỪNG SẢN PHẨM...
            child: ModuleHelper.getImage(
              pathThumnailProduct,
              height:
                  GlobalSetting.ratioStyle.getField('heightThumnailProduct'),
              width: GlobalSetting.ratioStyle.getField('widthThumnailProduct'),
            ),
          ),
        Expanded(
          flex: 3,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Tên sản phẩm
              Padding(
                padding: EdgeInsets.only(right: 2.w),
                child: Text(
                  toUpperCaseNameProductName ? name.toUpperCase() : name,
                  style: useMediumStyle
                      ? GlobalSetting.ratioStyle
                          .getTextStyle('nameProductStyleMedium')
                          .copyWith(height: 1.5, color: colorProductName)
                      : GlobalSetting.ratioStyle
                          .getTextStyle('nameProductStyle')
                          .copyWith(height: 1.5, color: colorProductName),
                  maxLines: maxLineProductName,
                ),
              ),

              // Mô tả sản phẩm
              Padding(
                padding: EdgeInsets.only(top: 0.2.w, right: 2.w),
                child: Text(
                  description,
                  style: useMediumStyle
                      ? GlobalSetting.ratioStyle
                          .getTextStyle('discriptionProductStyleMedium')
                          .copyWith(height: 1.5, color: colorDescription)
                      : GlobalSetting.ratioStyle
                          .getTextStyle('discriptionProductStyle')
                          .copyWith(height: 1.5, color: colorDescription),
                  maxLines: maxLineProductDescription,
                ),
              ),
            ],
          ),
        ),

        //Giá sản phẩm
        Container(
          constraints: BoxConstraints(minWidth: 3.w),
          child: Text(
            price.toUpperCase(),
            style: useMediumStyle
                ? GlobalSetting.ratioStyle
                    .getTextStyle('priceProductStyleMedium')
                    .copyWith(color: colorPrice)
                : GlobalSetting.ratioStyle
                    .getTextStyle('priceProductStyle')
                    .copyWith(color: colorPrice),
            maxLines: maxLineProductName,
          ),
        ),
      ],
    ),
  );
}
