import 'package:flutter/material.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';

/*
// Used to:
    Create a category title with an option to use a background image or background color.

// Layout will display:
    - A category title with an optional background image or background color.

// Required inputs:
  - ratioStyle: Scale and style for interface elements.
  - catName: Name of the product category (string) to be displayed.
  - useBackgroundCategoryTitle: Determines whether to use a background image or background color for the category title (default is true).
  - pathImageBgCategoryTitle: Path to the background image for the category title (string).
  - colorCategoryTitle: Color for the category title text (default is white).
  - colorBackgroundCategoryTitle: Background color for the category title (default is transparent).

// Note:
  - If useBackgroundCategoryTitle is set to true,
    the category title will have a background image or background color.
    Otherwise, the title will have no background.
*/

Widget categoryTitle({
  required catName,
  bool useBackgroundCategoryTitle = true,
  bool useFullWidthCategoryTitle = false,
  String pathImageBgCategoryTitle = "",
  Color colorCategoryTitle = const Color(0xFFFFFFFF),
  Color colorBackgroundCategoryTitle = const Color(0x00FFFFFF), //Trong suốt
}) {
  return Container(
    margin: EdgeInsets.only(
        bottom: GlobalSetting.ratioStyle.getField('marginBottomCategoryTitle')),
    padding: EdgeInsets.symmetric(
      vertical:
          GlobalSetting.ratioStyle.getField('paddingVerticalCategorTilte'),
      horizontal:
          GlobalSetting.ratioStyle.getField('paddingHorizontalCategorTilte'),
    ),
    //Sử dụng background cho title hay không?
    decoration: useBackgroundCategoryTitle
        ? BoxDecoration(
            image: pathImageBgCategoryTitle == ''
                ? null
                : ModuleHelper.getImageDecoration(
                    pathImageBgCategoryTitle,
                    boxFit: BoxFit.fill,
                  ),
            color: colorBackgroundCategoryTitle,
            borderRadius: BorderRadius.circular(
                GlobalSetting.ratioStyle.getField('boderRadiusStyle')),
          )
        : null,
    child: useFullWidthCategoryTitle
        ? Center(
            child: Text(
              catName.toUpperCase(),
              style: GlobalSetting.ratioStyle
                  .getTextStyle('categoryNameStyle')
                  .copyWith(color: colorCategoryTitle),
            ),
          )
        : Text(
            catName.toUpperCase(),
            style: GlobalSetting.ratioStyle
                .getTextStyle('categoryNameStyle')
                .copyWith(color: colorCategoryTitle),
          ),
  );
}
