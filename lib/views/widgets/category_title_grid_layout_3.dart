import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/category_title.dart';
import 'package:flutter/material.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';

import 'package:tv_menu/views/widgets/product_item.dart';
import 'package:sizer/sizer.dart';

/*
// Used to:
    Create an interface displaying information about a product category, including the category title, a list of products, and the option to use a background for the category title.

// Layout will display:
- Row 1: Title of the product category with an option to use a background.
- Row 2: A horizontal row containing 2 columns:
  + Column 1: List of products (first 3 products).
  + Column 2: List of products (next 3 products).

// Required inputs:
  - ratioStyle: Scale and style for interface elements.
  - categoryData: Information about the product category (type CategoryData).
  - useBackgroundCategoryTitle: Determines whether to use a background for the category title (default is true).
  - pathImageBgCategoryTitle: Path to the background image for the category title (string).
  - colorCategoryTitle: Color for the category title (default is white).
  - colorBackgroundCategoryTitle: Background color for the category title (default is transparent).
  - colorPrice: Color for product prices (default is white).
  - colorProductName: Color for product names (default is white).
  - colorDescription: Color for product descriptions (default is white).
  - productList: List of product data
*/
// [092326Tin] Change listMenu in categoryData to productList
Widget categoryTitleGridLayout3({
  required CategoryData categoryData,
  List<ProductData> productList = const [],
  bool useBackgroundCategoryTitle = true,
  bool useFullWidthCategoryTitle = false,
  String pathImageBgCategoryTitle = '',
  Color colorCategoryTitle = const Color(0xFFFFFFFF),
  Color colorBackgroundCategoryTitle = const Color(0x00FFFFFF),
  Color colorPrice = const Color(0xFFFFFFFF),
  Color colorProductName = const Color(0xFFFFFFFF),
  Color colorDescription = const Color(0xFFFFFFFF),
}) {
  //Trong 1 Danh mục lấy sp1, sp2, sp3
  List<Widget> productWidgetList1 = productList
      .sublist(0, 3)
      .map(
        (item) => productItem(
          name: item.productName,
          price: item.price,
          description: item.productDescription,
          useMediumStyle: false,
          colorPrice: colorPrice,
          colorProductName: colorProductName,
          colorDescription: colorDescription,
        ),
      )
      .toList();

  //Trong 1 Danh mục lấy sp4, sp5, sp6
  List<Widget> productWidgetList2 = productList
      .sublist(3, 6)
      .map(
        (item) => productItem(
          name: item.productName,
          price: item.price,
          description: item.productDescription,
          useMediumStyle: false,
          colorPrice: colorPrice,
          colorProductName: colorProductName,
          colorDescription: colorDescription,
        ),
      )
      .toList();
  return Expanded(
    child: Column(
      children: [
        // Row 1: Tiêu đề của danh mục sản phẩm với tùy chọn sử dụng nền.
        categoryTitle(
          catName: categoryData.categoryName,
          useBackgroundCategoryTitle: useBackgroundCategoryTitle,
          pathImageBgCategoryTitle: pathImageBgCategoryTitle,
          colorCategoryTitle: colorCategoryTitle,
          colorBackgroundCategoryTitle: colorBackgroundCategoryTitle,
          useFullWidthCategoryTitle: useFullWidthCategoryTitle,
        ),

        // Row 2: 2 cột
        Expanded(
          child: Row(
            children: [
              //Cột 1: Danh sách sản phẩm (3 sản phẩm đầu tiên)
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: productWidgetList1,
                ),
              ),

              SizedBox(width: 3.w),

              //Cột 2: Danh sách sản phẩm (3 sản phẩm tiếp theo)
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: productWidgetList2,
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
