import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

/*
// Used to:
    Create a column of multiple child elements (widgets) and arrange them in a vertical stack within a column.

// Layout will display:
    - A column containing widgets listed in rows.
    - The widgets will be vertically aligned along the Y-axis (vertical) of the column.

// Required inputs:
  - widgets: List of widgets to be displayed in the column (type List<Widget>).
  - start: Index of the first widget to be displayed (default is 0).
  - end: Index of the last widget to be displayed (default is 0, meaning display from start to the end of the list).

// Note:
  - This method takes a list of widgets and arranges them into a column, with the option to display only a portion of the list by specifying start and end.
  - Each widget in the list will be wrapped in an Expanded element so that they can expand to fill the vertical space of the column.
  - Each widget will be separated from other widgets by inserting a SizedBox with a width of 0 to eliminate spacing between them.
  - The last element (SizedBox) will be removed from the list to avoid creating unwanted spacing at the bottom of the column.
*/

Widget qtyWidgetInColumn(List<Widget>? widgets, [int start = 0, int end = 0]) {
  widgets = end != 0 ? widgets!.sublist(start, end) : widgets!.sublist(start);
  widgets = widgets
      .expand((widget) => [Expanded(child: widget), SizedBox(width: 0.w)])
      .toList();
  widgets.removeLast();
  return Expanded(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: widgets,
    ),
  );
}
