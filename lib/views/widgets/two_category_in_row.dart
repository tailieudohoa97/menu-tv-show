// import 'package:flutter/material.dart';
// import 'package:sizer/sizer.dart';
// import 'package:tv_menu/helper/module_helper.dart';
// import 'package:tv_menu/models/model_datas/category_data.dart';
// import 'package:tv_menu/views/widgets/category_title_single_column.dart';
// import 'package:tv_menu/helper/global_setting.dart';

// /*
// // Used to:
//     Create a row of interface elements containing two categories and their respective products, arranged into columns within the row.

// // Layout will display:
//     - A row containing two categories and their respective products, arranged into two columns.
//     - The categories and products will be horizontally aligned along the X-axis (horizontal) of the row.
//     - A horizontal separator will separate the two categories and their products within the row.

// // Required inputs:
//   - ratioStyle: Dynamic parameter to determine interface ratios and sizes based on the device.
//   - categoryDataList: List of category and product data (type List<CategoryData>).
//   - indexCategory1: Index of the first category to be displayed from the list.
//   - indexCategory2: Index of the second category to be displayed from the list.
//   - useBackgroundCategoryTitle: Choose to use a background for the category title or not (default is true).
//   - useFullWidthCategoryTitle: Choose to use the full width for the category title or not (default is false).
//   - pathImageBgCategoryTitle: Path to the background image of the category title (default is a specific image path).
//   - colorCategoryTitle: Color for the category title (default is white).
//   - colorBackgroundCategoryTitle: Color for the background of the category title (default is transparent).
//   - colorPrice: Color for the product price (default is white).
//   - colorProductName: Color for the product name (default is white).
//   - colorDescription: Color for the product description (default is white).
//   - useMediumStyle: Parameter to decide whether to use the medium interface style or not (default is false).
//   - useThumnailProduct: Choose to use product thumbnails or not (default is false).
//   - pathThumnailProduct: Path to the product thumbnail (default is a specific image path or a default image).
//   - toUpperCaseNameProductName: Choose whether to capitalize the product name or not (default is true).
//   - startIndexProduct: Starting index of the product list to display (default is 0).
//   - endIndexProduct: Ending index of the product list to display (default is 3).

// // Note:
//   - This method creates a row of interface elements containing two categories and their respective products, arranged into two columns.
//   - Column 1 will contain the category and products of Category 1, and Column 2 will contain the category and products of Category 2.
//   - The two columns will be horizontally aligned along the X-axis of the row and separated by a horizontal separator.
// */

// Widget twoCategoryInRow({
//   required List<CategoryData> categoryDataList,
//   required int indexCategory1,
//   required int indexCategory2,
//   bool useBackgroundCategoryTitle = true,
//   bool useFullWidthCategoryTitle = false,
//   String pathImageBgCategoryTitle = '',
//   Color colorCategoryTitle = const Color(0xFFFFFFFF),
//   Color colorBackgroundCategoryTitle = const Color(0x00FFFFFF),
//   Color colorPrice = const Color(0xFFFFFFFF),
//   Color colorProductName = const Color(0xFFFFFFFF),
//   Color colorDescription = const Color(0xFFFFFFFF),
//   bool useMediumStyle = false, // Tham số quyết định style sử dụng

//   bool useThumnailProduct = false,
//   String pathThumnailProduct = GlobalSetting.noImage,
//   bool toUpperCaseNameProductName = true,
//   int startIndexProduct = 0,
//   int endIndexProduct = 3,
// }) {
//   return Expanded(
//     child: Row(
//       crossAxisAlignment: CrossAxisAlignment.stretch,
//       children: [
//         // Cột 1: Danh mục với sản phẩm
//         Expanded(
//           child: categoryTitleSingleColumn(
//             categoryData: categoryDataList[indexCategory1],
//             productData: categoryDataList[indexCategory1].listMenu,
//             useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//             pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//             colorCategoryTitle: colorCategoryTitle,
//             colorBackgroundCategoryTitle: colorBackgroundCategoryTitle,
//             colorProductName: colorProductName,
//             colorPrice: colorPrice,
//             colorDescription: colorDescription,
//             useMediumStyle: useMediumStyle,
//             useThumnailProduct: useThumnailProduct,
//             pathThumnailProduct: pathThumnailProduct,
//             toUpperCaseNameProductName: toUpperCaseNameProductName,
//             // startIndexProduct: startIndexProduct,
//             // endIndexProduct: endIndexProduct,
//           ),
//         ),

//         //Module Fixed - dấu gạch ngang
//         Container(
//           width: 5.w,
//           padding: EdgeInsets.symmetric(vertical: 2.w),
//           child: ModuleHelper.getImage(
//             '/temp_restaurant_1/decorate/Seperator-Line.png',
//             fit: BoxFit.contain,
//           ),
//         ),

//         // Cột 2: Danh mục với sản phẩm
//         Expanded(
//           child: categoryTitleSingleColumn(
//             categoryData: categoryDataList[indexCategory2],
//             productData: categoryDataList[indexCategory2].listMenu,
//             useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//             pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//             colorCategoryTitle: colorCategoryTitle,
//             colorBackgroundCategoryTitle: colorBackgroundCategoryTitle,
//             colorProductName: colorProductName,
//             colorPrice: colorPrice,
//             colorDescription: colorDescription,
//             useMediumStyle: useMediumStyle,
//             useThumnailProduct: useThumnailProduct,
//             pathThumnailProduct: pathThumnailProduct,
//             toUpperCaseNameProductName: toUpperCaseNameProductName,
//             // startIndexProduct: startIndexProduct,
//             // endIndexProduct: endIndexProduct,
//           ),
//         ),
//       ],
//     ),
//   );
// }
