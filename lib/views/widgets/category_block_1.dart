import 'package:flutter/widgets.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';

/// Used to build a bolck contains image, name, and list of category item rows
///
/// Layout: Has 2 columns
///   - Column 1: A image of category (optional)
///   - Column 2: Has 2 rows
///     + Row 1: Name of category
///     + Row 2: List of category items, arranged vertically
///
/// Fields:
///   - category: Category object
///   - categoryTextStyle: Text style for category name
///   - width: with of block
///   - height: height of block
///   - elementGap: Spacing of 2 columns of block
///   - categoryItemRows: Widgets for category item
///   - categoryImage: Widget for category image
class CategoryBlock1 extends StatelessWidget {
  static const double _defaultElementGap = 16;

  final CategoryData category;
  final TextStyle? categoryTextStyle;
  final double? width;
  final double? height;
  final double? elementGap;
  final List<Widget> categoryItemRows;
  final Widget? categoryImage;

  const CategoryBlock1({
    super.key,
    required this.category,
    this.categoryTextStyle,
    this.width,
    this.height,
    this.elementGap,
    this.categoryItemRows = const [],
    this.categoryImage,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (categoryImage != null) ...[
            categoryImage as Widget,
            SizedBox(width: elementGap ?? _defaultElementGap),
          ],
          Expanded(
            child: Column(
              children: [
                Text(category.categoryName, style: categoryTextStyle),
                ...categoryItemRows,
              ],
            ),
          )
        ],
      ),
    );
  }
}
