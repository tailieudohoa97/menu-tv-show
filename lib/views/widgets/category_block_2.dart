import 'package:flutter/material.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';

/// Used to build a block to display category info: name, image, item list,
/// in order from top to bottom
///
/// Layout: Has 4 rows
///   - Row 1: Category name
///   - Row 2: Category description if any
///   - Row 3: divider if any
///   - Row 4: List of food rows and category image,
///   that divided into 2 columns and arranged from top to bottom and left to right
///
/// Fields:
///   - category: Category object
///   - upperCaseCategoryName: to upper case or not for category name
///   - categoryNameTextStyle: style for category name
///   - categoryDescriptionTextStyle: style for category description
///   - categoryItemGap: Spacing of category item
///   - categoryItems: List of widget for category item
///   - indentCategoryDescription: px count for indent of category description
///   - categoryItemSectionPadding: padding of row that contains category items
///   - divider: Divider between row contains category items and rows contains category info
class CategoryBlock2 extends StatelessWidget {
  static const double _defaultCategoryItemGap = 16;
  final CategoryData category;
  final bool upperCaseCategoryName;
  final TextStyle categoryNameTextStyle;
  final TextStyle categoryDescriptionTextStyle;
  final double? categoryItemGap;
  final List<Widget> categoryItems;
  final double indentCategoryDescription;
  final double categoryItemSectionPadding;
  final Widget? divider;

  const CategoryBlock2({
    super.key,
    required this.category,
    this.upperCaseCategoryName = false,
    this.categoryNameTextStyle = const TextStyle(),
    this.categoryDescriptionTextStyle = const TextStyle(),
    this.categoryItemGap,
    this.categoryItems = const [],
    this.indentCategoryDescription = 0,
    this.categoryItemSectionPadding = 0,
    this.divider,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          category.categoryName.toUpperCase(),
          style: categoryNameTextStyle,
        ),
        if (category.categoryDescription != null)
          Padding(
            padding:
                EdgeInsets.symmetric(horizontal: indentCategoryDescription),
            child: Text(
              category.categoryDescription as String,
              style: categoryDescriptionTextStyle,
              textAlign: TextAlign.center,
            ),
          ),
        if (divider != null) divider as Widget,
        Expanded(
          child: Padding(
            padding: EdgeInsets.all(categoryItemSectionPadding),
            child: SizedBox(
              width: double.maxFinite,
              height: double.maxFinite,
              child: Wrap(
                alignment: WrapAlignment.start,
                runAlignment: WrapAlignment.start,
                spacing: categoryItemGap ?? _defaultCategoryItemGap,
                runSpacing: categoryItemGap ?? _defaultCategoryItemGap,
                direction: Axis.vertical,
                clipBehavior: Clip.hardEdge,
                children: categoryItems,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
