import 'package:sizer/sizer.dart';

class SizingValue {
  static double fromString(String stringSize) {
    final parts = stringSize.split(".");
    if (parts.length < 2) {
      return double.parse(stringSize);
    }

    final unit = parts.last;
    double value;
    switch (unit) {
      case 'w':
        parts.removeLast();
        value = double.parse(parts.join("."));
        return value.w;
      case 'h':
        parts.removeLast();
        value = double.parse(parts.join("."));
        return value.h;
      default:
        return double.parse(parts.join("."));
    }
  }
}
