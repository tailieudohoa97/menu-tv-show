import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

/*
// Used to:
    Create a row of interface elements containing a list of widgets and arrange them into columns within the row.

// Layout will display:
    - A row containing widgets listed in columns.
    - The widgets will be horizontally aligned along the X-axis (horizontal) of the row.

// Required inputs:
  - widgets: List of widgets to be displayed in the row (type List<Widget>).
  - start: Index of the first widget to be displayed (default is 0).
  - end: Index of the last widget to be displayed (default is 0, meaning display from start to the end of the list).

// Note:
  - This method takes a list of widgets and arranges them into a row, with the option to display only a portion of the list by specifying start and end.
  - Each widget in the list will be wrapped in an Expanded element so that they can expand to fill the horizontal space of the row.
  - Each widget will be separated from other widgets by inserting a SizedBox with a width of 2.w to create horizontal spacing between them.
  - The last element (SizedBox) will be removed from the list to avoid creating unwanted spacing at the end of the row.
  - The entire row will be wrapped in an Expanded element so that it can expand to fill the horizontal space of its parent column or row (depending on the layout structure).
*/

Widget qtyWidgetInRow(List<Widget> widgets) {
  if (widgets.isEmpty) return SizedBox();
  widgets = widgets
      .expand(
        (widget) => [
          Expanded(child: widget),
          SizedBox(width: 2.w),
        ],
      )
      .toList();
  widgets.removeLast();
  return Expanded(
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: widgets,
    ),
  );
}
