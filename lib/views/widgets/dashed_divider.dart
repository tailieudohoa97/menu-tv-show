import 'package:flutter/material.dart';

// [112314TIN] create this widget
///
/// Layout: A horizontal line with dashes (similar to a dotted line)
/// to visually separate different sections or elements.
///
/// Fields
///   + dashWidth: Determines the width of each dash in the dashed line.
///   + dashSpace: Specifies the space between consecutive dashes.
///   + color: Sets the color of the dashed line.
///   + indent: Defines the initial position of the dashed line (how far it starts from the left edge).
///   + thickness: Controls the thickness of the dashed line.

class DashedDivider extends StatelessWidget {
  final double? dashWidth;
  final double? dashSpace;
  final double? indent;
  final double? thickness;
  final Color? color;
  const DashedDivider({
    super.key,
    this.dashWidth,
    this.dashSpace,
    this.color,
    this.indent,
    this.thickness,
  });

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: DashedLinePainter(
        dashWidth: dashSpace,
        dashSpace: dashSpace,
        color: color,
        indent: indent,
        thickness: thickness,
      ),
    );
  }
}

class DashedLinePainter extends CustomPainter {
  // Defaulte values
  static const double _defaultDashWidth = 8;
  static const double _defaultDashSpace = 4;
  static const double _defaultIndent = 0;
  static const double _defaultThickness = 1;
  static const Color _defaultColor = Color(0xff000000);

  final double? dashWidth;
  final double? dashSpace;
  final double? indent;
  final double? thickness;
  final Color? color;

  const DashedLinePainter({
    this.dashWidth,
    this.dashSpace,
    this.indent,
    this.thickness,
    this.color,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = color ?? _defaultColor
      ..strokeWidth = thickness ?? _defaultThickness;
    double currentPosition = indent ?? _defaultIndent;

    final double dashWidth = this.dashWidth ?? _defaultDashWidth;
    final double dashSpace = this.dashSpace ?? _defaultDashSpace;

    while (currentPosition < size.width) {
      canvas.drawLine(Offset(currentPosition, 0),
          Offset(currentPosition + dashWidth, 0), paint);
      currentPosition += dashWidth + dashSpace;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
