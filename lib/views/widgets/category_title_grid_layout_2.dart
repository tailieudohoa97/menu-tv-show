// import 'package:flutter/material.dart';
// import 'package:sizer/sizer.dart';
// import 'package:tv_menu/helper/module_helper.dart';
// import 'package:tv_menu/views/widgets/category_title.dart';
// import 'package:tv_menu/models/model_datas/category_data.dart';
// import 'package:tv_menu/views/widgets/image.dart';
// import 'package:tv_menu/views/widgets/product_item.dart';

// /*
// // Used to:
//     Create an interface displaying information about a product category, including the category title, a list of products, and a category thumbnail.

// // Layout will display:
// - Row 1: Title of the product category.
// - Row 2: A horizontal row containing 2 columns:
//   + Column 1: List of products (first 3 products).
//   + Column 2: Category thumbnail.

// // Required inputs:
//   - ratioStyle: Scale and style for interface elements.
//   - categoryData: Information about the product category (type CategoryData).
//   - pathThumbnail: Path to the category thumbnail image (string).
//   - useBackgroundCategoryTitle: Determines whether to use a background for the category title (default is true).
//   - pathImageBgCategoryTitle: Path to the background image for the category title (string).
//   - colorCategoryTitle: Color for the category title (default is white).
//   - colorPrice: Color for product prices (default is white).
//   - colorProductName: Color for product names (default is white).
//   - colorDescription: Color for product descriptions (default is white).
// */

// Widget categoryTitleGridLayout2({
//   required CategoryData categoryData,
//   required String imagePath,
//   bool fixCover = true,
//   bool useBackgroundCategoryTitle = true,
//   String pathImageBgCategoryTitle = '',
//   Color colorCategoryTitle = const Color(0xFFFFFFFF),
//   Color colorPrice = const Color(0xFFFFFFFF),
//   Color colorProductName = const Color(0xFFFFFFFF),
//   Color colorDescription = const Color(0xFFFFFFFF),
// }) {
//   //Trong 1 Danh mục lấy sp1, sp2, sp3
//   List<Widget> productWidgetList = categoryData.listMenu
//       .sublist(0, 3)
//       .map(
//         (item) => productItem(
//           name: item.productName,
//           price: item.price,
//           description: item.productDescription,
//           useMediumStyle: false,
//           colorPrice: colorPrice,
//           colorProductName: colorProductName,
//           colorDescription: colorDescription,
//         ),
//       )
//       .toList();

//   return Expanded(
//     child: Column(
//       children: [
//         // Row 1: Tiêu đề danh mục
//         categoryTitle(
//           catName: categoryData.categoryName,
//           useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//           pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//           colorCategoryTitle: colorCategoryTitle,
//         ),

//         // Row 2: 2 cột
//         Expanded(
//           child: Row(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: [
//               //Cột 1: Danh sách sản phẩm (3 sản phẩm đầu tiên)
//               Expanded(
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   children: productWidgetList,
//                 ),
//               ),

//               //Module Fixed
//               Container(
//                 width: 5.w,
//                 padding: EdgeInsets.symmetric(vertical: 2.w),
//                 child: ModuleHelper.getImage(
//                   '/temp_restaurant_1/decorate/Seperator-Line.png',
//                   fit: BoxFit.contain,
//                 ),
//               ),

//               //Cột 2: 1 Hình đại diện danh mục
//               Expanded(
//                 child: Container(
//                   padding: EdgeInsets.symmetric(vertical: 1.w),
//                   child: image(
//                     imagePath: imagePath,
//                     fixCover: fixCover,
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ],
//     ),
//   );
// }
