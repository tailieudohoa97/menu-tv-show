import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

/// Build brand of shop has underline
///
/// Layout: a large text with bottom border
///
/// Fields:
///   + brand: Brand of shop
///   + textStyle: text style for text
///   + boderSide: border side for bottom border
class UnderlineBrand extends StatelessWidget {
  final String brand;
  final TextStyle? textStyle;
  final BorderSide bottomBorderSide;
  static const BorderSide _defaultBottomBorderSide = BorderSide();

  const UnderlineBrand({
    super.key,
    required this.brand,
    this.textStyle,
    BorderSide? bottomBorderSide = _defaultBottomBorderSide,
  }) : bottomBorderSide = bottomBorderSide ?? _defaultBottomBorderSide;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(vertical: 2.sp),
      decoration: BoxDecoration(
        border: Border(
          bottom: bottomBorderSide,
        ),
      ),
      child: Text(
        brand.toUpperCase(),
        style: textStyle,
      ),
    );
  }
}
