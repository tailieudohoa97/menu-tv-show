import 'package:flutter/material.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/product_row_1.dart';

/// Used to build a row display name, price and desciption of product
///
/// Layout: Has 3 rows
///   _ Row 1: Contains name and price of product
///   - Row 2: Divier if any
///   _ Row 3: Contains desciption of product if any
///
/// Fields:
///   - product: product data
///   - productNameTextStyle: style of product name
///   - productPriceTextStyle: style of product price
///   - productDescriptionTextStyle: style of product description
///   - maxLineOfProductName: Maximun line allowed of row
///   - prefixProductPrice: price prefix, eg: $
///   - subfixProductPrice: price subfix, eg: VND
///   - upperCaseProductName: to upper case or not for product name
///   - fractionDigitsForPrice: To fixed string for price,
///     eg: 1.23456 with fractionDigits = 2 => 1.23
///   - divider: divider between row 1 (name and price of product)
///     and row 3 (product description)
///
/// [112309TIN] add properties: useSubname, upperCaseProductSubName, productSubNameTextStyle, maxLineOfProductSubName
class ProductRow2 extends StatelessWidget {
  final ProductData product;
  final TextStyle productNameTextStyle;
  final TextStyle productPriceTextStyle;
  final TextStyle productDescriptionTextStyle;
  final Widget? divider;
  final int? maxLineOfProductName;
  final String? prefixProductPrice;
  final String? subfixProductPrice;
  final bool upperCaseProductName;
  final int? fractionDigitsForPrice;
  final bool? useSubname;
  final bool? upperCaseProductSubName;
  final TextStyle? productSubNameTextStyle;
  final int? maxLineOfProductSubName;

  const ProductRow2({
    super.key,
    required this.product,
    this.productNameTextStyle = const TextStyle(),
    this.productPriceTextStyle = const TextStyle(),
    this.productDescriptionTextStyle = const TextStyle(),
    this.maxLineOfProductName,
    this.prefixProductPrice,
    this.subfixProductPrice,
    this.upperCaseProductName = false,
    this.fractionDigitsForPrice,
    this.divider,
    this.useSubname,
    this.upperCaseProductSubName,
    this.productSubNameTextStyle,
    this.maxLineOfProductSubName,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ProductRow1(
          product: product,
          productNameTextStyle: productNameTextStyle,
          productPriceTextStyle: productPriceTextStyle,
          maxLineOfProductName: maxLineOfProductName,
          prefixProductPrice: prefixProductPrice,
          subfixProductPrice: subfixProductPrice,
          fractionDigitsForPrice: fractionDigitsForPrice,
          upperCaseProductName: upperCaseProductName,
          useSubname: useSubname,
          upperCaseProductSubName: upperCaseProductSubName,
          productSubNameTextStyle: productSubNameTextStyle,
          maxLineOfProductSubName: maxLineOfProductSubName,
          border: Border.all(color: Colors.transparent),
        ),
        if (divider != null) divider as Widget,
        if (product.productDescription != null &&
            (product.productDescription as String).isNotEmpty)
          Text(
            product.productDescription,
            style: productDescriptionTextStyle,
            maxLines: 2, //[092328HG]
          ),
      ],
    );
  }
}
