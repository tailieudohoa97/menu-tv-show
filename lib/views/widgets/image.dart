import 'package:flutter/material.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';

/*
// Used to:
    Create an image element in the interface.

// Layout will display:
    - An image displayed within a frame that can be rounded.

// Required inputs:
  - ratioStyle: Scale and style for interface elements.
  - imagePath: Path to the image to be displayed (string).
  - fixCover: Determine whether the image should be fixed to fit the frame (cover) or not (default is true).

// Note:
  - If fixCover is set to true, the image will be fixed to fit the frame while maintaining its aspect ratio.
  - If fixCover is set to false, the image will be displayed with its actual aspect ratio, which may change the frame size.
*/
// [092321Tin] Add width, height for image
Widget image({
  required String imagePath,
  bool fixCover = true,
  double? width,
  double? height,
}) {
  return Container(
    // color: Colors.amber,
    //092314LH Fix dynamic sample data to template
    height: height ?? GlobalSetting.ratioStyle.getField('imageHeight'),
    margin: EdgeInsets.only(
        bottom: GlobalSetting.ratioStyle.getField('marginBottomImage')),
    child: ClipRRect(
      borderRadius: BorderRadius.circular(
          GlobalSetting.ratioStyle.getField('boderRadiusStyle')),
      child: ModuleHelper.getImage(
        imagePath,
        fit: fixCover ? BoxFit.cover : BoxFit.contain,
        height: height ?? double.infinity,
        width: width ?? double.infinity,
      ),
    ),
  );
}
