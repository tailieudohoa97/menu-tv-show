import 'package:flutter/material.dart';

/// Used to: Build a image with a border that is another image as a frame
///
/// Layout: A image inside a container has background image as a frame
///
/// Fields
///   - image: image would shown
///   - imageFrame: image would be frame
///   - width: width of image (includes border)
///   - height: height of image (includes border)
///   - borderRadiusOfImage: border radius of image
///   - borderWidth: border width of image
class FramedImage extends StatelessWidget {
  final ImageProvider image;
  final ImageProvider imageFrame;
  final double? width;
  final double? height;
  final double borderRadiusOfImage;
  final double borderWidth;

  const FramedImage({
    super.key,
    required this.image,
    required this.imageFrame,
    this.width,
    this.height,
    this.borderRadiusOfImage = 0,
    this.borderWidth = 1,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      padding: EdgeInsets.all(borderWidth),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: imageFrame,
          fit: BoxFit.cover,
        ),
      ),
      child: Container(
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(borderRadiusOfImage),
        ),
        child: Image(
          image: image,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
