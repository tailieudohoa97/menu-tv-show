import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/image.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';

import 'package:tv_menu/views/widgets/category_title.dart';
import 'package:tv_menu/views/widgets/product_item.dart';
import 'package:tv_menu/views/widgets/qty_widget_in_row.dart';
import 'package:tv_menu/helper/global_setting.dart';

/*
// Used to:
    Create an interface displaying information about a product category, including
    a category thumbnail image, category title, and a list of products within that category.

// Layout will display:
- Row 1: Title of the product category.
- Row 2: A row containing 3 thumbnail images for the product category.
- Row 3: A row containing the first 3 products in the category.

// Required inputs:
  - ratioStyle: Scale and style for interface elements.
  - imageList: List of paths to category thumbnail images.
  - categoryData: Information about the product category (type CategoryData).
  - useBackgroundCategoryTitle: Determines whether to use a background for the category title (default is true).
  - pathImageBgCategoryTitle: Path to the background image for the category title (string).
  - colorCategoryTitle: Color for the category title (default is white).
  - colorPrice: Color for product prices (default is white).
  - colorProductName: Color for product names (default is white).
  - colorDescription: Color for product descriptions (default is white).
*/

Widget categoryTitleGridLayout4({
  //092314LH Fix dynamic sample data to template
  required List<String> imageList,
  required CategoryData categoryData,
  required List<ProductData> productList,
  bool useBackgroundCategoryTitle = false,
  bool useFullWidthCategoryTitle = false,
  String pathImageBgCategoryTitle = '',
  Color colorCategoryTitle = const Color(0xFFFFFFFF),
  Color colorBackgroundCategoryTitle = const Color(0x00FFFFFF),
  Color colorPrice = const Color(0xFFFFFFFF),
  Color colorProductName = const Color(0xFFFFFFFF),
  Color colorDescription = const Color(0xFFFFFFFF),
}) {
  List<Widget> imageWidgetList = imageList
      .map(
        (imagePath) => image(
          imagePath: imagePath,
        ),
      )
      .toList();

  //Trong cùng Danh mục lấy sp1, sp2, sp3
  //092314LH Fix data động tách 3 sản phẩm từ product list
  List<Widget> productWidgetList1 = [];
  if (productList.isNotEmpty) {
    productWidgetList1 =
        (productList.length >= 3 ? productList.sublist(0, 3) : productList)
            .map(
              (item) => productItem(
                name: item.productName,
                price: item.price,
                description: item.productDescription,
                useMediumStyle: true,
                colorPrice: colorPrice,
                colorProductName: colorProductName,
                colorDescription: colorDescription,
              ),
            )
            .toList();
    //092314LH Fix data động remove 3 sản phẩm từ product list sau khi tách danh sách
    productList.removeRange(
        0, productList.length >= 3 ? 3 : productList.length);
  }

  return Expanded(
    child: Column(
      children: [
        // Tiêu đề của danh mục sản phẩm
        categoryTitle(
          catName: categoryData.categoryName,
          useBackgroundCategoryTitle: useBackgroundCategoryTitle,
          pathImageBgCategoryTitle: pathImageBgCategoryTitle,
          colorCategoryTitle: colorCategoryTitle,
          colorBackgroundCategoryTitle: colorBackgroundCategoryTitle,
          useFullWidthCategoryTitle: useFullWidthCategoryTitle,
        ),

        // Layout hàng ngang: 3 hình đại diện cho Danh mục
        qtyWidgetInRow(imageWidgetList),

        // Layout hàng ngang: 3 sản phẩm đầu tiên của Danh mục
        Container(
          margin: EdgeInsets.only(
              bottom: GlobalSetting.ratioStyle.getField('marginBottomImage')),
          child: Row(
            children: [
              qtyWidgetInRow(productWidgetList1),
            ],
          ),
        ),
      ],
    ),
  );
}
