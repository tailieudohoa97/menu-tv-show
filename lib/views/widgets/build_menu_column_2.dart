// import 'package:tv_menu/models/model_datas/category_data.dart';
// import 'package:tv_menu/views/widgets/category_title_single_column.dart';
// import 'package:tv_menu/views/widgets/image.dart';
// import 'package:tv_menu/views/widgets/qty_widget_in_row.dart';
// import 'package:flutter/material.dart';
// import 'package:tv_menu/helper/global_setting.dart';

// /*
// // Used to:
//     Create a column containing product categories and a list of images.

// // Layout will display:
// - Row 1: One category and the corresponding product list.
// - Row 2: A list of 3 images.

// // Required inputs:
//   - ratioStyle: A dynamic parameter to adjust the size ratio.
//   - indexCategory: Index of the product category.
//   - useBackgroundCategoryTitle: Determines whether to use a background for the category title or not.
//   - pathImageBgCategoryTitle: The path to the background image for the category title.
//   - colorCategoryTitle: Color for the category title.
//   - colorBackgroundCategoryTitle: Background color for the category title.
//   - colorPrice: Color for product prices.
//   - colorProductName: Color for product names.
//   - colorDescription: Color for product descriptions.
//   - useThumbnailProduct: Determines whether to use product thumbnails or not.
//   - pathThumbnailProduct: The path to the product thumbnail image.
//   - toUpperCaseNameProductName: Determines whether to convert product names to uppercase or not.
//   - useMediumStyle: Determines whether to use medium interface style or not.
//   - categoryDataList: List of product category data.
//   - widgetList: List of product images.
//   - startIndexProduct: Starting index of the product list.
//   - endIndexProduct: Ending index of the product list.
//   - startIndexWidgetList: Starting index of the widget (image) list.
//   - endIndexWidgetList: Ending index of the widget (image) list.
// */

// Widget buildMenuColumn2({
//   required int indexCategory,
//   bool useBackgroundCategoryTitle = true,
//   bool useFullWidthCategoryTitle = false,
//   String pathImageBgCategoryTitle = '',
//   Color colorCategoryTitle = const Color(0xFFFFFFFF),
//   Color colorBackgroundCategoryTitle = const Color(0x00FFFFFF), //Trong suốt
//   Color colorPrice = const Color(0xFFFFFFFF),
//   Color colorProductName = const Color(0xFFFFFFFF),
//   Color colorDescription = const Color(0xFFFFFFFF),
//   bool useThumnailProduct = false,
//   String pathThumnailProduct = GlobalSetting.noImage,
//   bool toUpperCaseNameProductName = true,
//   bool useMediumStyle = false, // Tham số quyết định style sử dụng
//   required List<CategoryData> categoryDataList,
//   required List<String> widgetList,
//   int startIndexProduct = 0,
//   int endIndexProduct = 3,
//   int startIndexWidgetList = 0,
//   int endIndexWidgetList = 3,
//   bool fixCover = true,
// }) {
//   return Column(
//     mainAxisAlignment: MainAxisAlignment.start,
//     children: [
//       // Dòng 1: Một Danh mục và list sản phẩm tương ứng
//       categoryTitleSingleColumn(
//         categoryData: categoryDataList[indexCategory],
//         productData: categoryDataList[indexCategory].listMenu,
//         // indexCategory: indexCategory,
//         useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//         colorCategoryTitle: colorCategoryTitle,
//         colorBackgroundCategoryTitle: colorBackgroundCategoryTitle,
//         colorProductName: colorProductName,
//         colorPrice: colorPrice,
//         colorDescription: colorDescription,
//         useThumnailProduct: useThumnailProduct,
//         toUpperCaseNameProductName: toUpperCaseNameProductName,
//         // startIndexProduct: startIndexProduct,
//         // endIndexProduct: endIndexProduct,
//       ),

//       SizedBox(
//           height:
//               GlobalSetting.ratioStyle.getField('marginBottomCategoryTitle')),

//       //3 hình ảnh
//       qtyWidgetInRow(
//         widgetList
//             .sublist(startIndexWidgetList, endIndexWidgetList)
//             .map(
//               (item) => image(
//                 imagePath: item,
//                 fixCover: fixCover,
//               ),
//             )
//             .toList(),
//       ),
//     ],
//   );
// }
