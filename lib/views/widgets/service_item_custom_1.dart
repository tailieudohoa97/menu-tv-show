import 'package:flutter/material.dart';
import 'package:tv_menu/helper/global_setting.dart';

/// Used to build a column display name, price and desciption of product
///
/// Layout: Has 2 rows
///   _ Row 1: Contains name in a box and price outside the box
///   - Row 2: Contains desciption of product
///
/// Fields:
///   - name: product name
///   - price: product price
///   - description: product description
///   - colorProductName
///   - colorPrice
///   - colorProductDescription
///   - maxLineOfProductName: Maximun line allowed of row
///   - maxLineOfProductDescription: Maximun line allowed of row
///   - width: width of column
///   - useMediumStyle: use medium style or not
///   - upperCaseProductName: to upper case or not for product name
Widget serviceItemCustom1({
  required String name,
  bool toUpperCaseNameProductName = true,
  required String price,
  bool useMediumStyle = false, // Tham số quyết định style sử dụng
  Color colorPrice = const Color(0xFFFFFFFF),
  Color colorProductName = const Color(0xFFFFFFFF),
  int maxLineProductName = 2,
  int maxLineProductDescription = 2,
  required double width,
  required String description,
  Color colorDescription = const Color(0xFFFFFFFF),
}) {
  return SizedBox(
    width: width,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              flex: 60,
              child: Container(
                padding: const EdgeInsets.all(5),
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.black)),
                child: //service name
                    Text(
                  toUpperCaseNameProductName ? name.toUpperCase() : name,
                  style: useMediumStyle
                      ? GlobalSetting.ratioStyle
                          .getTextStyle('nameProductStyleMedium')
                          .copyWith(height: 1.5, color: colorProductName)
                      : GlobalSetting.ratioStyle
                          .getTextStyle('nameProductStyle')
                          .copyWith(height: 1.5, color: colorProductName),
                  maxLines: maxLineProductName,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            const Expanded(
                flex: 10, child: Divider(thickness: 1, color: Colors.black)),
            //service price
            Expanded(
              flex: 30,
              child: Text(
                price.toUpperCase(),
                style: useMediumStyle
                    ? GlobalSetting.ratioStyle
                        .getTextStyle('priceProductStyleMedium')
                        .copyWith(color: colorPrice)
                    : GlobalSetting.ratioStyle
                        .getTextStyle('priceProductStyle')
                        .copyWith(color: colorPrice),
                maxLines: maxLineProductName,
              ),
            ),
          ],
        ),
        Text(
          description,
          style: useMediumStyle
              ? GlobalSetting.ratioStyle
                  .getTextStyle('discriptionProductStyleMedium')
                  .copyWith(height: 1.5, color: colorDescription)
              : GlobalSetting.ratioStyle
                  .getTextStyle('discriptionProductStyle')
                  .copyWith(height: 1.5, color: colorDescription),
          maxLines: maxLineProductDescription,
        ),
      ],
    ),
  );
}
