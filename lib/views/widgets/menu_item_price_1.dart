import 'package:flutter/material.dart';

/// Used to build item price with decimal part of price is supperscript type
/// Eg: 10.10 -> 10.¹⁰
///
/// Item price divided into 2 part(int and decimal).
/// Decimal part is superscript type
///
/// Fields
///   + price: price of menu item
///   + prefix: prefix of price, eg: $, ¥, ...
///   + textStyle: Text Style for price and prefix
class MenuItemPrice1 extends StatelessWidget {
  late final int _intPart;
  late final int _decimalPart;
  late final TextStyle textStyleForDecimalPart;
  late final TextStyle _textStyle;
  late final String _prefix;

  MenuItemPrice1({
    super.key,
    num price = 0,
    TextStyle? textStyle,
    String? prefix,
  }) {
    _textStyle = textStyle ?? const TextStyle();
    _prefix = prefix ?? '\$';
    _intPart = price.floor();
    _decimalPart = (price - _intPart).round();

    textStyleForDecimalPart = _textStyle.copyWith(
      fontSize: (_textStyle.fontSize ?? 14) * 0.75,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Text(
          '$_prefix$_intPart',
          style: _textStyle,
        ),
        Text(
          '.${_decimalPart.toString().padRight(2, '0')}',
          style: textStyleForDecimalPart,
        ),
      ],
    );
  }
}
