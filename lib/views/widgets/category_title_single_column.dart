import 'package:tv_menu/models/model_datas/category_data.dart';
import 'package:flutter/material.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/category_title.dart';
import 'package:tv_menu/views/widgets/product_item.dart';
import 'package:tv_menu/helper/global_setting.dart';

/*
// Used to:
    Create an interface displaying information about a product category, including the category title, a list of products, and the option to use a background for the category title.

// Layout will display:
- Row 1: Title of the product category with an option to use a background.
- Row 2: List of products belonging to the category.

// Required inputs:
  - ratioStyle: Scale and style for interface elements.
  - indexCategory: Index of the category in the list of product categories.
  - useBackgroundCategoryTitle: Determines whether to use a background for the category title (default is true).
  - pathImageBgCategoryTitle: Path to the background image for the category title (string).
  - colorCategoryTitle: Color for the category title (default is white).
  - colorBackgroundCategoryTitle: Background color for the category title (default is transparent).
  - colorPrice: Color for product prices (default is white).
  - colorProductName: Color for product names (default is white).
  - colorDescription: Color for product descriptions (default is white).
  - useThumnailProduct: Determines whether to use a thumbnail for the products (default is false).
  - pathThumnailProduct: Path to the product thumbnails (string).
  - toUpperCaseNameProductName: Determines whether to capitalize the product names (default is true).
  - useMediumStyle: Parameter deciding the style for the products (default is false).
  - categoryDataList: List of product category information (type List<CategoryData>).
  - startIndexProduct: Index of the first product to display in the list of category products (default is 0).
  - endIndexProduct: Index of the last product to display in the list of category products (default is 3).
*/

Widget categoryTitleSingleColumn({
  //092314LH Fix dynamic sample data to template
  // required int indexCategory,
  required CategoryData categoryData,
  required List<ProductData> productData,
  bool useBackgroundCategoryTitle = true,
  // String pathImageBgCategoryTitle = GlobalSetting.noImage,
  String pathImageBgCategoryTitle = "",
  Color colorCategoryTitle = const Color(0xFFFFFFFF),
  Color colorBackgroundCategoryTitle = const Color(0x00FFFFFF),
  Color colorPrice = const Color(0xFFFFFFFF),
  Color colorProductName = const Color(0xFFFFFFFF),
  Color colorDescription = const Color(0xFFFFFFFF),
  bool useThumnailProduct = false,
  String pathThumnailProduct = GlobalSetting.noImage,
  bool toUpperCaseNameProductName = true,
  bool useMediumStyle = false, // Tham số quyết định style sử dụng
  // [092325Tin] add useFullWidthCategoryTitle property
  bool useFullWidthCategoryTitle = false,
  // [092328Tuan] add runspacing,mainAxisAlignment,spaceBetweenCategoryProductList
  double runSpacing = 0,
  MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
  double spaceBetweenCategoryProductList = 0,
}) {
  return Center(
    child: Column(
      mainAxisAlignment: mainAxisAlignment,
      children: [
        // Tiêu đề của danh mục sản phẩm với tùy chọn sử dụng nền.
        //092314LH Fix dynamic sample data to template
        categoryTitle(
          catName: categoryData.categoryName,
          useBackgroundCategoryTitle: useBackgroundCategoryTitle,
          pathImageBgCategoryTitle: pathImageBgCategoryTitle,
          colorCategoryTitle: colorCategoryTitle,
          colorBackgroundCategoryTitle: colorBackgroundCategoryTitle,
          useFullWidthCategoryTitle: useFullWidthCategoryTitle,
        ),
        SizedBox(
          height: spaceBetweenCategoryProductList,
        ),
        //List sản phẩm tương ứng với Danh mục
        //092314LH Fix dynamic sample data to template
        ...productData.map((item) {
          return Column(
            children: [
              productItem(
                name: item.productName,
                toUpperCaseNameProductName: toUpperCaseNameProductName,
                price: item.price,
                description: item.productDescription,
                useMediumStyle: useMediumStyle,
                colorPrice: colorPrice,
                colorProductName: colorProductName,
                colorDescription: colorDescription,
                useThumnailProduct: useThumnailProduct,
                pathThumnailProduct: pathThumnailProduct,
              ),
              SizedBox(
                height: runSpacing,
              )
            ],
          );
        }).toList(),
      ],
    ),
  );
}
