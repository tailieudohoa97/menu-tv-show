import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/product_row_2.dart';

/// Used to build a row with name, image, price and description of product
///
/// Layout: Has 2 columns
///   - Row 1: Image of product if any
///   - Row 2: Has 3 sub rows
///     + Sub row 1: Product name aligned on left side
///       and product price aligned on right side
///     + Sub row 2: Divider if any
///     + Sub row 3: Product decsription if any
///
/// Fields:
///   - product: Product data
///   - imageWidth: Width of product image
///   - imageHeight: Height of product image
///   - imageBorderRadius: Border radius of product image
///   - elementGap: Spacing between image product and other info(name, price, description)
///   - productNameTextStyle: Text style of product name
///   - productPriceTextStyle: Text style of product price
///   - productDescriptionTextStyle: Text style of product description
///   - fractionDigitsForPrice: To fixed string for price,
///     eg: 1.23456 with fractionDigits = 2 => 1.23
///   - maxLineOfProductName: Maximun line allowed of row
///   - prefixProductPrice: price prefix, eg: $
///   - subfixProductPrice: price subfix, eg: VND
///   - upperCaseProductName: Uppercase for product name or not
///   - horizontalDivider: Divider between name-price row and description row
///
/// /// [112309TIN] add properties: useSubname, upperCaseProductSubName,
///  productSubNameTextStyle, maxLineOfProductSubName
/// imageAtEnd
class ProductRow3 extends StatelessWidget {
// default values
  static const double _defaultElementGap = 16;

  final ProductData product;
  final double? imageWidth;
  final double? imageHeight;
  final BorderRadiusGeometry? imageBorderRadius;
  final double elementGap;
  final TextStyle productNameTextStyle;
  final TextStyle productPriceTextStyle;
  final TextStyle productDescriptionTextStyle;
  final int? fractionDigitsForPrice;
  final int? maxLineOfProductName;
  final String? prefixProductPrice;
  final String? subfixProductPrice;
  final bool upperCaseProductName;
  final Widget? horizontalDivider;
  final bool? useSubname;
  final bool? upperCaseProductSubName;
  final TextStyle? productSubNameTextStyle;
  final int? maxLineOfProductSubName;
  final bool? imageAtEnd;

  const ProductRow3({
    super.key,
    required this.product,
    this.imageWidth,
    this.imageHeight,
    this.imageBorderRadius,
    double? elementGap,
    this.productNameTextStyle = const TextStyle(),
    this.productPriceTextStyle = const TextStyle(),
    this.productDescriptionTextStyle = const TextStyle(),
    this.fractionDigitsForPrice,
    this.maxLineOfProductName,
    this.prefixProductPrice,
    this.subfixProductPrice,
    this.upperCaseProductName = false,
    this.horizontalDivider,
    this.useSubname,
    this.upperCaseProductSubName,
    this.productSubNameTextStyle,
    this.maxLineOfProductSubName,
    this.imageAtEnd,
  }) : elementGap = elementGap ?? _defaultElementGap;

  @override
  Widget build(BuildContext context) {
    final List<Widget> rowChildren = [
      Expanded(
        child: ProductRow2(
          productNameTextStyle: productNameTextStyle,
          productPriceTextStyle: productPriceTextStyle,
          productDescriptionTextStyle: productDescriptionTextStyle,
          fractionDigitsForPrice: fractionDigitsForPrice,
          maxLineOfProductName: maxLineOfProductName,
          prefixProductPrice: prefixProductPrice,
          subfixProductPrice: subfixProductPrice,
          upperCaseProductName: upperCaseProductName,
          divider: horizontalDivider,
          useSubname: useSubname,
          upperCaseProductSubName: upperCaseProductSubName,
          productSubNameTextStyle: productSubNameTextStyle,
          maxLineOfProductSubName: maxLineOfProductSubName,
          product: product,
        ),
      )
    ];

    if (product.image != null && (product.image as String).isNotEmpty) {
      final imageWidget = Container(
        width: imageWidth,
        height: imageHeight,
        clipBehavior: Clip.hardEdge,
        decoration: BoxDecoration(borderRadius: imageBorderRadius),
        child: Image(
          image: ModuleHelper.getImageProvider(product.image as String),
          fit: BoxFit.cover,
        ),
      );
      final Widget spacingWidget = SizedBox(width: elementGap);
      if (imageAtEnd == true) {
        rowChildren.addAll([spacingWidget, imageWidget]);
      } else {
        rowChildren.insertAll(0, [imageWidget, spacingWidget]);
      }
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: rowChildren,
      // children: [
      // if (product.image != null && (product.image as String).isNotEmpty) ...[
      //   Container(
      //     width: imageWidth,
      //     height: imageHeight,
      //     clipBehavior: Clip.hardEdge,
      //     decoration: BoxDecoration(borderRadius: imageBorderRadius),
      //     child: Image(
      //       image: ModuleHelper.getImageProvider(product.image as String),
      //       fit: BoxFit.cover,
      //     ),
      //   ),
      //   SizedBox(width: elementGap),
      // ],
      // Expanded(
      //   child: ProductRow2(
      //     productNameTextStyle: productNameTextStyle,
      //     productPriceTextStyle: productPriceTextStyle,
      //     productDescriptionTextStyle: productDescriptionTextStyle,
      //     fractionDigitsForPrice: fractionDigitsForPrice,
      //     maxLineOfProductName: maxLineOfProductName,
      //     prefixProductPrice: prefixProductPrice,
      //     subfixProductPrice: subfixProductPrice,
      //     upperCaseProductName: upperCaseProductName,
      //     divider: horizontalDivider,
      //     useSubname: useSubname,
      //     upperCaseProductSubName: upperCaseProductSubName,
      //     productSubNameTextStyle: productSubNameTextStyle,
      //     maxLineOfProductSubName: maxLineOfProductSubName,
      //     product: product,
      //   ),
      // )

      // ],
    );
  }
}
