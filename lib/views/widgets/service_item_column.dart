import 'package:flutter/material.dart';
import 'package:tv_menu/helper/global_setting.dart';

/// Used to build a column display name, price and desciption of product
///
/// Layout: Has 3 rows
///   _ Row 1: Contains name
///   - Row 2: Contains desciption of product
///   _ Row 3: Contains price of product
///
/// Fields:
///   - name: product name
///   - price: product price
///   - description: product description
///   - colorProductName
///   - colorPrice
///   - colorProductDescription
///   - maxLineOfProductName: Maximun line allowed of row
///   - maxLineOfProductDescription: Maximun line allowed of row
///   - width: width of column
///   - useMediumStyle: use medium style or not
///   - upperCaseProductName: to upper case or not for product name

Widget serviceItemColumn({
  FontWeight fontWeight = FontWeight.normal,
  required String name,
  bool toUpperCaseNameProductName = true,
  required String price,
  bool useMediumStyle = false, // Tham số quyết định style sử dụng
  Color colorPrice = const Color(0xFF000000),
  Color colorProductName = const Color(0xFF000000),
  int maxLineProductName = 2,
  int maxLineProductDescription = 2,
  required double width,
  required String description,
  Color colorDescription = const Color(0xFF000000),
}) {
  return SizedBox(
    width: width,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          toUpperCaseNameProductName ? name.toUpperCase() : name,
          style: useMediumStyle
              ? GlobalSetting.ratioStyle
                  .getTextStyle('nameProductStyleMedium')
                  .copyWith(height: 1.5, color: colorProductName)
              : GlobalSetting.ratioStyle
                  .getTextStyle('nameProductStyle')
                  .copyWith(height: 1.5, color: colorProductName),
          maxLines: maxLineProductName,
          textAlign: TextAlign.center,
        ),
        Text(
          description,
          style: useMediumStyle
              ? GlobalSetting.ratioStyle
                  .getTextStyle('discriptionProductStyleMedium')
                  .copyWith(height: 1.5, color: colorDescription)
              : GlobalSetting.ratioStyle
                  .getTextStyle('discriptionProductStyle')
                  .copyWith(height: 1.5, color: colorDescription),
          maxLines: maxLineProductDescription,
          textAlign: TextAlign.center,
        ),
        Text(
          price.toUpperCase(),
          style: useMediumStyle
              ? GlobalSetting.ratioStyle
                  .getTextStyle('priceProductStyleMedium')
                  .copyWith(color: colorPrice)
              : GlobalSetting.ratioStyle
                  .getTextStyle('priceProductStyle')
                  .copyWith(color: colorPrice),
          maxLines: maxLineProductName,
        ),
      ],
    ),
  );
}
