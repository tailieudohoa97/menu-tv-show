import 'package:flutter/material.dart';

/// Used to build a menu contains brand, slogan and list menu item
///
/// Layout: Has 3 sections, arranged horizontally
///   - Section 1: Brand of shop
///   - Section 2: Slogan of shop
///   - Section 3: List of menu item list, arranged horizontally
///
/// Fields:
///   - brand: brand of shop
///   - slogan: slogan of shop
///   - menuItems: Items of menu
///   - brandTextStyle: Text style for brand
///   - sloganTextStyle: Text style for slogan
// ignore: must_be_immutable
class MenuColumn1 extends StatelessWidget {
  final String brand;
  final String slogan;
  final List<Widget> menuItems;
  final TextStyle? brandTextStyle;
  final TextStyle? sloganTextStyle;

  const MenuColumn1({
    super.key,
    this.brand = '',
    this.slogan = '',
    required this.menuItems,
    this.brandTextStyle,
    this.sloganTextStyle,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        if (brand.isNotEmpty) Text(brand, style: brandTextStyle),
        if (slogan.isNotEmpty) Text(slogan, style: sloganTextStyle),
        if (brand.isNotEmpty || slogan.isNotEmpty) const SizedBox(height: 32),
        SizedBox(
          child: Column(
            children: menuItems,
          ),
        ),
      ],
    );
  }
}
