// import 'package:tv_menu/models/model_datas/category_data.dart';
// import 'package:tv_menu/views/widgets/image.dart';
// import 'package:flutter/material.dart';
// import 'package:sizer/sizer.dart';

// import 'package:tv_menu/views/widgets/category_title_single_column.dart';
// import 'package:tv_menu/helper/global_setting.dart';

// /*
// // Used to:
//     Create an interface displaying a product category and its category image in a vertical row.

// // Layout will display:
//     - A row consisting of two columns:
//         + Column 1: Displays the category title and the corresponding list of products.
//         + Column 2: Displays the category's cover image.

// // Required inputs:
//   - ratioStyle: A dynamic parameter to determine interface scaling and size based on the device.
//   - categoryDataList: List of category and product data (type List<CategoryData>).
//   - indexCategory: Index of the category to be displayed from the list.
//   - imagePath: Path to the category's cover image.
//   - fixCover: Choose how to display the image (default is true using BoxFit.cover).
//   - useBackgroundCategoryTitle: Choose whether to use a background for the category title (default is true).
//   - useFullWidthCategoryTitle: Choose whether to use full width for the category title (default is false).
//   - pathImageBgCategoryTitle: Path to the background image for the category title (default is a specific image path).
//   - colorCategoryTitle: Color for the category title (default is white).
//   - colorBackgroundCategoryTitle: Color for the category title background (default is transparent).
//   - colorPrice: Color for product prices (default is white).
//   - colorProductName: Color for product names (default is white).
//   - colorDescription: Color for product descriptions (default is white).
//   - useMediumStyle: Choose to use the medium interface style (default is false).
//   - useThumnailProduct: Choose whether to use product thumbnail images (default is false).
//   - pathThumnailProduct: Path to product thumbnail images (default is a specific image path or a default image).
//   - toUpperCaseNameProductName: Choose whether to use uppercase for product names (default is true).
//   - startIndexProduct: Index of the first product in the list to be displayed (default is 0).
//   - endIndexProduct: Index of the last product in the list to be displayed (default is 3).
// */

// Widget categoryWithImageInRow({
//   required List<CategoryData> categoryDataList,
//   required int indexCategory,
//   required String imagePath,
//   bool fixCover = true,
//   bool useBackgroundCategoryTitle = true,
//   bool useFullWidthCategoryTitle = false,
//   String pathImageBgCategoryTitle = '',
//   Color colorCategoryTitle = const Color(0xFFFFFFFF),
//   Color colorBackgroundCategoryTitle = const Color(0x00FFFFFF),
//   Color colorPrice = const Color(0xFFFFFFFF),
//   Color colorProductName = const Color(0xFFFFFFFF),
//   Color colorDescription = const Color(0xFFFFFFFF),
//   bool useMediumStyle = false, // Tham số quyết định style sử dụng

//   bool useThumnailProduct = false,
//   String pathThumnailProduct = GlobalSetting.noImage,
//   bool toUpperCaseNameProductName = true,
//   int startIndexProduct = 0,
//   int endIndexProduct = 3,
// }) {
//   return Expanded(
//     child: Row(
//       crossAxisAlignment: CrossAxisAlignment.stretch,
//       children: [
//         // Cột 1: Hiển thị tiêu đề danh mục và danh sách sản phẩm tương ứng.
//         Expanded(
//           child: categoryTitleSingleColumn(
//             categoryData: categoryDataList[indexCategory],
//             productData: categoryDataList[indexCategory].listMenu,
//             // indexCategory: indexCategory,
//             useBackgroundCategoryTitle: useBackgroundCategoryTitle,
//             pathImageBgCategoryTitle: pathImageBgCategoryTitle,
//             colorCategoryTitle: colorCategoryTitle,
//             colorBackgroundCategoryTitle: colorBackgroundCategoryTitle,
//             colorProductName: colorProductName,
//             colorPrice: colorPrice,
//             colorDescription: colorDescription,
//             useMediumStyle: useMediumStyle,
//             useThumnailProduct: useThumnailProduct,
//             pathThumnailProduct: pathThumnailProduct,
//             toUpperCaseNameProductName: toUpperCaseNameProductName,
//             // startIndexProduct: startIndexProduct,
//             // endIndexProduct: endIndexProduct,
//           ),
//         ),

//         // Cột 2: Hình đại diện danh mục
//         Expanded(
//           child: Container(
//             padding: EdgeInsets.symmetric(vertical: 1.w),
//             child: image(
//               imagePath: imagePath,
//               fixCover: fixCover,
//             ),
//           ),
//         ),
//       ],
//     ),
//   );
// }
