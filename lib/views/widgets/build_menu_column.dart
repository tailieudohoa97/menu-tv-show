import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/category_title.dart';
import 'package:tv_menu/views/widgets/image.dart';
import 'package:tv_menu/views/widgets/product_item.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:tv_menu/helper/global_setting.dart';

/*
// Used to:
    Create a widget column displaying information about a product category along with a list of products belonging to that category.

// Layout will display:
- Row 1: Title of the product category.
- Row 2: Thumbnail image of the product category.
- Row 3: List of corresponding products.

// Required inputs:
  - ratioStyle: Scale and style for interface elements.
  - categoryName: Name of the product category (string).
  - imagePath: Path to the thumbnail image of the product category (string).
  - colorPrice: Color for product prices (default is white).
  - products: List of products belonging to the category (type List<ProductData>).
*/

Widget buildMenuColumn({
  required String categoryName,
  required String imagePath,
  bool fixCover = true,
  bool toUpperCaseNameProductName =
      true, // Thêm biến để bật/tắt việc sử dụng toUpperCase cho tên sản phẩm
  bool useMediumStyle = false, // Tham số quyết định style sử dụng
  Color colorPrice = const Color(0xFFFFFFFF),
  Color colorProductName = const Color(0xFFFFFFFF),
  Color colorDescription = const Color(0xFFFFFFFF),
  bool useThumnailProduct = false,
  String pathThumnailProduct = GlobalSetting.noImage,
  bool useBackgroundCategoryTitle = true,
  bool useFullWidthCategoryTitle = false,
  String pathImageBgCategoryTitle = "",
  Color colorCategoryTitle = const Color(0xFFFFFFFF),
  Color colorBackgroundCategoryTitle = const Color(0x00FFFFFF),
  required List<ProductData> product,
  // required List<CategoryData> categoryDataList,
  int startIndexProduct = 0,
  int endIndexProduct = 3,
  int maxLineProductName = 1,
  int maxLineProductDescription = 2,
}) {
  return Container(
    padding: EdgeInsets.all(2.w),
    child: Column(
      children: [
        // Tiêu đề của danh mục sản phẩm
        categoryTitle(
          catName: categoryName.toUpperCase(),
          useBackgroundCategoryTitle: useBackgroundCategoryTitle,
          pathImageBgCategoryTitle: pathImageBgCategoryTitle,
          colorCategoryTitle: colorCategoryTitle,
          colorBackgroundCategoryTitle: colorBackgroundCategoryTitle,
          useFullWidthCategoryTitle: useFullWidthCategoryTitle,
        ),

        // Hình đại diện Danh mục
        Container(
          margin: EdgeInsets.only(
              bottom: GlobalSetting.ratioStyle.getField('marginBottomImage')),
          padding: EdgeInsets.symmetric(vertical: 1.5.w),
          height: GlobalSetting.ratioStyle.getField('imageHeight'),
          child: Center(
            child: image(
              imagePath: imagePath,
              fixCover: fixCover,
            ),
          ),
        ),

        // Danh sách các sản phẩm tương ứng
        Expanded(
          child: ListView(
            padding: EdgeInsets.zero,
            reverse:
                true, // Đảo ngược thứ tự các phần tử. Do sử dụng ".reversed" ở toList() bên dưới
            shrinkWrap: true, // Giảm kích thước theo chiều dọc
            children:

                //List sản phẩm tương ứng với Danh mục
                product
                    .sublist(startIndexProduct, endIndexProduct)
                    .map(
                      (item) => productItem(
                        name: item.productName,
                        toUpperCaseNameProductName: toUpperCaseNameProductName,
                        price: item.price,
                        description: item.productDescription,
                        useMediumStyle: false,
                        colorPrice: colorPrice,
                        colorProductName: colorProductName,
                        colorDescription: colorDescription,
                        useThumnailProduct: useThumnailProduct,
                        pathThumnailProduct: pathThumnailProduct,
                        maxLineProductName: maxLineProductName,
                        maxLineProductDescription: maxLineProductDescription,
                      ),
                    )
                    .toList()
                    .reversed // Đảo ngược danh sách các phần tử. Mục đích là để danh sách luôn nằm dưới cùng của cột
                    .toList(),
          ),
        ),
      ],
    ),
  );
}
