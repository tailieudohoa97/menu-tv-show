import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

/*
//usage:
  create a container showing the informations about the salon in a column

//layout will dislay:
  1st row: nail salon's logo
  2nd row: nail salon's name
  3rd row: nail salon's slogan
  4th row: nail salon's description


//input: all parameters are required
  height: height of the container
  width: width of the container
  templatStyle: should use extend class of CaculatRatio
  urlLogoImg: url of the nail salon's logo
  brandName: nail salon's name
  brandSlogan: nail salon's slogan
  brandDescription: nail nail's description
*/

Widget buildBrand({
  required double height,
  required double width,
  required dynamic templateStyle,
  required String urlLogoImg,
  required String brandName,
  required String brandSlogan,
  required String brandDescription,
}) {
  return Container(
    color: Colors.black.withOpacity(0.8),
    width: width,
    height: height,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(180),
          child: Image(
            image: AssetImage(urlLogoImg),
            width: 20.h,
            height: 20.h,
            fit: BoxFit.contain,
          ),
        ),
        Text(brandName.toUpperCase(), style: templateStyle.brandNameStyle),
        Text(brandSlogan.toUpperCase(), style: templateStyle.sloganStyle),
        SizedBox(height: height * 0.05),
        SizedBox(
          width: width * 0.8,
          child: Text(brandDescription,
              textAlign: TextAlign.center,
              style: templateStyle.brandIntroductionStyle),
        ),
      ],
    ),
  );
}
