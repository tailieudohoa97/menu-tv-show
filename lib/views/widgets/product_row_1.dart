import 'package:flutter/material.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';

/// Used to build a row with name and price of product
///
/// Layout: a row has product name and price, that aligned to spaceBetween,
///
/// Fields:
///   + product: product data
///   + border: border style of row
///   + productNameTextStyle: style of product name,
///   + productPriceTextStyle: style of product price,
///   + maxLineOfproductName: Maximun line allowed of row
///   + padding: padding of row
///   + prefixProductPrice: price prefix, eg: $
///   + subfixProductPrice: price subfix, eg: VND
///   + upperCaseProductName: to upper case or not for product name
///   + fractionDigitsForPrice: To fixed string for price,
///     eg: 1.23456 with fractionDigits = 2 => 1.23
///
/// [112320TIN] add properties: productNameLAlignment, namePriceGap
/// [112309TIN] add properties: useSubname, upperCaseProductSubName, productSubNameTextStyle, maxLineOfProductSubName
class ProductRow1 extends StatelessWidget {
  // default values
  static const Border _defaultBorder = Border(bottom: BorderSide());
  static const TextStyle _defaultProductNameTextStyle = TextStyle();
  static const TextStyle _defaultProductPriceTextStyle = TextStyle();
  static const int _defaultMaxLineOfProductName = 3;
  static const String _defaultPrefixProductPrice = '\$';
  static const String _defaultSubfixProductPrice = '';

  final ProductData product;
  final TextStyle productNameTextStyle;
  final int maxLineOfProductName;
  final TextStyle productPriceTextStyle;
  final Border border;
  final EdgeInsetsGeometry? padding;
  final String prefixProductPrice;
  final String subfixProductPrice;
  final bool upperCaseProductName;
  final int? fractionDigitsForPrice;
  final bool? useSubname;
  final bool? upperCaseProductSubName;
  final TextStyle? productSubNameTextStyle;
  final int? maxLineOfProductSubName;
  final TextAlign? productNameLAlignment;
  final double? namePriceGap;

  const ProductRow1({
    super.key,
    required this.product,
    Border? border,
    this.productNameTextStyle = _defaultProductNameTextStyle,
    this.productPriceTextStyle = _defaultProductPriceTextStyle,
    int? maxLineOfProductName,
    this.padding,
    String? prefixProductPrice,
    String? subfixProductPrice,
    this.upperCaseProductName = false,
    this.fractionDigitsForPrice,
    this.useSubname,
    this.upperCaseProductSubName,
    this.productSubNameTextStyle,
    this.maxLineOfProductSubName,
    this.productNameLAlignment,
    this.namePriceGap,
  })  : border = border ?? _defaultBorder,
        maxLineOfProductName =
            maxLineOfProductName ?? _defaultMaxLineOfProductName,
        prefixProductPrice = prefixProductPrice ?? _defaultPrefixProductPrice,
        subfixProductPrice = subfixProductPrice ?? _defaultSubfixProductPrice;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      decoration: BoxDecoration(border: border),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  upperCaseProductName
                      ? product.productName.toUpperCase()
                      : product.productName,
                  style: productNameTextStyle,
                  softWrap: false,
                  overflow: TextOverflow.ellipsis,
                  maxLines: maxLineOfProductName,
                  textAlign: productNameLAlignment,
                ),
              ),
              if (namePriceGap != null)
                SizedBox(
                  width: namePriceGap,
                ),
              Text(
                '$prefixProductPrice${fractionDigitsForPrice != null ? num.parse(product.price).toStringAsFixed(fractionDigitsForPrice as int) : product.price}$subfixProductPrice',
                style: productPriceTextStyle,
              )
            ],
          ),
          if (useSubname == true &&
              product.subName != null &&
              (product.subName ?? '').isNotEmpty)
            Text(
              (upperCaseProductSubName == true)
                  ? (product.subName ?? '').toUpperCase()
                  : (product.subName ?? ''),
              style: productSubNameTextStyle,
              softWrap: false,
              overflow: TextOverflow.ellipsis,
              maxLines: maxLineOfProductSubName,
            ),
        ],
      ),
    );
  }
}
