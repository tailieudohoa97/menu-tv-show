import 'package:flutter/material.dart';
import 'package:tv_menu/models/SizingValue.dart';

class EdgeInsetsData {
  double all = 0;
  double? vertical;
  double? horizontal;
  double? top;
  double? right;
  double? bottom;
  double? left;

  EdgeInsetsData.fromJson(Map<String, dynamic> json)
      : all = json.containsKey('all') ? SizingValue.fromString(json['all']) : 0,
        vertical = json.containsKey('vertical')
            ? SizingValue.fromString(json['vertical'])
            : null,
        horizontal = json.containsKey('horizontal')
            ? SizingValue.fromString(json['horizontal'])
            : null,
        top = json.containsKey('top')
            ? SizingValue.fromString(json['top'])
            : null,
        right = json.containsKey('right')
            ? SizingValue.fromString(json['right'])
            : null,
        bottom = json.containsKey('bottom')
            ? SizingValue.fromString(json['bottom'])
            : null,
        left = json.containsKey('left')
            ? SizingValue.fromString(json['left'])
            : null;

  EdgeInsets toEdgeInsets() {
    return EdgeInsets.fromLTRB(
        left ?? horizontal ?? all,
        top ?? vertical ?? all,
        right ?? horizontal ?? all,
        bottom ?? vertical ?? all);
  }
}
