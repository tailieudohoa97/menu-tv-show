import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';

Widget expandedWidget(Map<String, dynamic> widgetSetting) {
  return Expanded(
    flex: widgetSetting.containsKey('flex')
        ? int.parse(widgetSetting['flex'])
        : 1,
    child: widgetSetting.containsKey('child')
        ? ModuleHelper.renderWidget(widgetSetting['child'])
        : Text(''),
  );
}
