import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/product_row_3.dart';

Widget productRow3Widget(Map<String, dynamic> widgetSetting) {
  if (!widgetSetting.containsKey('product')) {
    throw ErrorDescription("Require product field for ProductRow3 widget");
  }

  final ProductData product = ProductData.fromMap(
    ModuleHelper.getDataFromSetting(widgetSetting['product']),
  );

  final int? fractionDigitsForPrice =
      widgetSetting.containsKey('fractionDigitsForPrice')
          ? int.parse(widgetSetting['fractionDigitsForPrice'])
          : null;

  final int? maxLineOfProductName =
      widgetSetting.containsKey('maxLineOfProductName')
          ? int.parse(widgetSetting['maxLineOfProductName'])
          : null;

  final String? prefixProductPrice =
      widgetSetting.containsKey('prefixProductPrice')
          ? widgetSetting['prefixProductPrice'].toString()
          : null;

  final String? subfixProductPrice =
      widgetSetting.containsKey('subfixProductPrice')
          ? widgetSetting['subfixProductPrice'].toString()
          : null;

  final bool upperCaseProductName =
      widgetSetting['upperCaseProductName'] == 'true';

  double? productNameFontSizeFromRatioSetting =
      widgetSetting.containsKey('productNameFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['productNameFontSizeFromFieldKey'])
          : null;

  TextStyle productNameTextStyle =
      widgetSetting.containsKey('productNameTextStyle')
          ? ModuleHelper.getTextStyle(widgetSetting['productNameTextStyle'])
          : TextStyle();

  if (productNameTextStyle.fontSize == null) {
    productNameTextStyle = productNameTextStyle.copyWith(
      fontSize: productNameFontSizeFromRatioSetting,
    );
  }

  double? productPriceFontSizeFromRatioSetting =
      widgetSetting.containsKey('productPriceFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['productPriceFontSizeFromFieldKey'])
          : null;

  TextStyle productPriceTextStyle =
      widgetSetting.containsKey('productPriceTextStyle')
          ? ModuleHelper.getTextStyle(widgetSetting['productPriceTextStyle'])
          : TextStyle();

  if (productPriceTextStyle.fontSize == null) {
    productPriceTextStyle = productPriceTextStyle.copyWith(
      fontSize: productPriceFontSizeFromRatioSetting,
    );
  }

  double? productDesFontSizeFromRatioSetting =
      widgetSetting.containsKey('productDesFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['productDesFontSizeFromFieldKey'])
          : null;

  TextStyle productDescriptionTextStyle = widgetSetting
          .containsKey('productDescriptionTextStyle')
      ? ModuleHelper.getTextStyle(widgetSetting['productDescriptionTextStyle'])
      : TextStyle();

  if (productDescriptionTextStyle.fontSize == null) {
    productDescriptionTextStyle = productDescriptionTextStyle.copyWith(
      fontSize: productDesFontSizeFromRatioSetting,
    );
  }

  final double? elementGap = widgetSetting.containsKey('elementGap')
      ? SizingValue.fromString(widgetSetting['elementGap'])
      : null;

  final double? imageBorderRadius =
      widgetSetting.containsKey('imageBorderRadius')
          ? SizingValue.fromString(widgetSetting['imageBorderRadius'])
          : null;

  final Widget? horizontalDivider =
      widgetSetting.containsKey('horizontalDivider')
          ? ModuleHelper.renderWidget(widgetSetting['horizontalDivider'])
          : null;

  final double? imageHeight = widgetSetting.containsKey('imageHeight')
      ? SizingValue.fromString(widgetSetting['imageHeight'])
      : null;

  final double? imageWidth = widgetSetting.containsKey('imageWidth')
      ? SizingValue.fromString(widgetSetting['imageWidth'])
      : null;

  final double? imageHeightFromRatioSetting =
      widgetSetting.containsKey('imageHeightFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['imageHeightFromFieldKey'])
          : null;

  final double? imageWidthFromRatioSetting =
      widgetSetting.containsKey('imageWidthFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['imageWidthFromFieldKey'])
          : null;

  final bool useSubname = widgetSetting.containsKey('useSubname') &&
      widgetSetting['useSubname'] == "true";

  final bool upperCaseProductSubName =
      widgetSetting.containsKey('upperCaseProductSubName') &&
          widgetSetting['upperCaseProductSubName'] == "true";

  final int? maxLineOfProductSubName =
      widgetSetting.containsKey('maxLineOfProductSubName')
          ? int.parse(widgetSetting['maxLineOfProductSubName'])
          : null;

  TextStyle productSubNameTextStyle =
      widgetSetting.containsKey('productSubNameTextStyle')
          ? ModuleHelper.getTextStyle(widgetSetting['productSubNameTextStyle'])
          : TextStyle();

  double? productSubNameFontSizeFromRatioSetting =
      widgetSetting.containsKey('productSubNameFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['productSubNameFontSizeFromFieldKey'])
          : null;

  if (productSubNameTextStyle.fontSize == null) {
    productSubNameTextStyle = productSubNameTextStyle.copyWith(
      fontSize: productSubNameFontSizeFromRatioSetting,
    );
  }

  final bool imageAtEnd = widgetSetting.containsKey('imageAtEnd') &&
      widgetSetting['imageAtEnd'] == "true";

  return ProductRow3(
    product: product,
    fractionDigitsForPrice: fractionDigitsForPrice,
    productNameTextStyle: productNameTextStyle,
    productPriceTextStyle: productPriceTextStyle,
    elementGap: elementGap,
    maxLineOfProductName: maxLineOfProductName,
    prefixProductPrice: prefixProductPrice,
    subfixProductPrice: subfixProductPrice,
    productDescriptionTextStyle: productDescriptionTextStyle,
    upperCaseProductName: upperCaseProductName,
    imageBorderRadius: BorderRadius.circular(imageBorderRadius ?? 0),
    horizontalDivider: horizontalDivider,
    imageHeight: imageHeight ?? imageHeightFromRatioSetting,
    imageWidth: imageWidth ?? imageWidthFromRatioSetting,
    useSubname: useSubname,
    upperCaseProductSubName: upperCaseProductSubName,
    maxLineOfProductSubName: maxLineOfProductSubName,
    productSubNameTextStyle: productSubNameTextStyle,
    imageAtEnd: imageAtEnd,
  );
}
