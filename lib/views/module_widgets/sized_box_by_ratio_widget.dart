import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';

Widget sizedBoxByRatioWidget(Map<String, dynamic> widgetSetting) {
  double? heightFromRatioSetting = widgetSetting
          .containsKey('heightFromFieldKey')
      ? GlobalSetting.ratioStyle.getField(widgetSetting['heightFromFieldKey'])
      : null;

  double? widthFromRatioSetting = widgetSetting.containsKey('widthFromFieldKey')
      ? GlobalSetting.ratioStyle.getField(widgetSetting['widthFromFieldKey'])
      : null;
  final double? height = widgetSetting.containsKey('height')
      ? SizingValue.fromString(widgetSetting['height'])
      : null;

  final double? width = widgetSetting.containsKey('width')
      ? SizingValue.fromString(widgetSetting['width'])
      : null;

  final Widget? child = widgetSetting.containsKey('child')
      ? ModuleHelper.renderWidget(widgetSetting['child'])
      : null;

  return SizedBox(
    width: width ?? widthFromRatioSetting,
    height: height ?? heightFromRatioSetting,
    child: child,
  );
}
