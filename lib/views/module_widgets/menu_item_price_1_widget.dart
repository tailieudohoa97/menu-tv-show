import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/views/widgets/menu_item_price_1.dart';

Widget menuItemPrice1Widget(Map<String, dynamic> widgetSetting) {
  if (!widgetSetting.containsKey('price')) {
    throw ErrorDescription("Require price field for MenuItemPrice1 widget");
  }

  final double price = widgetSetting.containsKey('price')
      ? double.parse(widgetSetting['price'])
      : 0;

  final TextStyle textStyle =
      ModuleHelper.getTextStyle(widgetSetting['textStyle']);

  final String? prefix = widgetSetting.containsKey('prefix')
      ? widgetSetting['prefix'].toString()
      : null;

  return MenuItemPrice1(
    price: price,
    prefix: prefix,
    textStyle: textStyle,
  );
}
