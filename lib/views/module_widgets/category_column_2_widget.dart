import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';
import 'package:tv_menu/views/module_widgets/box_decoration.dart';
import 'package:tv_menu/views/widgets/category_column_2.dart';
import 'package:tv_menu/views/widgets/edge_insets.dart';

Widget categoryColumn2Widget(Map<String, dynamic> widgetSetting) {
  if (!widgetSetting.containsKey('category')) {
    throw ErrorDescription("Require category field for CategoryColumn2 widget");
  }

  final CategoryData category = CategoryData.fromMap(
    ModuleHelper.getDataFromSetting(widgetSetting['category']),
  );

  final bool upperCaseCategoryName =
      widgetSetting['upperCaseCategoryName'] == 'true';

  final double? categoryNameFontSizeFromRatioSetting =
      widgetSetting.containsKey('categoryNameFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['categoryNameFontSizeFromFieldKey'])
          : null;

  TextStyle categoryNameTextStyle =
      widgetSetting.containsKey('categoryNameTextStyle')
          ? ModuleHelper.getTextStyle(widgetSetting['categoryNameTextStyle'])
          : TextStyle();

  if (categoryNameTextStyle.fontSize == null) {
    categoryNameTextStyle = categoryNameTextStyle.copyWith(
      fontSize: categoryNameFontSizeFromRatioSetting,
    );
  }

  final BoxDecoration categoryNameContainerDecoration =
      widgetSetting.containsKey('categoryNameContainerDecoration')
          ? boxDecoration(widgetSetting['categoryNameContainerDecoration'])
          : BoxDecoration();

  final double? categoryImageHeight =
      widgetSetting.containsKey('categoryImageHeight')
          ? SizingValue.fromString(widgetSetting['categoryImageHeight'])
          : null;

  final double? categoryImageWidth =
      widgetSetting.containsKey('categoryImageWidth')
          ? SizingValue.fromString(widgetSetting['categoryImageWidth'])
          : null;
  final double? categoryImageHeightFromRatioSetting =
      widgetSetting.containsKey('categoryImageHeightFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['categoryImageHeightFromFieldKey'])
          : null;

  final double? categoryImageWidthFromRatioSetting =
      widgetSetting.containsKey('categoryImageWidthFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['categoryImageWidthFromFieldKey'])
          : null;

  final double? elementGap = widgetSetting.containsKey('elementGap')
      ? SizingValue.fromString(widgetSetting['elementGap'])
      : null;

  final double? elementGapByRatioSetting =
      widgetSetting.containsKey('elementGapFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['elementGapFromFieldKey'])
          : null;

  final BorderRadius categoryImageBorderRadius = widgetSetting
          .containsKey('categoryImageBorderRadius')
      ? BorderRadius.circular(
          SizingValue.fromString(widgetSetting['categoryImageBorderRadius']))
      : BorderRadius.zero;

  final EdgeInsets categoryNameContainnerPadding = widgetSetting
          .containsKey('categoryNameContainnerPadding')
      ? EdgeInsetsData.fromJson(widgetSetting['categoryNameContainnerPadding'])
          .toEdgeInsets()
      : EdgeInsets.zero;

  final List<Widget> categoryItems = [];
  if (widgetSetting.containsKey('categoryItems')) {
    for (dynamic categoryItem
        in widgetSetting['categoryItems'] as List<dynamic>) {
      categoryItems.add(ModuleHelper.renderWidget(categoryItem));
    }
  }

  return CategoryColumn2(
    category: category,
    categoryImageHeight:
        categoryImageHeight ?? categoryImageHeightFromRatioSetting,
    categoryImageWidth:
        categoryImageWidth ?? categoryImageWidthFromRatioSetting,
    elementGap: elementGap ?? elementGapByRatioSetting,
    categoryItems: categoryItems,
    categoryNameContainerDecoration: categoryNameContainerDecoration,
    categoryImageBorderRadius: categoryImageBorderRadius,
    categoryNameContainnerPadding: categoryNameContainnerPadding,
    categoryNameTextStyle: categoryNameTextStyle,
    hideScrollbarWhenOverflow: true,
    upperCaseCategoryName: upperCaseCategoryName,
  );
}
