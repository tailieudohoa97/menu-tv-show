import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';
import 'package:tv_menu/views/widgets/category_block_1.dart';

Widget categoryBlock1Widget(Map<String, dynamic> widgetSetting) {
  if (!widgetSetting.containsKey('category')) {
    throw ErrorDescription("Require category field for CategoryBlock1 widget");
  }

  final CategoryData category = CategoryData.fromMap(
      ModuleHelper.getDataFromSetting(widgetSetting['category'].toString()));

  final Widget? categoryImage = widgetSetting.containsKey('categoryImage')
      ? ModuleHelper.renderWidget(widgetSetting['categoryImage'])
      : null;

  final double? categoryFontSizeFromRatioSetting =
      widgetSetting.containsKey('categoryFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['categoryFontSizeFromFieldKey'])
          : null;

  TextStyle categoryTextStyle = widgetSetting.containsKey('categoryTextStyle')
      ? ModuleHelper.getTextStyle(widgetSetting['categoryTextStyle'])
      : TextStyle();

  if (categoryTextStyle.fontSize == null) {
    categoryTextStyle = categoryTextStyle.copyWith(
      fontSize: categoryFontSizeFromRatioSetting,
    );
  }

  final double? elementGap = widgetSetting.containsKey('elementGap')
      ? SizingValue.fromString(widgetSetting['elementGap'])
      : null;

  final double? height = widgetSetting.containsKey('height')
      ? SizingValue.fromString(widgetSetting['height'])
      : null;

  final double? width = widgetSetting.containsKey('width')
      ? SizingValue.fromString(widgetSetting['width'])
      : null;

  double? heightFromRatioSetting = widgetSetting
          .containsKey('heightFromFieldKey')
      ? GlobalSetting.ratioStyle.getField(widgetSetting['heightFromFieldKey'])
      : null;

  double? widthFromRatioSetting = widgetSetting.containsKey('widthFromFieldKey')
      ? GlobalSetting.ratioStyle.getField(widgetSetting['widthFromFieldKey'])
      : null;

  final List<Widget> categoryItemRows = [];

  if (widgetSetting.containsKey('categoryItemRows')) {
    for (dynamic cateogryItem
        in widgetSetting['categoryItemRows'] as List<dynamic>) {
      categoryItemRows.add(ModuleHelper.renderWidget(cateogryItem));
    }
  }

  return CategoryBlock1(
    category: category,
    categoryImage: categoryImage,
    categoryItemRows: categoryItemRows,
    categoryTextStyle: categoryTextStyle,
    elementGap: elementGap,
    height: height ?? heightFromRatioSetting,
    width: width ?? widthFromRatioSetting,
  );
}
