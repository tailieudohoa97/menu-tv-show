import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/service_item_with_thumbnail.dart';

Widget serviceItemWithThumbnailWidget(Map<String, dynamic> widgetSetting) {
  ProductData productData = widgetSetting.containsKey('productId')
      ? ProductData.fromMap(
          ModuleHelper.getDataFromSetting(widgetSetting['productId']))
      : ProductData(
          productName: '',
          productDescription: '',
          price: '',
        );
  // String name =
  //     widgetSetting.containsKey('name') ? widgetSetting['name'] : null;

  bool toUpperCaseNameProductName =
      widgetSetting.containsKey('toUpperCaseNameProductName')
          ? widgetSetting['toUpperCaseNameProductName'] == "true"
          : false;

  // String price =
  //     widgetSetting.containsKey('price') ? widgetSetting['price'] : null;

  // String description = widgetSetting.containsKey('description')
  //     ? widgetSetting['description']
  //     : null;

  bool useMediumStyle = widgetSetting.containsKey('useMediumStyle')
      ? widgetSetting['useMediumStyle'] == "true"
      : false;

  Color colorPrice = widgetSetting.containsKey('colorPrice')
      ? Color(int.parse(widgetSetting['colorPrice']))
      : const Color(0xFFFFFFFF);

  Color colorProductName = widgetSetting.containsKey('colorProductName') &&
          widgetSetting['colorProductName'] != ""
      ? Color(int.parse(widgetSetting['colorProductName']))
      : const Color(0xFFFFFFFF);

  Color colorDescription = widgetSetting.containsKey('colorDescription') &&
          widgetSetting['colorDescription'] != ""
      ? Color(int.parse(widgetSetting['colorDescription']))
      : const Color(0xFFFFFFFF);

  int maxLineProductName = widgetSetting.containsKey('maxLineProductName')
      ? int.parse(widgetSetting['maxLineProductName'])
      : 1;

  int maxLineProductDescription =
      widgetSetting.containsKey('maxLineProductDescription')
          ? int.parse(widgetSetting['maxLineProductDescription'])
          : 2;
  return serviceItemWithThumbnail(
    name: productData.productName,
    toUpperCaseNameProductName: toUpperCaseNameProductName,
    price: productData.price,
    description: productData.productDescription,
    useMediumStyle: useMediumStyle,
    colorPrice: colorPrice,
    colorProductName: colorProductName,
    colorDescription: colorDescription,
    pathThumnailProduct: productData.image ?? GlobalSetting.noImage,
    maxLineProductName: maxLineProductName,
    maxLineProductDescription: maxLineProductDescription,
  );
}
