import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';

Widget intrinsicHeightWidget(Map<String, dynamic> widgetSetting) {
  Widget? child = widgetSetting.containsKey('child')
      ? ModuleHelper.renderWidget(widgetSetting['child'])
      : null;

  return IntrinsicHeight(
    child: child,
  );
}
