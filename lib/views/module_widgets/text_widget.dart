import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
// import 'package:tv_menu/helper/module_helper.dart';

// [112308TIN] add dataFromSetting property
// [092325Tin] Combine properties of textstyle into style property
// [092320Tin] Add style property
// [092327Tin] Add fontSizeFromFieldKey
Widget textWidget(Map<String, dynamic> widgetSetting) {
  TextAlign textAlign = widgetSetting.containsKey('textAlign')
      ? (ModuleHelper.arrTextAlign[widgetSetting['textAlign']] ??
          TextAlign.center)
      : TextAlign.center;

  double? fontSizeFromRatioSetting = widgetSetting
          .containsKey('fontSizeFromFieldKey')
      ? GlobalSetting.ratioStyle.getField(widgetSetting['fontSizeFromFieldKey'])
      : null;
  // if does not contains key style will use keys: color, fontSize
  TextStyle style = widgetSetting.containsKey('style')
      ? ModuleHelper.getTextStyle(widgetSetting['style'])
      : TextStyle(
          color: widgetSetting.containsKey('color')
              ? (Color(int.parse(widgetSetting['color'])))
              : null,
          fontSize: widgetSetting.containsKey("fontSize")
              ? SizingValue.fromString(widgetSetting['fontSize'])
              : null);

  if (style.fontSize == null) {
    style = style.copyWith(fontSize: fontSizeFromRatioSetting);
  }

  // [112308TIN] add get data from data setting for this widget
  String data = widgetSetting.containsKey('data') ? widgetSetting['data'] : '';
  String dataFromSettingValue = widgetSetting.containsKey('dataFromSetting')
      ? widgetSetting['dataFromSetting'].toString()
      : '.';
  var [dataFromSettingKey, dataFromSettingSubKey] =
      dataFromSettingValue.split(RegExp(r'\s*\.\s*')).toList();

  if (dataFromSettingKey.isNotEmpty) {
    data = ModuleHelper.getDataFromSetting(
        dataFromSettingKey, dataFromSettingSubKey ?? null);
  }

  return Text(
    data,
    style: style,
    textAlign: textAlign,
  );
}
