import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';
import 'package:tv_menu/views/widgets/category_column_1.dart';

// [102302Tin] Add fields: mainAxisAlignment, crossAxisAlignment
// [092327Tin] Add field categoryFontSizeFromFieldKey
Widget categoryColumn1Widget(Map<String, dynamic> widgetSetting) {
  if (!widgetSetting.containsKey('category')) {
    throw ErrorDescription("Require category field for CategoryColumn1 widget");
  }

  final CategoryData category = CategoryData.fromMap(
      ModuleHelper.getDataFromSetting(widgetSetting['category'].toString()));

  final bool upperCaseCategoryName =
      widgetSetting['upperCaseCategoryName'] == 'true';

  final double? categoryFontSizeFromRatioSetting =
      widgetSetting.containsKey('categoryFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['categoryFontSizeFromFieldKey'])
          : null;

  TextStyle categoryTextStyle = widgetSetting.containsKey('categoryTextStyle')
      ? ModuleHelper.getTextStyle(widgetSetting['categoryTextStyle'])
      : TextStyle();

  if (categoryTextStyle.fontSize == null) {
    categoryTextStyle = categoryTextStyle.copyWith(
      fontSize: categoryFontSizeFromRatioSetting,
    );
  }

  final double? categoryImageHeight =
      widgetSetting.containsKey('categoryImageHeight')
          ? SizingValue.fromString(widgetSetting['categoryImageHeight'])
          : null;

  final double? categoryImageWidth =
      widgetSetting.containsKey('categoryImageWidth')
          ? SizingValue.fromString(widgetSetting['categoryImageWidth'])
          : null;

  final double? elementGap = widgetSetting.containsKey('elementGap')
      ? SizingValue.fromString(widgetSetting['elementGap'])
      : null;

  final List<Widget> productRows = [];

  double? categoryImageHeightFromRatioSetting =
      widgetSetting.containsKey('categoryImageHeightFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['categoryImageHeightFromFieldKey'])
          : null;

  double? categoryImageWidthFromRatioSetting =
      widgetSetting.containsKey('categoryImageWidthFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['categoryImageWidthFromFieldKey'])
          : null;

  MainAxisAlignment mainAxisAlignment = widgetSetting
          .containsKey('mainAxisAlignment')
      ? ModuleHelper.arrMainAxisAlignment[widgetSetting['mainAxisAlignment']] ??
          MainAxisAlignment.start
      : MainAxisAlignment.start;

  CrossAxisAlignment crossAxisAlignment =
      widgetSetting.containsKey('crossAxisAlignment')
          ? ModuleHelper
                  .arrCrossAxisAlignment[widgetSetting['crossAxisAlignment']] ??
              CrossAxisAlignment.start
          : CrossAxisAlignment.start;

  if (widgetSetting.containsKey('productRows')) {
    for (dynamic productRow in widgetSetting['productRows'] as List<dynamic>) {
      productRows.add(ModuleHelper.renderWidget(productRow));
    }
  }

  return CategoryColumn1(
    category: category,
    categoryImageHeight:
        categoryImageHeight ?? categoryImageHeightFromRatioSetting,
    categoryImageWidth:
        categoryImageWidth ?? categoryImageWidthFromRatioSetting,
    categoryTextStyle: categoryTextStyle,
    elementGap: elementGap,
    upperCaseCategoryName: upperCaseCategoryName,
    productRows: productRows,
    mainAxisAlignment: mainAxisAlignment,
    crossAxisAlignment: crossAxisAlignment,
  );
}
