import 'dart:html';

import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/views/widgets/menu_column_1.dart';

Widget menuColumn1Widget(Map<String, dynamic> widgetSetting) {
  final String brand = widgetSetting.containsKey('brand')
      ? widgetSetting['brand'].toString()
      : '';

  final String slogan = widgetSetting.containsKey('slogan')
      ? widgetSetting['slogan'].toString()
      : '';

  TextStyle brandTextStyle = widgetSetting.containsKey('brandTextStyle')
      ? ModuleHelper.getTextStyle(widgetSetting['brandTextStyle'])
      : TextStyle();

  TextStyle sloganTextStyle = widgetSetting.containsKey('sloganTextStyle')
      ? ModuleHelper.getTextStyle(widgetSetting['sloganTextStyle'])
      : TextStyle();

  final double? brandFontSizeFromRatioSetting =
      widgetSetting.containsKey('brandFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['brandFontSizeFromFieldKey'])
          : null;

  final double? sloganFontSizeFromRatioSetting =
      widgetSetting.containsKey('sloganFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['sloganFontSizeFromFieldKey'])
          : null;

  if (brandTextStyle.fontSize == null) {
    brandTextStyle = brandTextStyle.copyWith(
      fontSize: brandFontSizeFromRatioSetting,
    );
  }

  if (sloganTextStyle.fontSize == null) {
    sloganTextStyle = sloganTextStyle.copyWith(
      fontSize: sloganFontSizeFromRatioSetting,
    );
  }

  final List<Widget> menuItems = [];

  if (widgetSetting.containsKey('menuItems')) {
    for (dynamic menuItem in widgetSetting['menuItems'] as List<dynamic>) {
      menuItems.add(ModuleHelper.renderWidget(menuItem));
    }
  }

  return MenuColumn1(
    menuItems: menuItems,
    brand: brand,
    slogan: slogan,
    brandTextStyle: brandTextStyle,
    sloganTextStyle: sloganTextStyle,
  );
}
