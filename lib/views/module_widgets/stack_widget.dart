import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';

// [112320TIN] add clipBehavior property
// [112314TIN] add alignment property
Widget stackWidget(Map<dynamic, dynamic> widgetSetting) {
  List<dynamic> childrens =
      widgetSetting.containsKey('children') ? widgetSetting['children'] : [];
  final AlignmentDirectional? alignment = widgetSetting.containsKey('alignment')
      ? ModuleHelper.arrAlignmentDirectional[widgetSetting['alignment']]
      : null;

  final Clip clipBehavior = widgetSetting.containsKey('clipBehavior')
      ? ModuleHelper.arrClip[widgetSetting['clipBehavior']] ?? Clip.hardEdge
      : Clip.hardEdge;

  return Stack(
    alignment: alignment ?? AlignmentDirectional.topStart,
    clipBehavior: clipBehavior,
    children:
        childrens.map((element) => ModuleHelper.renderWidget(element)).toList(),
  );
}
