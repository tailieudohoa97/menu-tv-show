import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/module_helper.dart';

// [112310TIN] add colorFilter of image
BoxDecoration boxDecoration(Map<String, dynamic> setting) {
  //Lấy hình background theo "dataSetting" với key
  //Nếu lấy data từ key thì ghi "image", còn sử dụng url thì ghi "imageUrl"
  String _image = setting.containsKey('imageUrl')
      ? setting['imageUrl']
      : (setting.containsKey('image')
          ? ModuleHelper.getDataFromSetting(setting['image'], 'image') ?? ""
          : "");

  final _color = setting['color'];
  // final _image = setting['image'];
  final _fit = setting['boxFit'] ?? 'cover';
  final _border = setting['border'] ?? {};
  final _borderRadius = setting['borderRadius'];
  final _backgroundBlendMode = setting['backgroundBlendMode'];

  final ColorFilter? colorFilter = setting.containsKey('colorFilter')
      ? ModuleHelper.getColorFilter(setting['colorFilter'])
      : null;

  return BoxDecoration(
    color: _color != null ? Color(int.parse(_color)) : null,
    image: _image != ""
        ? ModuleHelper.getImageDecoration(_image,
            boxFit: ModuleHelper.boxFitMap[_fit] ?? BoxFit.cover,
            colorFilter: colorFilter)
        : null,
    border: Border.all(
      width: SizingValue.fromString(_border['width'] ?? "1"),
      // [092319Tin] Replace exp condition,
      // because _border['color'] type is String
      // but it throw error "type 'String' is not a 'bool' in boolean expression"
      color: _border['color'] != null
          ? Color(int.parse(_border['color']))
          : Colors.transparent,
      // color: _border['color'] ?? false
      //     ? Color(int.parse(_border['color']))
      //     : Colors.transparent,
    ),

    // [092325Tin] Replace double.parse to SizingValue.fromString
    borderRadius: BorderRadius.circular(
      SizingValue.fromString(_borderRadius ?? "0"),
    ),
    backgroundBlendMode: ModuleHelper.blendModeMap[_backgroundBlendMode],
  );
}
