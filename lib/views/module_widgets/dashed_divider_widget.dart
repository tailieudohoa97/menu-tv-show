import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/views/widgets/dashed_divider.dart';

// [112314TIN] create this widget
Widget dashedDividerWidget(Map<String, dynamic> widgetSetting) {
  final double? dashWidth = widgetSetting.containsKey('dashWidth')
      ? SizingValue.fromString(widgetSetting['dashWidth'])
      : null;
  final double? dashSpace = widgetSetting.containsKey('dashSpace')
      ? SizingValue.fromString(widgetSetting['dashSpace'])
      : null;
  final double? indent = widgetSetting.containsKey('indent')
      ? SizingValue.fromString(widgetSetting['indent'])
      : null;
  final double? thickness = widgetSetting.containsKey('thickness')
      ? SizingValue.fromString(widgetSetting['thickness'])
      : null;
  final Color? color = widgetSetting.containsKey('color')
      ? Color(int.parse(widgetSetting['color']))
      : null;

  return DashedDivider(
    dashWidth: dashWidth,
    dashSpace: dashSpace,
    color: color,
    indent: indent,
    thickness: thickness,
  );
}
