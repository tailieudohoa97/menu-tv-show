import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/module_helper.dart';

Widget wrapWidget(Map<String, dynamic> widgetSetting) {
  final WrapAlignment alignment = widgetSetting.containsKey('alignment')
      ? ModuleHelper.arrWrapAlignment[widgetSetting['alignment']] ??
          WrapAlignment.start
      : WrapAlignment.start;

  final WrapAlignment runAlignment = widgetSetting.containsKey('runAlignment')
      ? ModuleHelper.arrWrapAlignment[widgetSetting['runAlignment']] ??
          WrapAlignment.start
      : WrapAlignment.start;

  final double runSpacing = widgetSetting.containsKey('runSpacing')
      ? SizingValue.fromString(widgetSetting['runSpacing'])
      : 0;

  final double spacing = widgetSetting.containsKey('spacing')
      ? SizingValue.fromString(widgetSetting['spacing'])
      : 0;

  final WrapCrossAlignment crossAxisAlignment =
      widgetSetting.containsKey('crossAxisAlignment')
          ? (ModuleHelper
                  .arrWrapCrossAlignment[widgetSetting['crossAxisAlignment']] ??
              WrapCrossAlignment.start)
          : WrapCrossAlignment.start;

  final Axis direction = widgetSetting.containsKey('direction')
      ? (ModuleHelper.arrAxis[widgetSetting['direction']] ?? Axis.horizontal)
      : Axis.horizontal;

  final VerticalDirection verticalDirection =
      widgetSetting.containsKey('verticalDirection')
          ? (ModuleHelper
                  .arrVerticalDirection[widgetSetting['verticalDirection']] ??
              VerticalDirection.down)
          : VerticalDirection.down;

  final Clip clipBehavior = widgetSetting.containsKey('clipBehavior')
      ? ModuleHelper.arrClip[widgetSetting['clipBehavior']] ?? Clip.none
      : Clip.none;

  final List<Widget> children = [];
  if (widgetSetting.containsKey('children')) {
    for (Map<String, dynamic> childSetting in widgetSetting['children']) {
      children.add(ModuleHelper.renderWidget(childSetting));
    }
  }

  return Wrap(
    crossAxisAlignment: crossAxisAlignment,
    direction: direction,
    runAlignment: runAlignment,
    runSpacing: runSpacing,
    spacing: spacing,
    verticalDirection: verticalDirection,
    alignment: alignment,
    clipBehavior: clipBehavior,
    children: children,
  );
}
