import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/views/widgets/edge_insets.dart';

Widget paddingWidget(dynamic setting) {
  return Padding(
    padding: EdgeInsetsData.fromJson(setting['padding']).toEdgeInsets(),
    child: ModuleHelper.renderWidget(setting['child']),
  );
}
