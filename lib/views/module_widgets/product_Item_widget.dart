import 'package:flutter/material.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
// import 'package:tv_menu/helper/module_helper.dart';
// import 'package:tv_menu/views/widgets/image.dart';
import 'package:tv_menu/views/widgets/product_item.dart';

// [092327Tin] Add productDataKey field to use available dataSetting in json data
Widget productItemWidget(Map<String, dynamic> widgetSetting) {
  ProductData? productData = widgetSetting.containsKey('productDataKey')
      ? ProductData.fromMap(
          ModuleHelper.getDataFromSetting(widgetSetting['productDataKey']))
      : null;

  String name = widgetSetting.containsKey('name')
      ? widgetSetting['name']
      : productData?.productName;

  bool toUpperCaseNameProductName =
      widgetSetting.containsKey('toUpperCaseNameProductName')
          ? widgetSetting['toUpperCaseNameProductName'] == "true"
          : false;

  String price = widgetSetting.containsKey('price')
      ? widgetSetting['price']
      : productData?.price;

  String description = widgetSetting.containsKey('description')
      ? widgetSetting['description']
      : productData?.price;

  bool useMediumStyle = widgetSetting.containsKey('useMediumStyle')
      ? widgetSetting['useMediumStyle'] == "true"
      : false;

  Color colorPrice = widgetSetting.containsKey('colorPrice')
      ? Color(int.parse(widgetSetting['colorPrice']))
      : const Color(0xFFFFFFFF);

  Color colorProductName = widgetSetting.containsKey('colorProductName') &&
          widgetSetting['colorProductName'] != ""
      ? Color(int.parse(widgetSetting['colorProductName']))
      : const Color(0xFFFFFFFF);

  Color colorDescription = widgetSetting.containsKey('colorDescription') &&
          widgetSetting['colorDescription'] != ""
      ? Color(int.parse(widgetSetting['colorDescription']))
      : const Color(0xFFFFFFFF);

  bool useThumnailProduct = widgetSetting.containsKey('useThumnailProduct')
      ? widgetSetting['useThumnailProduct'] == "true"
      : false;

  String pathThumnailProduct =
      widgetSetting.containsKey('pathThumnailProduct') &&
              widgetSetting['pathThumnailProduct'] != ""
          ? widgetSetting['pathThumnailProduct']
          : GlobalSetting.noImage;

  int maxLineProductName = widgetSetting.containsKey('maxLineProductName')
      ? int.parse(widgetSetting['maxLineProductName'])
      : 1;

  int maxLineProductDescription =
      widgetSetting.containsKey('maxLineProductDescription')
          ? int.parse(widgetSetting['maxLineProductDescription'])
          : 2;

  return productItem(
    name: name,
    toUpperCaseNameProductName: toUpperCaseNameProductName,
    price: price,
    description: description,
    useMediumStyle: useMediumStyle,
    colorPrice: colorPrice,
    colorProductName: colorProductName,
    colorDescription: colorDescription,
    useThumnailProduct: useThumnailProduct,
    pathThumnailProduct: pathThumnailProduct,
    maxLineProductName: maxLineProductName,
    maxLineProductDescription: maxLineProductDescription,
  );
}
