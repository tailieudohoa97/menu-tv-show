import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/views/module_widgets/box_decoration.dart';
import 'package:tv_menu/views/widgets/edge_insets.dart';

// [112316TIN] add clipBehavior property
Widget containerWidget(Map<String, dynamic> widgetSetting) {
  return Container(
    clipBehavior: widgetSetting.containsKey('clipBehavior')
        ? (ModuleHelper.arrClip[widgetSetting['clipBehavior']] ?? Clip.none)
        : Clip.none,
    width: widgetSetting.containsKey("width")
        ? SizingValue.fromString(widgetSetting['width'])
        : null,
    height: widgetSetting.containsKey("height")
        ? SizingValue.fromString(widgetSetting['height'])
        : null,
    padding: widgetSetting.containsKey('padding')
        ? EdgeInsetsData.fromJson(widgetSetting['padding']).toEdgeInsets()
        : null,
    margin: widgetSetting.containsKey('margin')
        ? EdgeInsetsData.fromJson(widgetSetting['margin']).toEdgeInsets()
        : null,
    decoration: widgetSetting.containsKey('decoration')
        ? boxDecoration(widgetSetting['decoration'])
        : null,
    color: widgetSetting.containsKey('color')
        ? (Color(int.parse(widgetSetting['color'])))
        : null,
    child: widgetSetting.containsKey('child')
        ? ModuleHelper.renderWidget(widgetSetting['child'])
        : null,
  );
}
