import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';
import 'package:tv_menu/views/widgets/category_block_2.dart';

Widget categoryBlock2Widget(Map<String, dynamic> widgetSetting) {
  if (!widgetSetting.containsKey('category')) {
    throw ErrorDescription("Require category field for CategoryBlock2 widget");
  }

  final CategoryData category = CategoryData.fromMap(
      ModuleHelper.getDataFromSetting(widgetSetting['category'].toString()));

  final bool upperCaseCategoryName =
      widgetSetting['upperCaseCategoryName'] == 'true';

  final Widget? divider = widgetSetting.containsKey('divider')
      ? ModuleHelper.renderWidget(widgetSetting['divider'])
      : null;

  TextStyle categoryNameTextStyle =
      widgetSetting.containsKey('categoryNameTextStyle')
          ? ModuleHelper.getTextStyle(widgetSetting['categoryNameTextStyle'])
          : TextStyle();

  final double? categoryNameFontSizeFromRatioSetting =
      widgetSetting.containsKey('categoryNameFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['categoryNameFontSizeFromFieldKey'])
          : null;

  if (categoryNameTextStyle.fontSize == null) {
    categoryNameTextStyle = categoryNameTextStyle.copyWith(
      fontSize: categoryNameFontSizeFromRatioSetting,
    );
  }

  TextStyle categoryDescriptionTextStyle = widgetSetting
          .containsKey('categoryDescriptionTextStyle')
      ? ModuleHelper.getTextStyle(widgetSetting['categoryDescriptionTextStyle'])
      : TextStyle();

  final double? categoryDesFontSizeFromRatioSetting =
      widgetSetting.containsKey('categoryDesFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['categoryDesFontSizeFromFieldKey'])
          : null;

  if (categoryDescriptionTextStyle.fontSize == null) {
    categoryDescriptionTextStyle = categoryDescriptionTextStyle.copyWith(
      fontSize: categoryDesFontSizeFromRatioSetting,
    );
  }

  final double? categoryItemGap = widgetSetting.containsKey('categoryItemGap')
      ? SizingValue.fromString(widgetSetting['categoryItemGap'])
      : null;

  final double categoryItemSectionPadding =
      widgetSetting.containsKey('categoryItemSectionPadding')
          ? SizingValue.fromString(widgetSetting['categoryItemSectionPadding'])
          : 0;

  final double indentCategoryDescription =
      widgetSetting.containsKey('indentCategoryDescription')
          ? SizingValue.fromString(widgetSetting['indentCategoryDescription'])
          : 0;

  final List<Widget> categoryItems = [];

  if (widgetSetting.containsKey('categoryItems')) {
    for (dynamic cateogryItem
        in widgetSetting['categoryItems'] as List<dynamic>) {
      categoryItems.add(ModuleHelper.renderWidget(cateogryItem));
    }
  }

  return CategoryBlock2(
    category: category,
    categoryDescriptionTextStyle: categoryDescriptionTextStyle,
    categoryItemGap: categoryItemGap,
    categoryItemSectionPadding: categoryItemSectionPadding,
    categoryItems: categoryItems,
    categoryNameTextStyle: categoryNameTextStyle,
    divider: divider,
    indentCategoryDescription: indentCategoryDescription,
    upperCaseCategoryName: upperCaseCategoryName,
  );
}
