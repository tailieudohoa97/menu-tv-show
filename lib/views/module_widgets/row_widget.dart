import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';

// Default row setting
//   Key? key,
//   MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
//   MainAxisSize mainAxisSize = MainAxisSize.max,
//   CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.center,
//   TextDirection? textDirection,
//   VerticalDirection verticalDirection = VerticalDirection.down,
//   TextBaseline? textBaseline,
//   List<dynamic> childrens = const <dynamic>[],

Widget rowWidget(Map<String, dynamic> widgetSetting) {
  List<dynamic> childrens =
      widgetSetting.containsKey('children') ? widgetSetting['children'] : [];

  //Sử dụng giá trị Bool để chọn giá trị tương ứng với thuộc tính
  TextDirection _textDirection = widgetSetting.containsKey('textRightToLeft') &&
          bool.parse(widgetSetting['textRightToLeft'])
      ? TextDirection.rtl
      : TextDirection.ltr;

  //[092313HG] add mainAxisAlignment
  /** Giải thích
   * 1. Đoạn mã này bắt đầu bằng việc kiểm tra xem widgetSetting có chứa khóa "mainAxisAlignment" hay không
bằng cách sử dụng widgetSetting.containsKey('mainAxisAlignment')
   -------------------------------------- 
   * 2. Nếu widgetSetting chứa khóa "mainAxisAlignment", điều kiện sau dấu ? được thực hiện:
  - "ModuleHelper.arrMainAxisAlignment[widgetSetting['mainAxisAlignment']]": 
  Đoạn mã này thử truy cập "widgetSetting['mainAxisAlignment']" để lấy giá trị tương ứng từ "ModuleHelper.arrMainAxisAlignment".

      + "ModuleHelper.arrMainAxisAlignment" có thể là một bảng ánh xạ (map) chứa các giá trị được ánh xạ từ chuỗi
      "mainAxisAlignment" sang các giá trị "MainAxisAlignment" tương ứng.

  - "?? MainAxisAlignment.start": Đoạn mã này sử dụng toán tử ?? để 
  trả về "MainAxisAlignment.start" nếu không tìm thấy giá trị tương ứng trong "ModuleHelper.arrMainAxisAlignment".
  -------------------------------------- 
   * 3. Nếu "widgetSetting" không chứa khóa "mainAxisAlignment", điều kiện sau dấu : được thực 
  hiện và giá trị "MainAxisAlignment.start" được gán cho mainAxisAlignment.
   
   */
  MainAxisAlignment mainAxisAlignment =
      widgetSetting.containsKey('mainAxisAlignment')
          ? (ModuleHelper
                  .arrMainAxisAlignment[widgetSetting['mainAxisAlignment']] ??
              MainAxisAlignment.start)
          : MainAxisAlignment.start;

  //[092313HG] add crossAxisAlignment
  CrossAxisAlignment crossAxisAlignment =
      widgetSetting.containsKey('crossAxisAlignment')
          ? (ModuleHelper
                  .arrCrossAxisAlignment[widgetSetting['crossAxisAlignment']] ??
              CrossAxisAlignment.center)
          : CrossAxisAlignment.center;

  return Row(
    textDirection: _textDirection,
    crossAxisAlignment: crossAxisAlignment,
    mainAxisAlignment: mainAxisAlignment,
    children:
        childrens.map((element) => ModuleHelper.renderWidget(element)).toList(),
  );
}
