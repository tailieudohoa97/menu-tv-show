import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';

Widget verticalDividerWidget(Map<String, dynamic> widgetSetting) {
  return VerticalDivider(
    width: widgetSetting.containsKey('width')
        ? SizingValue.fromString(widgetSetting['width'])
        : null,
    thickness: widgetSetting.containsKey('thickness')
        ? SizingValue.fromString(widgetSetting['thickness'])
        : null,
    indent: widgetSetting.containsKey('indent')
        ? SizingValue.fromString(widgetSetting['indent'])
        : null,
    endIndent: widgetSetting.containsKey('endIndent')
        ? SizingValue.fromString(widgetSetting['endIndent'])
        : null,
    color: widgetSetting.containsKey('color')
        ? (Color(int.parse(widgetSetting['color'])))
        : null,
  );
}
