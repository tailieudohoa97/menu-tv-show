import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/views/widgets/image.dart';

// [092321Tin] Add width, height for image widget
Widget imageWidget(Map<String, dynamic> widgetSetting) {
  //Get data from DataSetting list
  String imageId = widgetSetting.containsKey('imageId')
      ? ModuleHelper.getDataFromSetting(widgetSetting['imageId'], 'image') ?? ""
      : GlobalSetting.noImage;

  //dynamic Data
  bool fixCover = widgetSetting.containsKey('fixCover')
      ? widgetSetting['fixCover'] == "true"
      : true;

  double? width = widgetSetting.containsKey('width')
      ? SizingValue.fromString(widgetSetting['width'])
      : null;

  double? height = widgetSetting.containsKey('height')
      ? SizingValue.fromString(widgetSetting['height'])
      : null;

  return image(
    imagePath: imageId,
    fixCover: fixCover,
    width: width,
    height: height,
  );
}
