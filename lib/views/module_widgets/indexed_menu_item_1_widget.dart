import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/indexed_menu_item_1.dart';

// [102302Tin] Add field indexChipRadiusFromFieldKey
Widget indexedMenuItem1Widget(Map<String, dynamic> widgetSetting) {
  if (!widgetSetting.containsKey('product')) {
    throw ErrorDescription("Require product field for ProductRow1 widget");
  }

  final ProductData product = ProductData.fromMap(
      ModuleHelper.getDataFromSetting(widgetSetting['product']));

  final Color? indexChipBgColor = widgetSetting.containsKey('indexChipBgColor')
      ? Color(int.parse(widgetSetting['indexChipBgColor']))
      : null;

  final Color? indexChipFgColor = widgetSetting.containsKey('indexChipFgColor')
      ? Color(int.parse(widgetSetting['indexChipFgColor']))
      : null;

  final int index = widgetSetting.containsKey('index')
      ? int.parse(widgetSetting['index'])
      : 0;

  final double? elementGap = widgetSetting.containsKey('elementGap')
      ? SizingValue.fromString(widgetSetting['elementGap'])
      : null;

  final double? indexChipRadiusFromRatioSetting =
      widgetSetting.containsKey('indexChipRadiusFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['indexChipRadiusFromFieldKey'])
          : null;

  final double? indexChipRadius = widgetSetting.containsKey('indexChipRadius')
      ? SizingValue.fromString(widgetSetting['indexChipRadius'])
      : null;

  double? fontSizeFromRatioSetting = widgetSetting
          .containsKey('fontSizeFromFieldKey')
      ? GlobalSetting.ratioStyle.getField(widgetSetting['fontSizeFromFieldKey'])
      : null;

  TextStyle textStyle = widgetSetting.containsKey('textStyle')
      ? ModuleHelper.getTextStyle(widgetSetting['textStyle'])
      : TextStyle();

  if (textStyle.fontSize == null) {
    textStyle = textStyle.copyWith(
      fontSize: fontSizeFromRatioSetting,
    );
  }

  double? priceFontSizeFromRatioSetting =
      widgetSetting.containsKey('priceFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['priceFontSizeFromFieldKey'])
          : null;

  TextStyle priceTextStyle = widgetSetting.containsKey('priceTextStyle')
      ? ModuleHelper.getTextStyle(widgetSetting['priceTextStyle'])
      : TextStyle();

  if (priceTextStyle.fontSize == null) {
    priceTextStyle = priceTextStyle.copyWith(
      fontSize: priceFontSizeFromRatioSetting,
    );
  }

  return IndexedMenuItem1(
    index: index,
    product: product,
    elementGap: elementGap,
    indexChipBgColor: indexChipBgColor,
    indexChipFgColor: indexChipFgColor,
    indexChipRadius: indexChipRadius ?? indexChipRadiusFromRatioSetting,
    priceTextStyle: priceTextStyle,
    textStyle: textStyle,
  );
}
