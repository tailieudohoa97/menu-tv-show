import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/repo_data/data_sample_restaurant.dart';
// import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/views/widgets/category_title_single_column.dart';

Widget categoryTitleSingleColumnWidget(
  Map<String, dynamic> widgetSetting,
) {
  //092314LH Fix dynamic sample data to template
  Map<String, dynamic> dataList =
      widgetSetting.containsKey('dataList') ? widgetSetting['dataList'] : {};

  dynamic categoryData = dataList.containsKey('category')
      ? CategoryData.fromMap(
          ModuleHelper.getDataFromSetting(dataList['category']))
      : [];
  List<dynamic> products =
      dataList.containsKey('product') ? dataList['product'] : [];
  List<ProductData> productData = products
      .map((item) => ProductData.fromMap(ModuleHelper.getDataFromSetting(item)))
      .toList();
  // dynamic categoryDataList = DataSample().categoryDataList;

  // int indexCategory = widgetSetting.containsKey('indexCategory')
  //     ? int.parse(widgetSetting['indexCategory'])
  //     : 0;
  bool useBackgroundCategoryTitle =
      widgetSetting.containsKey('useBackgroundCategoryTitle')
          ? widgetSetting['useBackgroundCategoryTitle'] == "true"
          : true;

  String pathImageBgCategoryTitle =
      widgetSetting.containsKey('pathImageBgCategoryTitle')
          ? widgetSetting['pathImageBgCategoryTitle']
          : "";

  // [092326HG]
  //Get data from DataSetting list
  String imageId = widgetSetting.containsKey('imageId')
      ? ModuleHelper.getDataFromSetting(widgetSetting['imageId'], 'image') ?? ""
      : "";

  Color colorCategoryTitle = widgetSetting.containsKey('colorCategoryTitle')
      ? Color(int.parse(widgetSetting['colorCategoryTitle']))
      : Color(0xFFFFFFFF);

  Color colorBackgroundCategoryTitle =
      widgetSetting.containsKey('colorBackgroundCategoryTitle')
          ? Color(int.parse(widgetSetting['colorBackgroundCategoryTitle']))
          : const Color(0x00FFFFFF);

  Color colorPrice = widgetSetting.containsKey('colorPrice')
      ? Color(int.parse(widgetSetting['colorPrice']))
      : const Color(0xFFFFFFFF);

  Color colorProductName = widgetSetting.containsKey('colorProductName')
      ? Color(int.parse(widgetSetting['colorProductName']))
      : const Color(0xFFFFFFFF);

  Color colorDescription = widgetSetting.containsKey('colorDescription')
      ? Color(int.parse(widgetSetting['colorDescription']))
      : const Color(0xFFFFFFFF);

  bool useThumnailProduct = widgetSetting.containsKey('useThumnailProduct')
      ? widgetSetting['useThumnailProduct'] == "true"
      : false;

  String pathThumnailProduct = widgetSetting.containsKey('pathThumnailProduct')
      ? widgetSetting['pathThumnailProduct']
      : GlobalSetting.noImage;

  bool toUpperCaseNameProductName =
      widgetSetting.containsKey('toUpperCaseNameProductName')
          ? widgetSetting['toUpperCaseNameProductName'] == "true"
          : true;

  bool useMediumStyle = widgetSetting.containsKey('useMediumStyle')
      ? widgetSetting['useMediumStyle'] == "true"
      : false;

  // [092325Tin] add useFullWidthCategoryTitle property
  bool useFullWidthCategoryTitle =
      widgetSetting['useFullWidthCategoryTitle'] == 'true';

  // int startIndexProduct = widgetSetting.containsKey('startIndexProduct')
  //     ? int.parse(widgetSetting['startIndexProduct'])
  //     : 0;

  // int endIndexProduct = widgetSetting.containsKey('endIndexProduct')
  //     ? int.parse(widgetSetting['endIndexProduct'])
  //     : 3;
  // [092328Tuan] add runSpacing and mainAxisAligment
  double runSpacing = widgetSetting.containsKey("runSpacing")
      ? SizingValue.fromString(widgetSetting['runSpacing'])
      : 0;
  MainAxisAlignment mainAxisAlignment =
      widgetSetting.containsKey('mainAxisAlignment')
          ? (ModuleHelper
                  .arrMainAxisAlignment[widgetSetting['mainAxisAlignment']] ??
              MainAxisAlignment.start)
          : MainAxisAlignment.start;
  double spaceBetweenCategoryProductList = widgetSetting
          .containsKey("spaceBetweenCategoryProductList")
      ? SizingValue.fromString(widgetSetting['spaceBetweenCategoryProductList'])
      : 0;
  return categoryTitleSingleColumn(
    spaceBetweenCategoryProductList: spaceBetweenCategoryProductList,
    mainAxisAlignment: mainAxisAlignment,
    runSpacing: runSpacing,
    //092314LH Fix dynamic sample data to template
    categoryData: categoryData,
    productData: productData,
    // indexCategory: indexCategory,
    useBackgroundCategoryTitle: useBackgroundCategoryTitle,
    // pathImageBgCategoryTitle: pathImageBgCategoryTitle,
    pathImageBgCategoryTitle: imageId, // [092326HG]
    colorCategoryTitle: colorCategoryTitle,
    colorBackgroundCategoryTitle: colorBackgroundCategoryTitle,
    colorPrice: colorPrice,
    colorProductName: colorProductName,
    colorDescription: colorDescription,
    useThumnailProduct: useThumnailProduct,
    pathThumnailProduct: pathThumnailProduct,
    toUpperCaseNameProductName: toUpperCaseNameProductName,
    useMediumStyle: useMediumStyle,
    useFullWidthCategoryTitle: useFullWidthCategoryTitle,
    // startIndexProduct: startIndexProduct,
    // endIndexProduct: endIndexProduct,
  );
}
