import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';

Widget rotatedBoxWidget(dynamic widgetSetting) {
  return RotatedBox(
    quarterTurns: widgetSetting.containsKey('quarterTurns')
        ? int.parse(widgetSetting['quarterTurns'])
        : 0,
    child: widgetSetting.containsKey('child')
        ? ModuleHelper.renderWidget(widgetSetting['child'])
        : const Placeholder(),
  );
}
