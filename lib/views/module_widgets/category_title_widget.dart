import 'package:flutter/material.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';
import 'package:tv_menu/views/widgets/category_title.dart';
// import 'package:tv_menu/helper/module_helper.dart';
// import 'package:tv_menu/views/widgets/category_title_single_column.dart';

Widget categoryTitleWidget(
  Map<String, dynamic> widgetSetting,
) {
  //Get data from DataSetting list
  CategoryData categoryData = widgetSetting.containsKey('categoryId')
      ? CategoryData.fromMap(
          ModuleHelper.getDataFromSetting(widgetSetting['categoryId']))
      : CategoryData(
          categoryImage: '',
          categoryName: '',
        );

  //dynamic Data
  bool useBackground = widgetSetting.containsKey('useBackground')
      ? widgetSetting['useBackground'] == "true"
      : false;

  bool useFullWidth = widgetSetting.containsKey('useFullWidth')
      ? widgetSetting['useFullWidth'] == "true"
      : false;

  // String pathImageBg = widgetSetting.containsKey('pathImageBg')
  //     ? widgetSetting['pathImageBg']
  //     : '';

  // [092326HG]
  // Get data from DataSetting list
  String imageId = widgetSetting.containsKey('imageId')
      ? ModuleHelper.getDataFromSetting(widgetSetting['imageId'], 'image') ?? ""
      : "";
  // : GlobalSetting.noImage;

  Color color = widgetSetting.containsKey('color')
      ? Color(int.parse(widgetSetting['color']))
      : const Color(0xFFFFFFFF);

  Color colorBackground = widgetSetting.containsKey('colorBackground')
      ? Color(int.parse(widgetSetting['colorBackground']))
      : const Color(0x00FFFFFF);

  return categoryTitle(
    catName: categoryData.categoryName,
    useBackgroundCategoryTitle: useBackground,
    useFullWidthCategoryTitle: useFullWidth,
    pathImageBgCategoryTitle: imageId,
    colorCategoryTitle: color,
    colorBackgroundCategoryTitle: colorBackground,
  );
}
