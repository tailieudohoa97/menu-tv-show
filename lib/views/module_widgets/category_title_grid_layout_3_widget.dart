import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/category_title_grid_layout_3.dart';

Widget categoryTitleGridLayout3Widget(Map<String, dynamic> widgetSetting) {
  Map<String, dynamic> dataList =
      widgetSetting.containsKey('dataList') ? widgetSetting['dataList'] : {};
  dynamic categoryData = dataList.containsKey('category')
      ? CategoryData.fromMap(
          ModuleHelper.getDataFromSetting(dataList['category']))
      : CategoryData(categoryImage: "", categoryName: "");

  List<String> products = dataList.containsKey('product')
      ? (dataList['product'] as List).map((item) => item as String).toList()
      : [];

  List<ProductData> productList = products
      .map((item) => ProductData.fromMap(ModuleHelper.getDataFromSetting(item)))
      .toList();

  bool useBackgroundCategoryTitle =
      widgetSetting.containsKey('useBackgroundCategoryTitle')
          ? widgetSetting['useBackgroundCategoryTitle'] == "true"
          : false;

  bool useFullWidthCategoryTitle =
      widgetSetting.containsKey('useFullWidthCategoryTitle')
          ? widgetSetting['useFullWidthCategoryTitle'] == "true"
          : false;

  String pathImageBgCategoryTitle =
      widgetSetting.containsKey('pathImageBgCategoryTitle')
          ? widgetSetting['pathImageBgCategoryTitle']
          : '';

  // [092328HG]
  //Get data from DataSetting list
  String imageId = widgetSetting.containsKey('imageId')
      ? ModuleHelper.getDataFromSetting(widgetSetting['imageId'], 'image') ?? ""
      : "";

  Color colorCategoryTitle = widgetSetting.containsKey('colorCategoryTitle')
      ? Color(int.parse(widgetSetting['colorCategoryTitle']))
      : const Color(0xFFFFFFFF);

  Color colorPrice = widgetSetting.containsKey('colorPrice')
      ? Color(int.parse(widgetSetting['colorPrice']))
      : const Color(0xFFFFFFFF);

  Color colorProductName = widgetSetting.containsKey('colorProductName')
      ? Color(int.parse(widgetSetting['colorProductName']))
      : const Color(0xFFFFFFFF);

  Color colorDescription = widgetSetting.containsKey('colorDescription')
      ? Color(int.parse(widgetSetting['colorDescription']))
      : const Color(0xFFFFFFFF);

  Color colorBackgroundCategoryTitle =
      widgetSetting.containsKey('colorBackgroundCategoryTitle')
          ? Color(int.parse(widgetSetting['colorBackgroundCategoryTitle']))
          : const Color(0xFFFFFFFF);

  return categoryTitleGridLayout3(
    categoryData: categoryData,
    productList: productList,
    useFullWidthCategoryTitle: useFullWidthCategoryTitle,
    useBackgroundCategoryTitle: useBackgroundCategoryTitle,
    pathImageBgCategoryTitle: imageId, // [092328HG]
    colorBackgroundCategoryTitle: colorBackgroundCategoryTitle,
    colorCategoryTitle: colorCategoryTitle,
    colorPrice: colorPrice,
    colorProductName: colorProductName,
    colorDescription: colorDescription,
  );
}
