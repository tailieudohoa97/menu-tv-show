import 'package:flutter/material.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';

/// Padding widget with padding got from GlobalSetting.ratioStyle
Widget paddingByRatioWidget(Map<String, dynamic> widgetSetting) {
  Widget? child = widgetSetting.containsKey('child')
      ? ModuleHelper.renderWidget(widgetSetting['child'])
      : null;

  String paddingFieldKey = widgetSetting.containsKey('paddingFieldKey')
      ? widgetSetting['paddingFieldKey'].toString()
      : 'padding';

  String topPaddingFieldKey = widgetSetting.containsKey('topPaddingFieldKey')
      ? widgetSetting['topPaddingFieldKey'].toString()
      : 'topPadding';

  String bottomPaddingFieldKey =
      widgetSetting.containsKey('bottomPaddingFieldKey')
          ? widgetSetting['bottomPaddingFieldKey'].toString()
          : 'bottomPadding';

  String rightPaddingFieldKey =
      widgetSetting.containsKey('rightPaddingFieldKey')
          ? widgetSetting['rightPaddingFieldKey'].toString()
          : 'rightPadding';

  String leftPaddingFieldKey = widgetSetting.containsKey('leftPaddingFieldKey')
      ? widgetSetting['leftPaddingFieldKey'].toString()
      : 'leftPadding';

  String verticalPaddingFieldKey =
      widgetSetting.containsKey('verticalPaddingFieldKey')
          ? widgetSetting['verticalPaddingFieldKey'].toString()
          : 'verticalPadding';

  String horizontalPaddingFieldKey =
      widgetSetting.containsKey('horizontalPaddingFieldKey')
          ? widgetSetting['horizontalPaddingFieldKey'].toString()
          : 'horizontalPadding';

  double paddingAll = GlobalSetting.ratioStyle.getField(paddingFieldKey);
  double topPadding = GlobalSetting.ratioStyle.getField(topPaddingFieldKey);
  double bottomPadding =
      GlobalSetting.ratioStyle.getField(bottomPaddingFieldKey);
  double rightPadding = GlobalSetting.ratioStyle.getField(rightPaddingFieldKey);
  double leftPadding = GlobalSetting.ratioStyle.getField(leftPaddingFieldKey);
  double verticalPadding =
      GlobalSetting.ratioStyle.getField(verticalPaddingFieldKey);
  double horizontalPadding =
      GlobalSetting.ratioStyle.getField(horizontalPaddingFieldKey);

  EdgeInsetsGeometry padding = EdgeInsets.fromLTRB(
    leftPadding != 0
        ? leftPadding
        : horizontalPadding != 0
            ? horizontalPadding
            : paddingAll,
    topPadding != 0
        ? topPadding
        : verticalPadding != 0
            ? verticalPadding
            : paddingAll,
    rightPadding != 0
        ? rightPadding
        : horizontalPadding != 0
            ? horizontalPadding
            : paddingAll,
    bottomPadding != 0
        ? bottomPadding
        : verticalPadding != 0
            ? verticalPadding
            : paddingAll,
  );

  return Padding(
    padding: padding,
    child: child,
  );
}
