import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';

Widget simpleImageWidget(Map<String, dynamic> widgetSetting) {
  String? imageFixedPath = widgetSetting.containsKey('imagePath')
      ? widgetSetting['imagePath'].toString()
      : null;
  String? imageDataKey = widgetSetting.containsKey('imageDataKey')
      ? widgetSetting['imageDataKey'].toString()
      : null;

  if (imageFixedPath == null && imageDataKey == null) {
    throw ErrorDescription(
        'SimpleImageWidget required imagePath or imageDataKey');
  }

  String imageRealPath = GlobalSetting.noImage;

  if (imageDataKey != null) {
    imageRealPath = ModuleHelper.getDataFromSetting(imageDataKey, 'image') ??
        GlobalSetting.noImage;
  }

  if (imageFixedPath != null) {
    imageRealPath = imageFixedPath;
  }

  ImageProvider image = ModuleHelper.getImageProvider(imageRealPath);

  double? heightFromRatioSetting = widgetSetting
          .containsKey('heightFromFieldKey')
      ? GlobalSetting.ratioStyle.getField(widgetSetting['heightFromFieldKey'])
      : null;

  double? widthFromRatioSetting = widgetSetting.containsKey('widthFromFieldKey')
      ? GlobalSetting.ratioStyle.getField(widgetSetting['widthFromFieldKey'])
      : null;

  double? height = widgetSetting.containsKey('height')
      ? SizingValue.fromString(widgetSetting['height'])
      : null;

  double? widht = widgetSetting.containsKey('widht')
      ? SizingValue.fromString(widgetSetting['widht'])
      : null;

  BoxFit? fit = widgetSetting.containsKey('fit')
      ? ModuleHelper.boxFitMap[widgetSetting['fit']]
      : null;

  return Image(
    image: image,
    height: height ?? heightFromRatioSetting,
    width: widht ?? widthFromRatioSetting,
    fit: fit,
  );
}
