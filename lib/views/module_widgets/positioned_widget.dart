import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/module_helper.dart';

Widget positionedWidget(dynamic widgetSetting) {
  return Positioned(
    top: widgetSetting.containsKey('top')
        ? SizingValue.fromString(widgetSetting['top'])
        : null,
    right: widgetSetting.containsKey('right')
        ? SizingValue.fromString(widgetSetting['right'])
        : null,
    bottom: widgetSetting.containsKey('bottom')
        ? SizingValue.fromString(widgetSetting['bottom'])
        : null,
    left: widgetSetting.containsKey('left')
        ? SizingValue.fromString(widgetSetting['left'])
        : null,
    child: widgetSetting.containsKey('child')
        ? ModuleHelper.renderWidget(widgetSetting['child'])
        : const Placeholder(),
  );
}
