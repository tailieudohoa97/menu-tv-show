import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/service_item_column.dart';

Widget serviceItemColumnWidget(Map<String, dynamic> widgetSetting) {
  ProductData productData = widgetSetting.containsKey('productId')
      ? ProductData.fromMap(
          ModuleHelper.getDataFromSetting(widgetSetting['productId']))
      : ProductData(
          productName: '',
          productDescription: '',
          price: '',
        );

  bool toUpperCaseNameProductName =
      widgetSetting.containsKey('toUpperCaseNameProductName')
          ? widgetSetting['toUpperCaseNameProductName'] == "true"
          : false;

  bool useMediumStyle = widgetSetting.containsKey('useMediumStyle')
      ? widgetSetting['useMediumStyle'] == "true"
      : false;

  Color colorPrice = widgetSetting.containsKey('colorPrice')
      ? Color(int.parse(widgetSetting['colorPrice']))
      : const Color(0xFF000000);

  Color colorProductName = widgetSetting.containsKey('colorProductName') &&
          widgetSetting['colorProductName'] != ""
      ? Color(int.parse(widgetSetting['colorProductName']))
      : const Color(0xFF000000);

  int maxLineProductName = widgetSetting.containsKey('maxLineProductName')
      ? int.parse(widgetSetting['maxLineProductName'])
      : 1;

  double width = widgetSetting.containsKey("width")
      ? SizingValue.fromString(widgetSetting['width'])
      : 200;

  Color colorDescription = widgetSetting.containsKey('colorDescription') &&
          widgetSetting['colorDescription'] != ""
      ? Color(int.parse(widgetSetting['colorDescription']))
      : const Color(0xFF000000);

  return serviceItemColumn(
    name: productData.productName,
    toUpperCaseNameProductName: toUpperCaseNameProductName,
    price: productData.price,
    useMediumStyle: useMediumStyle,
    colorPrice: colorPrice,
    colorProductName: colorProductName,
    maxLineProductName: maxLineProductName,
    width: width,
    description: productData.productDescription,
    colorDescription: colorDescription,
  );
}
