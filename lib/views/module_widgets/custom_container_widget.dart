import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/views/widgets/edge_insets.dart';

Widget customContainerWidget(Map<String, dynamic> widgetSetting) {
  String imagePath = widgetSetting['image'] ?? '';
  return Container(
    decoration: BoxDecoration(
      image: DecorationImage(
        opacity: widgetSetting['opacity'] != null
            ? double.parse(widgetSetting['opacity'])
            : 1.0,
        image: imagePath.contains('http')
            ? NetworkImage(imagePath) as ImageProvider
            : AssetImage(imagePath != '' ? imagePath : GlobalSetting.noImage),
        fit: ModuleHelper.boxFitMap[widgetSetting['fit']] ?? BoxFit.cover,
        alignment:
            ModuleHelper.arrAlignmentGeometry[widgetSetting['alignment']] ??
                Alignment.center,
        colorFilter: ColorFilter.mode(
            widgetSetting.containsKey('color')
                ? (Color(int.parse(widgetSetting['color'])))
                : Colors.black,
            ModuleHelper.blendModeMap[
                    widgetSetting['blendMode'] ?? BlendMode.color] ??
                BlendMode.color),
      ),
    ),
    width: widgetSetting.containsKey("width")
        ? SizingValue.fromString(widgetSetting['width'])
        : null,
    height: widgetSetting.containsKey("height")
        ? SizingValue.fromString(widgetSetting['height'])
        : null,
    padding: widgetSetting.containsKey('padding')
        ? EdgeInsetsData.fromJson(widgetSetting['padding']).toEdgeInsets()
        : null,
    margin: widgetSetting.containsKey('margin')
        ? EdgeInsetsData.fromJson(widgetSetting['margin']).toEdgeInsets()
        : null,
    child: widgetSetting.containsKey('child')
        ? ModuleHelper.renderWidget(widgetSetting['child'])
        : null,
  );
}
