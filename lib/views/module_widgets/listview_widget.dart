import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/views/module_widgets/box_decoration.dart';
import 'package:tv_menu/views/widgets/edge_insets.dart';

Widget listViewWidget(Map<String, dynamic> widgetSetting) {
  List<dynamic> childrens = widgetSetting['children'] ?? [];

  bool reverse = widgetSetting.containsKey('reverse')
      ? widgetSetting['reverse'] == "true"
      : true;
  bool shrinkWrap = widgetSetting.containsKey('shrinkWrap')
      ? widgetSetting['shrinkWrap'] == "true"
      : true;

  return ListView(
    reverse: reverse,
    shrinkWrap: shrinkWrap,
    padding: widgetSetting.containsKey('padding')
        ? EdgeInsetsData.fromJson(widgetSetting['padding']).toEdgeInsets()
        : null,
    children: childrens.map((e) => ModuleHelper.renderWidget(e)).toList(),
  );
}
