import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/views/widgets/underline_brand.dart';

// [092327Tin] Add fontSizeFromFieldKey
Widget underlineBrandWidget(Map<String, dynamic> widgetSetting) {
  if (!widgetSetting.containsKey('brand')) {
    throw ErrorDescription("Require brand field for UnderlineBrand widget");
  }

  final String brand = widgetSetting.containsKey('brand')
      ? widgetSetting['brand'].toString()
      : '';
  double? fontSizeFromRatioSetting = widgetSetting
          .containsKey('fontSizeFromFieldKey')
      ? GlobalSetting.ratioStyle.getField(widgetSetting['fontSizeFromFieldKey'])
      : null;

  TextStyle textStyle = widgetSetting.containsKey('textStyle')
      ? ModuleHelper.getTextStyle(widgetSetting['textStyle'])
      : TextStyle();

  if (textStyle.fontSize == null) {
    textStyle = textStyle.copyWith(fontSize: fontSizeFromRatioSetting);
  }

  final BorderSide? bottomBorderSide =
      widgetSetting.containsKey('bottomBorderSide')
          ? ModuleHelper.getBorderSide(widgetSetting['bottomBorderSide'])
          : null;

  return UnderlineBrand(
    brand: brand,
    bottomBorderSide: bottomBorderSide,
    textStyle: textStyle,
  );
}
