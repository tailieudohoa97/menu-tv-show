import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/product_row_2.dart';

Widget productRow2Widget(Map<String, dynamic> widgetSetting) {
  if (!widgetSetting.containsKey('product')) {
    throw ErrorDescription("Require product field for ProductRow2 widget");
  }

  final ProductData product = ProductData.fromMap(
      ModuleHelper.getDataFromSetting(widgetSetting['product']));

  final int? fractionDigitsForPrice =
      widgetSetting.containsKey('fractionDigitsForPrice')
          ? int.parse(widgetSetting['fractionDigitsForPrice'])
          : null;

  final int? maxLineOfProductName =
      widgetSetting.containsKey('maxLineOfProductName')
          ? int.parse(widgetSetting['maxLineOfProductName'])
          : null;

  final String? prefixProductPrice =
      widgetSetting.containsKey('prefixProductPrice')
          ? widgetSetting['prefixProductPrice'].toString()
          : null;

  final String? subfixProductPrice =
      widgetSetting.containsKey('subfixProductPrice')
          ? widgetSetting['subfixProductPrice'].toString()
          : null;

  final bool upperCaseProductName =
      widgetSetting['upperCaseProductName'] == 'true';

  double? productNameFontSizeFromRatioSetting =
      widgetSetting.containsKey('productNameFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['productNameFontSizeFromFieldKey'])
          : null;

  TextStyle productNameTextStyle =
      widgetSetting.containsKey('productNameTextStyle')
          ? ModuleHelper.getTextStyle(widgetSetting['productNameTextStyle'])
          : TextStyle();

  if (productNameTextStyle.fontSize == null) {
    productNameTextStyle = productNameTextStyle.copyWith(
      fontSize: productNameFontSizeFromRatioSetting,
    );
  }

  double? productPriceFontSizeFromRatioSetting =
      widgetSetting.containsKey('productPriceFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['productPriceFontSizeFromFieldKey'])
          : null;

  TextStyle productPriceTextStyle =
      widgetSetting.containsKey('productPriceTextStyle')
          ? ModuleHelper.getTextStyle(widgetSetting['productPriceTextStyle'])
          : TextStyle();

  if (productPriceTextStyle.fontSize == null) {
    productPriceTextStyle = productPriceTextStyle.copyWith(
      fontSize: productPriceFontSizeFromRatioSetting,
    );
  }

  double? productDesFontSizeFromRatioSetting =
      widgetSetting.containsKey('productDesFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['productDesFontSizeFromFieldKey'])
          : null;

  TextStyle productDescriptionTextStyle = widgetSetting
          .containsKey('productDescriptionTextStyle')
      ? ModuleHelper.getTextStyle(widgetSetting['productDescriptionTextStyle'])
      : TextStyle();

  if (productDescriptionTextStyle.fontSize == null) {
    productDescriptionTextStyle = productDescriptionTextStyle.copyWith(
      fontSize: productDesFontSizeFromRatioSetting,
    );
  }

  final Widget? divider = widgetSetting.containsKey('divider')
      ? ModuleHelper.renderWidget(widgetSetting['divider'])
      : null;

  final bool useSubname = widgetSetting.containsKey('useSubname') &&
      widgetSetting['useSubname'] == "true";

  final bool upperCaseProductSubName =
      widgetSetting.containsKey('upperCaseProductSubName') &&
          widgetSetting['upperCaseProductSubName'] == "true";

  final int? maxLineOfProductSubName =
      widgetSetting.containsKey('maxLineOfProductSubName')
          ? int.parse(widgetSetting['maxLineOfProductSubName'])
          : null;
  TextStyle productSubNameTextStyle =
      widgetSetting.containsKey('productSubNameTextStyle')
          ? ModuleHelper.getTextStyle(widgetSetting['productSubNameTextStyle'])
          : TextStyle();

  return ProductRow2(
    product: product,
    fractionDigitsForPrice: fractionDigitsForPrice,
    productNameTextStyle: productNameTextStyle,
    productPriceTextStyle: productPriceTextStyle,
    divider: divider,
    maxLineOfProductName: maxLineOfProductName,
    prefixProductPrice: prefixProductPrice,
    subfixProductPrice: subfixProductPrice,
    productDescriptionTextStyle: productDescriptionTextStyle,
    upperCaseProductName: upperCaseProductName,
    useSubname: useSubname,
    upperCaseProductSubName: upperCaseProductSubName,
    maxLineOfProductSubName: maxLineOfProductSubName,
    productSubNameTextStyle: productSubNameTextStyle,
  );
}
