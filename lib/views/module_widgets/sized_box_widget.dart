import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/helper/SizingValue.dart';

// [112314TIN] use SizingValue in helper folder
Widget sizedBoxWidget(Map<String, dynamic> widgetSetting) {
  return SizedBox(
    width: widgetSetting.containsKey("width")
        ? SizingValue.fromString(widgetSetting['width'])
        : null,
    height: widgetSetting.containsKey("height")
        ? SizingValue.fromString(widgetSetting['height'])
        : null,
    child: widgetSetting.containsKey('child')
        ? ModuleHelper.renderWidget(widgetSetting['child'])
        : null,
  );
}
