import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/views/widgets/framed_image.dart';

Widget framedImageWidget(Map<String, dynamic> widgetSetting) {
  if (!widgetSetting.containsKey('image') &&
      !widgetSetting.containsKey('imageDataKey')) {
    throw ErrorDescription(
        "Require image or imageDataKey field for FramedImage widget");
  }

  if (!widgetSetting.containsKey('imageFrame') &&
      !widgetSetting.containsKey('imageFrameDataKey')) {
    throw ErrorDescription("Require imageFrame field for FramedImage widget");
  }

  final ImageProvider image = widgetSetting.containsKey('image')
      ? ModuleHelper.getImageProvider(widgetSetting['image'].toString())
      : ModuleHelper.getImageProvider(
          ModuleHelper.getDataFromSetting(
                  widgetSetting['imageDataKey'], 'image')
              .toString(),
        );
  final ImageProvider imageFrame = widgetSetting.containsKey('imageFrame')
      ? ModuleHelper.getImageProvider(widgetSetting['imageFrame'].toString())
      : ModuleHelper.getImageProvider(
          ModuleHelper.getDataFromSetting(
                  widgetSetting['imageFrameDataKey'], 'image')
              .toString(),
        );
  final double borderRadiusOfImage =
      widgetSetting.containsKey('borderRadiusOfImage')
          ? SizingValue.fromString(widgetSetting['borderRadiusOfImage'])
          : 0;

  final double borderWidth = widgetSetting.containsKey('borderWidth')
      ? SizingValue.fromString(widgetSetting['borderWidth'])
      : 1;

  final double? height = widgetSetting.containsKey('height')
      ? SizingValue.fromString(widgetSetting['height'])
      : null;

  final double? width = widgetSetting.containsKey('width')
      ? SizingValue.fromString(widgetSetting['width'])
      : null;

  return FramedImage(
    image: image,
    imageFrame: imageFrame,
    borderRadiusOfImage: borderRadiusOfImage,
    borderWidth: borderWidth,
    height: height,
    width: width,
  );
}
