import 'package:flutter/material.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/module_helper.dart';

// [112309TIN]  create this module
Widget transformRotateWidget(Map<String, dynamic> widgetSetting) {
  final double angle = widgetSetting.containsKey('angle')
      ? SizingValue.fromString(widgetSetting['angle'])
      : 0;
  final alignment = widgetSetting.containsKey('alignment')
      ? ModuleHelper.arrAlignmentGeometry[
          widgetSetting.containsKey('alignment').toString()]
      : null;
  final origin = widgetSetting.containsKey('origin')
      ? ModuleHelper.getOffset(widgetSetting['origin'])
      : null;
  final child = widgetSetting.containsKey('child')
      ? ModuleHelper.renderWidget(widgetSetting['child'])
      : null;

  return Transform.rotate(
    angle: angle,
    alignment: alignment,
    origin: origin,
    child: child,
  );
}
