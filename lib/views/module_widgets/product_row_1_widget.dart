import 'package:flutter/widgets.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/edge_insets.dart';
import 'package:tv_menu/views/widgets/product_row_1.dart';

/// [112320TIN] add properties: productNameLAlignment, namePriceGap
/// [112309TIN] add properties: useSubname, upperCaseProductSubName, productSubNameTextStyle, maxLineOfProductSubName
// [092327Tin] Add field productNameFontSizeFromFieldKey, productPriceFontSizeFromFieldKey
Widget productRow1Widget(Map<String, dynamic> widgetSetting) {
  if (!widgetSetting.containsKey('product')) {
    throw ErrorDescription("Require product field for ProductRow1 widget");
  }

  final ProductData product = ProductData.fromMap(
      ModuleHelper.getDataFromSetting(widgetSetting['product']));

  final Border? border = widgetSetting.containsKey('border')
      ? ModuleHelper.getBorder(widgetSetting['border'])
      : null;

  final int? fractionDigitsForPrice =
      widgetSetting.containsKey('fractionDigitsForPrice')
          ? int.parse(widgetSetting['fractionDigitsForPrice'])
          : null;

  final int? maxLineOfProductName =
      widgetSetting.containsKey('maxLineOfProductName')
          ? int.parse(widgetSetting['maxLineOfProductName'])
          : null;

  final String? prefixProductPrice =
      widgetSetting.containsKey('prefixProductPrice')
          ? widgetSetting['prefixProductPrice'].toString()
          : null;

  final String? subfixProductPrice =
      widgetSetting.containsKey('subfixProductPrice')
          ? widgetSetting['subfixProductPrice'].toString()
          : null;

  final bool upperCaseProductName =
      widgetSetting['upperCaseProductName'] == 'true';

  double? productNameFontSizeFromRatioSetting =
      widgetSetting.containsKey('productNameFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['productNameFontSizeFromFieldKey'])
          : null;

  TextStyle productNameTextStyle =
      widgetSetting.containsKey('productNameTextStyle')
          ? ModuleHelper.getTextStyle(widgetSetting['productNameTextStyle'])
          : TextStyle();

  if (productNameTextStyle.fontSize == null) {
    productNameTextStyle = productNameTextStyle.copyWith(
      fontSize: productNameFontSizeFromRatioSetting,
    );
  }

  double? productPriceFontSizeFromRatioSetting =
      widgetSetting.containsKey('productPriceFontSizeFromFieldKey')
          ? GlobalSetting.ratioStyle
              .getField(widgetSetting['productPriceFontSizeFromFieldKey'])
          : null;

  TextStyle productPriceTextStyle =
      widgetSetting.containsKey('productPriceTextStyle')
          ? ModuleHelper.getTextStyle(widgetSetting['productPriceTextStyle'])
          : TextStyle();

  if (productPriceTextStyle.fontSize == null) {
    productPriceTextStyle = productPriceTextStyle.copyWith(
      fontSize: productPriceFontSizeFromRatioSetting,
    );
  }

  final EdgeInsets? padding = widgetSetting.containsKey('padding')
      ? EdgeInsetsData.fromJson(widgetSetting['padding']).toEdgeInsets()
      : null;

  final bool useSubname = widgetSetting.containsKey('useSubname') &&
      widgetSetting['useSubname'] == "true";

  final bool upperCaseProductSubName =
      widgetSetting.containsKey('upperCaseProductSubName') &&
          widgetSetting['upperCaseProductSubName'] == "true";

  final int? maxLineOfProductSubName =
      widgetSetting.containsKey('maxLineOfProductSubName')
          ? int.parse(widgetSetting['maxLineOfProductSubName'])
          : null;
  TextStyle productSubNameTextStyle =
      widgetSetting.containsKey('productSubNameTextStyle')
          ? ModuleHelper.getTextStyle(widgetSetting['productSubNameTextStyle'])
          : TextStyle();

  /// [112320TIN] add productNameLAlignment property
  final TextAlign? productNameLAlignment =
      widgetSetting.containsKey('productNameLAlignment')
          ? ModuleHelper.arrTextAlign[widgetSetting['productNameLAlignment']]
          : null;

  /// [112320TIN] add namePriceGap property
  final namePriceGap = widgetSetting.containsKey('namePriceGap')
      ? SizingValue.fromString(widgetSetting['namePriceGap'])
      : null;

  return ProductRow1(
    product: product,
    border: border,
    fractionDigitsForPrice: fractionDigitsForPrice,
    maxLineOfProductName: maxLineOfProductName,
    padding: padding,
    prefixProductPrice: prefixProductPrice,
    subfixProductPrice: subfixProductPrice,
    upperCaseProductName: upperCaseProductName,
    productNameTextStyle: productNameTextStyle,
    productPriceTextStyle: productPriceTextStyle,
    useSubname: useSubname,
    upperCaseProductSubName: upperCaseProductSubName,
    maxLineOfProductSubName: maxLineOfProductSubName,
    productSubNameTextStyle: productSubNameTextStyle,
    productNameLAlignment: productNameLAlignment,
    namePriceGap: namePriceGap,
  );
}
