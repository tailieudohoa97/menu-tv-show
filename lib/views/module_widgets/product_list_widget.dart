import 'package:flutter/material.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/product_item.dart';

Widget productListWidget(Map<String, dynamic> widgetSetting) {
  List<String> dataList = widgetSetting.containsKey('dataList')
      ? (widgetSetting['dataList'] as List)
          .map((item) => item as String)
          .toList()
      : [];
  List<ProductData> productList = dataList
      .map((item) => ProductData.fromMap(ModuleHelper.getDataFromSetting(item)))
      .toList();

  bool toUpperCaseNameProductName =
      widgetSetting.containsKey('toUpperCaseNameProductName')
          ? widgetSetting['toUpperCaseNameProductName'] == "true"
          : true;

  bool useMediumStyle = widgetSetting.containsKey('useMediumStyle')
      ? widgetSetting['useMediumStyle'] == "true"
      : false;

  Color colorPrice = widgetSetting.containsKey('colorPrice')
      ? Color(int.parse(widgetSetting['colorPrice']))
      : const Color(0xFFFFFFFF);

  Color colorProductName = widgetSetting.containsKey('colorProductName') &&
          widgetSetting['colorProductName'] != ""
      ? Color(int.parse(widgetSetting['colorProductName']))
      : const Color(0xFFFFFFFF);

  Color colorDescription = widgetSetting.containsKey('colorDescription') &&
          widgetSetting['colorDescription'] != ""
      ? Color(int.parse(widgetSetting['colorDescription']))
      : const Color(0xFFFFFFFF);

  bool useThumnailProduct = widgetSetting.containsKey('useThumnailProduct')
      ? widgetSetting['useThumnailProduct'] == "true"
      : false;

  String pathThumnailProduct =
      widgetSetting.containsKey('pathThumnailProduct') &&
              widgetSetting['pathThumnailProduct'] != ""
          ? widgetSetting['pathThumnailProduct']
          : GlobalSetting.noImage;

  int maxLineProductName = widgetSetting.containsKey('maxLineProductName')
      ? int.parse(widgetSetting['maxLineProductName'])
      : 1;

  int maxLineProductDescription =
      widgetSetting.containsKey('maxLineProductDescription')
          ? int.parse(widgetSetting['maxLineProductDescription'])
          : 2;

  //[092313HG] add mainAxisAlignment
  MainAxisAlignment mainAxisAlignment =
      widgetSetting.containsKey('mainAxisAlignment')
          ? (ModuleHelper
                  .arrMainAxisAlignment[widgetSetting['mainAxisAlignment']] ??
              MainAxisAlignment.start)
          : MainAxisAlignment.start;

  //[092313HG] add crossAxisAlignment
  CrossAxisAlignment crossAxisAlignment =
      widgetSetting.containsKey('crossAxisAlignment')
          ? (ModuleHelper
                  .arrCrossAxisAlignment[widgetSetting['crossAxisAlignment']] ??
              CrossAxisAlignment.center)
          : CrossAxisAlignment.center;

  return Column(
    crossAxisAlignment: crossAxisAlignment,
    mainAxisAlignment: mainAxisAlignment,
    children: productList
        .map((product) => productItem(
              name: product.productName,
              toUpperCaseNameProductName: toUpperCaseNameProductName,
              price: product.price,
              description: product.productDescription,
              useMediumStyle: useMediumStyle,
              colorPrice: colorPrice,
              colorProductName: colorProductName,
              colorDescription: colorDescription,
              useThumnailProduct: useThumnailProduct,
              pathThumnailProduct: pathThumnailProduct,
              maxLineProductName: maxLineProductName,
              maxLineProductDescription: maxLineProductDescription,
            ))
        .toList(),
  );
}
