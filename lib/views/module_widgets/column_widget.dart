import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';

Widget columnWidget(Map<dynamic, dynamic> widgetSetting) {
  List<dynamic> childrens = widgetSetting['children'] ?? [];

  //[092313HG] add mainAxisAlignment
  MainAxisAlignment mainAxisAlignment =
      widgetSetting.containsKey('mainAxisAlignment')
          ? (ModuleHelper
                  .arrMainAxisAlignment[widgetSetting['mainAxisAlignment']] ??
              MainAxisAlignment.start)
          : MainAxisAlignment.start;

  //[092313HG] add crossAxisAlignment
  CrossAxisAlignment crossAxisAlignment =
      widgetSetting.containsKey('crossAxisAlignment')
          ? (ModuleHelper
                  .arrCrossAxisAlignment[widgetSetting['crossAxisAlignment']] ??
              CrossAxisAlignment.center)
          : CrossAxisAlignment.center;

  return Column(
    crossAxisAlignment: crossAxisAlignment,
    mainAxisAlignment: mainAxisAlignment,
    children: childrens.map((e) => ModuleHelper.renderWidget(e)).toList(),
  );
}
