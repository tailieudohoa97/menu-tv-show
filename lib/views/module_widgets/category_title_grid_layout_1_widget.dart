import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';
import 'package:tv_menu/models/model_datas/category_data.dart';
import 'package:tv_menu/models/model_datas/product_data.dart';
import 'package:tv_menu/views/widgets/category_title_grid_layout_1.dart';
// import 'package:tv_menu/helper/module_helper.dart';

Widget categoryTitleGridLayout1Widget(
  Map<String, dynamic> widgetSetting,
) {
  Map<String, dynamic> dataList =
      widgetSetting.containsKey('dataList') ? widgetSetting['dataList'] : {};
  //092314LH Fix dynamic sample data to template
  dynamic categoryData = dataList.containsKey('category')
      ? CategoryData.fromMap(
          ModuleHelper.getDataFromSetting(dataList['category']))
      : CategoryData(categoryImage: "", categoryName: "");

  List<String> products = dataList.containsKey('product')
      ? (dataList['product'] as List).map((item) => item as String).toList()
      : [];

  List<ProductData> productList = products
      .map((item) => ProductData.fromMap(ModuleHelper.getDataFromSetting(item)))
      .toList();

  //List image sẽ lấy trong "dataSetting" của Json
  List<String> imageList = dataList.containsKey('image')
      ? (dataList['image'] as List).map((item) => item as String).toList()
      : [];
  imageList = imageList
      .map((img) => ModuleHelper.getDataFromSetting(img, "image") as String)
      .toList();

  bool useBackgroundCategoryTitle =
      widgetSetting.containsKey('useBackgroundCategoryTitle')
          ? widgetSetting['useBackgroundCategoryTitle'] == "true"
          : false;

  //[102302HG] add this code
  bool fixCover = widgetSetting.containsKey('fixCover')
      ? widgetSetting['fixCover'] == "true"
      : true;

  String pathImageBgCategoryTitle =
      widgetSetting.containsKey('pathImageBgCategoryTitle')
          ? widgetSetting['pathImageBgCategoryTitle']
          : '';

  Color colorCategoryTitle = widgetSetting.containsKey('colorCategoryTitle')
      ? Color(int.parse(widgetSetting['colorCategoryTitle']))
      : const Color(0xFFFFFFFF);

  Color colorPrice = widgetSetting.containsKey('colorPrice')
      ? Color(int.parse(widgetSetting['colorPrice']))
      : const Color(0xFFFFFFFF);

  Color colorProductName = widgetSetting.containsKey('colorProductName')
      ? Color(int.parse(widgetSetting['colorProductName']))
      : const Color(0xFFFFFFFF);

  Color colorDescription = widgetSetting.containsKey('colorDescription')
      ? Color(int.parse(widgetSetting['colorDescription']))
      : const Color(0xFFFFFFFF);

  return categoryTitleGridLayout1(
    //092314LH Fix dynamic sample data to template
    imageList: imageList,
    fixCover: fixCover, //[102302HG] add this code
    categoryData: categoryData,
    productList: productList,
    useBackgroundCategoryTitle: useBackgroundCategoryTitle,
    pathImageBgCategoryTitle: pathImageBgCategoryTitle,
    colorCategoryTitle: colorCategoryTitle,
    colorPrice: colorPrice,
    colorProductName: colorProductName,
    colorDescription: colorDescription,
  );
}
