import 'package:flutter/material.dart';
import 'package:tv_menu/helper/module_helper.dart';

Widget centerWidget(dynamic widgetSetting) {
  return Center(
    child: widgetSetting.containsKey('child')
        ? ModuleHelper.renderWidget(widgetSetting['child'])
        : const Placeholder(),
  );
}
