import 'dart:convert';

import 'package:flutter/material.dart';

import '../../helper/app_setting.dart';
import '../../helper/helper.dart';

import '../../routes/route_names.dart';
import '../../widgets/card_wth_options.dart';
import '../../widgets/version/version.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

class WelcomePage extends StatefulWidget {
  WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    // Helpers.debugLog(
    //     "test > calculCoupon", Helpers.calculCoupon(COUPON.YWSU, "4168220069"));
    return Material(
        child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // [032321HG] Leave the logo temporarily
          // Image(
          //   image: AssetImage(AppSetting.ywLogo),
          // ),

          const SizedBox(height: 65),
          Text(AppSetting.welcomeMsg,
              style: const TextStyle(
                  fontSize: 45, fontWeight: FontWeight.bold, letterSpacing: 1)),
          const SizedBox(height: 50),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _cardCTA(
                label: 'Template',
                icon: AppSetting.profileIcon,
                onPressed: () {
                  Navigator.pushNamed(context, Routes.HomeScreenRoute);
                },
              ),
              const SizedBox(
                width: 45,
              ),
              _cardCTA(
                label: 'Admin',
                icon: AppSetting.addNewIcon,
                onPressed: () {
                  Navigator.pushNamed(context, Routes.AdminLoginRoute);
                },
              )
            ],
          ),
          version(context)
        ],
      ),
    ));
  }

  // Widget _recentWorker() {
  Widget _cardCTA({
    IconData? icon,
    String? label,
    Function()? onPressed,
  }) {
    return cardCTA(
        icon: Center(
            child: Icon(
          icon,
          size: 75,
          color: Colors.white,
        )),
        midchild: Text(AppSetting.loginMsgOnCard,
            style: const TextStyle(fontSize: 12)),
        child: Text(label ?? "", style: AppSetting.lgWhiteTxt),
        onPressed: onPressed,
        height: 250,
        width: 250);
  }
}
