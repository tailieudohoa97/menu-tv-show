// import 'dart:ffi';

//[072311HG]
//Global Grid/List Switch Button

import 'package:tv_menu/helper/helper.dart';

class ToggleGridListButton {
  bool _isGrid = true; //Biến kiểm tra trạng thái

  //Function GET status
  bool get getStatus => _isGrid;
  // bool getStatus () => _isGrid; //Kieu 2

  //Function to set the input Status to new value
  //Đặt giá trị mới
  set setStatus(bool value) {
    _isGrid = value;
  }

  // Function quick switch set status
  void switchStatus() {
    _isGrid = !_isGrid;
    Helpers.debugLog("ToggleGridListButton => Switch status", _isGrid);
  }
}
