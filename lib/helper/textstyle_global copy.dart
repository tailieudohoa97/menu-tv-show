import 'package:flutter/material.dart';

class TextStyleGlobal {
  // static const TextStyle nameCategory = TextStyle(...)
  //Đây là một thuộc tính tĩnh (static) của lớp TextStyleGlobal.
  //Thuộc tính này được gọi là nameCategory và được khởi tạo với một đối tượng TextStyle.
  //Điều này cho phép bạn tái sử dụng các kiểu văn bản nhất định trong toàn ứng dụng.
  // TextStyle là tên của một lớp trong Flutter được sử dụng để định nghĩa kiểu văn bản.
  //Bạn không thể thay thế tên của lớp TextStyle bằng một chuỗi khác vì nó cần phải tương thích với cấu trúc và định nghĩa của lớp TextStyle.

  static const TextStyle nameCategory = TextStyle(
    fontSize: 22,
    fontWeight: FontWeight.bold,
    color: Colors.white,
  );

  static const TextStyle mediumNameProduct = TextStyle(
    fontSize: 22,
    fontWeight: FontWeight.w800,
    color: Colors.white,
  );

  static const TextStyle smallNameProduct = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w800,
    color: Colors.white,
  );

  static const TextStyle mediumDiscriptionProduct = TextStyle(
    fontSize: 26,
    fontWeight: FontWeight.w300,
    color: Colors.white,
  );

  static const TextStyle smallDiscriptionProduct = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w300,
    color: Colors.white,
  );
}
