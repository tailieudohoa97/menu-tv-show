import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class CalculateRatio {
  final BuildContext context;
  late double _categoryNameStyle;
  late double _nameProductStyle;
  late double _discriptionProductStyle;
  late double _priceProductStyle;

  late double _calculatedimageHeight;
  late EdgeInsets _calculatedMarginImage;
  late EdgeInsets _calculatedMarginItem;

  //Tạo các phương thức để trả về các TextStyle cho các loại văn bản khác nhau
  TextStyle get categoryNameStyle => TextStyle(
        fontSize: _categoryNameStyle,
        fontWeight: FontWeight.bold,
        color: Colors.white,
      );

  TextStyle get nameProductStyle => TextStyle(
        fontSize: _nameProductStyle,
        fontWeight: FontWeight.w800,
        color: Colors.white,
      );

  TextStyle get discriptionProductStyle => TextStyle(
        fontSize: _discriptionProductStyle,
        fontWeight: FontWeight.w300,
        color: Colors.white,
      );

  TextStyle get priceProductStyle => TextStyle(
        fontSize: _priceProductStyle,
        fontWeight: FontWeight.w800,
        color: Color(0xFFecc451),
      );

  double get calculatedimageHeight => _calculatedimageHeight;
  EdgeInsets get calculatedMarginImage => _calculatedMarginImage;
  EdgeInsets get calculatedMarginItem => _calculatedMarginItem;

  CalculateRatio(this.context) {
    _calculateRatio();
  }

  //Tạo hàm để tính toán chiều cao của Hình theo tỉ lệ màn hình
  double _getImageHeight(double width) {
    return width / 3; //Chia theo 3 cột của màn hình
  }

  //Tạo một hàm để tính toán font size theo tỉ lệ màn hình
  void _calculateRatio() {
    //Lấy chiều rộng và chiều cao của màn hình
    double widthDevice = MediaQuery.of(context).size.width;
    double heighDevice = MediaQuery.of(context).size.height;

    //Tính tỉ lệ màn hình bằng cách chia chiều rộng cho chiều cao
    double ratio = widthDevice / heighDevice;

    //Sử dụng switch case để kiểm tra các trường hợp khác nhau của tỷ lệ màn hình
    switch (ratio) {
      case const (32 / 9): //Nếu tỷ lệ màn hình là 32:9
        print("Tỷ lệ màn hình: 32:9");
        _categoryNameStyle = 1.5.w;
        _nameProductStyle = 1.5.w;
        _priceProductStyle = 1.5.w;
        _discriptionProductStyle = 1.w;
        _calculatedimageHeight = _getImageHeight(widthDevice / 4);
        _calculatedMarginImage = EdgeInsets.symmetric(vertical: 1.h);
        _calculatedMarginItem = EdgeInsets.only(bottom: 2.h);
        break;

      case const (21 / 9): //Nếu tỷ lệ màn hình là 21:9
        print("Tỷ lệ màn hình: 21:9");
        _categoryNameStyle = 1.5.w;
        _nameProductStyle = 1.5.w;
        _priceProductStyle = 1.5.w;
        _discriptionProductStyle = 1.w;
        _calculatedimageHeight = _getImageHeight(widthDevice / 3);
        _calculatedMarginImage = EdgeInsets.symmetric(vertical: 1.h);
        _calculatedMarginItem = EdgeInsets.only(bottom: 0.5.h);
        break;

      case const (16 / 9): //Nếu tỷ lệ màn hình là 16:9
        print("Tỷ lệ màn hình: 16:9");
        _categoryNameStyle = 1.5.w;
        _nameProductStyle = 1.5.w;
        _priceProductStyle = 1.5.w;
        _discriptionProductStyle = 1.w;
        _calculatedimageHeight = _getImageHeight(widthDevice / 2);
        _calculatedMarginImage = EdgeInsets.symmetric(vertical: 2.h);
        _calculatedMarginItem = EdgeInsets.only(bottom: 2.h);
        break;

      case const (16 / 10): //Nếu tỷ lệ màn hình là 16:10
        print("Tỷ lệ màn hình: 16:10");
        _categoryNameStyle = 1.5.w;
        _nameProductStyle = 1.5.w;
        _priceProductStyle = 1.5.w;
        _discriptionProductStyle = 1.w;
        _calculatedimageHeight = _getImageHeight(widthDevice / 1.5);
        _calculatedMarginImage = EdgeInsets.symmetric(vertical: 3.h);
        _calculatedMarginItem = EdgeInsets.only(bottom: 2.h);
        break;

      case const (4 / 3): //Nếu tỷ lệ màn hình là 4:3
        print("Tỷ lệ màn hình: 4:3");
        _categoryNameStyle = 1.5.w;
        _nameProductStyle = 1.5.w;
        _priceProductStyle = 1.5.w;
        _discriptionProductStyle = 1.1.w;
        _calculatedimageHeight = _getImageHeight(widthDevice);
        _calculatedMarginImage = EdgeInsets.symmetric(vertical: 2.h);
        _calculatedMarginItem = EdgeInsets.only(bottom: 2.h);
        break;

      default: //Nếu không nhận biết được tỷ lệ màn hình
        print("Không nhận biết được tỷ lệ màn hình");
        _categoryNameStyle = 1.65.w;
        _nameProductStyle = 1.5.w;
        _priceProductStyle = 1.5.w;
        _discriptionProductStyle = 1.625.w;
        _calculatedimageHeight = _getImageHeight(widthDevice / 2);
        _calculatedMarginImage = EdgeInsets.zero;
        _calculatedMarginItem = EdgeInsets.only(bottom: 3.h);
    }
  }
}
