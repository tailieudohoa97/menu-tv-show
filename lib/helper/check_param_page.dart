// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:tv_menu/controller/template_controller.dart';
import 'package:tv_menu/helper/extension.dart';

// [102302HG] load json trên db
// import '../controllers/template_controller.dart';

// [102304HG] Load json trên source
import '../controllers/template_controller-khaFix.dart';

import 'package:tv_menu/controllers/template_user_controller.dart';

class CheckParamScreen extends StatelessWidget {
  final String param;
  const CheckParamScreen({
    Key? key,
    required this.param,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (param.isInt) {
      return TemplateControllerState(
        idTemplate: param,
        userId: '0',
      );
    } else {
      return TemplateUserControllerState(
        tokenUserTemplate: param,
      );
    }
  }
}
