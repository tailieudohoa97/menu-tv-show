import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../data/enum.dart';

class AppSetting {
  static String welcomeMsg = 'Welcome';
  static String loginMsgOnWelcomeScreen = 'Please login with your username';
  static String loginMsgOnCard = 'Login as';
  static String topBarSearch = 'Search Products, Sku#';
  static String cashierDashboardTitle = 'Add Item';
  static String cashierDashboardCategoryTitle = 'Category';
  static String checkOutCancelBtn = 'Cancellation';
  static String checkOutHoldBtn = 'Hold';
  static int nanTime = 999;
  static String currency = "\$";

  static final dynamic headerReq = {
    "Access-Control-Allow-Origin": "*",
    'Content-Type': 'application/json',
    'Accept': '*/*',
    // "Cache-Control": "no-cache"
  };
  static String avatarImage = '/images/default_image.jpg';
  static String barcodeImage = '/images/barcode.jpg';
  static String searchImage = '/images/search.png';

  // static String loginPageRoute = 'login';
  // static String pinPageRoute = 'pin';
  // static String dashboardPageRoute = 'dashboard';
  // static String homePageRoute = '/';
  // static String cashBalanceRoute = 'cash-balance-input';
  // static String cashierDashBoardRoute = 'cashier-dashboard';

  static TextStyle printinvoice = const TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w600,
    decoration: TextDecoration.underline,
    color: Colors.blueAccent,
  );
  static TextStyle totalAmt = const TextStyle(
    color: Colors.blueAccent,
    fontSize: 32,
    fontWeight: FontWeight.w600,
  );
  // static TextStyle amtHead = const TextStyle(
  //   color: Colors.black,
  //   fontSize: 23,
  //   fontWeight: FontWeight.w600,
  // );
  static TextStyle changeAmt = TextStyle(
    color: Colors.green[800],
    fontSize: 16,
    fontWeight: FontWeight.w600,
  );
  // static TextStyle inputAmt = const TextStyle(
  //   color: Colors.black,
  //   fontSize: 19,
  //   fontWeight: FontWeight.w700,
  //   letterSpacing: 0.8,
  // );
  // static TextStyle bodyText = const TextStyle(
  //   color: Colors.black,
  //   fontSize: 15,
  //   fontWeight: FontWeight.w600,
  // );

  static TextStyle cartHeader =
      const TextStyle(color: Colors.black54, fontWeight: FontWeight.bold);

  static TextStyle cusProfile = const TextStyle(
      color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold);

  static String ver = "122102";
  static bool devOp = true;
  static String serverUrl = "https://foodiemenu.co/";
  static String localUrl = "http://localhost:3002/";
  static String baseUrl = serverUrl;
  static String helloWorld = baseUrl;
  // 230306LH Update login using fetch data from WP plugin API
  static String loginUser = '${baseUrl}wp-json/tv-menu-api/v1/login/';
  static String listTemplate = '${baseUrl}wp-json/tv-menu-api/v1/templates';
  static String listUserTemplate =
      '${baseUrl}wp-json/tv-menu-api/v1/user-templates';
  static String listUser = '${baseUrl}wp-json/tv-menu-api/v1/users';
  static String addNewTemplate =
      '${baseUrl}wp-json/tv-menu-api/v1/templates/add-template';
  static String editTemplate =
      '${baseUrl}wp-json/tv-menu-api/v1/templates/update-template';
  static String deleteTemplate =
      '${baseUrl}wp-json/tv-menu-api/v1/templates/delete-template';
  static String changeStatusActiveTemplate =
      '${baseUrl}wp-json/tv-menu-api/v1/templates/change-status-active';
  static String changeStatusInActiveTemplate =
      '${baseUrl}wp-json/tv-menu-api/v1/templates/change-status-inactive';
  static String getListSearch =
      '${baseUrl}wp-json/tv-menu-api/v1/templates/get-list-search';
  static String getListUserSearch =
      '${baseUrl}wp-json/tv-menu-api/v1/users/get-list-user-search';
  static String addNewUser = '${baseUrl}wp-json/tv-menu-api/v1/users/add-user';
  static String editUser = '${baseUrl}wp-json/tv-menu-api/v1/users/update-user';
  static String deleteUser =
      '${baseUrl}wp-json/tv-menu-api/v1/users/delete-user';
  static String defaultNoImage = 'assets/No_Image_Available.png';

  static List<TextInputFormatter> numberInputFormatters = [
    FilteringTextInputFormatter.deny(RegExp(r'[a-zA-Z=&^$#@!~\(\)\s]')),
  ];
  // 20211210KC added pinInputFormatters for validations
  static List<TextInputFormatter> pinInputFormatters = [
    FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
  ];

  static List<TextInputFormatter> moneyInputFormatters = [
    FilteringTextInputFormatter.allow(RegExp(r'^(\d+)?\.?\d{0,2}')),
    FilteringTextInputFormatter.allow(RegExp(r'[0-9\.]')),
    // FilteringTextInputFormatter.deny(RegExp(r'[a-zA-Z=&^$#@!~\(\)\s]'))
  ];

  static List<TextInputFormatter> nameInputFormatters = [
    FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z\s]')),
    // FilteringTextInputFormatter.allow(RegExp(r'[]')),
    FilteringTextInputFormatter.deny(RegExp(r'[0-9]')),
  ];
  static List<TextInputFormatter> usernameInputFormatters = [
    FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z0-9\s]')),
  ];

  // calculator textsstyle
  static TextStyle calcInput = const TextStyle(
    color: Colors.white,
    fontSize: 18,
    fontWeight: FontWeight.w600,
    letterSpacing: 1,
  );
  static TextStyle calcNum = const TextStyle(
    color: Colors.white,
    fontSize: 25,
    fontWeight: FontWeight.bold,
    letterSpacing: 1,
  );
  /* product card */

  static TextStyle prodSkubag = const TextStyle(
    color: Colors.white,
    fontSize: 16,
    fontWeight: FontWeight.w600,
  );
  static TextStyle prodname = const TextStyle(
    color: Colors.black,
    fontSize: 16,
    fontWeight: FontWeight.w600,
  );
  // // calculator textsstyle
  // static TextStyle calcIenput = const TextStyle(
  //   color: Colors.white,
  //   fontSize: 18,
  //   fontWeight: FontWeight.w600,
  //   letterSpacing: 1,
  // );
  // static TextStyle calc-Num = const TextStyle(
  //   color: Colors.white,
  //   fontSize: 25,
  //   fontWeight: FontWeight.bold,
  //   letterSpacing: 1,
  // );
  // /* product card */

  // static TextStyle prodSkubg1 = const TextStyle(
  //   color: Colors.white,
  //   fontSize: 16,
  //   fontWeight: FontWeight.w600,
  // );
  // static TextStyle proodname = const TextStyle(
  //   color: Colors.black,
  //   fontSize: 16,
  //   fontWeight: FontWeight.w600,
  // );
  //test acct
  // bloud / bloud123!@#
  static Map<String, String> testAcct = const {
    "name": "bloud",
    "pass": "bloud123!@#"
  };

  // static var bags = ["1g", "2g", "3g", "7g", "28g"];
  static List<String> bagUnit = ["1", "2", "3", "7", "14", "28", "-1"];
  static final bag2Unit = {
    "qTy1": "1.0g",
    "qTy2": "2.0g",
    "qTy3": "3.0g",
    "qTy7": "7.0g",
    "qTy14": "14.0g",
    "qTy28": "28.0g",
    "eqTy": "0.0",
  };
  static Map<dynamic, String> boxNames = {
    BOX.conf: "conf", //save conf device
    BOX.app: "session", // save session playing ticket
  };

  // static var categories = [
  //   {"Sativa": bags},
  //   {"Sativa-hybrid": bags},
  //   {"Indica": bags},
  //   {"Indica-hybrid": bags},
  //   {"Edible": []},
  //   {"Concentrate": []},
  // ];

  static var checkBoxOptionForAddItem = [
    {'name': "New Arrival", 'number': '1'},
    {'name': "On Sale", 'number': '2'},
    {'name': "Promo Item", 'number': '3'},
    {'name': "1 Oz Special Price", 'number': '4'},
    {'name': "Buy 1 Get 1 Free", 'number': '5'},
    {'name': "FreeBee Item", 'number': '6'},
  ];

  static var exchangeOrderItems = [
    {
      "sku": '2',
      "name": 'Afghan Kush',
      "unit": '3.0g',
      "qty": '5',
      "price": '120',
      "isExpanded": false
    },
    {
      "sku": '1',
      "name": 'Afghan Kush',
      "unit": '7.0g',
      "qty": '5',
      "price": '50',
      "isExpanded": false
    },
    {
      "sku": '42',
      "name": 'Afghan Kush',
      "unit": '14.0g',
      "qty": '5',
      "price": '120',
      "isExpanded": false
    },
    {
      "sku": '7425',
      "name": 'Afghan Kush',
      "unit": '28.0g',
      "qty": '5',
      "price": '150',
      "isExpanded": false
    },
    {
      "sku": '75',
      "name": 'Afghan Kush',
      "unit": '3.0g',
      "qty": '5',
      "price": '120',
      "isExpanded": false
    },
    {
      "sku": '42',
      "name": 'Afghan Kush',
      "unit": '7.0g',
      "qty": '5',
      "price": '150',
      "isExpanded": false
    },
    {
      "sku": '46',
      "name": 'Afghan Kush',
      "unit": '14.0g',
      "qty": '5',
      "price": '50',
      "isExpanded": false
    },
    {
      "sku": '45',
      "name": 'Afghan Kush',
      "unit": '1.0g',
      "qty": '5',
      "price": '150',
      "isExpanded": false
    },
    {
      "sku": '35',
      "name": 'Afghan Kush',
      "unit": '1.0g',
      "qty": '5',
      "price": '100',
      "isExpanded": false
    },
  ];

  // static var bagColors = [
  //   {"1g": Colors.},
  //   {"Sativa-hybrid": bags},
  //   {"Indica": bags},
  //   {"Indica-hybrid": bags},
  //   {"Edible": []},
  //   {"Concentrate": []},
  // ];

  static var prodByCategory = [
    {
      'Sativa': [
        '5AAAAA SUPERQUADS',
        'Pink Bubble Gum',
        'Purple Mimosa',
        '5AAAAA SUPERQUADS',
        'Pink Bubble Gum',
        'Purple Mimosa'
      ]
    },
    {
      'Sativa-Hybrid': ['Vanilla Kush', 'Billy Kimber', 'Cinderella 99']
    },
    {
      'Indica': ['Cookies & Cream', 'Crystal Coma', 'Do Si Dos']
    },
    {
      'Edible': ['Green Goblin', 'Outer Space Pink']
    },
    {
      'Concentrate': ['Phantom Cookies', 'Pink Candy', 'Purple Chocolate']
    },
  ];

  static var bagColor = {
    "1.0g": const Color(0xFFEF4791),
    "2.0g": const Color(0xFF70C9A8),
    "3.0g": const Color(0xFF7B64AB),
    "7.0g": const Color(0xFF8EA7D4),
    "14.0g": const Color(0xFFBF4C85),
    "28.0g": const Color(0xFFEF4791),
    "0.0": Colors.black,
  };

  static var indexBag = {
    0: "1.0g",
    1: "2.0g",
    2: "3.0g",
    3: "7.0g",
    4: "14.0g",
    5: "28.0g"
  };

  static String circleLogo = '/images/yesweed-circle-logo.png';
  static String ywLogo = '/images/yesweed-logo.png';
  static String bloudLogo = '/images/bloud-logo.png';
  static String defaultLogo = '/images/yesweed-circle-logo.png';
  static String defaultText = 'N/A';

  static var coupons = {
    "YW10": "-10",
    "YWBOGO": "-20",
    "FREE": "-20",
  };

  /*Text style font x-small to xlarge with black and white color */
  // calculator textsstyle
  /* product card */
  static TextStyle xsmWhiteTxt = const TextStyle(
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.w600,
  );
  static TextStyle smallWhiteTxt = const TextStyle(
    color: Colors.white,
    fontSize: 16,
    fontWeight: FontWeight.w600,
  );
  //
  static TextStyle mdWhiteTxt = const TextStyle(
    color: Colors.white,
    fontSize: 20,
    fontWeight: FontWeight.w600,
    letterSpacing: 1,
  );
  //calcvnum
  static TextStyle lgWhiteTxt = const TextStyle(
    color: Colors.white,
    fontSize: 24,
    fontWeight: FontWeight.bold,
    letterSpacing: 1,
  );
  static TextStyle xlgWhiteTxt = const TextStyle(
    color: Colors.white,
    fontSize: 28,
    fontWeight: FontWeight.bold,
    letterSpacing: 1,
  );

  /// black text start
  static TextStyle xsmallBlackTxt = const TextStyle(
    color: Colors.black,
    fontSize: 15,
    fontWeight: FontWeight.w600,
  );
  static TextStyle smBlackTxt = const TextStyle(
    color: Colors.black,
    fontSize: 16,
    fontWeight: FontWeight.w600,
  );
  static TextStyle mdBlackTxt = const TextStyle(
    color: Colors.black,
    fontSize: 19,
    fontWeight: FontWeight.w700,
    letterSpacing: 0.8,
  );
  static TextStyle lgBlackTxt = const TextStyle(
    color: Colors.black,
    fontSize: 24,
    fontWeight: FontWeight.w600,
  );
  static TextStyle xlgBlackTxt = const TextStyle(
    color: Colors.black,
    fontSize: 28,
    fontWeight: FontWeight.w600,
  );
  /*Red colro font style */
  static TextStyle smRedTxt = TextStyle(
    color: Colors.red.shade700,
    fontSize: 15,
    fontWeight: FontWeight.w600,
    letterSpacing: 0.3,
  );
  static TextStyle mdRedTxt = TextStyle(
    color: Colors.red.shade700,
    fontSize: 19,
    fontWeight: FontWeight.w600,
    letterSpacing: 0.2,
  );
  static TextStyle lgRedTxt = TextStyle(
    color: Colors.red.shade700,
    fontSize: 24,
    fontWeight: FontWeight.w600,
  );

  // all Icons
  static IconData profileIcon = Icons.person_pin;
  static IconData addNewIcon = Icons.add_circle_outline;
  static IconData resumeSessionIcon = Icons.not_started_outlined;
  static IconData startnewSessionIcon = Icons.play_circle_outline_rounded;
  static IconData bellIcon = Icons.notifications;
  static IconData timerIcon = Icons.timer;
  static IconData calcIcon = Icons.calculate_rounded;
  static IconData menuIcon = Icons.menu;
  static IconData cancelIcon = Icons.cancel_outlined;
  static IconData backIcon = Icons.arrow_back_rounded;
}
