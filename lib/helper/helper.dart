// import 'dart:convert';
import 'dart:convert';
import 'dart:math';

import 'package:hive/hive.dart';
import 'package:intl/intl.dart';

import 'package:talker_flutter/talker_flutter.dart';

import 'package:flutter/material.dart';
import 'package:tv_menu/models/worker.dart';
import 'dart:html' as html;

import '../data/enum.dart';
import '../models/models.dart';
import '../helper/app_setting.dart';

class Helpers {
  static final talker = TalkerFlutter.init();

  static debugLog(screen, msg, [bool dev = true]) {
    dev = AppSetting.devOp;
    print("--------------------------");
    if (dev) print('debug_log: $screen : $msg');
  }

  static debugLogV2(screen, msg,
      [bool dev = true, LOGTYPE? type = LOGTYPE.INFO]) {
    dev = AppSetting.devOp;

    if (dev) {
      switch (type) {
        case LOGTYPE.INFO:
          talker.info('$type: $screen : $msg');
          break;
        case LOGTYPE.ERROR:
          talker.error('$type: $screen : $msg');
          break;
        default:
      }
    }
  }

  static int getCurrentSes() {
    return new DateTime.now().millisecondsSinceEpoch;
  }

  // static List<dynamic> getData(String dataName) {
  //   return RepoData.data[dataName];
  // }

  // static List<Category> getCategories() {
  //   List cats = RepoData.data["categories"];
  //   return cats.map((cat) => Category.fromMap(cat)).toList();
  // }

  // static List<Product> getProds() {
  //   List prods = RepoData.data["products"];
  //   return prods.map((prod) => Product.fromMap(prod)).toList();
  // }

  static showPopUp(BuildContext context, Widget content, Alignment alignment) {
    Widget allertDialog() {
      return AlertDialog(
          alignment: alignment,
          backgroundColor: Colors.white,
          contentPadding: EdgeInsets.zero,
          elevation: 0.0,
          content: content);
    }

    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => allertDialog(),
    );
  }

  static bool isNumeric(String s) => double.tryParse(s) != null;
  static double resSize(BuildContext context) =>
      MediaQuery.of(context).size.height + MediaQuery.of(context).size.width;
  static String bagOnHand(String? qTy, String? stock) =>
      (double.parse(stock ?? "") - double.parse(qTy ?? "")).toString();

  // static RouteArguments getRouteArgByKey(
  //     {required BuildContext context, required ARGSKEY argsKey}) {
  //   List<RouteArguments> routeArgs =
  //       ModalRoute.of(context)!.settings.arguments as List<RouteArguments>;
  //   var callback = routeArgs.firstWhereOrNull((arg) => arg.key == argsKey);
  //   return callback ?? RouteArguments(key: argsKey, val: null);
  // }

  static MaterialStateProperty<Color?> getTableAlternateRowColor(int key) {
    return MaterialStateProperty.all(
        (key % 2) > 0 ? const Color(0XFFF2F2F2) : const Color(0XFFFFFFFF));
  }

  ///salt = phone number || current date time second
  ///length = 8-9
  ///prefix = ddmm
  ///sqrt salt string
  static String generateID({String? salt}) {
    int tempSalt = int.parse(salt ?? "0");
    var prefix = DateTime.now().day.toString().padLeft(2, "0") +
        DateTime.now().month.toString().padLeft(2, "0");
    //square root + current second
    var id = prefix + sqrt(tempSalt).ceil().toString();
    return id;
  }

  static String getCurrentDate([DAYTIME? format]) {
    final current = new DateTime.now();
    switch (format) {
      case DAYTIME.MMDD:
        return "${current.month.toString().padLeft(2, '0')}${current.day.toString().padLeft(2, '0')}";
      case DAYTIME.MMDDYY:
        return "${current.month.toString().padLeft(2, '0')}${current.day.toString().padLeft(2, '0')}${current.year.toString().padLeft(2, '0')}";
      default:
        return "${current.year.toString().padRight(2)}${current.month.toString().padLeft(2, '0')}${current.day.toString().padLeft(2, '0')}";
    }
  }

  static String readTimestamp(int timestamp) {
    var now = DateTime.now();
    var format = DateFormat('HH:mm a');
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    var diff = now.difference(date);
    var time = '';

    if (diff.inSeconds <= 0 ||
        diff.inSeconds > 0 && diff.inMinutes == 0 ||
        diff.inMinutes > 0 && diff.inHours == 0 ||
        diff.inHours > 0 && diff.inDays == 0) {
      time = format.format(date);
    } else if (diff.inDays > 0 && diff.inDays < 7) {
      if (diff.inDays == 1) {
        time = diff.inDays.toString() + ' DAY AGO';
      } else {
        time = diff.inDays.toString() + ' DAYS AGO';
      }
    } else {
      if (diff.inDays == 7) {
        time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
      } else {
        time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
      }
    }

    return time;
  }

  static void disableBrowserBackButton() {
    html.window.history.pushState(null, "", html.window.location.href);
    html.window.onPopState.listen((event) {
      html.window.history.pushState(null, "", html.window.location.href);
    });
  }

  //function to get local database
  static Box? getLocalDb(BOX boxName) {
    //Define box object to store data with parameter box name
    Box? _localBox = Hive.box<String>(AppSetting.boxNames[boxName]!);

    return _localBox;
  }

  static bool isStatusCode401() {
    if (Helpers.getLocalDb(BOX.app)!.get('status_code') == "401") {
      clearSession(BOX.app);
      return true;
    } else {
      return false;
    }
  }

  //function to get clear local database
  //use for logout
  static Box? clearLocalDb(BOX boxName) {
    Box? _localBox = Hive.box<String>(AppSetting.boxNames[boxName]!);
    _localBox.put('worker', "");
    _localBox.put('templateList', "");
    _localBox.put('userTemplateList', "");
    _localBox.put('userList', "");

    return _localBox;
  }

  //function to clear all local database
  //use for close register
  static Box? clearSession(BOX boxName) {
    Box? _localBox = Hive.box<String>(AppSetting.boxNames[boxName]!);
    _localBox.put('worker', "");
    _localBox.put('templateList', "");
    _localBox.put('userTemplateList', "");
    _localBox.put('userList', "");

    return _localBox;
  }

  //function to save data to local database
  static bool saveLocalDb(BOX boxName, String? model, dynamic data) {
    Box? _localBox = Hive.box<String>(AppSetting.boxNames[boxName]!);
    //switch_case to save data to
    switch (model) {
      case "worker":
        String localStr = "";
        if (data is Worker) {
          localStr = jsonEncode(data.toMap());
        }
        _localBox.put('worker', localStr);
        break;
      case "templateList":
        String localStr = "";
        if (data is List<dynamic>) {
          localStr = jsonEncode(data.map((e) => e.toMap()).toList());
        }
        _localBox.put('templateList', localStr);
        break;
      case "userList":
        String localStr = "";
        if (data is List<dynamic>) {
          localStr = jsonEncode(data.map((e) => e.toMap()).toList());
        }
        _localBox.put('userList', localStr);
        break;
      case "userTemplateList":
        String localStr = "";
        if (data is List<dynamic>) {
          localStr = jsonEncode(data.map((e) => e.toMap()).toList());
        }
        _localBox.put('userTemplateList', localStr);
        break;
      default:
    }
    if (_localBox.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  static Widget GetImage(String imagePath,
      [double width = 50, BoxFit boxFit = BoxFit.cover]) {
    return imagePath.contains('http')
        ? Image.network(
            imagePath,
            width: width,
            fit: boxFit,
          )
        : Image.asset(
            imagePath,
            width: width,
            fit: boxFit,
          );
  }
}

// [042317HG] Capitalize the first letter of each word
extension StringCasingExtension on String {
  String toCapitalized() =>
      length > 0 ? '${this[0].toUpperCase()}${substring(1).toLowerCase()}' : '';
  String toTitleCase() => replaceAll(RegExp(' +'), ' ')
      .split(' ')
      .map((str) => str.toCapitalized())
      .join(' ');
}
