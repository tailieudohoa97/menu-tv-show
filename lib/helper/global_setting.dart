import 'package:tv_menu/helper/calulate_ratio_temp.dart';

class GlobalSetting {
  //092314LH Fix dynamic sample data to template
  static late CalculateRatioTemplate ratioStyle;
  static late dynamic ratioDataSetting;
  static late Map<String, dynamic> dataSetting;
  static late Map<String, dynamic> dynamicDataSetting;

  static const String noImage = '/no-image.png';
  static const String empty = '';
  static const String logoDefault = '/5s3s-logoblack.png';
}
