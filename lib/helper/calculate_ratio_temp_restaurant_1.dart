import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:google_fonts/google_fonts.dart';

class CalculateRatioRestaurant1 {
  final BuildContext context;
  late double _categoryNameStyle;
  late double _nameProductStyle;
  late double _nameProductStyleMedium;
  late double _discriptionProductStyle;
  late double _discriptionProductStyleMedium;
  late double _priceProductStyle;
  late double _priceProductStyleMedium;

  late double _imageHeight;
  late double _marginBottomImage;
  late double _marginBottomProduct;
  late double _marginBottomCategoryTitle;
  late double _paddingVerticalCategorTilte;
  late double _paddingHorizontalCategorTilte;
  late double _boderRadiusStyle;

  //Tạo các phương thức để trả về các TextStyle cho các loại văn bản khác nhau
  TextStyle get categoryNameStyle => TextStyle(
        fontSize: _categoryNameStyle,
        fontWeight: FontWeight.bold,
        fontFamily: GoogleFonts.bebasNeue().fontFamily,
      );

  TextStyle get nameProductStyle => TextStyle(
        fontSize: _nameProductStyle,
        fontWeight: FontWeight.w800,
        fontFamily: GoogleFonts.bebasNeue().fontFamily,
        height: 1.5,
      );

  TextStyle get nameProductStyleMedium => TextStyle(
        fontSize: _nameProductStyleMedium,
        fontWeight: FontWeight.w800,
        fontFamily: GoogleFonts.bebasNeue().fontFamily,
        height: 1.5,
      );

  TextStyle get discriptionProductStyle => TextStyle(
        fontSize: _discriptionProductStyle,
        fontWeight: FontWeight.w300,
        fontFamily: GoogleFonts.openSans().fontFamily,
        height: 1.5,
      );
  TextStyle get discriptionProductStyleMedium => TextStyle(
        fontSize: _discriptionProductStyleMedium,
        fontWeight: FontWeight.w300,
        fontFamily: GoogleFonts.openSans().fontFamily,
        height: 1.5,
      );

  TextStyle get priceProductStyle => TextStyle(
        fontSize: _priceProductStyle,
        fontWeight: FontWeight.w800,
        fontFamily: GoogleFonts.bebasNeue().fontFamily,
      );
  TextStyle get priceProductStyleMedium => TextStyle(
        fontSize: _priceProductStyleMedium,
        fontWeight: FontWeight.w800,
        fontFamily: GoogleFonts.bebasNeue().fontFamily,
      );

  double get imageHeight => _imageHeight;
  double get marginBottomImage => _marginBottomImage;
  double get marginBottomProduct => _marginBottomProduct;
  double get marginBottomCategoryTitle => _marginBottomCategoryTitle;
  double get paddingHorizontalCategorTilte => _paddingHorizontalCategorTilte;
  double get paddingVerticalCategorTilte => _paddingVerticalCategorTilte;
  double get boderRadiusStyle => _boderRadiusStyle;

  CalculateRatioRestaurant1(this.context) {
    _calculateRatioRestaurant1();
  }

  //Tạo hàm để tính toán chiều cao của Hình theo tỉ lệ màn hình
  double _getImageHeight(double width) {
    return width / 3; //Chia theo 3 cột của màn hình
  }

  //Tạo một hàm để tính toán font size theo tỉ lệ màn hình
  void _calculateRatioRestaurant1() {
    //Lấy chiều rộng và chiều cao của màn hình
    double widthDevice = MediaQuery.of(context).size.width;
    double heighDevice = MediaQuery.of(context).size.height;

    //Tính tỉ lệ màn hình bằng cách chia chiều rộng cho chiều cao
    double ratio = widthDevice / heighDevice;

    //Sử dụng switch case để kiểm tra các trường hợp khác nhau của tỷ lệ màn hình
    switch (ratio) {
      case >= (32 / 9): //Nếu tỷ lệ màn hình là 32:9
        print("Tỷ lệ màn hình: 32:9");
        _categoryNameStyle = 2.w;
        _nameProductStyle = 1.5.w;
        _nameProductStyleMedium = 1.w;
        _priceProductStyle = 1.5.w;
        _priceProductStyleMedium = 1.w;
        _discriptionProductStyle = 0.7.w;
        _discriptionProductStyleMedium = 0.6.w;
        _imageHeight = 14.h;
        _marginBottomImage = 1.w;
        _marginBottomProduct = 2.h;
        _boderRadiusStyle = 100;
        _marginBottomCategoryTitle = 1.h;
        _paddingVerticalCategorTilte = 0;
        _paddingHorizontalCategorTilte = 0;
        break;

      case >= (21 / 9): //Nếu tỷ lệ màn hình là 21:9
        print("Tỷ lệ màn hình: 21:9");
        _categoryNameStyle = 1.7.w;
        _nameProductStyle = 1.w;
        _nameProductStyleMedium = 0.8.w;
        _priceProductStyle = 1.w;
        _priceProductStyleMedium = 0.8.w;
        _discriptionProductStyle = 0.7.w;
        _discriptionProductStyleMedium = 0.5.w;
        _imageHeight = 13.h;
        _marginBottomImage = 0.5.w;
        _marginBottomProduct = 1.5.h;
        _boderRadiusStyle = 30;
        _marginBottomCategoryTitle = 1.5.h;
        _paddingVerticalCategorTilte = 0;
        _paddingHorizontalCategorTilte = 0;
        break;

      case >= (16 / 9): //Nếu tỷ lệ màn hình là 16:9
        print("Tỷ lệ màn hình: 16:9");
        _categoryNameStyle = 2.w;
        _nameProductStyle = 1.5.w;
        _nameProductStyleMedium = 1.w;
        _priceProductStyle = 1.5.w;
        _priceProductStyleMedium = 1.w;
        _discriptionProductStyle = 0.7.w;
        _discriptionProductStyleMedium = 0.6.w;
        _imageHeight = 14.h;
        _marginBottomImage = 1.w;
        _marginBottomProduct = 1.5.h;
        _boderRadiusStyle = 30;
        _marginBottomCategoryTitle = 2.h;
        _paddingVerticalCategorTilte = 1.h;
        _paddingHorizontalCategorTilte = 1.h;
        break;

      case >= (16 / 10): //Nếu tỷ lệ màn hình là 16:10
        print("Tỷ lệ màn hình: 16:10");
        _categoryNameStyle = 2.5.w;
        _nameProductStyle = 1.7.w;
        _nameProductStyleMedium = 1.5.w;
        _priceProductStyle = 1.7.w;
        _priceProductStyleMedium = 1.5.w;
        _discriptionProductStyle = 0.7.w;
        _discriptionProductStyleMedium = 0.6.w;
        _imageHeight = 18.h;
        _marginBottomImage = 1.w;
        _marginBottomProduct = 2.5.h;
        _boderRadiusStyle = 30;
        _marginBottomCategoryTitle = 1.h;
        _paddingVerticalCategorTilte = 0;
        _paddingHorizontalCategorTilte = 0;
        break;

      case >= (4 / 3): //Nếu tỷ lệ màn hình là 4:3
        print("Tỷ lệ màn hình: 4:3");
        _categoryNameStyle = 2.5.w;
        _nameProductStyle = 1.8.w;
        _nameProductStyleMedium = 1.5.w;
        _priceProductStyle = 1.8.w;
        _priceProductStyleMedium = 1.5.w;
        _discriptionProductStyle = 1.w;
        _discriptionProductStyleMedium = 0.7.w;
        _imageHeight = 14.h;
        _marginBottomImage = 1.w;
        _marginBottomProduct = 2.h;
        _boderRadiusStyle = 30;
        _marginBottomCategoryTitle = 1.h;
        _paddingVerticalCategorTilte = 0;
        _paddingHorizontalCategorTilte = 0;
        break;

      default: //Nếu không nhận biết được tỷ lệ màn hình
        print("Không nhận biết được tỷ lệ màn hình");
        _categoryNameStyle = 2.w;
        _nameProductStyle = 1.5.w;
        _nameProductStyleMedium = 1.w;
        _priceProductStyle = 1.5.w;
        _priceProductStyleMedium = 1.w;
        _discriptionProductStyle = 0.7.w;
        _discriptionProductStyleMedium = 0.6.w;
        _imageHeight = 14.h;
        _marginBottomImage = 1.w;
        _marginBottomProduct = 2.h;
        _boderRadiusStyle = 30;
        _marginBottomCategoryTitle = 2.h;
        _paddingVerticalCategorTilte = 1.h;
        _paddingHorizontalCategorTilte = 1.h;
    }
  }
}
