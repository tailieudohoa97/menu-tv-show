@JS()
library print;

import 'package:js/js.dart';

@JS()
// [032330HG] Create HTML layout for printing receipts in POS
external void printReceipt(dynamic data, [String? printId]);

// [190623HG] Create HTML layout for printing receipts in POS
external void printGiftReceipt(dynamic data, [String? printId]);

// [062313HG] Create HTML layout for send mail and printing daily report
external void dailyReport(dynamic data, [String? printId]);
