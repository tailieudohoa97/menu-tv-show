import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/module_helper.dart';

class CalculateRatioTemplate {
  final BuildContext context;
  //092314LH Fix dynamic sample data to template
  final Map<String, dynamic> textStyles;
  late Map<String, dynamic> currentRatio;
  // late double _categoryNameStyle;
  // late double _nameProductStyle;
  // late double _nameProductStyleMedium;
  // late double _discriptionProductStyle;
  // late double _discriptionProductStyleMedium;
  // late double _priceProductStyle;
  // late double _priceProductStyleMedium;

  // late double _imageHeight;
  // late double _marginBottomImage;
  // late double _marginBottomProduct;
  // late double _marginBottomCategoryTitle;
  // late double _paddingVerticalCategorTilte;
  // late double _paddingHorizontalCategorTilte;
  // late double _boderRadiusStyle;

  //Tạo các phương thức để trả về các TextStyle cho các loại văn bản khác nhau
  // TextStyle get categoryNameStyle => TextStyle(
  //       fontSize: _categoryNameStyle,
  //       fontWeight: FontWeight.bold,
  //       fontFamily: GoogleFonts.bebasNeue().fontFamily,
  //     );

  // TextStyle get nameProductStyle => TextStyle(
  //       fontSize: _nameProductStyle,
  //       fontWeight: FontWeight.w800,
  //       fontFamily: GoogleFonts.bebasNeue().fontFamily,
  //       height: 1.5,
  //     );

  // TextStyle get nameProductStyleMedium => TextStyle(
  //       fontSize: _nameProductStyleMedium,
  //       fontWeight: FontWeight.w800,
  //       fontFamily: GoogleFonts.bebasNeue().fontFamily,
  //       height: 1.5,
  //     );

  // TextStyle get discriptionProductStyle => TextStyle(
  //       fontSize: _discriptionProductStyle,
  //       fontWeight: FontWeight.w300,
  //       fontFamily: GoogleFonts.openSans().fontFamily,
  //       height: 1.5,
  //     );
  // TextStyle get discriptionProductStyleMedium => TextStyle(
  //       fontSize: _discriptionProductStyleMedium,
  //       fontWeight: FontWeight.w300,
  //       fontFamily: GoogleFonts.openSans().fontFamily,
  //       height: 1.5,
  //     );

  // TextStyle get priceProductStyle => TextStyle(
  //       fontSize: _priceProductStyle,
  //       fontWeight: FontWeight.w800,
  //       fontFamily: GoogleFonts.bebasNeue().fontFamily,
  //     );
  // TextStyle get priceProductStyleMedium => TextStyle(
  //       fontSize: _priceProductStyleMedium,
  //       fontWeight: FontWeight.w800,
  //       fontFamily: GoogleFonts.bebasNeue().fontFamily,
  //     );

  // double get imageHeight => _imageHeight;
  // double get marginBottomImage => _marginBottomImage;
  // double get marginBottomProduct => _marginBottomProduct;
  // double get marginBottomCategoryTitle => _marginBottomCategoryTitle;
  // double get paddingHorizontalCategorTilte => _paddingHorizontalCategorTilte;
  // double get paddingVerticalCategorTilte => _paddingVerticalCategorTilte;
  // double get boderRadiusStyle => _boderRadiusStyle;

  CalculateRatioTemplate(this.context, this.textStyles) {
    _calculateRatioTeCalculateRatioTemplate();
  }

  //Tạo hàm để tính toán chiều cao của Hình theo tỉ lệ màn hình
  // double _getImageHeight(double width) {
  //   return width / 3; //Chia theo 3 cột của màn hình
  // }

  //Tạo một hàm để tính toán font size theo tỉ lệ màn hình
  void _calculateRatioTeCalculateRatioTemplate() {
    //Lấy chiều rộng và chiều cao của màn hình
    double widthDevice = MediaQuery.of(context).size.width;
    double heighDevice = MediaQuery.of(context).size.height;

    //Tính tỉ lệ màn hình bằng cách chia chiều rộng cho chiều cao
    double ratio = widthDevice / heighDevice;

    //Sử dụng switch case để kiểm tra các trường hợp khác nhau của tỷ lệ màn hình
    //092314LH Fix dynamic sample data to template
    switch (ratio) {
      case >= (32 / 9): //Nếu tỷ lệ màn hình là 32:9
        print("Tỷ lệ màn hình: 32:9");
        currentRatio = GlobalSetting.ratioDataSetting.containsKey('32/9')
            ? GlobalSetting.ratioDataSetting['32/9']
            : {};
        break;
      case >= (21 / 9): //Nếu tỷ lệ màn hình là 21:9
        print("Tỷ lệ màn hình: 21:9");
        currentRatio = GlobalSetting.ratioDataSetting.containsKey('21/9')
            ? GlobalSetting.ratioDataSetting['21/9']
            : {};
        break;
      case >= (16 / 9): //Nếu tỷ lệ màn hình là 16:9
        currentRatio = GlobalSetting.ratioDataSetting.containsKey("16/9")
            ? GlobalSetting.ratioDataSetting['16/9']
            : {};
        break;

      case >= (16 / 10): //Nếu tỷ lệ màn hình là 16:10
        print("Tỷ lệ màn hình: 16:10");
        currentRatio = GlobalSetting.ratioDataSetting.containsKey('16/10')
            ? GlobalSetting.ratioDataSetting['16/10']
            : {};
        break;
      case >= (4 / 3): //Nếu tỷ lệ màn hình là 4:3
        print("Tỷ lệ màn hình: 4:3");
        currentRatio = GlobalSetting.ratioDataSetting.containsKey('4/3')
            ? GlobalSetting.ratioDataSetting['4/3']
            : {};
        break;
      default: //Nếu không nhận biết được tỷ lệ màn hìn
        print("Không nhận biết được tỷ lệ màn hình");
        // [102302HG] Add this code for Default
        currentRatio = GlobalSetting.ratioDataSetting.containsKey('default')
            ? GlobalSetting.ratioDataSetting['default']
            : {};
    }
  }

  // 092314LH Create a dynamic value get function text style
  TextStyle getTextStyle(String key) {
    Map<String, dynamic> dataStyle =
        textStyles.containsKey(key) ? textStyles[key] : {};
    return TextStyle(
      fontSize: currentRatio.containsKey(key) ? getField(key) : null,
      fontWeight: dataStyle.containsKey('fontWeight')
          ? ModuleHelper.arrFontWeight[dataStyle['fontWeight']] ??
              FontWeight.normal
          : FontWeight.normal,
      fontFamily:
          dataStyle.containsKey('fontFamily') && dataStyle['fontFamily'] != ''
              ? GoogleFonts.getFont(dataStyle['fontFamily']).fontFamily
              : null,
      height: dataStyle.containsKey('height') && dataStyle['height'] != ''
          ? SizingValue.fromString(dataStyle['height'])
          : null,
      // [112308TIN] add letterSpacing property
      letterSpacing: dataStyle.containsKey('letterSpacing') &&
              dataStyle['letterSpacing'] != ''
          ? SizingValue.fromString(dataStyle['letterSpacing'])
          : null,
    );
  }

  // 092314LH Create a dynamic value get function double
  double getField(String key) {
    return currentRatio.containsKey(key) && currentRatio[key] != ''
        ? SizingValue.fromString(currentRatio[key])
        : 0;
  }
}
