extension NumExtensions on String {
  bool get isInt => RegExp(r'^-?[0-9]+$').hasMatch(this);
}
