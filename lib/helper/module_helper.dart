import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:tv_menu/helper/SizingValue.dart';
import 'package:tv_menu/helper/global_setting.dart';
import 'package:tv_menu/helper/helper.dart';
import 'package:tv_menu/views/module_widgets/category_title_grid_layout_1_widget.dart';
import 'package:tv_menu/views/module_widgets/category_title_widget.dart';
import 'package:tv_menu/views/module_widgets/modules_export.dart';

class ModuleHelper {
  static final Map<String, BoxFit> boxFitMap = {
    for (var item in BoxFit.values) item.name.toString(): item
  };

  // [112309TIN] add full values of Alignment
  static final Map<String, AlignmentGeometry> arrAlignmentGeometry = {
    'topLeft': Alignment.topLeft,
    'topCenter': Alignment.topCenter,
    'topRight': Alignment.topRight,
    'centerLeft': Alignment.centerLeft,
    'center': Alignment.center,
    'centerRight': Alignment.centerRight,
    'bottomLeft': Alignment.bottomLeft,
    'bottomCenter': Alignment.bottomCenter,
    'bottomRight': Alignment.bottomRight,
  };

  // [112309TIN] add values of AlignmentDirectional
  static final Map<String, AlignmentDirectional> arrAlignmentDirectional = {
    'topStart': AlignmentDirectional.topStart,
    'topCenter': AlignmentDirectional.topCenter,
    'topEnd': AlignmentDirectional.topEnd,
    'centerStart': AlignmentDirectional.centerStart,
    'center': AlignmentDirectional.center,
    'centerEnd': AlignmentDirectional.centerEnd,
    'bottomStart': AlignmentDirectional.bottomStart,
    'bottomCenter': AlignmentDirectional.bottomCenter,
    'bottomEnd': AlignmentDirectional.bottomEnd,
  };

  //[092313HG] Add MainAxisAlignment
  static final Map<String, MainAxisAlignment> arrMainAxisAlignment = {
    for (var item in MainAxisAlignment.values) item.name.toString(): item
  };

  // [092313HG] Add CrossAxisAlignment
  static final Map<String, CrossAxisAlignment> arrCrossAxisAlignment = {
    for (var item in CrossAxisAlignment.values) item.name.toString(): item
  };

  // [092320Legolas] Add TextAlign
  static final Map<String, TextAlign> arrTextAlign = {
    for (var item in TextAlign.values) item.name.toString(): item
  };
  // [092320Legolas] Add BlendMode
  static final Map<String, BlendMode> blendModeMap = {
    for (var item in BlendMode.values) item.name.toString(): item
  };

  //092314LH Add FontWeight Array list
  static final Map<String, FontWeight> arrFontWeight = {
    for (var item in FontWeight.values)
      item.toString().replaceAll('FontWeight.', ''): item
  };

  // [092320Tin] Add WrapAlignment array list
  static final Map<String, WrapAlignment> arrWrapAlignment = {
    for (var item in WrapAlignment.values) item.name.toString(): item
  };

  // [092320Tin] Add WrapCrossAlignment array list
  static final Map<String, WrapCrossAlignment> arrWrapCrossAlignment = {
    for (var item in WrapCrossAlignment.values) item.name.toString(): item
  };

  // [092320Tin] Add VerticalDirection array list
  static final Map<String, VerticalDirection> arrVerticalDirection = {
    for (var item in VerticalDirection.values) item.name.toString(): item
  };

  // [092320Tin] Add Axis array list
  static final Map<String, Axis> arrAxis = {
    for (var item in Axis.values) item.name.toString(): item
  };

  // [092321Tin] Add arr Clip
  static final Map<String, Clip> arrClip = {
    for (var item in Clip.values) item.name.toString(): item
  };

  // [092322Tin] Add arr FontStyle
  static final Map<String, FontStyle> arrFontStyle = {
    for (var item in FontStyle.values) item.name.toString(): item
  };

  // [112321TIN] replace widgetName for widget has first letter in name is lowercase
  static Widget renderWidget(dynamic setting) {
    if (setting.isEmpty || !setting.containsKey('widgetName')) {
      return Placeholder();
    }
    switch (setting['widgetName']) {
      case 'Row':
        return rowWidget(setting['widgetSetting']);
      case 'Container':
        return containerWidget(setting['widgetSetting']);
      case 'Column':
        return columnWidget(setting['widgetSetting']);
      case 'Expanded':
        return expandedWidget(setting['widgetSetting']);
      case 'Padding':
        return paddingWidget(setting['widgetSetting']);

      // case 'categoryTitleSingleColumn':
      //   return categoryTitleSingleColumnWidget(setting['widgetSetting']);
      case 'CategoryTitleSingleColumn':
        return categoryTitleSingleColumnWidget(setting['widgetSetting']);
      case 'SizedBox':
        return sizedBoxWidget(setting['widgetSetting']);

      //[092313HG]
      // case 'image':
      //   return imageWidget(setting['widgetSetting']);
      // case 'categoryTitleGridLayout1':
      //   return categoryTitleGridLayout1Widget(setting['widgetSetting']);

      case 'Image':
        return imageWidget(setting['widgetSetting']);
      case 'CategoryTitleGridLayout1':
        return categoryTitleGridLayout1Widget(setting['widgetSetting']);

      //[092315HG]
      // case 'categoryTitle':
      //   return categoryTitleWidget(setting['widgetSetting']);
      case 'CategoryTitle':
        return categoryTitleWidget(setting['widgetSetting']);

      //[092318HG]
      case 'ListView':
        return listViewWidget(setting['widgetSetting']);
      // case 'productItem':
      //   return productItemWidget(setting['widgetSetting']);
      // case 'productList':
      //   return productListWidget(setting['widgetSetting']);
      // // [092328HG]
      // case 'categoryTitleGridLayout4':
      //   return categoryTitleGridLayout4Widget(setting['widgetSetting']);

      case 'ProductItem':
        return productItemWidget(setting['widgetSetting']);
      case 'ProductList':
        return productListWidget(setting['widgetSetting']);
      // [092328HG]
      case 'CategoryTitleGridLayout4':
        return categoryTitleGridLayout4Widget(setting['widgetSetting']);

      // [092319Tin] add categoryBlock1Widget, categoryBlock2Widget
      // categoryColumn1Widget, categoryColumn2Widget,
      // ProductRow1, ProductRow2, ProductRow3,
      // FramedImage, IndexedMenuItem,
      // MenuColumn1, MenuItemPrice1, UnderlineBrand
      case 'CategoryBlock1':
        return categoryBlock1Widget(setting['widgetSetting']);
      case 'CategoryBlock2':
        return categoryBlock2Widget(setting['widgetSetting']);
      case 'CategoryColumn1':
        return categoryColumn1Widget(setting['widgetSetting']);
      case 'CategoryColumn2':
        return categoryColumn2Widget(setting['widgetSetting']);
      case 'ProductRow1':
        return productRow1Widget(setting['widgetSetting']);
      case 'ProductRow2':
        return productRow2Widget(setting['widgetSetting']);
      case 'ProductRow3':
        return productRow3Widget(setting['widgetSetting']);
      case 'FramedImage':
        return framedImageWidget(setting['widgetSetting']);
      case 'IndexedMenuItem1':
        return indexedMenuItem1Widget(setting['widgetSetting']);
      case 'MenuColumn1':
        return menuColumn1Widget(setting['widgetSetting']);
      case 'MenuItemPrice1':
        return menuItemPrice1Widget(setting['widgetSetting']);
      case 'UnderlineBrand':
        return underlineBrandWidget(setting['widgetSetting']);
      //[092318Legolas]
      // case 'serviceItemRowWidget':
      //   return serviceItemRowWidget(setting['widgetSetting']);
      case 'Positioned':
        return positionedWidget(setting['widgetSetting']);
      case 'Stack':
        return stackWidget(setting['widgetSetting']);
      case 'Text':
        return textWidget(setting['widgetSetting']);
      case 'Center':
        return centerWidget(setting['widgetSetting']);
      case 'RotatedBox':
        return rotatedBoxWidget(setting['widgetSetting']);

      // [092320Tin] Add wrapWidget
      case 'Wrap':
        return wrapWidget(setting['widgetSetting']);

      //[092320Legolas]
      case 'Divider':
        return dividerWidget(setting['widgetSetting']);
      case 'VerticalDivider':
        return verticalDividerWidget(setting['widgetSetting']);
      case 'CustomContainer':
        return customContainerWidget(setting['widgetSetting']);

      // [092321Tin] Add paddingByRatioWidget
      case 'PaddingByRatio':
        return paddingByRatioWidget(setting['widgetSetting']);

      // [092322Tin] Add SimpleImage widget
      case 'SimpleImage':
        return simpleImageWidget(setting['widgetSetting']);
      case 'ServiceItemWithThumbnail':
        return serviceItemWithThumbnailWidget(setting['widgetSetting']);
      case 'ServiceItemCustom1':
        return serviceItemCustom1Widget(setting['widgetSetting']);

      // [092327Tin] Add CategoryTitleGridLayout3, IntrinsicHeight, SizedBoxByRatio
      case 'CategoryTitleGridLayout3':
        return categoryTitleGridLayout3Widget(setting['widgetSetting']);
      case 'IntrinsicHeight':
        return intrinsicHeightWidget(setting['widgetSetting']);
      case 'SizedBoxByRatio':
        return sizedBoxByRatioWidget(setting['widgetSetting']);
      case 'ServiceItemColumn':
        return serviceItemColumnWidget(setting['widgetSetting']);
      // [112309TIN] add TransformRotate module
      case 'TransformRotate':
        return transformRotateWidget(setting['widgetSetting']);
      // [112314TIN] add DashedDivider module
      case 'DashedDivider':
        return dashedDividerWidget(setting['widgetSetting']);
      default:
        return Placeholder();
    }
  }

  // [112310TIN] add colorFilter property
  static DecorationImage getImageDecoration(String imagePath,
      {BoxFit boxFit = BoxFit.cover, ColorFilter? colorFilter}) {
    return imagePath.contains('http')
        ? DecorationImage(
            image: NetworkImage(imagePath),
            fit: boxFit,
            colorFilter: colorFilter,
          )
        : DecorationImage(
            image: AssetImage(
                '${imagePath != '' ? imagePath : GlobalSetting.noImage}'),
            fit: boxFit,
          );
  }

  //092314LH Fix dynamic sample data to template
  static Image getImage(String imagePath,
      {BoxFit? fit = BoxFit.contain, double? height, double? width}) {
    return imagePath.contains('http')
        ? Image.network(
            imagePath,
            fit: fit,
            height: height,
            width: width,
          )
        : Image.asset(
            '${imagePath != '' ? imagePath : GlobalSetting.noImage}',
            fit: fit,
            height: height,
            width: width,
          );
  }

  //092314LH Fix dynamic sample data to template
  static dynamic getDataFromSetting(String dataKey, [String? subKey = '']) {
    dynamic data = GlobalSetting.dynamicDataSetting.containsKey(dataKey)
        ? GlobalSetting.dynamicDataSetting[dataKey]
        : GlobalSetting.dataSetting.containsKey(dataKey)
            ? GlobalSetting.dataSetting[dataKey]
            : '';
    return subKey != '' ? data[subKey] : data;
  }

  // [112317TIN] add backgroundColor property
  // [112316TIN] fix don't get bold for some fonts and factory code
  static TextStyle getTextStyle(Map<String, dynamic> dataStyle) {
    // [112309TIN] add shadows property
    final shadows = dataStyle.containsKey('shadows')
        ? (dataStyle['shadows'] as List<Map<String, dynamic>>)
            .map(getShadow)
            .toList()
        : null;

    final fontSize = dataStyle.containsKey('fontSize')
        ? SizingValue.fromString(dataStyle['fontSize'])
        : null;
    final fontWeight = dataStyle.containsKey('fontWeight')
        ? ModuleHelper.arrFontWeight[dataStyle['fontWeight']] ??
            FontWeight.normal
        : FontWeight.normal;

    final height = dataStyle.containsKey('height') && dataStyle['height'] != ''
        ? SizingValue.fromString(dataStyle['height'])
        : null;
    final color = dataStyle.containsKey('color')
        ? Color(int.parse(dataStyle['color']))
        : null;

    final letterSpacing = dataStyle.containsKey('letterSpacing')
        ? SizingValue.fromString(dataStyle['letterSpacing'])
        : null;
    final fontStyle = dataStyle.containsKey('fontStyle')
        ? ModuleHelper.arrFontStyle[dataStyle['fontStyle']]
        : null;

    final backgroundColor = dataStyle.containsKey('backgroundColor')
        ? Color(int.parse(dataStyle['backgroundColor']))
        : null;

    TextStyle style = TextStyle(
      fontSize: fontSize,
      fontWeight: fontWeight,
      height: height,
      color: color,
      letterSpacing: letterSpacing,
      fontStyle: fontStyle,
      shadows: shadows,
      backgroundColor: backgroundColor,
    );

    final String? fontFamily = dataStyle.containsKey('fontFamily')
        ? dataStyle['fontFamily'].toString()
        : null;

    if (fontFamily != null && fontFamily.isNotEmpty) {
      try {
        // assign fontWeigth to getFont function to get bold font
        style = GoogleFonts.getFont(
          fontFamily,
          fontSize: fontSize,
          fontWeight: fontWeight,
          height: height,
          color: color,
          letterSpacing: letterSpacing,
          fontStyle: fontStyle,
          shadows: shadows,
          backgroundColor: backgroundColor,
        );
      } catch (e) {
        Helpers.debugLog('Template screen',
            'Not found font "$fontFamily", change to default font');
      }
    }

    return style;

    // return TextStyle(
    //   fontSize: dataStyle.containsKey('fontSize')
    //       ? SizingValue.fromString(dataStyle['fontSize'])
    //       : null,
    //   fontWeight: dataStyle.containsKey('fontWeight')
    //       ? ModuleHelper.arrFontWeight[dataStyle['fontWeight']] ??
    //           FontWeight.normal
    //       : FontWeight.normal,
    //   fontFamily:
    //       dataStyle.containsKey('fontFamily') && dataStyle['fontFamily'] != ''
    //           ? GoogleFonts.getFont(dataStyle['fontFamily']).fontFamily
    //           : null,
    //   height: dataStyle.containsKey('height') && dataStyle['height'] != ''
    //       ? SizingValue.fromString(dataStyle['height'])
    //       : null,
    //   color: dataStyle.containsKey('color')
    //       ? Color(int.parse(dataStyle['color']))
    //       : null,

    //   // [092320Tin] Add letterSpacing property
    //   letterSpacing: dataStyle.containsKey('letterSpacing')
    //       ? SizingValue.fromString(dataStyle['letterSpacing'])
    //       : null,
    //   // [092322Tin] Add fontStyle property
    //   fontStyle: dataStyle.containsKey('fontStyle')
    //       ? ModuleHelper.arrFontStyle[dataStyle['fontStyle']]
    //       : null,
    //   shadows: shadows,
    // );
  }

  // [092319Tin] Add methods: getBorder and getBorderSide
  static Border getBorder(Map<String, dynamic> setting) {
    BorderSide top = setting.containsKey('top')
        ? getBorderSide(setting['top'])
        : BorderSide.none;
    BorderSide bottom = setting.containsKey('bottom')
        ? getBorderSide(setting['bottom'])
        : BorderSide.none;
    BorderSide right = setting.containsKey('right')
        ? getBorderSide(setting['right'])
        : BorderSide.none;
    BorderSide left = setting.containsKey('left')
        ? getBorderSide(setting['left'])
        : BorderSide.none;
    BorderSide? all =
        setting.containsKey('all') ? getBorderSide(setting['all']) : null;

    if (all != null) {
      return Border.fromBorderSide(all);
    }

    return Border(
      top: top,
      bottom: bottom,
      left: left,
      right: right,
    );
  }

  static BorderSide getBorderSide(Map<String, dynamic> setting) {
    Color? color = setting.containsKey('color')
        ? Color(int.parse(setting['color']))
        : Colors.black;

    double width = setting.containsKey('width')
        ? SizingValue.fromString(setting['width'])
        : 1;

    return BorderSide(
      color: color,
      width: width,
    );
  }

  // [092321Tin] create getImageProvider
  static ImageProvider getImageProvider(String imagePath) {
    if (imagePath.startsWith(RegExp('https?://'))) {
      return NetworkImage(imagePath);
    }

    return AssetImage(imagePath);
  }

  // [112309TIN] create getShadow
  static Shadow getShadow(Map<String, dynamic> setting) {
    final color = setting.containsKey('color')
        ? Color(int.parse(setting['color']))
        : Colors.transparent;
    final double blurRadius = setting.containsKey('blurRadius')
        ? SizingValue.fromString(setting['blurRadius'])
        : 0;
    final offset = setting.containsKey('offset')
        ? getOffset(setting['offset'])
        : Offset.zero;
    return Shadow(
      color: color,
      offset: offset,
      blurRadius: blurRadius,
    );
  }

  // [112309TIN] create getOffset
  static Offset getOffset(Map<String, dynamic> setting) {
    final double dx =
        setting.containsKey('dx') ? SizingValue.fromString(setting['dx']) : 0;
    final double dy =
        setting.containsKey('dy') ? SizingValue.fromString(setting['dy']) : 0;
    return Offset(dx, dy);
  }

  // [112310TIN] create getColorFilter
  static ColorFilter getColorFilter(Map<String, dynamic> setting) {
    final Color color = setting.containsKey('color')
        ? Color(int.parse(setting['color']))
        : Colors.transparent;
    final BlendMode blendMode = setting.containsKey('blendMode')
        ? blendModeMap[setting['blendMode']] ?? BlendMode.softLight
        : BlendMode.softLight;

    return ColorFilter.mode(color, blendMode);
  }
}
