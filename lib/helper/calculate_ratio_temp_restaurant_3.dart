import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:google_fonts/google_fonts.dart';

class CalculateRatioRestaurant3 {
  final BuildContext context;
  late double _categoryNameStyle;
  late double _nameProductStyle;
  late double _nameProductStyleMedium;
  late double _discriptionProductStyle;
  late double _discriptionProductStyleMedium;
  late double _priceProductStyle;
  late double _priceProductStyleMedium;

  late double _imageHeight;
  late double _marginBottomImage;
  late double _marginBottomProduct;
  late double _marginBottomCategoryTitle;
  late double _paddingVerticalCategorTilte;
  late double _paddingHorizontalCategorTilte;
  late double _boderRadiusStyle;
  late double _widthThumnailProduct;
  late double _heightThumnailProduct;

  //Tạo các phương thức để trả về các TextStyle cho các loại văn bản khác nhau
  TextStyle get categoryNameStyle => TextStyle(
        fontSize: _categoryNameStyle,
        fontWeight: FontWeight.w300,
        fontFamily: GoogleFonts.alfaSlabOne().fontFamily,
        letterSpacing: 5,
      );

  TextStyle get nameProductStyle => TextStyle(
        fontSize: _nameProductStyle,
        fontWeight: FontWeight.w800,
        fontFamily: GoogleFonts.poppins().fontFamily,
      );

  TextStyle get nameProductStyleMedium => TextStyle(
        fontSize: _nameProductStyleMedium,
        fontWeight: FontWeight.w800,
        fontFamily: GoogleFonts.poppins().fontFamily,
      );

  TextStyle get discriptionProductStyle => TextStyle(
        fontSize: _discriptionProductStyle,
        fontWeight: FontWeight.w300,
        fontFamily: GoogleFonts.poppins().fontFamily,
      );
  TextStyle get discriptionProductStyleMedium => TextStyle(
        fontSize: _discriptionProductStyleMedium,
        fontWeight: FontWeight.w300,
        fontFamily: GoogleFonts.poppins().fontFamily,
      );

  TextStyle get priceProductStyle => TextStyle(
        fontSize: _priceProductStyle,
        fontWeight: FontWeight.w800,
        fontFamily: GoogleFonts.poppins().fontFamily,
      );
  TextStyle get priceProductStyleMedium => TextStyle(
        fontSize: _priceProductStyleMedium,
        fontWeight: FontWeight.w800,
        fontFamily: GoogleFonts.poppins().fontFamily,
      );

  double get imageHeight => _imageHeight;
  double get marginBottomImage => _marginBottomImage;
  double get marginBottomProduct => _marginBottomProduct;
  double get marginBottomCategoryTitle => _marginBottomCategoryTitle;
  double get paddingVerticalCategorTilte => _paddingVerticalCategorTilte;
  double get paddingHorizontalCategorTilte => _paddingHorizontalCategorTilte;
  double get boderRadiusStyle => _boderRadiusStyle;
  double get widthThumnailProduct => _widthThumnailProduct;
  double get heightThumnailProduct => _heightThumnailProduct;

  CalculateRatioRestaurant3(this.context) {
    _calculateRatioRestaurant3();
  }

  //Tạo hàm để tính toán chiều cao của Hình theo tỉ lệ màn hình
  // double _getImageHeight(double width) {
  //   return width / 3; //Chia theo 3 cột của màn hình
  // }

  //Tạo một hàm để tính toán font size theo tỉ lệ màn hình
  void _calculateRatioRestaurant3() {
    //Lấy chiều rộng và chiều cao của màn hình
    double widthDevice = MediaQuery.of(context).size.width;
    double heighDevice = MediaQuery.of(context).size.height;

    //Tính tỉ lệ màn hình bằng cách chia chiều rộng cho chiều cao
    double ratio = widthDevice / heighDevice;

    //Sử dụng switch case để kiểm tra các trường hợp khác nhau của tỷ lệ màn hình
    switch (ratio) {
      case >= (32 / 9): //Nếu tỷ lệ màn hình là 32:9
        print("Tỷ lệ màn hình: 32:9");
        _categoryNameStyle = 1.w;
        _nameProductStyle = 0.7.w;
        _nameProductStyleMedium = 0.5.w;
        _priceProductStyle = 0.7.w;
        _priceProductStyleMedium = 0.5.w;
        _discriptionProductStyle = 0.5.w;
        _discriptionProductStyleMedium = 0.3.w;
        _imageHeight = 50.h;
        _marginBottomImage = 0;
        _marginBottomProduct = 2.h;
        _boderRadiusStyle = 30;
        _marginBottomCategoryTitle = 5.h;
        _paddingVerticalCategorTilte = 0.w;
        _paddingHorizontalCategorTilte = 2.w;
        _widthThumnailProduct = 3.w;
        _heightThumnailProduct = 3.w;
        break;

      case >= (21 / 9): //Nếu tỷ lệ màn hình là 21:9
        print("Tỷ lệ màn hình: 21:9");
        _categoryNameStyle = 1.5.w;
        _nameProductStyle = 1.w;
        _nameProductStyleMedium = 0.7.w;
        _priceProductStyle = 1.w;
        _priceProductStyleMedium = 0.7.w;
        _discriptionProductStyle = 0.7.w;
        _discriptionProductStyleMedium = 0.8.w;
        _imageHeight = 50.h;
        _marginBottomImage = 0;
        _marginBottomProduct = 2.h;
        _boderRadiusStyle = 30;
        _marginBottomCategoryTitle = 5.h;
        _paddingVerticalCategorTilte = 0.w;
        _paddingHorizontalCategorTilte = 2.w;
        _widthThumnailProduct = 3.w;
        _heightThumnailProduct = 3.w;
        break;

      case >= (16 / 9): //Nếu tỷ lệ màn hình là 16:9
        print("Tỷ lệ màn hình: 16:9");
        _categoryNameStyle = 2.5.w;
        _nameProductStyle = 1.5.w;
        _nameProductStyleMedium = 1.w;
        _priceProductStyle = 1.5.w;
        _priceProductStyleMedium = 1.w;
        _discriptionProductStyle = 1.w;
        _discriptionProductStyleMedium = 0.8.w;
        _imageHeight = 20.h;
        _marginBottomImage = 0;
        _marginBottomProduct = 2.h;
        _boderRadiusStyle = 30;
        _marginBottomCategoryTitle = 5.h;
        _paddingVerticalCategorTilte = 0.5.w;
        _paddingHorizontalCategorTilte = 2.w;
        _widthThumnailProduct = 3.w;
        _heightThumnailProduct = 3.w;
        break;

      case >= (16 / 10): //Nếu tỷ lệ màn hình là 16:10
        print("Tỷ lệ màn hình: 16:10");
        _categoryNameStyle = 3.w;
        _nameProductStyle = 1.5.w;
        _nameProductStyleMedium = 1.w;
        _priceProductStyle = 1.5.w;
        _priceProductStyleMedium = 1.w;
        _discriptionProductStyle = 1.w;
        _discriptionProductStyleMedium = 0.8.w;
        _imageHeight = 50.h;
        _marginBottomImage = 0;
        _marginBottomProduct = 2.h;
        _boderRadiusStyle = 30;
        _marginBottomCategoryTitle = 5.h;
        _paddingVerticalCategorTilte = 1.w;
        _paddingHorizontalCategorTilte = 2.w;
        _widthThumnailProduct = 3.w;
        _heightThumnailProduct = 3.w;
        break;

      case >= (4 / 3): //Nếu tỷ lệ màn hình là 4:3
        print("Tỷ lệ màn hình: 4:3");
        _categoryNameStyle = 3.w;
        _nameProductStyle = 2.w;
        _nameProductStyleMedium = 1.5.w;
        _priceProductStyle = 2.w;
        _priceProductStyleMedium = 1.5.w;
        _discriptionProductStyle = 1.5.w;
        _discriptionProductStyleMedium = 0.8.w;
        _imageHeight = 50.h;
        _marginBottomImage = 0;
        _marginBottomProduct = 2.h;
        _boderRadiusStyle = 30;
        _marginBottomCategoryTitle = 5.h;
        _paddingVerticalCategorTilte = 0.w;
        _paddingHorizontalCategorTilte = 2.w;
        _widthThumnailProduct = 3.w;
        _heightThumnailProduct = 3.w;
        break;

      default: //Nếu không nhận biết được tỷ lệ màn hình
        print("Không nhận biết được tỷ lệ màn hình");
        _categoryNameStyle = 3.w;
        _nameProductStyle = 1.5.w;
        _nameProductStyleMedium = 1.w;
        _priceProductStyle = 1.5.w;
        _priceProductStyleMedium = 1.w;
        _discriptionProductStyle = 1.w;
        _discriptionProductStyleMedium = 0.8.w;
        _imageHeight = 20.h;
        _marginBottomImage = 0;
        _marginBottomProduct = 2.h;
        _boderRadiusStyle = 30;
        _marginBottomCategoryTitle = 5.h;
        _paddingVerticalCategorTilte = 0.w;
        _paddingHorizontalCategorTilte = 2.w;
        _widthThumnailProduct = 3.w;
        _heightThumnailProduct = 3.w;
    }
  }
}
