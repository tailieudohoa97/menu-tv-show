import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

//cách sử dụng:
// TextStyleGlobal.nameCategory(context),
// TextStyleGlobal.nameProduct(context),
// TextStyleGlobal.smallNameProduct,
// TextStyleGlobal.mediumDiscriptionProduct,
// TextStyleGlobal.smallDiscriptionProduct(context),

class TextStyleGlobal2 {
  //Tạo 5 biến để lưu trữ font size cho 5 loại văn bản khác nhau
  static double? nameCategoryW;
  static double? nameProductW;
  static double? discriptionProductW;
  static double? smallDiscriptionProductW;

  //Tạo một hàm để tính toán font size theo tỉ lệ màn hình
  static void _calculateFontSize(BuildContext context) {
    //Lấy chiều rộng và chiều cao của màn hình
    double maxWidth = MediaQuery.of(context).size.width;
    double maxHeight = MediaQuery.of(context).size.height;
    //Tính tỉ lệ màn hình bằng cách chia chiều rộng cho chiều cao
    double ratio = maxWidth / maxHeight;

    //Sử dụng câu lệnh if-else để xét các trường hợp khác nhau của tỉ lệ màn hình
    if (ratio >= 32 / 9) {
      print("Tỷ lệ màn hình: 32:9");
      //Gán giá trị cho các biến font size theo tỉ lệ phần trăm của chiều rộng màn hình
      nameCategoryW = 1.5.w;
      nameProductW = 1.5.w;
      discriptionProductW = 1.w;
    } else if (ratio >= 21 / 9) {
      print("Tỷ lệ màn hình: 21:9");
      nameCategoryW = 1.5.w;
      nameProductW = 1.5.w;
      discriptionProductW = 1.w;
    } else if (ratio >= 16 / 9) {
      print("Tỷ lệ màn hình: 16:9");
      nameCategoryW = 1.5.w;
      nameProductW = 1.5.w;
      discriptionProductW = 1.w;
    } else if (ratio >= 16 / 10) {
      print("Tỷ lệ màn hình: 16:10");
      nameCategoryW = 1.5.w;
      nameProductW = 1.5.w;

      discriptionProductW = 1.w;
    } else if (ratio >= 4 / 3) {
      print("Tỷ lệ màn hình: 4:3");
      nameCategoryW = 1.5.w;
      nameProductW = 1.5.w;
      discriptionProductW = 1.1.w;
    } else {
      print("Không nhận biết được tỷ lệ màn hình");
      nameCategoryW = 1.65.w;
      nameProductW = 1.5.w;

      discriptionProductW = 1.625.w;
    }
    //Kết thúc câu lệnh if-else
  }

//Tạo các phương thức để trả về các TextStyle cho các loại văn bản khác nhau
//Các phương thức này sẽ gọi hàm _calculateFontSize để cập nhật giá trị của các biến font size
  static TextStyle nameCategory(BuildContext context) {
    //Gọi hàm _calculateFontSize
    _calculateFontSize(context);
    //Trả về một TextStyle với font size là giá trị của biến nameCategory
    return TextStyle(
      fontSize: nameCategoryW,
      fontWeight: FontWeight.bold,
      color: Colors.white,
    );
  }

  static TextStyle nameProduct(BuildContext context) {
    //Gọi hàm _calculateFontSize
    _calculateFontSize(context);
    //Trả về một TextStyle với font size là giá trị của biến nameProduct
    return TextStyle(
      fontSize: nameProductW,
      fontWeight: FontWeight.w800,
      color: Colors.white,
    );
  }

  static TextStyle discriptionProduct(BuildContext context) {
    //Gọi hàm _calculateFontSize
    _calculateFontSize(context);
    //Trả về một TextStyle với font size là giá trị của biến mediumDiscriptionProduct
    return TextStyle(
      fontSize: discriptionProductW,
      fontWeight: FontWeight.w300,
      color: Colors.white,
    );
  }
}
