import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../common/colors/app_color.dart';
import '../../common/constants/string_manager.dart';

class AppImage extends StatelessWidget {
  final String asset;
  final String url;
  final double? width;
  final double? height;
  final Color? color;
  final BoxFit? fit;

  const AppImage({
    Key? key,
    this.asset = '',
    this.url = '',
    this.width,
    this.height,
    this.color,
    this.fit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (url.isNotEmpty) {
      return _ImageNetwork(
        url: url,
        width: width,
        height: height,
        fit: fit,
        color: color,
        assetImageIfError: asset,
      );
    }
    return _ImageAsset(
      assetName: asset,
      width: width,
      height: height,
      color: color,
      fit: fit,
    );
  }
}

class _ImageAsset extends StatelessWidget {
  final double? height;
  final double? width;
  final String assetName;
  final Color? color;
  final BoxFit? fit;
  const _ImageAsset({
    required this.assetName,
    this.height,
    this.width,
    this.color,
    this.fit,
  });

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      assetName,
      height: height,
      width: width,
      color: color,
      fit: fit ?? BoxFit.contain,
    );
  }
}

class _ImageNetwork extends StatelessWidget {
  final double? height;
  final double? width;
  final String url;
  final BoxFit? fit;
  final String? assetImageIfError;
  final Color? color;

  const _ImageNetwork({
    required this.url,
    this.fit = BoxFit.contain,
    this.height,
    this.width,
    this.assetImageIfError,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    if (url.isEmpty) {
      return SizedBox(
        height: height,
        width: width,
        child: _onError(),
      );
    }
    return SizedBox(
      width: width,
      height: height,
      child: CachedNetworkImage(
        imageUrl: url,
        fit: fit,
        errorWidget: (context, url, error) => _onError(),
        placeholder: (context, url) => Container(),
        width: width,
        height: height,
        color: color,
        fadeOutDuration: const Duration(milliseconds: 500),
      ),
    );
  }

  Widget _onError() {
    return Opacity(
      opacity: 0.5,
      child: Padding(
        padding: const EdgeInsets.all(4),
        child: Container(
          color: AppColors.white,
          width: width,
          height: height,
          child: assetImageIfError != null ||
                  (assetImageIfError?.isNotEmpty ?? false)
              ? Image.asset(
                  assetImageIfError!,
                  package: StringManager.package,
                  fit: fit,
                )
              : null,
        ),
      ),
    );
  }
}
