import 'package:sizer/sizer.dart';

class SizingValue {
  static double fromString(String stringSize) {
    // [092325Tin] Add handle part for values: infinity, maxFinite, minPositive, negativeInfinity, nan
    final double? enumValue = SizingValue.fromEnumString(stringSize);
    if (enumValue != null) return enumValue;

    final parts = stringSize.split(".");

    if (parts.length < 2) {
      return double.parse(stringSize);
    }

    final unit = parts.last;
    double value;
    switch (unit) {
      case 'w':
        parts.removeLast();
        value = double.parse(parts.join("."));
        return value.w;
      case 'h':
        parts.removeLast();
        value = double.parse(parts.join("."));
        return value.h;
      // [092319Tin] Add .sp unit
      case 'sp':
        parts.removeLast();
        value = double.parse(parts.join("."));
        return value.sp;
      default:
        return double.parse(parts.join("."));
    }
  }

  // [092325Tin] Add method handle string values:
  // infinity, maxFinite, minPositive, negativeInfinity, nan
  // to dobule value
  static double? fromEnumString(String string) {
    switch (string) {
      case 'infinity':
        return double.infinity;
      case 'maxFinite':
        return double.maxFinite;
      case 'minPositive':
        return double.minPositive;
      case 'negativeInfinity':
        return double.negativeInfinity;
      case 'nan':
        return double.nan;
      default:
        return null;
    }
  }
}
