import 'package:flutter/material.dart';

import '../common/colors/app_color.dart';

class CustomCard extends StatelessWidget {
  final Widget child;
  final Color? color;
  final EdgeInsetsGeometry? padding;

  const CustomCard({
    super.key,
    this.color,
    this.padding,
    required this.child,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(
            Radius.circular(20),
          ),
          // color: color ?? AppColors.cardBackgroundColor,
          color: color ??
              Colors
                  .transparent, //[102316HG] Makeup: 202936, có ảnh hưởng đến BG Analysis
        ),
        child: Padding(
          // padding: padding ?? const EdgeInsets.all(12.0),
          padding: padding ?? const EdgeInsets.symmetric(vertical: 12.0),
          child: child,
        ));
  }
}
