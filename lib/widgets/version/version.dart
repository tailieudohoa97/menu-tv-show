import 'package:flutter/material.dart';
import 'package:tv_menu/helper/app_setting.dart';

Widget version(BuildContext context) {
  String _ver = "_" + AppSetting.ver;
  Size _sizeDevice = MediaQuery.of(context).size;
  return Visibility(
    visible: true,
    child: Material(
      child: Align(
        alignment: Alignment.bottomRight,
        child: RotatedBox(
            quarterTurns: 3,
            child: Text(
              _ver,
              style: const TextStyle(
                color: Colors.black45,
                fontSize: 12,
                height: 1,
              ),
            )),
      ),
    ),
  );
}
