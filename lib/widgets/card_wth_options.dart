import 'package:flutter/material.dart';

Widget cardCTA(
    {Widget? icon,
    Widget? midchild,
    Widget? child,
    double? height,
    double? width,
    Function()? onPressed}) {
  return SizedBox(
    height: height ?? 30,
    width: width ?? 30,
    child: InkWell(
      borderRadius: BorderRadius.circular(15.0), //[102312HG] Makeup
      onTap: onPressed,
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Color(0xff212121), //[102312HG] Makeup
        elevation: 4.0,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              icon ?? const SizedBox(height: 0),
              const SizedBox(height: 5),
              midchild ?? const SizedBox(height: 0),
              const SizedBox(height: 5),
              child ?? const SizedBox(height: 0)
            ],
          ),
        ),
      ),
    ),
  );
}

// class CardWithOptions extends StatelessWidget {
//   Widget? icon;
//   //Widget? image;
//   Widget? text1;
//   Widget? text2;
//   double height;
//   double width;
//   dynamic onPressed;

//   CardWithOptions({
//     Key? key,
//     this.icon,
//     //this.image,
//     this.text1,
//     this.text2,
//     required this.height,
//     required this.width,
//     required this.onPressed,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       height: height,
//       width: width,
//       child: InkWell(
//         onTap: onPressed,
//         child: Card(
//           shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(15.0),
//           ),
//           color: Colors.black,
//           elevation: 4.0,
//           child:
//               //Stack(children: [
//               Center(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               crossAxisAlignment: CrossAxisAlignment.center,
//               children: [
//                 icon == null ? SizedBox(height: 0) : icon!,
//                 //image == null ? SizedBox(height: 0) : image!,
//                 const SizedBox(height: 5),
//                 text1 == null ? SizedBox(height: 0) : text1!,
//                 const SizedBox(height: 5),
//                 text2 == null ? SizedBox(height: 0) : text2!
//               ],
//               //   ),
//             ),
//             //  ]
//           ),
//         ),
//       ),
//     );
//   }
// }
