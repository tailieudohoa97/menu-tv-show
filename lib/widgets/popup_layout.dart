import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tv_menu/common/colors/app_color.dart';

//[112301TIN] add max width for dialog
//[102316HG] Create this widget
Widget popupLayout({
  required Widget child,
  double maxWidth = double.infinity,
}) {
  return AlertDialog(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(20),
    ),
    backgroundColor: Colors.transparent,
    content: BackdropFilter(
      filter: ImageFilter.blur(
          sigmaX: 5, sigmaY: 5), // Điều chỉnh mức độ blur tại đây
      // [112301TIN] set max width for dialog
      child: Container(
        // [102324TIN] Decrease padding from 40 to 24
        padding: const EdgeInsets.all(24),
        constraints: BoxConstraints(maxWidth: maxWidth),
        // [102323TIN] makeup dialog
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24),
          // border: Border.all(color: Colors.white, width: 2),
          // color: Colors.white.withOpacity(0.09),
          // color: Colors.grey.withOpacity(0.09),
          // [102324TIN] change popup color
          color: AppColors.cardBackgroundColor,

          // image: DecorationImage(
          //   image: AssetImage('assets/dialog-bg-2.png'),
          //   fit: BoxFit.cover,
          // ),
        ),
        child: child,
      ),
    ),

    // content: Container(
    //   padding: const EdgeInsets.all(40),
    //   decoration: BoxDecoration(
    //     borderRadius: BorderRadius.circular(12),
    //     border: Border.all(color: Colors.white, width: 2),
    //     color: Colors.black54,
    //     image: DecorationImage(
    //       image: AssetImage('assets/dialog-bg-2.png'),
    //       fit: BoxFit.cover,
    //     ),
    //   ),
    //   child: child,
    // ),
  );
}
