import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class SwapperScreen extends StatefulWidget {
  const SwapperScreen({super.key});

  @override
  State<SwapperScreen> createState() => _SwapperScreenState();
}

class _SwapperScreenState extends State<SwapperScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //Default Page When Customer doen't input any TemplateID and Replate With Home or Widget Inform
    context.go('/template/1');
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
