/* [092301TREE]
Fake templates data
*/
class DataApp {
  static final templates = {
    "templates": [
      {
        "id": 1,
        "name": "Painted stork",
        "subscriptionLevel": "free",
        "jsonData": '''{"widget_setting": "
        "widgetName": "Container",
        "widgetSetting": {
          "decoration": {
            "image": "/temp_restaurant_1/BG.png",
            "boxFit": "fill"
          },
          "child": {
            "widgetName": "Row",
            "widgetSetting": {
              "textRightToLeft": "false",
              "children": [
                {
                  "widgetName": "Expanded",
                  "widgetSetting": {
                    "child": {
                      "widgetName": "Padding",
                      "widgetSetting": {
                        "padding": {"all": "2.w"}
                      }
                    }
                  }
                },
                {"widgetName": "Expanded", "widgetSetting": {}}
              ]
            }
          }
        }
      }"}'''
      },
      {
        "id": 2,
        "name": "Tortoise black mountain",
        "subscriptionLevel": "medium",
        "jsonData": '''{"widget_setting": "
        "widgetName": "Container",
        "widgetSetting": {
          "decoration": {
            "image": "/temp_restaurant_1/BG.png",
            "boxFit": "fill"
          },
          "child": {
            "widgetName": "Row",
            "widgetSetting": {
              "textRightToLeft": "false",
              "children": [
                {
                  "widgetName": "Expanded",
                  "widgetSetting": {
                    "child": {
                      "widgetName": "Padding",
                      "widgetSetting": {
                        "padding": {"all": "2.w"}
                      }
                    }
                  }
                },
                {"widgetName": "Expanded", "widgetSetting": {}}
              ]
            }
          }
        }
      }"}'''
      },
      {
        "id": 3,
        "name": "Southern right whale",
        "subscriptionLevel": "master",
        "jsonData": '''{"widget_setting": "
        "widgetName": "Container",
        "widgetSetting": {
          "decoration": {
            "image": "/temp_restaurant_1/BG.png",
            "boxFit": "fill"
          },
          "child": {
            "widgetName": "Row",
            "widgetSetting": {
              "textRightToLeft": "false",
              "children": [
                {
                  "widgetName": "Expanded",
                  "widgetSetting": {
                    "child": {
                      "widgetName": "Padding",
                      "widgetSetting": {
                        "padding": {"all": "2.w"}
                      }
                    }
                  }
                },
                {"widgetName": "Expanded", "widgetSetting": {}}
              ]
            }
          }
        }
      }"}'''
      },
      {
        "id": 4,
        "name": "Opossum, american virginia",
        "subscriptionLevel": "master",
        "jsonData": '''{"widget_setting": "
        "widgetName": "Container",
        "widgetSetting": {
          "decoration": {
            "image": "/temp_restaurant_1/BG.png",
            "boxFit": "fill"
          },
          "child": {
            "widgetName": "Row",
            "widgetSetting": {
              "textRightToLeft": "false",
              "children": [
                {
                  "widgetName": "Expanded",
                  "widgetSetting": {
                    "child": {
                      "widgetName": "Padding",
                      "widgetSetting": {
                        "padding": {"all": "2.w"}
                      }
                    }
                  }
                },
                {"widgetName": "Expanded", "widgetSetting": {}}
              ]
            }
          }
        }
      }"}'''
      },
      {
        "id": 5,
        "name": "Chilean flamingo",
        "subscriptionLevel": "master",
        "jsonData": '''{"widget_setting": "
        "widgetName": "Container",
        "widgetSetting": {
          "decoration": {
            "image": "/temp_restaurant_1/BG.png",
            "boxFit": "fill"
          },
          "child": {
            "widgetName": "Row",
            "widgetSetting": {
              "textRightToLeft": "false",
              "children": [
                {
                  "widgetName": "Expanded",
                  "widgetSetting": {
                    "child": {
                      "widgetName": "Padding",
                      "widgetSetting": {
                        "padding": {"all": "2.w"}
                      }
                    }
                  }
                },
                {"widgetName": "Expanded", "widgetSetting": {}}
              ]
            }
          }
        }
      }"}'''
      },
      {
        "id": 6,
        "name": "Egyptian vulture",
        "subscriptionLevel": "master",
        "jsonData": '''{"widget_setting": "
        "widgetName": "Container",
        "widgetSetting": {
          "decoration": {
            "image": "/temp_restaurant_1/BG.png",
            "boxFit": "fill"
          },
          "child": {
            "widgetName": "Row",
            "widgetSetting": {
              "textRightToLeft": "false",
              "children": [
                {
                  "widgetName": "Expanded",
                  "widgetSetting": {
                    "child": {
                      "widgetName": "Padding",
                      "widgetSetting": {
                        "padding": {"all": "2.w"}
                      }
                    }
                  }
                },
                {"widgetName": "Expanded", "widgetSetting": {}}
              ]
            }
          }
        }
      }"}'''
      },
      {
        "id": 7,
        "name": "Yellow-billed hornbill",
        "subscriptionLevel": "medium",
        "jsonData": '''{"widget_setting": "
        "widgetName": "Container",
        "widgetSetting": {
          "decoration": {
            "image": "/temp_restaurant_1/BG.png",
            "boxFit": "fill"
          },
          "child": {
            "widgetName": "Row",
            "widgetSetting": {
              "textRightToLeft": "false",
              "children": [
                {
                  "widgetName": "Expanded",
                  "widgetSetting": {
                    "child": {
                      "widgetName": "Padding",
                      "widgetSetting": {
                        "padding": {"all": "2.w"}
                      }
                    }
                  }
                },
                {"widgetName": "Expanded", "widgetSetting": {}}
              ]
            }
          }
        }
      }"}'''
      },
      {
        "id": 8,
        "name": "Kafue flats lechwe",
        "subscriptionLevel": "medium",
        "jsonData": '''{"widget_setting": "
        "widgetName": "Container",
        "widgetSetting": {
          "decoration": {
            "image": "/temp_restaurant_1/BG.png",
            "boxFit": "fill"
          },
          "child": {
            "widgetName": "Row",
            "widgetSetting": {
              "textRightToLeft": "false",
              "children": [
                {
                  "widgetName": "Expanded",
                  "widgetSetting": {
                    "child": {
                      "widgetName": "Padding",
                      "widgetSetting": {
                        "padding": {"all": "2.w"}
                      }
                    }
                  }
                },
                {"widgetName": "Expanded", "widgetSetting": {}}
              ]
            }
          }
        }
      }"}'''
      },
      {
        "id": 9,
        "name": "Cobra (unidentified)",
        "subscriptionLevel": "free",
        "jsonData": '''{"widget_setting": "
        "widgetName": "Container",
        "widgetSetting": {
          "decoration": {
            "image": "/temp_restaurant_1/BG.png",
            "boxFit": "fill"
          },
          "child": {
            "widgetName": "Row",
            "widgetSetting": {
              "textRightToLeft": "false",
              "children": [
                {
                  "widgetName": "Expanded",
                  "widgetSetting": {
                    "child": {
                      "widgetName": "Padding",
                      "widgetSetting": {
                        "padding": {"all": "2.w"}
                      }
                    }
                  }
                },
                {"widgetName": "Expanded", "widgetSetting": {}}
              ]
            }
          }
        }
      }"}'''
      },
      {
        "id": 10,
        "name": "North American beaver",
        "subscriptionLevel": "medium",
        "jsonData": '''{"widget_setting": "
        "widgetName": "Container",
        "widgetSetting": {
          "decoration": {
            "image": "/temp_restaurant_1/BG.png",
            "boxFit": "fill"
          },
          "child": {
            "widgetName": "Row",
            "widgetSetting": {
              "textRightToLeft": "false",
              "children": [
                {
                  "widgetName": "Expanded",
                  "widgetSetting": {
                    "child": {
                      "widgetName": "Padding",
                      "widgetSetting": {
                        "padding": {"all": "2.w"}
                      }
                    }
                  }
                },
                {"widgetName": "Expanded", "widgetSetting": {}}
              ]
            }
          }
        }
      }"}'''
      },
    ]
  };
}
