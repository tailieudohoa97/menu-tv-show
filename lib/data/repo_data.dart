class RepoData {
  static Map<dynamic, dynamic> data = {
    "workers": [
      {
        "userId": 17,
        "username": "shopmanager",
        "pin": "1234",
        "phone": "0987654321",
        "email": "huannhi1412@gmail.com",
        "avatar":
            "https://secure.gravatar.com/avatar/822a3f6a35aa8f5fa3151368c5f6324c?s=96&d=mm&r=g",
        "role": "shop_manager",
        "nonce": "OZEJK5LHEmayyFU8ZdZnylV7WoQHmmyqEFT9TrYd0W4"
      },
      {
        "pin": "0143",
        "phoneNo": "6479220143",
        "username": "DARCY",
        "role": "admin",
        "status": "",
        "totalSale": "0",
        "email": ""
      }
    ],
    "categories": [
      {
        "id": "123",
        "name": "prodA",
        "slug": "prodA",
        "desc": "",
        "count": 120,
        "child": [
          {"tag": "7g", "count": 10},
          {"tag": "3g", "count": 3}
        ]
      },
      {
        "id": "123",
        "name": "prodA-hybrid",
        "slug": "prodA-hybrid",
        "desc": "",
        "count": 120,
        "child": [
          {"tag": "7g", "count": 10},
          {"tag": "3g", "count": 3},
          {"tag": "1g", "count": 5}
        ]
      },
      {
        "id": "123",
        "name": "Indica",
        "slug": "Indica",
        "desc": "",
        "count": 120,
        "child": [
          {"tag": "7g", "count": 10},
          {"tag": "3g", "count": 3}
        ]
      }
    ],
    "products": [
      {
        "sku": "83",
        "id": "sp1",
        "cat": "prodA",
        "tag": "7.0g",
        "img": "sp3.png",
        "name": "Apple Pie",
        "qTy": "5",
        "weight": "7.0g",
        "price": "50.00",
        "desc": "",
        "thc": "30%",
        "specialPrices": [
          {"desc": "1/4oz", "weight": "7.0g", "price": "70.00"},
          {"desc": "1/2oz", "weight": "14.0g", "price": "140.00"},
          {"desc": "1oz", "weight": "28.0g", "price": "260.00"}
        ]
      },
      {
        "sku": "83",
        "id": "sp2",
        "cat": "prodA",
        "tag": "3.0g",
        "name": "Chocolate",
        "img": "sp2.png",
        "qTy": "5",
        "weight": "3.0g",
        "price": "50.00",
        "desc": "",
        "thc": "30%",
        "specialPrices": [
          {"desc": "1/4oz", "weight": "7.0g", "price": "70.00"},
          {"desc": "1/2oz", "weight": "14.0g", "price": "140.00"},
          {"desc": "1oz", "weight": "28.0g", "price": "260.00"}
        ]
      },
      {
        "sku": "83",
        "id": "sp3",
        "cat": "prodA",
        "tag": "1.0g",
        "img": "sp1.png",
        "name": "Bread",
        "qTy": "5",
        "weight": "1.0g",
        "price": "50.00",
        "desc": "",
        "thc": "30%",
        "specialPrices": [
          {"desc": "1/4oz", "weight": "7.0g", "price": "70.00"},
          {"desc": "1/2oz", "weight": "14.0g", "price": "140.00"},
          {"desc": "1oz", "weight": "28.0g", "price": "260.00"}
        ]
      }
    ],
    "orders": [
      {
        "id": "1687329891",
        "date": "2023-06-21 15:28:05.510",
        "products": [
          {
            "product": {
              "sku": "491",
              "id": "1512673",
              "name": "PRESIDENTIAL PINK",
              "note": "null",
              "desc":
                  "<h1>Presidential Pink</h1>\n<h2>Lawrence Smoke is a 24 Hour Scarborough Dispensary. Visit our marijuana shop and check out our cannabis menu&#8217;s hottest strains selection.</h2>\n",
              "cat": "indica-hybrid",
              "img":
                  "https://staging.lawrencesmoke.ca/wp-content/uploads/2022/12/491-Presidential-Pink.jpg",
              "tags": ["33-thc", "premium2"],
              "weight": "",
              "price": "15",
              "stock": "3",
              "specialPrices": null
            },
            "qTy": "3",
            "key": "4d2dc0e3fd381b2c19e1cabf2dce5713",
            "total": "45",
            "unitBag": "null"
          },
          {
            "product": {
              "sku": "708",
              "id": "1512719",
              "name": "BACKWOODS DARK STOUT",
              "note": "null",
              "desc":
                  "<h1><b>Backwoods Dark Stout</b></h1>\n<h2>Lawrence Smoke is a 24 Hour Scarborough Dispensary. Visit our marijuana shop and check out our cannabis menu&#8217;s hottest strains selection.</h2>\n",
              "cat": "pre-rolls-grabba",
              "img":
                  "https://staging.lawrencesmoke.ca/wp-content/uploads/2022/12/708-backwoods-dark-stout-cannabis-dispensary.jpg",
              "tags": [""],
              "weight": "",
              "price": "25",
              "stock": "3",
              "specialPrices": null
            },
            "qTy": "2",
            "key": "7f77dddafc46d3fb9ff17c9b12e9362a",
            "total": "50",
            "unitBag": "null"
          },
          {
            "product": {
              "sku": "785",
              "id": "1512721",
              "name": "COOKIES 510 900MAH BATTERY",
              "note": "null",
              "desc":
                  "<h1><b>Cookies 510 900mAh Battery</b></h1>\n<p><strong>INCLUDES:</strong></p>\n<ul type=\"disc\">\n<li>1 Cookies 510 Thread 900mAh Battery</li>\n<li>1 Cookies Charger</li>\n</ul>\n<p><strong>FEATURES:</strong></p>\n<ul type=\"disc\">\n<li>Cookies 900 mAh Batteries</li>\n<li>510 Thread Compatible</li>\n<li>2 Click Pre-Heat Mode</li>\n<li>Voltage: 3.3V – 4.8V</li>\n<li>On / Off Function / Click 5 Time Fast</li>\n<li>Include USB Charger</li>\n<li>Overcharge Protection</li>\n</ul>\n",
              "cat": "vape-pen",
              "img":
                  "https://staging.lawrencesmoke.ca/wp-content/uploads/2022/12/785-cookies-510-mah-bettery.jpeg",
              "tags": [""],
              "weight": "",
              "price": "20",
              "stock": "3",
              "specialPrices": null
            },
            "qTy": "1",
            "key": "4e2e02c28346ffd25816867a06678bc2",
            "total": "20",
            "unitBag": "null"
          },
          {
            "product": {
              "sku": "392-oz",
              "id": "1512566",
              "name": "BC KUSH",
              "note": "null",
              "desc":
                  "<h1><b>BC Kush</b></h1>\n<h2>Lawrence Smoke is a 24 Hour Scarborough Dispensary. Visit our marijuana shop and check out our cannabis menu&#8217;s hottest strains selection.</h2>\n<p><strong>ENJOY OUR OZ SPECIAL!</strong></p>\n<p><strong>Non-Members: </strong><strong>110 | </strong><strong>Members: </strong><strong>90</strong></p>\n<p><strong>*(Only on selected strains &amp; excluding other promos)*</strong></p>\n",
              "cat": "indica",
              "img":
                  "https://staging.lawrencesmoke.ca/wp-content/uploads/2022/12/392-BC-Kush.jpg",
              "tags": ["27-thc", "oz-special"],
              "weight": "",
              "price": "90",
              "stock": "3",
              "specialPrices": null
            },
            "qTy": "1",
            "key": "9cb04938e360adcce2838913ab223dd7",
            "total": "90",
            "unitBag": "null"
          }
        ],
        "total": "205.00",
        "status": "Pending payment",
        "orderNote": "",
        "shipAddr": "",
        "latlng": [],
        "locID": "locID",
        "telno": "",
        "web": "",
        "shipping": {"feeId": "none", "shipType": "none", "shipValue": "0"},
        "feeOrDiscount": [],
        "coupon": {
          "couponCode": "none",
          "couponAmount": "0",
          "couponExpiry": "none",
          "couponDesc": "none"
        },
        "etrans": "",
        "payments": [],
        "customer": {
          "phone": "123",
          "email": null,
          "username": "Test",
          "address": null,
          "loyaltyPoint": null,
          "id": ""
        },
        "origin": "",
        "promotion": []
      },
      {
        "id": "1686940562",
        "date": "2023-06-21 15:30:56.108",
        "products": [
          {
            "product": {
              "sku": "442",
              "id": "1512623",
              "name": "MEAT BREATH",
              "note": "null",
              "desc":
                  "<h1>Meat Breath</h1>\n<h2>Lawrence Smoke is a 24 Hour Scarborough Dispensary. Visit our marijuana shop and check out our cannabis menu&#8217;s hottest strains selection.</h2>\n",
              "cat": "indica",
              "img":
                  "https://staging.lawrencesmoke.ca/wp-content/uploads/2022/12/442-Meat-Breath.jpg",
              "tags": ["34-thc", "aaa"],
              "weight": "",
              "price": "10",
              "stock": "3",
              "specialPrices": null
            },
            "qTy": "1",
            "key": "013021e5d3c6be72f3cd51fb683c3f00",
            "total": "5",
            "unitBag": "null"
          }
        ],
        "total": "4.50",
        "status": "Pending payment",
        "orderNote": "",
        "shipAddr": "",
        "latlng": [],
        "locID": "locID",
        "telno": "",
        "web": "",
        "shipping": {"feeId": "none", "shipType": "none", "shipValue": "0"},
        "feeOrDiscount": [
          {
            "fodType": "Discount",
            "fodValue": "10",
            "fodIndex": "1686940835",
            "fodAmountType": "Percentage",
            "reason": "Discount"
          }
        ],
        "coupon": {
          "couponCode": "none",
          "couponAmount": "0",
          "couponExpiry": "none",
          "couponDesc": "none"
        },
        "etrans": "",
        "payments": [],
        "customer": {
          "phone": "123",
          "email": null,
          "username": "Test",
          "address": null,
          "loyaltyPoint": null,
          "id": ""
        },
        "origin": "",
        "promotion": [
          {
            "promotionId": "1512901",
            "promotionTitle": "demo0611623",
            "promotionCode": "vphs3zwr",
            "promotionAmount": "50",
            "basedOn": "",
            "promotionType": "percent",
            "dateAllow": [],
            "startTime": null,
            "expiryTime": null,
            "categoryIds": [],
            "productIds": [],
            "individualUse": "",
            "usageLimit": "",
            "maxItemApply": "",
            "autoApply": ""
          }
        ]
      },
      {
        "id": "1687336477152",
        "date": "2023-06-21 15:36:56.649",
        "products": [
          {
            "product": {
              "sku": "188",
              "id": "1512416",
              "name": "PRE-ROLLS ( HYBRID )",
              "note": "null",
              "desc":
                  "<h1>PRE-ROLLS (HYBRID)</h1>\n<h2>Lawrence Smoke is a 24 Hour Scarborough Dispensary. Visit our marijuana shop and check out our cannabis menu&#8217;s hottest strains selection.</h2>\n<p>We’ve had a ton of requests for pre-rolled joints from our customers, so we went out and found some folks to roll ’em up for all of you. Pre-rolls are great for when you’re on the go, or don’t have easy access to the tools you need to roll the perfect joint yourself. Maybe you’re not sure exactly how to roll a joint, that’s cool! This product is for you. It might not teach you how to roll yourself, but it’ll circumvent the problem all the same.</p>\n<p>Our pre-rolled joints contain a mix of strains, which means they tend to smoke like hybrid flower. Expect a wide array of physical and mental effects, without a clear focus on one or the other. Effects may vary from one pre-roll to the next, as each is packed with a variety of the most potent strains we offer.</p>\n<p>Pre-rolls are usually one of the first things to try when you’re shopping around for a cannabis store, so we want ours to be the best on the market.</p>\n<p>Each pre-roll contains 1 gram of marijuana flower, but the exact combination of strains will be different each time. Every pre-rolled joint has just a touch of mystery inside, which is part of the fun! You never know exactly what you’ll get.</p>\n<p>Lighter not included, so be sure to spend a few too many minutes doing laps around your house trying to find the one you stashed away. It’s probably right in that spot you would NEVER have put it.</p>\n",
              "cat": "pre-rolls-grabba",
              "img":
                  "https://staging.lawrencesmoke.ca/wp-content/uploads/2022/12/188-Prerolls-3-for-20.jpg",
              "tags": [""],
              "weight": "",
              "price": "7",
              "stock": "3",
              "specialPrices": null
            },
            "qTy": "1",
            "key": "32d7b4bcdaf4d597cfed20bc86ff9a9b",
            "total": "7",
            "unitBag": "null"
          },
          {
            "product": {
              "sku": "701",
              "id": "1512705",
              "name": "BACKWOODS RUSSIAN CREAM",
              "note": "null",
              "desc":
                  "<h1><b>Backwoods Russian Cream</b></h1>\n<h2>Lawrence Smoke is a 24 Hour Scarborough Dispensary. Visit our marijuana shop and check out our cannabis menu&#8217;s hottest strains selection.</h2>\n",
              "cat": "pre-rolls-grabba",
              "img":
                  "https://staging.lawrencesmoke.ca/wp-content/uploads/2022/12/701-backwoods-russian-cream-cannabis-dispensary.jpg",
              "tags": [""],
              "weight": "",
              "price": "20",
              "stock": "3",
              "specialPrices": null
            },
            "qTy": "1",
            "key": "c1dd54bcf509d2cd9ab58ae95ca6f73c",
            "total": "20",
            "unitBag": "null"
          }
        ],
        "total": "27.00",
        "status": "Pending payment",
        "orderNote": "",
        "shipAddr": "",
        "latlng": [],
        "locID": "locID",
        "telno": "",
        "web": "",
        "shipping": {"feeId": "none", "shipType": "none", "shipValue": "0"},
        "feeOrDiscount": [],
        "coupon": {
          "couponCode": "none",
          "couponAmount": "0",
          "couponExpiry": "none",
          "couponDesc": "none"
        },
        "etrans": "",
        "payments": [],
        "customer": {
          "phone": "123",
          "email": null,
          "username": "Test",
          "address": null,
          "loyaltyPoint": null,
          "id": ""
        },
        "origin": "",
        "promotion": []
      },
      {
        "id": "1682101308",
        "date": "2023-06-21 15:37:55.260",
        "products": [
          {
            "product": {
              "sku": "389-oz",
              "id": "1512565",
              "name": "BLACK LABEL KUSH",
              "note": "null",
              "desc":
                  "<h1><b>Black Label Kush</b></h1>\n<h2>Lawrence Smoke is a 24 Hour Scarborough Dispensary. Visit our marijuana shop and check out our cannabis menu&#8217;s hottest strains selection.</h2>\n<p><strong>ENJOY OUR OZ SPECIAL!</strong></p>\n<p><strong>Non-Members: </strong><strong>100 | </strong><strong>Members: </strong><strong>80</strong></p>\n<p><strong>*(Only on selected strains &amp; excluding other promos)*</strong></p>\n",
              "cat": "indica",
              "img":
                  "https://staging.lawrencesmoke.ca/wp-content/uploads/2022/12/389-Black-Label-Kush.jpg",
              "tags": ["27-thc", "oz-special"],
              "weight": "",
              "price": "80",
              "stock": "3",
              "specialPrices": null
            },
            "qTy": "1",
            "key": "5acfa4eedf928605f129780bc6cc0338",
            "total": "80",
            "unitBag": "null"
          },
          {
            "product": {
              "sku": "441",
              "id": "1512622",
              "name": "BLUEFIN TUNA",
              "note": "null",
              "desc":
                  "<h1><b>Bluefin Tuna</b></h1>\n<h2>Lawrence Smoke is a 24 Hour Scarborough Dispensary. Visit our marijuana shop and check out our cannabis menu&#8217;s hottest strains selection.</h2>\n<p>Bluefin Tuna is an Indica strain. You&#8217;ve discovered Bluefin Tuna, a strain with a truly unique flavor and a strong high. This flower has a sweet blueberry flavor with traces of diesel and a pungent fish flavor. The odor is extremely similar, but also very spicy and dominated by diesel. Bluefin Tuna Kush has a unique taste and a unique impact. It will cause your mind to soar while your body gradually fades away. You will begin to sense a steady influx of creative energy entering your thoughts. Your ideas will oscillate between clarity and obscurity. The subsequent heavy body high renders you immobile and unable to leave the couch. Bluefin Tuna Kush is an effective treatment for chronic discomfort, difficulty sleeping, chronic tension or anxiety, and muscle cramps or spasms. This bud has pepper-shaped, dusty green crystals with faint amber undertones, orange hairs, and a frosty coating of sandy amber crystal trichomes.</p>\n",
              "cat": "indica",
              "img":
                  "https://staging.lawrencesmoke.ca/wp-content/uploads/2022/12/441-Bluefin-Tuna.jpg",
              "tags": ["33-thc", "aaa"],
              "weight": "",
              "price": "10",
              "stock": "3",
              "specialPrices": null
            },
            "qTy": "1",
            "key": "d7dea08d7fc61e89e9853546a1e68142",
            "total": "10",
            "unitBag": "null"
          }
        ],
        "total": "90.00",
        "status": "Pending payment",
        "orderNote": "",
        "shipAddr": "",
        "latlng": [],
        "locID": "locID",
        "telno": "",
        "web": "",
        "shipping": {"feeId": "none", "shipType": "none", "shipValue": "0"},
        "feeOrDiscount": [],
        "coupon": {
          "couponCode": "none",
          "couponAmount": "0",
          "couponExpiry": "none",
          "couponDesc": "none"
        },
        "etrans": "",
        "payments": [],
        "customer": {
          "phone": "123",
          "email": null,
          "username": "Test",
          "address": null,
          "loyaltyPoint": null,
          "id": ""
        },
        "origin": "",
        "promotion": []
      }
    ]
  };
}
