class JsonDataTemplate {
  static final template1 = {
    "_id": "64ee744398c3d4605b477abc",
    "templateSlug": "temp_restaurant_1",
    "RatioSetting": {
      "textStyle": {
        "categoryNameStyle": {
          "fontWeight": "bold",
          "fontFamily": "bebasNeue",
          "color": "white"
        },
        "nameProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "bebasNeue",
          "color": "white",
          "height": "1.5"
        },
        "nameProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "bebasNeue",
          "color": "white",
          "height": "1.5"
        },
        "discriptionProductStyle": {
          "fontWeight": "w300",
          "fontFamily": "bebasNeue",
          "color": "white",
          "height": "1.5"
        },
        "discriptionProductStyleMedium": {
          "fontWeight": "w300",
          "fontFamily": "openSans",
          "color": "white",
          "height": "1.5"
        },
        "priceProductStyle": {
          "fontWeight": "w800",
          "fontFamily": "bebasNeue",
          "color": "white"
        },
        "priceProductStyleMedium": {
          "fontWeight": "w800",
          "fontFamily": "bebasNeue",
          "color": "white"
        }
      },
      "renderScreen": {
        "32/9": {
          "categoryNameStyle": "2.w",
          "nameProductStyle": "1.5.w",
          "nameProductStyleMedium": "1.w",
          "priceProductStyle": "1.5.w",
          "priceProductStyleMedium": "1.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.6.w",
          "calculatedimageHeight": "14.h",
          "calculatedMarginImage": "1.w",
          "calculatedMarginItem": "2.h",
          "boderRadiusStyle": "100",
          "calculatedMarginTilte": "1.h"
        },
        "16/9": {
          "categoryNameStyle": "2.5.w",
          "nameProductStyle": "1.7.w",
          "nameProductStyleMedium": "1.5.w",
          "priceProductStyle": "1.7.w",
          "priceProductStyleMedium": "1.5.w",
          "discriptionProductStyle": "0.7.w",
          "discriptionProductStyleMedium": "0.6.w",
          "calculatedimageHeight": "14.h",
          "calculatedMarginImage": "1.w",
          "calculatedMarginItem": "2.h",
          "boderRadiusStyle": "30",
          "calculatedMarginTilte": "1.h"
        }
      }
    },
    "isDeleted": false,
    "widgets_setting": {
      "widgetName": "Container",
      "widgetSetting": {
        "decoration": {"image": "/temp_restaurant_1/BG.png", "boxFit": "fill"},
        "child": {
          "widgetName": "Row",
          "widgetSetting": {
            "textRightToLeft": "false",
            "children": [
              {
                "widgetName": "Expanded",
                "widgetSetting": {
                  "child": {
                    "widgetName": "Padding",
                    "widgetSetting": {
                      "padding": {"all": "2.w"},
                      "child": {
                        "widgetName": "Column",
                        "widgetSetting": {
                          "children": [
                            {
                              "widgetName": "Expanded",
                              "widgetSetting": {
                                "child": {
                                  "widgetName": "Padding",
                                  "widgetSetting": {
                                    "padding": {"bottom": "1.w"},
                                    "child": {
                                      "widgetName": "Row",
                                      "widgetSetting": {
                                        "children": [
                                          {
                                            "widgetName": "Expanded",
                                            "widgetSetting": {
                                              "flex": 2,
                                              "child": {
                                                "widgetName": "Container",
                                                "widgetSetting": {
                                                  "decoration": {
                                                    "borderRadius": 30,
                                                    "color": "0x18FFFFFF"
                                                  },
                                                  "child": {
                                                    "widgetName":
                                                        "categoryTitleSingleColumn",
                                                    "widgetSetting": {
                                                      "indexCategory": 0
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          },
                                          {
                                            "widgetName": "SizedBox",
                                            "widgetSetting": {"width": "1.5.w"}
                                          },
                                          {
                                            "widgetName": "Expanded",
                                            "widgetSetting": {"child": {}}
                                          }
                                        ]
                                      }
                                    }
                                  }
                                }
                              }
                            },
                            {
                              "widgetName": "Expanded",
                              "widgetSetting": {"child": {}}
                            }
                          ]
                        }
                      }
                    }
                  }
                }
              },
              {"widgetName": "Expanded", "widgetSetting": {}}
            ]
          }
        }
      }
    }
  };
}
