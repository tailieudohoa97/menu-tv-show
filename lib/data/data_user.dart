/* [092301TREE]
Fake templates data
*/
class DataApp {
  static final users = {
    "users": [
      {
        "id": 1,
        "userName": "Hugosieulun",
        "passWord": "123456",
        "email": "hugo_sieulun@yahoo.com.vn",
        "subscriptionLevel": "diamond"
      },
      {
        "id": 2,
        "userName": "yweLmI",
        "passWord": "123456",
        "email": "ywelmi@gmail.com",
        "subscriptionLevel": "gold"
      },
      {
        "id": 3,
        "userName": "typicalZooo",
        "passWord": "123456",
        "email": "typicalZooo@yahoo.com.vn",
        "subscriptionLevel": "silver"
      },
      {
        "id": 4,
        "userName": "legolasZXX",
        "passWord": "123456",
        "email": "legolasZXX@yahoo.com.vn",
        "subscriptionLevel": "diamond"
      },
      {
        "id": 5,
        "userName": "dynamicT&T",
        "passWord": "123456",
        "email": "dynamicT@work.com.vn",
        "subscriptionLevel": "silver"
      },
    ]
  };
}
