import 'package:equatable/equatable.dart';

abstract class ScreenState extends Equatable {}

class InitialState extends ScreenState {
  @override
  List<Object> get props => [];
}

class LoadingState extends ScreenState {
  @override
  List<Object> get props => [];
}

class LoadedState extends ScreenState {
  LoadedState(this.screen);

  final dynamic screen;

  @override
  List<Object> get props => [screen];
}

class ErrorState extends ScreenState {
  @override
  List<Object> get props => [];
}
