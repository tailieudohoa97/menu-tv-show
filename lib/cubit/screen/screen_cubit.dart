import 'package:flutter_bloc/flutter_bloc.dart';

import '../../repo/request/general_request.dart';
import 'screen_state.dart';

class ScreenCubit extends Cubit<ScreenState> {
  ScreenCubit(
      {required this.templateId,
      required this.userId,
      required this.listProductId})
      : super(InitialState()) {
    getListProduct();
  }
  final String templateId;
  final String userId;
  final String listProductId;
  //function fetch list product from server
  void getListProduct() async {
    try {
      // emit loading progress while waiting for fetch from server
      emit(LoadingState());
      // call function fetch list product

      final screen = [];

      // emit loaded state after fetch product
      emit(LoadedState(screen!));
    } catch (e) {
      emit(ErrorState());
    }
  }
}
