import 'package:equatable/equatable.dart';

abstract class WorkerState extends Equatable {}

class InitialState extends WorkerState {
  @override
  List<Object> get props => [];
}

class LoadingState extends WorkerState {
  @override
  List<Object> get props => [];
}

class LoadedState extends WorkerState {
  LoadedState(this.admin);

  final dynamic admin;

  @override
  List<Object> get props => [admin];
}

class ErrorState extends WorkerState {
  final String message;

  ErrorState(this.message);
  @override
  List<Object> get props => [];
}
