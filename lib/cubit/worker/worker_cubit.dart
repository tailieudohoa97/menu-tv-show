import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:tv_menu/cubit/worker/worker_state.dart';
import 'package:tv_menu/repo/request/general_request.dart';

class WorkerCubit extends Cubit<WorkerState> {
  WorkerCubit() : super(InitialState());

  //function fetch list product from server

  Future<void> login(String username, String password) async {
    try {
      // emit loading progress while waiting for fetch from server
      emit(LoadingState());
      dynamic result = await GeneralRequest().fetchLogin(username, password);
      // call function fetch list product
      if (result != null) {
        Future.delayed(Duration(seconds: 1))
            .then((value) => emit(LoadedState("admin"!)));
      } else {
        Future.delayed(Duration(seconds: 1))
            .then((value) => emit(ErrorState("Wrong username or password")));
      }
      // emit loaded state after fetch product
    } catch (e) {
      emit(ErrorState(e.toString()));
    }
  }
}
