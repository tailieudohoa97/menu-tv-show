import 'dart:convert';
import 'dart:html';

import 'package:http/http.dart' as http;

import 'package:tv_menu/helper/app_exception.dart';

import 'package:tv_menu/helper/encrypt_random.dart';

class TemplateService {
  Future<Map<String, dynamic>> getTemplateData(int templateID) async {
    final ramdonToken = EncryptRandom().encryptRandom();
    final response = await http.get(Uri.parse(
        'https://foodiemenu.co/wp-json/tv-menu-api/v1/templates/get-template-data?templateId=$templateID&token=$ramdonToken'));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      if (response.body.isNotEmpty) {
        return json.decode(response.body);
      } else {
        return {};
      }
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw AppException();
    }
  }

  Future<Map<String, dynamic>> getListTemplateData() async {
    final response = await http.get(
        Uri.parse('https://foodiemenu.co/wp-json/tv-menu-api/v1/templates'));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      if (response.body.isNotEmpty) {
        return json.decode(response.body);
      } else {
        return {};
      }
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw AppException();
    }
  }

  Future<Map<String, dynamic>> getUserSettingTemplate(
      String tokenUserTemplate) async {
    final ramdonToken = EncryptRandom().encryptRandom();
    //TODO: Replace UserID by Token User
    final response = await http.get(Uri.parse(
        "https://foodiemenu.co/wp-json/tv-menu-api/v1/templates/get-user-setting-template?tokenUserTemplate=$tokenUserTemplate&token=$ramdonToken"));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      if (response.body.isNotEmpty) {
        return json.decode(response.body);
      } else {
        return {};
      }
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw AppException();
    }
  }

  static Future<List<dynamic>> getCategories() async {
    const categoryEndpoint =
        'https://foodiemenu.co/wp-json/tv-menu-api/v1/templates/categories/';
    final ramdonToken = EncryptRandom().encryptRandom();
    final categoryURL = Uri.parse('$categoryEndpoint?none=$ramdonToken');
    final response = await http.get(categoryURL);
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      if (response.body.isNotEmpty) {
        return json.decode(response.body);
      } else {
        return [];
      }
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw AppException();
    }
  }
}
